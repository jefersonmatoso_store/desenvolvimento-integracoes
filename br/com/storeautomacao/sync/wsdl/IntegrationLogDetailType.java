
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * IntegrationLogDetail is a structure containing the details of the log message - a qualifier/value pair
 * 
 * <p>Classe Java de IntegrationLogDetailType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="IntegrationLogDetailType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ILogDetailQualGid" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ILogDetailValue" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IntegrationLogDetailType", propOrder = {
    "iLogDetailQualGid",
    "iLogDetailValue"
})
public class IntegrationLogDetailType {

    @XmlElement(name = "ILogDetailQualGid", required = true)
    protected String iLogDetailQualGid;
    @XmlElement(name = "ILogDetailValue", required = true)
    protected String iLogDetailValue;

    /**
     * Obtém o valor da propriedade iLogDetailQualGid.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getILogDetailQualGid() {
        return iLogDetailQualGid;
    }

    /**
     * Define o valor da propriedade iLogDetailQualGid.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setILogDetailQualGid(String value) {
        this.iLogDetailQualGid = value;
    }

    /**
     * Obtém o valor da propriedade iLogDetailValue.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getILogDetailValue() {
        return iLogDetailValue;
    }

    /**
     * Define o valor da propriedade iLogDetailValue.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setILogDetailValue(String value) {
        this.iLogDetailValue = value;
    }

}
