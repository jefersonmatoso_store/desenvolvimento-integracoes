
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * PostalCodeRange is a range of postal codes.
 *             Example: 191 to 195.
 *             In this case the HnameComponentGid would be USZIP3.
 *          
 * 
 * <p>Classe Java de PostalCodeRangeType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="PostalCodeRangeType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="HnameComponentGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="LowRangeValue" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="HighRangeValue" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PostalCodeRangeType", propOrder = {
    "hnameComponentGid",
    "lowRangeValue",
    "highRangeValue"
})
public class PostalCodeRangeType {

    @XmlElement(name = "HnameComponentGid", required = true)
    protected GLogXMLGidType hnameComponentGid;
    @XmlElement(name = "LowRangeValue", required = true)
    protected String lowRangeValue;
    @XmlElement(name = "HighRangeValue", required = true)
    protected String highRangeValue;

    /**
     * Obtém o valor da propriedade hnameComponentGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getHnameComponentGid() {
        return hnameComponentGid;
    }

    /**
     * Define o valor da propriedade hnameComponentGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setHnameComponentGid(GLogXMLGidType value) {
        this.hnameComponentGid = value;
    }

    /**
     * Obtém o valor da propriedade lowRangeValue.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLowRangeValue() {
        return lowRangeValue;
    }

    /**
     * Define o valor da propriedade lowRangeValue.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLowRangeValue(String value) {
        this.lowRangeValue = value;
    }

    /**
     * Obtém o valor da propriedade highRangeValue.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHighRangeValue() {
        return highRangeValue;
    }

    /**
     * Define o valor da propriedade highRangeValue.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHighRangeValue(String value) {
        this.highRangeValue = value;
    }

}
