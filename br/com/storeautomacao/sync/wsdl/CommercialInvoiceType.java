
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;


/**
 * 
 *             The Commercial Invoice is one of the most common documents used in international trade.
 *             Primarily, the document contains details of what, how much, and the cost of what is being shipped.
 *             The cost of the goods can vary depending on payment terms.
 *          
 * 
 * <p>Classe Java de CommercialInvoiceType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="CommercialInvoiceType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CommercialInvoiceGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="TransactionCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransactionCodeType"/>
 *         &lt;element name="CommercialInvoiceType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CommercialInvoiceNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InvoiceDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="ExchangeRateValue" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="GlobalCurrencyCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ExchangeRateInfo" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ExchangeRateInfoType" minOccurs="0"/>
 *         &lt;element name="CurrencyFrom" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}CurrencyCodeType" minOccurs="0"/>
 *         &lt;element name="IsOverrideExchangeRate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ProductAmount" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType"/>
 *         &lt;element name="IsCalcProductAmt" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OtherCharge" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/>
 *         &lt;element name="IsCalcTotalInvoiceAmt" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TotalInvoiceAmount" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/>
 *         &lt;element name="CommercialTerms" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}CommercialTermsType" minOccurs="0"/>
 *         &lt;element name="FinalCommercialTerms" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FinalCommercialTermsType" minOccurs="0"/>
 *         &lt;element name="CommercialInvoiceTerms" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}CommercialInvoiceTermsType" minOccurs="0"/>
 *         &lt;element name="CommercialInvoiceCharge" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}CommercialInvoiceChargeType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CommercialInvoiceType", propOrder = {
    "commercialInvoiceGid",
    "transactionCode",
    "commercialInvoiceType",
    "commercialInvoiceNum",
    "invoiceDate",
    "exchangeRateValue",
    "globalCurrencyCode",
    "exchangeRateInfo",
    "currencyFrom",
    "isOverrideExchangeRate",
    "productAmount",
    "isCalcProductAmt",
    "otherCharge",
    "isCalcTotalInvoiceAmt",
    "totalInvoiceAmount",
    "commercialTerms",
    "finalCommercialTerms",
    "commercialInvoiceTerms",
    "commercialInvoiceCharge"
})
public class CommercialInvoiceType {

    @XmlElement(name = "CommercialInvoiceGid", required = true)
    protected GLogXMLGidType commercialInvoiceGid;
    @XmlElement(name = "TransactionCode", required = true)
    @XmlSchemaType(name = "string")
    protected TransactionCodeType transactionCode;
    @XmlElement(name = "CommercialInvoiceType")
    protected String commercialInvoiceType;
    @XmlElement(name = "CommercialInvoiceNum")
    protected String commercialInvoiceNum;
    @XmlElement(name = "InvoiceDate")
    protected GLogDateTimeType invoiceDate;
    @XmlElement(name = "ExchangeRateValue")
    protected String exchangeRateValue;
    @XmlElement(name = "GlobalCurrencyCode")
    protected String globalCurrencyCode;
    @XmlElement(name = "ExchangeRateInfo")
    protected ExchangeRateInfoType exchangeRateInfo;
    @XmlElement(name = "CurrencyFrom")
    protected CurrencyCodeType currencyFrom;
    @XmlElement(name = "IsOverrideExchangeRate")
    protected String isOverrideExchangeRate;
    @XmlElement(name = "ProductAmount", required = true)
    protected GLogXMLFinancialAmountType productAmount;
    @XmlElement(name = "IsCalcProductAmt")
    protected String isCalcProductAmt;
    @XmlElement(name = "OtherCharge")
    protected GLogXMLFinancialAmountType otherCharge;
    @XmlElement(name = "IsCalcTotalInvoiceAmt")
    protected String isCalcTotalInvoiceAmt;
    @XmlElement(name = "TotalInvoiceAmount")
    protected GLogXMLFinancialAmountType totalInvoiceAmount;
    @XmlElement(name = "CommercialTerms")
    protected CommercialTermsType commercialTerms;
    @XmlElement(name = "FinalCommercialTerms")
    protected FinalCommercialTermsType finalCommercialTerms;
    @XmlElement(name = "CommercialInvoiceTerms")
    protected CommercialInvoiceTermsType commercialInvoiceTerms;
    @XmlElement(name = "CommercialInvoiceCharge")
    protected List<CommercialInvoiceChargeType> commercialInvoiceCharge;

    /**
     * Obtém o valor da propriedade commercialInvoiceGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getCommercialInvoiceGid() {
        return commercialInvoiceGid;
    }

    /**
     * Define o valor da propriedade commercialInvoiceGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setCommercialInvoiceGid(GLogXMLGidType value) {
        this.commercialInvoiceGid = value;
    }

    /**
     * Obtém o valor da propriedade transactionCode.
     * 
     * @return
     *     possible object is
     *     {@link TransactionCodeType }
     *     
     */
    public TransactionCodeType getTransactionCode() {
        return transactionCode;
    }

    /**
     * Define o valor da propriedade transactionCode.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionCodeType }
     *     
     */
    public void setTransactionCode(TransactionCodeType value) {
        this.transactionCode = value;
    }

    /**
     * Obtém o valor da propriedade commercialInvoiceType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCommercialInvoiceType() {
        return commercialInvoiceType;
    }

    /**
     * Define o valor da propriedade commercialInvoiceType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCommercialInvoiceType(String value) {
        this.commercialInvoiceType = value;
    }

    /**
     * Obtém o valor da propriedade commercialInvoiceNum.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCommercialInvoiceNum() {
        return commercialInvoiceNum;
    }

    /**
     * Define o valor da propriedade commercialInvoiceNum.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCommercialInvoiceNum(String value) {
        this.commercialInvoiceNum = value;
    }

    /**
     * Obtém o valor da propriedade invoiceDate.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getInvoiceDate() {
        return invoiceDate;
    }

    /**
     * Define o valor da propriedade invoiceDate.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setInvoiceDate(GLogDateTimeType value) {
        this.invoiceDate = value;
    }

    /**
     * Obtém o valor da propriedade exchangeRateValue.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExchangeRateValue() {
        return exchangeRateValue;
    }

    /**
     * Define o valor da propriedade exchangeRateValue.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExchangeRateValue(String value) {
        this.exchangeRateValue = value;
    }

    /**
     * Obtém o valor da propriedade globalCurrencyCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGlobalCurrencyCode() {
        return globalCurrencyCode;
    }

    /**
     * Define o valor da propriedade globalCurrencyCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGlobalCurrencyCode(String value) {
        this.globalCurrencyCode = value;
    }

    /**
     * Obtém o valor da propriedade exchangeRateInfo.
     * 
     * @return
     *     possible object is
     *     {@link ExchangeRateInfoType }
     *     
     */
    public ExchangeRateInfoType getExchangeRateInfo() {
        return exchangeRateInfo;
    }

    /**
     * Define o valor da propriedade exchangeRateInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link ExchangeRateInfoType }
     *     
     */
    public void setExchangeRateInfo(ExchangeRateInfoType value) {
        this.exchangeRateInfo = value;
    }

    /**
     * Obtém o valor da propriedade currencyFrom.
     * 
     * @return
     *     possible object is
     *     {@link CurrencyCodeType }
     *     
     */
    public CurrencyCodeType getCurrencyFrom() {
        return currencyFrom;
    }

    /**
     * Define o valor da propriedade currencyFrom.
     * 
     * @param value
     *     allowed object is
     *     {@link CurrencyCodeType }
     *     
     */
    public void setCurrencyFrom(CurrencyCodeType value) {
        this.currencyFrom = value;
    }

    /**
     * Obtém o valor da propriedade isOverrideExchangeRate.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsOverrideExchangeRate() {
        return isOverrideExchangeRate;
    }

    /**
     * Define o valor da propriedade isOverrideExchangeRate.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsOverrideExchangeRate(String value) {
        this.isOverrideExchangeRate = value;
    }

    /**
     * Obtém o valor da propriedade productAmount.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getProductAmount() {
        return productAmount;
    }

    /**
     * Define o valor da propriedade productAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setProductAmount(GLogXMLFinancialAmountType value) {
        this.productAmount = value;
    }

    /**
     * Obtém o valor da propriedade isCalcProductAmt.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsCalcProductAmt() {
        return isCalcProductAmt;
    }

    /**
     * Define o valor da propriedade isCalcProductAmt.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsCalcProductAmt(String value) {
        this.isCalcProductAmt = value;
    }

    /**
     * Obtém o valor da propriedade otherCharge.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getOtherCharge() {
        return otherCharge;
    }

    /**
     * Define o valor da propriedade otherCharge.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setOtherCharge(GLogXMLFinancialAmountType value) {
        this.otherCharge = value;
    }

    /**
     * Obtém o valor da propriedade isCalcTotalInvoiceAmt.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsCalcTotalInvoiceAmt() {
        return isCalcTotalInvoiceAmt;
    }

    /**
     * Define o valor da propriedade isCalcTotalInvoiceAmt.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsCalcTotalInvoiceAmt(String value) {
        this.isCalcTotalInvoiceAmt = value;
    }

    /**
     * Obtém o valor da propriedade totalInvoiceAmount.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getTotalInvoiceAmount() {
        return totalInvoiceAmount;
    }

    /**
     * Define o valor da propriedade totalInvoiceAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setTotalInvoiceAmount(GLogXMLFinancialAmountType value) {
        this.totalInvoiceAmount = value;
    }

    /**
     * Obtém o valor da propriedade commercialTerms.
     * 
     * @return
     *     possible object is
     *     {@link CommercialTermsType }
     *     
     */
    public CommercialTermsType getCommercialTerms() {
        return commercialTerms;
    }

    /**
     * Define o valor da propriedade commercialTerms.
     * 
     * @param value
     *     allowed object is
     *     {@link CommercialTermsType }
     *     
     */
    public void setCommercialTerms(CommercialTermsType value) {
        this.commercialTerms = value;
    }

    /**
     * Obtém o valor da propriedade finalCommercialTerms.
     * 
     * @return
     *     possible object is
     *     {@link FinalCommercialTermsType }
     *     
     */
    public FinalCommercialTermsType getFinalCommercialTerms() {
        return finalCommercialTerms;
    }

    /**
     * Define o valor da propriedade finalCommercialTerms.
     * 
     * @param value
     *     allowed object is
     *     {@link FinalCommercialTermsType }
     *     
     */
    public void setFinalCommercialTerms(FinalCommercialTermsType value) {
        this.finalCommercialTerms = value;
    }

    /**
     * Obtém o valor da propriedade commercialInvoiceTerms.
     * 
     * @return
     *     possible object is
     *     {@link CommercialInvoiceTermsType }
     *     
     */
    public CommercialInvoiceTermsType getCommercialInvoiceTerms() {
        return commercialInvoiceTerms;
    }

    /**
     * Define o valor da propriedade commercialInvoiceTerms.
     * 
     * @param value
     *     allowed object is
     *     {@link CommercialInvoiceTermsType }
     *     
     */
    public void setCommercialInvoiceTerms(CommercialInvoiceTermsType value) {
        this.commercialInvoiceTerms = value;
    }

    /**
     * Gets the value of the commercialInvoiceCharge property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the commercialInvoiceCharge property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCommercialInvoiceCharge().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CommercialInvoiceChargeType }
     * 
     * 
     */
    public List<CommercialInvoiceChargeType> getCommercialInvoiceCharge() {
        if (commercialInvoiceCharge == null) {
            commercialInvoiceCharge = new ArrayList<CommercialInvoiceChargeType>();
        }
        return this.commercialInvoiceCharge;
    }

}
