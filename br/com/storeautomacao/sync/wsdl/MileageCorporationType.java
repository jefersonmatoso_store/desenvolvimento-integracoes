
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * SourceAddress is the Address of the Source.
 *             Note: The CorporationGid is only valid when the SourceAddress is in the OrderRoutingRuleQuery element.
 *          
 * 
 * <p>Classe Java de MileageCorporationType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="MileageCorporationType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="MileageAddress" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}MileageAddressType"/>
 *         &lt;element name="CorporationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MileageCorporationType", propOrder = {
    "mileageAddress",
    "corporationGid"
})
public class MileageCorporationType {

    @XmlElement(name = "MileageAddress", required = true)
    protected MileageAddressType mileageAddress;
    @XmlElement(name = "CorporationGid")
    protected GLogXMLGidType corporationGid;

    /**
     * Obtém o valor da propriedade mileageAddress.
     * 
     * @return
     *     possible object is
     *     {@link MileageAddressType }
     *     
     */
    public MileageAddressType getMileageAddress() {
        return mileageAddress;
    }

    /**
     * Define o valor da propriedade mileageAddress.
     * 
     * @param value
     *     allowed object is
     *     {@link MileageAddressType }
     *     
     */
    public void setMileageAddress(MileageAddressType value) {
        this.mileageAddress = value;
    }

    /**
     * Obtém o valor da propriedade corporationGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getCorporationGid() {
        return corporationGid;
    }

    /**
     * Define o valor da propriedade corporationGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setCorporationGid(GLogXMLGidType value) {
        this.corporationGid = value;
    }

}
