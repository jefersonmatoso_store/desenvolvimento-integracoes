
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de RegionalHandlingTimeType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="RegionalHandlingTimeType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FixedHandlingTime" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDurationType"/>
 *         &lt;element name="SourceRegionGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLRegionGidType" minOccurs="0"/>
 *         &lt;element name="DestRegionGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLRegionGidType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RegionalHandlingTimeType", propOrder = {
    "fixedHandlingTime",
    "sourceRegionGid",
    "destRegionGid"
})
public class RegionalHandlingTimeType {

    @XmlElement(name = "FixedHandlingTime", required = true)
    protected GLogXMLDurationType fixedHandlingTime;
    @XmlElement(name = "SourceRegionGid")
    protected GLogXMLRegionGidType sourceRegionGid;
    @XmlElement(name = "DestRegionGid")
    protected GLogXMLRegionGidType destRegionGid;

    /**
     * Obtém o valor da propriedade fixedHandlingTime.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public GLogXMLDurationType getFixedHandlingTime() {
        return fixedHandlingTime;
    }

    /**
     * Define o valor da propriedade fixedHandlingTime.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public void setFixedHandlingTime(GLogXMLDurationType value) {
        this.fixedHandlingTime = value;
    }

    /**
     * Obtém o valor da propriedade sourceRegionGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLRegionGidType }
     *     
     */
    public GLogXMLRegionGidType getSourceRegionGid() {
        return sourceRegionGid;
    }

    /**
     * Define o valor da propriedade sourceRegionGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLRegionGidType }
     *     
     */
    public void setSourceRegionGid(GLogXMLRegionGidType value) {
        this.sourceRegionGid = value;
    }

    /**
     * Obtém o valor da propriedade destRegionGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLRegionGidType }
     *     
     */
    public GLogXMLRegionGidType getDestRegionGid() {
        return destRegionGid;
    }

    /**
     * Define o valor da propriedade destRegionGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLRegionGidType }
     *     
     */
    public void setDestRegionGid(GLogXMLRegionGidType value) {
        this.destRegionGid = value;
    }

}
