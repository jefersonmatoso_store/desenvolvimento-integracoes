
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * An XLaneRef is either a reference to an existing lane, or a new lane definition.
 * 
 * <p>Classe Java de XLaneRefType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="XLaneRefType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element name="XLaneGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="XLane" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}XLaneType"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "XLaneRefType", propOrder = {
    "xLaneGid",
    "xLane"
})
public class XLaneRefType {

    @XmlElement(name = "XLaneGid")
    protected GLogXMLGidType xLaneGid;
    @XmlElement(name = "XLane")
    protected XLaneType xLane;

    /**
     * Obtém o valor da propriedade xLaneGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getXLaneGid() {
        return xLaneGid;
    }

    /**
     * Define o valor da propriedade xLaneGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setXLaneGid(GLogXMLGidType value) {
        this.xLaneGid = value;
    }

    /**
     * Obtém o valor da propriedade xLane.
     * 
     * @return
     *     possible object is
     *     {@link XLaneType }
     *     
     */
    public XLaneType getXLane() {
        return xLane;
    }

    /**
     * Define o valor da propriedade xLane.
     * 
     * @param value
     *     allowed object is
     *     {@link XLaneType }
     *     
     */
    public void setXLane(XLaneType value) {
        this.xLane = value;
    }

}
