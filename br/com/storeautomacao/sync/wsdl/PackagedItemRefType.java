
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Specifies packaged item information.
 * 
 * <p>Classe Java de PackagedItemRefType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="PackagedItemRefType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element name="PackagedItem" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}PackagedItemType"/>
 *         &lt;element name="PackagedItemGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PackagedItemRefType", propOrder = {
    "packagedItem",
    "packagedItemGid"
})
public class PackagedItemRefType {

    @XmlElement(name = "PackagedItem")
    protected PackagedItemType packagedItem;
    @XmlElement(name = "PackagedItemGid")
    protected GLogXMLGidType packagedItemGid;

    /**
     * Obtém o valor da propriedade packagedItem.
     * 
     * @return
     *     possible object is
     *     {@link PackagedItemType }
     *     
     */
    public PackagedItemType getPackagedItem() {
        return packagedItem;
    }

    /**
     * Define o valor da propriedade packagedItem.
     * 
     * @param value
     *     allowed object is
     *     {@link PackagedItemType }
     *     
     */
    public void setPackagedItem(PackagedItemType value) {
        this.packagedItem = value;
    }

    /**
     * Obtém o valor da propriedade packagedItemGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getPackagedItemGid() {
        return packagedItemGid;
    }

    /**
     * Define o valor da propriedade packagedItemGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setPackagedItemGid(GLogXMLGidType value) {
        this.packagedItemGid = value;
    }

}
