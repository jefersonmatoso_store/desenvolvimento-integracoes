
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Provides the information about the service provider equipment.
 * 
 * <p>Classe Java de ServprovEquipmentType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="ServprovEquipmentType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;choice>
 *           &lt;element name="SEquipmentGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *           &lt;element name="SEquipmentIndex" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *           &lt;element name="SEquipGidMatchOption" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *           &lt;element name="IntSavedQuery" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}IntSavedQueryType"/>
 *         &lt;/choice>
 *         &lt;element name="EquipmentGroupGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="EquipmentTypeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="EquipmentGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="EquipmentInitial" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EquipmentNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EquipmentInitialNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CheckDigit" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LicensePlate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServprovEquipmentType", propOrder = {
    "sEquipmentGid",
    "sEquipmentIndex",
    "sEquipGidMatchOption",
    "intSavedQuery",
    "equipmentGroupGid",
    "equipmentTypeGid",
    "equipmentGid",
    "equipmentInitial",
    "equipmentNumber",
    "equipmentInitialNumber",
    "checkDigit",
    "licensePlate"
})
public class ServprovEquipmentType {

    @XmlElement(name = "SEquipmentGid")
    protected GLogXMLGidType sEquipmentGid;
    @XmlElement(name = "SEquipmentIndex")
    protected String sEquipmentIndex;
    @XmlElement(name = "SEquipGidMatchOption")
    protected String sEquipGidMatchOption;
    @XmlElement(name = "IntSavedQuery")
    protected IntSavedQueryType intSavedQuery;
    @XmlElement(name = "EquipmentGroupGid")
    protected GLogXMLGidType equipmentGroupGid;
    @XmlElement(name = "EquipmentTypeGid")
    protected GLogXMLGidType equipmentTypeGid;
    @XmlElement(name = "EquipmentGid")
    protected GLogXMLGidType equipmentGid;
    @XmlElement(name = "EquipmentInitial")
    protected String equipmentInitial;
    @XmlElement(name = "EquipmentNumber")
    protected String equipmentNumber;
    @XmlElement(name = "EquipmentInitialNumber")
    protected String equipmentInitialNumber;
    @XmlElement(name = "CheckDigit")
    protected String checkDigit;
    @XmlElement(name = "LicensePlate")
    protected String licensePlate;

    /**
     * Obtém o valor da propriedade sEquipmentGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getSEquipmentGid() {
        return sEquipmentGid;
    }

    /**
     * Define o valor da propriedade sEquipmentGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setSEquipmentGid(GLogXMLGidType value) {
        this.sEquipmentGid = value;
    }

    /**
     * Obtém o valor da propriedade sEquipmentIndex.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSEquipmentIndex() {
        return sEquipmentIndex;
    }

    /**
     * Define o valor da propriedade sEquipmentIndex.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSEquipmentIndex(String value) {
        this.sEquipmentIndex = value;
    }

    /**
     * Obtém o valor da propriedade sEquipGidMatchOption.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSEquipGidMatchOption() {
        return sEquipGidMatchOption;
    }

    /**
     * Define o valor da propriedade sEquipGidMatchOption.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSEquipGidMatchOption(String value) {
        this.sEquipGidMatchOption = value;
    }

    /**
     * Obtém o valor da propriedade intSavedQuery.
     * 
     * @return
     *     possible object is
     *     {@link IntSavedQueryType }
     *     
     */
    public IntSavedQueryType getIntSavedQuery() {
        return intSavedQuery;
    }

    /**
     * Define o valor da propriedade intSavedQuery.
     * 
     * @param value
     *     allowed object is
     *     {@link IntSavedQueryType }
     *     
     */
    public void setIntSavedQuery(IntSavedQueryType value) {
        this.intSavedQuery = value;
    }

    /**
     * Obtém o valor da propriedade equipmentGroupGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getEquipmentGroupGid() {
        return equipmentGroupGid;
    }

    /**
     * Define o valor da propriedade equipmentGroupGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setEquipmentGroupGid(GLogXMLGidType value) {
        this.equipmentGroupGid = value;
    }

    /**
     * Obtém o valor da propriedade equipmentTypeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getEquipmentTypeGid() {
        return equipmentTypeGid;
    }

    /**
     * Define o valor da propriedade equipmentTypeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setEquipmentTypeGid(GLogXMLGidType value) {
        this.equipmentTypeGid = value;
    }

    /**
     * Obtém o valor da propriedade equipmentGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getEquipmentGid() {
        return equipmentGid;
    }

    /**
     * Define o valor da propriedade equipmentGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setEquipmentGid(GLogXMLGidType value) {
        this.equipmentGid = value;
    }

    /**
     * Obtém o valor da propriedade equipmentInitial.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEquipmentInitial() {
        return equipmentInitial;
    }

    /**
     * Define o valor da propriedade equipmentInitial.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEquipmentInitial(String value) {
        this.equipmentInitial = value;
    }

    /**
     * Obtém o valor da propriedade equipmentNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEquipmentNumber() {
        return equipmentNumber;
    }

    /**
     * Define o valor da propriedade equipmentNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEquipmentNumber(String value) {
        this.equipmentNumber = value;
    }

    /**
     * Obtém o valor da propriedade equipmentInitialNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEquipmentInitialNumber() {
        return equipmentInitialNumber;
    }

    /**
     * Define o valor da propriedade equipmentInitialNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEquipmentInitialNumber(String value) {
        this.equipmentInitialNumber = value;
    }

    /**
     * Obtém o valor da propriedade checkDigit.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCheckDigit() {
        return checkDigit;
    }

    /**
     * Define o valor da propriedade checkDigit.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCheckDigit(String value) {
        this.checkDigit = value;
    }

    /**
     * Obtém o valor da propriedade licensePlate.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLicensePlate() {
        return licensePlate;
    }

    /**
     * Define o valor da propriedade licensePlate.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLicensePlate(String value) {
        this.licensePlate = value;
    }

}
