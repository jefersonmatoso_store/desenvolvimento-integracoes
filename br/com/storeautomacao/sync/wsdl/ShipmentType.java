
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * Shipment represents a planned or actual movement of one or more ShipUnits.
 *             The path taken by a shipment is represented by 2 or more ShipmentStops.
 * 
 *             The RATE_OFFERING, RATE_GEO, and FLIGHT_INSTANCE elements are for outbound only, and are used
 *             to indicate the rate_offering, rate_geo, and flight_instance data that was used for the shipment.
 *             The elements are 'database centric'. The schema was autogenerated by Oracle, and the element
 *             names directly correspond to the table and column names. Refer to the database schema for
 *             additional element information.
 * 
 *             The ShipUnitViewByReleaseLine, ShipUnitViewByRelease, ShipUnitSpec, TransOrder, ShipmentStatus,
 *             SecondaryCharges, and PrimaryShipmentRefInfo elements are for outbound only.
 * 
 *             The SShipUnit element is used on the inbound only. It is an alternative to using the ShipUnit
 *             element and provides additional control features for managing the ship unit in the shipment.
 * 
 *             The RATE_OFFERING, RATE_GEO, TransOrder, ShipmentStatus, FLIGHT_INSTANCE, and SecondaryCharges elements
 *             are optional and by default are not sent in the outbound Shipment XML. Prior to Release 4.0, they could
 *             be enabled via system properties. As of Release 4.0, the properties are no longer supported. The logic
 *             to determine if the elements are to be included on the outbound is defined by the Out XML Profile configured
 *             and assigned to the External System. Refer to External System Manager in the UI for additional details.
 * 
 *             The query for generating the PackagedItem element for the outbound Shipment is configurable via properties for performance.
 *             The Shipment.PackagedItem can contain the details for the PackagedItem for the Release(s) and TransOrder(s).
 *             The following property controls whether the query is run for the PackagedItem on the Release:
 *             glog.integration.Shipment.PackagedItem.includeFromRelease = true
 *             And the following properties control whether the query is run for the TransOrder (whether Line or ShipUnit):
 *             glog.integration.Shipment.PackagedItem.includeFromTransOrderLine = false
 *             glog.integration.Shipment.PackagedItem.includeFromTransOrderShipUnit = false
 *             To enable for either, set the value to true. Also, if an outbound profile is configured which eliminates either the Release or the TransOrder
 *             from the Shipment, then the portion of the query will not be run for that element.
 *          
 * 
 * <p>Classe Java de ShipmentType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="ShipmentType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ShipmentHeader" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ShipmentHeaderType"/>
 *         &lt;element name="ShipmentHeader2" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ShipmentHeader2Type" minOccurs="0"/>
 *         &lt;element name="SEquipment" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}SEquipmentType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="ContainerGroup" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ContainerGroupType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="ShipmentStop" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ShipmentStopType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Location" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LocationType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;choice minOccurs="0">
 *           &lt;element name="ShipUnit" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ShipUnitType" maxOccurs="unbounded" minOccurs="0"/>
 *           &lt;element name="SShipUnit" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}SShipUnitType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;/choice>
 *         &lt;element name="ShipUnitViewInfo" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ShipUnitViewInfoType" minOccurs="0"/>
 *         &lt;element name="Release" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ReleaseType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="ShipmentOrderRelease" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ShipmentOrderReleaseType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="PackagedItem" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}PackagedItemType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="ShipUnitSpec" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ShipUnitSpecType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="RATE_OFFERING" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RateOfferingType" minOccurs="0"/>
 *         &lt;element name="RATE_GEO" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RateGeoType" minOccurs="0"/>
 *         &lt;element name="TransOrder" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransOrderType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="ShipmentStatus" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ShipmentStatusType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="FLIGHT_INSTANCE" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlightInstanceType" minOccurs="0"/>
 *         &lt;element name="SecondaryCharges" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}SecondaryChargesType" minOccurs="0"/>
 *         &lt;element name="PrimaryShipmentRefInfo" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}PrimaryShipmentRefInfoTYpe" minOccurs="0"/>
 *         &lt;element name="Driver" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}DriverType" minOccurs="0"/>
 *         &lt;element name="PowerUnit" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}PowerUnitType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ShipmentType", propOrder = {
    "shipmentHeader",
    "shipmentHeader2",
    "sEquipment",
    "containerGroup",
    "shipmentStop",
    "location",
    "shipUnit",
    "sShipUnit",
    "shipUnitViewInfo",
    "release",
    "shipmentOrderRelease",
    "packagedItem",
    "shipUnitSpec",
    "rateoffering",
    "rategeo",
    "transOrder",
    "shipmentStatus",
    "flightinstance",
    "secondaryCharges",
    "primaryShipmentRefInfo",
    "driver",
    "powerUnit"
})
public class ShipmentType {

    @XmlElement(name = "ShipmentHeader", required = true)
    protected ShipmentHeaderType shipmentHeader;
    @XmlElement(name = "ShipmentHeader2")
    protected ShipmentHeader2Type shipmentHeader2;
    @XmlElement(name = "SEquipment")
    protected List<SEquipmentType> sEquipment;
    @XmlElement(name = "ContainerGroup")
    protected List<ContainerGroupType> containerGroup;
    @XmlElement(name = "ShipmentStop")
    protected List<ShipmentStopType> shipmentStop;
    @XmlElement(name = "Location")
    protected List<LocationType> location;
    @XmlElement(name = "ShipUnit")
    protected List<ShipUnitType> shipUnit;
    @XmlElement(name = "SShipUnit")
    protected List<SShipUnitType> sShipUnit;
    @XmlElement(name = "ShipUnitViewInfo")
    protected ShipUnitViewInfoType shipUnitViewInfo;
    @XmlElement(name = "Release")
    protected List<ReleaseType> release;
    @XmlElement(name = "ShipmentOrderRelease")
    protected List<ShipmentOrderReleaseType> shipmentOrderRelease;
    @XmlElement(name = "PackagedItem")
    protected List<PackagedItemType> packagedItem;
    @XmlElement(name = "ShipUnitSpec")
    protected List<ShipUnitSpecType> shipUnitSpec;
    @XmlElement(name = "RATE_OFFERING")
    protected RateOfferingType rateoffering;
    @XmlElement(name = "RATE_GEO")
    protected RateGeoType rategeo;
    @XmlElement(name = "TransOrder")
    protected List<TransOrderType> transOrder;
    @XmlElement(name = "ShipmentStatus")
    protected List<ShipmentStatusType> shipmentStatus;
    @XmlElement(name = "FLIGHT_INSTANCE")
    protected FlightInstanceType flightinstance;
    @XmlElement(name = "SecondaryCharges")
    protected SecondaryChargesType secondaryCharges;
    @XmlElement(name = "PrimaryShipmentRefInfo")
    protected PrimaryShipmentRefInfoTYpe primaryShipmentRefInfo;
    @XmlElement(name = "Driver")
    protected DriverType driver;
    @XmlElement(name = "PowerUnit")
    protected PowerUnitType powerUnit;

    /**
     * Obtém o valor da propriedade shipmentHeader.
     * 
     * @return
     *     possible object is
     *     {@link ShipmentHeaderType }
     *     
     */
    public ShipmentHeaderType getShipmentHeader() {
        return shipmentHeader;
    }

    /**
     * Define o valor da propriedade shipmentHeader.
     * 
     * @param value
     *     allowed object is
     *     {@link ShipmentHeaderType }
     *     
     */
    public void setShipmentHeader(ShipmentHeaderType value) {
        this.shipmentHeader = value;
    }

    /**
     * Obtém o valor da propriedade shipmentHeader2.
     * 
     * @return
     *     possible object is
     *     {@link ShipmentHeader2Type }
     *     
     */
    public ShipmentHeader2Type getShipmentHeader2() {
        return shipmentHeader2;
    }

    /**
     * Define o valor da propriedade shipmentHeader2.
     * 
     * @param value
     *     allowed object is
     *     {@link ShipmentHeader2Type }
     *     
     */
    public void setShipmentHeader2(ShipmentHeader2Type value) {
        this.shipmentHeader2 = value;
    }

    /**
     * Gets the value of the sEquipment property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the sEquipment property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSEquipment().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SEquipmentType }
     * 
     * 
     */
    public List<SEquipmentType> getSEquipment() {
        if (sEquipment == null) {
            sEquipment = new ArrayList<SEquipmentType>();
        }
        return this.sEquipment;
    }

    /**
     * Gets the value of the containerGroup property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the containerGroup property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getContainerGroup().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ContainerGroupType }
     * 
     * 
     */
    public List<ContainerGroupType> getContainerGroup() {
        if (containerGroup == null) {
            containerGroup = new ArrayList<ContainerGroupType>();
        }
        return this.containerGroup;
    }

    /**
     * Gets the value of the shipmentStop property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the shipmentStop property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getShipmentStop().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ShipmentStopType }
     * 
     * 
     */
    public List<ShipmentStopType> getShipmentStop() {
        if (shipmentStop == null) {
            shipmentStop = new ArrayList<ShipmentStopType>();
        }
        return this.shipmentStop;
    }

    /**
     * Gets the value of the location property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the location property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLocation().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LocationType }
     * 
     * 
     */
    public List<LocationType> getLocation() {
        if (location == null) {
            location = new ArrayList<LocationType>();
        }
        return this.location;
    }

    /**
     * Gets the value of the shipUnit property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the shipUnit property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getShipUnit().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ShipUnitType }
     * 
     * 
     */
    public List<ShipUnitType> getShipUnit() {
        if (shipUnit == null) {
            shipUnit = new ArrayList<ShipUnitType>();
        }
        return this.shipUnit;
    }

    /**
     * Gets the value of the sShipUnit property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the sShipUnit property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSShipUnit().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SShipUnitType }
     * 
     * 
     */
    public List<SShipUnitType> getSShipUnit() {
        if (sShipUnit == null) {
            sShipUnit = new ArrayList<SShipUnitType>();
        }
        return this.sShipUnit;
    }

    /**
     * Obtém o valor da propriedade shipUnitViewInfo.
     * 
     * @return
     *     possible object is
     *     {@link ShipUnitViewInfoType }
     *     
     */
    public ShipUnitViewInfoType getShipUnitViewInfo() {
        return shipUnitViewInfo;
    }

    /**
     * Define o valor da propriedade shipUnitViewInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link ShipUnitViewInfoType }
     *     
     */
    public void setShipUnitViewInfo(ShipUnitViewInfoType value) {
        this.shipUnitViewInfo = value;
    }

    /**
     * Gets the value of the release property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the release property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRelease().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ReleaseType }
     * 
     * 
     */
    public List<ReleaseType> getRelease() {
        if (release == null) {
            release = new ArrayList<ReleaseType>();
        }
        return this.release;
    }

    /**
     * Gets the value of the shipmentOrderRelease property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the shipmentOrderRelease property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getShipmentOrderRelease().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ShipmentOrderReleaseType }
     * 
     * 
     */
    public List<ShipmentOrderReleaseType> getShipmentOrderRelease() {
        if (shipmentOrderRelease == null) {
            shipmentOrderRelease = new ArrayList<ShipmentOrderReleaseType>();
        }
        return this.shipmentOrderRelease;
    }

    /**
     * Gets the value of the packagedItem property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the packagedItem property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPackagedItem().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PackagedItemType }
     * 
     * 
     */
    public List<PackagedItemType> getPackagedItem() {
        if (packagedItem == null) {
            packagedItem = new ArrayList<PackagedItemType>();
        }
        return this.packagedItem;
    }

    /**
     * Gets the value of the shipUnitSpec property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the shipUnitSpec property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getShipUnitSpec().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ShipUnitSpecType }
     * 
     * 
     */
    public List<ShipUnitSpecType> getShipUnitSpec() {
        if (shipUnitSpec == null) {
            shipUnitSpec = new ArrayList<ShipUnitSpecType>();
        }
        return this.shipUnitSpec;
    }

    /**
     * Obtém o valor da propriedade rateoffering.
     * 
     * @return
     *     possible object is
     *     {@link RateOfferingType }
     *     
     */
    public RateOfferingType getRATEOFFERING() {
        return rateoffering;
    }

    /**
     * Define o valor da propriedade rateoffering.
     * 
     * @param value
     *     allowed object is
     *     {@link RateOfferingType }
     *     
     */
    public void setRATEOFFERING(RateOfferingType value) {
        this.rateoffering = value;
    }

    /**
     * Obtém o valor da propriedade rategeo.
     * 
     * @return
     *     possible object is
     *     {@link RateGeoType }
     *     
     */
    public RateGeoType getRATEGEO() {
        return rategeo;
    }

    /**
     * Define o valor da propriedade rategeo.
     * 
     * @param value
     *     allowed object is
     *     {@link RateGeoType }
     *     
     */
    public void setRATEGEO(RateGeoType value) {
        this.rategeo = value;
    }

    /**
     * Gets the value of the transOrder property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the transOrder property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTransOrder().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TransOrderType }
     * 
     * 
     */
    public List<TransOrderType> getTransOrder() {
        if (transOrder == null) {
            transOrder = new ArrayList<TransOrderType>();
        }
        return this.transOrder;
    }

    /**
     * Gets the value of the shipmentStatus property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the shipmentStatus property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getShipmentStatus().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ShipmentStatusType }
     * 
     * 
     */
    public List<ShipmentStatusType> getShipmentStatus() {
        if (shipmentStatus == null) {
            shipmentStatus = new ArrayList<ShipmentStatusType>();
        }
        return this.shipmentStatus;
    }

    /**
     * Obtém o valor da propriedade flightinstance.
     * 
     * @return
     *     possible object is
     *     {@link FlightInstanceType }
     *     
     */
    public FlightInstanceType getFLIGHTINSTANCE() {
        return flightinstance;
    }

    /**
     * Define o valor da propriedade flightinstance.
     * 
     * @param value
     *     allowed object is
     *     {@link FlightInstanceType }
     *     
     */
    public void setFLIGHTINSTANCE(FlightInstanceType value) {
        this.flightinstance = value;
    }

    /**
     * Obtém o valor da propriedade secondaryCharges.
     * 
     * @return
     *     possible object is
     *     {@link SecondaryChargesType }
     *     
     */
    public SecondaryChargesType getSecondaryCharges() {
        return secondaryCharges;
    }

    /**
     * Define o valor da propriedade secondaryCharges.
     * 
     * @param value
     *     allowed object is
     *     {@link SecondaryChargesType }
     *     
     */
    public void setSecondaryCharges(SecondaryChargesType value) {
        this.secondaryCharges = value;
    }

    /**
     * Obtém o valor da propriedade primaryShipmentRefInfo.
     * 
     * @return
     *     possible object is
     *     {@link PrimaryShipmentRefInfoTYpe }
     *     
     */
    public PrimaryShipmentRefInfoTYpe getPrimaryShipmentRefInfo() {
        return primaryShipmentRefInfo;
    }

    /**
     * Define o valor da propriedade primaryShipmentRefInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link PrimaryShipmentRefInfoTYpe }
     *     
     */
    public void setPrimaryShipmentRefInfo(PrimaryShipmentRefInfoTYpe value) {
        this.primaryShipmentRefInfo = value;
    }

    /**
     * Obtém o valor da propriedade driver.
     * 
     * @return
     *     possible object is
     *     {@link DriverType }
     *     
     */
    public DriverType getDriver() {
        return driver;
    }

    /**
     * Define o valor da propriedade driver.
     * 
     * @param value
     *     allowed object is
     *     {@link DriverType }
     *     
     */
    public void setDriver(DriverType value) {
        this.driver = value;
    }

    /**
     * Obtém o valor da propriedade powerUnit.
     * 
     * @return
     *     possible object is
     *     {@link PowerUnitType }
     *     
     */
    public PowerUnitType getPowerUnit() {
        return powerUnit;
    }

    /**
     * Define o valor da propriedade powerUnit.
     * 
     * @param value
     *     allowed object is
     *     {@link PowerUnitType }
     *     
     */
    public void setPowerUnit(PowerUnitType value) {
        this.powerUnit = value;
    }

}
