
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * 
 *             When the AllocationBase is for a Voucher with a Parent Invoice, all of the AllocationBase for the Child Invoices
 *             will be contained in the ChildAllocationBases element. The AllocatedCost in the parent AllocationBase will correspond
 *             to the Voucher Cost.
 *          
 * 
 * <p>Classe Java de AllocationBaseType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="AllocationBaseType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OTMTransactionOut">
 *       &lt;sequence>
 *         &lt;element name="SendReason" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}SendReasonType" minOccurs="0"/>
 *         &lt;element name="AllocSeqNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AllocTypeQualGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="AllocDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType"/>
 *         &lt;element name="ExchangeRateInfo" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ExchangeRateInfoType" minOccurs="0"/>
 *         &lt;element name="AllocatedCost" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/>
 *         &lt;choice minOccurs="0">
 *           &lt;element name="Shipment" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ShipmentType" minOccurs="0"/>
 *           &lt;element name="ShipmentGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;/choice>
 *         &lt;element name="Invoice" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}InvoiceType" minOccurs="0"/>
 *         &lt;element name="ParentInvoice" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ParentInvoiceType" minOccurs="0"/>
 *         &lt;choice minOccurs="0">
 *           &lt;element name="VoucherGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *           &lt;element name="Voucher" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}VoucherType" minOccurs="0"/>
 *         &lt;/choice>
 *         &lt;element name="AllocReleaseDetail" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}AllocReleaseDetailType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="AllocReleaseLineDetail" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}AllocReleaseLineDetailType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="ChildAllocationBases" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="AllocationBase" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}AllocationBaseType" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AllocationBaseType", propOrder = {
    "sendReason",
    "allocSeqNo",
    "allocTypeQualGid",
    "allocDate",
    "exchangeRateInfo",
    "allocatedCost",
    "shipment",
    "shipmentGid",
    "invoice",
    "parentInvoice",
    "voucherGid",
    "voucher",
    "allocReleaseDetail",
    "allocReleaseLineDetail",
    "childAllocationBases"
})
public class AllocationBaseType
    extends OTMTransactionOut
{

    @XmlElement(name = "SendReason")
    protected SendReasonType sendReason;
    @XmlElement(name = "AllocSeqNo", required = true)
    protected String allocSeqNo;
    @XmlElement(name = "AllocTypeQualGid", required = true)
    protected GLogXMLGidType allocTypeQualGid;
    @XmlElement(name = "AllocDate", required = true)
    protected GLogDateTimeType allocDate;
    @XmlElement(name = "ExchangeRateInfo")
    protected ExchangeRateInfoType exchangeRateInfo;
    @XmlElement(name = "AllocatedCost")
    protected GLogXMLFinancialAmountType allocatedCost;
    @XmlElement(name = "Shipment")
    protected ShipmentType shipment;
    @XmlElement(name = "ShipmentGid")
    protected GLogXMLGidType shipmentGid;
    @XmlElement(name = "Invoice")
    protected InvoiceType invoice;
    @XmlElement(name = "ParentInvoice")
    protected ParentInvoiceType parentInvoice;
    @XmlElement(name = "VoucherGid")
    protected GLogXMLGidType voucherGid;
    @XmlElement(name = "Voucher")
    protected VoucherType voucher;
    @XmlElement(name = "AllocReleaseDetail")
    protected List<AllocReleaseDetailType> allocReleaseDetail;
    @XmlElement(name = "AllocReleaseLineDetail")
    protected List<AllocReleaseLineDetailType> allocReleaseLineDetail;
    @XmlElement(name = "ChildAllocationBases")
    protected AllocationBaseType.ChildAllocationBases childAllocationBases;

    /**
     * Obtém o valor da propriedade sendReason.
     * 
     * @return
     *     possible object is
     *     {@link SendReasonType }
     *     
     */
    public SendReasonType getSendReason() {
        return sendReason;
    }

    /**
     * Define o valor da propriedade sendReason.
     * 
     * @param value
     *     allowed object is
     *     {@link SendReasonType }
     *     
     */
    public void setSendReason(SendReasonType value) {
        this.sendReason = value;
    }

    /**
     * Obtém o valor da propriedade allocSeqNo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAllocSeqNo() {
        return allocSeqNo;
    }

    /**
     * Define o valor da propriedade allocSeqNo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAllocSeqNo(String value) {
        this.allocSeqNo = value;
    }

    /**
     * Obtém o valor da propriedade allocTypeQualGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getAllocTypeQualGid() {
        return allocTypeQualGid;
    }

    /**
     * Define o valor da propriedade allocTypeQualGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setAllocTypeQualGid(GLogXMLGidType value) {
        this.allocTypeQualGid = value;
    }

    /**
     * Obtém o valor da propriedade allocDate.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getAllocDate() {
        return allocDate;
    }

    /**
     * Define o valor da propriedade allocDate.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setAllocDate(GLogDateTimeType value) {
        this.allocDate = value;
    }

    /**
     * Obtém o valor da propriedade exchangeRateInfo.
     * 
     * @return
     *     possible object is
     *     {@link ExchangeRateInfoType }
     *     
     */
    public ExchangeRateInfoType getExchangeRateInfo() {
        return exchangeRateInfo;
    }

    /**
     * Define o valor da propriedade exchangeRateInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link ExchangeRateInfoType }
     *     
     */
    public void setExchangeRateInfo(ExchangeRateInfoType value) {
        this.exchangeRateInfo = value;
    }

    /**
     * Obtém o valor da propriedade allocatedCost.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getAllocatedCost() {
        return allocatedCost;
    }

    /**
     * Define o valor da propriedade allocatedCost.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setAllocatedCost(GLogXMLFinancialAmountType value) {
        this.allocatedCost = value;
    }

    /**
     * Obtém o valor da propriedade shipment.
     * 
     * @return
     *     possible object is
     *     {@link ShipmentType }
     *     
     */
    public ShipmentType getShipment() {
        return shipment;
    }

    /**
     * Define o valor da propriedade shipment.
     * 
     * @param value
     *     allowed object is
     *     {@link ShipmentType }
     *     
     */
    public void setShipment(ShipmentType value) {
        this.shipment = value;
    }

    /**
     * Obtém o valor da propriedade shipmentGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getShipmentGid() {
        return shipmentGid;
    }

    /**
     * Define o valor da propriedade shipmentGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setShipmentGid(GLogXMLGidType value) {
        this.shipmentGid = value;
    }

    /**
     * Obtém o valor da propriedade invoice.
     * 
     * @return
     *     possible object is
     *     {@link InvoiceType }
     *     
     */
    public InvoiceType getInvoice() {
        return invoice;
    }

    /**
     * Define o valor da propriedade invoice.
     * 
     * @param value
     *     allowed object is
     *     {@link InvoiceType }
     *     
     */
    public void setInvoice(InvoiceType value) {
        this.invoice = value;
    }

    /**
     * Obtém o valor da propriedade parentInvoice.
     * 
     * @return
     *     possible object is
     *     {@link ParentInvoiceType }
     *     
     */
    public ParentInvoiceType getParentInvoice() {
        return parentInvoice;
    }

    /**
     * Define o valor da propriedade parentInvoice.
     * 
     * @param value
     *     allowed object is
     *     {@link ParentInvoiceType }
     *     
     */
    public void setParentInvoice(ParentInvoiceType value) {
        this.parentInvoice = value;
    }

    /**
     * Obtém o valor da propriedade voucherGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getVoucherGid() {
        return voucherGid;
    }

    /**
     * Define o valor da propriedade voucherGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setVoucherGid(GLogXMLGidType value) {
        this.voucherGid = value;
    }

    /**
     * Obtém o valor da propriedade voucher.
     * 
     * @return
     *     possible object is
     *     {@link VoucherType }
     *     
     */
    public VoucherType getVoucher() {
        return voucher;
    }

    /**
     * Define o valor da propriedade voucher.
     * 
     * @param value
     *     allowed object is
     *     {@link VoucherType }
     *     
     */
    public void setVoucher(VoucherType value) {
        this.voucher = value;
    }

    /**
     * Gets the value of the allocReleaseDetail property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the allocReleaseDetail property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAllocReleaseDetail().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AllocReleaseDetailType }
     * 
     * 
     */
    public List<AllocReleaseDetailType> getAllocReleaseDetail() {
        if (allocReleaseDetail == null) {
            allocReleaseDetail = new ArrayList<AllocReleaseDetailType>();
        }
        return this.allocReleaseDetail;
    }

    /**
     * Gets the value of the allocReleaseLineDetail property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the allocReleaseLineDetail property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAllocReleaseLineDetail().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AllocReleaseLineDetailType }
     * 
     * 
     */
    public List<AllocReleaseLineDetailType> getAllocReleaseLineDetail() {
        if (allocReleaseLineDetail == null) {
            allocReleaseLineDetail = new ArrayList<AllocReleaseLineDetailType>();
        }
        return this.allocReleaseLineDetail;
    }

    /**
     * Obtém o valor da propriedade childAllocationBases.
     * 
     * @return
     *     possible object is
     *     {@link AllocationBaseType.ChildAllocationBases }
     *     
     */
    public AllocationBaseType.ChildAllocationBases getChildAllocationBases() {
        return childAllocationBases;
    }

    /**
     * Define o valor da propriedade childAllocationBases.
     * 
     * @param value
     *     allowed object is
     *     {@link AllocationBaseType.ChildAllocationBases }
     *     
     */
    public void setChildAllocationBases(AllocationBaseType.ChildAllocationBases value) {
        this.childAllocationBases = value;
    }


    /**
     * <p>Classe Java de anonymous complex type.
     * 
     * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="AllocationBase" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}AllocationBaseType" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "allocationBase"
    })
    public static class ChildAllocationBases {

        @XmlElement(name = "AllocationBase")
        protected List<AllocationBaseType> allocationBase;

        /**
         * Gets the value of the allocationBase property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the allocationBase property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getAllocationBase().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link AllocationBaseType }
         * 
         * 
         */
        public List<AllocationBaseType> getAllocationBase() {
            if (allocationBase == null) {
                allocationBase = new ArrayList<AllocationBaseType>();
            }
            return this.allocationBase;
        }

    }

}
