
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Demurrage Transaction Event. These includes all the events (notify, outgate, ingate,
 *             released, picked etc.) between start and end of a demurrage transaction that match based on
 *             asset, location and time window (Mandatory).
 *          
 * 
 * <p>Classe Java de DemurrageTransactionEventType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="DemurrageTransactionEventType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="EventStatusCodeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="DmEventDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DemurrageTransactionEventType", propOrder = {
    "eventStatusCodeGid",
    "dmEventDate"
})
public class DemurrageTransactionEventType {

    @XmlElement(name = "EventStatusCodeGid")
    protected GLogXMLGidType eventStatusCodeGid;
    @XmlElement(name = "DmEventDate")
    protected GLogDateTimeType dmEventDate;

    /**
     * Obtém o valor da propriedade eventStatusCodeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getEventStatusCodeGid() {
        return eventStatusCodeGid;
    }

    /**
     * Define o valor da propriedade eventStatusCodeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setEventStatusCodeGid(GLogXMLGidType value) {
        this.eventStatusCodeGid = value;
    }

    /**
     * Obtém o valor da propriedade dmEventDate.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getDmEventDate() {
        return dmEventDate;
    }

    /**
     * Define o valor da propriedade dmEventDate.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setDmEventDate(GLogDateTimeType value) {
        this.dmEventDate = value;
    }

}
