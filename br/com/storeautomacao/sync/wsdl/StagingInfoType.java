
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de StagingInfoType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="StagingInfoType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="StagingQuery" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLIntSavedQueryType" minOccurs="0"/>
 *         &lt;element name="StagingProcess" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLIntSavedQueryType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "StagingInfoType", propOrder = {
    "stagingQuery",
    "stagingProcess"
})
public class StagingInfoType {

    @XmlElement(name = "StagingQuery")
    protected GLogXMLIntSavedQueryType stagingQuery;
    @XmlElement(name = "StagingProcess")
    protected GLogXMLIntSavedQueryType stagingProcess;

    /**
     * Obtém o valor da propriedade stagingQuery.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLIntSavedQueryType }
     *     
     */
    public GLogXMLIntSavedQueryType getStagingQuery() {
        return stagingQuery;
    }

    /**
     * Define o valor da propriedade stagingQuery.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLIntSavedQueryType }
     *     
     */
    public void setStagingQuery(GLogXMLIntSavedQueryType value) {
        this.stagingQuery = value;
    }

    /**
     * Obtém o valor da propriedade stagingProcess.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLIntSavedQueryType }
     *     
     */
    public GLogXMLIntSavedQueryType getStagingProcess() {
        return stagingProcess;
    }

    /**
     * Define o valor da propriedade stagingProcess.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLIntSavedQueryType }
     *     
     */
    public void setStagingProcess(GLogXMLIntSavedQueryType value) {
        this.stagingProcess = value;
    }

}
