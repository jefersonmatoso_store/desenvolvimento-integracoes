
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Specifies the profit split for the agent role.
 * 
 * <p>Classe Java de ShippingAgentContactProfitType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="ShippingAgentContactProfitType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ShippingAgentContactProfitGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="ModeProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="JobType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ShipperLocationProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocProfileType" minOccurs="0"/>
 *         &lt;element name="ConsigneeLocationProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocProfileType" minOccurs="0"/>
 *         &lt;element name="ProfitSplitPct" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ProfitSplitAmount" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/>
 *         &lt;element name="ConsolidationTypeProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocProfileType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ShippingAgentContactProfitType", propOrder = {
    "shippingAgentContactProfitGid",
    "modeProfileGid",
    "jobType",
    "shipperLocationProfileGid",
    "consigneeLocationProfileGid",
    "profitSplitPct",
    "profitSplitAmount",
    "consolidationTypeProfileGid"
})
public class ShippingAgentContactProfitType {

    @XmlElement(name = "ShippingAgentContactProfitGid", required = true)
    protected GLogXMLGidType shippingAgentContactProfitGid;
    @XmlElement(name = "ModeProfileGid")
    protected GLogXMLGidType modeProfileGid;
    @XmlElement(name = "JobType")
    protected String jobType;
    @XmlElement(name = "ShipperLocationProfileGid")
    protected GLogXMLLocProfileType shipperLocationProfileGid;
    @XmlElement(name = "ConsigneeLocationProfileGid")
    protected GLogXMLLocProfileType consigneeLocationProfileGid;
    @XmlElement(name = "ProfitSplitPct")
    protected String profitSplitPct;
    @XmlElement(name = "ProfitSplitAmount")
    protected GLogXMLFinancialAmountType profitSplitAmount;
    @XmlElement(name = "ConsolidationTypeProfileGid")
    protected GLogXMLLocProfileType consolidationTypeProfileGid;

    /**
     * Obtém o valor da propriedade shippingAgentContactProfitGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getShippingAgentContactProfitGid() {
        return shippingAgentContactProfitGid;
    }

    /**
     * Define o valor da propriedade shippingAgentContactProfitGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setShippingAgentContactProfitGid(GLogXMLGidType value) {
        this.shippingAgentContactProfitGid = value;
    }

    /**
     * Obtém o valor da propriedade modeProfileGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getModeProfileGid() {
        return modeProfileGid;
    }

    /**
     * Define o valor da propriedade modeProfileGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setModeProfileGid(GLogXMLGidType value) {
        this.modeProfileGid = value;
    }

    /**
     * Obtém o valor da propriedade jobType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJobType() {
        return jobType;
    }

    /**
     * Define o valor da propriedade jobType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJobType(String value) {
        this.jobType = value;
    }

    /**
     * Obtém o valor da propriedade shipperLocationProfileGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocProfileType }
     *     
     */
    public GLogXMLLocProfileType getShipperLocationProfileGid() {
        return shipperLocationProfileGid;
    }

    /**
     * Define o valor da propriedade shipperLocationProfileGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocProfileType }
     *     
     */
    public void setShipperLocationProfileGid(GLogXMLLocProfileType value) {
        this.shipperLocationProfileGid = value;
    }

    /**
     * Obtém o valor da propriedade consigneeLocationProfileGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocProfileType }
     *     
     */
    public GLogXMLLocProfileType getConsigneeLocationProfileGid() {
        return consigneeLocationProfileGid;
    }

    /**
     * Define o valor da propriedade consigneeLocationProfileGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocProfileType }
     *     
     */
    public void setConsigneeLocationProfileGid(GLogXMLLocProfileType value) {
        this.consigneeLocationProfileGid = value;
    }

    /**
     * Obtém o valor da propriedade profitSplitPct.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProfitSplitPct() {
        return profitSplitPct;
    }

    /**
     * Define o valor da propriedade profitSplitPct.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProfitSplitPct(String value) {
        this.profitSplitPct = value;
    }

    /**
     * Obtém o valor da propriedade profitSplitAmount.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getProfitSplitAmount() {
        return profitSplitAmount;
    }

    /**
     * Define o valor da propriedade profitSplitAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setProfitSplitAmount(GLogXMLFinancialAmountType value) {
        this.profitSplitAmount = value;
    }

    /**
     * Obtém o valor da propriedade consolidationTypeProfileGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocProfileType }
     *     
     */
    public GLogXMLLocProfileType getConsolidationTypeProfileGid() {
        return consolidationTypeProfileGid;
    }

    /**
     * Define o valor da propriedade consolidationTypeProfileGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocProfileType }
     *     
     */
    public void setConsolidationTypeProfileGid(GLogXMLLocProfileType value) {
        this.consolidationTypeProfileGid = value;
    }

}
