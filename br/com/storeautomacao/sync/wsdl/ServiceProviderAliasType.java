
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * ServiceProviderAlias is an alternate identifier for a service provider.
 * 
 * <p>Classe Java de ServiceProviderAliasType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="ServiceProviderAliasType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ServiceProviderAliasQualifierGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="ServiceProviderAliasValue" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceProviderAliasType", propOrder = {
    "serviceProviderAliasQualifierGid",
    "serviceProviderAliasValue"
})
public class ServiceProviderAliasType {

    @XmlElement(name = "ServiceProviderAliasQualifierGid", required = true)
    protected GLogXMLGidType serviceProviderAliasQualifierGid;
    @XmlElement(name = "ServiceProviderAliasValue", required = true)
    protected String serviceProviderAliasValue;

    /**
     * Obtém o valor da propriedade serviceProviderAliasQualifierGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getServiceProviderAliasQualifierGid() {
        return serviceProviderAliasQualifierGid;
    }

    /**
     * Define o valor da propriedade serviceProviderAliasQualifierGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setServiceProviderAliasQualifierGid(GLogXMLGidType value) {
        this.serviceProviderAliasQualifierGid = value;
    }

    /**
     * Obtém o valor da propriedade serviceProviderAliasValue.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderAliasValue() {
        return serviceProviderAliasValue;
    }

    /**
     * Define o valor da propriedade serviceProviderAliasValue.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderAliasValue(String value) {
        this.serviceProviderAliasValue = value;
    }

}
