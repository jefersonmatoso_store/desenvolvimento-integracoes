
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * (Outbound) RuleControl represents the controls applicable for the input data based on compliance screening  
 *          
 * 
 * <p>Classe Java de RuleControlType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="RuleControlType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RuleId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ControlType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ControlCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ControlCategory" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Precedence" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DocumentDefGid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RegimeId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InvolvedPartyQualGid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ScreeningStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ExceptionControlTypeGid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ExceptionControlCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PassFail" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Text" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TextType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RuleControlType", namespace = "http://xmlns.oracle.com/apps/gtm/transmission/v6.4", propOrder = {
    "ruleId",
    "controlType",
    "controlCode",
    "controlCategory",
    "precedence",
    "documentDefGid",
    "regimeId",
    "involvedPartyQualGid",
    "screeningStatus",
    "exceptionControlTypeGid",
    "exceptionControlCode",
    "passFail",
    "text"
})
public class RuleControlType {

    @XmlElement(name = "RuleId", required = true)
    protected String ruleId;
    @XmlElement(name = "ControlType")
    protected String controlType;
    @XmlElement(name = "ControlCode")
    protected String controlCode;
    @XmlElement(name = "ControlCategory")
    protected String controlCategory;
    @XmlElement(name = "Precedence")
    protected String precedence;
    @XmlElement(name = "DocumentDefGid")
    protected String documentDefGid;
    @XmlElement(name = "RegimeId")
    protected String regimeId;
    @XmlElement(name = "InvolvedPartyQualGid")
    protected String involvedPartyQualGid;
    @XmlElement(name = "ScreeningStatus")
    protected String screeningStatus;
    @XmlElement(name = "ExceptionControlTypeGid")
    protected String exceptionControlTypeGid;
    @XmlElement(name = "ExceptionControlCode")
    protected String exceptionControlCode;
    @XmlElement(name = "PassFail")
    protected String passFail;
    @XmlElement(name = "Text")
    protected List<TextType> text;

    /**
     * Obtém o valor da propriedade ruleId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRuleId() {
        return ruleId;
    }

    /**
     * Define o valor da propriedade ruleId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRuleId(String value) {
        this.ruleId = value;
    }

    /**
     * Obtém o valor da propriedade controlType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getControlType() {
        return controlType;
    }

    /**
     * Define o valor da propriedade controlType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setControlType(String value) {
        this.controlType = value;
    }

    /**
     * Obtém o valor da propriedade controlCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getControlCode() {
        return controlCode;
    }

    /**
     * Define o valor da propriedade controlCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setControlCode(String value) {
        this.controlCode = value;
    }

    /**
     * Obtém o valor da propriedade controlCategory.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getControlCategory() {
        return controlCategory;
    }

    /**
     * Define o valor da propriedade controlCategory.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setControlCategory(String value) {
        this.controlCategory = value;
    }

    /**
     * Obtém o valor da propriedade precedence.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrecedence() {
        return precedence;
    }

    /**
     * Define o valor da propriedade precedence.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrecedence(String value) {
        this.precedence = value;
    }

    /**
     * Obtém o valor da propriedade documentDefGid.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDocumentDefGid() {
        return documentDefGid;
    }

    /**
     * Define o valor da propriedade documentDefGid.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDocumentDefGid(String value) {
        this.documentDefGid = value;
    }

    /**
     * Obtém o valor da propriedade regimeId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRegimeId() {
        return regimeId;
    }

    /**
     * Define o valor da propriedade regimeId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegimeId(String value) {
        this.regimeId = value;
    }

    /**
     * Obtém o valor da propriedade involvedPartyQualGid.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInvolvedPartyQualGid() {
        return involvedPartyQualGid;
    }

    /**
     * Define o valor da propriedade involvedPartyQualGid.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInvolvedPartyQualGid(String value) {
        this.involvedPartyQualGid = value;
    }

    /**
     * Obtém o valor da propriedade screeningStatus.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getScreeningStatus() {
        return screeningStatus;
    }

    /**
     * Define o valor da propriedade screeningStatus.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setScreeningStatus(String value) {
        this.screeningStatus = value;
    }

    /**
     * Obtém o valor da propriedade exceptionControlTypeGid.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExceptionControlTypeGid() {
        return exceptionControlTypeGid;
    }

    /**
     * Define o valor da propriedade exceptionControlTypeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExceptionControlTypeGid(String value) {
        this.exceptionControlTypeGid = value;
    }

    /**
     * Obtém o valor da propriedade exceptionControlCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExceptionControlCode() {
        return exceptionControlCode;
    }

    /**
     * Define o valor da propriedade exceptionControlCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExceptionControlCode(String value) {
        this.exceptionControlCode = value;
    }

    /**
     * Obtém o valor da propriedade passFail.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPassFail() {
        return passFail;
    }

    /**
     * Define o valor da propriedade passFail.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPassFail(String value) {
        this.passFail = value;
    }

    /**
     * Gets the value of the text property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the text property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getText().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TextType }
     * 
     * 
     */
    public List<TextType> getText() {
        if (text == null) {
            text = new ArrayList<TextType>();
        }
        return this.text;
    }

}
