
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * Specifies the total allocation for an order release on a particular shipment.
 *          
 * 
 * <p>Classe Java de ReleaseAllocShipmentType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="ReleaseAllocShipmentType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ShipmentGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="TotalAllocCost" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType"/>
 *         &lt;element name="ReleaseAllocShipmentDetail" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ReleaseAllocShipmentDetailType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReleaseAllocShipmentType", propOrder = {
    "shipmentGid",
    "totalAllocCost",
    "releaseAllocShipmentDetail"
})
public class ReleaseAllocShipmentType {

    @XmlElement(name = "ShipmentGid", required = true)
    protected GLogXMLGidType shipmentGid;
    @XmlElement(name = "TotalAllocCost", required = true)
    protected GLogXMLFinancialAmountType totalAllocCost;
    @XmlElement(name = "ReleaseAllocShipmentDetail")
    protected List<ReleaseAllocShipmentDetailType> releaseAllocShipmentDetail;

    /**
     * Obtém o valor da propriedade shipmentGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getShipmentGid() {
        return shipmentGid;
    }

    /**
     * Define o valor da propriedade shipmentGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setShipmentGid(GLogXMLGidType value) {
        this.shipmentGid = value;
    }

    /**
     * Obtém o valor da propriedade totalAllocCost.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getTotalAllocCost() {
        return totalAllocCost;
    }

    /**
     * Define o valor da propriedade totalAllocCost.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setTotalAllocCost(GLogXMLFinancialAmountType value) {
        this.totalAllocCost = value;
    }

    /**
     * Gets the value of the releaseAllocShipmentDetail property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the releaseAllocShipmentDetail property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReleaseAllocShipmentDetail().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ReleaseAllocShipmentDetailType }
     * 
     * 
     */
    public List<ReleaseAllocShipmentDetailType> getReleaseAllocShipmentDetail() {
        if (releaseAllocShipmentDetail == null) {
            releaseAllocShipmentDetail = new ArrayList<ReleaseAllocShipmentDetailType>();
        }
        return this.releaseAllocShipmentDetail;
    }

}
