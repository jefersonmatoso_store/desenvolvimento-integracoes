
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Specifies the conditional booking information for the Shipment.
 * 
 * <p>Classe Java de BookingInfoType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="BookingInfoType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ReceiptLocation" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocRefType" minOccurs="0"/>
 *         &lt;element name="DeliveryLocation" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocRefType" minOccurs="0"/>
 *         &lt;element name="SailDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="RailDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="SailCutoffDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="PortOfDischrgETA" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="PortOfExitDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="PortOfExitLocation" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocRefType" minOccurs="0"/>
 *         &lt;element name="ContainerDeliveryLocation" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocRefType" minOccurs="0"/>
 *         &lt;element name="ContainerPickupLocation" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocRefType" minOccurs="0"/>
 *         &lt;element name="CfsCutoffLocation" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocRefType" minOccurs="0"/>
 *         &lt;element name="CfsCutoffDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="SiCutoffDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BookingInfoType", propOrder = {
    "receiptLocation",
    "deliveryLocation",
    "sailDt",
    "railDt",
    "sailCutoffDt",
    "portOfDischrgETA",
    "portOfExitDt",
    "portOfExitLocation",
    "containerDeliveryLocation",
    "containerPickupLocation",
    "cfsCutoffLocation",
    "cfsCutoffDt",
    "siCutoffDt"
})
public class BookingInfoType {

    @XmlElement(name = "ReceiptLocation")
    protected GLogXMLLocRefType receiptLocation;
    @XmlElement(name = "DeliveryLocation")
    protected GLogXMLLocRefType deliveryLocation;
    @XmlElement(name = "SailDt")
    protected GLogDateTimeType sailDt;
    @XmlElement(name = "RailDt")
    protected GLogDateTimeType railDt;
    @XmlElement(name = "SailCutoffDt")
    protected GLogDateTimeType sailCutoffDt;
    @XmlElement(name = "PortOfDischrgETA")
    protected GLogDateTimeType portOfDischrgETA;
    @XmlElement(name = "PortOfExitDt")
    protected GLogDateTimeType portOfExitDt;
    @XmlElement(name = "PortOfExitLocation")
    protected GLogXMLLocRefType portOfExitLocation;
    @XmlElement(name = "ContainerDeliveryLocation")
    protected GLogXMLLocRefType containerDeliveryLocation;
    @XmlElement(name = "ContainerPickupLocation")
    protected GLogXMLLocRefType containerPickupLocation;
    @XmlElement(name = "CfsCutoffLocation")
    protected GLogXMLLocRefType cfsCutoffLocation;
    @XmlElement(name = "CfsCutoffDt")
    protected GLogDateTimeType cfsCutoffDt;
    @XmlElement(name = "SiCutoffDt")
    protected GLogDateTimeType siCutoffDt;

    /**
     * Obtém o valor da propriedade receiptLocation.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public GLogXMLLocRefType getReceiptLocation() {
        return receiptLocation;
    }

    /**
     * Define o valor da propriedade receiptLocation.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public void setReceiptLocation(GLogXMLLocRefType value) {
        this.receiptLocation = value;
    }

    /**
     * Obtém o valor da propriedade deliveryLocation.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public GLogXMLLocRefType getDeliveryLocation() {
        return deliveryLocation;
    }

    /**
     * Define o valor da propriedade deliveryLocation.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public void setDeliveryLocation(GLogXMLLocRefType value) {
        this.deliveryLocation = value;
    }

    /**
     * Obtém o valor da propriedade sailDt.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getSailDt() {
        return sailDt;
    }

    /**
     * Define o valor da propriedade sailDt.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setSailDt(GLogDateTimeType value) {
        this.sailDt = value;
    }

    /**
     * Obtém o valor da propriedade railDt.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getRailDt() {
        return railDt;
    }

    /**
     * Define o valor da propriedade railDt.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setRailDt(GLogDateTimeType value) {
        this.railDt = value;
    }

    /**
     * Obtém o valor da propriedade sailCutoffDt.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getSailCutoffDt() {
        return sailCutoffDt;
    }

    /**
     * Define o valor da propriedade sailCutoffDt.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setSailCutoffDt(GLogDateTimeType value) {
        this.sailCutoffDt = value;
    }

    /**
     * Obtém o valor da propriedade portOfDischrgETA.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getPortOfDischrgETA() {
        return portOfDischrgETA;
    }

    /**
     * Define o valor da propriedade portOfDischrgETA.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setPortOfDischrgETA(GLogDateTimeType value) {
        this.portOfDischrgETA = value;
    }

    /**
     * Obtém o valor da propriedade portOfExitDt.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getPortOfExitDt() {
        return portOfExitDt;
    }

    /**
     * Define o valor da propriedade portOfExitDt.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setPortOfExitDt(GLogDateTimeType value) {
        this.portOfExitDt = value;
    }

    /**
     * Obtém o valor da propriedade portOfExitLocation.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public GLogXMLLocRefType getPortOfExitLocation() {
        return portOfExitLocation;
    }

    /**
     * Define o valor da propriedade portOfExitLocation.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public void setPortOfExitLocation(GLogXMLLocRefType value) {
        this.portOfExitLocation = value;
    }

    /**
     * Obtém o valor da propriedade containerDeliveryLocation.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public GLogXMLLocRefType getContainerDeliveryLocation() {
        return containerDeliveryLocation;
    }

    /**
     * Define o valor da propriedade containerDeliveryLocation.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public void setContainerDeliveryLocation(GLogXMLLocRefType value) {
        this.containerDeliveryLocation = value;
    }

    /**
     * Obtém o valor da propriedade containerPickupLocation.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public GLogXMLLocRefType getContainerPickupLocation() {
        return containerPickupLocation;
    }

    /**
     * Define o valor da propriedade containerPickupLocation.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public void setContainerPickupLocation(GLogXMLLocRefType value) {
        this.containerPickupLocation = value;
    }

    /**
     * Obtém o valor da propriedade cfsCutoffLocation.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public GLogXMLLocRefType getCfsCutoffLocation() {
        return cfsCutoffLocation;
    }

    /**
     * Define o valor da propriedade cfsCutoffLocation.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public void setCfsCutoffLocation(GLogXMLLocRefType value) {
        this.cfsCutoffLocation = value;
    }

    /**
     * Obtém o valor da propriedade cfsCutoffDt.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getCfsCutoffDt() {
        return cfsCutoffDt;
    }

    /**
     * Define o valor da propriedade cfsCutoffDt.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setCfsCutoffDt(GLogDateTimeType value) {
        this.cfsCutoffDt = value;
    }

    /**
     * Obtém o valor da propriedade siCutoffDt.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getSiCutoffDt() {
        return siCutoffDt;
    }

    /**
     * Define o valor da propriedade siCutoffDt.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setSiCutoffDt(GLogDateTimeType value) {
        this.siCutoffDt = value;
    }

}
