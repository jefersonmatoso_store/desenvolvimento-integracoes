
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Demurrage Transaction Refnum.
 * 
 * <p>Classe Java de DemurrageTransactionRefnumType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="DemurrageTransactionRefnumType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DmTransactionRefnumQualGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="DmTransactionRefnumValue" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DemurrageTransactionRefnumType", propOrder = {
    "dmTransactionRefnumQualGid",
    "dmTransactionRefnumValue"
})
public class DemurrageTransactionRefnumType {

    @XmlElement(name = "DmTransactionRefnumQualGid")
    protected GLogXMLGidType dmTransactionRefnumQualGid;
    @XmlElement(name = "DmTransactionRefnumValue")
    protected String dmTransactionRefnumValue;

    /**
     * Obtém o valor da propriedade dmTransactionRefnumQualGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getDmTransactionRefnumQualGid() {
        return dmTransactionRefnumQualGid;
    }

    /**
     * Define o valor da propriedade dmTransactionRefnumQualGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setDmTransactionRefnumQualGid(GLogXMLGidType value) {
        this.dmTransactionRefnumQualGid = value;
    }

    /**
     * Obtém o valor da propriedade dmTransactionRefnumValue.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDmTransactionRefnumValue() {
        return dmTransactionRefnumValue;
    }

    /**
     * Define o valor da propriedade dmTransactionRefnumValue.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDmTransactionRefnumValue(String value) {
        this.dmTransactionRefnumValue = value;
    }

}
