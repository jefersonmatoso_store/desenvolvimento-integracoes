
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * An VoucherRefnum is an alternate way of referring to a Voucher.
 * 
 * <p>Classe Java de VoucherRefnumType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="VoucherRefnumType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="VoucherRefnumQualifierGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="VoucherRefnumValue" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VoucherRefnumType", propOrder = {
    "voucherRefnumQualifierGid",
    "voucherRefnumValue"
})
public class VoucherRefnumType {

    @XmlElement(name = "VoucherRefnumQualifierGid", required = true)
    protected GLogXMLGidType voucherRefnumQualifierGid;
    @XmlElement(name = "VoucherRefnumValue", required = true)
    protected String voucherRefnumValue;

    /**
     * Obtém o valor da propriedade voucherRefnumQualifierGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getVoucherRefnumQualifierGid() {
        return voucherRefnumQualifierGid;
    }

    /**
     * Define o valor da propriedade voucherRefnumQualifierGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setVoucherRefnumQualifierGid(GLogXMLGidType value) {
        this.voucherRefnumQualifierGid = value;
    }

    /**
     * Obtém o valor da propriedade voucherRefnumValue.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVoucherRefnumValue() {
        return voucherRefnumValue;
    }

    /**
     * Define o valor da propriedade voucherRefnumValue.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVoucherRefnumValue(String value) {
        this.voucherRefnumValue = value;
    }

}
