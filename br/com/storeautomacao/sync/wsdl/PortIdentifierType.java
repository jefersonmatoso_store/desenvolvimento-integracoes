
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Used to identify the location of a port.
 * 
 * <p>Classe Java de PortIdentifierType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="PortIdentifierType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PortIdentifierValue" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PortIdentifierQualifier" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PortIdentifierType", propOrder = {
    "portIdentifierValue",
    "portIdentifierQualifier"
})
public class PortIdentifierType {

    @XmlElement(name = "PortIdentifierValue", required = true)
    protected String portIdentifierValue;
    @XmlElement(name = "PortIdentifierQualifier", required = true)
    protected String portIdentifierQualifier;

    /**
     * Obtém o valor da propriedade portIdentifierValue.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPortIdentifierValue() {
        return portIdentifierValue;
    }

    /**
     * Define o valor da propriedade portIdentifierValue.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPortIdentifierValue(String value) {
        this.portIdentifierValue = value;
    }

    /**
     * Obtém o valor da propriedade portIdentifierQualifier.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPortIdentifierQualifier() {
        return portIdentifierQualifier;
    }

    /**
     * Define o valor da propriedade portIdentifierQualifier.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPortIdentifierQualifier(String value) {
        this.portIdentifierQualifier = value;
    }

}
