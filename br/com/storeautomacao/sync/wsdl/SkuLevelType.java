
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Specifies SKU stock levels.
 * 
 * <p>Classe Java de SkuLevelType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="SkuLevelType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SkuLevelTypeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="Level" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SkuQuantityTypeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SkuLevelType", propOrder = {
    "skuLevelTypeGid",
    "level",
    "skuQuantityTypeGid"
})
public class SkuLevelType {

    @XmlElement(name = "SkuLevelTypeGid", required = true)
    protected GLogXMLGidType skuLevelTypeGid;
    @XmlElement(name = "Level", required = true)
    protected String level;
    @XmlElement(name = "SkuQuantityTypeGid", required = true)
    protected GLogXMLGidType skuQuantityTypeGid;

    /**
     * Obtém o valor da propriedade skuLevelTypeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getSkuLevelTypeGid() {
        return skuLevelTypeGid;
    }

    /**
     * Define o valor da propriedade skuLevelTypeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setSkuLevelTypeGid(GLogXMLGidType value) {
        this.skuLevelTypeGid = value;
    }

    /**
     * Obtém o valor da propriedade level.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLevel() {
        return level;
    }

    /**
     * Define o valor da propriedade level.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLevel(String value) {
        this.level = value;
    }

    /**
     * Obtém o valor da propriedade skuQuantityTypeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getSkuQuantityTypeGid() {
        return skuQuantityTypeGid;
    }

    /**
     * Define o valor da propriedade skuQuantityTypeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setSkuQuantityTypeGid(GLogXMLGidType value) {
        this.skuQuantityTypeGid = value;
    }

}
