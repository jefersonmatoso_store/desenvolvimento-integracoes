
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;


/**
 * Indicates a representation of a piece of equipment associated with a SHIPMENT.
 *             It can be, but is not required to be, associated with an actual EQUIPMENT record.
 * 
 *             Note that the Equipment and EquipmentType elements are supported outbound only.
 *          
 * 
 * <p>Classe Java de SEquipmentType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="SEquipmentType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IntSavedQuery" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}IntSavedQueryType" minOccurs="0"/>
 *         &lt;element name="SEquipmentGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="TransactionCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransactionCodeType" minOccurs="0"/>
 *         &lt;element name="ReplaceChildren" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ReplaceChildrenType" minOccurs="0"/>
 *         &lt;element name="EquipmentInitial" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EquipmentNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EquipmentInitialNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EquipmentTypeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="EquipmentGroupGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="EquipmentGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="SEquipmentSeal" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}SEquipmentSealType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="EquipmentAttribute" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}EquipmentAttributeType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="WeightQualifier" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ScaleWeight" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLWeightType" minOccurs="0"/>
 *         &lt;element name="TareWeight" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLWeightType" minOccurs="0"/>
 *         &lt;element name="ScaleLocation" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ScaleName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ScaleTicket" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IntermodalEquipLength" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SubstituteEquipmentGroupGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="LoadConfigVolume" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLVolumeType" minOccurs="0"/>
 *         &lt;element name="ShipmentSEquipmentInfo" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ShipmentSEquipmentInfoType" minOccurs="0"/>
 *         &lt;element name="Equipment" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}EquipmentType" minOccurs="0"/>
 *         &lt;element name="EquipmentType" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}EquipmentDescriptionType" minOccurs="0"/>
 *         &lt;element name="IsFreight" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CheckDigit" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LicensePlate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SEquipmentType", propOrder = {
    "intSavedQuery",
    "sEquipmentGid",
    "transactionCode",
    "replaceChildren",
    "equipmentInitial",
    "equipmentNumber",
    "equipmentInitialNumber",
    "equipmentTypeGid",
    "equipmentGroupGid",
    "equipmentGid",
    "sEquipmentSeal",
    "equipmentAttribute",
    "weightQualifier",
    "scaleWeight",
    "tareWeight",
    "scaleLocation",
    "scaleName",
    "scaleTicket",
    "intermodalEquipLength",
    "substituteEquipmentGroupGid",
    "loadConfigVolume",
    "shipmentSEquipmentInfo",
    "equipment",
    "equipmentType",
    "isFreight",
    "checkDigit",
    "licensePlate"
})
public class SEquipmentType {

    @XmlElement(name = "IntSavedQuery")
    protected IntSavedQueryType intSavedQuery;
    @XmlElement(name = "SEquipmentGid", required = true)
    protected GLogXMLGidType sEquipmentGid;
    @XmlElement(name = "TransactionCode")
    @XmlSchemaType(name = "string")
    protected TransactionCodeType transactionCode;
    @XmlElement(name = "ReplaceChildren")
    protected ReplaceChildrenType replaceChildren;
    @XmlElement(name = "EquipmentInitial")
    protected String equipmentInitial;
    @XmlElement(name = "EquipmentNumber")
    protected String equipmentNumber;
    @XmlElement(name = "EquipmentInitialNumber")
    protected String equipmentInitialNumber;
    @XmlElement(name = "EquipmentTypeGid")
    protected GLogXMLGidType equipmentTypeGid;
    @XmlElement(name = "EquipmentGroupGid")
    protected GLogXMLGidType equipmentGroupGid;
    @XmlElement(name = "EquipmentGid")
    protected GLogXMLGidType equipmentGid;
    @XmlElement(name = "SEquipmentSeal")
    protected List<SEquipmentSealType> sEquipmentSeal;
    @XmlElement(name = "EquipmentAttribute")
    protected List<EquipmentAttributeType> equipmentAttribute;
    @XmlElement(name = "WeightQualifier")
    protected String weightQualifier;
    @XmlElement(name = "ScaleWeight")
    protected GLogXMLWeightType scaleWeight;
    @XmlElement(name = "TareWeight")
    protected GLogXMLWeightType tareWeight;
    @XmlElement(name = "ScaleLocation")
    protected String scaleLocation;
    @XmlElement(name = "ScaleName")
    protected String scaleName;
    @XmlElement(name = "ScaleTicket")
    protected String scaleTicket;
    @XmlElement(name = "IntermodalEquipLength")
    protected String intermodalEquipLength;
    @XmlElement(name = "SubstituteEquipmentGroupGid")
    protected GLogXMLGidType substituteEquipmentGroupGid;
    @XmlElement(name = "LoadConfigVolume")
    protected GLogXMLVolumeType loadConfigVolume;
    @XmlElement(name = "ShipmentSEquipmentInfo")
    protected ShipmentSEquipmentInfoType shipmentSEquipmentInfo;
    @XmlElement(name = "Equipment")
    protected EquipmentType equipment;
    @XmlElement(name = "EquipmentType")
    protected EquipmentDescriptionType equipmentType;
    @XmlElement(name = "IsFreight")
    protected String isFreight;
    @XmlElement(name = "CheckDigit")
    protected String checkDigit;
    @XmlElement(name = "LicensePlate")
    protected String licensePlate;

    /**
     * Obtém o valor da propriedade intSavedQuery.
     * 
     * @return
     *     possible object is
     *     {@link IntSavedQueryType }
     *     
     */
    public IntSavedQueryType getIntSavedQuery() {
        return intSavedQuery;
    }

    /**
     * Define o valor da propriedade intSavedQuery.
     * 
     * @param value
     *     allowed object is
     *     {@link IntSavedQueryType }
     *     
     */
    public void setIntSavedQuery(IntSavedQueryType value) {
        this.intSavedQuery = value;
    }

    /**
     * Obtém o valor da propriedade sEquipmentGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getSEquipmentGid() {
        return sEquipmentGid;
    }

    /**
     * Define o valor da propriedade sEquipmentGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setSEquipmentGid(GLogXMLGidType value) {
        this.sEquipmentGid = value;
    }

    /**
     * Obtém o valor da propriedade transactionCode.
     * 
     * @return
     *     possible object is
     *     {@link TransactionCodeType }
     *     
     */
    public TransactionCodeType getTransactionCode() {
        return transactionCode;
    }

    /**
     * Define o valor da propriedade transactionCode.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionCodeType }
     *     
     */
    public void setTransactionCode(TransactionCodeType value) {
        this.transactionCode = value;
    }

    /**
     * Obtém o valor da propriedade replaceChildren.
     * 
     * @return
     *     possible object is
     *     {@link ReplaceChildrenType }
     *     
     */
    public ReplaceChildrenType getReplaceChildren() {
        return replaceChildren;
    }

    /**
     * Define o valor da propriedade replaceChildren.
     * 
     * @param value
     *     allowed object is
     *     {@link ReplaceChildrenType }
     *     
     */
    public void setReplaceChildren(ReplaceChildrenType value) {
        this.replaceChildren = value;
    }

    /**
     * Obtém o valor da propriedade equipmentInitial.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEquipmentInitial() {
        return equipmentInitial;
    }

    /**
     * Define o valor da propriedade equipmentInitial.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEquipmentInitial(String value) {
        this.equipmentInitial = value;
    }

    /**
     * Obtém o valor da propriedade equipmentNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEquipmentNumber() {
        return equipmentNumber;
    }

    /**
     * Define o valor da propriedade equipmentNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEquipmentNumber(String value) {
        this.equipmentNumber = value;
    }

    /**
     * Obtém o valor da propriedade equipmentInitialNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEquipmentInitialNumber() {
        return equipmentInitialNumber;
    }

    /**
     * Define o valor da propriedade equipmentInitialNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEquipmentInitialNumber(String value) {
        this.equipmentInitialNumber = value;
    }

    /**
     * Obtém o valor da propriedade equipmentTypeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getEquipmentTypeGid() {
        return equipmentTypeGid;
    }

    /**
     * Define o valor da propriedade equipmentTypeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setEquipmentTypeGid(GLogXMLGidType value) {
        this.equipmentTypeGid = value;
    }

    /**
     * Obtém o valor da propriedade equipmentGroupGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getEquipmentGroupGid() {
        return equipmentGroupGid;
    }

    /**
     * Define o valor da propriedade equipmentGroupGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setEquipmentGroupGid(GLogXMLGidType value) {
        this.equipmentGroupGid = value;
    }

    /**
     * Obtém o valor da propriedade equipmentGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getEquipmentGid() {
        return equipmentGid;
    }

    /**
     * Define o valor da propriedade equipmentGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setEquipmentGid(GLogXMLGidType value) {
        this.equipmentGid = value;
    }

    /**
     * Gets the value of the sEquipmentSeal property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the sEquipmentSeal property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSEquipmentSeal().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SEquipmentSealType }
     * 
     * 
     */
    public List<SEquipmentSealType> getSEquipmentSeal() {
        if (sEquipmentSeal == null) {
            sEquipmentSeal = new ArrayList<SEquipmentSealType>();
        }
        return this.sEquipmentSeal;
    }

    /**
     * Gets the value of the equipmentAttribute property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the equipmentAttribute property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEquipmentAttribute().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EquipmentAttributeType }
     * 
     * 
     */
    public List<EquipmentAttributeType> getEquipmentAttribute() {
        if (equipmentAttribute == null) {
            equipmentAttribute = new ArrayList<EquipmentAttributeType>();
        }
        return this.equipmentAttribute;
    }

    /**
     * Obtém o valor da propriedade weightQualifier.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWeightQualifier() {
        return weightQualifier;
    }

    /**
     * Define o valor da propriedade weightQualifier.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWeightQualifier(String value) {
        this.weightQualifier = value;
    }

    /**
     * Obtém o valor da propriedade scaleWeight.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLWeightType }
     *     
     */
    public GLogXMLWeightType getScaleWeight() {
        return scaleWeight;
    }

    /**
     * Define o valor da propriedade scaleWeight.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLWeightType }
     *     
     */
    public void setScaleWeight(GLogXMLWeightType value) {
        this.scaleWeight = value;
    }

    /**
     * Obtém o valor da propriedade tareWeight.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLWeightType }
     *     
     */
    public GLogXMLWeightType getTareWeight() {
        return tareWeight;
    }

    /**
     * Define o valor da propriedade tareWeight.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLWeightType }
     *     
     */
    public void setTareWeight(GLogXMLWeightType value) {
        this.tareWeight = value;
    }

    /**
     * Obtém o valor da propriedade scaleLocation.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getScaleLocation() {
        return scaleLocation;
    }

    /**
     * Define o valor da propriedade scaleLocation.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setScaleLocation(String value) {
        this.scaleLocation = value;
    }

    /**
     * Obtém o valor da propriedade scaleName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getScaleName() {
        return scaleName;
    }

    /**
     * Define o valor da propriedade scaleName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setScaleName(String value) {
        this.scaleName = value;
    }

    /**
     * Obtém o valor da propriedade scaleTicket.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getScaleTicket() {
        return scaleTicket;
    }

    /**
     * Define o valor da propriedade scaleTicket.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setScaleTicket(String value) {
        this.scaleTicket = value;
    }

    /**
     * Obtém o valor da propriedade intermodalEquipLength.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIntermodalEquipLength() {
        return intermodalEquipLength;
    }

    /**
     * Define o valor da propriedade intermodalEquipLength.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIntermodalEquipLength(String value) {
        this.intermodalEquipLength = value;
    }

    /**
     * Obtém o valor da propriedade substituteEquipmentGroupGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getSubstituteEquipmentGroupGid() {
        return substituteEquipmentGroupGid;
    }

    /**
     * Define o valor da propriedade substituteEquipmentGroupGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setSubstituteEquipmentGroupGid(GLogXMLGidType value) {
        this.substituteEquipmentGroupGid = value;
    }

    /**
     * Obtém o valor da propriedade loadConfigVolume.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLVolumeType }
     *     
     */
    public GLogXMLVolumeType getLoadConfigVolume() {
        return loadConfigVolume;
    }

    /**
     * Define o valor da propriedade loadConfigVolume.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLVolumeType }
     *     
     */
    public void setLoadConfigVolume(GLogXMLVolumeType value) {
        this.loadConfigVolume = value;
    }

    /**
     * Obtém o valor da propriedade shipmentSEquipmentInfo.
     * 
     * @return
     *     possible object is
     *     {@link ShipmentSEquipmentInfoType }
     *     
     */
    public ShipmentSEquipmentInfoType getShipmentSEquipmentInfo() {
        return shipmentSEquipmentInfo;
    }

    /**
     * Define o valor da propriedade shipmentSEquipmentInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link ShipmentSEquipmentInfoType }
     *     
     */
    public void setShipmentSEquipmentInfo(ShipmentSEquipmentInfoType value) {
        this.shipmentSEquipmentInfo = value;
    }

    /**
     * Obtém o valor da propriedade equipment.
     * 
     * @return
     *     possible object is
     *     {@link EquipmentType }
     *     
     */
    public EquipmentType getEquipment() {
        return equipment;
    }

    /**
     * Define o valor da propriedade equipment.
     * 
     * @param value
     *     allowed object is
     *     {@link EquipmentType }
     *     
     */
    public void setEquipment(EquipmentType value) {
        this.equipment = value;
    }

    /**
     * Obtém o valor da propriedade equipmentType.
     * 
     * @return
     *     possible object is
     *     {@link EquipmentDescriptionType }
     *     
     */
    public EquipmentDescriptionType getEquipmentType() {
        return equipmentType;
    }

    /**
     * Define o valor da propriedade equipmentType.
     * 
     * @param value
     *     allowed object is
     *     {@link EquipmentDescriptionType }
     *     
     */
    public void setEquipmentType(EquipmentDescriptionType value) {
        this.equipmentType = value;
    }

    /**
     * Obtém o valor da propriedade isFreight.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsFreight() {
        return isFreight;
    }

    /**
     * Define o valor da propriedade isFreight.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsFreight(String value) {
        this.isFreight = value;
    }

    /**
     * Obtém o valor da propriedade checkDigit.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCheckDigit() {
        return checkDigit;
    }

    /**
     * Define o valor da propriedade checkDigit.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCheckDigit(String value) {
        this.checkDigit = value;
    }

    /**
     * Obtém o valor da propriedade licensePlate.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLicensePlate() {
        return licensePlate;
    }

    /**
     * Define o valor da propriedade licensePlate.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLicensePlate(String value) {
        this.licensePlate = value;
    }

}
