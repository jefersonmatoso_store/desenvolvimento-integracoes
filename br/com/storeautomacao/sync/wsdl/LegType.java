
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;


/**
 * 
 *             A Leg defines a destination for shipment activity on an itinerary and may establish requirements associated destination.
 *             For example, you can associate scheduling priorities, intermediate stops, service providers, costs, commodities, and
 *             modes of transport as requirements for a particular leg.
 *          
 * 
 * <p>Classe Java de LegType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="LegType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="LegGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="TransactionCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransactionCodeType"/>
 *         &lt;element name="LegName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SourceLocationRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocRefType" minOccurs="0"/>
 *         &lt;element name="SourceLocationRoleGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocRoleType" minOccurs="0"/>
 *         &lt;element name="DestinationLocationRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocRefType" minOccurs="0"/>
 *         &lt;element name="DestLocationRoleGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocRoleType" minOccurs="0"/>
 *         &lt;element name="DestLocationProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocProfileType" minOccurs="0"/>
 *         &lt;element name="DestLocationRadius" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDistanceType" minOccurs="0"/>
 *         &lt;element name="DestRadiusEmphasis" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CreateSingleShipment" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsPrimary" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ServiceProviderProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="EquipmentGroupProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="ModeProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="ExpectedTransitTime" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDurationType" minOccurs="0"/>
 *         &lt;element name="ExpectedCost" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/>
 *         &lt;element name="ArrangementResponsibility" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SchedulingPriority" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EquipmentAssignmentType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CalculateContractedRate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CalculateServiceTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RateOfferingGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="RateGeoGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="XDockLocation" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocRefType" minOccurs="0"/>
 *         &lt;element name="HazmatRegionGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLRegionGidType" minOccurs="0"/>
 *         &lt;element name="IntermediaryCorporationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="Rule11_Indicator" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RailInterModalPlanGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="CustomerRateCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="COFC_TOFC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EquipGroupProfileSetGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="ServiceProviderAssignmentType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MaxLocationCount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AutoConsolidateType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InterimPointOption" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RateServiceProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="PaymentMethodCodeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="ViaSourceLocProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocProfileType" minOccurs="0"/>
 *         &lt;element name="ViaDestLocProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocProfileType" minOccurs="0"/>
 *         &lt;element name="ViaSourceLocationRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocRefType" minOccurs="0"/>
 *         &lt;element name="ViaDestLocationRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocRefType" minOccurs="0"/>
 *         &lt;element name="PlanToOperationalLocation" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsTemporary" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TransArbSearchType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LegClassificationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="UseConsol" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InvolvedParty" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}InvolvedPartyType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="LegInterimPoint" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LegInterimPointType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="LegSchedule" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="RepetitionScheduleGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="CapacityOverrideGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="LegType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RoutingNetworkGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="SourceRegionGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLRegionGidType" minOccurs="0"/>
 *         &lt;element name="DestRegionGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLRegionGidType" minOccurs="0"/>
 *         &lt;element name="LegConsolidationGroupGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="DepotProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="AllowCrossConsolidation" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LegType", propOrder = {
    "legGid",
    "transactionCode",
    "legName",
    "sourceLocationRef",
    "sourceLocationRoleGid",
    "destinationLocationRef",
    "destLocationRoleGid",
    "destLocationProfileGid",
    "destLocationRadius",
    "destRadiusEmphasis",
    "createSingleShipment",
    "isPrimary",
    "serviceProviderProfileGid",
    "equipmentGroupProfileGid",
    "modeProfileGid",
    "expectedTransitTime",
    "expectedCost",
    "arrangementResponsibility",
    "schedulingPriority",
    "equipmentAssignmentType",
    "calculateContractedRate",
    "calculateServiceTime",
    "rateOfferingGid",
    "rateGeoGid",
    "xDockLocation",
    "hazmatRegionGid",
    "intermediaryCorporationGid",
    "rule11Indicator",
    "railInterModalPlanGid",
    "customerRateCode",
    "cofctofc",
    "equipGroupProfileSetGid",
    "serviceProviderAssignmentType",
    "maxLocationCount",
    "autoConsolidateType",
    "interimPointOption",
    "rateServiceProfileGid",
    "paymentMethodCodeGid",
    "viaSourceLocProfileGid",
    "viaDestLocProfileGid",
    "viaSourceLocationRef",
    "viaDestLocationRef",
    "planToOperationalLocation",
    "isTemporary",
    "transArbSearchType",
    "legClassificationGid",
    "useConsol",
    "involvedParty",
    "legInterimPoint",
    "legSchedule",
    "capacityOverrideGid",
    "legType",
    "routingNetworkGid",
    "sourceRegionGid",
    "destRegionGid",
    "legConsolidationGroupGid",
    "depotProfileGid",
    "allowCrossConsolidation"
})
public class LegType {

    @XmlElement(name = "LegGid", required = true)
    protected GLogXMLGidType legGid;
    @XmlElement(name = "TransactionCode", required = true)
    @XmlSchemaType(name = "string")
    protected TransactionCodeType transactionCode;
    @XmlElement(name = "LegName")
    protected String legName;
    @XmlElement(name = "SourceLocationRef")
    protected GLogXMLLocRefType sourceLocationRef;
    @XmlElement(name = "SourceLocationRoleGid")
    protected GLogXMLLocRoleType sourceLocationRoleGid;
    @XmlElement(name = "DestinationLocationRef")
    protected GLogXMLLocRefType destinationLocationRef;
    @XmlElement(name = "DestLocationRoleGid")
    protected GLogXMLLocRoleType destLocationRoleGid;
    @XmlElement(name = "DestLocationProfileGid")
    protected GLogXMLLocProfileType destLocationProfileGid;
    @XmlElement(name = "DestLocationRadius")
    protected GLogXMLDistanceType destLocationRadius;
    @XmlElement(name = "DestRadiusEmphasis")
    protected String destRadiusEmphasis;
    @XmlElement(name = "CreateSingleShipment")
    protected String createSingleShipment;
    @XmlElement(name = "IsPrimary")
    protected String isPrimary;
    @XmlElement(name = "ServiceProviderProfileGid")
    protected GLogXMLGidType serviceProviderProfileGid;
    @XmlElement(name = "EquipmentGroupProfileGid")
    protected GLogXMLGidType equipmentGroupProfileGid;
    @XmlElement(name = "ModeProfileGid", required = true)
    protected GLogXMLGidType modeProfileGid;
    @XmlElement(name = "ExpectedTransitTime")
    protected GLogXMLDurationType expectedTransitTime;
    @XmlElement(name = "ExpectedCost")
    protected GLogXMLFinancialAmountType expectedCost;
    @XmlElement(name = "ArrangementResponsibility")
    protected String arrangementResponsibility;
    @XmlElement(name = "SchedulingPriority")
    protected String schedulingPriority;
    @XmlElement(name = "EquipmentAssignmentType")
    protected String equipmentAssignmentType;
    @XmlElement(name = "CalculateContractedRate")
    protected String calculateContractedRate;
    @XmlElement(name = "CalculateServiceTime")
    protected String calculateServiceTime;
    @XmlElement(name = "RateOfferingGid")
    protected GLogXMLGidType rateOfferingGid;
    @XmlElement(name = "RateGeoGid")
    protected GLogXMLGidType rateGeoGid;
    @XmlElement(name = "XDockLocation")
    protected GLogXMLLocRefType xDockLocation;
    @XmlElement(name = "HazmatRegionGid")
    protected GLogXMLRegionGidType hazmatRegionGid;
    @XmlElement(name = "IntermediaryCorporationGid")
    protected GLogXMLGidType intermediaryCorporationGid;
    @XmlElement(name = "Rule11_Indicator")
    protected String rule11Indicator;
    @XmlElement(name = "RailInterModalPlanGid")
    protected GLogXMLGidType railInterModalPlanGid;
    @XmlElement(name = "CustomerRateCode")
    protected String customerRateCode;
    @XmlElement(name = "COFC_TOFC")
    protected String cofctofc;
    @XmlElement(name = "EquipGroupProfileSetGid")
    protected GLogXMLGidType equipGroupProfileSetGid;
    @XmlElement(name = "ServiceProviderAssignmentType")
    protected String serviceProviderAssignmentType;
    @XmlElement(name = "MaxLocationCount")
    protected String maxLocationCount;
    @XmlElement(name = "AutoConsolidateType")
    protected String autoConsolidateType;
    @XmlElement(name = "InterimPointOption")
    protected String interimPointOption;
    @XmlElement(name = "RateServiceProfileGid")
    protected GLogXMLGidType rateServiceProfileGid;
    @XmlElement(name = "PaymentMethodCodeGid")
    protected GLogXMLGidType paymentMethodCodeGid;
    @XmlElement(name = "ViaSourceLocProfileGid")
    protected GLogXMLLocProfileType viaSourceLocProfileGid;
    @XmlElement(name = "ViaDestLocProfileGid")
    protected GLogXMLLocProfileType viaDestLocProfileGid;
    @XmlElement(name = "ViaSourceLocationRef")
    protected GLogXMLLocRefType viaSourceLocationRef;
    @XmlElement(name = "ViaDestLocationRef")
    protected GLogXMLLocRefType viaDestLocationRef;
    @XmlElement(name = "PlanToOperationalLocation")
    protected String planToOperationalLocation;
    @XmlElement(name = "IsTemporary")
    protected String isTemporary;
    @XmlElement(name = "TransArbSearchType")
    protected String transArbSearchType;
    @XmlElement(name = "LegClassificationGid")
    protected GLogXMLGidType legClassificationGid;
    @XmlElement(name = "UseConsol")
    protected String useConsol;
    @XmlElement(name = "InvolvedParty")
    protected List<InvolvedPartyType> involvedParty;
    @XmlElement(name = "LegInterimPoint")
    protected List<LegInterimPointType> legInterimPoint;
    @XmlElement(name = "LegSchedule")
    protected List<LegType.LegSchedule> legSchedule;
    @XmlElement(name = "CapacityOverrideGid")
    protected List<GLogXMLGidType> capacityOverrideGid;
    @XmlElement(name = "LegType")
    protected String legType;
    @XmlElement(name = "RoutingNetworkGid")
    protected GLogXMLGidType routingNetworkGid;
    @XmlElement(name = "SourceRegionGid")
    protected GLogXMLRegionGidType sourceRegionGid;
    @XmlElement(name = "DestRegionGid")
    protected GLogXMLRegionGidType destRegionGid;
    @XmlElement(name = "LegConsolidationGroupGid")
    protected GLogXMLGidType legConsolidationGroupGid;
    @XmlElement(name = "DepotProfileGid")
    protected GLogXMLGidType depotProfileGid;
    @XmlElement(name = "AllowCrossConsolidation")
    protected String allowCrossConsolidation;

    /**
     * Obtém o valor da propriedade legGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getLegGid() {
        return legGid;
    }

    /**
     * Define o valor da propriedade legGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setLegGid(GLogXMLGidType value) {
        this.legGid = value;
    }

    /**
     * Obtém o valor da propriedade transactionCode.
     * 
     * @return
     *     possible object is
     *     {@link TransactionCodeType }
     *     
     */
    public TransactionCodeType getTransactionCode() {
        return transactionCode;
    }

    /**
     * Define o valor da propriedade transactionCode.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionCodeType }
     *     
     */
    public void setTransactionCode(TransactionCodeType value) {
        this.transactionCode = value;
    }

    /**
     * Obtém o valor da propriedade legName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLegName() {
        return legName;
    }

    /**
     * Define o valor da propriedade legName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLegName(String value) {
        this.legName = value;
    }

    /**
     * Obtém o valor da propriedade sourceLocationRef.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public GLogXMLLocRefType getSourceLocationRef() {
        return sourceLocationRef;
    }

    /**
     * Define o valor da propriedade sourceLocationRef.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public void setSourceLocationRef(GLogXMLLocRefType value) {
        this.sourceLocationRef = value;
    }

    /**
     * Obtém o valor da propriedade sourceLocationRoleGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocRoleType }
     *     
     */
    public GLogXMLLocRoleType getSourceLocationRoleGid() {
        return sourceLocationRoleGid;
    }

    /**
     * Define o valor da propriedade sourceLocationRoleGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocRoleType }
     *     
     */
    public void setSourceLocationRoleGid(GLogXMLLocRoleType value) {
        this.sourceLocationRoleGid = value;
    }

    /**
     * Obtém o valor da propriedade destinationLocationRef.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public GLogXMLLocRefType getDestinationLocationRef() {
        return destinationLocationRef;
    }

    /**
     * Define o valor da propriedade destinationLocationRef.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public void setDestinationLocationRef(GLogXMLLocRefType value) {
        this.destinationLocationRef = value;
    }

    /**
     * Obtém o valor da propriedade destLocationRoleGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocRoleType }
     *     
     */
    public GLogXMLLocRoleType getDestLocationRoleGid() {
        return destLocationRoleGid;
    }

    /**
     * Define o valor da propriedade destLocationRoleGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocRoleType }
     *     
     */
    public void setDestLocationRoleGid(GLogXMLLocRoleType value) {
        this.destLocationRoleGid = value;
    }

    /**
     * Obtém o valor da propriedade destLocationProfileGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocProfileType }
     *     
     */
    public GLogXMLLocProfileType getDestLocationProfileGid() {
        return destLocationProfileGid;
    }

    /**
     * Define o valor da propriedade destLocationProfileGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocProfileType }
     *     
     */
    public void setDestLocationProfileGid(GLogXMLLocProfileType value) {
        this.destLocationProfileGid = value;
    }

    /**
     * Obtém o valor da propriedade destLocationRadius.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDistanceType }
     *     
     */
    public GLogXMLDistanceType getDestLocationRadius() {
        return destLocationRadius;
    }

    /**
     * Define o valor da propriedade destLocationRadius.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDistanceType }
     *     
     */
    public void setDestLocationRadius(GLogXMLDistanceType value) {
        this.destLocationRadius = value;
    }

    /**
     * Obtém o valor da propriedade destRadiusEmphasis.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDestRadiusEmphasis() {
        return destRadiusEmphasis;
    }

    /**
     * Define o valor da propriedade destRadiusEmphasis.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDestRadiusEmphasis(String value) {
        this.destRadiusEmphasis = value;
    }

    /**
     * Obtém o valor da propriedade createSingleShipment.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCreateSingleShipment() {
        return createSingleShipment;
    }

    /**
     * Define o valor da propriedade createSingleShipment.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCreateSingleShipment(String value) {
        this.createSingleShipment = value;
    }

    /**
     * Obtém o valor da propriedade isPrimary.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsPrimary() {
        return isPrimary;
    }

    /**
     * Define o valor da propriedade isPrimary.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsPrimary(String value) {
        this.isPrimary = value;
    }

    /**
     * Obtém o valor da propriedade serviceProviderProfileGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getServiceProviderProfileGid() {
        return serviceProviderProfileGid;
    }

    /**
     * Define o valor da propriedade serviceProviderProfileGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setServiceProviderProfileGid(GLogXMLGidType value) {
        this.serviceProviderProfileGid = value;
    }

    /**
     * Obtém o valor da propriedade equipmentGroupProfileGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getEquipmentGroupProfileGid() {
        return equipmentGroupProfileGid;
    }

    /**
     * Define o valor da propriedade equipmentGroupProfileGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setEquipmentGroupProfileGid(GLogXMLGidType value) {
        this.equipmentGroupProfileGid = value;
    }

    /**
     * Obtém o valor da propriedade modeProfileGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getModeProfileGid() {
        return modeProfileGid;
    }

    /**
     * Define o valor da propriedade modeProfileGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setModeProfileGid(GLogXMLGidType value) {
        this.modeProfileGid = value;
    }

    /**
     * Obtém o valor da propriedade expectedTransitTime.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public GLogXMLDurationType getExpectedTransitTime() {
        return expectedTransitTime;
    }

    /**
     * Define o valor da propriedade expectedTransitTime.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public void setExpectedTransitTime(GLogXMLDurationType value) {
        this.expectedTransitTime = value;
    }

    /**
     * Obtém o valor da propriedade expectedCost.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getExpectedCost() {
        return expectedCost;
    }

    /**
     * Define o valor da propriedade expectedCost.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setExpectedCost(GLogXMLFinancialAmountType value) {
        this.expectedCost = value;
    }

    /**
     * Obtém o valor da propriedade arrangementResponsibility.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getArrangementResponsibility() {
        return arrangementResponsibility;
    }

    /**
     * Define o valor da propriedade arrangementResponsibility.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setArrangementResponsibility(String value) {
        this.arrangementResponsibility = value;
    }

    /**
     * Obtém o valor da propriedade schedulingPriority.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSchedulingPriority() {
        return schedulingPriority;
    }

    /**
     * Define o valor da propriedade schedulingPriority.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSchedulingPriority(String value) {
        this.schedulingPriority = value;
    }

    /**
     * Obtém o valor da propriedade equipmentAssignmentType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEquipmentAssignmentType() {
        return equipmentAssignmentType;
    }

    /**
     * Define o valor da propriedade equipmentAssignmentType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEquipmentAssignmentType(String value) {
        this.equipmentAssignmentType = value;
    }

    /**
     * Obtém o valor da propriedade calculateContractedRate.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCalculateContractedRate() {
        return calculateContractedRate;
    }

    /**
     * Define o valor da propriedade calculateContractedRate.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCalculateContractedRate(String value) {
        this.calculateContractedRate = value;
    }

    /**
     * Obtém o valor da propriedade calculateServiceTime.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCalculateServiceTime() {
        return calculateServiceTime;
    }

    /**
     * Define o valor da propriedade calculateServiceTime.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCalculateServiceTime(String value) {
        this.calculateServiceTime = value;
    }

    /**
     * Obtém o valor da propriedade rateOfferingGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRateOfferingGid() {
        return rateOfferingGid;
    }

    /**
     * Define o valor da propriedade rateOfferingGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRateOfferingGid(GLogXMLGidType value) {
        this.rateOfferingGid = value;
    }

    /**
     * Obtém o valor da propriedade rateGeoGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRateGeoGid() {
        return rateGeoGid;
    }

    /**
     * Define o valor da propriedade rateGeoGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRateGeoGid(GLogXMLGidType value) {
        this.rateGeoGid = value;
    }

    /**
     * Obtém o valor da propriedade xDockLocation.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public GLogXMLLocRefType getXDockLocation() {
        return xDockLocation;
    }

    /**
     * Define o valor da propriedade xDockLocation.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public void setXDockLocation(GLogXMLLocRefType value) {
        this.xDockLocation = value;
    }

    /**
     * Obtém o valor da propriedade hazmatRegionGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLRegionGidType }
     *     
     */
    public GLogXMLRegionGidType getHazmatRegionGid() {
        return hazmatRegionGid;
    }

    /**
     * Define o valor da propriedade hazmatRegionGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLRegionGidType }
     *     
     */
    public void setHazmatRegionGid(GLogXMLRegionGidType value) {
        this.hazmatRegionGid = value;
    }

    /**
     * Obtém o valor da propriedade intermediaryCorporationGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getIntermediaryCorporationGid() {
        return intermediaryCorporationGid;
    }

    /**
     * Define o valor da propriedade intermediaryCorporationGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setIntermediaryCorporationGid(GLogXMLGidType value) {
        this.intermediaryCorporationGid = value;
    }

    /**
     * Obtém o valor da propriedade rule11Indicator.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRule11Indicator() {
        return rule11Indicator;
    }

    /**
     * Define o valor da propriedade rule11Indicator.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRule11Indicator(String value) {
        this.rule11Indicator = value;
    }

    /**
     * Obtém o valor da propriedade railInterModalPlanGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRailInterModalPlanGid() {
        return railInterModalPlanGid;
    }

    /**
     * Define o valor da propriedade railInterModalPlanGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRailInterModalPlanGid(GLogXMLGidType value) {
        this.railInterModalPlanGid = value;
    }

    /**
     * Obtém o valor da propriedade customerRateCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomerRateCode() {
        return customerRateCode;
    }

    /**
     * Define o valor da propriedade customerRateCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomerRateCode(String value) {
        this.customerRateCode = value;
    }

    /**
     * Obtém o valor da propriedade cofctofc.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCOFCTOFC() {
        return cofctofc;
    }

    /**
     * Define o valor da propriedade cofctofc.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCOFCTOFC(String value) {
        this.cofctofc = value;
    }

    /**
     * Obtém o valor da propriedade equipGroupProfileSetGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getEquipGroupProfileSetGid() {
        return equipGroupProfileSetGid;
    }

    /**
     * Define o valor da propriedade equipGroupProfileSetGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setEquipGroupProfileSetGid(GLogXMLGidType value) {
        this.equipGroupProfileSetGid = value;
    }

    /**
     * Obtém o valor da propriedade serviceProviderAssignmentType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderAssignmentType() {
        return serviceProviderAssignmentType;
    }

    /**
     * Define o valor da propriedade serviceProviderAssignmentType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderAssignmentType(String value) {
        this.serviceProviderAssignmentType = value;
    }

    /**
     * Obtém o valor da propriedade maxLocationCount.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMaxLocationCount() {
        return maxLocationCount;
    }

    /**
     * Define o valor da propriedade maxLocationCount.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMaxLocationCount(String value) {
        this.maxLocationCount = value;
    }

    /**
     * Obtém o valor da propriedade autoConsolidateType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAutoConsolidateType() {
        return autoConsolidateType;
    }

    /**
     * Define o valor da propriedade autoConsolidateType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAutoConsolidateType(String value) {
        this.autoConsolidateType = value;
    }

    /**
     * Obtém o valor da propriedade interimPointOption.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInterimPointOption() {
        return interimPointOption;
    }

    /**
     * Define o valor da propriedade interimPointOption.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInterimPointOption(String value) {
        this.interimPointOption = value;
    }

    /**
     * Obtém o valor da propriedade rateServiceProfileGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRateServiceProfileGid() {
        return rateServiceProfileGid;
    }

    /**
     * Define o valor da propriedade rateServiceProfileGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRateServiceProfileGid(GLogXMLGidType value) {
        this.rateServiceProfileGid = value;
    }

    /**
     * Obtém o valor da propriedade paymentMethodCodeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getPaymentMethodCodeGid() {
        return paymentMethodCodeGid;
    }

    /**
     * Define o valor da propriedade paymentMethodCodeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setPaymentMethodCodeGid(GLogXMLGidType value) {
        this.paymentMethodCodeGid = value;
    }

    /**
     * Obtém o valor da propriedade viaSourceLocProfileGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocProfileType }
     *     
     */
    public GLogXMLLocProfileType getViaSourceLocProfileGid() {
        return viaSourceLocProfileGid;
    }

    /**
     * Define o valor da propriedade viaSourceLocProfileGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocProfileType }
     *     
     */
    public void setViaSourceLocProfileGid(GLogXMLLocProfileType value) {
        this.viaSourceLocProfileGid = value;
    }

    /**
     * Obtém o valor da propriedade viaDestLocProfileGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocProfileType }
     *     
     */
    public GLogXMLLocProfileType getViaDestLocProfileGid() {
        return viaDestLocProfileGid;
    }

    /**
     * Define o valor da propriedade viaDestLocProfileGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocProfileType }
     *     
     */
    public void setViaDestLocProfileGid(GLogXMLLocProfileType value) {
        this.viaDestLocProfileGid = value;
    }

    /**
     * Obtém o valor da propriedade viaSourceLocationRef.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public GLogXMLLocRefType getViaSourceLocationRef() {
        return viaSourceLocationRef;
    }

    /**
     * Define o valor da propriedade viaSourceLocationRef.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public void setViaSourceLocationRef(GLogXMLLocRefType value) {
        this.viaSourceLocationRef = value;
    }

    /**
     * Obtém o valor da propriedade viaDestLocationRef.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public GLogXMLLocRefType getViaDestLocationRef() {
        return viaDestLocationRef;
    }

    /**
     * Define o valor da propriedade viaDestLocationRef.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public void setViaDestLocationRef(GLogXMLLocRefType value) {
        this.viaDestLocationRef = value;
    }

    /**
     * Obtém o valor da propriedade planToOperationalLocation.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPlanToOperationalLocation() {
        return planToOperationalLocation;
    }

    /**
     * Define o valor da propriedade planToOperationalLocation.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPlanToOperationalLocation(String value) {
        this.planToOperationalLocation = value;
    }

    /**
     * Obtém o valor da propriedade isTemporary.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsTemporary() {
        return isTemporary;
    }

    /**
     * Define o valor da propriedade isTemporary.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsTemporary(String value) {
        this.isTemporary = value;
    }

    /**
     * Obtém o valor da propriedade transArbSearchType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransArbSearchType() {
        return transArbSearchType;
    }

    /**
     * Define o valor da propriedade transArbSearchType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransArbSearchType(String value) {
        this.transArbSearchType = value;
    }

    /**
     * Obtém o valor da propriedade legClassificationGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getLegClassificationGid() {
        return legClassificationGid;
    }

    /**
     * Define o valor da propriedade legClassificationGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setLegClassificationGid(GLogXMLGidType value) {
        this.legClassificationGid = value;
    }

    /**
     * Obtém o valor da propriedade useConsol.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUseConsol() {
        return useConsol;
    }

    /**
     * Define o valor da propriedade useConsol.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUseConsol(String value) {
        this.useConsol = value;
    }

    /**
     * Gets the value of the involvedParty property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the involvedParty property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInvolvedParty().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link InvolvedPartyType }
     * 
     * 
     */
    public List<InvolvedPartyType> getInvolvedParty() {
        if (involvedParty == null) {
            involvedParty = new ArrayList<InvolvedPartyType>();
        }
        return this.involvedParty;
    }

    /**
     * Gets the value of the legInterimPoint property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the legInterimPoint property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLegInterimPoint().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LegInterimPointType }
     * 
     * 
     */
    public List<LegInterimPointType> getLegInterimPoint() {
        if (legInterimPoint == null) {
            legInterimPoint = new ArrayList<LegInterimPointType>();
        }
        return this.legInterimPoint;
    }

    /**
     * Gets the value of the legSchedule property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the legSchedule property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLegSchedule().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LegType.LegSchedule }
     * 
     * 
     */
    public List<LegType.LegSchedule> getLegSchedule() {
        if (legSchedule == null) {
            legSchedule = new ArrayList<LegType.LegSchedule>();
        }
        return this.legSchedule;
    }

    /**
     * Gets the value of the capacityOverrideGid property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the capacityOverrideGid property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCapacityOverrideGid().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GLogXMLGidType }
     * 
     * 
     */
    public List<GLogXMLGidType> getCapacityOverrideGid() {
        if (capacityOverrideGid == null) {
            capacityOverrideGid = new ArrayList<GLogXMLGidType>();
        }
        return this.capacityOverrideGid;
    }

    /**
     * Obtém o valor da propriedade legType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLegType() {
        return legType;
    }

    /**
     * Define o valor da propriedade legType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLegType(String value) {
        this.legType = value;
    }

    /**
     * Obtém o valor da propriedade routingNetworkGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRoutingNetworkGid() {
        return routingNetworkGid;
    }

    /**
     * Define o valor da propriedade routingNetworkGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRoutingNetworkGid(GLogXMLGidType value) {
        this.routingNetworkGid = value;
    }

    /**
     * Obtém o valor da propriedade sourceRegionGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLRegionGidType }
     *     
     */
    public GLogXMLRegionGidType getSourceRegionGid() {
        return sourceRegionGid;
    }

    /**
     * Define o valor da propriedade sourceRegionGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLRegionGidType }
     *     
     */
    public void setSourceRegionGid(GLogXMLRegionGidType value) {
        this.sourceRegionGid = value;
    }

    /**
     * Obtém o valor da propriedade destRegionGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLRegionGidType }
     *     
     */
    public GLogXMLRegionGidType getDestRegionGid() {
        return destRegionGid;
    }

    /**
     * Define o valor da propriedade destRegionGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLRegionGidType }
     *     
     */
    public void setDestRegionGid(GLogXMLRegionGidType value) {
        this.destRegionGid = value;
    }

    /**
     * Obtém o valor da propriedade legConsolidationGroupGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getLegConsolidationGroupGid() {
        return legConsolidationGroupGid;
    }

    /**
     * Define o valor da propriedade legConsolidationGroupGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setLegConsolidationGroupGid(GLogXMLGidType value) {
        this.legConsolidationGroupGid = value;
    }

    /**
     * Obtém o valor da propriedade depotProfileGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getDepotProfileGid() {
        return depotProfileGid;
    }

    /**
     * Define o valor da propriedade depotProfileGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setDepotProfileGid(GLogXMLGidType value) {
        this.depotProfileGid = value;
    }

    /**
     * Obtém o valor da propriedade allowCrossConsolidation.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAllowCrossConsolidation() {
        return allowCrossConsolidation;
    }

    /**
     * Define o valor da propriedade allowCrossConsolidation.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAllowCrossConsolidation(String value) {
        this.allowCrossConsolidation = value;
    }


    /**
     * <p>Classe Java de anonymous complex type.
     * 
     * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="RepetitionScheduleGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "repetitionScheduleGid"
    })
    public static class LegSchedule {

        @XmlElement(name = "RepetitionScheduleGid", required = true)
        protected GLogXMLGidType repetitionScheduleGid;

        /**
         * Obtém o valor da propriedade repetitionScheduleGid.
         * 
         * @return
         *     possible object is
         *     {@link GLogXMLGidType }
         *     
         */
        public GLogXMLGidType getRepetitionScheduleGid() {
            return repetitionScheduleGid;
        }

        /**
         * Define o valor da propriedade repetitionScheduleGid.
         * 
         * @param value
         *     allowed object is
         *     {@link GLogXMLGidType }
         *     
         */
        public void setRepetitionScheduleGid(GLogXMLGidType value) {
            this.repetitionScheduleGid = value;
        }

    }

}
