
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;


/**
 * Used to specify a pre-formatted selection of text.
 * 
 * <p>Classe Java de TextTemplateType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="TextTemplateType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TextTemplateGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="TextTemplatePattern" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="DataQueryTypeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="TextTemplateIsModifiable" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TextTemplateDocumentDef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TextTemplateDocumentDefType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="TransactionCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransactionCodeType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TextTemplateType", propOrder = {
    "textTemplateGid",
    "textTemplatePattern",
    "dataQueryTypeGid",
    "textTemplateIsModifiable",
    "textTemplateDocumentDef",
    "transactionCode"
})
public class TextTemplateType {

    @XmlElement(name = "TextTemplateGid", required = true)
    protected GLogXMLGidType textTemplateGid;
    @XmlElement(name = "TextTemplatePattern", required = true)
    protected String textTemplatePattern;
    @XmlElement(name = "DataQueryTypeGid")
    protected GLogXMLGidType dataQueryTypeGid;
    @XmlElement(name = "TextTemplateIsModifiable")
    protected String textTemplateIsModifiable;
    @XmlElement(name = "TextTemplateDocumentDef")
    protected List<TextTemplateDocumentDefType> textTemplateDocumentDef;
    @XmlElement(name = "TransactionCode")
    @XmlSchemaType(name = "string")
    protected TransactionCodeType transactionCode;

    /**
     * Obtém o valor da propriedade textTemplateGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getTextTemplateGid() {
        return textTemplateGid;
    }

    /**
     * Define o valor da propriedade textTemplateGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setTextTemplateGid(GLogXMLGidType value) {
        this.textTemplateGid = value;
    }

    /**
     * Obtém o valor da propriedade textTemplatePattern.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTextTemplatePattern() {
        return textTemplatePattern;
    }

    /**
     * Define o valor da propriedade textTemplatePattern.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTextTemplatePattern(String value) {
        this.textTemplatePattern = value;
    }

    /**
     * Obtém o valor da propriedade dataQueryTypeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getDataQueryTypeGid() {
        return dataQueryTypeGid;
    }

    /**
     * Define o valor da propriedade dataQueryTypeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setDataQueryTypeGid(GLogXMLGidType value) {
        this.dataQueryTypeGid = value;
    }

    /**
     * Obtém o valor da propriedade textTemplateIsModifiable.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTextTemplateIsModifiable() {
        return textTemplateIsModifiable;
    }

    /**
     * Define o valor da propriedade textTemplateIsModifiable.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTextTemplateIsModifiable(String value) {
        this.textTemplateIsModifiable = value;
    }

    /**
     * Gets the value of the textTemplateDocumentDef property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the textTemplateDocumentDef property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTextTemplateDocumentDef().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TextTemplateDocumentDefType }
     * 
     * 
     */
    public List<TextTemplateDocumentDefType> getTextTemplateDocumentDef() {
        if (textTemplateDocumentDef == null) {
            textTemplateDocumentDef = new ArrayList<TextTemplateDocumentDefType>();
        }
        return this.textTemplateDocumentDef;
    }

    /**
     * Obtém o valor da propriedade transactionCode.
     * 
     * @return
     *     possible object is
     *     {@link TransactionCodeType }
     *     
     */
    public TransactionCodeType getTransactionCode() {
        return transactionCode;
    }

    /**
     * Define o valor da propriedade transactionCode.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionCodeType }
     *     
     */
    public void setTransactionCode(TransactionCodeType value) {
        this.transactionCode = value;
    }

}
