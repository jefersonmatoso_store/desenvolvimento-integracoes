
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Specifies the invoice cost information that has been copied to the shipment.
 * 
 * <p>Classe Java de ShipmentInvoiceCostInfoType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="ShipmentInvoiceCostInfoType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TotalMatchedInvoiceCost" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/>
 *         &lt;element name="OriginalInvoiceCost" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/>
 *         &lt;element name="TotalApprovedCost" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ShipmentInvoiceCostInfoType", propOrder = {
    "totalMatchedInvoiceCost",
    "originalInvoiceCost",
    "totalApprovedCost"
})
public class ShipmentInvoiceCostInfoType {

    @XmlElement(name = "TotalMatchedInvoiceCost")
    protected GLogXMLFinancialAmountType totalMatchedInvoiceCost;
    @XmlElement(name = "OriginalInvoiceCost")
    protected GLogXMLFinancialAmountType originalInvoiceCost;
    @XmlElement(name = "TotalApprovedCost")
    protected GLogXMLFinancialAmountType totalApprovedCost;

    /**
     * Obtém o valor da propriedade totalMatchedInvoiceCost.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getTotalMatchedInvoiceCost() {
        return totalMatchedInvoiceCost;
    }

    /**
     * Define o valor da propriedade totalMatchedInvoiceCost.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setTotalMatchedInvoiceCost(GLogXMLFinancialAmountType value) {
        this.totalMatchedInvoiceCost = value;
    }

    /**
     * Obtém o valor da propriedade originalInvoiceCost.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getOriginalInvoiceCost() {
        return originalInvoiceCost;
    }

    /**
     * Define o valor da propriedade originalInvoiceCost.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setOriginalInvoiceCost(GLogXMLFinancialAmountType value) {
        this.originalInvoiceCost = value;
    }

    /**
     * Obtém o valor da propriedade totalApprovedCost.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getTotalApprovedCost() {
        return totalApprovedCost;
    }

    /**
     * Define o valor da propriedade totalApprovedCost.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setTotalApprovedCost(GLogXMLFinancialAmountType value) {
        this.totalApprovedCost = value;
    }

}
