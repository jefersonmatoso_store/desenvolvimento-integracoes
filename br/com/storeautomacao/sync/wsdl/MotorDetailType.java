
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * 
 *             (inbound) MotorDetail provides invoice data specific to motor carriers.
 *          
 * 
 * <p>Classe Java de MotorDetailType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="MotorDetailType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PickupDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType"/>
 *         &lt;element name="DeliveryDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType"/>
 *         &lt;element name="MotorStopOff" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}StopOffType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="MotorEquipment" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}MotorEquipmentType" maxOccurs="unbounded"/>
 *         &lt;element name="MotorLineItem" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}MotorLineItemType" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MotorDetailType", propOrder = {
    "pickupDate",
    "deliveryDate",
    "motorStopOff",
    "motorEquipment",
    "motorLineItem"
})
public class MotorDetailType {

    @XmlElement(name = "PickupDate", required = true)
    protected GLogDateTimeType pickupDate;
    @XmlElement(name = "DeliveryDate", required = true)
    protected GLogDateTimeType deliveryDate;
    @XmlElement(name = "MotorStopOff")
    protected List<StopOffType> motorStopOff;
    @XmlElement(name = "MotorEquipment", required = true)
    protected List<MotorEquipmentType> motorEquipment;
    @XmlElement(name = "MotorLineItem", required = true)
    protected List<MotorLineItemType> motorLineItem;

    /**
     * Obtém o valor da propriedade pickupDate.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getPickupDate() {
        return pickupDate;
    }

    /**
     * Define o valor da propriedade pickupDate.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setPickupDate(GLogDateTimeType value) {
        this.pickupDate = value;
    }

    /**
     * Obtém o valor da propriedade deliveryDate.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getDeliveryDate() {
        return deliveryDate;
    }

    /**
     * Define o valor da propriedade deliveryDate.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setDeliveryDate(GLogDateTimeType value) {
        this.deliveryDate = value;
    }

    /**
     * Gets the value of the motorStopOff property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the motorStopOff property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMotorStopOff().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link StopOffType }
     * 
     * 
     */
    public List<StopOffType> getMotorStopOff() {
        if (motorStopOff == null) {
            motorStopOff = new ArrayList<StopOffType>();
        }
        return this.motorStopOff;
    }

    /**
     * Gets the value of the motorEquipment property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the motorEquipment property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMotorEquipment().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MotorEquipmentType }
     * 
     * 
     */
    public List<MotorEquipmentType> getMotorEquipment() {
        if (motorEquipment == null) {
            motorEquipment = new ArrayList<MotorEquipmentType>();
        }
        return this.motorEquipment;
    }

    /**
     * Gets the value of the motorLineItem property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the motorLineItem property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMotorLineItem().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MotorLineItemType }
     * 
     * 
     */
    public List<MotorLineItemType> getMotorLineItem() {
        if (motorLineItem == null) {
            motorLineItem = new ArrayList<MotorLineItemType>();
        }
        return this.motorLineItem;
    }

}
