
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.*;


/**
 * Indicates the special services required to make a delivery. Used to deprecate SpecialService element
 * 
 * <p>Classe Java de SpclServiceType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="SpclServiceType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IntSavedQuery" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}IntSavedQueryType" minOccurs="0"/>
 *         &lt;element name="SpecialServiceGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="TransactionCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransactionCodeType"/>
 *         &lt;element name="SpecialServiceDesc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SpclServiceGroupGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="ModeProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="IsArbitrary" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsForDriverChk" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsForEquipChk" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsForPUChk" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsForRating" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PULevel" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DriverLevel" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EquipLevel" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ForSourceDest" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ActivityTypeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="SequenceNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsTimeBased" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsIgnorable" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PayableIndicatorGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="BillableIndicatorGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="DriverTimeType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SpclServiceType", propOrder = {
    "intSavedQuery",
    "specialServiceGid",
    "transactionCode",
    "specialServiceDesc",
    "spclServiceGroupGid",
    "modeProfileGid",
    "isArbitrary",
    "isForDriverChk",
    "isForEquipChk",
    "isForPUChk",
    "isForRating",
    "puLevel",
    "driverLevel",
    "equipLevel",
    "forSourceDest",
    "activityTypeGid",
    "sequenceNumber",
    "isTimeBased",
    "isIgnorable",
    "payableIndicatorGid",
    "billableIndicatorGid",
    "driverTimeType"
})
public class SpclServiceType {

    @XmlElement(name = "IntSavedQuery")
    protected IntSavedQueryType intSavedQuery;
    @XmlElement(name = "SpecialServiceGid", required = true)
    protected GLogXMLGidType specialServiceGid;
    @XmlElement(name = "TransactionCode", required = true)
    @XmlSchemaType(name = "string")
    protected TransactionCodeType transactionCode;
    @XmlElement(name = "SpecialServiceDesc")
    protected String specialServiceDesc;
    @XmlElement(name = "SpclServiceGroupGid")
    protected GLogXMLGidType spclServiceGroupGid;
    @XmlElement(name = "ModeProfileGid")
    protected GLogXMLGidType modeProfileGid;
    @XmlElement(name = "IsArbitrary")
    protected String isArbitrary;
    @XmlElement(name = "IsForDriverChk")
    protected String isForDriverChk;
    @XmlElement(name = "IsForEquipChk")
    protected String isForEquipChk;
    @XmlElement(name = "IsForPUChk")
    protected String isForPUChk;
    @XmlElement(name = "IsForRating")
    protected String isForRating;
    @XmlElement(name = "PULevel")
    protected String puLevel;
    @XmlElement(name = "DriverLevel")
    protected String driverLevel;
    @XmlElement(name = "EquipLevel")
    protected String equipLevel;
    @XmlElement(name = "ForSourceDest")
    protected String forSourceDest;
    @XmlElement(name = "ActivityTypeGid")
    protected GLogXMLGidType activityTypeGid;
    @XmlElement(name = "SequenceNumber")
    protected String sequenceNumber;
    @XmlElement(name = "IsTimeBased")
    protected String isTimeBased;
    @XmlElement(name = "IsIgnorable")
    protected String isIgnorable;
    @XmlElement(name = "PayableIndicatorGid")
    protected GLogXMLGidType payableIndicatorGid;
    @XmlElement(name = "BillableIndicatorGid")
    protected GLogXMLGidType billableIndicatorGid;
    @XmlElement(name = "DriverTimeType")
    protected String driverTimeType;

    /**
     * Obtém o valor da propriedade intSavedQuery.
     * 
     * @return
     *     possible object is
     *     {@link IntSavedQueryType }
     *     
     */
    public IntSavedQueryType getIntSavedQuery() {
        return intSavedQuery;
    }

    /**
     * Define o valor da propriedade intSavedQuery.
     * 
     * @param value
     *     allowed object is
     *     {@link IntSavedQueryType }
     *     
     */
    public void setIntSavedQuery(IntSavedQueryType value) {
        this.intSavedQuery = value;
    }

    /**
     * Obtém o valor da propriedade specialServiceGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getSpecialServiceGid() {
        return specialServiceGid;
    }

    /**
     * Define o valor da propriedade specialServiceGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setSpecialServiceGid(GLogXMLGidType value) {
        this.specialServiceGid = value;
    }

    /**
     * Obtém o valor da propriedade transactionCode.
     * 
     * @return
     *     possible object is
     *     {@link TransactionCodeType }
     *     
     */
    public TransactionCodeType getTransactionCode() {
        return transactionCode;
    }

    /**
     * Define o valor da propriedade transactionCode.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionCodeType }
     *     
     */
    public void setTransactionCode(TransactionCodeType value) {
        this.transactionCode = value;
    }

    /**
     * Obtém o valor da propriedade specialServiceDesc.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSpecialServiceDesc() {
        return specialServiceDesc;
    }

    /**
     * Define o valor da propriedade specialServiceDesc.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSpecialServiceDesc(String value) {
        this.specialServiceDesc = value;
    }

    /**
     * Obtém o valor da propriedade spclServiceGroupGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getSpclServiceGroupGid() {
        return spclServiceGroupGid;
    }

    /**
     * Define o valor da propriedade spclServiceGroupGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setSpclServiceGroupGid(GLogXMLGidType value) {
        this.spclServiceGroupGid = value;
    }

    /**
     * Obtém o valor da propriedade modeProfileGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getModeProfileGid() {
        return modeProfileGid;
    }

    /**
     * Define o valor da propriedade modeProfileGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setModeProfileGid(GLogXMLGidType value) {
        this.modeProfileGid = value;
    }

    /**
     * Obtém o valor da propriedade isArbitrary.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsArbitrary() {
        return isArbitrary;
    }

    /**
     * Define o valor da propriedade isArbitrary.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsArbitrary(String value) {
        this.isArbitrary = value;
    }

    /**
     * Obtém o valor da propriedade isForDriverChk.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsForDriverChk() {
        return isForDriverChk;
    }

    /**
     * Define o valor da propriedade isForDriverChk.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsForDriverChk(String value) {
        this.isForDriverChk = value;
    }

    /**
     * Obtém o valor da propriedade isForEquipChk.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsForEquipChk() {
        return isForEquipChk;
    }

    /**
     * Define o valor da propriedade isForEquipChk.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsForEquipChk(String value) {
        this.isForEquipChk = value;
    }

    /**
     * Obtém o valor da propriedade isForPUChk.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsForPUChk() {
        return isForPUChk;
    }

    /**
     * Define o valor da propriedade isForPUChk.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsForPUChk(String value) {
        this.isForPUChk = value;
    }

    /**
     * Obtém o valor da propriedade isForRating.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsForRating() {
        return isForRating;
    }

    /**
     * Define o valor da propriedade isForRating.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsForRating(String value) {
        this.isForRating = value;
    }

    /**
     * Obtém o valor da propriedade puLevel.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPULevel() {
        return puLevel;
    }

    /**
     * Define o valor da propriedade puLevel.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPULevel(String value) {
        this.puLevel = value;
    }

    /**
     * Obtém o valor da propriedade driverLevel.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDriverLevel() {
        return driverLevel;
    }

    /**
     * Define o valor da propriedade driverLevel.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDriverLevel(String value) {
        this.driverLevel = value;
    }

    /**
     * Obtém o valor da propriedade equipLevel.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEquipLevel() {
        return equipLevel;
    }

    /**
     * Define o valor da propriedade equipLevel.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEquipLevel(String value) {
        this.equipLevel = value;
    }

    /**
     * Obtém o valor da propriedade forSourceDest.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getForSourceDest() {
        return forSourceDest;
    }

    /**
     * Define o valor da propriedade forSourceDest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setForSourceDest(String value) {
        this.forSourceDest = value;
    }

    /**
     * Obtém o valor da propriedade activityTypeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getActivityTypeGid() {
        return activityTypeGid;
    }

    /**
     * Define o valor da propriedade activityTypeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setActivityTypeGid(GLogXMLGidType value) {
        this.activityTypeGid = value;
    }

    /**
     * Obtém o valor da propriedade sequenceNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSequenceNumber() {
        return sequenceNumber;
    }

    /**
     * Define o valor da propriedade sequenceNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSequenceNumber(String value) {
        this.sequenceNumber = value;
    }

    /**
     * Obtém o valor da propriedade isTimeBased.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsTimeBased() {
        return isTimeBased;
    }

    /**
     * Define o valor da propriedade isTimeBased.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsTimeBased(String value) {
        this.isTimeBased = value;
    }

    /**
     * Obtém o valor da propriedade isIgnorable.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsIgnorable() {
        return isIgnorable;
    }

    /**
     * Define o valor da propriedade isIgnorable.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsIgnorable(String value) {
        this.isIgnorable = value;
    }

    /**
     * Obtém o valor da propriedade payableIndicatorGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getPayableIndicatorGid() {
        return payableIndicatorGid;
    }

    /**
     * Define o valor da propriedade payableIndicatorGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setPayableIndicatorGid(GLogXMLGidType value) {
        this.payableIndicatorGid = value;
    }

    /**
     * Obtém o valor da propriedade billableIndicatorGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getBillableIndicatorGid() {
        return billableIndicatorGid;
    }

    /**
     * Define o valor da propriedade billableIndicatorGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setBillableIndicatorGid(GLogXMLGidType value) {
        this.billableIndicatorGid = value;
    }

    /**
     * Obtém o valor da propriedade driverTimeType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDriverTimeType() {
        return driverTimeType;
    }

    /**
     * Define o valor da propriedade driverTimeType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDriverTimeType(String value) {
        this.driverTimeType = value;
    }

}
