
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Device Object Association details.
 * 
 * <p>Classe Java de DeviceObjectType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="DeviceObjectType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="StartDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType"/>
 *         &lt;element name="EndDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="DataQueryTypeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="ObjectGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DeviceObjectType", propOrder = {
    "startDt",
    "endDt",
    "dataQueryTypeGid",
    "objectGid"
})
public class DeviceObjectType {

    @XmlElement(name = "StartDt", required = true)
    protected GLogDateTimeType startDt;
    @XmlElement(name = "EndDt")
    protected GLogDateTimeType endDt;
    @XmlElement(name = "DataQueryTypeGid", required = true)
    protected GLogXMLGidType dataQueryTypeGid;
    @XmlElement(name = "ObjectGid", required = true)
    protected GLogXMLGidType objectGid;

    /**
     * Obtém o valor da propriedade startDt.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getStartDt() {
        return startDt;
    }

    /**
     * Define o valor da propriedade startDt.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setStartDt(GLogDateTimeType value) {
        this.startDt = value;
    }

    /**
     * Obtém o valor da propriedade endDt.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getEndDt() {
        return endDt;
    }

    /**
     * Define o valor da propriedade endDt.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setEndDt(GLogDateTimeType value) {
        this.endDt = value;
    }

    /**
     * Obtém o valor da propriedade dataQueryTypeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getDataQueryTypeGid() {
        return dataQueryTypeGid;
    }

    /**
     * Define o valor da propriedade dataQueryTypeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setDataQueryTypeGid(GLogXMLGidType value) {
        this.dataQueryTypeGid = value;
    }

    /**
     * Obtém o valor da propriedade objectGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getObjectGid() {
        return objectGid;
    }

    /**
     * Define o valor da propriedade objectGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setObjectGid(GLogXMLGidType value) {
        this.objectGid = value;
    }

}
