
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * FinancialAmount includes a currency code and an amount of money in that currency.
 *             Note: Functional currency will only be set for a domain if and when the exchange rate gid/date is set.
 *          
 * 
 * <p>Classe Java de FinancialAmountType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="FinancialAmountType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GlobalCurrencyCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="MonetaryAmount" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="RateToBase" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FuncCurrencyCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FuncCurrencyAmount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FinancialAmountType", propOrder = {
    "globalCurrencyCode",
    "monetaryAmount",
    "rateToBase",
    "funcCurrencyCode",
    "funcCurrencyAmount"
})
public class FinancialAmountType {

    @XmlElement(name = "GlobalCurrencyCode", required = true)
    protected String globalCurrencyCode;
    @XmlElement(name = "MonetaryAmount", required = true)
    protected String monetaryAmount;
    @XmlElement(name = "RateToBase")
    protected String rateToBase;
    @XmlElement(name = "FuncCurrencyCode")
    protected String funcCurrencyCode;
    @XmlElement(name = "FuncCurrencyAmount")
    protected String funcCurrencyAmount;

    /**
     * Obtém o valor da propriedade globalCurrencyCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGlobalCurrencyCode() {
        return globalCurrencyCode;
    }

    /**
     * Define o valor da propriedade globalCurrencyCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGlobalCurrencyCode(String value) {
        this.globalCurrencyCode = value;
    }

    /**
     * Obtém o valor da propriedade monetaryAmount.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMonetaryAmount() {
        return monetaryAmount;
    }

    /**
     * Define o valor da propriedade monetaryAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMonetaryAmount(String value) {
        this.monetaryAmount = value;
    }

    /**
     * Obtém o valor da propriedade rateToBase.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRateToBase() {
        return rateToBase;
    }

    /**
     * Define o valor da propriedade rateToBase.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRateToBase(String value) {
        this.rateToBase = value;
    }

    /**
     * Obtém o valor da propriedade funcCurrencyCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFuncCurrencyCode() {
        return funcCurrencyCode;
    }

    /**
     * Define o valor da propriedade funcCurrencyCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFuncCurrencyCode(String value) {
        this.funcCurrencyCode = value;
    }

    /**
     * Obtém o valor da propriedade funcCurrencyAmount.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFuncCurrencyAmount() {
        return funcCurrencyAmount;
    }

    /**
     * Define o valor da propriedade funcCurrencyAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFuncCurrencyAmount(String value) {
        this.funcCurrencyAmount = value;
    }

}
