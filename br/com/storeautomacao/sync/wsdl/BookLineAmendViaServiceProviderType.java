
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * Specifies the set of Booking Line Amendment Changes from a Service Provider centric view. This can be sent to notify the
 *             Service Provider of the Voyage with the list of amendment changes.
 *          
 * 
 * <p>Classe Java de BookLineAmendViaServiceProviderType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="BookLineAmendViaServiceProviderType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ServiceProviderGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="BookLineAmendDetail" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}BookLineAmendDetailType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BookLineAmendViaServiceProviderType", propOrder = {
    "serviceProviderGid",
    "bookLineAmendDetail"
})
public class BookLineAmendViaServiceProviderType {

    @XmlElement(name = "ServiceProviderGid")
    protected GLogXMLGidType serviceProviderGid;
    @XmlElement(name = "BookLineAmendDetail")
    protected List<BookLineAmendDetailType> bookLineAmendDetail;

    /**
     * Obtém o valor da propriedade serviceProviderGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getServiceProviderGid() {
        return serviceProviderGid;
    }

    /**
     * Define o valor da propriedade serviceProviderGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setServiceProviderGid(GLogXMLGidType value) {
        this.serviceProviderGid = value;
    }

    /**
     * Gets the value of the bookLineAmendDetail property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the bookLineAmendDetail property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBookLineAmendDetail().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BookLineAmendDetailType }
     * 
     * 
     */
    public List<BookLineAmendDetailType> getBookLineAmendDetail() {
        if (bookLineAmendDetail == null) {
            bookLineAmendDetail = new ArrayList<BookLineAmendDetailType>();
        }
        return this.bookLineAmendDetail;
    }

}
