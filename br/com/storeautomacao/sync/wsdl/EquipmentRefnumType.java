
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 *             EquipmentRefnum is an alternate method for identifying an Equipment. It consists of a qualifier and a value.
 *          
 * 
 * <p>Classe Java de EquipmentRefnumType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="EquipmentRefnumType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="EquipmentRefnumQualifierGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="EquipmentRefnumValue" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EquipmentRefnumType", propOrder = {
    "equipmentRefnumQualifierGid",
    "equipmentRefnumValue"
})
public class EquipmentRefnumType {

    @XmlElement(name = "EquipmentRefnumQualifierGid", required = true)
    protected GLogXMLGidType equipmentRefnumQualifierGid;
    @XmlElement(name = "EquipmentRefnumValue", required = true)
    protected String equipmentRefnumValue;

    /**
     * Obtém o valor da propriedade equipmentRefnumQualifierGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getEquipmentRefnumQualifierGid() {
        return equipmentRefnumQualifierGid;
    }

    /**
     * Define o valor da propriedade equipmentRefnumQualifierGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setEquipmentRefnumQualifierGid(GLogXMLGidType value) {
        this.equipmentRefnumQualifierGid = value;
    }

    /**
     * Obtém o valor da propriedade equipmentRefnumValue.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEquipmentRefnumValue() {
        return equipmentRefnumValue;
    }

    /**
     * Define o valor da propriedade equipmentRefnumValue.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEquipmentRefnumValue(String value) {
        this.equipmentRefnumValue = value;
    }

}
