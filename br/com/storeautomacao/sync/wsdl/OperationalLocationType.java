
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * Specifies a sub location, such as terminals at a port. When the element is specified in the Location, it indicates that the
 *             Location is a sub location of another Location.
 *          
 * 
 * <p>Classe Java de OperationalLocationType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="OperationalLocationType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ParentLocationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocGidType"/>
 *         &lt;element name="OperLocDetail" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OperLocDetailType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OperationalLocationType", propOrder = {
    "parentLocationGid",
    "operLocDetail"
})
public class OperationalLocationType {

    @XmlElement(name = "ParentLocationGid", required = true)
    protected GLogXMLLocGidType parentLocationGid;
    @XmlElement(name = "OperLocDetail")
    protected List<OperLocDetailType> operLocDetail;

    /**
     * Obtém o valor da propriedade parentLocationGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocGidType }
     *     
     */
    public GLogXMLLocGidType getParentLocationGid() {
        return parentLocationGid;
    }

    /**
     * Define o valor da propriedade parentLocationGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocGidType }
     *     
     */
    public void setParentLocationGid(GLogXMLLocGidType value) {
        this.parentLocationGid = value;
    }

    /**
     * Gets the value of the operLocDetail property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the operLocDetail property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOperLocDetail().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OperLocDetailType }
     * 
     * 
     */
    public List<OperLocDetailType> getOperLocDetail() {
        if (operLocDetail == null) {
            operLocDetail = new ArrayList<OperLocDetailType>();
        }
        return this.operLocDetail;
    }

}
