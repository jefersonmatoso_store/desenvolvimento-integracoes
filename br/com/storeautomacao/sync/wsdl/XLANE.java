
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Classe Java de anonymous complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="X_LANE_ROW" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="X_LANE_GID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="X_LANE_XID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="SOURCE_LOCATION_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="SOURCE_CITY" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="SOURCE_PROVINCE_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="SOURCE_POSTAL_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="SOURCE_COUNTRY_CODE3_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="SOURCE_ZONE1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="SOURCE_ZONE2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="SOURCE_ZONE3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="SOURCE_ZONE4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="SOURCE_GEO_HIERARCHY_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="DEST_LOCATION_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="DEST_CITY" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="DEST_PROVINCE_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="DEST_POSTAL_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="DEST_COUNTRY_CODE3_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="DEST_ZONE1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="DEST_ZONE2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="DEST_ZONE3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="DEST_ZONE4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="DEST_GEO_HIERARCHY_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="SOURCE_REGION_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="DEST_REGION_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="CONSTRAINT_SET_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="PLANNING_PARAMETER_SET_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="SOURCE_RAIL_SPLC_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="DEST_RAIL_SPLC_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="SOURCE_RAIL_STATION_CODE_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="DEST_RAIL_STATION_CODE_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                 &lt;/sequence>
 *                 &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" />
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "xlanerow"
})
@XmlRootElement(name = "X_LANE")
public class XLANE {

    @XmlElement(name = "X_LANE_ROW")
    protected List<XLANE.XLANEROW> xlanerow;

    /**
     * Gets the value of the xlanerow property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the xlanerow property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getXLANEROW().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link XLANE.XLANEROW }
     * 
     * 
     */
    public List<XLANE.XLANEROW> getXLANEROW() {
        if (xlanerow == null) {
            xlanerow = new ArrayList<XLANE.XLANEROW>();
        }
        return this.xlanerow;
    }


    /**
     * <p>Classe Java de anonymous complex type.
     * 
     * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="X_LANE_GID" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="X_LANE_XID" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="SOURCE_LOCATION_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="SOURCE_CITY" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="SOURCE_PROVINCE_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="SOURCE_POSTAL_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="SOURCE_COUNTRY_CODE3_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="SOURCE_ZONE1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="SOURCE_ZONE2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="SOURCE_ZONE3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="SOURCE_ZONE4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="SOURCE_GEO_HIERARCHY_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="DEST_LOCATION_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="DEST_CITY" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="DEST_PROVINCE_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="DEST_POSTAL_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="DEST_COUNTRY_CODE3_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="DEST_ZONE1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="DEST_ZONE2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="DEST_ZONE3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="DEST_ZONE4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="DEST_GEO_HIERARCHY_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="SOURCE_REGION_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="DEST_REGION_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="CONSTRAINT_SET_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="PLANNING_PARAMETER_SET_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="SOURCE_RAIL_SPLC_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="DEST_RAIL_SPLC_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="SOURCE_RAIL_STATION_CODE_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="DEST_RAIL_STATION_CODE_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *       &lt;/sequence>
     *       &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" />
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "xlanegid",
        "xlanexid",
        "sourcelocationgid",
        "sourcecity",
        "sourceprovincecode",
        "sourcepostalcode",
        "sourcecountrycode3GID",
        "sourcezone1",
        "sourcezone2",
        "sourcezone3",
        "sourcezone4",
        "sourcegeohierarchygid",
        "destlocationgid",
        "destcity",
        "destprovincecode",
        "destpostalcode",
        "destcountrycode3GID",
        "destzone1",
        "destzone2",
        "destzone3",
        "destzone4",
        "destgeohierarchygid",
        "sourceregiongid",
        "destregiongid",
        "constraintsetgid",
        "planningparametersetgid",
        "sourcerailsplcgid",
        "destrailsplcgid",
        "sourcerailstationcodegid",
        "destrailstationcodegid",
        "domainname",
        "insertuser",
        "insertdate",
        "updateuser",
        "updatedate"
    })
    public static class XLANEROW {

        @XmlElement(name = "X_LANE_GID", required = true)
        protected String xlanegid;
        @XmlElement(name = "X_LANE_XID", required = true)
        protected String xlanexid;
        @XmlElement(name = "SOURCE_LOCATION_GID")
        protected String sourcelocationgid;
        @XmlElement(name = "SOURCE_CITY")
        protected String sourcecity;
        @XmlElement(name = "SOURCE_PROVINCE_CODE")
        protected String sourceprovincecode;
        @XmlElement(name = "SOURCE_POSTAL_CODE")
        protected String sourcepostalcode;
        @XmlElement(name = "SOURCE_COUNTRY_CODE3_GID")
        protected String sourcecountrycode3GID;
        @XmlElement(name = "SOURCE_ZONE1")
        protected String sourcezone1;
        @XmlElement(name = "SOURCE_ZONE2")
        protected String sourcezone2;
        @XmlElement(name = "SOURCE_ZONE3")
        protected String sourcezone3;
        @XmlElement(name = "SOURCE_ZONE4")
        protected String sourcezone4;
        @XmlElement(name = "SOURCE_GEO_HIERARCHY_GID")
        protected String sourcegeohierarchygid;
        @XmlElement(name = "DEST_LOCATION_GID")
        protected String destlocationgid;
        @XmlElement(name = "DEST_CITY")
        protected String destcity;
        @XmlElement(name = "DEST_PROVINCE_CODE")
        protected String destprovincecode;
        @XmlElement(name = "DEST_POSTAL_CODE")
        protected String destpostalcode;
        @XmlElement(name = "DEST_COUNTRY_CODE3_GID")
        protected String destcountrycode3GID;
        @XmlElement(name = "DEST_ZONE1")
        protected String destzone1;
        @XmlElement(name = "DEST_ZONE2")
        protected String destzone2;
        @XmlElement(name = "DEST_ZONE3")
        protected String destzone3;
        @XmlElement(name = "DEST_ZONE4")
        protected String destzone4;
        @XmlElement(name = "DEST_GEO_HIERARCHY_GID")
        protected String destgeohierarchygid;
        @XmlElement(name = "SOURCE_REGION_GID")
        protected String sourceregiongid;
        @XmlElement(name = "DEST_REGION_GID")
        protected String destregiongid;
        @XmlElement(name = "CONSTRAINT_SET_GID")
        protected String constraintsetgid;
        @XmlElement(name = "PLANNING_PARAMETER_SET_GID")
        protected String planningparametersetgid;
        @XmlElement(name = "SOURCE_RAIL_SPLC_GID")
        protected String sourcerailsplcgid;
        @XmlElement(name = "DEST_RAIL_SPLC_GID")
        protected String destrailsplcgid;
        @XmlElement(name = "SOURCE_RAIL_STATION_CODE_GID")
        protected String sourcerailstationcodegid;
        @XmlElement(name = "DEST_RAIL_STATION_CODE_GID")
        protected String destrailstationcodegid;
        @XmlElement(name = "DOMAIN_NAME", required = true)
        protected String domainname;
        @XmlElement(name = "INSERT_USER", required = true)
        protected String insertuser;
        @XmlElement(name = "INSERT_DATE", required = true)
        protected String insertdate;
        @XmlElement(name = "UPDATE_USER")
        protected String updateuser;
        @XmlElement(name = "UPDATE_DATE")
        protected String updatedate;
        @XmlAttribute(name = "num")
        protected String num;

        /**
         * Obtém o valor da propriedade xlanegid.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getXLANEGID() {
            return xlanegid;
        }

        /**
         * Define o valor da propriedade xlanegid.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setXLANEGID(String value) {
            this.xlanegid = value;
        }

        /**
         * Obtém o valor da propriedade xlanexid.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getXLANEXID() {
            return xlanexid;
        }

        /**
         * Define o valor da propriedade xlanexid.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setXLANEXID(String value) {
            this.xlanexid = value;
        }

        /**
         * Obtém o valor da propriedade sourcelocationgid.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSOURCELOCATIONGID() {
            return sourcelocationgid;
        }

        /**
         * Define o valor da propriedade sourcelocationgid.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSOURCELOCATIONGID(String value) {
            this.sourcelocationgid = value;
        }

        /**
         * Obtém o valor da propriedade sourcecity.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSOURCECITY() {
            return sourcecity;
        }

        /**
         * Define o valor da propriedade sourcecity.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSOURCECITY(String value) {
            this.sourcecity = value;
        }

        /**
         * Obtém o valor da propriedade sourceprovincecode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSOURCEPROVINCECODE() {
            return sourceprovincecode;
        }

        /**
         * Define o valor da propriedade sourceprovincecode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSOURCEPROVINCECODE(String value) {
            this.sourceprovincecode = value;
        }

        /**
         * Obtém o valor da propriedade sourcepostalcode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSOURCEPOSTALCODE() {
            return sourcepostalcode;
        }

        /**
         * Define o valor da propriedade sourcepostalcode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSOURCEPOSTALCODE(String value) {
            this.sourcepostalcode = value;
        }

        /**
         * Obtém o valor da propriedade sourcecountrycode3GID.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSOURCECOUNTRYCODE3GID() {
            return sourcecountrycode3GID;
        }

        /**
         * Define o valor da propriedade sourcecountrycode3GID.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSOURCECOUNTRYCODE3GID(String value) {
            this.sourcecountrycode3GID = value;
        }

        /**
         * Obtém o valor da propriedade sourcezone1.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSOURCEZONE1() {
            return sourcezone1;
        }

        /**
         * Define o valor da propriedade sourcezone1.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSOURCEZONE1(String value) {
            this.sourcezone1 = value;
        }

        /**
         * Obtém o valor da propriedade sourcezone2.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSOURCEZONE2() {
            return sourcezone2;
        }

        /**
         * Define o valor da propriedade sourcezone2.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSOURCEZONE2(String value) {
            this.sourcezone2 = value;
        }

        /**
         * Obtém o valor da propriedade sourcezone3.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSOURCEZONE3() {
            return sourcezone3;
        }

        /**
         * Define o valor da propriedade sourcezone3.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSOURCEZONE3(String value) {
            this.sourcezone3 = value;
        }

        /**
         * Obtém o valor da propriedade sourcezone4.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSOURCEZONE4() {
            return sourcezone4;
        }

        /**
         * Define o valor da propriedade sourcezone4.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSOURCEZONE4(String value) {
            this.sourcezone4 = value;
        }

        /**
         * Obtém o valor da propriedade sourcegeohierarchygid.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSOURCEGEOHIERARCHYGID() {
            return sourcegeohierarchygid;
        }

        /**
         * Define o valor da propriedade sourcegeohierarchygid.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSOURCEGEOHIERARCHYGID(String value) {
            this.sourcegeohierarchygid = value;
        }

        /**
         * Obtém o valor da propriedade destlocationgid.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDESTLOCATIONGID() {
            return destlocationgid;
        }

        /**
         * Define o valor da propriedade destlocationgid.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDESTLOCATIONGID(String value) {
            this.destlocationgid = value;
        }

        /**
         * Obtém o valor da propriedade destcity.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDESTCITY() {
            return destcity;
        }

        /**
         * Define o valor da propriedade destcity.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDESTCITY(String value) {
            this.destcity = value;
        }

        /**
         * Obtém o valor da propriedade destprovincecode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDESTPROVINCECODE() {
            return destprovincecode;
        }

        /**
         * Define o valor da propriedade destprovincecode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDESTPROVINCECODE(String value) {
            this.destprovincecode = value;
        }

        /**
         * Obtém o valor da propriedade destpostalcode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDESTPOSTALCODE() {
            return destpostalcode;
        }

        /**
         * Define o valor da propriedade destpostalcode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDESTPOSTALCODE(String value) {
            this.destpostalcode = value;
        }

        /**
         * Obtém o valor da propriedade destcountrycode3GID.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDESTCOUNTRYCODE3GID() {
            return destcountrycode3GID;
        }

        /**
         * Define o valor da propriedade destcountrycode3GID.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDESTCOUNTRYCODE3GID(String value) {
            this.destcountrycode3GID = value;
        }

        /**
         * Obtém o valor da propriedade destzone1.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDESTZONE1() {
            return destzone1;
        }

        /**
         * Define o valor da propriedade destzone1.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDESTZONE1(String value) {
            this.destzone1 = value;
        }

        /**
         * Obtém o valor da propriedade destzone2.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDESTZONE2() {
            return destzone2;
        }

        /**
         * Define o valor da propriedade destzone2.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDESTZONE2(String value) {
            this.destzone2 = value;
        }

        /**
         * Obtém o valor da propriedade destzone3.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDESTZONE3() {
            return destzone3;
        }

        /**
         * Define o valor da propriedade destzone3.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDESTZONE3(String value) {
            this.destzone3 = value;
        }

        /**
         * Obtém o valor da propriedade destzone4.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDESTZONE4() {
            return destzone4;
        }

        /**
         * Define o valor da propriedade destzone4.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDESTZONE4(String value) {
            this.destzone4 = value;
        }

        /**
         * Obtém o valor da propriedade destgeohierarchygid.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDESTGEOHIERARCHYGID() {
            return destgeohierarchygid;
        }

        /**
         * Define o valor da propriedade destgeohierarchygid.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDESTGEOHIERARCHYGID(String value) {
            this.destgeohierarchygid = value;
        }

        /**
         * Obtém o valor da propriedade sourceregiongid.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSOURCEREGIONGID() {
            return sourceregiongid;
        }

        /**
         * Define o valor da propriedade sourceregiongid.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSOURCEREGIONGID(String value) {
            this.sourceregiongid = value;
        }

        /**
         * Obtém o valor da propriedade destregiongid.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDESTREGIONGID() {
            return destregiongid;
        }

        /**
         * Define o valor da propriedade destregiongid.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDESTREGIONGID(String value) {
            this.destregiongid = value;
        }

        /**
         * Obtém o valor da propriedade constraintsetgid.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCONSTRAINTSETGID() {
            return constraintsetgid;
        }

        /**
         * Define o valor da propriedade constraintsetgid.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCONSTRAINTSETGID(String value) {
            this.constraintsetgid = value;
        }

        /**
         * Obtém o valor da propriedade planningparametersetgid.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPLANNINGPARAMETERSETGID() {
            return planningparametersetgid;
        }

        /**
         * Define o valor da propriedade planningparametersetgid.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPLANNINGPARAMETERSETGID(String value) {
            this.planningparametersetgid = value;
        }

        /**
         * Obtém o valor da propriedade sourcerailsplcgid.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSOURCERAILSPLCGID() {
            return sourcerailsplcgid;
        }

        /**
         * Define o valor da propriedade sourcerailsplcgid.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSOURCERAILSPLCGID(String value) {
            this.sourcerailsplcgid = value;
        }

        /**
         * Obtém o valor da propriedade destrailsplcgid.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDESTRAILSPLCGID() {
            return destrailsplcgid;
        }

        /**
         * Define o valor da propriedade destrailsplcgid.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDESTRAILSPLCGID(String value) {
            this.destrailsplcgid = value;
        }

        /**
         * Obtém o valor da propriedade sourcerailstationcodegid.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSOURCERAILSTATIONCODEGID() {
            return sourcerailstationcodegid;
        }

        /**
         * Define o valor da propriedade sourcerailstationcodegid.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSOURCERAILSTATIONCODEGID(String value) {
            this.sourcerailstationcodegid = value;
        }

        /**
         * Obtém o valor da propriedade destrailstationcodegid.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDESTRAILSTATIONCODEGID() {
            return destrailstationcodegid;
        }

        /**
         * Define o valor da propriedade destrailstationcodegid.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDESTRAILSTATIONCODEGID(String value) {
            this.destrailstationcodegid = value;
        }

        /**
         * Obtém o valor da propriedade domainname.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDOMAINNAME() {
            return domainname;
        }

        /**
         * Define o valor da propriedade domainname.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDOMAINNAME(String value) {
            this.domainname = value;
        }

        /**
         * Obtém o valor da propriedade insertuser.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getINSERTUSER() {
            return insertuser;
        }

        /**
         * Define o valor da propriedade insertuser.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setINSERTUSER(String value) {
            this.insertuser = value;
        }

        /**
         * Obtém o valor da propriedade insertdate.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getINSERTDATE() {
            return insertdate;
        }

        /**
         * Define o valor da propriedade insertdate.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setINSERTDATE(String value) {
            this.insertdate = value;
        }

        /**
         * Obtém o valor da propriedade updateuser.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getUPDATEUSER() {
            return updateuser;
        }

        /**
         * Define o valor da propriedade updateuser.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setUPDATEUSER(String value) {
            this.updateuser = value;
        }

        /**
         * Obtém o valor da propriedade updatedate.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getUPDATEDATE() {
            return updatedate;
        }

        /**
         * Define o valor da propriedade updatedate.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setUPDATEDATE(String value) {
            this.updatedate = value;
        }

        /**
         * Obtém o valor da propriedade num.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNum() {
            return num;
        }

        /**
         * Define o valor da propriedade num.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNum(String value) {
            this.num = value;
        }

    }

}
