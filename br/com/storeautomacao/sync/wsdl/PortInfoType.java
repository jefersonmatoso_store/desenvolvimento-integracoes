
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * (Both) Details of the port through which the business transaction is carried. 
 *          
 * 
 * <p>Classe Java de PortInfoType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="PortInfoType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PortQualifier" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="PortId" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="PortLocation" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LocationRefType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PortInfoType", namespace = "http://xmlns.oracle.com/apps/gtm/transmission/v6.4", propOrder = {
    "portQualifier",
    "portId",
    "portLocation"
})
public class PortInfoType {

    @XmlElement(name = "PortQualifier", required = true)
    protected GLogXMLGidType portQualifier;
    @XmlElement(name = "PortId", required = true)
    protected GLogXMLGidType portId;
    @XmlElement(name = "PortLocation")
    protected LocationRefType portLocation;

    /**
     * Obtém o valor da propriedade portQualifier.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getPortQualifier() {
        return portQualifier;
    }

    /**
     * Define o valor da propriedade portQualifier.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setPortQualifier(GLogXMLGidType value) {
        this.portQualifier = value;
    }

    /**
     * Obtém o valor da propriedade portId.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getPortId() {
        return portId;
    }

    /**
     * Define o valor da propriedade portId.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setPortId(GLogXMLGidType value) {
        this.portId = value;
    }

    /**
     * Obtém o valor da propriedade portLocation.
     * 
     * @return
     *     possible object is
     *     {@link LocationRefType }
     *     
     */
    public LocationRefType getPortLocation() {
        return portLocation;
    }

    /**
     * Define o valor da propriedade portLocation.
     * 
     * @param value
     *     allowed object is
     *     {@link LocationRefType }
     *     
     */
    public void setPortLocation(LocationRefType value) {
        this.portLocation = value;
    }

}
