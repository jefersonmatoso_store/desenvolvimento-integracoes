
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;


/**
 * 
 *             A Location is a place where transportation related activities occur, such as loading and unloading freight.
 *             In addition, a location may represent a corporation, and/or a service provider.
 * 
 *             Note: The ChildOpreationalLocations element is supported on the outbound only.
 *          
 * 
 * <p>Classe Java de LocationType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="LocationType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OTMTransactionInOut">
 *       &lt;sequence>
 *         &lt;element name="SendReason" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}SendReasonType" minOccurs="0"/>
 *         &lt;element name="TransactionCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransactionCodeType"/>
 *         &lt;element name="ReplaceChildren" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ReplaceChildrenType" minOccurs="0"/>
 *         &lt;element name="IntSavedQuery" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}IntSavedQueryType" minOccurs="0"/>
 *         &lt;element name="LocationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="LocationName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsTemplate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Address" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}AddressType"/>
 *         &lt;element name="PostalSPLCGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="RailSPLCGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="RailStationCodeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="RailJunctionCodeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="ERPCGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="ServingServprovProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="RegionRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RegionRefType" minOccurs="0"/>
 *         &lt;element name="LocationGroupGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="LocationRefnum" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LocationRefnumType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Remark" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RemarkType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="LocationSpecialService" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}SpecialServiceType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="IsTemporary" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;choice minOccurs="0">
 *           &lt;element name="ContactGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" maxOccurs="unbounded" minOccurs="0"/>
 *           &lt;element name="Contact" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ContactType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;/choice>
 *         &lt;element name="LocationRole" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LocationRoleType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="AccessorialCodeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="IsMakeAppointmentBeforePlan" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Corporation" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}CorporationType" minOccurs="0"/>
 *         &lt;element name="ServiceProvider" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ServiceProviderType" minOccurs="0"/>
 *         &lt;element name="RateClassificationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="IsShipperKnown" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SubstituteLocation" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocRefType" minOccurs="0"/>
 *         &lt;element name="ServiceProviderProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="EquipmentGroupProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="IsAddressValid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NumRowsInYard" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NumSlotsPerRowInYard" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsLTLSplitable" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ApptObjectType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ApptActivityType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ApptSearchDays" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ApptShowNumOfOptions" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="StandingApptCutoffWindow" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SlotTimeInterval" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDurationType" minOccurs="0"/>
 *         &lt;element name="ExcludeFromRouteExec" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="UseApptPriority" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SchedLowPriorityAppt" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EnforceTimeWindowAppt" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SchedInfeasibleAppt" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AppointDisplayStartTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AppointDisplayDuration" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDurationType" minOccurs="0"/>
 *         &lt;element name="PickupRoutingSeqGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="DropoffRoutingSeqGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="OperationalLocation" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OperationalLocationType" minOccurs="0"/>
 *         &lt;element name="ChildOperationalLocations" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Location" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LocationType" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="LoadUnloadPoint" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="LoadUnloadPointName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="EquipmentGroupProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *                   &lt;element name="IsLoad" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="LoadSequence" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="IsUnload" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="UnloadSequence" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="LocationCapacityGroupInfo" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="LocationCapacityGroupGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *                   &lt;element name="EffectiveDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType"/>
 *                   &lt;element name="ExpirationDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="LocationResourceType" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LocationResourceTypeType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Status" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}StatusType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="LocationServProvPref" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LocationServProvPrefType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="AssetParentLocation" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}AssetParentLocationType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="AllowDriverRest" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsFixedAddress" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PrimaryAddressLineSeq" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FlexFieldStrings" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldStringType" minOccurs="0"/>
 *         &lt;element name="FlexFieldNumbers" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldNumberType" minOccurs="0"/>
 *         &lt;element name="FlexFieldDates" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldDateType" minOccurs="0"/>
 *         &lt;element name="IsActive" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LocationType", propOrder = {
    "sendReason",
    "transactionCode",
    "replaceChildren",
    "intSavedQuery",
    "locationGid",
    "locationName",
    "description",
    "isTemplate",
    "address",
    "postalSPLCGid",
    "railSPLCGid",
    "railStationCodeGid",
    "railJunctionCodeGid",
    "erpcGid",
    "servingServprovProfileGid",
    "regionRef",
    "locationGroupGid",
    "locationRefnum",
    "remark",
    "locationSpecialService",
    "isTemporary",
    "contactGid",
    "contact",
    "locationRole",
    "accessorialCodeGid",
    "isMakeAppointmentBeforePlan",
    "corporation",
    "serviceProvider",
    "rateClassificationGid",
    "isShipperKnown",
    "substituteLocation",
    "serviceProviderProfileGid",
    "equipmentGroupProfileGid",
    "isAddressValid",
    "numRowsInYard",
    "numSlotsPerRowInYard",
    "isLTLSplitable",
    "apptObjectType",
    "apptActivityType",
    "apptSearchDays",
    "apptShowNumOfOptions",
    "standingApptCutoffWindow",
    "slotTimeInterval",
    "excludeFromRouteExec",
    "useApptPriority",
    "schedLowPriorityAppt",
    "enforceTimeWindowAppt",
    "schedInfeasibleAppt",
    "appointDisplayStartTime",
    "appointDisplayDuration",
    "pickupRoutingSeqGid",
    "dropoffRoutingSeqGid",
    "operationalLocation",
    "childOperationalLocations",
    "loadUnloadPoint",
    "locationCapacityGroupInfo",
    "locationResourceType",
    "status",
    "locationServProvPref",
    "assetParentLocation",
    "allowDriverRest",
    "isFixedAddress",
    "primaryAddressLineSeq",
    "flexFieldStrings",
    "flexFieldNumbers",
    "flexFieldDates",
    "isActive"
})
public class LocationType
    extends OTMTransactionInOut
{

    @XmlElement(name = "SendReason")
    protected SendReasonType sendReason;
    @XmlElement(name = "TransactionCode", required = true)
    @XmlSchemaType(name = "string")
    protected TransactionCodeType transactionCode;
    @XmlElement(name = "ReplaceChildren")
    protected ReplaceChildrenType replaceChildren;
    @XmlElement(name = "IntSavedQuery")
    protected IntSavedQueryType intSavedQuery;
    @XmlElement(name = "LocationGid", required = true)
    protected GLogXMLGidType locationGid;
    @XmlElement(name = "LocationName")
    protected String locationName;
    @XmlElement(name = "Description")
    protected String description;
    @XmlElement(name = "IsTemplate")
    protected String isTemplate;
    @XmlElement(name = "Address", required = true)
    protected AddressType address;
    @XmlElement(name = "PostalSPLCGid")
    protected GLogXMLGidType postalSPLCGid;
    @XmlElement(name = "RailSPLCGid")
    protected GLogXMLGidType railSPLCGid;
    @XmlElement(name = "RailStationCodeGid")
    protected GLogXMLGidType railStationCodeGid;
    @XmlElement(name = "RailJunctionCodeGid")
    protected GLogXMLGidType railJunctionCodeGid;
    @XmlElement(name = "ERPCGid")
    protected GLogXMLGidType erpcGid;
    @XmlElement(name = "ServingServprovProfileGid")
    protected GLogXMLGidType servingServprovProfileGid;
    @XmlElement(name = "RegionRef")
    protected RegionRefType regionRef;
    @XmlElement(name = "LocationGroupGid")
    protected GLogXMLGidType locationGroupGid;
    @XmlElement(name = "LocationRefnum")
    protected List<LocationRefnumType> locationRefnum;
    @XmlElement(name = "Remark")
    protected List<RemarkType> remark;
    @XmlElement(name = "LocationSpecialService")
    protected List<SpecialServiceType> locationSpecialService;
    @XmlElement(name = "IsTemporary")
    protected String isTemporary;
    @XmlElement(name = "ContactGid")
    protected List<GLogXMLGidType> contactGid;
    @XmlElement(name = "Contact")
    protected List<ContactType> contact;
    @XmlElement(name = "LocationRole")
    protected List<LocationRoleType> locationRole;
    @XmlElement(name = "AccessorialCodeGid")
    protected List<GLogXMLGidType> accessorialCodeGid;
    @XmlElement(name = "IsMakeAppointmentBeforePlan")
    protected String isMakeAppointmentBeforePlan;
    @XmlElement(name = "Corporation")
    protected CorporationType corporation;
    @XmlElement(name = "ServiceProvider")
    protected ServiceProviderType serviceProvider;
    @XmlElement(name = "RateClassificationGid")
    protected GLogXMLGidType rateClassificationGid;
    @XmlElement(name = "IsShipperKnown")
    protected String isShipperKnown;
    @XmlElement(name = "SubstituteLocation")
    protected GLogXMLLocRefType substituteLocation;
    @XmlElement(name = "ServiceProviderProfileGid")
    protected GLogXMLGidType serviceProviderProfileGid;
    @XmlElement(name = "EquipmentGroupProfileGid")
    protected GLogXMLGidType equipmentGroupProfileGid;
    @XmlElement(name = "IsAddressValid")
    protected String isAddressValid;
    @XmlElement(name = "NumRowsInYard")
    protected String numRowsInYard;
    @XmlElement(name = "NumSlotsPerRowInYard")
    protected String numSlotsPerRowInYard;
    @XmlElement(name = "IsLTLSplitable")
    protected String isLTLSplitable;
    @XmlElement(name = "ApptObjectType")
    protected String apptObjectType;
    @XmlElement(name = "ApptActivityType")
    protected String apptActivityType;
    @XmlElement(name = "ApptSearchDays")
    protected String apptSearchDays;
    @XmlElement(name = "ApptShowNumOfOptions")
    protected String apptShowNumOfOptions;
    @XmlElement(name = "StandingApptCutoffWindow")
    protected String standingApptCutoffWindow;
    @XmlElement(name = "SlotTimeInterval")
    protected GLogXMLDurationType slotTimeInterval;
    @XmlElement(name = "ExcludeFromRouteExec")
    protected String excludeFromRouteExec;
    @XmlElement(name = "UseApptPriority")
    protected String useApptPriority;
    @XmlElement(name = "SchedLowPriorityAppt")
    protected String schedLowPriorityAppt;
    @XmlElement(name = "EnforceTimeWindowAppt")
    protected String enforceTimeWindowAppt;
    @XmlElement(name = "SchedInfeasibleAppt")
    protected String schedInfeasibleAppt;
    @XmlElement(name = "AppointDisplayStartTime")
    protected String appointDisplayStartTime;
    @XmlElement(name = "AppointDisplayDuration")
    protected GLogXMLDurationType appointDisplayDuration;
    @XmlElement(name = "PickupRoutingSeqGid")
    protected GLogXMLGidType pickupRoutingSeqGid;
    @XmlElement(name = "DropoffRoutingSeqGid")
    protected GLogXMLGidType dropoffRoutingSeqGid;
    @XmlElement(name = "OperationalLocation")
    protected OperationalLocationType operationalLocation;
    @XmlElement(name = "ChildOperationalLocations")
    protected LocationType.ChildOperationalLocations childOperationalLocations;
    @XmlElement(name = "LoadUnloadPoint")
    protected List<LocationType.LoadUnloadPoint> loadUnloadPoint;
    @XmlElement(name = "LocationCapacityGroupInfo")
    protected List<LocationType.LocationCapacityGroupInfo> locationCapacityGroupInfo;
    @XmlElement(name = "LocationResourceType")
    protected List<LocationResourceTypeType> locationResourceType;
    @XmlElement(name = "Status")
    protected List<StatusType> status;
    @XmlElement(name = "LocationServProvPref")
    protected List<LocationServProvPrefType> locationServProvPref;
    @XmlElement(name = "AssetParentLocation")
    protected List<AssetParentLocationType> assetParentLocation;
    @XmlElement(name = "AllowDriverRest")
    protected String allowDriverRest;
    @XmlElement(name = "IsFixedAddress")
    protected String isFixedAddress;
    @XmlElement(name = "PrimaryAddressLineSeq")
    protected String primaryAddressLineSeq;
    @XmlElement(name = "FlexFieldStrings")
    protected FlexFieldStringType flexFieldStrings;
    @XmlElement(name = "FlexFieldNumbers")
    protected FlexFieldNumberType flexFieldNumbers;
    @XmlElement(name = "FlexFieldDates")
    protected FlexFieldDateType flexFieldDates;
    @XmlElement(name = "IsActive")
    protected String isActive;

    /**
     * Obtém o valor da propriedade sendReason.
     * 
     * @return
     *     possible object is
     *     {@link SendReasonType }
     *     
     */
    public SendReasonType getSendReason() {
        return sendReason;
    }

    /**
     * Define o valor da propriedade sendReason.
     * 
     * @param value
     *     allowed object is
     *     {@link SendReasonType }
     *     
     */
    public void setSendReason(SendReasonType value) {
        this.sendReason = value;
    }

    /**
     * Obtém o valor da propriedade transactionCode.
     * 
     * @return
     *     possible object is
     *     {@link TransactionCodeType }
     *     
     */
    public TransactionCodeType getTransactionCode() {
        return transactionCode;
    }

    /**
     * Define o valor da propriedade transactionCode.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionCodeType }
     *     
     */
    public void setTransactionCode(TransactionCodeType value) {
        this.transactionCode = value;
    }

    /**
     * Obtém o valor da propriedade replaceChildren.
     * 
     * @return
     *     possible object is
     *     {@link ReplaceChildrenType }
     *     
     */
    public ReplaceChildrenType getReplaceChildren() {
        return replaceChildren;
    }

    /**
     * Define o valor da propriedade replaceChildren.
     * 
     * @param value
     *     allowed object is
     *     {@link ReplaceChildrenType }
     *     
     */
    public void setReplaceChildren(ReplaceChildrenType value) {
        this.replaceChildren = value;
    }

    /**
     * Obtém o valor da propriedade intSavedQuery.
     * 
     * @return
     *     possible object is
     *     {@link IntSavedQueryType }
     *     
     */
    public IntSavedQueryType getIntSavedQuery() {
        return intSavedQuery;
    }

    /**
     * Define o valor da propriedade intSavedQuery.
     * 
     * @param value
     *     allowed object is
     *     {@link IntSavedQueryType }
     *     
     */
    public void setIntSavedQuery(IntSavedQueryType value) {
        this.intSavedQuery = value;
    }

    /**
     * Obtém o valor da propriedade locationGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getLocationGid() {
        return locationGid;
    }

    /**
     * Define o valor da propriedade locationGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setLocationGid(GLogXMLGidType value) {
        this.locationGid = value;
    }

    /**
     * Obtém o valor da propriedade locationName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLocationName() {
        return locationName;
    }

    /**
     * Define o valor da propriedade locationName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLocationName(String value) {
        this.locationName = value;
    }

    /**
     * Obtém o valor da propriedade description.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Define o valor da propriedade description.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Obtém o valor da propriedade isTemplate.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsTemplate() {
        return isTemplate;
    }

    /**
     * Define o valor da propriedade isTemplate.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsTemplate(String value) {
        this.isTemplate = value;
    }

    /**
     * Obtém o valor da propriedade address.
     * 
     * @return
     *     possible object is
     *     {@link AddressType }
     *     
     */
    public AddressType getAddress() {
        return address;
    }

    /**
     * Define o valor da propriedade address.
     * 
     * @param value
     *     allowed object is
     *     {@link AddressType }
     *     
     */
    public void setAddress(AddressType value) {
        this.address = value;
    }

    /**
     * Obtém o valor da propriedade postalSPLCGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getPostalSPLCGid() {
        return postalSPLCGid;
    }

    /**
     * Define o valor da propriedade postalSPLCGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setPostalSPLCGid(GLogXMLGidType value) {
        this.postalSPLCGid = value;
    }

    /**
     * Obtém o valor da propriedade railSPLCGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRailSPLCGid() {
        return railSPLCGid;
    }

    /**
     * Define o valor da propriedade railSPLCGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRailSPLCGid(GLogXMLGidType value) {
        this.railSPLCGid = value;
    }

    /**
     * Obtém o valor da propriedade railStationCodeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRailStationCodeGid() {
        return railStationCodeGid;
    }

    /**
     * Define o valor da propriedade railStationCodeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRailStationCodeGid(GLogXMLGidType value) {
        this.railStationCodeGid = value;
    }

    /**
     * Obtém o valor da propriedade railJunctionCodeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRailJunctionCodeGid() {
        return railJunctionCodeGid;
    }

    /**
     * Define o valor da propriedade railJunctionCodeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRailJunctionCodeGid(GLogXMLGidType value) {
        this.railJunctionCodeGid = value;
    }

    /**
     * Obtém o valor da propriedade erpcGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getERPCGid() {
        return erpcGid;
    }

    /**
     * Define o valor da propriedade erpcGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setERPCGid(GLogXMLGidType value) {
        this.erpcGid = value;
    }

    /**
     * Obtém o valor da propriedade servingServprovProfileGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getServingServprovProfileGid() {
        return servingServprovProfileGid;
    }

    /**
     * Define o valor da propriedade servingServprovProfileGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setServingServprovProfileGid(GLogXMLGidType value) {
        this.servingServprovProfileGid = value;
    }

    /**
     * Obtém o valor da propriedade regionRef.
     * 
     * @return
     *     possible object is
     *     {@link RegionRefType }
     *     
     */
    public RegionRefType getRegionRef() {
        return regionRef;
    }

    /**
     * Define o valor da propriedade regionRef.
     * 
     * @param value
     *     allowed object is
     *     {@link RegionRefType }
     *     
     */
    public void setRegionRef(RegionRefType value) {
        this.regionRef = value;
    }

    /**
     * Obtém o valor da propriedade locationGroupGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getLocationGroupGid() {
        return locationGroupGid;
    }

    /**
     * Define o valor da propriedade locationGroupGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setLocationGroupGid(GLogXMLGidType value) {
        this.locationGroupGid = value;
    }

    /**
     * Gets the value of the locationRefnum property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the locationRefnum property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLocationRefnum().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LocationRefnumType }
     * 
     * 
     */
    public List<LocationRefnumType> getLocationRefnum() {
        if (locationRefnum == null) {
            locationRefnum = new ArrayList<LocationRefnumType>();
        }
        return this.locationRefnum;
    }

    /**
     * Gets the value of the remark property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the remark property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRemark().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RemarkType }
     * 
     * 
     */
    public List<RemarkType> getRemark() {
        if (remark == null) {
            remark = new ArrayList<RemarkType>();
        }
        return this.remark;
    }

    /**
     * Gets the value of the locationSpecialService property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the locationSpecialService property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLocationSpecialService().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SpecialServiceType }
     * 
     * 
     */
    public List<SpecialServiceType> getLocationSpecialService() {
        if (locationSpecialService == null) {
            locationSpecialService = new ArrayList<SpecialServiceType>();
        }
        return this.locationSpecialService;
    }

    /**
     * Obtém o valor da propriedade isTemporary.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsTemporary() {
        return isTemporary;
    }

    /**
     * Define o valor da propriedade isTemporary.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsTemporary(String value) {
        this.isTemporary = value;
    }

    /**
     * Gets the value of the contactGid property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the contactGid property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getContactGid().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GLogXMLGidType }
     * 
     * 
     */
    public List<GLogXMLGidType> getContactGid() {
        if (contactGid == null) {
            contactGid = new ArrayList<GLogXMLGidType>();
        }
        return this.contactGid;
    }

    /**
     * Gets the value of the contact property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the contact property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getContact().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ContactType }
     * 
     * 
     */
    public List<ContactType> getContact() {
        if (contact == null) {
            contact = new ArrayList<ContactType>();
        }
        return this.contact;
    }

    /**
     * Gets the value of the locationRole property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the locationRole property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLocationRole().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LocationRoleType }
     * 
     * 
     */
    public List<LocationRoleType> getLocationRole() {
        if (locationRole == null) {
            locationRole = new ArrayList<LocationRoleType>();
        }
        return this.locationRole;
    }

    /**
     * Gets the value of the accessorialCodeGid property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the accessorialCodeGid property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAccessorialCodeGid().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GLogXMLGidType }
     * 
     * 
     */
    public List<GLogXMLGidType> getAccessorialCodeGid() {
        if (accessorialCodeGid == null) {
            accessorialCodeGid = new ArrayList<GLogXMLGidType>();
        }
        return this.accessorialCodeGid;
    }

    /**
     * Obtém o valor da propriedade isMakeAppointmentBeforePlan.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsMakeAppointmentBeforePlan() {
        return isMakeAppointmentBeforePlan;
    }

    /**
     * Define o valor da propriedade isMakeAppointmentBeforePlan.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsMakeAppointmentBeforePlan(String value) {
        this.isMakeAppointmentBeforePlan = value;
    }

    /**
     * Obtém o valor da propriedade corporation.
     * 
     * @return
     *     possible object is
     *     {@link CorporationType }
     *     
     */
    public CorporationType getCorporation() {
        return corporation;
    }

    /**
     * Define o valor da propriedade corporation.
     * 
     * @param value
     *     allowed object is
     *     {@link CorporationType }
     *     
     */
    public void setCorporation(CorporationType value) {
        this.corporation = value;
    }

    /**
     * Obtém o valor da propriedade serviceProvider.
     * 
     * @return
     *     possible object is
     *     {@link ServiceProviderType }
     *     
     */
    public ServiceProviderType getServiceProvider() {
        return serviceProvider;
    }

    /**
     * Define o valor da propriedade serviceProvider.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceProviderType }
     *     
     */
    public void setServiceProvider(ServiceProviderType value) {
        this.serviceProvider = value;
    }

    /**
     * Obtém o valor da propriedade rateClassificationGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRateClassificationGid() {
        return rateClassificationGid;
    }

    /**
     * Define o valor da propriedade rateClassificationGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRateClassificationGid(GLogXMLGidType value) {
        this.rateClassificationGid = value;
    }

    /**
     * Obtém o valor da propriedade isShipperKnown.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsShipperKnown() {
        return isShipperKnown;
    }

    /**
     * Define o valor da propriedade isShipperKnown.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsShipperKnown(String value) {
        this.isShipperKnown = value;
    }

    /**
     * Obtém o valor da propriedade substituteLocation.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public GLogXMLLocRefType getSubstituteLocation() {
        return substituteLocation;
    }

    /**
     * Define o valor da propriedade substituteLocation.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public void setSubstituteLocation(GLogXMLLocRefType value) {
        this.substituteLocation = value;
    }

    /**
     * Obtém o valor da propriedade serviceProviderProfileGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getServiceProviderProfileGid() {
        return serviceProviderProfileGid;
    }

    /**
     * Define o valor da propriedade serviceProviderProfileGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setServiceProviderProfileGid(GLogXMLGidType value) {
        this.serviceProviderProfileGid = value;
    }

    /**
     * Obtém o valor da propriedade equipmentGroupProfileGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getEquipmentGroupProfileGid() {
        return equipmentGroupProfileGid;
    }

    /**
     * Define o valor da propriedade equipmentGroupProfileGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setEquipmentGroupProfileGid(GLogXMLGidType value) {
        this.equipmentGroupProfileGid = value;
    }

    /**
     * Obtém o valor da propriedade isAddressValid.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsAddressValid() {
        return isAddressValid;
    }

    /**
     * Define o valor da propriedade isAddressValid.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsAddressValid(String value) {
        this.isAddressValid = value;
    }

    /**
     * Obtém o valor da propriedade numRowsInYard.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumRowsInYard() {
        return numRowsInYard;
    }

    /**
     * Define o valor da propriedade numRowsInYard.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumRowsInYard(String value) {
        this.numRowsInYard = value;
    }

    /**
     * Obtém o valor da propriedade numSlotsPerRowInYard.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumSlotsPerRowInYard() {
        return numSlotsPerRowInYard;
    }

    /**
     * Define o valor da propriedade numSlotsPerRowInYard.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumSlotsPerRowInYard(String value) {
        this.numSlotsPerRowInYard = value;
    }

    /**
     * Obtém o valor da propriedade isLTLSplitable.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsLTLSplitable() {
        return isLTLSplitable;
    }

    /**
     * Define o valor da propriedade isLTLSplitable.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsLTLSplitable(String value) {
        this.isLTLSplitable = value;
    }

    /**
     * Obtém o valor da propriedade apptObjectType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApptObjectType() {
        return apptObjectType;
    }

    /**
     * Define o valor da propriedade apptObjectType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApptObjectType(String value) {
        this.apptObjectType = value;
    }

    /**
     * Obtém o valor da propriedade apptActivityType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApptActivityType() {
        return apptActivityType;
    }

    /**
     * Define o valor da propriedade apptActivityType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApptActivityType(String value) {
        this.apptActivityType = value;
    }

    /**
     * Obtém o valor da propriedade apptSearchDays.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApptSearchDays() {
        return apptSearchDays;
    }

    /**
     * Define o valor da propriedade apptSearchDays.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApptSearchDays(String value) {
        this.apptSearchDays = value;
    }

    /**
     * Obtém o valor da propriedade apptShowNumOfOptions.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApptShowNumOfOptions() {
        return apptShowNumOfOptions;
    }

    /**
     * Define o valor da propriedade apptShowNumOfOptions.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApptShowNumOfOptions(String value) {
        this.apptShowNumOfOptions = value;
    }

    /**
     * Obtém o valor da propriedade standingApptCutoffWindow.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStandingApptCutoffWindow() {
        return standingApptCutoffWindow;
    }

    /**
     * Define o valor da propriedade standingApptCutoffWindow.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStandingApptCutoffWindow(String value) {
        this.standingApptCutoffWindow = value;
    }

    /**
     * Obtém o valor da propriedade slotTimeInterval.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public GLogXMLDurationType getSlotTimeInterval() {
        return slotTimeInterval;
    }

    /**
     * Define o valor da propriedade slotTimeInterval.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public void setSlotTimeInterval(GLogXMLDurationType value) {
        this.slotTimeInterval = value;
    }

    /**
     * Obtém o valor da propriedade excludeFromRouteExec.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExcludeFromRouteExec() {
        return excludeFromRouteExec;
    }

    /**
     * Define o valor da propriedade excludeFromRouteExec.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExcludeFromRouteExec(String value) {
        this.excludeFromRouteExec = value;
    }

    /**
     * Obtém o valor da propriedade useApptPriority.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUseApptPriority() {
        return useApptPriority;
    }

    /**
     * Define o valor da propriedade useApptPriority.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUseApptPriority(String value) {
        this.useApptPriority = value;
    }

    /**
     * Obtém o valor da propriedade schedLowPriorityAppt.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSchedLowPriorityAppt() {
        return schedLowPriorityAppt;
    }

    /**
     * Define o valor da propriedade schedLowPriorityAppt.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSchedLowPriorityAppt(String value) {
        this.schedLowPriorityAppt = value;
    }

    /**
     * Obtém o valor da propriedade enforceTimeWindowAppt.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEnforceTimeWindowAppt() {
        return enforceTimeWindowAppt;
    }

    /**
     * Define o valor da propriedade enforceTimeWindowAppt.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEnforceTimeWindowAppt(String value) {
        this.enforceTimeWindowAppt = value;
    }

    /**
     * Obtém o valor da propriedade schedInfeasibleAppt.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSchedInfeasibleAppt() {
        return schedInfeasibleAppt;
    }

    /**
     * Define o valor da propriedade schedInfeasibleAppt.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSchedInfeasibleAppt(String value) {
        this.schedInfeasibleAppt = value;
    }

    /**
     * Obtém o valor da propriedade appointDisplayStartTime.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAppointDisplayStartTime() {
        return appointDisplayStartTime;
    }

    /**
     * Define o valor da propriedade appointDisplayStartTime.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAppointDisplayStartTime(String value) {
        this.appointDisplayStartTime = value;
    }

    /**
     * Obtém o valor da propriedade appointDisplayDuration.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public GLogXMLDurationType getAppointDisplayDuration() {
        return appointDisplayDuration;
    }

    /**
     * Define o valor da propriedade appointDisplayDuration.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public void setAppointDisplayDuration(GLogXMLDurationType value) {
        this.appointDisplayDuration = value;
    }

    /**
     * Obtém o valor da propriedade pickupRoutingSeqGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getPickupRoutingSeqGid() {
        return pickupRoutingSeqGid;
    }

    /**
     * Define o valor da propriedade pickupRoutingSeqGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setPickupRoutingSeqGid(GLogXMLGidType value) {
        this.pickupRoutingSeqGid = value;
    }

    /**
     * Obtém o valor da propriedade dropoffRoutingSeqGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getDropoffRoutingSeqGid() {
        return dropoffRoutingSeqGid;
    }

    /**
     * Define o valor da propriedade dropoffRoutingSeqGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setDropoffRoutingSeqGid(GLogXMLGidType value) {
        this.dropoffRoutingSeqGid = value;
    }

    /**
     * Obtém o valor da propriedade operationalLocation.
     * 
     * @return
     *     possible object is
     *     {@link OperationalLocationType }
     *     
     */
    public OperationalLocationType getOperationalLocation() {
        return operationalLocation;
    }

    /**
     * Define o valor da propriedade operationalLocation.
     * 
     * @param value
     *     allowed object is
     *     {@link OperationalLocationType }
     *     
     */
    public void setOperationalLocation(OperationalLocationType value) {
        this.operationalLocation = value;
    }

    /**
     * Obtém o valor da propriedade childOperationalLocations.
     * 
     * @return
     *     possible object is
     *     {@link LocationType.ChildOperationalLocations }
     *     
     */
    public LocationType.ChildOperationalLocations getChildOperationalLocations() {
        return childOperationalLocations;
    }

    /**
     * Define o valor da propriedade childOperationalLocations.
     * 
     * @param value
     *     allowed object is
     *     {@link LocationType.ChildOperationalLocations }
     *     
     */
    public void setChildOperationalLocations(LocationType.ChildOperationalLocations value) {
        this.childOperationalLocations = value;
    }

    /**
     * Gets the value of the loadUnloadPoint property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the loadUnloadPoint property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLoadUnloadPoint().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LocationType.LoadUnloadPoint }
     * 
     * 
     */
    public List<LocationType.LoadUnloadPoint> getLoadUnloadPoint() {
        if (loadUnloadPoint == null) {
            loadUnloadPoint = new ArrayList<LocationType.LoadUnloadPoint>();
        }
        return this.loadUnloadPoint;
    }

    /**
     * Gets the value of the locationCapacityGroupInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the locationCapacityGroupInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLocationCapacityGroupInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LocationType.LocationCapacityGroupInfo }
     * 
     * 
     */
    public List<LocationType.LocationCapacityGroupInfo> getLocationCapacityGroupInfo() {
        if (locationCapacityGroupInfo == null) {
            locationCapacityGroupInfo = new ArrayList<LocationType.LocationCapacityGroupInfo>();
        }
        return this.locationCapacityGroupInfo;
    }

    /**
     * Gets the value of the locationResourceType property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the locationResourceType property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLocationResourceType().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LocationResourceTypeType }
     * 
     * 
     */
    public List<LocationResourceTypeType> getLocationResourceType() {
        if (locationResourceType == null) {
            locationResourceType = new ArrayList<LocationResourceTypeType>();
        }
        return this.locationResourceType;
    }

    /**
     * Gets the value of the status property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the status property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getStatus().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link StatusType }
     * 
     * 
     */
    public List<StatusType> getStatus() {
        if (status == null) {
            status = new ArrayList<StatusType>();
        }
        return this.status;
    }

    /**
     * Gets the value of the locationServProvPref property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the locationServProvPref property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLocationServProvPref().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LocationServProvPrefType }
     * 
     * 
     */
    public List<LocationServProvPrefType> getLocationServProvPref() {
        if (locationServProvPref == null) {
            locationServProvPref = new ArrayList<LocationServProvPrefType>();
        }
        return this.locationServProvPref;
    }

    /**
     * Gets the value of the assetParentLocation property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the assetParentLocation property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAssetParentLocation().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AssetParentLocationType }
     * 
     * 
     */
    public List<AssetParentLocationType> getAssetParentLocation() {
        if (assetParentLocation == null) {
            assetParentLocation = new ArrayList<AssetParentLocationType>();
        }
        return this.assetParentLocation;
    }

    /**
     * Obtém o valor da propriedade allowDriverRest.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAllowDriverRest() {
        return allowDriverRest;
    }

    /**
     * Define o valor da propriedade allowDriverRest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAllowDriverRest(String value) {
        this.allowDriverRest = value;
    }

    /**
     * Obtém o valor da propriedade isFixedAddress.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsFixedAddress() {
        return isFixedAddress;
    }

    /**
     * Define o valor da propriedade isFixedAddress.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsFixedAddress(String value) {
        this.isFixedAddress = value;
    }

    /**
     * Obtém o valor da propriedade primaryAddressLineSeq.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrimaryAddressLineSeq() {
        return primaryAddressLineSeq;
    }

    /**
     * Define o valor da propriedade primaryAddressLineSeq.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrimaryAddressLineSeq(String value) {
        this.primaryAddressLineSeq = value;
    }

    /**
     * Obtém o valor da propriedade flexFieldStrings.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldStringType }
     *     
     */
    public FlexFieldStringType getFlexFieldStrings() {
        return flexFieldStrings;
    }

    /**
     * Define o valor da propriedade flexFieldStrings.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldStringType }
     *     
     */
    public void setFlexFieldStrings(FlexFieldStringType value) {
        this.flexFieldStrings = value;
    }

    /**
     * Obtém o valor da propriedade flexFieldNumbers.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldNumberType }
     *     
     */
    public FlexFieldNumberType getFlexFieldNumbers() {
        return flexFieldNumbers;
    }

    /**
     * Define o valor da propriedade flexFieldNumbers.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldNumberType }
     *     
     */
    public void setFlexFieldNumbers(FlexFieldNumberType value) {
        this.flexFieldNumbers = value;
    }

    /**
     * Obtém o valor da propriedade flexFieldDates.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldDateType }
     *     
     */
    public FlexFieldDateType getFlexFieldDates() {
        return flexFieldDates;
    }

    /**
     * Define o valor da propriedade flexFieldDates.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldDateType }
     *     
     */
    public void setFlexFieldDates(FlexFieldDateType value) {
        this.flexFieldDates = value;
    }

    /**
     * Obtém o valor da propriedade isActive.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsActive() {
        return isActive;
    }

    /**
     * Define o valor da propriedade isActive.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsActive(String value) {
        this.isActive = value;
    }


    /**
     * <p>Classe Java de anonymous complex type.
     * 
     * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Location" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LocationType" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "location"
    })
    public static class ChildOperationalLocations {

        @XmlElement(name = "Location")
        protected List<LocationType> location;

        /**
         * Gets the value of the location property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the location property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getLocation().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link LocationType }
         * 
         * 
         */
        public List<LocationType> getLocation() {
            if (location == null) {
                location = new ArrayList<LocationType>();
            }
            return this.location;
        }

    }


    /**
     * <p>Classe Java de anonymous complex type.
     * 
     * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="LoadUnloadPointName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="EquipmentGroupProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
     *         &lt;element name="IsLoad" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="LoadSequence" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="IsUnload" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="UnloadSequence" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "loadUnloadPointName",
        "description",
        "equipmentGroupProfileGid",
        "isLoad",
        "loadSequence",
        "isUnload",
        "unloadSequence"
    })
    public static class LoadUnloadPoint {

        @XmlElement(name = "LoadUnloadPointName", required = true)
        protected String loadUnloadPointName;
        @XmlElement(name = "Description")
        protected String description;
        @XmlElement(name = "EquipmentGroupProfileGid")
        protected GLogXMLGidType equipmentGroupProfileGid;
        @XmlElement(name = "IsLoad")
        protected String isLoad;
        @XmlElement(name = "LoadSequence")
        protected String loadSequence;
        @XmlElement(name = "IsUnload")
        protected String isUnload;
        @XmlElement(name = "UnloadSequence")
        protected String unloadSequence;

        /**
         * Obtém o valor da propriedade loadUnloadPointName.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLoadUnloadPointName() {
            return loadUnloadPointName;
        }

        /**
         * Define o valor da propriedade loadUnloadPointName.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLoadUnloadPointName(String value) {
            this.loadUnloadPointName = value;
        }

        /**
         * Obtém o valor da propriedade description.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDescription() {
            return description;
        }

        /**
         * Define o valor da propriedade description.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDescription(String value) {
            this.description = value;
        }

        /**
         * Obtém o valor da propriedade equipmentGroupProfileGid.
         * 
         * @return
         *     possible object is
         *     {@link GLogXMLGidType }
         *     
         */
        public GLogXMLGidType getEquipmentGroupProfileGid() {
            return equipmentGroupProfileGid;
        }

        /**
         * Define o valor da propriedade equipmentGroupProfileGid.
         * 
         * @param value
         *     allowed object is
         *     {@link GLogXMLGidType }
         *     
         */
        public void setEquipmentGroupProfileGid(GLogXMLGidType value) {
            this.equipmentGroupProfileGid = value;
        }

        /**
         * Obtém o valor da propriedade isLoad.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getIsLoad() {
            return isLoad;
        }

        /**
         * Define o valor da propriedade isLoad.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setIsLoad(String value) {
            this.isLoad = value;
        }

        /**
         * Obtém o valor da propriedade loadSequence.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLoadSequence() {
            return loadSequence;
        }

        /**
         * Define o valor da propriedade loadSequence.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLoadSequence(String value) {
            this.loadSequence = value;
        }

        /**
         * Obtém o valor da propriedade isUnload.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getIsUnload() {
            return isUnload;
        }

        /**
         * Define o valor da propriedade isUnload.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setIsUnload(String value) {
            this.isUnload = value;
        }

        /**
         * Obtém o valor da propriedade unloadSequence.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getUnloadSequence() {
            return unloadSequence;
        }

        /**
         * Define o valor da propriedade unloadSequence.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setUnloadSequence(String value) {
            this.unloadSequence = value;
        }

    }


    /**
     * <p>Classe Java de anonymous complex type.
     * 
     * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="LocationCapacityGroupGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
     *         &lt;element name="EffectiveDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType"/>
     *         &lt;element name="ExpirationDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "locationCapacityGroupGid",
        "effectiveDate",
        "expirationDate"
    })
    public static class LocationCapacityGroupInfo {

        @XmlElement(name = "LocationCapacityGroupGid", required = true)
        protected GLogXMLGidType locationCapacityGroupGid;
        @XmlElement(name = "EffectiveDate", required = true)
        protected GLogDateTimeType effectiveDate;
        @XmlElement(name = "ExpirationDate", required = true)
        protected GLogDateTimeType expirationDate;

        /**
         * Obtém o valor da propriedade locationCapacityGroupGid.
         * 
         * @return
         *     possible object is
         *     {@link GLogXMLGidType }
         *     
         */
        public GLogXMLGidType getLocationCapacityGroupGid() {
            return locationCapacityGroupGid;
        }

        /**
         * Define o valor da propriedade locationCapacityGroupGid.
         * 
         * @param value
         *     allowed object is
         *     {@link GLogXMLGidType }
         *     
         */
        public void setLocationCapacityGroupGid(GLogXMLGidType value) {
            this.locationCapacityGroupGid = value;
        }

        /**
         * Obtém o valor da propriedade effectiveDate.
         * 
         * @return
         *     possible object is
         *     {@link GLogDateTimeType }
         *     
         */
        public GLogDateTimeType getEffectiveDate() {
            return effectiveDate;
        }

        /**
         * Define o valor da propriedade effectiveDate.
         * 
         * @param value
         *     allowed object is
         *     {@link GLogDateTimeType }
         *     
         */
        public void setEffectiveDate(GLogDateTimeType value) {
            this.effectiveDate = value;
        }

        /**
         * Obtém o valor da propriedade expirationDate.
         * 
         * @return
         *     possible object is
         *     {@link GLogDateTimeType }
         *     
         */
        public GLogDateTimeType getExpirationDate() {
            return expirationDate;
        }

        /**
         * Define o valor da propriedade expirationDate.
         * 
         * @param value
         *     allowed object is
         *     {@link GLogDateTimeType }
         *     
         */
        public void setExpirationDate(GLogDateTimeType value) {
            this.expirationDate = value;
        }

    }

}
