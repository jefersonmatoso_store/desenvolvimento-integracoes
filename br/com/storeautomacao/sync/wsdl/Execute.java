
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.*;


/**
 * <p>Classe Java de anonymous complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}Transmission"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "transmission"
})
@XmlRootElement(name = "execute", namespace = "http://xmlns.oracle.com/apps/otm/TransmissionService")
public class Execute {

    @XmlElement(name = "Transmission", required = true)
    protected Transmission transmission;

    /**
     * Obtém o valor da propriedade transmission.
     * 
     * @return
     *     possible object is
     *     {@link Transmission }
     *     
     */
    public Transmission getTransmission() {
        return transmission;
    }

    /**
     * Define o valor da propriedade transmission.
     * 
     * @param value
     *     allowed object is
     *     {@link Transmission }
     *     
     */
    public void setTransmission(Transmission value) {
        this.transmission = value;
    }

}
