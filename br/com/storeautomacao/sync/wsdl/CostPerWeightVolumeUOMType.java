
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Indicates the cost per result i.e 100 USD PER KILO
 * 
 * <p>Classe Java de CostPerWeightVolumeUOMType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="CostPerWeightVolumeUOMType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FinancialAmount" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FinancialAmountType"/>
 *         &lt;element name="WeightUOMGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="VolumeUOMGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CostPerWeightVolumeUOMType", propOrder = {
    "financialAmount",
    "weightUOMGid",
    "volumeUOMGid"
})
public class CostPerWeightVolumeUOMType {

    @XmlElement(name = "FinancialAmount", required = true)
    protected FinancialAmountType financialAmount;
    @XmlElement(name = "WeightUOMGid")
    protected GLogXMLGidType weightUOMGid;
    @XmlElement(name = "VolumeUOMGid")
    protected GLogXMLGidType volumeUOMGid;

    /**
     * Obtém o valor da propriedade financialAmount.
     * 
     * @return
     *     possible object is
     *     {@link FinancialAmountType }
     *     
     */
    public FinancialAmountType getFinancialAmount() {
        return financialAmount;
    }

    /**
     * Define o valor da propriedade financialAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link FinancialAmountType }
     *     
     */
    public void setFinancialAmount(FinancialAmountType value) {
        this.financialAmount = value;
    }

    /**
     * Obtém o valor da propriedade weightUOMGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getWeightUOMGid() {
        return weightUOMGid;
    }

    /**
     * Define o valor da propriedade weightUOMGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setWeightUOMGid(GLogXMLGidType value) {
        this.weightUOMGid = value;
    }

    /**
     * Obtém o valor da propriedade volumeUOMGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getVolumeUOMGid() {
        return volumeUOMGid;
    }

    /**
     * Define o valor da propriedade volumeUOMGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setVolumeUOMGid(GLogXMLGidType value) {
        this.volumeUOMGid = value;
    }

}
