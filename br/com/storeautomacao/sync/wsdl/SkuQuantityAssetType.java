
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.*;


/**
 * Used to keep a track of asset inventory at locations as opposed to the Packaged Item
 *             inventory.
 *          
 * 
 * <p>Classe Java de SkuQuantityAssetType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="SkuQuantityAssetType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SequenceNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TransactionCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransactionCodeType" minOccurs="0"/>
 *         &lt;element name="SkuQuantityTypeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="EquipmentGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="PowerUnitGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="DriverGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="AssocDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SkuQuantityAssetType", propOrder = {
    "sequenceNumber",
    "transactionCode",
    "skuQuantityTypeGid",
    "equipmentGid",
    "powerUnitGid",
    "driverGid",
    "assocDate"
})
public class SkuQuantityAssetType {

    @XmlElement(name = "SequenceNumber")
    protected String sequenceNumber;
    @XmlElement(name = "TransactionCode")
    @XmlSchemaType(name = "string")
    protected TransactionCodeType transactionCode;
    @XmlElement(name = "SkuQuantityTypeGid", required = true)
    protected GLogXMLGidType skuQuantityTypeGid;
    @XmlElement(name = "EquipmentGid")
    protected GLogXMLGidType equipmentGid;
    @XmlElement(name = "PowerUnitGid")
    protected GLogXMLGidType powerUnitGid;
    @XmlElement(name = "DriverGid")
    protected GLogXMLGidType driverGid;
    @XmlElement(name = "AssocDate")
    protected GLogDateTimeType assocDate;

    /**
     * Obtém o valor da propriedade sequenceNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSequenceNumber() {
        return sequenceNumber;
    }

    /**
     * Define o valor da propriedade sequenceNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSequenceNumber(String value) {
        this.sequenceNumber = value;
    }

    /**
     * Obtém o valor da propriedade transactionCode.
     * 
     * @return
     *     possible object is
     *     {@link TransactionCodeType }
     *     
     */
    public TransactionCodeType getTransactionCode() {
        return transactionCode;
    }

    /**
     * Define o valor da propriedade transactionCode.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionCodeType }
     *     
     */
    public void setTransactionCode(TransactionCodeType value) {
        this.transactionCode = value;
    }

    /**
     * Obtém o valor da propriedade skuQuantityTypeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getSkuQuantityTypeGid() {
        return skuQuantityTypeGid;
    }

    /**
     * Define o valor da propriedade skuQuantityTypeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setSkuQuantityTypeGid(GLogXMLGidType value) {
        this.skuQuantityTypeGid = value;
    }

    /**
     * Obtém o valor da propriedade equipmentGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getEquipmentGid() {
        return equipmentGid;
    }

    /**
     * Define o valor da propriedade equipmentGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setEquipmentGid(GLogXMLGidType value) {
        this.equipmentGid = value;
    }

    /**
     * Obtém o valor da propriedade powerUnitGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getPowerUnitGid() {
        return powerUnitGid;
    }

    /**
     * Define o valor da propriedade powerUnitGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setPowerUnitGid(GLogXMLGidType value) {
        this.powerUnitGid = value;
    }

    /**
     * Obtém o valor da propriedade driverGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getDriverGid() {
        return driverGid;
    }

    /**
     * Define o valor da propriedade driverGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setDriverGid(GLogXMLGidType value) {
        this.driverGid = value;
    }

    /**
     * Obtém o valor da propriedade assocDate.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getAssocDate() {
        return assocDate;
    }

    /**
     * Define o valor da propriedade assocDate.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setAssocDate(GLogDateTimeType value) {
        this.assocDate = value;
    }

}
