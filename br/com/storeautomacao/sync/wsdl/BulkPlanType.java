
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * 
 *             This is used to provide statistics about the orders that were planned and the shipments that were
 *             created during a given run of bulk plan.
 *          
 * 
 * <p>Classe Java de BulkPlanType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="BulkPlanType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OTMTransactionOut">
 *       &lt;sequence>
 *         &lt;element name="SendReason" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}SendReasonType" minOccurs="0"/>
 *         &lt;element name="BulkPlanGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="Perspective" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="QueryName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="StartTime" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="EndTime" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="NumOfOrdersSelected" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NumOfOrdersUnassigned" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NumOfOrdersPlanned" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NumOfShipmentsBuilt" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NumOrdersPlannedFailed" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NumOrdersExcluded" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PlanningParamSetGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="NumOrderMovementsSelected" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NumOrderMovementsUnassigned" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NumOrderMovementsPlanned" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NumOrderMovementsFailed" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NumOrderMovementsExcluded" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TotalCost" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/>
 *         &lt;element name="TotalWeightVolume" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLWeightVolumeType" minOccurs="0"/>
 *         &lt;element name="TotalDistance" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDistanceType" minOccurs="0"/>
 *         &lt;element name="TotalNumStops" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BulkPlanByMode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}BulkPlanByModeType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="BulkPlanPartition" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}BulkPlanPartitionType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BulkPlanType", propOrder = {
    "sendReason",
    "bulkPlanGid",
    "perspective",
    "queryName",
    "startTime",
    "endTime",
    "numOfOrdersSelected",
    "numOfOrdersUnassigned",
    "numOfOrdersPlanned",
    "numOfShipmentsBuilt",
    "numOrdersPlannedFailed",
    "numOrdersExcluded",
    "planningParamSetGid",
    "numOrderMovementsSelected",
    "numOrderMovementsUnassigned",
    "numOrderMovementsPlanned",
    "numOrderMovementsFailed",
    "numOrderMovementsExcluded",
    "totalCost",
    "totalWeightVolume",
    "totalDistance",
    "totalNumStops",
    "bulkPlanByMode",
    "bulkPlanPartition"
})
public class BulkPlanType
    extends OTMTransactionOut
{

    @XmlElement(name = "SendReason")
    protected SendReasonType sendReason;
    @XmlElement(name = "BulkPlanGid", required = true)
    protected GLogXMLGidType bulkPlanGid;
    @XmlElement(name = "Perspective")
    protected String perspective;
    @XmlElement(name = "QueryName")
    protected String queryName;
    @XmlElement(name = "StartTime")
    protected GLogDateTimeType startTime;
    @XmlElement(name = "EndTime")
    protected GLogDateTimeType endTime;
    @XmlElement(name = "NumOfOrdersSelected")
    protected String numOfOrdersSelected;
    @XmlElement(name = "NumOfOrdersUnassigned")
    protected String numOfOrdersUnassigned;
    @XmlElement(name = "NumOfOrdersPlanned")
    protected String numOfOrdersPlanned;
    @XmlElement(name = "NumOfShipmentsBuilt")
    protected String numOfShipmentsBuilt;
    @XmlElement(name = "NumOrdersPlannedFailed")
    protected String numOrdersPlannedFailed;
    @XmlElement(name = "NumOrdersExcluded")
    protected String numOrdersExcluded;
    @XmlElement(name = "PlanningParamSetGid")
    protected GLogXMLGidType planningParamSetGid;
    @XmlElement(name = "NumOrderMovementsSelected")
    protected String numOrderMovementsSelected;
    @XmlElement(name = "NumOrderMovementsUnassigned")
    protected String numOrderMovementsUnassigned;
    @XmlElement(name = "NumOrderMovementsPlanned")
    protected String numOrderMovementsPlanned;
    @XmlElement(name = "NumOrderMovementsFailed")
    protected String numOrderMovementsFailed;
    @XmlElement(name = "NumOrderMovementsExcluded")
    protected String numOrderMovementsExcluded;
    @XmlElement(name = "TotalCost")
    protected GLogXMLFinancialAmountType totalCost;
    @XmlElement(name = "TotalWeightVolume")
    protected GLogXMLWeightVolumeType totalWeightVolume;
    @XmlElement(name = "TotalDistance")
    protected GLogXMLDistanceType totalDistance;
    @XmlElement(name = "TotalNumStops")
    protected String totalNumStops;
    @XmlElement(name = "BulkPlanByMode")
    protected List<BulkPlanByModeType> bulkPlanByMode;
    @XmlElement(name = "BulkPlanPartition")
    protected List<BulkPlanPartitionType> bulkPlanPartition;

    /**
     * Obtém o valor da propriedade sendReason.
     * 
     * @return
     *     possible object is
     *     {@link SendReasonType }
     *     
     */
    public SendReasonType getSendReason() {
        return sendReason;
    }

    /**
     * Define o valor da propriedade sendReason.
     * 
     * @param value
     *     allowed object is
     *     {@link SendReasonType }
     *     
     */
    public void setSendReason(SendReasonType value) {
        this.sendReason = value;
    }

    /**
     * Obtém o valor da propriedade bulkPlanGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getBulkPlanGid() {
        return bulkPlanGid;
    }

    /**
     * Define o valor da propriedade bulkPlanGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setBulkPlanGid(GLogXMLGidType value) {
        this.bulkPlanGid = value;
    }

    /**
     * Obtém o valor da propriedade perspective.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPerspective() {
        return perspective;
    }

    /**
     * Define o valor da propriedade perspective.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPerspective(String value) {
        this.perspective = value;
    }

    /**
     * Obtém o valor da propriedade queryName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQueryName() {
        return queryName;
    }

    /**
     * Define o valor da propriedade queryName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQueryName(String value) {
        this.queryName = value;
    }

    /**
     * Obtém o valor da propriedade startTime.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getStartTime() {
        return startTime;
    }

    /**
     * Define o valor da propriedade startTime.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setStartTime(GLogDateTimeType value) {
        this.startTime = value;
    }

    /**
     * Obtém o valor da propriedade endTime.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getEndTime() {
        return endTime;
    }

    /**
     * Define o valor da propriedade endTime.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setEndTime(GLogDateTimeType value) {
        this.endTime = value;
    }

    /**
     * Obtém o valor da propriedade numOfOrdersSelected.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumOfOrdersSelected() {
        return numOfOrdersSelected;
    }

    /**
     * Define o valor da propriedade numOfOrdersSelected.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumOfOrdersSelected(String value) {
        this.numOfOrdersSelected = value;
    }

    /**
     * Obtém o valor da propriedade numOfOrdersUnassigned.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumOfOrdersUnassigned() {
        return numOfOrdersUnassigned;
    }

    /**
     * Define o valor da propriedade numOfOrdersUnassigned.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumOfOrdersUnassigned(String value) {
        this.numOfOrdersUnassigned = value;
    }

    /**
     * Obtém o valor da propriedade numOfOrdersPlanned.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumOfOrdersPlanned() {
        return numOfOrdersPlanned;
    }

    /**
     * Define o valor da propriedade numOfOrdersPlanned.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumOfOrdersPlanned(String value) {
        this.numOfOrdersPlanned = value;
    }

    /**
     * Obtém o valor da propriedade numOfShipmentsBuilt.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumOfShipmentsBuilt() {
        return numOfShipmentsBuilt;
    }

    /**
     * Define o valor da propriedade numOfShipmentsBuilt.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumOfShipmentsBuilt(String value) {
        this.numOfShipmentsBuilt = value;
    }

    /**
     * Obtém o valor da propriedade numOrdersPlannedFailed.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumOrdersPlannedFailed() {
        return numOrdersPlannedFailed;
    }

    /**
     * Define o valor da propriedade numOrdersPlannedFailed.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumOrdersPlannedFailed(String value) {
        this.numOrdersPlannedFailed = value;
    }

    /**
     * Obtém o valor da propriedade numOrdersExcluded.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumOrdersExcluded() {
        return numOrdersExcluded;
    }

    /**
     * Define o valor da propriedade numOrdersExcluded.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumOrdersExcluded(String value) {
        this.numOrdersExcluded = value;
    }

    /**
     * Obtém o valor da propriedade planningParamSetGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getPlanningParamSetGid() {
        return planningParamSetGid;
    }

    /**
     * Define o valor da propriedade planningParamSetGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setPlanningParamSetGid(GLogXMLGidType value) {
        this.planningParamSetGid = value;
    }

    /**
     * Obtém o valor da propriedade numOrderMovementsSelected.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumOrderMovementsSelected() {
        return numOrderMovementsSelected;
    }

    /**
     * Define o valor da propriedade numOrderMovementsSelected.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumOrderMovementsSelected(String value) {
        this.numOrderMovementsSelected = value;
    }

    /**
     * Obtém o valor da propriedade numOrderMovementsUnassigned.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumOrderMovementsUnassigned() {
        return numOrderMovementsUnassigned;
    }

    /**
     * Define o valor da propriedade numOrderMovementsUnassigned.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumOrderMovementsUnassigned(String value) {
        this.numOrderMovementsUnassigned = value;
    }

    /**
     * Obtém o valor da propriedade numOrderMovementsPlanned.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumOrderMovementsPlanned() {
        return numOrderMovementsPlanned;
    }

    /**
     * Define o valor da propriedade numOrderMovementsPlanned.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumOrderMovementsPlanned(String value) {
        this.numOrderMovementsPlanned = value;
    }

    /**
     * Obtém o valor da propriedade numOrderMovementsFailed.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumOrderMovementsFailed() {
        return numOrderMovementsFailed;
    }

    /**
     * Define o valor da propriedade numOrderMovementsFailed.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumOrderMovementsFailed(String value) {
        this.numOrderMovementsFailed = value;
    }

    /**
     * Obtém o valor da propriedade numOrderMovementsExcluded.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumOrderMovementsExcluded() {
        return numOrderMovementsExcluded;
    }

    /**
     * Define o valor da propriedade numOrderMovementsExcluded.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumOrderMovementsExcluded(String value) {
        this.numOrderMovementsExcluded = value;
    }

    /**
     * Obtém o valor da propriedade totalCost.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getTotalCost() {
        return totalCost;
    }

    /**
     * Define o valor da propriedade totalCost.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setTotalCost(GLogXMLFinancialAmountType value) {
        this.totalCost = value;
    }

    /**
     * Obtém o valor da propriedade totalWeightVolume.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLWeightVolumeType }
     *     
     */
    public GLogXMLWeightVolumeType getTotalWeightVolume() {
        return totalWeightVolume;
    }

    /**
     * Define o valor da propriedade totalWeightVolume.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLWeightVolumeType }
     *     
     */
    public void setTotalWeightVolume(GLogXMLWeightVolumeType value) {
        this.totalWeightVolume = value;
    }

    /**
     * Obtém o valor da propriedade totalDistance.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDistanceType }
     *     
     */
    public GLogXMLDistanceType getTotalDistance() {
        return totalDistance;
    }

    /**
     * Define o valor da propriedade totalDistance.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDistanceType }
     *     
     */
    public void setTotalDistance(GLogXMLDistanceType value) {
        this.totalDistance = value;
    }

    /**
     * Obtém o valor da propriedade totalNumStops.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTotalNumStops() {
        return totalNumStops;
    }

    /**
     * Define o valor da propriedade totalNumStops.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTotalNumStops(String value) {
        this.totalNumStops = value;
    }

    /**
     * Gets the value of the bulkPlanByMode property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the bulkPlanByMode property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBulkPlanByMode().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BulkPlanByModeType }
     * 
     * 
     */
    public List<BulkPlanByModeType> getBulkPlanByMode() {
        if (bulkPlanByMode == null) {
            bulkPlanByMode = new ArrayList<BulkPlanByModeType>();
        }
        return this.bulkPlanByMode;
    }

    /**
     * Gets the value of the bulkPlanPartition property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the bulkPlanPartition property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBulkPlanPartition().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BulkPlanPartitionType }
     * 
     * 
     */
    public List<BulkPlanPartitionType> getBulkPlanPartition() {
        if (bulkPlanPartition == null) {
            bulkPlanPartition = new ArrayList<BulkPlanPartitionType>();
        }
        return this.bulkPlanPartition;
    }

}
