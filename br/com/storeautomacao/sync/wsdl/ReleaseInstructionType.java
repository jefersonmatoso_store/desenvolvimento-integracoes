
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.*;


/**
 * ReleaseInstruction is used to identify the line item or ship unit and the quantity to
 *             release in the base order.
 *             It provides further instructions for creating order releases.
 *          
 * 
 * <p>Classe Java de ReleaseInstructionType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="ReleaseInstructionType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OTMTransactionIn">
 *       &lt;sequence>
 *         &lt;choice minOccurs="0">
 *           &lt;element name="ReleaseInstructionGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *           &lt;element name="SequenceNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;/choice>
 *         &lt;element name="TransactionCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransactionCodeType" minOccurs="0"/>
 *         &lt;element name="TransOrderGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;choice>
 *           &lt;sequence>
 *             &lt;element name="TransOrderLineGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *             &lt;element name="QuantityToRelease">
 *               &lt;complexType>
 *                 &lt;complexContent>
 *                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                     &lt;choice>
 *                       &lt;element name="Weight" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}WeightType"/>
 *                       &lt;element name="Volume" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}VolumeType"/>
 *                       &lt;element name="PackagedItemCount" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                     &lt;/choice>
 *                   &lt;/restriction>
 *                 &lt;/complexContent>
 *               &lt;/complexType>
 *             &lt;/element>
 *             &lt;element name="LineRelInstInfo" minOccurs="0">
 *               &lt;complexType>
 *                 &lt;complexContent>
 *                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                     &lt;sequence>
 *                       &lt;element name="ShipUnitCount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;/sequence>
 *                   &lt;/restriction>
 *                 &lt;/complexContent>
 *               &lt;/complexType>
 *             &lt;/element>
 *           &lt;/sequence>
 *           &lt;sequence>
 *             &lt;element name="ShipUnitGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *             &lt;element name="ShipUnitReleaseCount" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *             &lt;element name="ShipUnitRelInstInfo" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ShipUnitRelInstInfoType" minOccurs="0"/>
 *           &lt;/sequence>
 *         &lt;/choice>
 *         &lt;element name="TotalWeightVolume" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLWeightVolumeType" minOccurs="0"/>
 *         &lt;element name="TransportHandlingUnitGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="SourceLocationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="DestLocationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="TimeWindow" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TimeWindowType" minOccurs="0"/>
 *         &lt;element name="ReleaseDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="ItemTag1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ItemTag2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ItemTag3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ItemTag4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReleaseInstructionType", propOrder = {
    "releaseInstructionGid",
    "sequenceNumber",
    "transactionCode",
    "transOrderGid",
    "transOrderLineGid",
    "quantityToRelease",
    "lineRelInstInfo",
    "shipUnitGid",
    "shipUnitReleaseCount",
    "shipUnitRelInstInfo",
    "totalWeightVolume",
    "transportHandlingUnitGid",
    "sourceLocationGid",
    "destLocationGid",
    "timeWindow",
    "releaseDate",
    "itemTag1",
    "itemTag2",
    "itemTag3",
    "itemTag4"
})
public class ReleaseInstructionType
    extends OTMTransactionIn
{

    @XmlElement(name = "ReleaseInstructionGid")
    protected GLogXMLGidType releaseInstructionGid;
    @XmlElement(name = "SequenceNumber")
    protected String sequenceNumber;
    @XmlElement(name = "TransactionCode")
    @XmlSchemaType(name = "string")
    protected TransactionCodeType transactionCode;
    @XmlElement(name = "TransOrderGid", required = true)
    protected GLogXMLGidType transOrderGid;
    @XmlElement(name = "TransOrderLineGid")
    protected GLogXMLGidType transOrderLineGid;
    @XmlElement(name = "QuantityToRelease")
    protected ReleaseInstructionType.QuantityToRelease quantityToRelease;
    @XmlElement(name = "LineRelInstInfo")
    protected ReleaseInstructionType.LineRelInstInfo lineRelInstInfo;
    @XmlElement(name = "ShipUnitGid")
    protected GLogXMLGidType shipUnitGid;
    @XmlElement(name = "ShipUnitReleaseCount")
    protected String shipUnitReleaseCount;
    @XmlElement(name = "ShipUnitRelInstInfo")
    protected ShipUnitRelInstInfoType shipUnitRelInstInfo;
    @XmlElement(name = "TotalWeightVolume")
    protected GLogXMLWeightVolumeType totalWeightVolume;
    @XmlElement(name = "TransportHandlingUnitGid")
    protected GLogXMLGidType transportHandlingUnitGid;
    @XmlElement(name = "SourceLocationGid")
    protected GLogXMLGidType sourceLocationGid;
    @XmlElement(name = "DestLocationGid")
    protected GLogXMLGidType destLocationGid;
    @XmlElement(name = "TimeWindow")
    protected TimeWindowType timeWindow;
    @XmlElement(name = "ReleaseDate")
    protected GLogDateTimeType releaseDate;
    @XmlElement(name = "ItemTag1")
    protected String itemTag1;
    @XmlElement(name = "ItemTag2")
    protected String itemTag2;
    @XmlElement(name = "ItemTag3")
    protected String itemTag3;
    @XmlElement(name = "ItemTag4")
    protected String itemTag4;

    /**
     * Obtém o valor da propriedade releaseInstructionGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getReleaseInstructionGid() {
        return releaseInstructionGid;
    }

    /**
     * Define o valor da propriedade releaseInstructionGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setReleaseInstructionGid(GLogXMLGidType value) {
        this.releaseInstructionGid = value;
    }

    /**
     * Obtém o valor da propriedade sequenceNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSequenceNumber() {
        return sequenceNumber;
    }

    /**
     * Define o valor da propriedade sequenceNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSequenceNumber(String value) {
        this.sequenceNumber = value;
    }

    /**
     * Obtém o valor da propriedade transactionCode.
     * 
     * @return
     *     possible object is
     *     {@link TransactionCodeType }
     *     
     */
    public TransactionCodeType getTransactionCode() {
        return transactionCode;
    }

    /**
     * Define o valor da propriedade transactionCode.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionCodeType }
     *     
     */
    public void setTransactionCode(TransactionCodeType value) {
        this.transactionCode = value;
    }

    /**
     * Obtém o valor da propriedade transOrderGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getTransOrderGid() {
        return transOrderGid;
    }

    /**
     * Define o valor da propriedade transOrderGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setTransOrderGid(GLogXMLGidType value) {
        this.transOrderGid = value;
    }

    /**
     * Obtém o valor da propriedade transOrderLineGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getTransOrderLineGid() {
        return transOrderLineGid;
    }

    /**
     * Define o valor da propriedade transOrderLineGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setTransOrderLineGid(GLogXMLGidType value) {
        this.transOrderLineGid = value;
    }

    /**
     * Obtém o valor da propriedade quantityToRelease.
     * 
     * @return
     *     possible object is
     *     {@link ReleaseInstructionType.QuantityToRelease }
     *     
     */
    public ReleaseInstructionType.QuantityToRelease getQuantityToRelease() {
        return quantityToRelease;
    }

    /**
     * Define o valor da propriedade quantityToRelease.
     * 
     * @param value
     *     allowed object is
     *     {@link ReleaseInstructionType.QuantityToRelease }
     *     
     */
    public void setQuantityToRelease(ReleaseInstructionType.QuantityToRelease value) {
        this.quantityToRelease = value;
    }

    /**
     * Obtém o valor da propriedade lineRelInstInfo.
     * 
     * @return
     *     possible object is
     *     {@link ReleaseInstructionType.LineRelInstInfo }
     *     
     */
    public ReleaseInstructionType.LineRelInstInfo getLineRelInstInfo() {
        return lineRelInstInfo;
    }

    /**
     * Define o valor da propriedade lineRelInstInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link ReleaseInstructionType.LineRelInstInfo }
     *     
     */
    public void setLineRelInstInfo(ReleaseInstructionType.LineRelInstInfo value) {
        this.lineRelInstInfo = value;
    }

    /**
     * Obtém o valor da propriedade shipUnitGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getShipUnitGid() {
        return shipUnitGid;
    }

    /**
     * Define o valor da propriedade shipUnitGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setShipUnitGid(GLogXMLGidType value) {
        this.shipUnitGid = value;
    }

    /**
     * Obtém o valor da propriedade shipUnitReleaseCount.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipUnitReleaseCount() {
        return shipUnitReleaseCount;
    }

    /**
     * Define o valor da propriedade shipUnitReleaseCount.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipUnitReleaseCount(String value) {
        this.shipUnitReleaseCount = value;
    }

    /**
     * Obtém o valor da propriedade shipUnitRelInstInfo.
     * 
     * @return
     *     possible object is
     *     {@link ShipUnitRelInstInfoType }
     *     
     */
    public ShipUnitRelInstInfoType getShipUnitRelInstInfo() {
        return shipUnitRelInstInfo;
    }

    /**
     * Define o valor da propriedade shipUnitRelInstInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link ShipUnitRelInstInfoType }
     *     
     */
    public void setShipUnitRelInstInfo(ShipUnitRelInstInfoType value) {
        this.shipUnitRelInstInfo = value;
    }

    /**
     * Obtém o valor da propriedade totalWeightVolume.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLWeightVolumeType }
     *     
     */
    public GLogXMLWeightVolumeType getTotalWeightVolume() {
        return totalWeightVolume;
    }

    /**
     * Define o valor da propriedade totalWeightVolume.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLWeightVolumeType }
     *     
     */
    public void setTotalWeightVolume(GLogXMLWeightVolumeType value) {
        this.totalWeightVolume = value;
    }

    /**
     * Obtém o valor da propriedade transportHandlingUnitGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getTransportHandlingUnitGid() {
        return transportHandlingUnitGid;
    }

    /**
     * Define o valor da propriedade transportHandlingUnitGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setTransportHandlingUnitGid(GLogXMLGidType value) {
        this.transportHandlingUnitGid = value;
    }

    /**
     * Obtém o valor da propriedade sourceLocationGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getSourceLocationGid() {
        return sourceLocationGid;
    }

    /**
     * Define o valor da propriedade sourceLocationGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setSourceLocationGid(GLogXMLGidType value) {
        this.sourceLocationGid = value;
    }

    /**
     * Obtém o valor da propriedade destLocationGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getDestLocationGid() {
        return destLocationGid;
    }

    /**
     * Define o valor da propriedade destLocationGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setDestLocationGid(GLogXMLGidType value) {
        this.destLocationGid = value;
    }

    /**
     * Obtém o valor da propriedade timeWindow.
     * 
     * @return
     *     possible object is
     *     {@link TimeWindowType }
     *     
     */
    public TimeWindowType getTimeWindow() {
        return timeWindow;
    }

    /**
     * Define o valor da propriedade timeWindow.
     * 
     * @param value
     *     allowed object is
     *     {@link TimeWindowType }
     *     
     */
    public void setTimeWindow(TimeWindowType value) {
        this.timeWindow = value;
    }

    /**
     * Obtém o valor da propriedade releaseDate.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getReleaseDate() {
        return releaseDate;
    }

    /**
     * Define o valor da propriedade releaseDate.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setReleaseDate(GLogDateTimeType value) {
        this.releaseDate = value;
    }

    /**
     * Obtém o valor da propriedade itemTag1.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItemTag1() {
        return itemTag1;
    }

    /**
     * Define o valor da propriedade itemTag1.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItemTag1(String value) {
        this.itemTag1 = value;
    }

    /**
     * Obtém o valor da propriedade itemTag2.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItemTag2() {
        return itemTag2;
    }

    /**
     * Define o valor da propriedade itemTag2.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItemTag2(String value) {
        this.itemTag2 = value;
    }

    /**
     * Obtém o valor da propriedade itemTag3.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItemTag3() {
        return itemTag3;
    }

    /**
     * Define o valor da propriedade itemTag3.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItemTag3(String value) {
        this.itemTag3 = value;
    }

    /**
     * Obtém o valor da propriedade itemTag4.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItemTag4() {
        return itemTag4;
    }

    /**
     * Define o valor da propriedade itemTag4.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItemTag4(String value) {
        this.itemTag4 = value;
    }


    /**
     * <p>Classe Java de anonymous complex type.
     * 
     * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="ShipUnitCount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "shipUnitCount"
    })
    public static class LineRelInstInfo {

        @XmlElement(name = "ShipUnitCount")
        protected String shipUnitCount;

        /**
         * Obtém o valor da propriedade shipUnitCount.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getShipUnitCount() {
            return shipUnitCount;
        }

        /**
         * Define o valor da propriedade shipUnitCount.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setShipUnitCount(String value) {
            this.shipUnitCount = value;
        }

    }


    /**
     * <p>Classe Java de anonymous complex type.
     * 
     * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;choice>
     *         &lt;element name="Weight" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}WeightType"/>
     *         &lt;element name="Volume" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}VolumeType"/>
     *         &lt;element name="PackagedItemCount" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/choice>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "weight",
        "volume",
        "packagedItemCount"
    })
    public static class QuantityToRelease {

        @XmlElement(name = "Weight")
        protected WeightType weight;
        @XmlElement(name = "Volume")
        protected VolumeType volume;
        @XmlElement(name = "PackagedItemCount")
        protected String packagedItemCount;

        /**
         * Obtém o valor da propriedade weight.
         * 
         * @return
         *     possible object is
         *     {@link WeightType }
         *     
         */
        public WeightType getWeight() {
            return weight;
        }

        /**
         * Define o valor da propriedade weight.
         * 
         * @param value
         *     allowed object is
         *     {@link WeightType }
         *     
         */
        public void setWeight(WeightType value) {
            this.weight = value;
        }

        /**
         * Obtém o valor da propriedade volume.
         * 
         * @return
         *     possible object is
         *     {@link VolumeType }
         *     
         */
        public VolumeType getVolume() {
            return volume;
        }

        /**
         * Define o valor da propriedade volume.
         * 
         * @param value
         *     allowed object is
         *     {@link VolumeType }
         *     
         */
        public void setVolume(VolumeType value) {
            this.volume = value;
        }

        /**
         * Obtém o valor da propriedade packagedItemCount.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPackagedItemCount() {
            return packagedItemCount;
        }

        /**
         * Define o valor da propriedade packagedItemCount.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPackagedItemCount(String value) {
            this.packagedItemCount = value;
        }

    }

}
