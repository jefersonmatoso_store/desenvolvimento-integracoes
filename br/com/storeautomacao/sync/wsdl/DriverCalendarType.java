
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Defines Driver's Calendar information.
 * 
 * <p>Classe Java de DriverCalendarType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="DriverCalendarType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="HomeLocGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="ActivityCalendarGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="EffectiveDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType"/>
 *         &lt;element name="ExpirationDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DriverCalendarType", propOrder = {
    "homeLocGid",
    "activityCalendarGid",
    "effectiveDate",
    "expirationDate"
})
public class DriverCalendarType {

    @XmlElement(name = "HomeLocGid", required = true)
    protected GLogXMLGidType homeLocGid;
    @XmlElement(name = "ActivityCalendarGid", required = true)
    protected GLogXMLGidType activityCalendarGid;
    @XmlElement(name = "EffectiveDate", required = true)
    protected GLogDateTimeType effectiveDate;
    @XmlElement(name = "ExpirationDate", required = true)
    protected GLogDateTimeType expirationDate;

    /**
     * Obtém o valor da propriedade homeLocGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getHomeLocGid() {
        return homeLocGid;
    }

    /**
     * Define o valor da propriedade homeLocGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setHomeLocGid(GLogXMLGidType value) {
        this.homeLocGid = value;
    }

    /**
     * Obtém o valor da propriedade activityCalendarGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getActivityCalendarGid() {
        return activityCalendarGid;
    }

    /**
     * Define o valor da propriedade activityCalendarGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setActivityCalendarGid(GLogXMLGidType value) {
        this.activityCalendarGid = value;
    }

    /**
     * Obtém o valor da propriedade effectiveDate.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getEffectiveDate() {
        return effectiveDate;
    }

    /**
     * Define o valor da propriedade effectiveDate.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setEffectiveDate(GLogDateTimeType value) {
        this.effectiveDate = value;
    }

    /**
     * Obtém o valor da propriedade expirationDate.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getExpirationDate() {
        return expirationDate;
    }

    /**
     * Define o valor da propriedade expirationDate.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setExpirationDate(GLogDateTimeType value) {
        this.expirationDate = value;
    }

}
