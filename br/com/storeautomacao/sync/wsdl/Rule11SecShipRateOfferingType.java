
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Contains the information from the RATE_OFFERING for the second Shipment in a Rail Rule 11 move.
 * 
 * <p>Classe Java de Rule11SecShipRateOfferingType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="Rule11SecShipRateOfferingType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TariffRefnumQualifier" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TariffRefnumValue" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TariffName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TariffPubAuthority" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TariffRegAgencyCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TariffAgencyCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TariffIssuingCarrierId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Rule11SecShipRateOfferingType", propOrder = {
    "tariffRefnumQualifier",
    "tariffRefnumValue",
    "tariffName",
    "tariffPubAuthority",
    "tariffRegAgencyCode",
    "tariffAgencyCode",
    "tariffIssuingCarrierId"
})
public class Rule11SecShipRateOfferingType {

    @XmlElement(name = "TariffRefnumQualifier")
    protected String tariffRefnumQualifier;
    @XmlElement(name = "TariffRefnumValue")
    protected String tariffRefnumValue;
    @XmlElement(name = "TariffName")
    protected String tariffName;
    @XmlElement(name = "TariffPubAuthority")
    protected String tariffPubAuthority;
    @XmlElement(name = "TariffRegAgencyCode")
    protected String tariffRegAgencyCode;
    @XmlElement(name = "TariffAgencyCode")
    protected String tariffAgencyCode;
    @XmlElement(name = "TariffIssuingCarrierId")
    protected String tariffIssuingCarrierId;

    /**
     * Obtém o valor da propriedade tariffRefnumQualifier.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTariffRefnumQualifier() {
        return tariffRefnumQualifier;
    }

    /**
     * Define o valor da propriedade tariffRefnumQualifier.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTariffRefnumQualifier(String value) {
        this.tariffRefnumQualifier = value;
    }

    /**
     * Obtém o valor da propriedade tariffRefnumValue.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTariffRefnumValue() {
        return tariffRefnumValue;
    }

    /**
     * Define o valor da propriedade tariffRefnumValue.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTariffRefnumValue(String value) {
        this.tariffRefnumValue = value;
    }

    /**
     * Obtém o valor da propriedade tariffName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTariffName() {
        return tariffName;
    }

    /**
     * Define o valor da propriedade tariffName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTariffName(String value) {
        this.tariffName = value;
    }

    /**
     * Obtém o valor da propriedade tariffPubAuthority.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTariffPubAuthority() {
        return tariffPubAuthority;
    }

    /**
     * Define o valor da propriedade tariffPubAuthority.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTariffPubAuthority(String value) {
        this.tariffPubAuthority = value;
    }

    /**
     * Obtém o valor da propriedade tariffRegAgencyCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTariffRegAgencyCode() {
        return tariffRegAgencyCode;
    }

    /**
     * Define o valor da propriedade tariffRegAgencyCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTariffRegAgencyCode(String value) {
        this.tariffRegAgencyCode = value;
    }

    /**
     * Obtém o valor da propriedade tariffAgencyCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTariffAgencyCode() {
        return tariffAgencyCode;
    }

    /**
     * Define o valor da propriedade tariffAgencyCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTariffAgencyCode(String value) {
        this.tariffAgencyCode = value;
    }

    /**
     * Obtém o valor da propriedade tariffIssuingCarrierId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTariffIssuingCarrierId() {
        return tariffIssuingCarrierId;
    }

    /**
     * Define o valor da propriedade tariffIssuingCarrierId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTariffIssuingCarrierId(String value) {
        this.tariffIssuingCarrierId = value;
    }

}
