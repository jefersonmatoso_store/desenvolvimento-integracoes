
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * Specifies a SKU Event.
 * 
 * <p>Classe Java de SkuEventType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="SkuEventType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OTMTransactionInOut">
 *       &lt;sequence>
 *         &lt;choice>
 *           &lt;element name="IntSavedQuery" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}IntSavedQueryType"/>
 *           &lt;element name="SkuGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;/choice>
 *         &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="StatusCodeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="StatusReasonCodeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="EventDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="StatusGroup" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}StatusGroupType" minOccurs="0"/>
 *         &lt;element name="ReasonGroup" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ReasonGroupType" minOccurs="0"/>
 *         &lt;element name="ResponsiblePartyGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="SkuEventQuantity" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}SkuEventQuantityType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Remark" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RemarkType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SkuEventType", propOrder = {
    "intSavedQuery",
    "skuGid",
    "description",
    "statusCodeGid",
    "statusReasonCodeGid",
    "eventDt",
    "statusGroup",
    "reasonGroup",
    "responsiblePartyGid",
    "skuEventQuantity",
    "remark"
})
public class SkuEventType
    extends OTMTransactionInOut
{

    @XmlElement(name = "IntSavedQuery")
    protected IntSavedQueryType intSavedQuery;
    @XmlElement(name = "SkuGid")
    protected GLogXMLGidType skuGid;
    @XmlElement(name = "Description")
    protected String description;
    @XmlElement(name = "StatusCodeGid")
    protected GLogXMLGidType statusCodeGid;
    @XmlElement(name = "StatusReasonCodeGid")
    protected GLogXMLGidType statusReasonCodeGid;
    @XmlElement(name = "EventDt")
    protected GLogDateTimeType eventDt;
    @XmlElement(name = "StatusGroup")
    protected StatusGroupType statusGroup;
    @XmlElement(name = "ReasonGroup")
    protected ReasonGroupType reasonGroup;
    @XmlElement(name = "ResponsiblePartyGid")
    protected GLogXMLGidType responsiblePartyGid;
    @XmlElement(name = "SkuEventQuantity")
    protected List<SkuEventQuantityType> skuEventQuantity;
    @XmlElement(name = "Remark")
    protected List<RemarkType> remark;

    /**
     * Obtém o valor da propriedade intSavedQuery.
     * 
     * @return
     *     possible object is
     *     {@link IntSavedQueryType }
     *     
     */
    public IntSavedQueryType getIntSavedQuery() {
        return intSavedQuery;
    }

    /**
     * Define o valor da propriedade intSavedQuery.
     * 
     * @param value
     *     allowed object is
     *     {@link IntSavedQueryType }
     *     
     */
    public void setIntSavedQuery(IntSavedQueryType value) {
        this.intSavedQuery = value;
    }

    /**
     * Obtém o valor da propriedade skuGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getSkuGid() {
        return skuGid;
    }

    /**
     * Define o valor da propriedade skuGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setSkuGid(GLogXMLGidType value) {
        this.skuGid = value;
    }

    /**
     * Obtém o valor da propriedade description.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Define o valor da propriedade description.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Obtém o valor da propriedade statusCodeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getStatusCodeGid() {
        return statusCodeGid;
    }

    /**
     * Define o valor da propriedade statusCodeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setStatusCodeGid(GLogXMLGidType value) {
        this.statusCodeGid = value;
    }

    /**
     * Obtém o valor da propriedade statusReasonCodeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getStatusReasonCodeGid() {
        return statusReasonCodeGid;
    }

    /**
     * Define o valor da propriedade statusReasonCodeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setStatusReasonCodeGid(GLogXMLGidType value) {
        this.statusReasonCodeGid = value;
    }

    /**
     * Obtém o valor da propriedade eventDt.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getEventDt() {
        return eventDt;
    }

    /**
     * Define o valor da propriedade eventDt.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setEventDt(GLogDateTimeType value) {
        this.eventDt = value;
    }

    /**
     * Obtém o valor da propriedade statusGroup.
     * 
     * @return
     *     possible object is
     *     {@link StatusGroupType }
     *     
     */
    public StatusGroupType getStatusGroup() {
        return statusGroup;
    }

    /**
     * Define o valor da propriedade statusGroup.
     * 
     * @param value
     *     allowed object is
     *     {@link StatusGroupType }
     *     
     */
    public void setStatusGroup(StatusGroupType value) {
        this.statusGroup = value;
    }

    /**
     * Obtém o valor da propriedade reasonGroup.
     * 
     * @return
     *     possible object is
     *     {@link ReasonGroupType }
     *     
     */
    public ReasonGroupType getReasonGroup() {
        return reasonGroup;
    }

    /**
     * Define o valor da propriedade reasonGroup.
     * 
     * @param value
     *     allowed object is
     *     {@link ReasonGroupType }
     *     
     */
    public void setReasonGroup(ReasonGroupType value) {
        this.reasonGroup = value;
    }

    /**
     * Obtém o valor da propriedade responsiblePartyGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getResponsiblePartyGid() {
        return responsiblePartyGid;
    }

    /**
     * Define o valor da propriedade responsiblePartyGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setResponsiblePartyGid(GLogXMLGidType value) {
        this.responsiblePartyGid = value;
    }

    /**
     * Gets the value of the skuEventQuantity property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the skuEventQuantity property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSkuEventQuantity().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SkuEventQuantityType }
     * 
     * 
     */
    public List<SkuEventQuantityType> getSkuEventQuantity() {
        if (skuEventQuantity == null) {
            skuEventQuantity = new ArrayList<SkuEventQuantityType>();
        }
        return this.skuEventQuantity;
    }

    /**
     * Gets the value of the remark property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the remark property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRemark().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RemarkType }
     * 
     * 
     */
    public List<RemarkType> getRemark() {
        if (remark == null) {
            remark = new ArrayList<RemarkType>();
        }
        return this.remark;
    }

}
