
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * SSContact is a structure containing various contact elements, such as Contact Function Code, Contact Name, Remarks.
 * 
 * <p>Classe Java de SSContactType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="SSContactType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SSContactFunctionCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SSContactName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SSContactType", propOrder = {
    "ssContactFunctionCode",
    "ssContactName"
})
public class SSContactType {

    @XmlElement(name = "SSContactFunctionCode")
    protected String ssContactFunctionCode;
    @XmlElement(name = "SSContactName")
    protected String ssContactName;

    /**
     * Obtém o valor da propriedade ssContactFunctionCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSSContactFunctionCode() {
        return ssContactFunctionCode;
    }

    /**
     * Define o valor da propriedade ssContactFunctionCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSSContactFunctionCode(String value) {
        this.ssContactFunctionCode = value;
    }

    /**
     * Obtém o valor da propriedade ssContactName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSSContactName() {
        return ssContactName;
    }

    /**
     * Define o valor da propriedade ssContactName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSSContactName(String value) {
        this.ssContactName = value;
    }

}
