
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;


/**
 * 
 *             Shipping options.
 *          
 * 
 * <p>Classe Java de CostOptionShipType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="CostOptionShipType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IntSavedQuery" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}IntSavedQueryType" minOccurs="0"/>
 *         &lt;element name="SequenceNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TransactionCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransactionCodeType" minOccurs="0"/>
 *         &lt;element name="IsPrimary" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TotalAllocCost" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType"/>
 *         &lt;element name="StartDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="EndDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="ServiceProviderGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="TransportModeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="RateOfferingGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="RateGeoGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="FlightGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="RateFromLocGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="RateToLocGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="SrcViaLocGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="DestViaLocGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="VoyageGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="Distance" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}DistanceType" minOccurs="0"/>
 *         &lt;element name="Perspective" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ExpireDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="SrcLoc" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="QuoteLocInfo" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}QuoteLocInfoType"/>
 *                   &lt;element name="LocationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *                   &lt;element name="SourceLocationName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="DestLoc" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="QuoteLocInfo" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}QuoteLocInfoType"/>
 *                   &lt;element name="LocationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *                   &lt;element name="DestLocationName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="ShipmentTypeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="NFRCRuleGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="CostOptionShipCost" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}CostOptionShipCostType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CostOptionShipType", propOrder = {
    "intSavedQuery",
    "sequenceNumber",
    "transactionCode",
    "isPrimary",
    "totalAllocCost",
    "startDt",
    "endDt",
    "serviceProviderGid",
    "transportModeGid",
    "rateOfferingGid",
    "rateGeoGid",
    "flightGid",
    "rateFromLocGid",
    "rateToLocGid",
    "srcViaLocGid",
    "destViaLocGid",
    "voyageGid",
    "distance",
    "perspective",
    "expireDt",
    "srcLoc",
    "destLoc",
    "shipmentTypeGid",
    "nfrcRuleGid",
    "costOptionShipCost"
})
public class CostOptionShipType {

    @XmlElement(name = "IntSavedQuery")
    protected IntSavedQueryType intSavedQuery;
    @XmlElement(name = "SequenceNumber")
    protected String sequenceNumber;
    @XmlElement(name = "TransactionCode")
    @XmlSchemaType(name = "string")
    protected TransactionCodeType transactionCode;
    @XmlElement(name = "IsPrimary")
    protected String isPrimary;
    @XmlElement(name = "TotalAllocCost", required = true)
    protected GLogXMLFinancialAmountType totalAllocCost;
    @XmlElement(name = "StartDt")
    protected GLogDateTimeType startDt;
    @XmlElement(name = "EndDt")
    protected GLogDateTimeType endDt;
    @XmlElement(name = "ServiceProviderGid")
    protected GLogXMLGidType serviceProviderGid;
    @XmlElement(name = "TransportModeGid")
    protected GLogXMLGidType transportModeGid;
    @XmlElement(name = "RateOfferingGid")
    protected GLogXMLGidType rateOfferingGid;
    @XmlElement(name = "RateGeoGid")
    protected GLogXMLGidType rateGeoGid;
    @XmlElement(name = "FlightGid")
    protected GLogXMLGidType flightGid;
    @XmlElement(name = "RateFromLocGid")
    protected GLogXMLGidType rateFromLocGid;
    @XmlElement(name = "RateToLocGid")
    protected GLogXMLGidType rateToLocGid;
    @XmlElement(name = "SrcViaLocGid")
    protected GLogXMLGidType srcViaLocGid;
    @XmlElement(name = "DestViaLocGid")
    protected GLogXMLGidType destViaLocGid;
    @XmlElement(name = "VoyageGid")
    protected GLogXMLGidType voyageGid;
    @XmlElement(name = "Distance")
    protected DistanceType distance;
    @XmlElement(name = "Perspective")
    protected String perspective;
    @XmlElement(name = "ExpireDt")
    protected GLogDateTimeType expireDt;
    @XmlElement(name = "SrcLoc")
    protected CostOptionShipType.SrcLoc srcLoc;
    @XmlElement(name = "DestLoc")
    protected CostOptionShipType.DestLoc destLoc;
    @XmlElement(name = "ShipmentTypeGid")
    protected GLogXMLGidType shipmentTypeGid;
    @XmlElement(name = "NFRCRuleGid")
    protected GLogXMLGidType nfrcRuleGid;
    @XmlElement(name = "CostOptionShipCost")
    protected List<CostOptionShipCostType> costOptionShipCost;

    /**
     * Obtém o valor da propriedade intSavedQuery.
     * 
     * @return
     *     possible object is
     *     {@link IntSavedQueryType }
     *     
     */
    public IntSavedQueryType getIntSavedQuery() {
        return intSavedQuery;
    }

    /**
     * Define o valor da propriedade intSavedQuery.
     * 
     * @param value
     *     allowed object is
     *     {@link IntSavedQueryType }
     *     
     */
    public void setIntSavedQuery(IntSavedQueryType value) {
        this.intSavedQuery = value;
    }

    /**
     * Obtém o valor da propriedade sequenceNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSequenceNumber() {
        return sequenceNumber;
    }

    /**
     * Define o valor da propriedade sequenceNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSequenceNumber(String value) {
        this.sequenceNumber = value;
    }

    /**
     * Obtém o valor da propriedade transactionCode.
     * 
     * @return
     *     possible object is
     *     {@link TransactionCodeType }
     *     
     */
    public TransactionCodeType getTransactionCode() {
        return transactionCode;
    }

    /**
     * Define o valor da propriedade transactionCode.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionCodeType }
     *     
     */
    public void setTransactionCode(TransactionCodeType value) {
        this.transactionCode = value;
    }

    /**
     * Obtém o valor da propriedade isPrimary.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsPrimary() {
        return isPrimary;
    }

    /**
     * Define o valor da propriedade isPrimary.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsPrimary(String value) {
        this.isPrimary = value;
    }

    /**
     * Obtém o valor da propriedade totalAllocCost.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getTotalAllocCost() {
        return totalAllocCost;
    }

    /**
     * Define o valor da propriedade totalAllocCost.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setTotalAllocCost(GLogXMLFinancialAmountType value) {
        this.totalAllocCost = value;
    }

    /**
     * Obtém o valor da propriedade startDt.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getStartDt() {
        return startDt;
    }

    /**
     * Define o valor da propriedade startDt.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setStartDt(GLogDateTimeType value) {
        this.startDt = value;
    }

    /**
     * Obtém o valor da propriedade endDt.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getEndDt() {
        return endDt;
    }

    /**
     * Define o valor da propriedade endDt.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setEndDt(GLogDateTimeType value) {
        this.endDt = value;
    }

    /**
     * Obtém o valor da propriedade serviceProviderGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getServiceProviderGid() {
        return serviceProviderGid;
    }

    /**
     * Define o valor da propriedade serviceProviderGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setServiceProviderGid(GLogXMLGidType value) {
        this.serviceProviderGid = value;
    }

    /**
     * Obtém o valor da propriedade transportModeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getTransportModeGid() {
        return transportModeGid;
    }

    /**
     * Define o valor da propriedade transportModeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setTransportModeGid(GLogXMLGidType value) {
        this.transportModeGid = value;
    }

    /**
     * Obtém o valor da propriedade rateOfferingGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRateOfferingGid() {
        return rateOfferingGid;
    }

    /**
     * Define o valor da propriedade rateOfferingGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRateOfferingGid(GLogXMLGidType value) {
        this.rateOfferingGid = value;
    }

    /**
     * Obtém o valor da propriedade rateGeoGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRateGeoGid() {
        return rateGeoGid;
    }

    /**
     * Define o valor da propriedade rateGeoGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRateGeoGid(GLogXMLGidType value) {
        this.rateGeoGid = value;
    }

    /**
     * Obtém o valor da propriedade flightGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getFlightGid() {
        return flightGid;
    }

    /**
     * Define o valor da propriedade flightGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setFlightGid(GLogXMLGidType value) {
        this.flightGid = value;
    }

    /**
     * Obtém o valor da propriedade rateFromLocGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRateFromLocGid() {
        return rateFromLocGid;
    }

    /**
     * Define o valor da propriedade rateFromLocGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRateFromLocGid(GLogXMLGidType value) {
        this.rateFromLocGid = value;
    }

    /**
     * Obtém o valor da propriedade rateToLocGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRateToLocGid() {
        return rateToLocGid;
    }

    /**
     * Define o valor da propriedade rateToLocGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRateToLocGid(GLogXMLGidType value) {
        this.rateToLocGid = value;
    }

    /**
     * Obtém o valor da propriedade srcViaLocGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getSrcViaLocGid() {
        return srcViaLocGid;
    }

    /**
     * Define o valor da propriedade srcViaLocGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setSrcViaLocGid(GLogXMLGidType value) {
        this.srcViaLocGid = value;
    }

    /**
     * Obtém o valor da propriedade destViaLocGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getDestViaLocGid() {
        return destViaLocGid;
    }

    /**
     * Define o valor da propriedade destViaLocGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setDestViaLocGid(GLogXMLGidType value) {
        this.destViaLocGid = value;
    }

    /**
     * Obtém o valor da propriedade voyageGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getVoyageGid() {
        return voyageGid;
    }

    /**
     * Define o valor da propriedade voyageGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setVoyageGid(GLogXMLGidType value) {
        this.voyageGid = value;
    }

    /**
     * Obtém o valor da propriedade distance.
     * 
     * @return
     *     possible object is
     *     {@link DistanceType }
     *     
     */
    public DistanceType getDistance() {
        return distance;
    }

    /**
     * Define o valor da propriedade distance.
     * 
     * @param value
     *     allowed object is
     *     {@link DistanceType }
     *     
     */
    public void setDistance(DistanceType value) {
        this.distance = value;
    }

    /**
     * Obtém o valor da propriedade perspective.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPerspective() {
        return perspective;
    }

    /**
     * Define o valor da propriedade perspective.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPerspective(String value) {
        this.perspective = value;
    }

    /**
     * Obtém o valor da propriedade expireDt.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getExpireDt() {
        return expireDt;
    }

    /**
     * Define o valor da propriedade expireDt.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setExpireDt(GLogDateTimeType value) {
        this.expireDt = value;
    }

    /**
     * Obtém o valor da propriedade srcLoc.
     * 
     * @return
     *     possible object is
     *     {@link CostOptionShipType.SrcLoc }
     *     
     */
    public CostOptionShipType.SrcLoc getSrcLoc() {
        return srcLoc;
    }

    /**
     * Define o valor da propriedade srcLoc.
     * 
     * @param value
     *     allowed object is
     *     {@link CostOptionShipType.SrcLoc }
     *     
     */
    public void setSrcLoc(CostOptionShipType.SrcLoc value) {
        this.srcLoc = value;
    }

    /**
     * Obtém o valor da propriedade destLoc.
     * 
     * @return
     *     possible object is
     *     {@link CostOptionShipType.DestLoc }
     *     
     */
    public CostOptionShipType.DestLoc getDestLoc() {
        return destLoc;
    }

    /**
     * Define o valor da propriedade destLoc.
     * 
     * @param value
     *     allowed object is
     *     {@link CostOptionShipType.DestLoc }
     *     
     */
    public void setDestLoc(CostOptionShipType.DestLoc value) {
        this.destLoc = value;
    }

    /**
     * Obtém o valor da propriedade shipmentTypeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getShipmentTypeGid() {
        return shipmentTypeGid;
    }

    /**
     * Define o valor da propriedade shipmentTypeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setShipmentTypeGid(GLogXMLGidType value) {
        this.shipmentTypeGid = value;
    }

    /**
     * Obtém o valor da propriedade nfrcRuleGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getNFRCRuleGid() {
        return nfrcRuleGid;
    }

    /**
     * Define o valor da propriedade nfrcRuleGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setNFRCRuleGid(GLogXMLGidType value) {
        this.nfrcRuleGid = value;
    }

    /**
     * Gets the value of the costOptionShipCost property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the costOptionShipCost property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCostOptionShipCost().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CostOptionShipCostType }
     * 
     * 
     */
    public List<CostOptionShipCostType> getCostOptionShipCost() {
        if (costOptionShipCost == null) {
            costOptionShipCost = new ArrayList<CostOptionShipCostType>();
        }
        return this.costOptionShipCost;
    }


    /**
     * <p>Classe Java de anonymous complex type.
     * 
     * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="QuoteLocInfo" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}QuoteLocInfoType"/>
     *         &lt;element name="LocationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
     *         &lt;element name="DestLocationName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "quoteLocInfo",
        "locationGid",
        "destLocationName"
    })
    public static class DestLoc {

        @XmlElement(name = "QuoteLocInfo", required = true)
        protected QuoteLocInfoType quoteLocInfo;
        @XmlElement(name = "LocationGid")
        protected GLogXMLGidType locationGid;
        @XmlElement(name = "DestLocationName")
        protected String destLocationName;

        /**
         * Obtém o valor da propriedade quoteLocInfo.
         * 
         * @return
         *     possible object is
         *     {@link QuoteLocInfoType }
         *     
         */
        public QuoteLocInfoType getQuoteLocInfo() {
            return quoteLocInfo;
        }

        /**
         * Define o valor da propriedade quoteLocInfo.
         * 
         * @param value
         *     allowed object is
         *     {@link QuoteLocInfoType }
         *     
         */
        public void setQuoteLocInfo(QuoteLocInfoType value) {
            this.quoteLocInfo = value;
        }

        /**
         * Obtém o valor da propriedade locationGid.
         * 
         * @return
         *     possible object is
         *     {@link GLogXMLGidType }
         *     
         */
        public GLogXMLGidType getLocationGid() {
            return locationGid;
        }

        /**
         * Define o valor da propriedade locationGid.
         * 
         * @param value
         *     allowed object is
         *     {@link GLogXMLGidType }
         *     
         */
        public void setLocationGid(GLogXMLGidType value) {
            this.locationGid = value;
        }

        /**
         * Obtém o valor da propriedade destLocationName.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDestLocationName() {
            return destLocationName;
        }

        /**
         * Define o valor da propriedade destLocationName.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDestLocationName(String value) {
            this.destLocationName = value;
        }

    }


    /**
     * <p>Classe Java de anonymous complex type.
     * 
     * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="QuoteLocInfo" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}QuoteLocInfoType"/>
     *         &lt;element name="LocationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
     *         &lt;element name="SourceLocationName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "quoteLocInfo",
        "locationGid",
        "sourceLocationName"
    })
    public static class SrcLoc {

        @XmlElement(name = "QuoteLocInfo", required = true)
        protected QuoteLocInfoType quoteLocInfo;
        @XmlElement(name = "LocationGid")
        protected GLogXMLGidType locationGid;
        @XmlElement(name = "SourceLocationName")
        protected String sourceLocationName;

        /**
         * Obtém o valor da propriedade quoteLocInfo.
         * 
         * @return
         *     possible object is
         *     {@link QuoteLocInfoType }
         *     
         */
        public QuoteLocInfoType getQuoteLocInfo() {
            return quoteLocInfo;
        }

        /**
         * Define o valor da propriedade quoteLocInfo.
         * 
         * @param value
         *     allowed object is
         *     {@link QuoteLocInfoType }
         *     
         */
        public void setQuoteLocInfo(QuoteLocInfoType value) {
            this.quoteLocInfo = value;
        }

        /**
         * Obtém o valor da propriedade locationGid.
         * 
         * @return
         *     possible object is
         *     {@link GLogXMLGidType }
         *     
         */
        public GLogXMLGidType getLocationGid() {
            return locationGid;
        }

        /**
         * Define o valor da propriedade locationGid.
         * 
         * @param value
         *     allowed object is
         *     {@link GLogXMLGidType }
         *     
         */
        public void setLocationGid(GLogXMLGidType value) {
            this.locationGid = value;
        }

        /**
         * Obtém o valor da propriedade sourceLocationName.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSourceLocationName() {
            return sourceLocationName;
        }

        /**
         * Define o valor da propriedade sourceLocationName.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSourceLocationName(String value) {
            this.sourceLocationName = value;
        }

    }

}
