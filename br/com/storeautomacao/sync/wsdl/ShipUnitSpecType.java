
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * ShipUnitSpec is an element containing information about ship unit.
 *          
 * 
 * <p>Classe Java de ShipUnitSpecType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="ShipUnitSpecType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ShipUnitSpecGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="ShipUnitSpecName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="UnitType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsInOnMax" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TareWeight" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLWeightType" minOccurs="0"/>
 *         &lt;element name="MaxWeight" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLWeightType" minOccurs="0"/>
 *         &lt;element name="Volume" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}VolumeType" minOccurs="0"/>
 *         &lt;element name="MaxVolume" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLVolumeType" minOccurs="0"/>
 *         &lt;element name="Length" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LengthType" minOccurs="0"/>
 *         &lt;element name="Width" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}WidthType" minOccurs="0"/>
 *         &lt;element name="Height" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}HeightType" minOccurs="0"/>
 *         &lt;element name="Diameter" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}DiameterType" minOccurs="0"/>
 *         &lt;element name="CoreDiameter" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDiameterType" minOccurs="0"/>
 *         &lt;element name="IsCylindrical" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PackagingFormCodeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="IsNestable" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MaxNestingCount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NestingVolume" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLVolumeType" minOccurs="0"/>
 *         &lt;element name="MaxOnLength" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLengthType" minOccurs="0"/>
 *         &lt;element name="MaxOnWidth" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLWidthType" minOccurs="0"/>
 *         &lt;element name="MaxOnHeight" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLHeightType" minOccurs="0"/>
 *         &lt;element name="Refnum" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RefnumType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="THUCapacityPackageRefUnit" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}THUCapacityPackageRefUnitType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Priority" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ShipUnitSpecType", propOrder = {
    "shipUnitSpecGid",
    "shipUnitSpecName",
    "unitType",
    "isInOnMax",
    "tareWeight",
    "maxWeight",
    "volume",
    "maxVolume",
    "length",
    "width",
    "height",
    "diameter",
    "coreDiameter",
    "isCylindrical",
    "packagingFormCodeGid",
    "isNestable",
    "maxNestingCount",
    "nestingVolume",
    "maxOnLength",
    "maxOnWidth",
    "maxOnHeight",
    "refnum",
    "thuCapacityPackageRefUnit",
    "priority"
})
public class ShipUnitSpecType {

    @XmlElement(name = "ShipUnitSpecGid", required = true)
    protected GLogXMLGidType shipUnitSpecGid;
    @XmlElement(name = "ShipUnitSpecName")
    protected String shipUnitSpecName;
    @XmlElement(name = "UnitType")
    protected String unitType;
    @XmlElement(name = "IsInOnMax")
    protected String isInOnMax;
    @XmlElement(name = "TareWeight")
    protected GLogXMLWeightType tareWeight;
    @XmlElement(name = "MaxWeight")
    protected GLogXMLWeightType maxWeight;
    @XmlElement(name = "Volume")
    protected VolumeType volume;
    @XmlElement(name = "MaxVolume")
    protected GLogXMLVolumeType maxVolume;
    @XmlElement(name = "Length")
    protected LengthType length;
    @XmlElement(name = "Width")
    protected WidthType width;
    @XmlElement(name = "Height")
    protected HeightType height;
    @XmlElement(name = "Diameter")
    protected DiameterType diameter;
    @XmlElement(name = "CoreDiameter")
    protected GLogXMLDiameterType coreDiameter;
    @XmlElement(name = "IsCylindrical")
    protected String isCylindrical;
    @XmlElement(name = "PackagingFormCodeGid")
    protected GLogXMLGidType packagingFormCodeGid;
    @XmlElement(name = "IsNestable")
    protected String isNestable;
    @XmlElement(name = "MaxNestingCount")
    protected String maxNestingCount;
    @XmlElement(name = "NestingVolume")
    protected GLogXMLVolumeType nestingVolume;
    @XmlElement(name = "MaxOnLength")
    protected GLogXMLLengthType maxOnLength;
    @XmlElement(name = "MaxOnWidth")
    protected GLogXMLWidthType maxOnWidth;
    @XmlElement(name = "MaxOnHeight")
    protected GLogXMLHeightType maxOnHeight;
    @XmlElement(name = "Refnum")
    protected List<RefnumType> refnum;
    @XmlElement(name = "THUCapacityPackageRefUnit")
    protected List<THUCapacityPackageRefUnitType> thuCapacityPackageRefUnit;
    @XmlElement(name = "Priority")
    protected String priority;

    /**
     * Obtém o valor da propriedade shipUnitSpecGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getShipUnitSpecGid() {
        return shipUnitSpecGid;
    }

    /**
     * Define o valor da propriedade shipUnitSpecGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setShipUnitSpecGid(GLogXMLGidType value) {
        this.shipUnitSpecGid = value;
    }

    /**
     * Obtém o valor da propriedade shipUnitSpecName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipUnitSpecName() {
        return shipUnitSpecName;
    }

    /**
     * Define o valor da propriedade shipUnitSpecName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipUnitSpecName(String value) {
        this.shipUnitSpecName = value;
    }

    /**
     * Obtém o valor da propriedade unitType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUnitType() {
        return unitType;
    }

    /**
     * Define o valor da propriedade unitType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUnitType(String value) {
        this.unitType = value;
    }

    /**
     * Obtém o valor da propriedade isInOnMax.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsInOnMax() {
        return isInOnMax;
    }

    /**
     * Define o valor da propriedade isInOnMax.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsInOnMax(String value) {
        this.isInOnMax = value;
    }

    /**
     * Obtém o valor da propriedade tareWeight.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLWeightType }
     *     
     */
    public GLogXMLWeightType getTareWeight() {
        return tareWeight;
    }

    /**
     * Define o valor da propriedade tareWeight.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLWeightType }
     *     
     */
    public void setTareWeight(GLogXMLWeightType value) {
        this.tareWeight = value;
    }

    /**
     * Obtém o valor da propriedade maxWeight.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLWeightType }
     *     
     */
    public GLogXMLWeightType getMaxWeight() {
        return maxWeight;
    }

    /**
     * Define o valor da propriedade maxWeight.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLWeightType }
     *     
     */
    public void setMaxWeight(GLogXMLWeightType value) {
        this.maxWeight = value;
    }

    /**
     * Obtém o valor da propriedade volume.
     * 
     * @return
     *     possible object is
     *     {@link VolumeType }
     *     
     */
    public VolumeType getVolume() {
        return volume;
    }

    /**
     * Define o valor da propriedade volume.
     * 
     * @param value
     *     allowed object is
     *     {@link VolumeType }
     *     
     */
    public void setVolume(VolumeType value) {
        this.volume = value;
    }

    /**
     * Obtém o valor da propriedade maxVolume.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLVolumeType }
     *     
     */
    public GLogXMLVolumeType getMaxVolume() {
        return maxVolume;
    }

    /**
     * Define o valor da propriedade maxVolume.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLVolumeType }
     *     
     */
    public void setMaxVolume(GLogXMLVolumeType value) {
        this.maxVolume = value;
    }

    /**
     * Obtém o valor da propriedade length.
     * 
     * @return
     *     possible object is
     *     {@link LengthType }
     *     
     */
    public LengthType getLength() {
        return length;
    }

    /**
     * Define o valor da propriedade length.
     * 
     * @param value
     *     allowed object is
     *     {@link LengthType }
     *     
     */
    public void setLength(LengthType value) {
        this.length = value;
    }

    /**
     * Obtém o valor da propriedade width.
     * 
     * @return
     *     possible object is
     *     {@link WidthType }
     *     
     */
    public WidthType getWidth() {
        return width;
    }

    /**
     * Define o valor da propriedade width.
     * 
     * @param value
     *     allowed object is
     *     {@link WidthType }
     *     
     */
    public void setWidth(WidthType value) {
        this.width = value;
    }

    /**
     * Obtém o valor da propriedade height.
     * 
     * @return
     *     possible object is
     *     {@link HeightType }
     *     
     */
    public HeightType getHeight() {
        return height;
    }

    /**
     * Define o valor da propriedade height.
     * 
     * @param value
     *     allowed object is
     *     {@link HeightType }
     *     
     */
    public void setHeight(HeightType value) {
        this.height = value;
    }

    /**
     * Obtém o valor da propriedade diameter.
     * 
     * @return
     *     possible object is
     *     {@link DiameterType }
     *     
     */
    public DiameterType getDiameter() {
        return diameter;
    }

    /**
     * Define o valor da propriedade diameter.
     * 
     * @param value
     *     allowed object is
     *     {@link DiameterType }
     *     
     */
    public void setDiameter(DiameterType value) {
        this.diameter = value;
    }

    /**
     * Obtém o valor da propriedade coreDiameter.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDiameterType }
     *     
     */
    public GLogXMLDiameterType getCoreDiameter() {
        return coreDiameter;
    }

    /**
     * Define o valor da propriedade coreDiameter.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDiameterType }
     *     
     */
    public void setCoreDiameter(GLogXMLDiameterType value) {
        this.coreDiameter = value;
    }

    /**
     * Obtém o valor da propriedade isCylindrical.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsCylindrical() {
        return isCylindrical;
    }

    /**
     * Define o valor da propriedade isCylindrical.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsCylindrical(String value) {
        this.isCylindrical = value;
    }

    /**
     * Obtém o valor da propriedade packagingFormCodeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getPackagingFormCodeGid() {
        return packagingFormCodeGid;
    }

    /**
     * Define o valor da propriedade packagingFormCodeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setPackagingFormCodeGid(GLogXMLGidType value) {
        this.packagingFormCodeGid = value;
    }

    /**
     * Obtém o valor da propriedade isNestable.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsNestable() {
        return isNestable;
    }

    /**
     * Define o valor da propriedade isNestable.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsNestable(String value) {
        this.isNestable = value;
    }

    /**
     * Obtém o valor da propriedade maxNestingCount.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMaxNestingCount() {
        return maxNestingCount;
    }

    /**
     * Define o valor da propriedade maxNestingCount.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMaxNestingCount(String value) {
        this.maxNestingCount = value;
    }

    /**
     * Obtém o valor da propriedade nestingVolume.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLVolumeType }
     *     
     */
    public GLogXMLVolumeType getNestingVolume() {
        return nestingVolume;
    }

    /**
     * Define o valor da propriedade nestingVolume.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLVolumeType }
     *     
     */
    public void setNestingVolume(GLogXMLVolumeType value) {
        this.nestingVolume = value;
    }

    /**
     * Obtém o valor da propriedade maxOnLength.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLengthType }
     *     
     */
    public GLogXMLLengthType getMaxOnLength() {
        return maxOnLength;
    }

    /**
     * Define o valor da propriedade maxOnLength.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLengthType }
     *     
     */
    public void setMaxOnLength(GLogXMLLengthType value) {
        this.maxOnLength = value;
    }

    /**
     * Obtém o valor da propriedade maxOnWidth.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLWidthType }
     *     
     */
    public GLogXMLWidthType getMaxOnWidth() {
        return maxOnWidth;
    }

    /**
     * Define o valor da propriedade maxOnWidth.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLWidthType }
     *     
     */
    public void setMaxOnWidth(GLogXMLWidthType value) {
        this.maxOnWidth = value;
    }

    /**
     * Obtém o valor da propriedade maxOnHeight.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLHeightType }
     *     
     */
    public GLogXMLHeightType getMaxOnHeight() {
        return maxOnHeight;
    }

    /**
     * Define o valor da propriedade maxOnHeight.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLHeightType }
     *     
     */
    public void setMaxOnHeight(GLogXMLHeightType value) {
        this.maxOnHeight = value;
    }

    /**
     * Gets the value of the refnum property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the refnum property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRefnum().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RefnumType }
     * 
     * 
     */
    public List<RefnumType> getRefnum() {
        if (refnum == null) {
            refnum = new ArrayList<RefnumType>();
        }
        return this.refnum;
    }

    /**
     * Gets the value of the thuCapacityPackageRefUnit property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the thuCapacityPackageRefUnit property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTHUCapacityPackageRefUnit().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link THUCapacityPackageRefUnitType }
     * 
     * 
     */
    public List<THUCapacityPackageRefUnitType> getTHUCapacityPackageRefUnit() {
        if (thuCapacityPackageRefUnit == null) {
            thuCapacityPackageRefUnit = new ArrayList<THUCapacityPackageRefUnitType>();
        }
        return this.thuCapacityPackageRefUnit;
    }

    /**
     * Obtém o valor da propriedade priority.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPriority() {
        return priority;
    }

    /**
     * Define o valor da propriedade priority.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPriority(String value) {
        this.priority = value;
    }

}
