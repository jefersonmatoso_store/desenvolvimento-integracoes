
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * BilledRated is used to specify, weight, volume, and type of service for an invoice line item.
 * 
 * <p>Classe Java de BilledRatedType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="BilledRatedType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BilledRatedQualifier" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BilledRatedQuantity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="UnitCount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;choice minOccurs="0">
 *           &lt;element name="TransportHandlingUnitRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLShipUnitSpecRefType" minOccurs="0"/>
 *           &lt;element name="PackagedItemSpecRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLShipUnitSpecRefType" minOccurs="0"/>
 *         &lt;/choice>
 *         &lt;element name="WeightVolume" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}WeightVolumeType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BilledRatedType", propOrder = {
    "billedRatedQualifier",
    "billedRatedQuantity",
    "unitCount",
    "transportHandlingUnitRef",
    "packagedItemSpecRef",
    "weightVolume"
})
public class BilledRatedType {

    @XmlElement(name = "BilledRatedQualifier")
    protected String billedRatedQualifier;
    @XmlElement(name = "BilledRatedQuantity")
    protected String billedRatedQuantity;
    @XmlElement(name = "UnitCount")
    protected String unitCount;
    @XmlElement(name = "TransportHandlingUnitRef")
    protected GLogXMLShipUnitSpecRefType transportHandlingUnitRef;
    @XmlElement(name = "PackagedItemSpecRef")
    protected GLogXMLShipUnitSpecRefType packagedItemSpecRef;
    @XmlElement(name = "WeightVolume")
    protected WeightVolumeType weightVolume;

    /**
     * Obtém o valor da propriedade billedRatedQualifier.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBilledRatedQualifier() {
        return billedRatedQualifier;
    }

    /**
     * Define o valor da propriedade billedRatedQualifier.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBilledRatedQualifier(String value) {
        this.billedRatedQualifier = value;
    }

    /**
     * Obtém o valor da propriedade billedRatedQuantity.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBilledRatedQuantity() {
        return billedRatedQuantity;
    }

    /**
     * Define o valor da propriedade billedRatedQuantity.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBilledRatedQuantity(String value) {
        this.billedRatedQuantity = value;
    }

    /**
     * Obtém o valor da propriedade unitCount.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUnitCount() {
        return unitCount;
    }

    /**
     * Define o valor da propriedade unitCount.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUnitCount(String value) {
        this.unitCount = value;
    }

    /**
     * Obtém o valor da propriedade transportHandlingUnitRef.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLShipUnitSpecRefType }
     *     
     */
    public GLogXMLShipUnitSpecRefType getTransportHandlingUnitRef() {
        return transportHandlingUnitRef;
    }

    /**
     * Define o valor da propriedade transportHandlingUnitRef.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLShipUnitSpecRefType }
     *     
     */
    public void setTransportHandlingUnitRef(GLogXMLShipUnitSpecRefType value) {
        this.transportHandlingUnitRef = value;
    }

    /**
     * Obtém o valor da propriedade packagedItemSpecRef.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLShipUnitSpecRefType }
     *     
     */
    public GLogXMLShipUnitSpecRefType getPackagedItemSpecRef() {
        return packagedItemSpecRef;
    }

    /**
     * Define o valor da propriedade packagedItemSpecRef.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLShipUnitSpecRefType }
     *     
     */
    public void setPackagedItemSpecRef(GLogXMLShipUnitSpecRefType value) {
        this.packagedItemSpecRef = value;
    }

    /**
     * Obtém o valor da propriedade weightVolume.
     * 
     * @return
     *     possible object is
     *     {@link WeightVolumeType }
     *     
     */
    public WeightVolumeType getWeightVolume() {
        return weightVolume;
    }

    /**
     * Define o valor da propriedade weightVolume.
     * 
     * @param value
     *     allowed object is
     *     {@link WeightVolumeType }
     *     
     */
    public void setWeightVolume(WeightVolumeType value) {
        this.weightVolume = value;
    }

}
