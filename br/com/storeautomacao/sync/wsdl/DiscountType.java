
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de DiscountType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="DiscountType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;choice>
 *           &lt;element name="DiscountAmount" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType"/>
 *           &lt;element name="DiscountPercentage" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;/choice>
 *         &lt;element name="DiscountDaysDue" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="DiscountDueDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DiscountType", propOrder = {
    "discountAmount",
    "discountPercentage",
    "discountDaysDue",
    "discountDueDate"
})
public class DiscountType {

    @XmlElement(name = "DiscountAmount")
    protected GLogXMLFinancialAmountType discountAmount;
    @XmlElement(name = "DiscountPercentage")
    protected String discountPercentage;
    @XmlElement(name = "DiscountDaysDue", required = true)
    protected String discountDaysDue;
    @XmlElement(name = "DiscountDueDate", required = true)
    protected GLogDateTimeType discountDueDate;

    /**
     * Obtém o valor da propriedade discountAmount.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getDiscountAmount() {
        return discountAmount;
    }

    /**
     * Define o valor da propriedade discountAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setDiscountAmount(GLogXMLFinancialAmountType value) {
        this.discountAmount = value;
    }

    /**
     * Obtém o valor da propriedade discountPercentage.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDiscountPercentage() {
        return discountPercentage;
    }

    /**
     * Define o valor da propriedade discountPercentage.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDiscountPercentage(String value) {
        this.discountPercentage = value;
    }

    /**
     * Obtém o valor da propriedade discountDaysDue.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDiscountDaysDue() {
        return discountDaysDue;
    }

    /**
     * Define o valor da propriedade discountDaysDue.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDiscountDaysDue(String value) {
        this.discountDaysDue = value;
    }

    /**
     * Obtém o valor da propriedade discountDueDate.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getDiscountDueDate() {
        return discountDueDate;
    }

    /**
     * Define o valor da propriedade discountDueDate.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setDiscountDueDate(GLogDateTimeType value) {
        this.discountDueDate = value;
    }

}
