
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * GenericStopOff is used to convey stopoff information pertaining to motor or rail carriers
 * 
 * <p>Classe Java de StopOffType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="StopOffType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="StopSequence" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="StopReasonGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="EquipmentPrefix" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EquipmentNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="StopSeal" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}StopSealType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="PickupDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="DeliveryDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="StopRefnum" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}StopRefnumType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;choice>
 *           &lt;element name="LocationRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LocationRefType"/>
 *           &lt;element name="LocationRefnum" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LocationRefnumType"/>
 *         &lt;/choice>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "StopOffType", propOrder = {
    "stopSequence",
    "stopReasonGid",
    "equipmentPrefix",
    "equipmentNumber",
    "stopSeal",
    "pickupDate",
    "deliveryDate",
    "stopRefnum",
    "locationRef",
    "locationRefnum"
})
public class StopOffType {

    @XmlElement(name = "StopSequence", required = true)
    protected String stopSequence;
    @XmlElement(name = "StopReasonGid")
    protected GLogXMLGidType stopReasonGid;
    @XmlElement(name = "EquipmentPrefix")
    protected String equipmentPrefix;
    @XmlElement(name = "EquipmentNumber")
    protected String equipmentNumber;
    @XmlElement(name = "StopSeal")
    protected List<StopSealType> stopSeal;
    @XmlElement(name = "PickupDate")
    protected GLogDateTimeType pickupDate;
    @XmlElement(name = "DeliveryDate")
    protected GLogDateTimeType deliveryDate;
    @XmlElement(name = "StopRefnum")
    protected List<StopRefnumType> stopRefnum;
    @XmlElement(name = "LocationRef")
    protected LocationRefType locationRef;
    @XmlElement(name = "LocationRefnum")
    protected LocationRefnumType locationRefnum;

    /**
     * Obtém o valor da propriedade stopSequence.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStopSequence() {
        return stopSequence;
    }

    /**
     * Define o valor da propriedade stopSequence.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStopSequence(String value) {
        this.stopSequence = value;
    }

    /**
     * Obtém o valor da propriedade stopReasonGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getStopReasonGid() {
        return stopReasonGid;
    }

    /**
     * Define o valor da propriedade stopReasonGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setStopReasonGid(GLogXMLGidType value) {
        this.stopReasonGid = value;
    }

    /**
     * Obtém o valor da propriedade equipmentPrefix.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEquipmentPrefix() {
        return equipmentPrefix;
    }

    /**
     * Define o valor da propriedade equipmentPrefix.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEquipmentPrefix(String value) {
        this.equipmentPrefix = value;
    }

    /**
     * Obtém o valor da propriedade equipmentNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEquipmentNumber() {
        return equipmentNumber;
    }

    /**
     * Define o valor da propriedade equipmentNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEquipmentNumber(String value) {
        this.equipmentNumber = value;
    }

    /**
     * Gets the value of the stopSeal property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the stopSeal property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getStopSeal().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link StopSealType }
     * 
     * 
     */
    public List<StopSealType> getStopSeal() {
        if (stopSeal == null) {
            stopSeal = new ArrayList<StopSealType>();
        }
        return this.stopSeal;
    }

    /**
     * Obtém o valor da propriedade pickupDate.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getPickupDate() {
        return pickupDate;
    }

    /**
     * Define o valor da propriedade pickupDate.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setPickupDate(GLogDateTimeType value) {
        this.pickupDate = value;
    }

    /**
     * Obtém o valor da propriedade deliveryDate.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getDeliveryDate() {
        return deliveryDate;
    }

    /**
     * Define o valor da propriedade deliveryDate.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setDeliveryDate(GLogDateTimeType value) {
        this.deliveryDate = value;
    }

    /**
     * Gets the value of the stopRefnum property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the stopRefnum property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getStopRefnum().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link StopRefnumType }
     * 
     * 
     */
    public List<StopRefnumType> getStopRefnum() {
        if (stopRefnum == null) {
            stopRefnum = new ArrayList<StopRefnumType>();
        }
        return this.stopRefnum;
    }

    /**
     * Obtém o valor da propriedade locationRef.
     * 
     * @return
     *     possible object is
     *     {@link LocationRefType }
     *     
     */
    public LocationRefType getLocationRef() {
        return locationRef;
    }

    /**
     * Define o valor da propriedade locationRef.
     * 
     * @param value
     *     allowed object is
     *     {@link LocationRefType }
     *     
     */
    public void setLocationRef(LocationRefType value) {
        this.locationRef = value;
    }

    /**
     * Obtém o valor da propriedade locationRefnum.
     * 
     * @return
     *     possible object is
     *     {@link LocationRefnumType }
     *     
     */
    public LocationRefnumType getLocationRefnum() {
        return locationRefnum;
    }

    /**
     * Define o valor da propriedade locationRefnum.
     * 
     * @param value
     *     allowed object is
     *     {@link LocationRefnumType }
     *     
     */
    public void setLocationRefnum(LocationRefnumType value) {
        this.locationRefnum = value;
    }

}
