
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * ProtectiveSvc is an element of RailDetail used to specify protective service
 *             and ventilation instructions on a rail specific invoice.
 *          
 * 
 * <p>Classe Java de ProtectiveSvcType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="ProtectiveSvcType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ProtectiveSvcCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ProtectiveSvcRuleCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Temperature" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TemperatureType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProtectiveSvcType", propOrder = {
    "protectiveSvcCode",
    "protectiveSvcRuleCode",
    "temperature"
})
public class ProtectiveSvcType {

    @XmlElement(name = "ProtectiveSvcCode")
    protected String protectiveSvcCode;
    @XmlElement(name = "ProtectiveSvcRuleCode")
    protected String protectiveSvcRuleCode;
    @XmlElement(name = "Temperature")
    protected TemperatureType temperature;

    /**
     * Obtém o valor da propriedade protectiveSvcCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProtectiveSvcCode() {
        return protectiveSvcCode;
    }

    /**
     * Define o valor da propriedade protectiveSvcCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProtectiveSvcCode(String value) {
        this.protectiveSvcCode = value;
    }

    /**
     * Obtém o valor da propriedade protectiveSvcRuleCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProtectiveSvcRuleCode() {
        return protectiveSvcRuleCode;
    }

    /**
     * Define o valor da propriedade protectiveSvcRuleCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProtectiveSvcRuleCode(String value) {
        this.protectiveSvcRuleCode = value;
    }

    /**
     * Obtém o valor da propriedade temperature.
     * 
     * @return
     *     possible object is
     *     {@link TemperatureType }
     *     
     */
    public TemperatureType getTemperature() {
        return temperature;
    }

    /**
     * Define o valor da propriedade temperature.
     * 
     * @param value
     *     allowed object is
     *     {@link TemperatureType }
     *     
     */
    public void setTemperature(TemperatureType value) {
        this.temperature = value;
    }

}
