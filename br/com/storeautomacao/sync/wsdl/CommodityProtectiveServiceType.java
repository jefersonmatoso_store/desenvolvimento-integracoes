
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * This element is used to define a Protective Service for a commodity.
 *             Protective service is used by the rail industry to communicate temperature
 *             requirements and is where the user communicates optimal temperature for the RR.
 * 
 *             In the Item element, this element is only supported on the outbound.
 *          
 * 
 * <p>Classe Java de CommodityProtectiveServiceType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="CommodityProtectiveServiceType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SequenceNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ProtectiveSvc" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ProtectiveSvcType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CommodityProtectiveServiceType", propOrder = {
    "sequenceNumber",
    "protectiveSvc"
})
public class CommodityProtectiveServiceType {

    @XmlElement(name = "SequenceNumber", required = true)
    protected String sequenceNumber;
    @XmlElement(name = "ProtectiveSvc", required = true)
    protected ProtectiveSvcType protectiveSvc;

    /**
     * Obtém o valor da propriedade sequenceNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSequenceNumber() {
        return sequenceNumber;
    }

    /**
     * Define o valor da propriedade sequenceNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSequenceNumber(String value) {
        this.sequenceNumber = value;
    }

    /**
     * Obtém o valor da propriedade protectiveSvc.
     * 
     * @return
     *     possible object is
     *     {@link ProtectiveSvcType }
     *     
     */
    public ProtectiveSvcType getProtectiveSvc() {
        return protectiveSvc;
    }

    /**
     * Define o valor da propriedade protectiveSvc.
     * 
     * @param value
     *     allowed object is
     *     {@link ProtectiveSvcType }
     *     
     */
    public void setProtectiveSvc(ProtectiveSvcType value) {
        this.protectiveSvc = value;
    }

}
