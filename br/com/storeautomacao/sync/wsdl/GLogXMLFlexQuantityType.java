
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de GLogXMLFlexQuantityType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="GLogXMLFlexQuantityType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FlexQuantity" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexQuantityType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GLogXMLFlexQuantityType", propOrder = {
    "flexQuantity"
})
public class GLogXMLFlexQuantityType {

    @XmlElement(name = "FlexQuantity", required = true)
    protected FlexQuantityType flexQuantity;

    /**
     * Obtém o valor da propriedade flexQuantity.
     * 
     * @return
     *     possible object is
     *     {@link FlexQuantityType }
     *     
     */
    public FlexQuantityType getFlexQuantity() {
        return flexQuantity;
    }

    /**
     * Define o valor da propriedade flexQuantity.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexQuantityType }
     *     
     */
    public void setFlexQuantity(FlexQuantityType value) {
        this.flexQuantity = value;
    }

}
