
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * LocationRole is a structure specifying a location role.
 *             Locations may play multiple roles.
 *             Examples of location roles include warehouse, crossdock, loading dock, etc.
 *             A calendar may be associated with a location role, to limit the times when a particular activity (such as loading) may occur.
 *          
 * 
 * <p>Classe Java de LocationRoleType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="LocationRoleType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="LocationRoleGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="CalendarGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="XDockIsInboundBias" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FixedHandlingTime" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDurationType" minOccurs="0"/>
 *         &lt;element name="VarHandlingTime" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDurationType" minOccurs="0"/>
 *         &lt;element name="VarHandlingWeight" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLWeightType" minOccurs="0"/>
 *         &lt;element name="MaxFreightIdleTime" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDurationType" minOccurs="0"/>
 *         &lt;element name="CreatePoolHandlingShipment" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CreateXDockHandlingShipment" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LocationActivityTimeDef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LocationActivityTimeDefType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="PctActivityBeforeLocOpen" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PctActivityAfterLocClose" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsMixedFreightTHUAllowed" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ShipUnitSpecProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="LocationTHUCapacity" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LocationTHUCapacityType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="VarHandlingCostPerVolume" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/>
 *         &lt;element name="VarHandlingVolume" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLVolumeType" minOccurs="0"/>
 *         &lt;element name="RegionalHandlingTime" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RegionalHandlingTimeType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LocationRoleType", propOrder = {
    "locationRoleGid",
    "calendarGid",
    "xDockIsInboundBias",
    "fixedHandlingTime",
    "varHandlingTime",
    "varHandlingWeight",
    "maxFreightIdleTime",
    "createPoolHandlingShipment",
    "createXDockHandlingShipment",
    "locationActivityTimeDef",
    "pctActivityBeforeLocOpen",
    "pctActivityAfterLocClose",
    "isMixedFreightTHUAllowed",
    "shipUnitSpecProfileGid",
    "locationTHUCapacity",
    "varHandlingCostPerVolume",
    "varHandlingVolume",
    "regionalHandlingTime"
})
public class LocationRoleType {

    @XmlElement(name = "LocationRoleGid", required = true)
    protected GLogXMLGidType locationRoleGid;
    @XmlElement(name = "CalendarGid")
    protected GLogXMLGidType calendarGid;
    @XmlElement(name = "XDockIsInboundBias")
    protected String xDockIsInboundBias;
    @XmlElement(name = "FixedHandlingTime")
    protected GLogXMLDurationType fixedHandlingTime;
    @XmlElement(name = "VarHandlingTime")
    protected GLogXMLDurationType varHandlingTime;
    @XmlElement(name = "VarHandlingWeight")
    protected GLogXMLWeightType varHandlingWeight;
    @XmlElement(name = "MaxFreightIdleTime")
    protected GLogXMLDurationType maxFreightIdleTime;
    @XmlElement(name = "CreatePoolHandlingShipment")
    protected String createPoolHandlingShipment;
    @XmlElement(name = "CreateXDockHandlingShipment")
    protected String createXDockHandlingShipment;
    @XmlElement(name = "LocationActivityTimeDef")
    protected List<LocationActivityTimeDefType> locationActivityTimeDef;
    @XmlElement(name = "PctActivityBeforeLocOpen")
    protected String pctActivityBeforeLocOpen;
    @XmlElement(name = "PctActivityAfterLocClose")
    protected String pctActivityAfterLocClose;
    @XmlElement(name = "IsMixedFreightTHUAllowed")
    protected String isMixedFreightTHUAllowed;
    @XmlElement(name = "ShipUnitSpecProfileGid")
    protected GLogXMLGidType shipUnitSpecProfileGid;
    @XmlElement(name = "LocationTHUCapacity")
    protected List<LocationTHUCapacityType> locationTHUCapacity;
    @XmlElement(name = "VarHandlingCostPerVolume")
    protected GLogXMLFinancialAmountType varHandlingCostPerVolume;
    @XmlElement(name = "VarHandlingVolume")
    protected GLogXMLVolumeType varHandlingVolume;
    @XmlElement(name = "RegionalHandlingTime")
    protected List<RegionalHandlingTimeType> regionalHandlingTime;

    /**
     * Obtém o valor da propriedade locationRoleGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getLocationRoleGid() {
        return locationRoleGid;
    }

    /**
     * Define o valor da propriedade locationRoleGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setLocationRoleGid(GLogXMLGidType value) {
        this.locationRoleGid = value;
    }

    /**
     * Obtém o valor da propriedade calendarGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getCalendarGid() {
        return calendarGid;
    }

    /**
     * Define o valor da propriedade calendarGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setCalendarGid(GLogXMLGidType value) {
        this.calendarGid = value;
    }

    /**
     * Obtém o valor da propriedade xDockIsInboundBias.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXDockIsInboundBias() {
        return xDockIsInboundBias;
    }

    /**
     * Define o valor da propriedade xDockIsInboundBias.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXDockIsInboundBias(String value) {
        this.xDockIsInboundBias = value;
    }

    /**
     * Obtém o valor da propriedade fixedHandlingTime.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public GLogXMLDurationType getFixedHandlingTime() {
        return fixedHandlingTime;
    }

    /**
     * Define o valor da propriedade fixedHandlingTime.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public void setFixedHandlingTime(GLogXMLDurationType value) {
        this.fixedHandlingTime = value;
    }

    /**
     * Obtém o valor da propriedade varHandlingTime.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public GLogXMLDurationType getVarHandlingTime() {
        return varHandlingTime;
    }

    /**
     * Define o valor da propriedade varHandlingTime.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public void setVarHandlingTime(GLogXMLDurationType value) {
        this.varHandlingTime = value;
    }

    /**
     * Obtém o valor da propriedade varHandlingWeight.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLWeightType }
     *     
     */
    public GLogXMLWeightType getVarHandlingWeight() {
        return varHandlingWeight;
    }

    /**
     * Define o valor da propriedade varHandlingWeight.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLWeightType }
     *     
     */
    public void setVarHandlingWeight(GLogXMLWeightType value) {
        this.varHandlingWeight = value;
    }

    /**
     * Obtém o valor da propriedade maxFreightIdleTime.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public GLogXMLDurationType getMaxFreightIdleTime() {
        return maxFreightIdleTime;
    }

    /**
     * Define o valor da propriedade maxFreightIdleTime.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public void setMaxFreightIdleTime(GLogXMLDurationType value) {
        this.maxFreightIdleTime = value;
    }

    /**
     * Obtém o valor da propriedade createPoolHandlingShipment.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCreatePoolHandlingShipment() {
        return createPoolHandlingShipment;
    }

    /**
     * Define o valor da propriedade createPoolHandlingShipment.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCreatePoolHandlingShipment(String value) {
        this.createPoolHandlingShipment = value;
    }

    /**
     * Obtém o valor da propriedade createXDockHandlingShipment.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCreateXDockHandlingShipment() {
        return createXDockHandlingShipment;
    }

    /**
     * Define o valor da propriedade createXDockHandlingShipment.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCreateXDockHandlingShipment(String value) {
        this.createXDockHandlingShipment = value;
    }

    /**
     * Gets the value of the locationActivityTimeDef property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the locationActivityTimeDef property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLocationActivityTimeDef().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LocationActivityTimeDefType }
     * 
     * 
     */
    public List<LocationActivityTimeDefType> getLocationActivityTimeDef() {
        if (locationActivityTimeDef == null) {
            locationActivityTimeDef = new ArrayList<LocationActivityTimeDefType>();
        }
        return this.locationActivityTimeDef;
    }

    /**
     * Obtém o valor da propriedade pctActivityBeforeLocOpen.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPctActivityBeforeLocOpen() {
        return pctActivityBeforeLocOpen;
    }

    /**
     * Define o valor da propriedade pctActivityBeforeLocOpen.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPctActivityBeforeLocOpen(String value) {
        this.pctActivityBeforeLocOpen = value;
    }

    /**
     * Obtém o valor da propriedade pctActivityAfterLocClose.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPctActivityAfterLocClose() {
        return pctActivityAfterLocClose;
    }

    /**
     * Define o valor da propriedade pctActivityAfterLocClose.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPctActivityAfterLocClose(String value) {
        this.pctActivityAfterLocClose = value;
    }

    /**
     * Obtém o valor da propriedade isMixedFreightTHUAllowed.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsMixedFreightTHUAllowed() {
        return isMixedFreightTHUAllowed;
    }

    /**
     * Define o valor da propriedade isMixedFreightTHUAllowed.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsMixedFreightTHUAllowed(String value) {
        this.isMixedFreightTHUAllowed = value;
    }

    /**
     * Obtém o valor da propriedade shipUnitSpecProfileGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getShipUnitSpecProfileGid() {
        return shipUnitSpecProfileGid;
    }

    /**
     * Define o valor da propriedade shipUnitSpecProfileGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setShipUnitSpecProfileGid(GLogXMLGidType value) {
        this.shipUnitSpecProfileGid = value;
    }

    /**
     * Gets the value of the locationTHUCapacity property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the locationTHUCapacity property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLocationTHUCapacity().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LocationTHUCapacityType }
     * 
     * 
     */
    public List<LocationTHUCapacityType> getLocationTHUCapacity() {
        if (locationTHUCapacity == null) {
            locationTHUCapacity = new ArrayList<LocationTHUCapacityType>();
        }
        return this.locationTHUCapacity;
    }

    /**
     * Obtém o valor da propriedade varHandlingCostPerVolume.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getVarHandlingCostPerVolume() {
        return varHandlingCostPerVolume;
    }

    /**
     * Define o valor da propriedade varHandlingCostPerVolume.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setVarHandlingCostPerVolume(GLogXMLFinancialAmountType value) {
        this.varHandlingCostPerVolume = value;
    }

    /**
     * Obtém o valor da propriedade varHandlingVolume.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLVolumeType }
     *     
     */
    public GLogXMLVolumeType getVarHandlingVolume() {
        return varHandlingVolume;
    }

    /**
     * Define o valor da propriedade varHandlingVolume.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLVolumeType }
     *     
     */
    public void setVarHandlingVolume(GLogXMLVolumeType value) {
        this.varHandlingVolume = value;
    }

    /**
     * Gets the value of the regionalHandlingTime property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the regionalHandlingTime property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRegionalHandlingTime().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RegionalHandlingTimeType }
     * 
     * 
     */
    public List<RegionalHandlingTimeType> getRegionalHandlingTime() {
        if (regionalHandlingTime == null) {
            regionalHandlingTime = new ArrayList<RegionalHandlingTimeType>();
        }
        return this.regionalHandlingTime;
    }

}
