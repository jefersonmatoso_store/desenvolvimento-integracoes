
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Contains common hazardous materials information.
 * 
 * <p>Classe Java de HazmatCommonInfoType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="HazmatCommonInfoType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="HazActivity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NetExplosiveContentWeight" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLWeightType" minOccurs="0"/>
 *         &lt;element name="IsLimitedQuantity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsReportableQuantity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RqTechnicalName1Gid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TechnicalNameGidType" minOccurs="0"/>
 *         &lt;element name="RqTechnicalName2Gid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TechnicalNameGidType" minOccurs="0"/>
 *         &lt;element name="IsNos" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NosTechnicalName1Gid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TechnicalNameGidType" minOccurs="0"/>
 *         &lt;element name="NosTechnicalName2Gid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TechnicalNameGidType" minOccurs="0"/>
 *         &lt;element name="IsToxicInhalation" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InhalationHazardZone" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsPassengerAircraftForbid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsCommercialAircraftForbid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FlashPoint" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLTemperatureType" minOccurs="0"/>
 *         &lt;element name="EmergencyTemperature" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLTemperatureType" minOccurs="0"/>
 *         &lt;element name="ControlTemperature" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLTemperatureType" minOccurs="0"/>
 *         &lt;element name="IsMarinePollutant" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MpTechnicalName1Gid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TechnicalNameGidType" minOccurs="0"/>
 *         &lt;element name="MpTechnicalName2Gid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TechnicalNameGidType" minOccurs="0"/>
 *         &lt;element name="IsOilContained" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsAllPacked" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Authorization" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ChemicalFormula" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ConcentrationPercent" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CritSafetyIndex" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IDGAddDescInfo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MaxQuantity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OuterPackagingType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OuterPackingCount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="HazmatPackageType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PackingCount" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PackingInstructions" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PhysicalForm" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="QValue" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="HazQuantity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RadioactiveLabel" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RadPackaging" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Radioactivity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RadioactivityUnits" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Radionuclide" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SubstanceNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SurfaceReading" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TransportIndex" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MaterialsType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Units" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ExternalCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsOverpack" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "HazmatCommonInfoType", propOrder = {
    "hazActivity",
    "description",
    "netExplosiveContentWeight",
    "isLimitedQuantity",
    "isReportableQuantity",
    "rqTechnicalName1Gid",
    "rqTechnicalName2Gid",
    "isNos",
    "nosTechnicalName1Gid",
    "nosTechnicalName2Gid",
    "isToxicInhalation",
    "inhalationHazardZone",
    "isPassengerAircraftForbid",
    "isCommercialAircraftForbid",
    "flashPoint",
    "emergencyTemperature",
    "controlTemperature",
    "isMarinePollutant",
    "mpTechnicalName1Gid",
    "mpTechnicalName2Gid",
    "isOilContained",
    "isAllPacked",
    "authorization",
    "chemicalFormula",
    "concentrationPercent",
    "critSafetyIndex",
    "idgAddDescInfo",
    "maxQuantity",
    "outerPackagingType",
    "outerPackingCount",
    "hazmatPackageType",
    "packingCount",
    "packingInstructions",
    "physicalForm",
    "qValue",
    "hazQuantity",
    "radioactiveLabel",
    "radPackaging",
    "radioactivity",
    "radioactivityUnits",
    "radionuclide",
    "substanceNumber",
    "surfaceReading",
    "transportIndex",
    "materialsType",
    "units",
    "externalCode",
    "isOverpack"
})
public class HazmatCommonInfoType {

    @XmlElement(name = "HazActivity")
    protected String hazActivity;
    @XmlElement(name = "Description")
    protected String description;
    @XmlElement(name = "NetExplosiveContentWeight")
    protected GLogXMLWeightType netExplosiveContentWeight;
    @XmlElement(name = "IsLimitedQuantity")
    protected String isLimitedQuantity;
    @XmlElement(name = "IsReportableQuantity")
    protected String isReportableQuantity;
    @XmlElement(name = "RqTechnicalName1Gid")
    protected TechnicalNameGidType rqTechnicalName1Gid;
    @XmlElement(name = "RqTechnicalName2Gid")
    protected TechnicalNameGidType rqTechnicalName2Gid;
    @XmlElement(name = "IsNos")
    protected String isNos;
    @XmlElement(name = "NosTechnicalName1Gid")
    protected TechnicalNameGidType nosTechnicalName1Gid;
    @XmlElement(name = "NosTechnicalName2Gid")
    protected TechnicalNameGidType nosTechnicalName2Gid;
    @XmlElement(name = "IsToxicInhalation")
    protected String isToxicInhalation;
    @XmlElement(name = "InhalationHazardZone")
    protected String inhalationHazardZone;
    @XmlElement(name = "IsPassengerAircraftForbid")
    protected String isPassengerAircraftForbid;
    @XmlElement(name = "IsCommercialAircraftForbid")
    protected String isCommercialAircraftForbid;
    @XmlElement(name = "FlashPoint")
    protected GLogXMLTemperatureType flashPoint;
    @XmlElement(name = "EmergencyTemperature")
    protected GLogXMLTemperatureType emergencyTemperature;
    @XmlElement(name = "ControlTemperature")
    protected GLogXMLTemperatureType controlTemperature;
    @XmlElement(name = "IsMarinePollutant")
    protected String isMarinePollutant;
    @XmlElement(name = "MpTechnicalName1Gid")
    protected TechnicalNameGidType mpTechnicalName1Gid;
    @XmlElement(name = "MpTechnicalName2Gid")
    protected TechnicalNameGidType mpTechnicalName2Gid;
    @XmlElement(name = "IsOilContained")
    protected String isOilContained;
    @XmlElement(name = "IsAllPacked")
    protected String isAllPacked;
    @XmlElement(name = "Authorization")
    protected String authorization;
    @XmlElement(name = "ChemicalFormula")
    protected String chemicalFormula;
    @XmlElement(name = "ConcentrationPercent")
    protected String concentrationPercent;
    @XmlElement(name = "CritSafetyIndex")
    protected String critSafetyIndex;
    @XmlElement(name = "IDGAddDescInfo")
    protected String idgAddDescInfo;
    @XmlElement(name = "MaxQuantity")
    protected String maxQuantity;
    @XmlElement(name = "OuterPackagingType")
    protected String outerPackagingType;
    @XmlElement(name = "OuterPackingCount")
    protected String outerPackingCount;
    @XmlElement(name = "HazmatPackageType")
    protected String hazmatPackageType;
    @XmlElement(name = "PackingCount", required = true)
    protected String packingCount;
    @XmlElement(name = "PackingInstructions")
    protected String packingInstructions;
    @XmlElement(name = "PhysicalForm")
    protected String physicalForm;
    @XmlElement(name = "QValue")
    protected String qValue;
    @XmlElement(name = "HazQuantity")
    protected String hazQuantity;
    @XmlElement(name = "RadioactiveLabel")
    protected String radioactiveLabel;
    @XmlElement(name = "RadPackaging")
    protected String radPackaging;
    @XmlElement(name = "Radioactivity")
    protected String radioactivity;
    @XmlElement(name = "RadioactivityUnits")
    protected String radioactivityUnits;
    @XmlElement(name = "Radionuclide")
    protected String radionuclide;
    @XmlElement(name = "SubstanceNumber")
    protected String substanceNumber;
    @XmlElement(name = "SurfaceReading")
    protected String surfaceReading;
    @XmlElement(name = "TransportIndex")
    protected String transportIndex;
    @XmlElement(name = "MaterialsType")
    protected String materialsType;
    @XmlElement(name = "Units")
    protected String units;
    @XmlElement(name = "ExternalCode")
    protected String externalCode;
    @XmlElement(name = "IsOverpack")
    protected String isOverpack;

    /**
     * Obtém o valor da propriedade hazActivity.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHazActivity() {
        return hazActivity;
    }

    /**
     * Define o valor da propriedade hazActivity.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHazActivity(String value) {
        this.hazActivity = value;
    }

    /**
     * Obtém o valor da propriedade description.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Define o valor da propriedade description.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Obtém o valor da propriedade netExplosiveContentWeight.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLWeightType }
     *     
     */
    public GLogXMLWeightType getNetExplosiveContentWeight() {
        return netExplosiveContentWeight;
    }

    /**
     * Define o valor da propriedade netExplosiveContentWeight.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLWeightType }
     *     
     */
    public void setNetExplosiveContentWeight(GLogXMLWeightType value) {
        this.netExplosiveContentWeight = value;
    }

    /**
     * Obtém o valor da propriedade isLimitedQuantity.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsLimitedQuantity() {
        return isLimitedQuantity;
    }

    /**
     * Define o valor da propriedade isLimitedQuantity.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsLimitedQuantity(String value) {
        this.isLimitedQuantity = value;
    }

    /**
     * Obtém o valor da propriedade isReportableQuantity.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsReportableQuantity() {
        return isReportableQuantity;
    }

    /**
     * Define o valor da propriedade isReportableQuantity.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsReportableQuantity(String value) {
        this.isReportableQuantity = value;
    }

    /**
     * Obtém o valor da propriedade rqTechnicalName1Gid.
     * 
     * @return
     *     possible object is
     *     {@link TechnicalNameGidType }
     *     
     */
    public TechnicalNameGidType getRqTechnicalName1Gid() {
        return rqTechnicalName1Gid;
    }

    /**
     * Define o valor da propriedade rqTechnicalName1Gid.
     * 
     * @param value
     *     allowed object is
     *     {@link TechnicalNameGidType }
     *     
     */
    public void setRqTechnicalName1Gid(TechnicalNameGidType value) {
        this.rqTechnicalName1Gid = value;
    }

    /**
     * Obtém o valor da propriedade rqTechnicalName2Gid.
     * 
     * @return
     *     possible object is
     *     {@link TechnicalNameGidType }
     *     
     */
    public TechnicalNameGidType getRqTechnicalName2Gid() {
        return rqTechnicalName2Gid;
    }

    /**
     * Define o valor da propriedade rqTechnicalName2Gid.
     * 
     * @param value
     *     allowed object is
     *     {@link TechnicalNameGidType }
     *     
     */
    public void setRqTechnicalName2Gid(TechnicalNameGidType value) {
        this.rqTechnicalName2Gid = value;
    }

    /**
     * Obtém o valor da propriedade isNos.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsNos() {
        return isNos;
    }

    /**
     * Define o valor da propriedade isNos.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsNos(String value) {
        this.isNos = value;
    }

    /**
     * Obtém o valor da propriedade nosTechnicalName1Gid.
     * 
     * @return
     *     possible object is
     *     {@link TechnicalNameGidType }
     *     
     */
    public TechnicalNameGidType getNosTechnicalName1Gid() {
        return nosTechnicalName1Gid;
    }

    /**
     * Define o valor da propriedade nosTechnicalName1Gid.
     * 
     * @param value
     *     allowed object is
     *     {@link TechnicalNameGidType }
     *     
     */
    public void setNosTechnicalName1Gid(TechnicalNameGidType value) {
        this.nosTechnicalName1Gid = value;
    }

    /**
     * Obtém o valor da propriedade nosTechnicalName2Gid.
     * 
     * @return
     *     possible object is
     *     {@link TechnicalNameGidType }
     *     
     */
    public TechnicalNameGidType getNosTechnicalName2Gid() {
        return nosTechnicalName2Gid;
    }

    /**
     * Define o valor da propriedade nosTechnicalName2Gid.
     * 
     * @param value
     *     allowed object is
     *     {@link TechnicalNameGidType }
     *     
     */
    public void setNosTechnicalName2Gid(TechnicalNameGidType value) {
        this.nosTechnicalName2Gid = value;
    }

    /**
     * Obtém o valor da propriedade isToxicInhalation.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsToxicInhalation() {
        return isToxicInhalation;
    }

    /**
     * Define o valor da propriedade isToxicInhalation.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsToxicInhalation(String value) {
        this.isToxicInhalation = value;
    }

    /**
     * Obtém o valor da propriedade inhalationHazardZone.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInhalationHazardZone() {
        return inhalationHazardZone;
    }

    /**
     * Define o valor da propriedade inhalationHazardZone.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInhalationHazardZone(String value) {
        this.inhalationHazardZone = value;
    }

    /**
     * Obtém o valor da propriedade isPassengerAircraftForbid.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsPassengerAircraftForbid() {
        return isPassengerAircraftForbid;
    }

    /**
     * Define o valor da propriedade isPassengerAircraftForbid.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsPassengerAircraftForbid(String value) {
        this.isPassengerAircraftForbid = value;
    }

    /**
     * Obtém o valor da propriedade isCommercialAircraftForbid.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsCommercialAircraftForbid() {
        return isCommercialAircraftForbid;
    }

    /**
     * Define o valor da propriedade isCommercialAircraftForbid.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsCommercialAircraftForbid(String value) {
        this.isCommercialAircraftForbid = value;
    }

    /**
     * Obtém o valor da propriedade flashPoint.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLTemperatureType }
     *     
     */
    public GLogXMLTemperatureType getFlashPoint() {
        return flashPoint;
    }

    /**
     * Define o valor da propriedade flashPoint.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLTemperatureType }
     *     
     */
    public void setFlashPoint(GLogXMLTemperatureType value) {
        this.flashPoint = value;
    }

    /**
     * Obtém o valor da propriedade emergencyTemperature.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLTemperatureType }
     *     
     */
    public GLogXMLTemperatureType getEmergencyTemperature() {
        return emergencyTemperature;
    }

    /**
     * Define o valor da propriedade emergencyTemperature.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLTemperatureType }
     *     
     */
    public void setEmergencyTemperature(GLogXMLTemperatureType value) {
        this.emergencyTemperature = value;
    }

    /**
     * Obtém o valor da propriedade controlTemperature.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLTemperatureType }
     *     
     */
    public GLogXMLTemperatureType getControlTemperature() {
        return controlTemperature;
    }

    /**
     * Define o valor da propriedade controlTemperature.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLTemperatureType }
     *     
     */
    public void setControlTemperature(GLogXMLTemperatureType value) {
        this.controlTemperature = value;
    }

    /**
     * Obtém o valor da propriedade isMarinePollutant.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsMarinePollutant() {
        return isMarinePollutant;
    }

    /**
     * Define o valor da propriedade isMarinePollutant.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsMarinePollutant(String value) {
        this.isMarinePollutant = value;
    }

    /**
     * Obtém o valor da propriedade mpTechnicalName1Gid.
     * 
     * @return
     *     possible object is
     *     {@link TechnicalNameGidType }
     *     
     */
    public TechnicalNameGidType getMpTechnicalName1Gid() {
        return mpTechnicalName1Gid;
    }

    /**
     * Define o valor da propriedade mpTechnicalName1Gid.
     * 
     * @param value
     *     allowed object is
     *     {@link TechnicalNameGidType }
     *     
     */
    public void setMpTechnicalName1Gid(TechnicalNameGidType value) {
        this.mpTechnicalName1Gid = value;
    }

    /**
     * Obtém o valor da propriedade mpTechnicalName2Gid.
     * 
     * @return
     *     possible object is
     *     {@link TechnicalNameGidType }
     *     
     */
    public TechnicalNameGidType getMpTechnicalName2Gid() {
        return mpTechnicalName2Gid;
    }

    /**
     * Define o valor da propriedade mpTechnicalName2Gid.
     * 
     * @param value
     *     allowed object is
     *     {@link TechnicalNameGidType }
     *     
     */
    public void setMpTechnicalName2Gid(TechnicalNameGidType value) {
        this.mpTechnicalName2Gid = value;
    }

    /**
     * Obtém o valor da propriedade isOilContained.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsOilContained() {
        return isOilContained;
    }

    /**
     * Define o valor da propriedade isOilContained.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsOilContained(String value) {
        this.isOilContained = value;
    }

    /**
     * Obtém o valor da propriedade isAllPacked.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsAllPacked() {
        return isAllPacked;
    }

    /**
     * Define o valor da propriedade isAllPacked.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsAllPacked(String value) {
        this.isAllPacked = value;
    }

    /**
     * Obtém o valor da propriedade authorization.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAuthorization() {
        return authorization;
    }

    /**
     * Define o valor da propriedade authorization.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAuthorization(String value) {
        this.authorization = value;
    }

    /**
     * Obtém o valor da propriedade chemicalFormula.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChemicalFormula() {
        return chemicalFormula;
    }

    /**
     * Define o valor da propriedade chemicalFormula.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChemicalFormula(String value) {
        this.chemicalFormula = value;
    }

    /**
     * Obtém o valor da propriedade concentrationPercent.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConcentrationPercent() {
        return concentrationPercent;
    }

    /**
     * Define o valor da propriedade concentrationPercent.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConcentrationPercent(String value) {
        this.concentrationPercent = value;
    }

    /**
     * Obtém o valor da propriedade critSafetyIndex.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCritSafetyIndex() {
        return critSafetyIndex;
    }

    /**
     * Define o valor da propriedade critSafetyIndex.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCritSafetyIndex(String value) {
        this.critSafetyIndex = value;
    }

    /**
     * Obtém o valor da propriedade idgAddDescInfo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIDGAddDescInfo() {
        return idgAddDescInfo;
    }

    /**
     * Define o valor da propriedade idgAddDescInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIDGAddDescInfo(String value) {
        this.idgAddDescInfo = value;
    }

    /**
     * Obtém o valor da propriedade maxQuantity.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMaxQuantity() {
        return maxQuantity;
    }

    /**
     * Define o valor da propriedade maxQuantity.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMaxQuantity(String value) {
        this.maxQuantity = value;
    }

    /**
     * Obtém o valor da propriedade outerPackagingType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOuterPackagingType() {
        return outerPackagingType;
    }

    /**
     * Define o valor da propriedade outerPackagingType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOuterPackagingType(String value) {
        this.outerPackagingType = value;
    }

    /**
     * Obtém o valor da propriedade outerPackingCount.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOuterPackingCount() {
        return outerPackingCount;
    }

    /**
     * Define o valor da propriedade outerPackingCount.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOuterPackingCount(String value) {
        this.outerPackingCount = value;
    }

    /**
     * Obtém o valor da propriedade hazmatPackageType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHazmatPackageType() {
        return hazmatPackageType;
    }

    /**
     * Define o valor da propriedade hazmatPackageType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHazmatPackageType(String value) {
        this.hazmatPackageType = value;
    }

    /**
     * Obtém o valor da propriedade packingCount.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPackingCount() {
        return packingCount;
    }

    /**
     * Define o valor da propriedade packingCount.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPackingCount(String value) {
        this.packingCount = value;
    }

    /**
     * Obtém o valor da propriedade packingInstructions.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPackingInstructions() {
        return packingInstructions;
    }

    /**
     * Define o valor da propriedade packingInstructions.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPackingInstructions(String value) {
        this.packingInstructions = value;
    }

    /**
     * Obtém o valor da propriedade physicalForm.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPhysicalForm() {
        return physicalForm;
    }

    /**
     * Define o valor da propriedade physicalForm.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPhysicalForm(String value) {
        this.physicalForm = value;
    }

    /**
     * Obtém o valor da propriedade qValue.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQValue() {
        return qValue;
    }

    /**
     * Define o valor da propriedade qValue.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQValue(String value) {
        this.qValue = value;
    }

    /**
     * Obtém o valor da propriedade hazQuantity.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHazQuantity() {
        return hazQuantity;
    }

    /**
     * Define o valor da propriedade hazQuantity.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHazQuantity(String value) {
        this.hazQuantity = value;
    }

    /**
     * Obtém o valor da propriedade radioactiveLabel.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRadioactiveLabel() {
        return radioactiveLabel;
    }

    /**
     * Define o valor da propriedade radioactiveLabel.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRadioactiveLabel(String value) {
        this.radioactiveLabel = value;
    }

    /**
     * Obtém o valor da propriedade radPackaging.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRadPackaging() {
        return radPackaging;
    }

    /**
     * Define o valor da propriedade radPackaging.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRadPackaging(String value) {
        this.radPackaging = value;
    }

    /**
     * Obtém o valor da propriedade radioactivity.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRadioactivity() {
        return radioactivity;
    }

    /**
     * Define o valor da propriedade radioactivity.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRadioactivity(String value) {
        this.radioactivity = value;
    }

    /**
     * Obtém o valor da propriedade radioactivityUnits.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRadioactivityUnits() {
        return radioactivityUnits;
    }

    /**
     * Define o valor da propriedade radioactivityUnits.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRadioactivityUnits(String value) {
        this.radioactivityUnits = value;
    }

    /**
     * Obtém o valor da propriedade radionuclide.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRadionuclide() {
        return radionuclide;
    }

    /**
     * Define o valor da propriedade radionuclide.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRadionuclide(String value) {
        this.radionuclide = value;
    }

    /**
     * Obtém o valor da propriedade substanceNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubstanceNumber() {
        return substanceNumber;
    }

    /**
     * Define o valor da propriedade substanceNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubstanceNumber(String value) {
        this.substanceNumber = value;
    }

    /**
     * Obtém o valor da propriedade surfaceReading.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSurfaceReading() {
        return surfaceReading;
    }

    /**
     * Define o valor da propriedade surfaceReading.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSurfaceReading(String value) {
        this.surfaceReading = value;
    }

    /**
     * Obtém o valor da propriedade transportIndex.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransportIndex() {
        return transportIndex;
    }

    /**
     * Define o valor da propriedade transportIndex.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransportIndex(String value) {
        this.transportIndex = value;
    }

    /**
     * Obtém o valor da propriedade materialsType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMaterialsType() {
        return materialsType;
    }

    /**
     * Define o valor da propriedade materialsType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMaterialsType(String value) {
        this.materialsType = value;
    }

    /**
     * Obtém o valor da propriedade units.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUnits() {
        return units;
    }

    /**
     * Define o valor da propriedade units.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUnits(String value) {
        this.units = value;
    }

    /**
     * Obtém o valor da propriedade externalCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExternalCode() {
        return externalCode;
    }

    /**
     * Define o valor da propriedade externalCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExternalCode(String value) {
        this.externalCode = value;
    }

    /**
     * Obtém o valor da propriedade isOverpack.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsOverpack() {
        return isOverpack;
    }

    /**
     * Define o valor da propriedade isOverpack.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsOverpack(String value) {
        this.isOverpack = value;
    }

}
