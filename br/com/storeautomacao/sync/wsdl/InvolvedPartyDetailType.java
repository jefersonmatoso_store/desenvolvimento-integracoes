
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * (Both) This tag will hold details of party involved.
 *          
 * 
 * <p>Classe Java de InvolvedPartyDetailType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="InvolvedPartyDetailType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="InvolvedPartyQualGid" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ContactGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InvolvedPartyDetailType", namespace = "http://xmlns.oracle.com/apps/gtm/transmission/v6.4", propOrder = {
    "involvedPartyQualGid",
    "contactGid"
})
public class InvolvedPartyDetailType {

    @XmlElement(name = "InvolvedPartyQualGid", required = true)
    protected String involvedPartyQualGid;
    @XmlElement(name = "ContactGid", required = true)
    protected GLogXMLGidType contactGid;

    /**
     * Obtém o valor da propriedade involvedPartyQualGid.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInvolvedPartyQualGid() {
        return involvedPartyQualGid;
    }

    /**
     * Define o valor da propriedade involvedPartyQualGid.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInvolvedPartyQualGid(String value) {
        this.involvedPartyQualGid = value;
    }

    /**
     * Obtém o valor da propriedade contactGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getContactGid() {
        return contactGid;
    }

    /**
     * Define o valor da propriedade contactGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setContactGid(GLogXMLGidType value) {
        this.contactGid = value;
    }

}
