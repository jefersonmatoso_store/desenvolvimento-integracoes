
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Specifies the user defined icon info. Validation: References ICON table.
 * 
 * <p>Classe Java de UserDefIconInfoType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="UserDefIconInfoType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="UserDef1IconGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="UserDef2IconGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="UserDef3IconGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="UserDef4IconGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="UserDef5IconGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserDefIconInfoType", propOrder = {
    "userDef1IconGid",
    "userDef2IconGid",
    "userDef3IconGid",
    "userDef4IconGid",
    "userDef5IconGid"
})
public class UserDefIconInfoType {

    @XmlElement(name = "UserDef1IconGid")
    protected GLogXMLGidType userDef1IconGid;
    @XmlElement(name = "UserDef2IconGid")
    protected GLogXMLGidType userDef2IconGid;
    @XmlElement(name = "UserDef3IconGid")
    protected GLogXMLGidType userDef3IconGid;
    @XmlElement(name = "UserDef4IconGid")
    protected GLogXMLGidType userDef4IconGid;
    @XmlElement(name = "UserDef5IconGid")
    protected GLogXMLGidType userDef5IconGid;

    /**
     * Obtém o valor da propriedade userDef1IconGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getUserDef1IconGid() {
        return userDef1IconGid;
    }

    /**
     * Define o valor da propriedade userDef1IconGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setUserDef1IconGid(GLogXMLGidType value) {
        this.userDef1IconGid = value;
    }

    /**
     * Obtém o valor da propriedade userDef2IconGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getUserDef2IconGid() {
        return userDef2IconGid;
    }

    /**
     * Define o valor da propriedade userDef2IconGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setUserDef2IconGid(GLogXMLGidType value) {
        this.userDef2IconGid = value;
    }

    /**
     * Obtém o valor da propriedade userDef3IconGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getUserDef3IconGid() {
        return userDef3IconGid;
    }

    /**
     * Define o valor da propriedade userDef3IconGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setUserDef3IconGid(GLogXMLGidType value) {
        this.userDef3IconGid = value;
    }

    /**
     * Obtém o valor da propriedade userDef4IconGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getUserDef4IconGid() {
        return userDef4IconGid;
    }

    /**
     * Define o valor da propriedade userDef4IconGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setUserDef4IconGid(GLogXMLGidType value) {
        this.userDef4IconGid = value;
    }

    /**
     * Obtém o valor da propriedade userDef5IconGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getUserDef5IconGid() {
        return userDef5IconGid;
    }

    /**
     * Define o valor da propriedade userDef5IconGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setUserDef5IconGid(GLogXMLGidType value) {
        this.userDef5IconGid = value;
    }

}
