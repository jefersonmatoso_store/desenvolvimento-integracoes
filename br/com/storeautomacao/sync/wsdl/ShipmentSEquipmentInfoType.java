
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Represents the attributes of the association of the SEquipment on the Shipment.
 * 
 * <p>Classe Java de ShipmentSEquipmentInfoType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="ShipmentSEquipmentInfoType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CapacityUsageGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="PickupStopNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DropoffStopNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SEquipmentIndex" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ShipmentSEquipmentInfoType", propOrder = {
    "capacityUsageGid",
    "pickupStopNum",
    "dropoffStopNum",
    "sEquipmentIndex"
})
public class ShipmentSEquipmentInfoType {

    @XmlElement(name = "CapacityUsageGid")
    protected GLogXMLGidType capacityUsageGid;
    @XmlElement(name = "PickupStopNum")
    protected String pickupStopNum;
    @XmlElement(name = "DropoffStopNum")
    protected String dropoffStopNum;
    @XmlElement(name = "SEquipmentIndex")
    protected String sEquipmentIndex;

    /**
     * Obtém o valor da propriedade capacityUsageGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getCapacityUsageGid() {
        return capacityUsageGid;
    }

    /**
     * Define o valor da propriedade capacityUsageGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setCapacityUsageGid(GLogXMLGidType value) {
        this.capacityUsageGid = value;
    }

    /**
     * Obtém o valor da propriedade pickupStopNum.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPickupStopNum() {
        return pickupStopNum;
    }

    /**
     * Define o valor da propriedade pickupStopNum.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPickupStopNum(String value) {
        this.pickupStopNum = value;
    }

    /**
     * Obtém o valor da propriedade dropoffStopNum.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDropoffStopNum() {
        return dropoffStopNum;
    }

    /**
     * Define o valor da propriedade dropoffStopNum.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDropoffStopNum(String value) {
        this.dropoffStopNum = value;
    }

    /**
     * Obtém o valor da propriedade sEquipmentIndex.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSEquipmentIndex() {
        return sEquipmentIndex;
    }

    /**
     * Define o valor da propriedade sEquipmentIndex.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSEquipmentIndex(String value) {
        this.sEquipmentIndex = value;
    }

}
