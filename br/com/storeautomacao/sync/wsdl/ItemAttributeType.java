
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Contains additional information about the the line item.
 * 
 * <p>Classe Java de ItemAttributeType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="ItemAttributeType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ItemFeatureQualGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="ItemFeatureValue" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ItemAttributeType", propOrder = {
    "itemFeatureQualGid",
    "itemFeatureValue"
})
public class ItemAttributeType {

    @XmlElement(name = "ItemFeatureQualGid", required = true)
    protected GLogXMLGidType itemFeatureQualGid;
    @XmlElement(name = "ItemFeatureValue", required = true)
    protected String itemFeatureValue;

    /**
     * Obtém o valor da propriedade itemFeatureQualGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getItemFeatureQualGid() {
        return itemFeatureQualGid;
    }

    /**
     * Define o valor da propriedade itemFeatureQualGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setItemFeatureQualGid(GLogXMLGidType value) {
        this.itemFeatureQualGid = value;
    }

    /**
     * Obtém o valor da propriedade itemFeatureValue.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItemFeatureValue() {
        return itemFeatureValue;
    }

    /**
     * Define o valor da propriedade itemFeatureValue.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItemFeatureValue(String value) {
        this.itemFeatureValue = value;
    }

}
