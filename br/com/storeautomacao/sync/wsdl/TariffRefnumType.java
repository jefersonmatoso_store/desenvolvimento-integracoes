
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 *             TariffRefnum is an element of FP_Tariff used to identify the tariff.
 *          
 * 
 * <p>Classe Java de TariffRefnumType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="TariffRefnumType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TariffRefnumValue" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="TariffRefnumQualifier" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ContractSuffix" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TariffRefnumType", propOrder = {
    "tariffRefnumValue",
    "tariffRefnumQualifier",
    "contractSuffix"
})
public class TariffRefnumType {

    @XmlElement(name = "TariffRefnumValue", required = true)
    protected String tariffRefnumValue;
    @XmlElement(name = "TariffRefnumQualifier", required = true)
    protected String tariffRefnumQualifier;
    @XmlElement(name = "ContractSuffix", required = true)
    protected String contractSuffix;

    /**
     * Obtém o valor da propriedade tariffRefnumValue.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTariffRefnumValue() {
        return tariffRefnumValue;
    }

    /**
     * Define o valor da propriedade tariffRefnumValue.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTariffRefnumValue(String value) {
        this.tariffRefnumValue = value;
    }

    /**
     * Obtém o valor da propriedade tariffRefnumQualifier.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTariffRefnumQualifier() {
        return tariffRefnumQualifier;
    }

    /**
     * Define o valor da propriedade tariffRefnumQualifier.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTariffRefnumQualifier(String value) {
        this.tariffRefnumQualifier = value;
    }

    /**
     * Obtém o valor da propriedade contractSuffix.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContractSuffix() {
        return contractSuffix;
    }

    /**
     * Define o valor da propriedade contractSuffix.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContractSuffix(String value) {
        this.contractSuffix = value;
    }

}
