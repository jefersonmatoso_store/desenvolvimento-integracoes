
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Classe Java de ReleaseAllocType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="ReleaseAllocType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AllocTypeQualGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="ReleaseAllocShipment" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ReleaseAllocShipmentType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReleaseAllocType", propOrder = {
    "allocTypeQualGid",
    "releaseAllocShipment"
})
public class ReleaseAllocType {

    @XmlElement(name = "AllocTypeQualGid", required = true)
    protected GLogXMLGidType allocTypeQualGid;
    @XmlElement(name = "ReleaseAllocShipment")
    protected List<ReleaseAllocShipmentType> releaseAllocShipment;

    /**
     * Obtém o valor da propriedade allocTypeQualGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getAllocTypeQualGid() {
        return allocTypeQualGid;
    }

    /**
     * Define o valor da propriedade allocTypeQualGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setAllocTypeQualGid(GLogXMLGidType value) {
        this.allocTypeQualGid = value;
    }

    /**
     * Gets the value of the releaseAllocShipment property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the releaseAllocShipment property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReleaseAllocShipment().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ReleaseAllocShipmentType }
     * 
     * 
     */
    public List<ReleaseAllocShipmentType> getReleaseAllocShipment() {
        if (releaseAllocShipment == null) {
            releaseAllocShipment = new ArrayList<ReleaseAllocShipmentType>();
        }
        return this.releaseAllocShipment;
    }

}
