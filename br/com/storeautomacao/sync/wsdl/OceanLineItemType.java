
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 *             OceanLineItem is an element of OceanDetail used to specify invoice line items
 *             for ocean invoices.
 *          
 * 
 * <p>Classe Java de OceanLineItemType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="OceanLineItemType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AssignedNum" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CommonInvoiceLineElements" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}CommonInvoiceLineElementsType"/>
 *         &lt;element name="ExportImportLicense" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ExportImportLicenseType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OceanLineItemType", propOrder = {
    "assignedNum",
    "commonInvoiceLineElements",
    "exportImportLicense"
})
public class OceanLineItemType {

    @XmlElement(name = "AssignedNum", required = true)
    protected String assignedNum;
    @XmlElement(name = "CommonInvoiceLineElements", required = true)
    protected CommonInvoiceLineElementsType commonInvoiceLineElements;
    @XmlElement(name = "ExportImportLicense")
    protected ExportImportLicenseType exportImportLicense;

    /**
     * Obtém o valor da propriedade assignedNum.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAssignedNum() {
        return assignedNum;
    }

    /**
     * Define o valor da propriedade assignedNum.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAssignedNum(String value) {
        this.assignedNum = value;
    }

    /**
     * Obtém o valor da propriedade commonInvoiceLineElements.
     * 
     * @return
     *     possible object is
     *     {@link CommonInvoiceLineElementsType }
     *     
     */
    public CommonInvoiceLineElementsType getCommonInvoiceLineElements() {
        return commonInvoiceLineElements;
    }

    /**
     * Define o valor da propriedade commonInvoiceLineElements.
     * 
     * @param value
     *     allowed object is
     *     {@link CommonInvoiceLineElementsType }
     *     
     */
    public void setCommonInvoiceLineElements(CommonInvoiceLineElementsType value) {
        this.commonInvoiceLineElements = value;
    }

    /**
     * Obtém o valor da propriedade exportImportLicense.
     * 
     * @return
     *     possible object is
     *     {@link ExportImportLicenseType }
     *     
     */
    public ExportImportLicenseType getExportImportLicense() {
        return exportImportLicense;
    }

    /**
     * Define o valor da propriedade exportImportLicense.
     * 
     * @param value
     *     allowed object is
     *     {@link ExportImportLicenseType }
     *     
     */
    public void setExportImportLicense(ExportImportLicenseType value) {
        this.exportImportLicense = value;
    }

}
