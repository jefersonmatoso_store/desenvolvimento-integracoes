
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Provides a breakdown of the shipment costs based on the shipment qualifiers.
 *             For example Item cost, order base cost, order release cost, order release line cost etc.
 *          
 * 
 * <p>Classe Java de ShipmentCostRefType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="ShipmentCostRefType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SequenceNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CostReferenceGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="ShipmentCostQualGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="IsPickup" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsDropoff" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ShipmentCostRefType", propOrder = {
    "sequenceNumber",
    "costReferenceGid",
    "shipmentCostQualGid",
    "isPickup",
    "isDropoff"
})
public class ShipmentCostRefType {

    @XmlElement(name = "SequenceNumber", required = true)
    protected String sequenceNumber;
    @XmlElement(name = "CostReferenceGid", required = true)
    protected GLogXMLGidType costReferenceGid;
    @XmlElement(name = "ShipmentCostQualGid", required = true)
    protected GLogXMLGidType shipmentCostQualGid;
    @XmlElement(name = "IsPickup")
    protected String isPickup;
    @XmlElement(name = "IsDropoff")
    protected String isDropoff;

    /**
     * Obtém o valor da propriedade sequenceNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSequenceNumber() {
        return sequenceNumber;
    }

    /**
     * Define o valor da propriedade sequenceNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSequenceNumber(String value) {
        this.sequenceNumber = value;
    }

    /**
     * Obtém o valor da propriedade costReferenceGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getCostReferenceGid() {
        return costReferenceGid;
    }

    /**
     * Define o valor da propriedade costReferenceGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setCostReferenceGid(GLogXMLGidType value) {
        this.costReferenceGid = value;
    }

    /**
     * Obtém o valor da propriedade shipmentCostQualGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getShipmentCostQualGid() {
        return shipmentCostQualGid;
    }

    /**
     * Define o valor da propriedade shipmentCostQualGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setShipmentCostQualGid(GLogXMLGidType value) {
        this.shipmentCostQualGid = value;
    }

    /**
     * Obtém o valor da propriedade isPickup.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsPickup() {
        return isPickup;
    }

    /**
     * Define o valor da propriedade isPickup.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsPickup(String value) {
        this.isPickup = value;
    }

    /**
     * Obtém o valor da propriedade isDropoff.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsDropoff() {
        return isDropoff;
    }

    /**
     * Define o valor da propriedade isDropoff.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsDropoff(String value) {
        this.isDropoff = value;
    }

}
