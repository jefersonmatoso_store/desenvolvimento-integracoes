
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;


/**
 * 
 *             The PaymentHeader information includes the invoice reference information.
 *             The TransactionCode within the PaymentHeader is optional, and defaults to 'I' for insert when not specified.
 *          
 * 
 * <p>Classe Java de PaymentHeaderType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="PaymentHeaderType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SendReason" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}SendReasonType" minOccurs="0"/>
 *         &lt;element name="DomainName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="InvoiceGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="TransactionCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransactionCodeType" minOccurs="0"/>
 *         &lt;element name="ReplaceChildren" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ReplaceChildrenType" minOccurs="0"/>
 *         &lt;element name="InvoiceNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InvoicingProcess" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InvoiceDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType"/>
 *         &lt;element name="InvoiceCorrectionCodeIdentifier" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InvoiceRefnum" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}InvoiceRefnumType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="ServiceProviderGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="ServiceProviderAlias" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ServiceProviderAliasType"/>
 *         &lt;element name="InvolvedParty" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}InvolvedPartyType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Remark" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RemarkType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Route" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RouteType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="PaymentMethodCodeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="StartDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="EndDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="GlobalCurrencyCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ExchangeRateInfo" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ExchangeRateInfoType" minOccurs="0"/>
 *         &lt;element name="Terms" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TermsType" minOccurs="0"/>
 *         &lt;element name="NetAmountDue" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/>
 *         &lt;element name="NetDueDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="DateReceived" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="GLDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="SupplyCountryCode3Gid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="ServProvVatRegNoGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="CustomerVatRegNoGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="VatExemptValue" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsVatAnalysisFixed" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NetAmountDueWithTax" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/>
 *         &lt;element name="IsPassThrough" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ConsolidationType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ParentInvoice" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ParentInvoiceType" minOccurs="0"/>
 *         &lt;element name="IsCostFixed" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BaseCharge" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/>
 *         &lt;element name="OtherCharge" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/>
 *         &lt;element name="IsCreditNote" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SailDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="SailCutoffDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="RailDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="IsHazardous" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsTemperatureControl" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SupplyProvinceVatReg" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ProvinceVatRegType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="VatAnalysis" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}VatAnalysisType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Text" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TextType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="FlexFieldStrings" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldStringType" minOccurs="0"/>
 *         &lt;element name="FlexFieldNumbers" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldNumberType" minOccurs="0"/>
 *         &lt;element name="FlexFieldDates" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldDateType" minOccurs="0"/>
 *         &lt;element name="FlexFieldCurrencies" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldCurrencyType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PaymentHeaderType", propOrder = {
    "sendReason",
    "domainName",
    "invoiceGid",
    "transactionCode",
    "replaceChildren",
    "invoiceNum",
    "invoicingProcess",
    "invoiceDate",
    "invoiceCorrectionCodeIdentifier",
    "invoiceRefnum",
    "serviceProviderGid",
    "serviceProviderAlias",
    "involvedParty",
    "remark",
    "route",
    "paymentMethodCodeGid",
    "startDt",
    "endDt",
    "globalCurrencyCode",
    "exchangeRateInfo",
    "terms",
    "netAmountDue",
    "netDueDate",
    "dateReceived",
    "glDate",
    "supplyCountryCode3Gid",
    "servProvVatRegNoGid",
    "customerVatRegNoGid",
    "vatExemptValue",
    "isVatAnalysisFixed",
    "netAmountDueWithTax",
    "isPassThrough",
    "consolidationType",
    "parentInvoice",
    "isCostFixed",
    "baseCharge",
    "otherCharge",
    "isCreditNote",
    "sailDt",
    "sailCutoffDt",
    "railDt",
    "isHazardous",
    "isTemperatureControl",
    "supplyProvinceVatReg",
    "vatAnalysis",
    "text",
    "flexFieldStrings",
    "flexFieldNumbers",
    "flexFieldDates",
    "flexFieldCurrencies"
})
public class PaymentHeaderType {

    @XmlElement(name = "SendReason")
    protected SendReasonType sendReason;
    @XmlElement(name = "DomainName", required = true)
    protected String domainName;
    @XmlElement(name = "InvoiceGid")
    protected GLogXMLGidType invoiceGid;
    @XmlElement(name = "TransactionCode")
    @XmlSchemaType(name = "string")
    protected TransactionCodeType transactionCode;
    @XmlElement(name = "ReplaceChildren")
    protected ReplaceChildrenType replaceChildren;
    @XmlElement(name = "InvoiceNum")
    protected String invoiceNum;
    @XmlElement(name = "InvoicingProcess")
    protected String invoicingProcess;
    @XmlElement(name = "InvoiceDate", required = true)
    protected GLogDateTimeType invoiceDate;
    @XmlElement(name = "InvoiceCorrectionCodeIdentifier")
    protected String invoiceCorrectionCodeIdentifier;
    @XmlElement(name = "InvoiceRefnum")
    protected List<InvoiceRefnumType> invoiceRefnum;
    @XmlElement(name = "ServiceProviderGid")
    protected GLogXMLGidType serviceProviderGid;
    @XmlElement(name = "ServiceProviderAlias", required = true)
    protected ServiceProviderAliasType serviceProviderAlias;
    @XmlElement(name = "InvolvedParty")
    protected List<InvolvedPartyType> involvedParty;
    @XmlElement(name = "Remark")
    protected List<RemarkType> remark;
    @XmlElement(name = "Route")
    protected List<RouteType> route;
    @XmlElement(name = "PaymentMethodCodeGid")
    protected GLogXMLGidType paymentMethodCodeGid;
    @XmlElement(name = "StartDt")
    protected GLogDateTimeType startDt;
    @XmlElement(name = "EndDt")
    protected GLogDateTimeType endDt;
    @XmlElement(name = "GlobalCurrencyCode")
    protected String globalCurrencyCode;
    @XmlElement(name = "ExchangeRateInfo")
    protected ExchangeRateInfoType exchangeRateInfo;
    @XmlElement(name = "Terms")
    protected TermsType terms;
    @XmlElement(name = "NetAmountDue")
    protected GLogXMLFinancialAmountType netAmountDue;
    @XmlElement(name = "NetDueDate")
    protected GLogDateTimeType netDueDate;
    @XmlElement(name = "DateReceived")
    protected GLogDateTimeType dateReceived;
    @XmlElement(name = "GLDate")
    protected GLogDateTimeType glDate;
    @XmlElement(name = "SupplyCountryCode3Gid")
    protected GLogXMLGidType supplyCountryCode3Gid;
    @XmlElement(name = "ServProvVatRegNoGid")
    protected GLogXMLGidType servProvVatRegNoGid;
    @XmlElement(name = "CustomerVatRegNoGid")
    protected GLogXMLGidType customerVatRegNoGid;
    @XmlElement(name = "VatExemptValue")
    protected String vatExemptValue;
    @XmlElement(name = "IsVatAnalysisFixed")
    protected String isVatAnalysisFixed;
    @XmlElement(name = "NetAmountDueWithTax")
    protected GLogXMLFinancialAmountType netAmountDueWithTax;
    @XmlElement(name = "IsPassThrough")
    protected String isPassThrough;
    @XmlElement(name = "ConsolidationType")
    protected String consolidationType;
    @XmlElement(name = "ParentInvoice")
    protected ParentInvoiceType parentInvoice;
    @XmlElement(name = "IsCostFixed")
    protected String isCostFixed;
    @XmlElement(name = "BaseCharge")
    protected GLogXMLFinancialAmountType baseCharge;
    @XmlElement(name = "OtherCharge")
    protected GLogXMLFinancialAmountType otherCharge;
    @XmlElement(name = "IsCreditNote")
    protected String isCreditNote;
    @XmlElement(name = "SailDt")
    protected GLogDateTimeType sailDt;
    @XmlElement(name = "SailCutoffDt")
    protected GLogDateTimeType sailCutoffDt;
    @XmlElement(name = "RailDt")
    protected GLogDateTimeType railDt;
    @XmlElement(name = "IsHazardous")
    protected String isHazardous;
    @XmlElement(name = "IsTemperatureControl")
    protected String isTemperatureControl;
    @XmlElement(name = "SupplyProvinceVatReg")
    protected List<ProvinceVatRegType> supplyProvinceVatReg;
    @XmlElement(name = "VatAnalysis")
    protected List<VatAnalysisType> vatAnalysis;
    @XmlElement(name = "Text")
    protected List<TextType> text;
    @XmlElement(name = "FlexFieldStrings")
    protected FlexFieldStringType flexFieldStrings;
    @XmlElement(name = "FlexFieldNumbers")
    protected FlexFieldNumberType flexFieldNumbers;
    @XmlElement(name = "FlexFieldDates")
    protected FlexFieldDateType flexFieldDates;
    @XmlElement(name = "FlexFieldCurrencies")
    protected FlexFieldCurrencyType flexFieldCurrencies;

    /**
     * Obtém o valor da propriedade sendReason.
     * 
     * @return
     *     possible object is
     *     {@link SendReasonType }
     *     
     */
    public SendReasonType getSendReason() {
        return sendReason;
    }

    /**
     * Define o valor da propriedade sendReason.
     * 
     * @param value
     *     allowed object is
     *     {@link SendReasonType }
     *     
     */
    public void setSendReason(SendReasonType value) {
        this.sendReason = value;
    }

    /**
     * Obtém o valor da propriedade domainName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDomainName() {
        return domainName;
    }

    /**
     * Define o valor da propriedade domainName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDomainName(String value) {
        this.domainName = value;
    }

    /**
     * Obtém o valor da propriedade invoiceGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getInvoiceGid() {
        return invoiceGid;
    }

    /**
     * Define o valor da propriedade invoiceGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setInvoiceGid(GLogXMLGidType value) {
        this.invoiceGid = value;
    }

    /**
     * Obtém o valor da propriedade transactionCode.
     * 
     * @return
     *     possible object is
     *     {@link TransactionCodeType }
     *     
     */
    public TransactionCodeType getTransactionCode() {
        return transactionCode;
    }

    /**
     * Define o valor da propriedade transactionCode.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionCodeType }
     *     
     */
    public void setTransactionCode(TransactionCodeType value) {
        this.transactionCode = value;
    }

    /**
     * Obtém o valor da propriedade replaceChildren.
     * 
     * @return
     *     possible object is
     *     {@link ReplaceChildrenType }
     *     
     */
    public ReplaceChildrenType getReplaceChildren() {
        return replaceChildren;
    }

    /**
     * Define o valor da propriedade replaceChildren.
     * 
     * @param value
     *     allowed object is
     *     {@link ReplaceChildrenType }
     *     
     */
    public void setReplaceChildren(ReplaceChildrenType value) {
        this.replaceChildren = value;
    }

    /**
     * Obtém o valor da propriedade invoiceNum.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInvoiceNum() {
        return invoiceNum;
    }

    /**
     * Define o valor da propriedade invoiceNum.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInvoiceNum(String value) {
        this.invoiceNum = value;
    }

    /**
     * Obtém o valor da propriedade invoicingProcess.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInvoicingProcess() {
        return invoicingProcess;
    }

    /**
     * Define o valor da propriedade invoicingProcess.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInvoicingProcess(String value) {
        this.invoicingProcess = value;
    }

    /**
     * Obtém o valor da propriedade invoiceDate.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getInvoiceDate() {
        return invoiceDate;
    }

    /**
     * Define o valor da propriedade invoiceDate.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setInvoiceDate(GLogDateTimeType value) {
        this.invoiceDate = value;
    }

    /**
     * Obtém o valor da propriedade invoiceCorrectionCodeIdentifier.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInvoiceCorrectionCodeIdentifier() {
        return invoiceCorrectionCodeIdentifier;
    }

    /**
     * Define o valor da propriedade invoiceCorrectionCodeIdentifier.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInvoiceCorrectionCodeIdentifier(String value) {
        this.invoiceCorrectionCodeIdentifier = value;
    }

    /**
     * Gets the value of the invoiceRefnum property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the invoiceRefnum property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInvoiceRefnum().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link InvoiceRefnumType }
     * 
     * 
     */
    public List<InvoiceRefnumType> getInvoiceRefnum() {
        if (invoiceRefnum == null) {
            invoiceRefnum = new ArrayList<InvoiceRefnumType>();
        }
        return this.invoiceRefnum;
    }

    /**
     * Obtém o valor da propriedade serviceProviderGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getServiceProviderGid() {
        return serviceProviderGid;
    }

    /**
     * Define o valor da propriedade serviceProviderGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setServiceProviderGid(GLogXMLGidType value) {
        this.serviceProviderGid = value;
    }

    /**
     * Obtém o valor da propriedade serviceProviderAlias.
     * 
     * @return
     *     possible object is
     *     {@link ServiceProviderAliasType }
     *     
     */
    public ServiceProviderAliasType getServiceProviderAlias() {
        return serviceProviderAlias;
    }

    /**
     * Define o valor da propriedade serviceProviderAlias.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceProviderAliasType }
     *     
     */
    public void setServiceProviderAlias(ServiceProviderAliasType value) {
        this.serviceProviderAlias = value;
    }

    /**
     * Gets the value of the involvedParty property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the involvedParty property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInvolvedParty().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link InvolvedPartyType }
     * 
     * 
     */
    public List<InvolvedPartyType> getInvolvedParty() {
        if (involvedParty == null) {
            involvedParty = new ArrayList<InvolvedPartyType>();
        }
        return this.involvedParty;
    }

    /**
     * Gets the value of the remark property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the remark property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRemark().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RemarkType }
     * 
     * 
     */
    public List<RemarkType> getRemark() {
        if (remark == null) {
            remark = new ArrayList<RemarkType>();
        }
        return this.remark;
    }

    /**
     * Gets the value of the route property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the route property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRoute().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RouteType }
     * 
     * 
     */
    public List<RouteType> getRoute() {
        if (route == null) {
            route = new ArrayList<RouteType>();
        }
        return this.route;
    }

    /**
     * Obtém o valor da propriedade paymentMethodCodeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getPaymentMethodCodeGid() {
        return paymentMethodCodeGid;
    }

    /**
     * Define o valor da propriedade paymentMethodCodeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setPaymentMethodCodeGid(GLogXMLGidType value) {
        this.paymentMethodCodeGid = value;
    }

    /**
     * Obtém o valor da propriedade startDt.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getStartDt() {
        return startDt;
    }

    /**
     * Define o valor da propriedade startDt.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setStartDt(GLogDateTimeType value) {
        this.startDt = value;
    }

    /**
     * Obtém o valor da propriedade endDt.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getEndDt() {
        return endDt;
    }

    /**
     * Define o valor da propriedade endDt.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setEndDt(GLogDateTimeType value) {
        this.endDt = value;
    }

    /**
     * Obtém o valor da propriedade globalCurrencyCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGlobalCurrencyCode() {
        return globalCurrencyCode;
    }

    /**
     * Define o valor da propriedade globalCurrencyCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGlobalCurrencyCode(String value) {
        this.globalCurrencyCode = value;
    }

    /**
     * Obtém o valor da propriedade exchangeRateInfo.
     * 
     * @return
     *     possible object is
     *     {@link ExchangeRateInfoType }
     *     
     */
    public ExchangeRateInfoType getExchangeRateInfo() {
        return exchangeRateInfo;
    }

    /**
     * Define o valor da propriedade exchangeRateInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link ExchangeRateInfoType }
     *     
     */
    public void setExchangeRateInfo(ExchangeRateInfoType value) {
        this.exchangeRateInfo = value;
    }

    /**
     * Obtém o valor da propriedade terms.
     * 
     * @return
     *     possible object is
     *     {@link TermsType }
     *     
     */
    public TermsType getTerms() {
        return terms;
    }

    /**
     * Define o valor da propriedade terms.
     * 
     * @param value
     *     allowed object is
     *     {@link TermsType }
     *     
     */
    public void setTerms(TermsType value) {
        this.terms = value;
    }

    /**
     * Obtém o valor da propriedade netAmountDue.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getNetAmountDue() {
        return netAmountDue;
    }

    /**
     * Define o valor da propriedade netAmountDue.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setNetAmountDue(GLogXMLFinancialAmountType value) {
        this.netAmountDue = value;
    }

    /**
     * Obtém o valor da propriedade netDueDate.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getNetDueDate() {
        return netDueDate;
    }

    /**
     * Define o valor da propriedade netDueDate.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setNetDueDate(GLogDateTimeType value) {
        this.netDueDate = value;
    }

    /**
     * Obtém o valor da propriedade dateReceived.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getDateReceived() {
        return dateReceived;
    }

    /**
     * Define o valor da propriedade dateReceived.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setDateReceived(GLogDateTimeType value) {
        this.dateReceived = value;
    }

    /**
     * Obtém o valor da propriedade glDate.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getGLDate() {
        return glDate;
    }

    /**
     * Define o valor da propriedade glDate.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setGLDate(GLogDateTimeType value) {
        this.glDate = value;
    }

    /**
     * Obtém o valor da propriedade supplyCountryCode3Gid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getSupplyCountryCode3Gid() {
        return supplyCountryCode3Gid;
    }

    /**
     * Define o valor da propriedade supplyCountryCode3Gid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setSupplyCountryCode3Gid(GLogXMLGidType value) {
        this.supplyCountryCode3Gid = value;
    }

    /**
     * Obtém o valor da propriedade servProvVatRegNoGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getServProvVatRegNoGid() {
        return servProvVatRegNoGid;
    }

    /**
     * Define o valor da propriedade servProvVatRegNoGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setServProvVatRegNoGid(GLogXMLGidType value) {
        this.servProvVatRegNoGid = value;
    }

    /**
     * Obtém o valor da propriedade customerVatRegNoGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getCustomerVatRegNoGid() {
        return customerVatRegNoGid;
    }

    /**
     * Define o valor da propriedade customerVatRegNoGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setCustomerVatRegNoGid(GLogXMLGidType value) {
        this.customerVatRegNoGid = value;
    }

    /**
     * Obtém o valor da propriedade vatExemptValue.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVatExemptValue() {
        return vatExemptValue;
    }

    /**
     * Define o valor da propriedade vatExemptValue.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVatExemptValue(String value) {
        this.vatExemptValue = value;
    }

    /**
     * Obtém o valor da propriedade isVatAnalysisFixed.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsVatAnalysisFixed() {
        return isVatAnalysisFixed;
    }

    /**
     * Define o valor da propriedade isVatAnalysisFixed.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsVatAnalysisFixed(String value) {
        this.isVatAnalysisFixed = value;
    }

    /**
     * Obtém o valor da propriedade netAmountDueWithTax.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getNetAmountDueWithTax() {
        return netAmountDueWithTax;
    }

    /**
     * Define o valor da propriedade netAmountDueWithTax.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setNetAmountDueWithTax(GLogXMLFinancialAmountType value) {
        this.netAmountDueWithTax = value;
    }

    /**
     * Obtém o valor da propriedade isPassThrough.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsPassThrough() {
        return isPassThrough;
    }

    /**
     * Define o valor da propriedade isPassThrough.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsPassThrough(String value) {
        this.isPassThrough = value;
    }

    /**
     * Obtém o valor da propriedade consolidationType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConsolidationType() {
        return consolidationType;
    }

    /**
     * Define o valor da propriedade consolidationType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConsolidationType(String value) {
        this.consolidationType = value;
    }

    /**
     * Obtém o valor da propriedade parentInvoice.
     * 
     * @return
     *     possible object is
     *     {@link ParentInvoiceType }
     *     
     */
    public ParentInvoiceType getParentInvoice() {
        return parentInvoice;
    }

    /**
     * Define o valor da propriedade parentInvoice.
     * 
     * @param value
     *     allowed object is
     *     {@link ParentInvoiceType }
     *     
     */
    public void setParentInvoice(ParentInvoiceType value) {
        this.parentInvoice = value;
    }

    /**
     * Obtém o valor da propriedade isCostFixed.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsCostFixed() {
        return isCostFixed;
    }

    /**
     * Define o valor da propriedade isCostFixed.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsCostFixed(String value) {
        this.isCostFixed = value;
    }

    /**
     * Obtém o valor da propriedade baseCharge.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getBaseCharge() {
        return baseCharge;
    }

    /**
     * Define o valor da propriedade baseCharge.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setBaseCharge(GLogXMLFinancialAmountType value) {
        this.baseCharge = value;
    }

    /**
     * Obtém o valor da propriedade otherCharge.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getOtherCharge() {
        return otherCharge;
    }

    /**
     * Define o valor da propriedade otherCharge.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setOtherCharge(GLogXMLFinancialAmountType value) {
        this.otherCharge = value;
    }

    /**
     * Obtém o valor da propriedade isCreditNote.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsCreditNote() {
        return isCreditNote;
    }

    /**
     * Define o valor da propriedade isCreditNote.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsCreditNote(String value) {
        this.isCreditNote = value;
    }

    /**
     * Obtém o valor da propriedade sailDt.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getSailDt() {
        return sailDt;
    }

    /**
     * Define o valor da propriedade sailDt.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setSailDt(GLogDateTimeType value) {
        this.sailDt = value;
    }

    /**
     * Obtém o valor da propriedade sailCutoffDt.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getSailCutoffDt() {
        return sailCutoffDt;
    }

    /**
     * Define o valor da propriedade sailCutoffDt.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setSailCutoffDt(GLogDateTimeType value) {
        this.sailCutoffDt = value;
    }

    /**
     * Obtém o valor da propriedade railDt.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getRailDt() {
        return railDt;
    }

    /**
     * Define o valor da propriedade railDt.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setRailDt(GLogDateTimeType value) {
        this.railDt = value;
    }

    /**
     * Obtém o valor da propriedade isHazardous.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsHazardous() {
        return isHazardous;
    }

    /**
     * Define o valor da propriedade isHazardous.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsHazardous(String value) {
        this.isHazardous = value;
    }

    /**
     * Obtém o valor da propriedade isTemperatureControl.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsTemperatureControl() {
        return isTemperatureControl;
    }

    /**
     * Define o valor da propriedade isTemperatureControl.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsTemperatureControl(String value) {
        this.isTemperatureControl = value;
    }

    /**
     * Gets the value of the supplyProvinceVatReg property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the supplyProvinceVatReg property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSupplyProvinceVatReg().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProvinceVatRegType }
     * 
     * 
     */
    public List<ProvinceVatRegType> getSupplyProvinceVatReg() {
        if (supplyProvinceVatReg == null) {
            supplyProvinceVatReg = new ArrayList<ProvinceVatRegType>();
        }
        return this.supplyProvinceVatReg;
    }

    /**
     * Gets the value of the vatAnalysis property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the vatAnalysis property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getVatAnalysis().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link VatAnalysisType }
     * 
     * 
     */
    public List<VatAnalysisType> getVatAnalysis() {
        if (vatAnalysis == null) {
            vatAnalysis = new ArrayList<VatAnalysisType>();
        }
        return this.vatAnalysis;
    }

    /**
     * Gets the value of the text property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the text property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getText().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TextType }
     * 
     * 
     */
    public List<TextType> getText() {
        if (text == null) {
            text = new ArrayList<TextType>();
        }
        return this.text;
    }

    /**
     * Obtém o valor da propriedade flexFieldStrings.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldStringType }
     *     
     */
    public FlexFieldStringType getFlexFieldStrings() {
        return flexFieldStrings;
    }

    /**
     * Define o valor da propriedade flexFieldStrings.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldStringType }
     *     
     */
    public void setFlexFieldStrings(FlexFieldStringType value) {
        this.flexFieldStrings = value;
    }

    /**
     * Obtém o valor da propriedade flexFieldNumbers.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldNumberType }
     *     
     */
    public FlexFieldNumberType getFlexFieldNumbers() {
        return flexFieldNumbers;
    }

    /**
     * Define o valor da propriedade flexFieldNumbers.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldNumberType }
     *     
     */
    public void setFlexFieldNumbers(FlexFieldNumberType value) {
        this.flexFieldNumbers = value;
    }

    /**
     * Obtém o valor da propriedade flexFieldDates.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldDateType }
     *     
     */
    public FlexFieldDateType getFlexFieldDates() {
        return flexFieldDates;
    }

    /**
     * Define o valor da propriedade flexFieldDates.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldDateType }
     *     
     */
    public void setFlexFieldDates(FlexFieldDateType value) {
        this.flexFieldDates = value;
    }

    /**
     * Obtém o valor da propriedade flexFieldCurrencies.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldCurrencyType }
     *     
     */
    public FlexFieldCurrencyType getFlexFieldCurrencies() {
        return flexFieldCurrencies;
    }

    /**
     * Define o valor da propriedade flexFieldCurrencies.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldCurrencyType }
     *     
     */
    public void setFlexFieldCurrencies(FlexFieldCurrencyType value) {
        this.flexFieldCurrencies = value;
    }

}
