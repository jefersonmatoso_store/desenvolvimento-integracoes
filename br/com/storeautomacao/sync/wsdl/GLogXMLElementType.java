
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.*;


/**
 * 
 *             A GLogXMLElement represents an OTM or GTM Integration transaction.
 *             The GLogXMLTransaction element is used a Substitution Group head element. All valid Transaction interface elements will
 *             declare that they can substitute for the GLogXMLTransaction. For example, the TransOrder interface is a valid
 *             inbound and outbound transaction and can be used in place of the GLogXMLTransaction element:
 * 
 *             GLogXMLElement start tag
 *                TransactionHeader (optional)
 *                TransOrder
 *             GLogXMLElement end tag
 * 
 *          
 * 
 * <p>Classe Java de GLogXMLElementType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="GLogXMLElementType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TransactionHeader" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransactionHeaderType" minOccurs="0"/>
 *         &lt;element ref="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLTransaction"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GLogXMLElementType", propOrder = {
    "transactionHeader",
    "gLogXMLTransaction"
})
public class GLogXMLElementType {

    @XmlElement(name = "TransactionHeader")
    protected TransactionHeaderType transactionHeader;
    @XmlElementRef(name = "GLogXMLTransaction", namespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", type = JAXBElement.class)
    protected JAXBElement<? extends GLogXMLTransactionType> gLogXMLTransaction;

    /**
     * Obtém o valor da propriedade transactionHeader.
     * 
     * @return
     *     possible object is
     *     {@link TransactionHeaderType }
     *     
     */
    public TransactionHeaderType getTransactionHeader() {
        return transactionHeader;
    }

    /**
     * Define o valor da propriedade transactionHeader.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionHeaderType }
     *     
     */
    public void setTransactionHeader(TransactionHeaderType value) {
        this.transactionHeader = value;
    }

    /**
     * Obtém o valor da propriedade gLogXMLTransaction.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link TenderResponseType }{@code >}
     *     {@link JAXBElement }{@code <}{@link CSVDataLoadType }{@code >}
     *     {@link JAXBElement }{@code <}{@link DriverCalendarEventType }{@code >}
     *     {@link JAXBElement }{@code <}{@link ActivityTimeDefType }{@code >}
     *     {@link JAXBElement }{@code <}{@link SkuType }{@code >}
     *     {@link JAXBElement }{@code <}{@link TopicType }{@code >}
     *     {@link JAXBElement }{@code <}{@link CorporationType }{@code >}
     *     {@link JAXBElement }{@code <}{@link DocumentType }{@code >}
     *     {@link JAXBElement }{@code <}{@link GtmContactType }{@code >}
     *     {@link JAXBElement }{@code <}{@link ActualShipmentType }{@code >}
     *     {@link JAXBElement }{@code <}{@link BulkRatingType }{@code >}
     *     {@link JAXBElement }{@code <}{@link TransOrderStatusType }{@code >}
     *     {@link JAXBElement }{@code <}{@link OBLineType }{@code >}
     *     {@link JAXBElement }{@code <}{@link TransOrderLinkType }{@code >}
     *     {@link JAXBElement }{@code <}{@link BookingLineAmendmentType }{@code >}
     *     {@link JAXBElement }{@code <}{@link ContactGroupType }{@code >}
     *     {@link JAXBElement }{@code <}{@link WorkInvoiceType }{@code >}
     *     {@link JAXBElement }{@code <}{@link PlannedShipmentType }{@code >}
     *     {@link JAXBElement }{@code <}{@link DriverType }{@code >}
     *     {@link JAXBElement }{@code <}{@link AllocationBaseType }{@code >}
     *     {@link JAXBElement }{@code <}{@link TenderOfferType }{@code >}
     *     {@link JAXBElement }{@code <}{@link BulkContMoveType }{@code >}
     *     {@link JAXBElement }{@code <}{@link ConsolType }{@code >}
     *     {@link JAXBElement }{@code <}{@link SShipUnitType }{@code >}
     *     {@link JAXBElement }{@code <}{@link ContactType }{@code >}
     *     {@link JAXBElement }{@code <}{@link GLogXMLTransactionType }{@code >}
     *     {@link JAXBElement }{@code <}{@link BulkPlanType }{@code >}
     *     {@link JAXBElement }{@code <}{@link LocationType }{@code >}
     *     {@link JAXBElement }{@code <}{@link SkuEventType }{@code >}
     *     {@link JAXBElement }{@code <}{@link RateOfferingType }{@code >}
     *     {@link JAXBElement }{@code <}{@link ShipmentGroupType }{@code >}
     *     {@link JAXBElement }{@code <}{@link HazmatItemType }{@code >}
     *     {@link JAXBElement }{@code <}{@link ExchangeRateType }{@code >}
     *     {@link JAXBElement }{@code <}{@link ReleaseInstructionType }{@code >}
     *     {@link JAXBElement }{@code <}{@link HazmatGenericType }{@code >}
     *     {@link JAXBElement }{@code <}{@link TransactionAckType }{@code >}
     *     {@link JAXBElement }{@code <}{@link VoyageType }{@code >}
     *     {@link JAXBElement }{@code <}{@link QuoteType }{@code >}
     *     {@link JAXBElement }{@code <}{@link TransOrderType }{@code >}
     *     {@link JAXBElement }{@code <}{@link VoucherType }{@code >}
     *     {@link JAXBElement }{@code <}{@link JobType }{@code >}
     *     {@link JAXBElement }{@code <}{@link OrderMovementType }{@code >}
     *     {@link JAXBElement }{@code <}{@link BulkTrailerBuildType }{@code >}
     *     {@link JAXBElement }{@code <}{@link ItemMasterType }{@code >}
     *     {@link JAXBElement }{@code <}{@link GtmStructureType }{@code >}
     *     {@link JAXBElement }{@code <}{@link ItineraryType }{@code >}
     *     {@link JAXBElement }{@code <}{@link ServiceResponseType }{@code >}
     *     {@link JAXBElement }{@code <}{@link ItemType }{@code >}
     *     {@link JAXBElement }{@code <}{@link BillingType }{@code >}
     *     {@link JAXBElement }{@code <}{@link ShipmentStatusType }{@code >}
     *     {@link JAXBElement }{@code <}{@link PowerUnitType }{@code >}
     *     {@link JAXBElement }{@code <}{@link GtmTransactionLineType }{@code >}
     *     {@link JAXBElement }{@code <}{@link GenericStatusUpdateType }{@code >}
     *     {@link JAXBElement }{@code <}{@link FleetBulkPlanType }{@code >}
     *     {@link JAXBElement }{@code <}{@link GtmRegistrationType }{@code >}
     *     {@link JAXBElement }{@code <}{@link ShipmentLinkType }{@code >}
     *     {@link JAXBElement }{@code <}{@link FinancialSystemFeedType }{@code >}
     *     {@link JAXBElement }{@code <}{@link GtmDeclarationType }{@code >}
     *     {@link JAXBElement }{@code <}{@link ServiceTimeType }{@code >}
     *     {@link JAXBElement }{@code <}{@link RemoteQueryReplyType }{@code >}
     *     {@link JAXBElement }{@code <}{@link XLaneType }{@code >}
     *     {@link JAXBElement }{@code <}{@link InvoiceType }{@code >}
     *     {@link JAXBElement }{@code <}{@link GtmDeclarationMessageType }{@code >}
     *     {@link JAXBElement }{@code <}{@link MileageType }{@code >}
     *     {@link JAXBElement }{@code <}{@link GtmTransactionType }{@code >}
     *     {@link JAXBElement }{@code <}{@link RateGeoType }{@code >}
     *     {@link JAXBElement }{@code <}{@link RemoteQueryType }{@code >}
     *     {@link JAXBElement }{@code <}{@link DataQuerySummaryType }{@code >}
     *     {@link JAXBElement }{@code <}{@link ShipmentGroupTenderOfferType }{@code >}
     *     {@link JAXBElement }{@code <}{@link GtmBondType }{@code >}
     *     {@link JAXBElement }{@code <}{@link ReleaseType }{@code >}
     *     {@link JAXBElement }{@code <}{@link CharterVoyageType }{@code >}
     *     {@link JAXBElement }{@code <}{@link DemurrageTransactionType }{@code >}
     *     {@link JAXBElement }{@code <}{@link DeviceType }{@code >}
     *     {@link JAXBElement }{@code <}{@link ServiceRequestType }{@code >}
     *     {@link JAXBElement }{@code <}{@link SkuTransactionType }{@code >}
     *     {@link JAXBElement }{@code <}{@link AccrualType }{@code >}
     *     {@link JAXBElement }{@code <}{@link ShipStopType }{@code >}
     *     {@link JAXBElement }{@code <}{@link ClaimType }{@code >}
     *     {@link JAXBElement }{@code <}{@link OBShipUnitType }{@code >}
     *     {@link JAXBElement }{@code <}{@link UserType }{@code >}
     *     {@link JAXBElement }{@code <}{@link EquipmentType }{@code >}
     *     {@link JAXBElement }{@code <}{@link RouteTemplateType }{@code >}
     *     
     */
    public JAXBElement<? extends GLogXMLTransactionType> getGLogXMLTransaction() {
        return gLogXMLTransaction;
    }

    /**
     * Define o valor da propriedade gLogXMLTransaction.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link TenderResponseType }{@code >}
     *     {@link JAXBElement }{@code <}{@link CSVDataLoadType }{@code >}
     *     {@link JAXBElement }{@code <}{@link DriverCalendarEventType }{@code >}
     *     {@link JAXBElement }{@code <}{@link ActivityTimeDefType }{@code >}
     *     {@link JAXBElement }{@code <}{@link SkuType }{@code >}
     *     {@link JAXBElement }{@code <}{@link TopicType }{@code >}
     *     {@link JAXBElement }{@code <}{@link CorporationType }{@code >}
     *     {@link JAXBElement }{@code <}{@link DocumentType }{@code >}
     *     {@link JAXBElement }{@code <}{@link GtmContactType }{@code >}
     *     {@link JAXBElement }{@code <}{@link ActualShipmentType }{@code >}
     *     {@link JAXBElement }{@code <}{@link BulkRatingType }{@code >}
     *     {@link JAXBElement }{@code <}{@link TransOrderStatusType }{@code >}
     *     {@link JAXBElement }{@code <}{@link OBLineType }{@code >}
     *     {@link JAXBElement }{@code <}{@link TransOrderLinkType }{@code >}
     *     {@link JAXBElement }{@code <}{@link BookingLineAmendmentType }{@code >}
     *     {@link JAXBElement }{@code <}{@link ContactGroupType }{@code >}
     *     {@link JAXBElement }{@code <}{@link WorkInvoiceType }{@code >}
     *     {@link JAXBElement }{@code <}{@link PlannedShipmentType }{@code >}
     *     {@link JAXBElement }{@code <}{@link DriverType }{@code >}
     *     {@link JAXBElement }{@code <}{@link AllocationBaseType }{@code >}
     *     {@link JAXBElement }{@code <}{@link TenderOfferType }{@code >}
     *     {@link JAXBElement }{@code <}{@link BulkContMoveType }{@code >}
     *     {@link JAXBElement }{@code <}{@link ConsolType }{@code >}
     *     {@link JAXBElement }{@code <}{@link SShipUnitType }{@code >}
     *     {@link JAXBElement }{@code <}{@link ContactType }{@code >}
     *     {@link JAXBElement }{@code <}{@link GLogXMLTransactionType }{@code >}
     *     {@link JAXBElement }{@code <}{@link BulkPlanType }{@code >}
     *     {@link JAXBElement }{@code <}{@link LocationType }{@code >}
     *     {@link JAXBElement }{@code <}{@link SkuEventType }{@code >}
     *     {@link JAXBElement }{@code <}{@link RateOfferingType }{@code >}
     *     {@link JAXBElement }{@code <}{@link ShipmentGroupType }{@code >}
     *     {@link JAXBElement }{@code <}{@link HazmatItemType }{@code >}
     *     {@link JAXBElement }{@code <}{@link ExchangeRateType }{@code >}
     *     {@link JAXBElement }{@code <}{@link ReleaseInstructionType }{@code >}
     *     {@link JAXBElement }{@code <}{@link HazmatGenericType }{@code >}
     *     {@link JAXBElement }{@code <}{@link TransactionAckType }{@code >}
     *     {@link JAXBElement }{@code <}{@link VoyageType }{@code >}
     *     {@link JAXBElement }{@code <}{@link QuoteType }{@code >}
     *     {@link JAXBElement }{@code <}{@link TransOrderType }{@code >}
     *     {@link JAXBElement }{@code <}{@link VoucherType }{@code >}
     *     {@link JAXBElement }{@code <}{@link JobType }{@code >}
     *     {@link JAXBElement }{@code <}{@link OrderMovementType }{@code >}
     *     {@link JAXBElement }{@code <}{@link BulkTrailerBuildType }{@code >}
     *     {@link JAXBElement }{@code <}{@link ItemMasterType }{@code >}
     *     {@link JAXBElement }{@code <}{@link GtmStructureType }{@code >}
     *     {@link JAXBElement }{@code <}{@link ItineraryType }{@code >}
     *     {@link JAXBElement }{@code <}{@link ServiceResponseType }{@code >}
     *     {@link JAXBElement }{@code <}{@link ItemType }{@code >}
     *     {@link JAXBElement }{@code <}{@link BillingType }{@code >}
     *     {@link JAXBElement }{@code <}{@link ShipmentStatusType }{@code >}
     *     {@link JAXBElement }{@code <}{@link PowerUnitType }{@code >}
     *     {@link JAXBElement }{@code <}{@link GtmTransactionLineType }{@code >}
     *     {@link JAXBElement }{@code <}{@link GenericStatusUpdateType }{@code >}
     *     {@link JAXBElement }{@code <}{@link FleetBulkPlanType }{@code >}
     *     {@link JAXBElement }{@code <}{@link GtmRegistrationType }{@code >}
     *     {@link JAXBElement }{@code <}{@link ShipmentLinkType }{@code >}
     *     {@link JAXBElement }{@code <}{@link FinancialSystemFeedType }{@code >}
     *     {@link JAXBElement }{@code <}{@link GtmDeclarationType }{@code >}
     *     {@link JAXBElement }{@code <}{@link ServiceTimeType }{@code >}
     *     {@link JAXBElement }{@code <}{@link RemoteQueryReplyType }{@code >}
     *     {@link JAXBElement }{@code <}{@link XLaneType }{@code >}
     *     {@link JAXBElement }{@code <}{@link InvoiceType }{@code >}
     *     {@link JAXBElement }{@code <}{@link GtmDeclarationMessageType }{@code >}
     *     {@link JAXBElement }{@code <}{@link MileageType }{@code >}
     *     {@link JAXBElement }{@code <}{@link GtmTransactionType }{@code >}
     *     {@link JAXBElement }{@code <}{@link RateGeoType }{@code >}
     *     {@link JAXBElement }{@code <}{@link RemoteQueryType }{@code >}
     *     {@link JAXBElement }{@code <}{@link DataQuerySummaryType }{@code >}
     *     {@link JAXBElement }{@code <}{@link ShipmentGroupTenderOfferType }{@code >}
     *     {@link JAXBElement }{@code <}{@link GtmBondType }{@code >}
     *     {@link JAXBElement }{@code <}{@link ReleaseType }{@code >}
     *     {@link JAXBElement }{@code <}{@link CharterVoyageType }{@code >}
     *     {@link JAXBElement }{@code <}{@link DemurrageTransactionType }{@code >}
     *     {@link JAXBElement }{@code <}{@link DeviceType }{@code >}
     *     {@link JAXBElement }{@code <}{@link ServiceRequestType }{@code >}
     *     {@link JAXBElement }{@code <}{@link SkuTransactionType }{@code >}
     *     {@link JAXBElement }{@code <}{@link AccrualType }{@code >}
     *     {@link JAXBElement }{@code <}{@link ShipStopType }{@code >}
     *     {@link JAXBElement }{@code <}{@link ClaimType }{@code >}
     *     {@link JAXBElement }{@code <}{@link OBShipUnitType }{@code >}
     *     {@link JAXBElement }{@code <}{@link UserType }{@code >}
     *     {@link JAXBElement }{@code <}{@link EquipmentType }{@code >}
     *     {@link JAXBElement }{@code <}{@link RouteTemplateType }{@code >}
     *     
     */
    public void setGLogXMLTransaction(JAXBElement<? extends GLogXMLTransactionType> value) {
        this.gLogXMLTransaction = value;
    }

}
