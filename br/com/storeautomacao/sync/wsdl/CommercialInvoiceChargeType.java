
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Used to specify additional/other charges for the commercial invoice.
 * 
 * <p>Classe Java de CommercialInvoiceChargeType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="CommercialInvoiceChargeType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SequenceNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ChargeAmount" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType"/>
 *         &lt;element name="CommercialInvChargeCodeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="ChargeActivity" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CommercialInvoiceChargeType", propOrder = {
    "sequenceNumber",
    "chargeAmount",
    "commercialInvChargeCodeGid",
    "chargeActivity"
})
public class CommercialInvoiceChargeType {

    @XmlElement(name = "SequenceNumber")
    protected String sequenceNumber;
    @XmlElement(name = "ChargeAmount", required = true)
    protected GLogXMLFinancialAmountType chargeAmount;
    @XmlElement(name = "CommercialInvChargeCodeGid", required = true)
    protected GLogXMLGidType commercialInvChargeCodeGid;
    @XmlElement(name = "ChargeActivity", required = true)
    protected String chargeActivity;

    /**
     * Obtém o valor da propriedade sequenceNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSequenceNumber() {
        return sequenceNumber;
    }

    /**
     * Define o valor da propriedade sequenceNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSequenceNumber(String value) {
        this.sequenceNumber = value;
    }

    /**
     * Obtém o valor da propriedade chargeAmount.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getChargeAmount() {
        return chargeAmount;
    }

    /**
     * Define o valor da propriedade chargeAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setChargeAmount(GLogXMLFinancialAmountType value) {
        this.chargeAmount = value;
    }

    /**
     * Obtém o valor da propriedade commercialInvChargeCodeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getCommercialInvChargeCodeGid() {
        return commercialInvChargeCodeGid;
    }

    /**
     * Define o valor da propriedade commercialInvChargeCodeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setCommercialInvChargeCodeGid(GLogXMLGidType value) {
        this.commercialInvChargeCodeGid = value;
    }

    /**
     * Obtém o valor da propriedade chargeActivity.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChargeActivity() {
        return chargeActivity;
    }

    /**
     * Define o valor da propriedade chargeActivity.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChargeActivity(String value) {
        this.chargeActivity = value;
    }

}
