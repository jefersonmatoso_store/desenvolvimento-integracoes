
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 *             Specifies rail road information related to the status message or CLM. The city, state, SPLC code, and date correspond to information related to the destination.
 *          
 * 
 * <p>Classe Java de RailInfoType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="RailInfoType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CarrierScac" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsCarLoaded" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EventCity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EventState" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SPLCCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EventCountry" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LocationID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ERPC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ETADt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="Indicator" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="HoursToRepair" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InterchangeETADt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="NextJunctionSPLC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RailInfoType", propOrder = {
    "carrierScac",
    "isCarLoaded",
    "eventCity",
    "eventState",
    "splcCode",
    "eventCountry",
    "locationID",
    "erpc",
    "etaDt",
    "indicator",
    "hoursToRepair",
    "interchangeETADt",
    "nextJunctionSPLC"
})
public class RailInfoType {

    @XmlElement(name = "CarrierScac")
    protected String carrierScac;
    @XmlElement(name = "IsCarLoaded")
    protected String isCarLoaded;
    @XmlElement(name = "EventCity")
    protected String eventCity;
    @XmlElement(name = "EventState")
    protected String eventState;
    @XmlElement(name = "SPLCCode")
    protected String splcCode;
    @XmlElement(name = "EventCountry")
    protected String eventCountry;
    @XmlElement(name = "LocationID")
    protected String locationID;
    @XmlElement(name = "ERPC")
    protected String erpc;
    @XmlElement(name = "ETADt")
    protected GLogDateTimeType etaDt;
    @XmlElement(name = "Indicator")
    protected String indicator;
    @XmlElement(name = "HoursToRepair")
    protected String hoursToRepair;
    @XmlElement(name = "InterchangeETADt")
    protected GLogDateTimeType interchangeETADt;
    @XmlElement(name = "NextJunctionSPLC")
    protected String nextJunctionSPLC;

    /**
     * Obtém o valor da propriedade carrierScac.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCarrierScac() {
        return carrierScac;
    }

    /**
     * Define o valor da propriedade carrierScac.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCarrierScac(String value) {
        this.carrierScac = value;
    }

    /**
     * Obtém o valor da propriedade isCarLoaded.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsCarLoaded() {
        return isCarLoaded;
    }

    /**
     * Define o valor da propriedade isCarLoaded.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsCarLoaded(String value) {
        this.isCarLoaded = value;
    }

    /**
     * Obtém o valor da propriedade eventCity.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEventCity() {
        return eventCity;
    }

    /**
     * Define o valor da propriedade eventCity.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEventCity(String value) {
        this.eventCity = value;
    }

    /**
     * Obtém o valor da propriedade eventState.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEventState() {
        return eventState;
    }

    /**
     * Define o valor da propriedade eventState.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEventState(String value) {
        this.eventState = value;
    }

    /**
     * Obtém o valor da propriedade splcCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSPLCCode() {
        return splcCode;
    }

    /**
     * Define o valor da propriedade splcCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSPLCCode(String value) {
        this.splcCode = value;
    }

    /**
     * Obtém o valor da propriedade eventCountry.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEventCountry() {
        return eventCountry;
    }

    /**
     * Define o valor da propriedade eventCountry.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEventCountry(String value) {
        this.eventCountry = value;
    }

    /**
     * Obtém o valor da propriedade locationID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLocationID() {
        return locationID;
    }

    /**
     * Define o valor da propriedade locationID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLocationID(String value) {
        this.locationID = value;
    }

    /**
     * Obtém o valor da propriedade erpc.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getERPC() {
        return erpc;
    }

    /**
     * Define o valor da propriedade erpc.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setERPC(String value) {
        this.erpc = value;
    }

    /**
     * Obtém o valor da propriedade etaDt.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getETADt() {
        return etaDt;
    }

    /**
     * Define o valor da propriedade etaDt.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setETADt(GLogDateTimeType value) {
        this.etaDt = value;
    }

    /**
     * Obtém o valor da propriedade indicator.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIndicator() {
        return indicator;
    }

    /**
     * Define o valor da propriedade indicator.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIndicator(String value) {
        this.indicator = value;
    }

    /**
     * Obtém o valor da propriedade hoursToRepair.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHoursToRepair() {
        return hoursToRepair;
    }

    /**
     * Define o valor da propriedade hoursToRepair.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHoursToRepair(String value) {
        this.hoursToRepair = value;
    }

    /**
     * Obtém o valor da propriedade interchangeETADt.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getInterchangeETADt() {
        return interchangeETADt;
    }

    /**
     * Define o valor da propriedade interchangeETADt.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setInterchangeETADt(GLogDateTimeType value) {
        this.interchangeETADt = value;
    }

    /**
     * Obtém o valor da propriedade nextJunctionSPLC.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNextJunctionSPLC() {
        return nextJunctionSPLC;
    }

    /**
     * Define o valor da propriedade nextJunctionSPLC.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNextJunctionSPLC(String value) {
        this.nextJunctionSPLC = value;
    }

}
