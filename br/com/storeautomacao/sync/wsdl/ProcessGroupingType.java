
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de ProcessGroupingType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="ProcessGroupingType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ProcessGroup" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ProcessGroupOwner" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InSequence" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="StopProcessOnError" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProcessGroupingType", propOrder = {
    "processGroup",
    "processGroupOwner",
    "inSequence",
    "stopProcessOnError"
})
public class ProcessGroupingType {

    @XmlElement(name = "ProcessGroup", required = true)
    protected String processGroup;
    @XmlElement(name = "ProcessGroupOwner")
    protected String processGroupOwner;
    @XmlElement(name = "InSequence", required = true)
    protected String inSequence;
    @XmlElement(name = "StopProcessOnError")
    protected String stopProcessOnError;

    /**
     * Obtém o valor da propriedade processGroup.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProcessGroup() {
        return processGroup;
    }

    /**
     * Define o valor da propriedade processGroup.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProcessGroup(String value) {
        this.processGroup = value;
    }

    /**
     * Obtém o valor da propriedade processGroupOwner.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProcessGroupOwner() {
        return processGroupOwner;
    }

    /**
     * Define o valor da propriedade processGroupOwner.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProcessGroupOwner(String value) {
        this.processGroupOwner = value;
    }

    /**
     * Obtém o valor da propriedade inSequence.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInSequence() {
        return inSequence;
    }

    /**
     * Define o valor da propriedade inSequence.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInSequence(String value) {
        this.inSequence = value;
    }

    /**
     * Obtém o valor da propriedade stopProcessOnError.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStopProcessOnError() {
        return stopProcessOnError;
    }

    /**
     * Define o valor da propriedade stopProcessOnError.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStopProcessOnError(String value) {
        this.stopProcessOnError = value;
    }

}
