
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 *             ORLineRefnum is an alternate method for identifying a Release Line. It consists of a qualifier and a value.
 *          
 * 
 * <p>Classe Java de ORLineRefnumType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="ORLineRefnumType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ORLineRefnumQualifierGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="ORLineRefnumValue" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ORLineRefnumType", propOrder = {
    "orLineRefnumQualifierGid",
    "orLineRefnumValue"
})
public class ORLineRefnumType {

    @XmlElement(name = "ORLineRefnumQualifierGid", required = true)
    protected GLogXMLGidType orLineRefnumQualifierGid;
    @XmlElement(name = "ORLineRefnumValue", required = true)
    protected String orLineRefnumValue;

    /**
     * Obtém o valor da propriedade orLineRefnumQualifierGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getORLineRefnumQualifierGid() {
        return orLineRefnumQualifierGid;
    }

    /**
     * Define o valor da propriedade orLineRefnumQualifierGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setORLineRefnumQualifierGid(GLogXMLGidType value) {
        this.orLineRefnumQualifierGid = value;
    }

    /**
     * Obtém o valor da propriedade orLineRefnumValue.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getORLineRefnumValue() {
        return orLineRefnumValue;
    }

    /**
     * Define o valor da propriedade orLineRefnumValue.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setORLineRefnumValue(String value) {
        this.orLineRefnumValue = value;
    }

}
