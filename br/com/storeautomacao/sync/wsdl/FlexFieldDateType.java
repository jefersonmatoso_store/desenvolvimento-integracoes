
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Common type for tables which contain Flex Field columns for DATE data type
 * 
 * <p>Classe Java de FlexFieldDateType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="FlexFieldDateType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AttributeDate1" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="AttributeDate2" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="AttributeDate3" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="AttributeDate4" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="AttributeDate5" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="AttributeDate6" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="AttributeDate7" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="AttributeDate8" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="AttributeDate9" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="AttributeDate10" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FlexFieldDateType", propOrder = {
    "attributeDate1",
    "attributeDate2",
    "attributeDate3",
    "attributeDate4",
    "attributeDate5",
    "attributeDate6",
    "attributeDate7",
    "attributeDate8",
    "attributeDate9",
    "attributeDate10"
})
public class FlexFieldDateType {

    @XmlElement(name = "AttributeDate1")
    protected GLogDateTimeType attributeDate1;
    @XmlElement(name = "AttributeDate2")
    protected GLogDateTimeType attributeDate2;
    @XmlElement(name = "AttributeDate3")
    protected GLogDateTimeType attributeDate3;
    @XmlElement(name = "AttributeDate4")
    protected GLogDateTimeType attributeDate4;
    @XmlElement(name = "AttributeDate5")
    protected GLogDateTimeType attributeDate5;
    @XmlElement(name = "AttributeDate6")
    protected GLogDateTimeType attributeDate6;
    @XmlElement(name = "AttributeDate7")
    protected GLogDateTimeType attributeDate7;
    @XmlElement(name = "AttributeDate8")
    protected GLogDateTimeType attributeDate8;
    @XmlElement(name = "AttributeDate9")
    protected GLogDateTimeType attributeDate9;
    @XmlElement(name = "AttributeDate10")
    protected GLogDateTimeType attributeDate10;

    /**
     * Obtém o valor da propriedade attributeDate1.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getAttributeDate1() {
        return attributeDate1;
    }

    /**
     * Define o valor da propriedade attributeDate1.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setAttributeDate1(GLogDateTimeType value) {
        this.attributeDate1 = value;
    }

    /**
     * Obtém o valor da propriedade attributeDate2.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getAttributeDate2() {
        return attributeDate2;
    }

    /**
     * Define o valor da propriedade attributeDate2.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setAttributeDate2(GLogDateTimeType value) {
        this.attributeDate2 = value;
    }

    /**
     * Obtém o valor da propriedade attributeDate3.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getAttributeDate3() {
        return attributeDate3;
    }

    /**
     * Define o valor da propriedade attributeDate3.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setAttributeDate3(GLogDateTimeType value) {
        this.attributeDate3 = value;
    }

    /**
     * Obtém o valor da propriedade attributeDate4.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getAttributeDate4() {
        return attributeDate4;
    }

    /**
     * Define o valor da propriedade attributeDate4.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setAttributeDate4(GLogDateTimeType value) {
        this.attributeDate4 = value;
    }

    /**
     * Obtém o valor da propriedade attributeDate5.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getAttributeDate5() {
        return attributeDate5;
    }

    /**
     * Define o valor da propriedade attributeDate5.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setAttributeDate5(GLogDateTimeType value) {
        this.attributeDate5 = value;
    }

    /**
     * Obtém o valor da propriedade attributeDate6.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getAttributeDate6() {
        return attributeDate6;
    }

    /**
     * Define o valor da propriedade attributeDate6.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setAttributeDate6(GLogDateTimeType value) {
        this.attributeDate6 = value;
    }

    /**
     * Obtém o valor da propriedade attributeDate7.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getAttributeDate7() {
        return attributeDate7;
    }

    /**
     * Define o valor da propriedade attributeDate7.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setAttributeDate7(GLogDateTimeType value) {
        this.attributeDate7 = value;
    }

    /**
     * Obtém o valor da propriedade attributeDate8.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getAttributeDate8() {
        return attributeDate8;
    }

    /**
     * Define o valor da propriedade attributeDate8.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setAttributeDate8(GLogDateTimeType value) {
        this.attributeDate8 = value;
    }

    /**
     * Obtém o valor da propriedade attributeDate9.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getAttributeDate9() {
        return attributeDate9;
    }

    /**
     * Define o valor da propriedade attributeDate9.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setAttributeDate9(GLogDateTimeType value) {
        this.attributeDate9 = value;
    }

    /**
     * Obtém o valor da propriedade attributeDate10.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getAttributeDate10() {
        return attributeDate10;
    }

    /**
     * Define o valor da propriedade attributeDate10.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setAttributeDate10(GLogDateTimeType value) {
        this.attributeDate10 = value;
    }

}
