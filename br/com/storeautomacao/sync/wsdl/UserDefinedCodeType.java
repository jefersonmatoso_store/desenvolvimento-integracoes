
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 *             General User Defined Type pertaining to business objects
 *          
 * 
 * <p>Classe Java de UserDefinedCodeType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="UserDefinedCodeType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GtmCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Code" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserDefinedCodeType", namespace = "http://xmlns.oracle.com/apps/gtm/transmission/v6.4", propOrder = {
    "gtmCode",
    "code"
})
public class UserDefinedCodeType {

    @XmlElement(name = "GtmCode", required = true)
    protected String gtmCode;
    @XmlElement(name = "Code", required = true)
    protected String code;

    /**
     * Obtém o valor da propriedade gtmCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGtmCode() {
        return gtmCode;
    }

    /**
     * Define o valor da propriedade gtmCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGtmCode(String value) {
        this.gtmCode = value;
    }

    /**
     * Obtém o valor da propriedade code.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCode() {
        return code;
    }

    /**
     * Define o valor da propriedade code.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCode(String value) {
        this.code = value;
    }

}
