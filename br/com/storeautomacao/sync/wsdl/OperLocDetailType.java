
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Specifies detail info for operational locations. Used as a lookup so that operational location may be automatically assigned.
 * 
 * <p>Classe Java de OperLocDetailType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="OperLocDetailType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SequenceNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PriLegServProv" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}PriLegServProvType"/>
 *         &lt;element name="PriLegLocProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocProfileType" minOccurs="0"/>
 *         &lt;element name="ImportExportType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InclusionRegionGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="ExclusionRegionGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OperLocDetailType", propOrder = {
    "sequenceNumber",
    "priLegServProv",
    "priLegLocProfileGid",
    "importExportType",
    "inclusionRegionGid",
    "exclusionRegionGid"
})
public class OperLocDetailType {

    @XmlElement(name = "SequenceNumber")
    protected String sequenceNumber;
    @XmlElement(name = "PriLegServProv", required = true)
    protected PriLegServProvType priLegServProv;
    @XmlElement(name = "PriLegLocProfileGid")
    protected GLogXMLLocProfileType priLegLocProfileGid;
    @XmlElement(name = "ImportExportType")
    protected String importExportType;
    @XmlElement(name = "InclusionRegionGid")
    protected GLogXMLGidType inclusionRegionGid;
    @XmlElement(name = "ExclusionRegionGid")
    protected GLogXMLGidType exclusionRegionGid;

    /**
     * Obtém o valor da propriedade sequenceNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSequenceNumber() {
        return sequenceNumber;
    }

    /**
     * Define o valor da propriedade sequenceNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSequenceNumber(String value) {
        this.sequenceNumber = value;
    }

    /**
     * Obtém o valor da propriedade priLegServProv.
     * 
     * @return
     *     possible object is
     *     {@link PriLegServProvType }
     *     
     */
    public PriLegServProvType getPriLegServProv() {
        return priLegServProv;
    }

    /**
     * Define o valor da propriedade priLegServProv.
     * 
     * @param value
     *     allowed object is
     *     {@link PriLegServProvType }
     *     
     */
    public void setPriLegServProv(PriLegServProvType value) {
        this.priLegServProv = value;
    }

    /**
     * Obtém o valor da propriedade priLegLocProfileGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocProfileType }
     *     
     */
    public GLogXMLLocProfileType getPriLegLocProfileGid() {
        return priLegLocProfileGid;
    }

    /**
     * Define o valor da propriedade priLegLocProfileGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocProfileType }
     *     
     */
    public void setPriLegLocProfileGid(GLogXMLLocProfileType value) {
        this.priLegLocProfileGid = value;
    }

    /**
     * Obtém o valor da propriedade importExportType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getImportExportType() {
        return importExportType;
    }

    /**
     * Define o valor da propriedade importExportType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setImportExportType(String value) {
        this.importExportType = value;
    }

    /**
     * Obtém o valor da propriedade inclusionRegionGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getInclusionRegionGid() {
        return inclusionRegionGid;
    }

    /**
     * Define o valor da propriedade inclusionRegionGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setInclusionRegionGid(GLogXMLGidType value) {
        this.inclusionRegionGid = value;
    }

    /**
     * Obtém o valor da propriedade exclusionRegionGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getExclusionRegionGid() {
        return exclusionRegionGid;
    }

    /**
     * Define o valor da propriedade exclusionRegionGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setExclusionRegionGid(GLogXMLGidType value) {
        this.exclusionRegionGid = value;
    }

}
