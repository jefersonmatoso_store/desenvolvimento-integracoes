
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * port associated with an ocean stop.
 * 
 * <p>Classe Java de PortType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="PortType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PortName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PortLocationFunctionCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PortIdentifier" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}PortIdentifierType" minOccurs="0"/>
 *         &lt;element name="ProvinceCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CountryCode3Gid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PortType", propOrder = {
    "portName",
    "portLocationFunctionCode",
    "portIdentifier",
    "provinceCode",
    "countryCode3Gid"
})
public class PortType {

    @XmlElement(name = "PortName")
    protected String portName;
    @XmlElement(name = "PortLocationFunctionCode", required = true)
    protected String portLocationFunctionCode;
    @XmlElement(name = "PortIdentifier")
    protected PortIdentifierType portIdentifier;
    @XmlElement(name = "ProvinceCode")
    protected String provinceCode;
    @XmlElement(name = "CountryCode3Gid")
    protected GLogXMLGidType countryCode3Gid;

    /**
     * Obtém o valor da propriedade portName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPortName() {
        return portName;
    }

    /**
     * Define o valor da propriedade portName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPortName(String value) {
        this.portName = value;
    }

    /**
     * Obtém o valor da propriedade portLocationFunctionCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPortLocationFunctionCode() {
        return portLocationFunctionCode;
    }

    /**
     * Define o valor da propriedade portLocationFunctionCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPortLocationFunctionCode(String value) {
        this.portLocationFunctionCode = value;
    }

    /**
     * Obtém o valor da propriedade portIdentifier.
     * 
     * @return
     *     possible object is
     *     {@link PortIdentifierType }
     *     
     */
    public PortIdentifierType getPortIdentifier() {
        return portIdentifier;
    }

    /**
     * Define o valor da propriedade portIdentifier.
     * 
     * @param value
     *     allowed object is
     *     {@link PortIdentifierType }
     *     
     */
    public void setPortIdentifier(PortIdentifierType value) {
        this.portIdentifier = value;
    }

    /**
     * Obtém o valor da propriedade provinceCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProvinceCode() {
        return provinceCode;
    }

    /**
     * Define o valor da propriedade provinceCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProvinceCode(String value) {
        this.provinceCode = value;
    }

    /**
     * Obtém o valor da propriedade countryCode3Gid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getCountryCode3Gid() {
        return countryCode3Gid;
    }

    /**
     * Define o valor da propriedade countryCode3Gid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setCountryCode3Gid(GLogXMLGidType value) {
        this.countryCode3Gid = value;
    }

}
