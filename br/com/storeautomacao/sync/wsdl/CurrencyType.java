
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * (Both) Currency on the business object.
 *          
 * 
 * <p>Classe Java de CurrencyType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="CurrencyType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GtmCurrencyTypeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="CurrencyValue" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType"/>
 *         &lt;element name="GtmFormulaExpressionGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="ValueNote" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MethodOfCalculation" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsFixed" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PaymentMethodGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="ReportingCurrencyCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ReportingMonetaryAmount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RateToReporting" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ExchangeRateInfo" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ExchangeRateInfoType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CurrencyType", namespace = "http://xmlns.oracle.com/apps/gtm/transmission/v6.4", propOrder = {
    "gtmCurrencyTypeGid",
    "currencyValue",
    "gtmFormulaExpressionGid",
    "valueNote",
    "methodOfCalculation",
    "isFixed",
    "paymentMethodGid",
    "reportingCurrencyCode",
    "reportingMonetaryAmount",
    "rateToReporting",
    "exchangeRateInfo"
})
public class CurrencyType {

    @XmlElement(name = "GtmCurrencyTypeGid", required = true)
    protected GLogXMLGidType gtmCurrencyTypeGid;
    @XmlElement(name = "CurrencyValue", required = true)
    protected GLogXMLFinancialAmountType currencyValue;
    @XmlElement(name = "GtmFormulaExpressionGid")
    protected GLogXMLGidType gtmFormulaExpressionGid;
    @XmlElement(name = "ValueNote")
    protected String valueNote;
    @XmlElement(name = "MethodOfCalculation")
    protected String methodOfCalculation;
    @XmlElement(name = "IsFixed")
    protected String isFixed;
    @XmlElement(name = "PaymentMethodGid")
    protected GLogXMLGidType paymentMethodGid;
    @XmlElement(name = "ReportingCurrencyCode")
    protected String reportingCurrencyCode;
    @XmlElement(name = "ReportingMonetaryAmount")
    protected String reportingMonetaryAmount;
    @XmlElement(name = "RateToReporting")
    protected String rateToReporting;
    @XmlElement(name = "ExchangeRateInfo")
    protected ExchangeRateInfoType exchangeRateInfo;

    /**
     * Obtém o valor da propriedade gtmCurrencyTypeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getGtmCurrencyTypeGid() {
        return gtmCurrencyTypeGid;
    }

    /**
     * Define o valor da propriedade gtmCurrencyTypeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setGtmCurrencyTypeGid(GLogXMLGidType value) {
        this.gtmCurrencyTypeGid = value;
    }

    /**
     * Obtém o valor da propriedade currencyValue.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getCurrencyValue() {
        return currencyValue;
    }

    /**
     * Define o valor da propriedade currencyValue.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setCurrencyValue(GLogXMLFinancialAmountType value) {
        this.currencyValue = value;
    }

    /**
     * Obtém o valor da propriedade gtmFormulaExpressionGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getGtmFormulaExpressionGid() {
        return gtmFormulaExpressionGid;
    }

    /**
     * Define o valor da propriedade gtmFormulaExpressionGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setGtmFormulaExpressionGid(GLogXMLGidType value) {
        this.gtmFormulaExpressionGid = value;
    }

    /**
     * Obtém o valor da propriedade valueNote.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValueNote() {
        return valueNote;
    }

    /**
     * Define o valor da propriedade valueNote.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValueNote(String value) {
        this.valueNote = value;
    }

    /**
     * Obtém o valor da propriedade methodOfCalculation.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMethodOfCalculation() {
        return methodOfCalculation;
    }

    /**
     * Define o valor da propriedade methodOfCalculation.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMethodOfCalculation(String value) {
        this.methodOfCalculation = value;
    }

    /**
     * Obtém o valor da propriedade isFixed.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsFixed() {
        return isFixed;
    }

    /**
     * Define o valor da propriedade isFixed.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsFixed(String value) {
        this.isFixed = value;
    }

    /**
     * Obtém o valor da propriedade paymentMethodGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getPaymentMethodGid() {
        return paymentMethodGid;
    }

    /**
     * Define o valor da propriedade paymentMethodGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setPaymentMethodGid(GLogXMLGidType value) {
        this.paymentMethodGid = value;
    }

    /**
     * Obtém o valor da propriedade reportingCurrencyCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReportingCurrencyCode() {
        return reportingCurrencyCode;
    }

    /**
     * Define o valor da propriedade reportingCurrencyCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReportingCurrencyCode(String value) {
        this.reportingCurrencyCode = value;
    }

    /**
     * Obtém o valor da propriedade reportingMonetaryAmount.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReportingMonetaryAmount() {
        return reportingMonetaryAmount;
    }

    /**
     * Define o valor da propriedade reportingMonetaryAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReportingMonetaryAmount(String value) {
        this.reportingMonetaryAmount = value;
    }

    /**
     * Obtém o valor da propriedade rateToReporting.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRateToReporting() {
        return rateToReporting;
    }

    /**
     * Define o valor da propriedade rateToReporting.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRateToReporting(String value) {
        this.rateToReporting = value;
    }

    /**
     * Obtém o valor da propriedade exchangeRateInfo.
     * 
     * @return
     *     possible object is
     *     {@link ExchangeRateInfoType }
     *     
     */
    public ExchangeRateInfoType getExchangeRateInfo() {
        return exchangeRateInfo;
    }

    /**
     * Define o valor da propriedade exchangeRateInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link ExchangeRateInfoType }
     *     
     */
    public void setExchangeRateInfo(ExchangeRateInfoType value) {
        this.exchangeRateInfo = value;
    }

}
