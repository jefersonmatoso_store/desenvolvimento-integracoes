
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * An HazmatItemRef is either a HazmatItem or an HazmatItemGid.
 * 
 * <p>Classe Java de HazmatItemRefType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="HazmatItemRefType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element name="HazmatItemGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="HazmatItem" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}HazmatItemType"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "HazmatItemRefType", propOrder = {
    "hazmatItemGid",
    "hazmatItem"
})
public class HazmatItemRefType {

    @XmlElement(name = "HazmatItemGid")
    protected GLogXMLGidType hazmatItemGid;
    @XmlElement(name = "HazmatItem")
    protected HazmatItemType hazmatItem;

    /**
     * Obtém o valor da propriedade hazmatItemGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getHazmatItemGid() {
        return hazmatItemGid;
    }

    /**
     * Define o valor da propriedade hazmatItemGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setHazmatItemGid(GLogXMLGidType value) {
        this.hazmatItemGid = value;
    }

    /**
     * Obtém o valor da propriedade hazmatItem.
     * 
     * @return
     *     possible object is
     *     {@link HazmatItemType }
     *     
     */
    public HazmatItemType getHazmatItem() {
        return hazmatItem;
    }

    /**
     * Define o valor da propriedade hazmatItem.
     * 
     * @param value
     *     allowed object is
     *     {@link HazmatItemType }
     *     
     */
    public void setHazmatItem(HazmatItemType value) {
        this.hazmatItem = value;
    }

}
