
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * FinalCommercialTerms specifies the Final Terms of Sale the Commercial Invoice is based on.
 * 
 * <p>Classe Java de FinalCommercialTermsType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="FinalCommercialTermsType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IncoTermGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="TermLocationText" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FinalCommercialTermsType", propOrder = {
    "incoTermGid",
    "termLocationText"
})
public class FinalCommercialTermsType {

    @XmlElement(name = "IncoTermGid")
    protected GLogXMLGidType incoTermGid;
    @XmlElement(name = "TermLocationText")
    protected String termLocationText;

    /**
     * Obtém o valor da propriedade incoTermGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getIncoTermGid() {
        return incoTermGid;
    }

    /**
     * Define o valor da propriedade incoTermGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setIncoTermGid(GLogXMLGidType value) {
        this.incoTermGid = value;
    }

    /**
     * Obtém o valor da propriedade termLocationText.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTermLocationText() {
        return termLocationText;
    }

    /**
     * Define o valor da propriedade termLocationText.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTermLocationText(String value) {
        this.termLocationText = value;
    }

}
