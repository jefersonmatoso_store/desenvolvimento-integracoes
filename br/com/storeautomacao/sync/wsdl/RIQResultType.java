
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * RIQResult: Rate Inquiry Results.
 * 
 * <p>Classe Java de RIQResultType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="RIQResultType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SourceLocationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="ServiceProviderGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="RateOfferingGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="RateGeoGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="RateServiceGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="TransportModeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="EquipmentGroupProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="EquipmentGroupGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="DeliveryBy" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}DeliveryByType"/>
 *         &lt;element name="PickupBy" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}PickupByType" minOccurs="0"/>
 *         &lt;element name="TransitTime" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDurationType"/>
 *         &lt;element name="Cost" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType"/>
 *         &lt;element name="TotalActualCostPerUOM" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLCostPerUOM" minOccurs="0"/>
 *         &lt;element name="TotalWeightedCostPerUOM" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLCostPerUOM" minOccurs="0"/>
 *         &lt;element name="CostDetails" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}CostDetailsType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="IsTimeFeasible" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsOptimalResult" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DimWeight" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLWeightType" minOccurs="0"/>
 *         &lt;element name="Perspective" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Distance" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}DistanceType" minOccurs="0"/>
 *         &lt;element name="OrigCarrierGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="DelivCarrierGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="RouteCodeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="RIQResultInfo" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RIQResultInfoType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RIQResultType", propOrder = {
    "sourceLocationGid",
    "serviceProviderGid",
    "rateOfferingGid",
    "rateGeoGid",
    "rateServiceGid",
    "transportModeGid",
    "equipmentGroupProfileGid",
    "equipmentGroupGid",
    "deliveryBy",
    "pickupBy",
    "transitTime",
    "cost",
    "totalActualCostPerUOM",
    "totalWeightedCostPerUOM",
    "costDetails",
    "isTimeFeasible",
    "isOptimalResult",
    "dimWeight",
    "perspective",
    "distance",
    "origCarrierGid",
    "delivCarrierGid",
    "routeCodeGid",
    "riqResultInfo"
})
public class RIQResultType {

    @XmlElement(name = "SourceLocationGid", required = true)
    protected GLogXMLGidType sourceLocationGid;
    @XmlElement(name = "ServiceProviderGid", required = true)
    protected GLogXMLGidType serviceProviderGid;
    @XmlElement(name = "RateOfferingGid", required = true)
    protected GLogXMLGidType rateOfferingGid;
    @XmlElement(name = "RateGeoGid", required = true)
    protected GLogXMLGidType rateGeoGid;
    @XmlElement(name = "RateServiceGid")
    protected GLogXMLGidType rateServiceGid;
    @XmlElement(name = "TransportModeGid", required = true)
    protected GLogXMLGidType transportModeGid;
    @XmlElement(name = "EquipmentGroupProfileGid")
    protected GLogXMLGidType equipmentGroupProfileGid;
    @XmlElement(name = "EquipmentGroupGid")
    protected GLogXMLGidType equipmentGroupGid;
    @XmlElement(name = "DeliveryBy", required = true)
    protected DeliveryByType deliveryBy;
    @XmlElement(name = "PickupBy")
    protected PickupByType pickupBy;
    @XmlElement(name = "TransitTime", required = true)
    protected GLogXMLDurationType transitTime;
    @XmlElement(name = "Cost", required = true)
    protected GLogXMLFinancialAmountType cost;
    @XmlElement(name = "TotalActualCostPerUOM")
    protected GLogXMLCostPerUOM totalActualCostPerUOM;
    @XmlElement(name = "TotalWeightedCostPerUOM")
    protected GLogXMLCostPerUOM totalWeightedCostPerUOM;
    @XmlElement(name = "CostDetails")
    protected List<CostDetailsType> costDetails;
    @XmlElement(name = "IsTimeFeasible")
    protected String isTimeFeasible;
    @XmlElement(name = "IsOptimalResult")
    protected String isOptimalResult;
    @XmlElement(name = "DimWeight")
    protected GLogXMLWeightType dimWeight;
    @XmlElement(name = "Perspective")
    protected String perspective;
    @XmlElement(name = "Distance")
    protected DistanceType distance;
    @XmlElement(name = "OrigCarrierGid")
    protected GLogXMLGidType origCarrierGid;
    @XmlElement(name = "DelivCarrierGid")
    protected GLogXMLGidType delivCarrierGid;
    @XmlElement(name = "RouteCodeGid")
    protected GLogXMLGidType routeCodeGid;
    @XmlElement(name = "RIQResultInfo")
    protected RIQResultInfoType riqResultInfo;

    /**
     * Obtém o valor da propriedade sourceLocationGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getSourceLocationGid() {
        return sourceLocationGid;
    }

    /**
     * Define o valor da propriedade sourceLocationGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setSourceLocationGid(GLogXMLGidType value) {
        this.sourceLocationGid = value;
    }

    /**
     * Obtém o valor da propriedade serviceProviderGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getServiceProviderGid() {
        return serviceProviderGid;
    }

    /**
     * Define o valor da propriedade serviceProviderGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setServiceProviderGid(GLogXMLGidType value) {
        this.serviceProviderGid = value;
    }

    /**
     * Obtém o valor da propriedade rateOfferingGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRateOfferingGid() {
        return rateOfferingGid;
    }

    /**
     * Define o valor da propriedade rateOfferingGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRateOfferingGid(GLogXMLGidType value) {
        this.rateOfferingGid = value;
    }

    /**
     * Obtém o valor da propriedade rateGeoGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRateGeoGid() {
        return rateGeoGid;
    }

    /**
     * Define o valor da propriedade rateGeoGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRateGeoGid(GLogXMLGidType value) {
        this.rateGeoGid = value;
    }

    /**
     * Obtém o valor da propriedade rateServiceGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRateServiceGid() {
        return rateServiceGid;
    }

    /**
     * Define o valor da propriedade rateServiceGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRateServiceGid(GLogXMLGidType value) {
        this.rateServiceGid = value;
    }

    /**
     * Obtém o valor da propriedade transportModeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getTransportModeGid() {
        return transportModeGid;
    }

    /**
     * Define o valor da propriedade transportModeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setTransportModeGid(GLogXMLGidType value) {
        this.transportModeGid = value;
    }

    /**
     * Obtém o valor da propriedade equipmentGroupProfileGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getEquipmentGroupProfileGid() {
        return equipmentGroupProfileGid;
    }

    /**
     * Define o valor da propriedade equipmentGroupProfileGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setEquipmentGroupProfileGid(GLogXMLGidType value) {
        this.equipmentGroupProfileGid = value;
    }

    /**
     * Obtém o valor da propriedade equipmentGroupGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getEquipmentGroupGid() {
        return equipmentGroupGid;
    }

    /**
     * Define o valor da propriedade equipmentGroupGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setEquipmentGroupGid(GLogXMLGidType value) {
        this.equipmentGroupGid = value;
    }

    /**
     * Obtém o valor da propriedade deliveryBy.
     * 
     * @return
     *     possible object is
     *     {@link DeliveryByType }
     *     
     */
    public DeliveryByType getDeliveryBy() {
        return deliveryBy;
    }

    /**
     * Define o valor da propriedade deliveryBy.
     * 
     * @param value
     *     allowed object is
     *     {@link DeliveryByType }
     *     
     */
    public void setDeliveryBy(DeliveryByType value) {
        this.deliveryBy = value;
    }

    /**
     * Obtém o valor da propriedade pickupBy.
     * 
     * @return
     *     possible object is
     *     {@link PickupByType }
     *     
     */
    public PickupByType getPickupBy() {
        return pickupBy;
    }

    /**
     * Define o valor da propriedade pickupBy.
     * 
     * @param value
     *     allowed object is
     *     {@link PickupByType }
     *     
     */
    public void setPickupBy(PickupByType value) {
        this.pickupBy = value;
    }

    /**
     * Obtém o valor da propriedade transitTime.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public GLogXMLDurationType getTransitTime() {
        return transitTime;
    }

    /**
     * Define o valor da propriedade transitTime.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public void setTransitTime(GLogXMLDurationType value) {
        this.transitTime = value;
    }

    /**
     * Obtém o valor da propriedade cost.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getCost() {
        return cost;
    }

    /**
     * Define o valor da propriedade cost.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setCost(GLogXMLFinancialAmountType value) {
        this.cost = value;
    }

    /**
     * Obtém o valor da propriedade totalActualCostPerUOM.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLCostPerUOM }
     *     
     */
    public GLogXMLCostPerUOM getTotalActualCostPerUOM() {
        return totalActualCostPerUOM;
    }

    /**
     * Define o valor da propriedade totalActualCostPerUOM.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLCostPerUOM }
     *     
     */
    public void setTotalActualCostPerUOM(GLogXMLCostPerUOM value) {
        this.totalActualCostPerUOM = value;
    }

    /**
     * Obtém o valor da propriedade totalWeightedCostPerUOM.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLCostPerUOM }
     *     
     */
    public GLogXMLCostPerUOM getTotalWeightedCostPerUOM() {
        return totalWeightedCostPerUOM;
    }

    /**
     * Define o valor da propriedade totalWeightedCostPerUOM.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLCostPerUOM }
     *     
     */
    public void setTotalWeightedCostPerUOM(GLogXMLCostPerUOM value) {
        this.totalWeightedCostPerUOM = value;
    }

    /**
     * Gets the value of the costDetails property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the costDetails property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCostDetails().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CostDetailsType }
     * 
     * 
     */
    public List<CostDetailsType> getCostDetails() {
        if (costDetails == null) {
            costDetails = new ArrayList<CostDetailsType>();
        }
        return this.costDetails;
    }

    /**
     * Obtém o valor da propriedade isTimeFeasible.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsTimeFeasible() {
        return isTimeFeasible;
    }

    /**
     * Define o valor da propriedade isTimeFeasible.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsTimeFeasible(String value) {
        this.isTimeFeasible = value;
    }

    /**
     * Obtém o valor da propriedade isOptimalResult.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsOptimalResult() {
        return isOptimalResult;
    }

    /**
     * Define o valor da propriedade isOptimalResult.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsOptimalResult(String value) {
        this.isOptimalResult = value;
    }

    /**
     * Obtém o valor da propriedade dimWeight.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLWeightType }
     *     
     */
    public GLogXMLWeightType getDimWeight() {
        return dimWeight;
    }

    /**
     * Define o valor da propriedade dimWeight.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLWeightType }
     *     
     */
    public void setDimWeight(GLogXMLWeightType value) {
        this.dimWeight = value;
    }

    /**
     * Obtém o valor da propriedade perspective.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPerspective() {
        return perspective;
    }

    /**
     * Define o valor da propriedade perspective.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPerspective(String value) {
        this.perspective = value;
    }

    /**
     * Obtém o valor da propriedade distance.
     * 
     * @return
     *     possible object is
     *     {@link DistanceType }
     *     
     */
    public DistanceType getDistance() {
        return distance;
    }

    /**
     * Define o valor da propriedade distance.
     * 
     * @param value
     *     allowed object is
     *     {@link DistanceType }
     *     
     */
    public void setDistance(DistanceType value) {
        this.distance = value;
    }

    /**
     * Obtém o valor da propriedade origCarrierGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getOrigCarrierGid() {
        return origCarrierGid;
    }

    /**
     * Define o valor da propriedade origCarrierGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setOrigCarrierGid(GLogXMLGidType value) {
        this.origCarrierGid = value;
    }

    /**
     * Obtém o valor da propriedade delivCarrierGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getDelivCarrierGid() {
        return delivCarrierGid;
    }

    /**
     * Define o valor da propriedade delivCarrierGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setDelivCarrierGid(GLogXMLGidType value) {
        this.delivCarrierGid = value;
    }

    /**
     * Obtém o valor da propriedade routeCodeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRouteCodeGid() {
        return routeCodeGid;
    }

    /**
     * Define o valor da propriedade routeCodeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRouteCodeGid(GLogXMLGidType value) {
        this.routeCodeGid = value;
    }

    /**
     * Obtém o valor da propriedade riqResultInfo.
     * 
     * @return
     *     possible object is
     *     {@link RIQResultInfoType }
     *     
     */
    public RIQResultInfoType getRIQResultInfo() {
        return riqResultInfo;
    }

    /**
     * Define o valor da propriedade riqResultInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link RIQResultInfoType }
     *     
     */
    public void setRIQResultInfo(RIQResultInfoType value) {
        this.riqResultInfo = value;
    }

}
