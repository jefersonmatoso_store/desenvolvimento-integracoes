
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Used to take order related data and return order routing rule data.
 *             Note: The PackagedItemGid is required when the GetFreightCost = Y.
 *          
 * 
 * <p>Classe Java de OrderRoutingRuleQueryType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="OrderRoutingRuleQueryType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SourceAddress" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}MileageCorporationType"/>
 *         &lt;element name="DestAddress" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}MileageCorporationType"/>
 *         &lt;element name="EstDepartureDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="EstArrivalDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="Weight" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}WeightType" minOccurs="0"/>
 *         &lt;element name="Volume" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}VolumeType" minOccurs="0"/>
 *         &lt;element name="PaymentMethodCodeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OrderRoutingRuleQueryType", propOrder = {
    "sourceAddress",
    "destAddress",
    "estDepartureDt",
    "estArrivalDt",
    "weight",
    "volume",
    "paymentMethodCodeGid"
})
public class OrderRoutingRuleQueryType {

    @XmlElement(name = "SourceAddress", required = true)
    protected MileageCorporationType sourceAddress;
    @XmlElement(name = "DestAddress", required = true)
    protected MileageCorporationType destAddress;
    @XmlElement(name = "EstDepartureDt")
    protected GLogDateTimeType estDepartureDt;
    @XmlElement(name = "EstArrivalDt")
    protected GLogDateTimeType estArrivalDt;
    @XmlElement(name = "Weight")
    protected WeightType weight;
    @XmlElement(name = "Volume")
    protected VolumeType volume;
    @XmlElement(name = "PaymentMethodCodeGid")
    protected GLogXMLGidType paymentMethodCodeGid;

    /**
     * Obtém o valor da propriedade sourceAddress.
     * 
     * @return
     *     possible object is
     *     {@link MileageCorporationType }
     *     
     */
    public MileageCorporationType getSourceAddress() {
        return sourceAddress;
    }

    /**
     * Define o valor da propriedade sourceAddress.
     * 
     * @param value
     *     allowed object is
     *     {@link MileageCorporationType }
     *     
     */
    public void setSourceAddress(MileageCorporationType value) {
        this.sourceAddress = value;
    }

    /**
     * Obtém o valor da propriedade destAddress.
     * 
     * @return
     *     possible object is
     *     {@link MileageCorporationType }
     *     
     */
    public MileageCorporationType getDestAddress() {
        return destAddress;
    }

    /**
     * Define o valor da propriedade destAddress.
     * 
     * @param value
     *     allowed object is
     *     {@link MileageCorporationType }
     *     
     */
    public void setDestAddress(MileageCorporationType value) {
        this.destAddress = value;
    }

    /**
     * Obtém o valor da propriedade estDepartureDt.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getEstDepartureDt() {
        return estDepartureDt;
    }

    /**
     * Define o valor da propriedade estDepartureDt.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setEstDepartureDt(GLogDateTimeType value) {
        this.estDepartureDt = value;
    }

    /**
     * Obtém o valor da propriedade estArrivalDt.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getEstArrivalDt() {
        return estArrivalDt;
    }

    /**
     * Define o valor da propriedade estArrivalDt.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setEstArrivalDt(GLogDateTimeType value) {
        this.estArrivalDt = value;
    }

    /**
     * Obtém o valor da propriedade weight.
     * 
     * @return
     *     possible object is
     *     {@link WeightType }
     *     
     */
    public WeightType getWeight() {
        return weight;
    }

    /**
     * Define o valor da propriedade weight.
     * 
     * @param value
     *     allowed object is
     *     {@link WeightType }
     *     
     */
    public void setWeight(WeightType value) {
        this.weight = value;
    }

    /**
     * Obtém o valor da propriedade volume.
     * 
     * @return
     *     possible object is
     *     {@link VolumeType }
     *     
     */
    public VolumeType getVolume() {
        return volume;
    }

    /**
     * Define o valor da propriedade volume.
     * 
     * @param value
     *     allowed object is
     *     {@link VolumeType }
     *     
     */
    public void setVolume(VolumeType value) {
        this.volume = value;
    }

    /**
     * Obtém o valor da propriedade paymentMethodCodeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getPaymentMethodCodeGid() {
        return paymentMethodCodeGid;
    }

    /**
     * Define o valor da propriedade paymentMethodCodeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setPaymentMethodCodeGid(GLogXMLGidType value) {
        this.paymentMethodCodeGid = value;
    }

}
