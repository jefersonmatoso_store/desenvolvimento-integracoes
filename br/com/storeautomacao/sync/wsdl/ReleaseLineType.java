
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * 
 *             ReleaseLine specifies and item and quantity that is released.
 *          
 * 
 * <p>Classe Java de ReleaseLineType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="ReleaseLineType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ReleaseLineGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="PackagedItemRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}PackagedItemRefType" minOccurs="0"/>
 *         &lt;element name="OrderBaseGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="OrderBaseLineGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="InitialItemGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="ItemQuantity" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ItemQuantityType" minOccurs="0"/>
 *         &lt;element name="PackageDimensions" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}PackageDimensionsType" minOccurs="0"/>
 *         &lt;element name="ManufacturedCountryCode3Gid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="IsDrawback" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PackagedItemSpecRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLShipUnitSpecRefType" minOccurs="0"/>
 *         &lt;element name="PackagedItemSpecCount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NumLayersPerShipUnit" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="QuantityPerLayer" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TransportHandlingUnitRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLShipUnitSpecRefType" minOccurs="0"/>
 *         &lt;element name="BuyGeneralLedgerGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="SellGeneralLedgerGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="CommercialInvoiceGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="CommercialInvoiceDesc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SecondaryWeightVolume" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}WeightVolumeType" minOccurs="0"/>
 *         &lt;element name="ReleaseLineHazmatInfo" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ReleaseLineHazmatInfoType" minOccurs="0"/>
 *         &lt;element name="BilledQuantity" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFlexQuantityType" minOccurs="0"/>
 *         &lt;element name="PricePerUnit" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}PricePerUnitType" minOccurs="0"/>
 *         &lt;element name="TotalBilledAmount" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/>
 *         &lt;element name="FreeAlongSide" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/>
 *         &lt;element name="BrandName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsSplitAllowed" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ShipUnitSpecProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="ItemAttributes" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ItemAttributeType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Refnum" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RefnumType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Remark" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RemarkType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="OrLineSpecialService" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}SpecialServiceType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="InvolvedParty" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}InvolvedPartyType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="ReleaseLineIntlClassification" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ReleaseLineIntlClassificationType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Text" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TextType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="ReleaseLineAllocationInfo" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="ReleaseLineAllocByType" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ReleaseAllocType" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="OrLineEquipRefUnit" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}THUCapacityEquipRefUnitType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="OrLinePackageRefUnit" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}THUCapacityPackageRefUnitType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="FlexFieldStrings" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldStringType" minOccurs="0"/>
 *         &lt;element name="FlexFieldNumbers" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldNumberType" minOccurs="0"/>
 *         &lt;element name="FlexFieldDates" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldDateType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReleaseLineType", propOrder = {
    "releaseLineGid",
    "packagedItemRef",
    "orderBaseGid",
    "orderBaseLineGid",
    "initialItemGid",
    "itemQuantity",
    "packageDimensions",
    "manufacturedCountryCode3Gid",
    "isDrawback",
    "packagedItemSpecRef",
    "packagedItemSpecCount",
    "numLayersPerShipUnit",
    "quantityPerLayer",
    "transportHandlingUnitRef",
    "buyGeneralLedgerGid",
    "sellGeneralLedgerGid",
    "commercialInvoiceGid",
    "commercialInvoiceDesc",
    "secondaryWeightVolume",
    "releaseLineHazmatInfo",
    "billedQuantity",
    "pricePerUnit",
    "totalBilledAmount",
    "freeAlongSide",
    "brandName",
    "isSplitAllowed",
    "shipUnitSpecProfileGid",
    "itemAttributes",
    "refnum",
    "remark",
    "orLineSpecialService",
    "involvedParty",
    "releaseLineIntlClassification",
    "text",
    "releaseLineAllocationInfo",
    "orLineEquipRefUnit",
    "orLinePackageRefUnit",
    "flexFieldStrings",
    "flexFieldNumbers",
    "flexFieldDates"
})
public class ReleaseLineType {

    @XmlElement(name = "ReleaseLineGid", required = true)
    protected GLogXMLGidType releaseLineGid;
    @XmlElement(name = "PackagedItemRef")
    protected PackagedItemRefType packagedItemRef;
    @XmlElement(name = "OrderBaseGid")
    protected GLogXMLGidType orderBaseGid;
    @XmlElement(name = "OrderBaseLineGid")
    protected GLogXMLGidType orderBaseLineGid;
    @XmlElement(name = "InitialItemGid")
    protected GLogXMLGidType initialItemGid;
    @XmlElement(name = "ItemQuantity")
    protected ItemQuantityType itemQuantity;
    @XmlElement(name = "PackageDimensions")
    protected PackageDimensionsType packageDimensions;
    @XmlElement(name = "ManufacturedCountryCode3Gid")
    protected GLogXMLGidType manufacturedCountryCode3Gid;
    @XmlElement(name = "IsDrawback")
    protected String isDrawback;
    @XmlElement(name = "PackagedItemSpecRef")
    protected GLogXMLShipUnitSpecRefType packagedItemSpecRef;
    @XmlElement(name = "PackagedItemSpecCount")
    protected String packagedItemSpecCount;
    @XmlElement(name = "NumLayersPerShipUnit")
    protected String numLayersPerShipUnit;
    @XmlElement(name = "QuantityPerLayer")
    protected String quantityPerLayer;
    @XmlElement(name = "TransportHandlingUnitRef")
    protected GLogXMLShipUnitSpecRefType transportHandlingUnitRef;
    @XmlElement(name = "BuyGeneralLedgerGid")
    protected GLogXMLGidType buyGeneralLedgerGid;
    @XmlElement(name = "SellGeneralLedgerGid")
    protected GLogXMLGidType sellGeneralLedgerGid;
    @XmlElement(name = "CommercialInvoiceGid")
    protected GLogXMLGidType commercialInvoiceGid;
    @XmlElement(name = "CommercialInvoiceDesc")
    protected String commercialInvoiceDesc;
    @XmlElement(name = "SecondaryWeightVolume")
    protected WeightVolumeType secondaryWeightVolume;
    @XmlElement(name = "ReleaseLineHazmatInfo")
    protected ReleaseLineHazmatInfoType releaseLineHazmatInfo;
    @XmlElement(name = "BilledQuantity")
    protected GLogXMLFlexQuantityType billedQuantity;
    @XmlElement(name = "PricePerUnit")
    protected PricePerUnitType pricePerUnit;
    @XmlElement(name = "TotalBilledAmount")
    protected GLogXMLFinancialAmountType totalBilledAmount;
    @XmlElement(name = "FreeAlongSide")
    protected GLogXMLFinancialAmountType freeAlongSide;
    @XmlElement(name = "BrandName")
    protected String brandName;
    @XmlElement(name = "IsSplitAllowed")
    protected String isSplitAllowed;
    @XmlElement(name = "ShipUnitSpecProfileGid")
    protected GLogXMLGidType shipUnitSpecProfileGid;
    @XmlElement(name = "ItemAttributes")
    protected List<ItemAttributeType> itemAttributes;
    @XmlElement(name = "Refnum")
    protected List<RefnumType> refnum;
    @XmlElement(name = "Remark")
    protected List<RemarkType> remark;
    @XmlElement(name = "OrLineSpecialService")
    protected List<SpecialServiceType> orLineSpecialService;
    @XmlElement(name = "InvolvedParty")
    protected List<InvolvedPartyType> involvedParty;
    @XmlElement(name = "ReleaseLineIntlClassification")
    protected List<ReleaseLineIntlClassificationType> releaseLineIntlClassification;
    @XmlElement(name = "Text")
    protected List<TextType> text;
    @XmlElement(name = "ReleaseLineAllocationInfo")
    protected ReleaseLineType.ReleaseLineAllocationInfo releaseLineAllocationInfo;
    @XmlElement(name = "OrLineEquipRefUnit")
    protected List<THUCapacityEquipRefUnitType> orLineEquipRefUnit;
    @XmlElement(name = "OrLinePackageRefUnit")
    protected List<THUCapacityPackageRefUnitType> orLinePackageRefUnit;
    @XmlElement(name = "FlexFieldStrings")
    protected FlexFieldStringType flexFieldStrings;
    @XmlElement(name = "FlexFieldNumbers")
    protected FlexFieldNumberType flexFieldNumbers;
    @XmlElement(name = "FlexFieldDates")
    protected FlexFieldDateType flexFieldDates;

    /**
     * Obtém o valor da propriedade releaseLineGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getReleaseLineGid() {
        return releaseLineGid;
    }

    /**
     * Define o valor da propriedade releaseLineGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setReleaseLineGid(GLogXMLGidType value) {
        this.releaseLineGid = value;
    }

    /**
     * Obtém o valor da propriedade packagedItemRef.
     * 
     * @return
     *     possible object is
     *     {@link PackagedItemRefType }
     *     
     */
    public PackagedItemRefType getPackagedItemRef() {
        return packagedItemRef;
    }

    /**
     * Define o valor da propriedade packagedItemRef.
     * 
     * @param value
     *     allowed object is
     *     {@link PackagedItemRefType }
     *     
     */
    public void setPackagedItemRef(PackagedItemRefType value) {
        this.packagedItemRef = value;
    }

    /**
     * Obtém o valor da propriedade orderBaseGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getOrderBaseGid() {
        return orderBaseGid;
    }

    /**
     * Define o valor da propriedade orderBaseGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setOrderBaseGid(GLogXMLGidType value) {
        this.orderBaseGid = value;
    }

    /**
     * Obtém o valor da propriedade orderBaseLineGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getOrderBaseLineGid() {
        return orderBaseLineGid;
    }

    /**
     * Define o valor da propriedade orderBaseLineGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setOrderBaseLineGid(GLogXMLGidType value) {
        this.orderBaseLineGid = value;
    }

    /**
     * Obtém o valor da propriedade initialItemGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getInitialItemGid() {
        return initialItemGid;
    }

    /**
     * Define o valor da propriedade initialItemGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setInitialItemGid(GLogXMLGidType value) {
        this.initialItemGid = value;
    }

    /**
     * Obtém o valor da propriedade itemQuantity.
     * 
     * @return
     *     possible object is
     *     {@link ItemQuantityType }
     *     
     */
    public ItemQuantityType getItemQuantity() {
        return itemQuantity;
    }

    /**
     * Define o valor da propriedade itemQuantity.
     * 
     * @param value
     *     allowed object is
     *     {@link ItemQuantityType }
     *     
     */
    public void setItemQuantity(ItemQuantityType value) {
        this.itemQuantity = value;
    }

    /**
     * Obtém o valor da propriedade packageDimensions.
     * 
     * @return
     *     possible object is
     *     {@link PackageDimensionsType }
     *     
     */
    public PackageDimensionsType getPackageDimensions() {
        return packageDimensions;
    }

    /**
     * Define o valor da propriedade packageDimensions.
     * 
     * @param value
     *     allowed object is
     *     {@link PackageDimensionsType }
     *     
     */
    public void setPackageDimensions(PackageDimensionsType value) {
        this.packageDimensions = value;
    }

    /**
     * Obtém o valor da propriedade manufacturedCountryCode3Gid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getManufacturedCountryCode3Gid() {
        return manufacturedCountryCode3Gid;
    }

    /**
     * Define o valor da propriedade manufacturedCountryCode3Gid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setManufacturedCountryCode3Gid(GLogXMLGidType value) {
        this.manufacturedCountryCode3Gid = value;
    }

    /**
     * Obtém o valor da propriedade isDrawback.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsDrawback() {
        return isDrawback;
    }

    /**
     * Define o valor da propriedade isDrawback.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsDrawback(String value) {
        this.isDrawback = value;
    }

    /**
     * Obtém o valor da propriedade packagedItemSpecRef.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLShipUnitSpecRefType }
     *     
     */
    public GLogXMLShipUnitSpecRefType getPackagedItemSpecRef() {
        return packagedItemSpecRef;
    }

    /**
     * Define o valor da propriedade packagedItemSpecRef.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLShipUnitSpecRefType }
     *     
     */
    public void setPackagedItemSpecRef(GLogXMLShipUnitSpecRefType value) {
        this.packagedItemSpecRef = value;
    }

    /**
     * Obtém o valor da propriedade packagedItemSpecCount.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPackagedItemSpecCount() {
        return packagedItemSpecCount;
    }

    /**
     * Define o valor da propriedade packagedItemSpecCount.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPackagedItemSpecCount(String value) {
        this.packagedItemSpecCount = value;
    }

    /**
     * Obtém o valor da propriedade numLayersPerShipUnit.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumLayersPerShipUnit() {
        return numLayersPerShipUnit;
    }

    /**
     * Define o valor da propriedade numLayersPerShipUnit.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumLayersPerShipUnit(String value) {
        this.numLayersPerShipUnit = value;
    }

    /**
     * Obtém o valor da propriedade quantityPerLayer.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQuantityPerLayer() {
        return quantityPerLayer;
    }

    /**
     * Define o valor da propriedade quantityPerLayer.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQuantityPerLayer(String value) {
        this.quantityPerLayer = value;
    }

    /**
     * Obtém o valor da propriedade transportHandlingUnitRef.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLShipUnitSpecRefType }
     *     
     */
    public GLogXMLShipUnitSpecRefType getTransportHandlingUnitRef() {
        return transportHandlingUnitRef;
    }

    /**
     * Define o valor da propriedade transportHandlingUnitRef.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLShipUnitSpecRefType }
     *     
     */
    public void setTransportHandlingUnitRef(GLogXMLShipUnitSpecRefType value) {
        this.transportHandlingUnitRef = value;
    }

    /**
     * Obtém o valor da propriedade buyGeneralLedgerGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getBuyGeneralLedgerGid() {
        return buyGeneralLedgerGid;
    }

    /**
     * Define o valor da propriedade buyGeneralLedgerGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setBuyGeneralLedgerGid(GLogXMLGidType value) {
        this.buyGeneralLedgerGid = value;
    }

    /**
     * Obtém o valor da propriedade sellGeneralLedgerGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getSellGeneralLedgerGid() {
        return sellGeneralLedgerGid;
    }

    /**
     * Define o valor da propriedade sellGeneralLedgerGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setSellGeneralLedgerGid(GLogXMLGidType value) {
        this.sellGeneralLedgerGid = value;
    }

    /**
     * Obtém o valor da propriedade commercialInvoiceGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getCommercialInvoiceGid() {
        return commercialInvoiceGid;
    }

    /**
     * Define o valor da propriedade commercialInvoiceGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setCommercialInvoiceGid(GLogXMLGidType value) {
        this.commercialInvoiceGid = value;
    }

    /**
     * Obtém o valor da propriedade commercialInvoiceDesc.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCommercialInvoiceDesc() {
        return commercialInvoiceDesc;
    }

    /**
     * Define o valor da propriedade commercialInvoiceDesc.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCommercialInvoiceDesc(String value) {
        this.commercialInvoiceDesc = value;
    }

    /**
     * Obtém o valor da propriedade secondaryWeightVolume.
     * 
     * @return
     *     possible object is
     *     {@link WeightVolumeType }
     *     
     */
    public WeightVolumeType getSecondaryWeightVolume() {
        return secondaryWeightVolume;
    }

    /**
     * Define o valor da propriedade secondaryWeightVolume.
     * 
     * @param value
     *     allowed object is
     *     {@link WeightVolumeType }
     *     
     */
    public void setSecondaryWeightVolume(WeightVolumeType value) {
        this.secondaryWeightVolume = value;
    }

    /**
     * Obtém o valor da propriedade releaseLineHazmatInfo.
     * 
     * @return
     *     possible object is
     *     {@link ReleaseLineHazmatInfoType }
     *     
     */
    public ReleaseLineHazmatInfoType getReleaseLineHazmatInfo() {
        return releaseLineHazmatInfo;
    }

    /**
     * Define o valor da propriedade releaseLineHazmatInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link ReleaseLineHazmatInfoType }
     *     
     */
    public void setReleaseLineHazmatInfo(ReleaseLineHazmatInfoType value) {
        this.releaseLineHazmatInfo = value;
    }

    /**
     * Obtém o valor da propriedade billedQuantity.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFlexQuantityType }
     *     
     */
    public GLogXMLFlexQuantityType getBilledQuantity() {
        return billedQuantity;
    }

    /**
     * Define o valor da propriedade billedQuantity.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFlexQuantityType }
     *     
     */
    public void setBilledQuantity(GLogXMLFlexQuantityType value) {
        this.billedQuantity = value;
    }

    /**
     * Obtém o valor da propriedade pricePerUnit.
     * 
     * @return
     *     possible object is
     *     {@link PricePerUnitType }
     *     
     */
    public PricePerUnitType getPricePerUnit() {
        return pricePerUnit;
    }

    /**
     * Define o valor da propriedade pricePerUnit.
     * 
     * @param value
     *     allowed object is
     *     {@link PricePerUnitType }
     *     
     */
    public void setPricePerUnit(PricePerUnitType value) {
        this.pricePerUnit = value;
    }

    /**
     * Obtém o valor da propriedade totalBilledAmount.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getTotalBilledAmount() {
        return totalBilledAmount;
    }

    /**
     * Define o valor da propriedade totalBilledAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setTotalBilledAmount(GLogXMLFinancialAmountType value) {
        this.totalBilledAmount = value;
    }

    /**
     * Obtém o valor da propriedade freeAlongSide.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getFreeAlongSide() {
        return freeAlongSide;
    }

    /**
     * Define o valor da propriedade freeAlongSide.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setFreeAlongSide(GLogXMLFinancialAmountType value) {
        this.freeAlongSide = value;
    }

    /**
     * Obtém o valor da propriedade brandName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBrandName() {
        return brandName;
    }

    /**
     * Define o valor da propriedade brandName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBrandName(String value) {
        this.brandName = value;
    }

    /**
     * Obtém o valor da propriedade isSplitAllowed.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsSplitAllowed() {
        return isSplitAllowed;
    }

    /**
     * Define o valor da propriedade isSplitAllowed.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsSplitAllowed(String value) {
        this.isSplitAllowed = value;
    }

    /**
     * Obtém o valor da propriedade shipUnitSpecProfileGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getShipUnitSpecProfileGid() {
        return shipUnitSpecProfileGid;
    }

    /**
     * Define o valor da propriedade shipUnitSpecProfileGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setShipUnitSpecProfileGid(GLogXMLGidType value) {
        this.shipUnitSpecProfileGid = value;
    }

    /**
     * Gets the value of the itemAttributes property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the itemAttributes property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getItemAttributes().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ItemAttributeType }
     * 
     * 
     */
    public List<ItemAttributeType> getItemAttributes() {
        if (itemAttributes == null) {
            itemAttributes = new ArrayList<ItemAttributeType>();
        }
        return this.itemAttributes;
    }

    /**
     * Gets the value of the refnum property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the refnum property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRefnum().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RefnumType }
     * 
     * 
     */
    public List<RefnumType> getRefnum() {
        if (refnum == null) {
            refnum = new ArrayList<RefnumType>();
        }
        return this.refnum;
    }

    /**
     * Gets the value of the remark property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the remark property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRemark().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RemarkType }
     * 
     * 
     */
    public List<RemarkType> getRemark() {
        if (remark == null) {
            remark = new ArrayList<RemarkType>();
        }
        return this.remark;
    }

    /**
     * Gets the value of the orLineSpecialService property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the orLineSpecialService property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOrLineSpecialService().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SpecialServiceType }
     * 
     * 
     */
    public List<SpecialServiceType> getOrLineSpecialService() {
        if (orLineSpecialService == null) {
            orLineSpecialService = new ArrayList<SpecialServiceType>();
        }
        return this.orLineSpecialService;
    }

    /**
     * Gets the value of the involvedParty property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the involvedParty property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInvolvedParty().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link InvolvedPartyType }
     * 
     * 
     */
    public List<InvolvedPartyType> getInvolvedParty() {
        if (involvedParty == null) {
            involvedParty = new ArrayList<InvolvedPartyType>();
        }
        return this.involvedParty;
    }

    /**
     * Gets the value of the releaseLineIntlClassification property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the releaseLineIntlClassification property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReleaseLineIntlClassification().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ReleaseLineIntlClassificationType }
     * 
     * 
     */
    public List<ReleaseLineIntlClassificationType> getReleaseLineIntlClassification() {
        if (releaseLineIntlClassification == null) {
            releaseLineIntlClassification = new ArrayList<ReleaseLineIntlClassificationType>();
        }
        return this.releaseLineIntlClassification;
    }

    /**
     * Gets the value of the text property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the text property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getText().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TextType }
     * 
     * 
     */
    public List<TextType> getText() {
        if (text == null) {
            text = new ArrayList<TextType>();
        }
        return this.text;
    }

    /**
     * Obtém o valor da propriedade releaseLineAllocationInfo.
     * 
     * @return
     *     possible object is
     *     {@link ReleaseLineType.ReleaseLineAllocationInfo }
     *     
     */
    public ReleaseLineType.ReleaseLineAllocationInfo getReleaseLineAllocationInfo() {
        return releaseLineAllocationInfo;
    }

    /**
     * Define o valor da propriedade releaseLineAllocationInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link ReleaseLineType.ReleaseLineAllocationInfo }
     *     
     */
    public void setReleaseLineAllocationInfo(ReleaseLineType.ReleaseLineAllocationInfo value) {
        this.releaseLineAllocationInfo = value;
    }

    /**
     * Gets the value of the orLineEquipRefUnit property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the orLineEquipRefUnit property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOrLineEquipRefUnit().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link THUCapacityEquipRefUnitType }
     * 
     * 
     */
    public List<THUCapacityEquipRefUnitType> getOrLineEquipRefUnit() {
        if (orLineEquipRefUnit == null) {
            orLineEquipRefUnit = new ArrayList<THUCapacityEquipRefUnitType>();
        }
        return this.orLineEquipRefUnit;
    }

    /**
     * Gets the value of the orLinePackageRefUnit property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the orLinePackageRefUnit property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOrLinePackageRefUnit().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link THUCapacityPackageRefUnitType }
     * 
     * 
     */
    public List<THUCapacityPackageRefUnitType> getOrLinePackageRefUnit() {
        if (orLinePackageRefUnit == null) {
            orLinePackageRefUnit = new ArrayList<THUCapacityPackageRefUnitType>();
        }
        return this.orLinePackageRefUnit;
    }

    /**
     * Obtém o valor da propriedade flexFieldStrings.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldStringType }
     *     
     */
    public FlexFieldStringType getFlexFieldStrings() {
        return flexFieldStrings;
    }

    /**
     * Define o valor da propriedade flexFieldStrings.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldStringType }
     *     
     */
    public void setFlexFieldStrings(FlexFieldStringType value) {
        this.flexFieldStrings = value;
    }

    /**
     * Obtém o valor da propriedade flexFieldNumbers.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldNumberType }
     *     
     */
    public FlexFieldNumberType getFlexFieldNumbers() {
        return flexFieldNumbers;
    }

    /**
     * Define o valor da propriedade flexFieldNumbers.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldNumberType }
     *     
     */
    public void setFlexFieldNumbers(FlexFieldNumberType value) {
        this.flexFieldNumbers = value;
    }

    /**
     * Obtém o valor da propriedade flexFieldDates.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldDateType }
     *     
     */
    public FlexFieldDateType getFlexFieldDates() {
        return flexFieldDates;
    }

    /**
     * Define o valor da propriedade flexFieldDates.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldDateType }
     *     
     */
    public void setFlexFieldDates(FlexFieldDateType value) {
        this.flexFieldDates = value;
    }


    /**
     * <p>Classe Java de anonymous complex type.
     * 
     * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="ReleaseLineAllocByType" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ReleaseAllocType" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "releaseLineAllocByType"
    })
    public static class ReleaseLineAllocationInfo {

        @XmlElement(name = "ReleaseLineAllocByType")
        protected List<ReleaseAllocType> releaseLineAllocByType;

        /**
         * Gets the value of the releaseLineAllocByType property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the releaseLineAllocByType property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getReleaseLineAllocByType().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link ReleaseAllocType }
         * 
         * 
         */
        public List<ReleaseAllocType> getReleaseLineAllocByType() {
            if (releaseLineAllocByType == null) {
                releaseLineAllocByType = new ArrayList<ReleaseAllocType>();
            }
            return this.releaseLineAllocByType;
        }

    }

}
