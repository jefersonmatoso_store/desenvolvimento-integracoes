
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de EventReasonType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="EventReasonType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="QuickCodeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="ResponsiblePartyGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="StatusGroup" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}StatusGroupType" minOccurs="0"/>
 *         &lt;element name="ReasonGroup" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ReasonGroupType" minOccurs="0"/>
 *         &lt;element name="StatusCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}StatusCodeType" minOccurs="0"/>
 *         &lt;element name="ReasonCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ReasonCodeType" minOccurs="0"/>
 *         &lt;element name="ReasonComment" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EventReasonType", propOrder = {
    "quickCodeGid",
    "responsiblePartyGid",
    "statusGroup",
    "reasonGroup",
    "statusCode",
    "reasonCode",
    "reasonComment"
})
public class EventReasonType {

    @XmlElement(name = "QuickCodeGid")
    protected GLogXMLGidType quickCodeGid;
    @XmlElement(name = "ResponsiblePartyGid")
    protected GLogXMLGidType responsiblePartyGid;
    @XmlElement(name = "StatusGroup")
    protected StatusGroupType statusGroup;
    @XmlElement(name = "ReasonGroup")
    protected ReasonGroupType reasonGroup;
    @XmlElement(name = "StatusCode")
    protected StatusCodeType statusCode;
    @XmlElement(name = "ReasonCode")
    protected ReasonCodeType reasonCode;
    @XmlElement(name = "ReasonComment")
    protected String reasonComment;

    /**
     * Obtém o valor da propriedade quickCodeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getQuickCodeGid() {
        return quickCodeGid;
    }

    /**
     * Define o valor da propriedade quickCodeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setQuickCodeGid(GLogXMLGidType value) {
        this.quickCodeGid = value;
    }

    /**
     * Obtém o valor da propriedade responsiblePartyGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getResponsiblePartyGid() {
        return responsiblePartyGid;
    }

    /**
     * Define o valor da propriedade responsiblePartyGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setResponsiblePartyGid(GLogXMLGidType value) {
        this.responsiblePartyGid = value;
    }

    /**
     * Obtém o valor da propriedade statusGroup.
     * 
     * @return
     *     possible object is
     *     {@link StatusGroupType }
     *     
     */
    public StatusGroupType getStatusGroup() {
        return statusGroup;
    }

    /**
     * Define o valor da propriedade statusGroup.
     * 
     * @param value
     *     allowed object is
     *     {@link StatusGroupType }
     *     
     */
    public void setStatusGroup(StatusGroupType value) {
        this.statusGroup = value;
    }

    /**
     * Obtém o valor da propriedade reasonGroup.
     * 
     * @return
     *     possible object is
     *     {@link ReasonGroupType }
     *     
     */
    public ReasonGroupType getReasonGroup() {
        return reasonGroup;
    }

    /**
     * Define o valor da propriedade reasonGroup.
     * 
     * @param value
     *     allowed object is
     *     {@link ReasonGroupType }
     *     
     */
    public void setReasonGroup(ReasonGroupType value) {
        this.reasonGroup = value;
    }

    /**
     * Obtém o valor da propriedade statusCode.
     * 
     * @return
     *     possible object is
     *     {@link StatusCodeType }
     *     
     */
    public StatusCodeType getStatusCode() {
        return statusCode;
    }

    /**
     * Define o valor da propriedade statusCode.
     * 
     * @param value
     *     allowed object is
     *     {@link StatusCodeType }
     *     
     */
    public void setStatusCode(StatusCodeType value) {
        this.statusCode = value;
    }

    /**
     * Obtém o valor da propriedade reasonCode.
     * 
     * @return
     *     possible object is
     *     {@link ReasonCodeType }
     *     
     */
    public ReasonCodeType getReasonCode() {
        return reasonCode;
    }

    /**
     * Define o valor da propriedade reasonCode.
     * 
     * @param value
     *     allowed object is
     *     {@link ReasonCodeType }
     *     
     */
    public void setReasonCode(ReasonCodeType value) {
        this.reasonCode = value;
    }

    /**
     * Obtém o valor da propriedade reasonComment.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReasonComment() {
        return reasonComment;
    }

    /**
     * Define o valor da propriedade reasonComment.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReasonComment(String value) {
        this.reasonComment = value;
    }

}
