
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * ShipUnitSpecRef may be used to reference an existing ship unit spec, or define a new one.
 * 
 * <p>Classe Java de ShipUnitSpecRefType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="ShipUnitSpecRefType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element name="ShipUnitSpecGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="ShipUnitSpec" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ShipUnitSpecType"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ShipUnitSpecRefType", propOrder = {
    "shipUnitSpecGid",
    "shipUnitSpec"
})
public class ShipUnitSpecRefType {

    @XmlElement(name = "ShipUnitSpecGid")
    protected GLogXMLGidType shipUnitSpecGid;
    @XmlElement(name = "ShipUnitSpec")
    protected ShipUnitSpecType shipUnitSpec;

    /**
     * Obtém o valor da propriedade shipUnitSpecGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getShipUnitSpecGid() {
        return shipUnitSpecGid;
    }

    /**
     * Define o valor da propriedade shipUnitSpecGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setShipUnitSpecGid(GLogXMLGidType value) {
        this.shipUnitSpecGid = value;
    }

    /**
     * Obtém o valor da propriedade shipUnitSpec.
     * 
     * @return
     *     possible object is
     *     {@link ShipUnitSpecType }
     *     
     */
    public ShipUnitSpecType getShipUnitSpec() {
        return shipUnitSpec;
    }

    /**
     * Define o valor da propriedade shipUnitSpec.
     * 
     * @param value
     *     allowed object is
     *     {@link ShipUnitSpecType }
     *     
     */
    public void setShipUnitSpec(ShipUnitSpecType value) {
        this.shipUnitSpec = value;
    }

}
