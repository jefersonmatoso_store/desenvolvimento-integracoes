
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;


/**
 * 
 *             Route Template Leg represents a leg of the route for the plan for a cooperative route.
 *          
 * 
 * <p>Classe Java de RouteTemplateLegType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="RouteTemplateLegType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IntSavedQuery" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}IntSavedQueryType" minOccurs="0"/>
 *         &lt;element name="RouteTemplateLegGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="TransactionCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransactionCodeType" minOccurs="0"/>
 *         &lt;element name="RouteTemplateLegHdr" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RouteTemplateLegHdrType" minOccurs="0"/>
 *         &lt;element name="RouteTempLegLocIncompat" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="LocCompat" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="LocationRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LocationRefType"/>
 *                             &lt;element name="ActivityGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="RouteTempLegOrderCompat" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Refnum" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RefnumType" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="RouteTempLegShipCompat" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Refnum" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RefnumType" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="Refnum" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RefnumType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Remark" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RemarkType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="InvolvedParty" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}InvolvedPartyType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RouteTemplateLegType", propOrder = {
    "intSavedQuery",
    "routeTemplateLegGid",
    "transactionCode",
    "routeTemplateLegHdr",
    "routeTempLegLocIncompat",
    "routeTempLegOrderCompat",
    "routeTempLegShipCompat",
    "refnum",
    "remark",
    "involvedParty"
})
public class RouteTemplateLegType {

    @XmlElement(name = "IntSavedQuery")
    protected IntSavedQueryType intSavedQuery;
    @XmlElement(name = "RouteTemplateLegGid", required = true)
    protected GLogXMLGidType routeTemplateLegGid;
    @XmlElement(name = "TransactionCode")
    @XmlSchemaType(name = "string")
    protected TransactionCodeType transactionCode;
    @XmlElement(name = "RouteTemplateLegHdr")
    protected RouteTemplateLegHdrType routeTemplateLegHdr;
    @XmlElement(name = "RouteTempLegLocIncompat")
    protected RouteTemplateLegType.RouteTempLegLocIncompat routeTempLegLocIncompat;
    @XmlElement(name = "RouteTempLegOrderCompat")
    protected RouteTemplateLegType.RouteTempLegOrderCompat routeTempLegOrderCompat;
    @XmlElement(name = "RouteTempLegShipCompat")
    protected RouteTemplateLegType.RouteTempLegShipCompat routeTempLegShipCompat;
    @XmlElement(name = "Refnum")
    protected List<RefnumType> refnum;
    @XmlElement(name = "Remark")
    protected List<RemarkType> remark;
    @XmlElement(name = "InvolvedParty")
    protected List<InvolvedPartyType> involvedParty;

    /**
     * Obtém o valor da propriedade intSavedQuery.
     * 
     * @return
     *     possible object is
     *     {@link IntSavedQueryType }
     *     
     */
    public IntSavedQueryType getIntSavedQuery() {
        return intSavedQuery;
    }

    /**
     * Define o valor da propriedade intSavedQuery.
     * 
     * @param value
     *     allowed object is
     *     {@link IntSavedQueryType }
     *     
     */
    public void setIntSavedQuery(IntSavedQueryType value) {
        this.intSavedQuery = value;
    }

    /**
     * Obtém o valor da propriedade routeTemplateLegGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRouteTemplateLegGid() {
        return routeTemplateLegGid;
    }

    /**
     * Define o valor da propriedade routeTemplateLegGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRouteTemplateLegGid(GLogXMLGidType value) {
        this.routeTemplateLegGid = value;
    }

    /**
     * Obtém o valor da propriedade transactionCode.
     * 
     * @return
     *     possible object is
     *     {@link TransactionCodeType }
     *     
     */
    public TransactionCodeType getTransactionCode() {
        return transactionCode;
    }

    /**
     * Define o valor da propriedade transactionCode.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionCodeType }
     *     
     */
    public void setTransactionCode(TransactionCodeType value) {
        this.transactionCode = value;
    }

    /**
     * Obtém o valor da propriedade routeTemplateLegHdr.
     * 
     * @return
     *     possible object is
     *     {@link RouteTemplateLegHdrType }
     *     
     */
    public RouteTemplateLegHdrType getRouteTemplateLegHdr() {
        return routeTemplateLegHdr;
    }

    /**
     * Define o valor da propriedade routeTemplateLegHdr.
     * 
     * @param value
     *     allowed object is
     *     {@link RouteTemplateLegHdrType }
     *     
     */
    public void setRouteTemplateLegHdr(RouteTemplateLegHdrType value) {
        this.routeTemplateLegHdr = value;
    }

    /**
     * Obtém o valor da propriedade routeTempLegLocIncompat.
     * 
     * @return
     *     possible object is
     *     {@link RouteTemplateLegType.RouteTempLegLocIncompat }
     *     
     */
    public RouteTemplateLegType.RouteTempLegLocIncompat getRouteTempLegLocIncompat() {
        return routeTempLegLocIncompat;
    }

    /**
     * Define o valor da propriedade routeTempLegLocIncompat.
     * 
     * @param value
     *     allowed object is
     *     {@link RouteTemplateLegType.RouteTempLegLocIncompat }
     *     
     */
    public void setRouteTempLegLocIncompat(RouteTemplateLegType.RouteTempLegLocIncompat value) {
        this.routeTempLegLocIncompat = value;
    }

    /**
     * Obtém o valor da propriedade routeTempLegOrderCompat.
     * 
     * @return
     *     possible object is
     *     {@link RouteTemplateLegType.RouteTempLegOrderCompat }
     *     
     */
    public RouteTemplateLegType.RouteTempLegOrderCompat getRouteTempLegOrderCompat() {
        return routeTempLegOrderCompat;
    }

    /**
     * Define o valor da propriedade routeTempLegOrderCompat.
     * 
     * @param value
     *     allowed object is
     *     {@link RouteTemplateLegType.RouteTempLegOrderCompat }
     *     
     */
    public void setRouteTempLegOrderCompat(RouteTemplateLegType.RouteTempLegOrderCompat value) {
        this.routeTempLegOrderCompat = value;
    }

    /**
     * Obtém o valor da propriedade routeTempLegShipCompat.
     * 
     * @return
     *     possible object is
     *     {@link RouteTemplateLegType.RouteTempLegShipCompat }
     *     
     */
    public RouteTemplateLegType.RouteTempLegShipCompat getRouteTempLegShipCompat() {
        return routeTempLegShipCompat;
    }

    /**
     * Define o valor da propriedade routeTempLegShipCompat.
     * 
     * @param value
     *     allowed object is
     *     {@link RouteTemplateLegType.RouteTempLegShipCompat }
     *     
     */
    public void setRouteTempLegShipCompat(RouteTemplateLegType.RouteTempLegShipCompat value) {
        this.routeTempLegShipCompat = value;
    }

    /**
     * Gets the value of the refnum property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the refnum property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRefnum().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RefnumType }
     * 
     * 
     */
    public List<RefnumType> getRefnum() {
        if (refnum == null) {
            refnum = new ArrayList<RefnumType>();
        }
        return this.refnum;
    }

    /**
     * Gets the value of the remark property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the remark property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRemark().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RemarkType }
     * 
     * 
     */
    public List<RemarkType> getRemark() {
        if (remark == null) {
            remark = new ArrayList<RemarkType>();
        }
        return this.remark;
    }

    /**
     * Gets the value of the involvedParty property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the involvedParty property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInvolvedParty().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link InvolvedPartyType }
     * 
     * 
     */
    public List<InvolvedPartyType> getInvolvedParty() {
        if (involvedParty == null) {
            involvedParty = new ArrayList<InvolvedPartyType>();
        }
        return this.involvedParty;
    }


    /**
     * <p>Classe Java de anonymous complex type.
     * 
     * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="LocCompat" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="LocationRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LocationRefType"/>
     *                   &lt;element name="ActivityGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "locCompat"
    })
    public static class RouteTempLegLocIncompat {

        @XmlElement(name = "LocCompat")
        protected List<RouteTemplateLegType.RouteTempLegLocIncompat.LocCompat> locCompat;

        /**
         * Gets the value of the locCompat property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the locCompat property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getLocCompat().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link RouteTemplateLegType.RouteTempLegLocIncompat.LocCompat }
         * 
         * 
         */
        public List<RouteTemplateLegType.RouteTempLegLocIncompat.LocCompat> getLocCompat() {
            if (locCompat == null) {
                locCompat = new ArrayList<RouteTemplateLegType.RouteTempLegLocIncompat.LocCompat>();
            }
            return this.locCompat;
        }


        /**
         * <p>Classe Java de anonymous complex type.
         * 
         * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="LocationRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LocationRefType"/>
         *         &lt;element name="ActivityGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "locationRef",
            "activityGid"
        })
        public static class LocCompat {

            @XmlElement(name = "LocationRef", required = true)
            protected LocationRefType locationRef;
            @XmlElement(name = "ActivityGid")
            protected GLogXMLGidType activityGid;

            /**
             * Obtém o valor da propriedade locationRef.
             * 
             * @return
             *     possible object is
             *     {@link LocationRefType }
             *     
             */
            public LocationRefType getLocationRef() {
                return locationRef;
            }

            /**
             * Define o valor da propriedade locationRef.
             * 
             * @param value
             *     allowed object is
             *     {@link LocationRefType }
             *     
             */
            public void setLocationRef(LocationRefType value) {
                this.locationRef = value;
            }

            /**
             * Obtém o valor da propriedade activityGid.
             * 
             * @return
             *     possible object is
             *     {@link GLogXMLGidType }
             *     
             */
            public GLogXMLGidType getActivityGid() {
                return activityGid;
            }

            /**
             * Define o valor da propriedade activityGid.
             * 
             * @param value
             *     allowed object is
             *     {@link GLogXMLGidType }
             *     
             */
            public void setActivityGid(GLogXMLGidType value) {
                this.activityGid = value;
            }

        }

    }


    /**
     * <p>Classe Java de anonymous complex type.
     * 
     * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Refnum" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RefnumType" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "refnum"
    })
    public static class RouteTempLegOrderCompat {

        @XmlElement(name = "Refnum")
        protected List<RefnumType> refnum;

        /**
         * Gets the value of the refnum property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the refnum property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getRefnum().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link RefnumType }
         * 
         * 
         */
        public List<RefnumType> getRefnum() {
            if (refnum == null) {
                refnum = new ArrayList<RefnumType>();
            }
            return this.refnum;
        }

    }


    /**
     * <p>Classe Java de anonymous complex type.
     * 
     * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Refnum" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RefnumType" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "refnum"
    })
    public static class RouteTempLegShipCompat {

        @XmlElement(name = "Refnum")
        protected List<RefnumType> refnum;

        /**
         * Gets the value of the refnum property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the refnum property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getRefnum().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link RefnumType }
         * 
         * 
         */
        public List<RefnumType> getRefnum() {
            if (refnum == null) {
                refnum = new ArrayList<RefnumType>();
            }
            return this.refnum;
        }

    }

}
