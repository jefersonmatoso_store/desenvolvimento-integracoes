
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;


/**
 * 
 *             Release is a structure for describing an order release. A release respresents a shippable subset of a
 *             TransOrder.
 *             It corresponds to the TransOrderLines which share common transportation requirements.
 *             It includes a common source/destination, time window, and multiple release lines which
 *             share the source/destination and time window.
 *          
 * 
 * <p>Classe Java de ReleaseType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="ReleaseType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OTMTransactionInOut">
 *       &lt;sequence>
 *         &lt;element name="SendReason" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}SendReasonType" minOccurs="0"/>
 *         &lt;element name="IntSavedQuery" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}IntSavedQueryType" minOccurs="0"/>
 *         &lt;element name="ReleaseGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="TransactionCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransactionCodeType"/>
 *         &lt;element name="ReplaceChildren" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ReplaceChildrenType" minOccurs="0"/>
 *         &lt;element name="IntCommand" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}IntCommandType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="TransOrderGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="ReleaseHeader" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ReleaseHeaderType" minOccurs="0"/>
 *         &lt;element name="ShipFromLocationRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocRefType" minOccurs="0"/>
 *         &lt;element name="ShipFromLocOverrideRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocOverrideRefType" minOccurs="0"/>
 *         &lt;element name="ShipFromLoadPoint" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ShipToLocationRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocRefType" minOccurs="0"/>
 *         &lt;element name="ShipToLocOverrideRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocOverrideRefType" minOccurs="0"/>
 *         &lt;element name="ShipToUnloadPoint" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TimeWindow" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TimeWindowType" minOccurs="0"/>
 *         &lt;element name="DeclaredValue" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/>
 *         &lt;element name="ReleaseLine" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ReleaseLineType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="ShipUnit" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ShipUnitType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="ReleaseTypeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="PlanFromLocationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocGidType" minOccurs="0"/>
 *         &lt;element name="PlanFromLocOverrideRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocOverrideRefType" minOccurs="0"/>
 *         &lt;element name="PlanFromLoadPoint" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PlanToLocationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocGidType" minOccurs="0"/>
 *         &lt;element name="PlanToLocOverrideRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocOverrideRefType" minOccurs="0"/>
 *         &lt;element name="PlanToUnloadPoint" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PriLegSourceLocationRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocRefType" minOccurs="0"/>
 *         &lt;element name="PriLegSourceLocOverrideRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocOverrideRefType" minOccurs="0"/>
 *         &lt;element name="PortOfLoadLocationRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocRefType" minOccurs="0"/>
 *         &lt;element name="PortOfLoadLocOverrideRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocOverrideRefType" minOccurs="0"/>
 *         &lt;element name="PortOfDisLocationRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocRefType" minOccurs="0"/>
 *         &lt;element name="PortOfDisLocOverrideRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocOverrideRefType" minOccurs="0"/>
 *         &lt;element name="PriLegDestLocationRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocRefType" minOccurs="0"/>
 *         &lt;element name="PriLegDestLocOverrideRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocOverrideRefType" minOccurs="0"/>
 *         &lt;element name="BulkPlanGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="SellBulkPlanGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="PlanPartitionGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="BestDirectBuyCost" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/>
 *         &lt;element name="BestDirectBuyRateOfferingGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="BestDirectSellCost" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/>
 *         &lt;element name="BestDirectSellRateOfferingGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="BuyGeneralLedgerGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="SellGeneralLedgerGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="TotalWeightVolume" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLWeightVolumeType" minOccurs="0"/>
 *         &lt;element name="TotalNetWeightVolume" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}WeightVolumeType" minOccurs="0"/>
 *         &lt;element name="TotalPackagedItemSpecCount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TotalPackagedItemCount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ReleaseRefnum" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ReleaseRefnumType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="ReleaseStatus" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}StatusType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Remark" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RemarkType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="REquipment" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}REquipmentType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="InvolvedParty" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}InvolvedPartyType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="CommercialInvoice" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}CommercialInvoiceType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Text" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TextType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="ReleaseService" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ReleaseServiceType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="ReleaseShipmentInfo" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="LatestEstDeliveryDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType"/>
 *                   &lt;element name="EarliestEstPickupDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="ReleaseAllocationInfo" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="ReleaseAllocByType" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ReleaseAllocType" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="TransOrder" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransOrderType" minOccurs="0"/>
 *         &lt;element name="OrStop" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OrStopType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="OrderMovement" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OrderMovementType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="AllocationGroupGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReleaseType", propOrder = {
    "sendReason",
    "intSavedQuery",
    "releaseGid",
    "transactionCode",
    "replaceChildren",
    "intCommand",
    "transOrderGid",
    "releaseHeader",
    "shipFromLocationRef",
    "shipFromLocOverrideRef",
    "shipFromLoadPoint",
    "shipToLocationRef",
    "shipToLocOverrideRef",
    "shipToUnloadPoint",
    "timeWindow",
    "declaredValue",
    "releaseLine",
    "shipUnit",
    "releaseTypeGid",
    "planFromLocationGid",
    "planFromLocOverrideRef",
    "planFromLoadPoint",
    "planToLocationGid",
    "planToLocOverrideRef",
    "planToUnloadPoint",
    "priLegSourceLocationRef",
    "priLegSourceLocOverrideRef",
    "portOfLoadLocationRef",
    "portOfLoadLocOverrideRef",
    "portOfDisLocationRef",
    "portOfDisLocOverrideRef",
    "priLegDestLocationRef",
    "priLegDestLocOverrideRef",
    "bulkPlanGid",
    "sellBulkPlanGid",
    "planPartitionGid",
    "bestDirectBuyCost",
    "bestDirectBuyRateOfferingGid",
    "bestDirectSellCost",
    "bestDirectSellRateOfferingGid",
    "buyGeneralLedgerGid",
    "sellGeneralLedgerGid",
    "totalWeightVolume",
    "totalNetWeightVolume",
    "totalPackagedItemSpecCount",
    "totalPackagedItemCount",
    "releaseRefnum",
    "releaseStatus",
    "remark",
    "rEquipment",
    "involvedParty",
    "commercialInvoice",
    "text",
    "releaseService",
    "releaseShipmentInfo",
    "releaseAllocationInfo",
    "transOrder",
    "orStop",
    "orderMovement",
    "allocationGroupGid"
})
public class ReleaseType
    extends OTMTransactionInOut
{

    @XmlElement(name = "SendReason")
    protected SendReasonType sendReason;
    @XmlElement(name = "IntSavedQuery")
    protected IntSavedQueryType intSavedQuery;
    @XmlElement(name = "ReleaseGid", required = true)
    protected GLogXMLGidType releaseGid;
    @XmlElement(name = "TransactionCode", required = true)
    @XmlSchemaType(name = "string")
    protected TransactionCodeType transactionCode;
    @XmlElement(name = "ReplaceChildren")
    protected ReplaceChildrenType replaceChildren;
    @XmlElement(name = "IntCommand")
    protected List<IntCommandType> intCommand;
    @XmlElement(name = "TransOrderGid")
    protected GLogXMLGidType transOrderGid;
    @XmlElement(name = "ReleaseHeader")
    protected ReleaseHeaderType releaseHeader;
    @XmlElement(name = "ShipFromLocationRef")
    protected GLogXMLLocRefType shipFromLocationRef;
    @XmlElement(name = "ShipFromLocOverrideRef")
    protected GLogXMLLocOverrideRefType shipFromLocOverrideRef;
    @XmlElement(name = "ShipFromLoadPoint")
    protected String shipFromLoadPoint;
    @XmlElement(name = "ShipToLocationRef")
    protected GLogXMLLocRefType shipToLocationRef;
    @XmlElement(name = "ShipToLocOverrideRef")
    protected GLogXMLLocOverrideRefType shipToLocOverrideRef;
    @XmlElement(name = "ShipToUnloadPoint")
    protected String shipToUnloadPoint;
    @XmlElement(name = "TimeWindow")
    protected TimeWindowType timeWindow;
    @XmlElement(name = "DeclaredValue")
    protected GLogXMLFinancialAmountType declaredValue;
    @XmlElement(name = "ReleaseLine")
    protected List<ReleaseLineType> releaseLine;
    @XmlElement(name = "ShipUnit")
    protected List<ShipUnitType> shipUnit;
    @XmlElement(name = "ReleaseTypeGid")
    protected GLogXMLGidType releaseTypeGid;
    @XmlElement(name = "PlanFromLocationGid")
    protected GLogXMLLocGidType planFromLocationGid;
    @XmlElement(name = "PlanFromLocOverrideRef")
    protected GLogXMLLocOverrideRefType planFromLocOverrideRef;
    @XmlElement(name = "PlanFromLoadPoint")
    protected String planFromLoadPoint;
    @XmlElement(name = "PlanToLocationGid")
    protected GLogXMLLocGidType planToLocationGid;
    @XmlElement(name = "PlanToLocOverrideRef")
    protected GLogXMLLocOverrideRefType planToLocOverrideRef;
    @XmlElement(name = "PlanToUnloadPoint")
    protected String planToUnloadPoint;
    @XmlElement(name = "PriLegSourceLocationRef")
    protected GLogXMLLocRefType priLegSourceLocationRef;
    @XmlElement(name = "PriLegSourceLocOverrideRef")
    protected GLogXMLLocOverrideRefType priLegSourceLocOverrideRef;
    @XmlElement(name = "PortOfLoadLocationRef")
    protected GLogXMLLocRefType portOfLoadLocationRef;
    @XmlElement(name = "PortOfLoadLocOverrideRef")
    protected GLogXMLLocOverrideRefType portOfLoadLocOverrideRef;
    @XmlElement(name = "PortOfDisLocationRef")
    protected GLogXMLLocRefType portOfDisLocationRef;
    @XmlElement(name = "PortOfDisLocOverrideRef")
    protected GLogXMLLocOverrideRefType portOfDisLocOverrideRef;
    @XmlElement(name = "PriLegDestLocationRef")
    protected GLogXMLLocRefType priLegDestLocationRef;
    @XmlElement(name = "PriLegDestLocOverrideRef")
    protected GLogXMLLocOverrideRefType priLegDestLocOverrideRef;
    @XmlElement(name = "BulkPlanGid")
    protected GLogXMLGidType bulkPlanGid;
    @XmlElement(name = "SellBulkPlanGid")
    protected GLogXMLGidType sellBulkPlanGid;
    @XmlElement(name = "PlanPartitionGid")
    protected GLogXMLGidType planPartitionGid;
    @XmlElement(name = "BestDirectBuyCost")
    protected GLogXMLFinancialAmountType bestDirectBuyCost;
    @XmlElement(name = "BestDirectBuyRateOfferingGid")
    protected GLogXMLGidType bestDirectBuyRateOfferingGid;
    @XmlElement(name = "BestDirectSellCost")
    protected GLogXMLFinancialAmountType bestDirectSellCost;
    @XmlElement(name = "BestDirectSellRateOfferingGid")
    protected GLogXMLGidType bestDirectSellRateOfferingGid;
    @XmlElement(name = "BuyGeneralLedgerGid")
    protected GLogXMLGidType buyGeneralLedgerGid;
    @XmlElement(name = "SellGeneralLedgerGid")
    protected GLogXMLGidType sellGeneralLedgerGid;
    @XmlElement(name = "TotalWeightVolume")
    protected GLogXMLWeightVolumeType totalWeightVolume;
    @XmlElement(name = "TotalNetWeightVolume")
    protected WeightVolumeType totalNetWeightVolume;
    @XmlElement(name = "TotalPackagedItemSpecCount")
    protected String totalPackagedItemSpecCount;
    @XmlElement(name = "TotalPackagedItemCount")
    protected String totalPackagedItemCount;
    @XmlElement(name = "ReleaseRefnum")
    protected List<ReleaseRefnumType> releaseRefnum;
    @XmlElement(name = "ReleaseStatus")
    protected List<StatusType> releaseStatus;
    @XmlElement(name = "Remark")
    protected List<RemarkType> remark;
    @XmlElement(name = "REquipment")
    protected List<REquipmentType> rEquipment;
    @XmlElement(name = "InvolvedParty")
    protected List<InvolvedPartyType> involvedParty;
    @XmlElement(name = "CommercialInvoice")
    protected List<CommercialInvoiceType> commercialInvoice;
    @XmlElement(name = "Text")
    protected List<TextType> text;
    @XmlElement(name = "ReleaseService")
    protected List<ReleaseServiceType> releaseService;
    @XmlElement(name = "ReleaseShipmentInfo")
    protected ReleaseType.ReleaseShipmentInfo releaseShipmentInfo;
    @XmlElement(name = "ReleaseAllocationInfo")
    protected ReleaseType.ReleaseAllocationInfo releaseAllocationInfo;
    @XmlElement(name = "TransOrder")
    protected TransOrderType transOrder;
    @XmlElement(name = "OrStop")
    protected List<OrStopType> orStop;
    @XmlElement(name = "OrderMovement")
    protected List<OrderMovementType> orderMovement;
    @XmlElement(name = "AllocationGroupGid")
    protected GLogXMLGidType allocationGroupGid;

    /**
     * Obtém o valor da propriedade sendReason.
     * 
     * @return
     *     possible object is
     *     {@link SendReasonType }
     *     
     */
    public SendReasonType getSendReason() {
        return sendReason;
    }

    /**
     * Define o valor da propriedade sendReason.
     * 
     * @param value
     *     allowed object is
     *     {@link SendReasonType }
     *     
     */
    public void setSendReason(SendReasonType value) {
        this.sendReason = value;
    }

    /**
     * Obtém o valor da propriedade intSavedQuery.
     * 
     * @return
     *     possible object is
     *     {@link IntSavedQueryType }
     *     
     */
    public IntSavedQueryType getIntSavedQuery() {
        return intSavedQuery;
    }

    /**
     * Define o valor da propriedade intSavedQuery.
     * 
     * @param value
     *     allowed object is
     *     {@link IntSavedQueryType }
     *     
     */
    public void setIntSavedQuery(IntSavedQueryType value) {
        this.intSavedQuery = value;
    }

    /**
     * Obtém o valor da propriedade releaseGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getReleaseGid() {
        return releaseGid;
    }

    /**
     * Define o valor da propriedade releaseGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setReleaseGid(GLogXMLGidType value) {
        this.releaseGid = value;
    }

    /**
     * Obtém o valor da propriedade transactionCode.
     * 
     * @return
     *     possible object is
     *     {@link TransactionCodeType }
     *     
     */
    public TransactionCodeType getTransactionCode() {
        return transactionCode;
    }

    /**
     * Define o valor da propriedade transactionCode.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionCodeType }
     *     
     */
    public void setTransactionCode(TransactionCodeType value) {
        this.transactionCode = value;
    }

    /**
     * Obtém o valor da propriedade replaceChildren.
     * 
     * @return
     *     possible object is
     *     {@link ReplaceChildrenType }
     *     
     */
    public ReplaceChildrenType getReplaceChildren() {
        return replaceChildren;
    }

    /**
     * Define o valor da propriedade replaceChildren.
     * 
     * @param value
     *     allowed object is
     *     {@link ReplaceChildrenType }
     *     
     */
    public void setReplaceChildren(ReplaceChildrenType value) {
        this.replaceChildren = value;
    }

    /**
     * Gets the value of the intCommand property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the intCommand property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getIntCommand().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link IntCommandType }
     * 
     * 
     */
    public List<IntCommandType> getIntCommand() {
        if (intCommand == null) {
            intCommand = new ArrayList<IntCommandType>();
        }
        return this.intCommand;
    }

    /**
     * Obtém o valor da propriedade transOrderGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getTransOrderGid() {
        return transOrderGid;
    }

    /**
     * Define o valor da propriedade transOrderGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setTransOrderGid(GLogXMLGidType value) {
        this.transOrderGid = value;
    }

    /**
     * Obtém o valor da propriedade releaseHeader.
     * 
     * @return
     *     possible object is
     *     {@link ReleaseHeaderType }
     *     
     */
    public ReleaseHeaderType getReleaseHeader() {
        return releaseHeader;
    }

    /**
     * Define o valor da propriedade releaseHeader.
     * 
     * @param value
     *     allowed object is
     *     {@link ReleaseHeaderType }
     *     
     */
    public void setReleaseHeader(ReleaseHeaderType value) {
        this.releaseHeader = value;
    }

    /**
     * Obtém o valor da propriedade shipFromLocationRef.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public GLogXMLLocRefType getShipFromLocationRef() {
        return shipFromLocationRef;
    }

    /**
     * Define o valor da propriedade shipFromLocationRef.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public void setShipFromLocationRef(GLogXMLLocRefType value) {
        this.shipFromLocationRef = value;
    }

    /**
     * Obtém o valor da propriedade shipFromLocOverrideRef.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocOverrideRefType }
     *     
     */
    public GLogXMLLocOverrideRefType getShipFromLocOverrideRef() {
        return shipFromLocOverrideRef;
    }

    /**
     * Define o valor da propriedade shipFromLocOverrideRef.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocOverrideRefType }
     *     
     */
    public void setShipFromLocOverrideRef(GLogXMLLocOverrideRefType value) {
        this.shipFromLocOverrideRef = value;
    }

    /**
     * Obtém o valor da propriedade shipFromLoadPoint.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipFromLoadPoint() {
        return shipFromLoadPoint;
    }

    /**
     * Define o valor da propriedade shipFromLoadPoint.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipFromLoadPoint(String value) {
        this.shipFromLoadPoint = value;
    }

    /**
     * Obtém o valor da propriedade shipToLocationRef.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public GLogXMLLocRefType getShipToLocationRef() {
        return shipToLocationRef;
    }

    /**
     * Define o valor da propriedade shipToLocationRef.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public void setShipToLocationRef(GLogXMLLocRefType value) {
        this.shipToLocationRef = value;
    }

    /**
     * Obtém o valor da propriedade shipToLocOverrideRef.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocOverrideRefType }
     *     
     */
    public GLogXMLLocOverrideRefType getShipToLocOverrideRef() {
        return shipToLocOverrideRef;
    }

    /**
     * Define o valor da propriedade shipToLocOverrideRef.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocOverrideRefType }
     *     
     */
    public void setShipToLocOverrideRef(GLogXMLLocOverrideRefType value) {
        this.shipToLocOverrideRef = value;
    }

    /**
     * Obtém o valor da propriedade shipToUnloadPoint.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipToUnloadPoint() {
        return shipToUnloadPoint;
    }

    /**
     * Define o valor da propriedade shipToUnloadPoint.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipToUnloadPoint(String value) {
        this.shipToUnloadPoint = value;
    }

    /**
     * Obtém o valor da propriedade timeWindow.
     * 
     * @return
     *     possible object is
     *     {@link TimeWindowType }
     *     
     */
    public TimeWindowType getTimeWindow() {
        return timeWindow;
    }

    /**
     * Define o valor da propriedade timeWindow.
     * 
     * @param value
     *     allowed object is
     *     {@link TimeWindowType }
     *     
     */
    public void setTimeWindow(TimeWindowType value) {
        this.timeWindow = value;
    }

    /**
     * Obtém o valor da propriedade declaredValue.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getDeclaredValue() {
        return declaredValue;
    }

    /**
     * Define o valor da propriedade declaredValue.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setDeclaredValue(GLogXMLFinancialAmountType value) {
        this.declaredValue = value;
    }

    /**
     * Gets the value of the releaseLine property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the releaseLine property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReleaseLine().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ReleaseLineType }
     * 
     * 
     */
    public List<ReleaseLineType> getReleaseLine() {
        if (releaseLine == null) {
            releaseLine = new ArrayList<ReleaseLineType>();
        }
        return this.releaseLine;
    }

    /**
     * Gets the value of the shipUnit property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the shipUnit property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getShipUnit().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ShipUnitType }
     * 
     * 
     */
    public List<ShipUnitType> getShipUnit() {
        if (shipUnit == null) {
            shipUnit = new ArrayList<ShipUnitType>();
        }
        return this.shipUnit;
    }

    /**
     * Obtém o valor da propriedade releaseTypeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getReleaseTypeGid() {
        return releaseTypeGid;
    }

    /**
     * Define o valor da propriedade releaseTypeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setReleaseTypeGid(GLogXMLGidType value) {
        this.releaseTypeGid = value;
    }

    /**
     * Obtém o valor da propriedade planFromLocationGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocGidType }
     *     
     */
    public GLogXMLLocGidType getPlanFromLocationGid() {
        return planFromLocationGid;
    }

    /**
     * Define o valor da propriedade planFromLocationGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocGidType }
     *     
     */
    public void setPlanFromLocationGid(GLogXMLLocGidType value) {
        this.planFromLocationGid = value;
    }

    /**
     * Obtém o valor da propriedade planFromLocOverrideRef.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocOverrideRefType }
     *     
     */
    public GLogXMLLocOverrideRefType getPlanFromLocOverrideRef() {
        return planFromLocOverrideRef;
    }

    /**
     * Define o valor da propriedade planFromLocOverrideRef.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocOverrideRefType }
     *     
     */
    public void setPlanFromLocOverrideRef(GLogXMLLocOverrideRefType value) {
        this.planFromLocOverrideRef = value;
    }

    /**
     * Obtém o valor da propriedade planFromLoadPoint.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPlanFromLoadPoint() {
        return planFromLoadPoint;
    }

    /**
     * Define o valor da propriedade planFromLoadPoint.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPlanFromLoadPoint(String value) {
        this.planFromLoadPoint = value;
    }

    /**
     * Obtém o valor da propriedade planToLocationGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocGidType }
     *     
     */
    public GLogXMLLocGidType getPlanToLocationGid() {
        return planToLocationGid;
    }

    /**
     * Define o valor da propriedade planToLocationGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocGidType }
     *     
     */
    public void setPlanToLocationGid(GLogXMLLocGidType value) {
        this.planToLocationGid = value;
    }

    /**
     * Obtém o valor da propriedade planToLocOverrideRef.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocOverrideRefType }
     *     
     */
    public GLogXMLLocOverrideRefType getPlanToLocOverrideRef() {
        return planToLocOverrideRef;
    }

    /**
     * Define o valor da propriedade planToLocOverrideRef.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocOverrideRefType }
     *     
     */
    public void setPlanToLocOverrideRef(GLogXMLLocOverrideRefType value) {
        this.planToLocOverrideRef = value;
    }

    /**
     * Obtém o valor da propriedade planToUnloadPoint.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPlanToUnloadPoint() {
        return planToUnloadPoint;
    }

    /**
     * Define o valor da propriedade planToUnloadPoint.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPlanToUnloadPoint(String value) {
        this.planToUnloadPoint = value;
    }

    /**
     * Obtém o valor da propriedade priLegSourceLocationRef.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public GLogXMLLocRefType getPriLegSourceLocationRef() {
        return priLegSourceLocationRef;
    }

    /**
     * Define o valor da propriedade priLegSourceLocationRef.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public void setPriLegSourceLocationRef(GLogXMLLocRefType value) {
        this.priLegSourceLocationRef = value;
    }

    /**
     * Obtém o valor da propriedade priLegSourceLocOverrideRef.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocOverrideRefType }
     *     
     */
    public GLogXMLLocOverrideRefType getPriLegSourceLocOverrideRef() {
        return priLegSourceLocOverrideRef;
    }

    /**
     * Define o valor da propriedade priLegSourceLocOverrideRef.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocOverrideRefType }
     *     
     */
    public void setPriLegSourceLocOverrideRef(GLogXMLLocOverrideRefType value) {
        this.priLegSourceLocOverrideRef = value;
    }

    /**
     * Obtém o valor da propriedade portOfLoadLocationRef.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public GLogXMLLocRefType getPortOfLoadLocationRef() {
        return portOfLoadLocationRef;
    }

    /**
     * Define o valor da propriedade portOfLoadLocationRef.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public void setPortOfLoadLocationRef(GLogXMLLocRefType value) {
        this.portOfLoadLocationRef = value;
    }

    /**
     * Obtém o valor da propriedade portOfLoadLocOverrideRef.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocOverrideRefType }
     *     
     */
    public GLogXMLLocOverrideRefType getPortOfLoadLocOverrideRef() {
        return portOfLoadLocOverrideRef;
    }

    /**
     * Define o valor da propriedade portOfLoadLocOverrideRef.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocOverrideRefType }
     *     
     */
    public void setPortOfLoadLocOverrideRef(GLogXMLLocOverrideRefType value) {
        this.portOfLoadLocOverrideRef = value;
    }

    /**
     * Obtém o valor da propriedade portOfDisLocationRef.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public GLogXMLLocRefType getPortOfDisLocationRef() {
        return portOfDisLocationRef;
    }

    /**
     * Define o valor da propriedade portOfDisLocationRef.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public void setPortOfDisLocationRef(GLogXMLLocRefType value) {
        this.portOfDisLocationRef = value;
    }

    /**
     * Obtém o valor da propriedade portOfDisLocOverrideRef.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocOverrideRefType }
     *     
     */
    public GLogXMLLocOverrideRefType getPortOfDisLocOverrideRef() {
        return portOfDisLocOverrideRef;
    }

    /**
     * Define o valor da propriedade portOfDisLocOverrideRef.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocOverrideRefType }
     *     
     */
    public void setPortOfDisLocOverrideRef(GLogXMLLocOverrideRefType value) {
        this.portOfDisLocOverrideRef = value;
    }

    /**
     * Obtém o valor da propriedade priLegDestLocationRef.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public GLogXMLLocRefType getPriLegDestLocationRef() {
        return priLegDestLocationRef;
    }

    /**
     * Define o valor da propriedade priLegDestLocationRef.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public void setPriLegDestLocationRef(GLogXMLLocRefType value) {
        this.priLegDestLocationRef = value;
    }

    /**
     * Obtém o valor da propriedade priLegDestLocOverrideRef.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocOverrideRefType }
     *     
     */
    public GLogXMLLocOverrideRefType getPriLegDestLocOverrideRef() {
        return priLegDestLocOverrideRef;
    }

    /**
     * Define o valor da propriedade priLegDestLocOverrideRef.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocOverrideRefType }
     *     
     */
    public void setPriLegDestLocOverrideRef(GLogXMLLocOverrideRefType value) {
        this.priLegDestLocOverrideRef = value;
    }

    /**
     * Obtém o valor da propriedade bulkPlanGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getBulkPlanGid() {
        return bulkPlanGid;
    }

    /**
     * Define o valor da propriedade bulkPlanGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setBulkPlanGid(GLogXMLGidType value) {
        this.bulkPlanGid = value;
    }

    /**
     * Obtém o valor da propriedade sellBulkPlanGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getSellBulkPlanGid() {
        return sellBulkPlanGid;
    }

    /**
     * Define o valor da propriedade sellBulkPlanGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setSellBulkPlanGid(GLogXMLGidType value) {
        this.sellBulkPlanGid = value;
    }

    /**
     * Obtém o valor da propriedade planPartitionGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getPlanPartitionGid() {
        return planPartitionGid;
    }

    /**
     * Define o valor da propriedade planPartitionGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setPlanPartitionGid(GLogXMLGidType value) {
        this.planPartitionGid = value;
    }

    /**
     * Obtém o valor da propriedade bestDirectBuyCost.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getBestDirectBuyCost() {
        return bestDirectBuyCost;
    }

    /**
     * Define o valor da propriedade bestDirectBuyCost.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setBestDirectBuyCost(GLogXMLFinancialAmountType value) {
        this.bestDirectBuyCost = value;
    }

    /**
     * Obtém o valor da propriedade bestDirectBuyRateOfferingGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getBestDirectBuyRateOfferingGid() {
        return bestDirectBuyRateOfferingGid;
    }

    /**
     * Define o valor da propriedade bestDirectBuyRateOfferingGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setBestDirectBuyRateOfferingGid(GLogXMLGidType value) {
        this.bestDirectBuyRateOfferingGid = value;
    }

    /**
     * Obtém o valor da propriedade bestDirectSellCost.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getBestDirectSellCost() {
        return bestDirectSellCost;
    }

    /**
     * Define o valor da propriedade bestDirectSellCost.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setBestDirectSellCost(GLogXMLFinancialAmountType value) {
        this.bestDirectSellCost = value;
    }

    /**
     * Obtém o valor da propriedade bestDirectSellRateOfferingGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getBestDirectSellRateOfferingGid() {
        return bestDirectSellRateOfferingGid;
    }

    /**
     * Define o valor da propriedade bestDirectSellRateOfferingGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setBestDirectSellRateOfferingGid(GLogXMLGidType value) {
        this.bestDirectSellRateOfferingGid = value;
    }

    /**
     * Obtém o valor da propriedade buyGeneralLedgerGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getBuyGeneralLedgerGid() {
        return buyGeneralLedgerGid;
    }

    /**
     * Define o valor da propriedade buyGeneralLedgerGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setBuyGeneralLedgerGid(GLogXMLGidType value) {
        this.buyGeneralLedgerGid = value;
    }

    /**
     * Obtém o valor da propriedade sellGeneralLedgerGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getSellGeneralLedgerGid() {
        return sellGeneralLedgerGid;
    }

    /**
     * Define o valor da propriedade sellGeneralLedgerGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setSellGeneralLedgerGid(GLogXMLGidType value) {
        this.sellGeneralLedgerGid = value;
    }

    /**
     * Obtém o valor da propriedade totalWeightVolume.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLWeightVolumeType }
     *     
     */
    public GLogXMLWeightVolumeType getTotalWeightVolume() {
        return totalWeightVolume;
    }

    /**
     * Define o valor da propriedade totalWeightVolume.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLWeightVolumeType }
     *     
     */
    public void setTotalWeightVolume(GLogXMLWeightVolumeType value) {
        this.totalWeightVolume = value;
    }

    /**
     * Obtém o valor da propriedade totalNetWeightVolume.
     * 
     * @return
     *     possible object is
     *     {@link WeightVolumeType }
     *     
     */
    public WeightVolumeType getTotalNetWeightVolume() {
        return totalNetWeightVolume;
    }

    /**
     * Define o valor da propriedade totalNetWeightVolume.
     * 
     * @param value
     *     allowed object is
     *     {@link WeightVolumeType }
     *     
     */
    public void setTotalNetWeightVolume(WeightVolumeType value) {
        this.totalNetWeightVolume = value;
    }

    /**
     * Obtém o valor da propriedade totalPackagedItemSpecCount.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTotalPackagedItemSpecCount() {
        return totalPackagedItemSpecCount;
    }

    /**
     * Define o valor da propriedade totalPackagedItemSpecCount.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTotalPackagedItemSpecCount(String value) {
        this.totalPackagedItemSpecCount = value;
    }

    /**
     * Obtém o valor da propriedade totalPackagedItemCount.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTotalPackagedItemCount() {
        return totalPackagedItemCount;
    }

    /**
     * Define o valor da propriedade totalPackagedItemCount.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTotalPackagedItemCount(String value) {
        this.totalPackagedItemCount = value;
    }

    /**
     * Gets the value of the releaseRefnum property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the releaseRefnum property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReleaseRefnum().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ReleaseRefnumType }
     * 
     * 
     */
    public List<ReleaseRefnumType> getReleaseRefnum() {
        if (releaseRefnum == null) {
            releaseRefnum = new ArrayList<ReleaseRefnumType>();
        }
        return this.releaseRefnum;
    }

    /**
     * Gets the value of the releaseStatus property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the releaseStatus property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReleaseStatus().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link StatusType }
     * 
     * 
     */
    public List<StatusType> getReleaseStatus() {
        if (releaseStatus == null) {
            releaseStatus = new ArrayList<StatusType>();
        }
        return this.releaseStatus;
    }

    /**
     * Gets the value of the remark property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the remark property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRemark().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RemarkType }
     * 
     * 
     */
    public List<RemarkType> getRemark() {
        if (remark == null) {
            remark = new ArrayList<RemarkType>();
        }
        return this.remark;
    }

    /**
     * Gets the value of the rEquipment property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the rEquipment property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getREquipment().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link REquipmentType }
     * 
     * 
     */
    public List<REquipmentType> getREquipment() {
        if (rEquipment == null) {
            rEquipment = new ArrayList<REquipmentType>();
        }
        return this.rEquipment;
    }

    /**
     * Gets the value of the involvedParty property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the involvedParty property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInvolvedParty().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link InvolvedPartyType }
     * 
     * 
     */
    public List<InvolvedPartyType> getInvolvedParty() {
        if (involvedParty == null) {
            involvedParty = new ArrayList<InvolvedPartyType>();
        }
        return this.involvedParty;
    }

    /**
     * Gets the value of the commercialInvoice property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the commercialInvoice property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCommercialInvoice().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CommercialInvoiceType }
     * 
     * 
     */
    public List<CommercialInvoiceType> getCommercialInvoice() {
        if (commercialInvoice == null) {
            commercialInvoice = new ArrayList<CommercialInvoiceType>();
        }
        return this.commercialInvoice;
    }

    /**
     * Gets the value of the text property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the text property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getText().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TextType }
     * 
     * 
     */
    public List<TextType> getText() {
        if (text == null) {
            text = new ArrayList<TextType>();
        }
        return this.text;
    }

    /**
     * Gets the value of the releaseService property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the releaseService property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReleaseService().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ReleaseServiceType }
     * 
     * 
     */
    public List<ReleaseServiceType> getReleaseService() {
        if (releaseService == null) {
            releaseService = new ArrayList<ReleaseServiceType>();
        }
        return this.releaseService;
    }

    /**
     * Obtém o valor da propriedade releaseShipmentInfo.
     * 
     * @return
     *     possible object is
     *     {@link ReleaseType.ReleaseShipmentInfo }
     *     
     */
    public ReleaseType.ReleaseShipmentInfo getReleaseShipmentInfo() {
        return releaseShipmentInfo;
    }

    /**
     * Define o valor da propriedade releaseShipmentInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link ReleaseType.ReleaseShipmentInfo }
     *     
     */
    public void setReleaseShipmentInfo(ReleaseType.ReleaseShipmentInfo value) {
        this.releaseShipmentInfo = value;
    }

    /**
     * Obtém o valor da propriedade releaseAllocationInfo.
     * 
     * @return
     *     possible object is
     *     {@link ReleaseType.ReleaseAllocationInfo }
     *     
     */
    public ReleaseType.ReleaseAllocationInfo getReleaseAllocationInfo() {
        return releaseAllocationInfo;
    }

    /**
     * Define o valor da propriedade releaseAllocationInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link ReleaseType.ReleaseAllocationInfo }
     *     
     */
    public void setReleaseAllocationInfo(ReleaseType.ReleaseAllocationInfo value) {
        this.releaseAllocationInfo = value;
    }

    /**
     * Obtém o valor da propriedade transOrder.
     * 
     * @return
     *     possible object is
     *     {@link TransOrderType }
     *     
     */
    public TransOrderType getTransOrder() {
        return transOrder;
    }

    /**
     * Define o valor da propriedade transOrder.
     * 
     * @param value
     *     allowed object is
     *     {@link TransOrderType }
     *     
     */
    public void setTransOrder(TransOrderType value) {
        this.transOrder = value;
    }

    /**
     * Gets the value of the orStop property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the orStop property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOrStop().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OrStopType }
     * 
     * 
     */
    public List<OrStopType> getOrStop() {
        if (orStop == null) {
            orStop = new ArrayList<OrStopType>();
        }
        return this.orStop;
    }

    /**
     * Gets the value of the orderMovement property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the orderMovement property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOrderMovement().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OrderMovementType }
     * 
     * 
     */
    public List<OrderMovementType> getOrderMovement() {
        if (orderMovement == null) {
            orderMovement = new ArrayList<OrderMovementType>();
        }
        return this.orderMovement;
    }

    /**
     * Obtém o valor da propriedade allocationGroupGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getAllocationGroupGid() {
        return allocationGroupGid;
    }

    /**
     * Define o valor da propriedade allocationGroupGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setAllocationGroupGid(GLogXMLGidType value) {
        this.allocationGroupGid = value;
    }


    /**
     * <p>Classe Java de anonymous complex type.
     * 
     * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="ReleaseAllocByType" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ReleaseAllocType" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "releaseAllocByType"
    })
    public static class ReleaseAllocationInfo {

        @XmlElement(name = "ReleaseAllocByType")
        protected List<ReleaseAllocType> releaseAllocByType;

        /**
         * Gets the value of the releaseAllocByType property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the releaseAllocByType property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getReleaseAllocByType().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link ReleaseAllocType }
         * 
         * 
         */
        public List<ReleaseAllocType> getReleaseAllocByType() {
            if (releaseAllocByType == null) {
                releaseAllocByType = new ArrayList<ReleaseAllocType>();
            }
            return this.releaseAllocByType;
        }

    }


    /**
     * <p>Classe Java de anonymous complex type.
     * 
     * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="LatestEstDeliveryDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType"/>
     *         &lt;element name="EarliestEstPickupDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "latestEstDeliveryDt",
        "earliestEstPickupDt"
    })
    public static class ReleaseShipmentInfo {

        @XmlElement(name = "LatestEstDeliveryDt", required = true)
        protected GLogDateTimeType latestEstDeliveryDt;
        @XmlElement(name = "EarliestEstPickupDt", required = true)
        protected GLogDateTimeType earliestEstPickupDt;

        /**
         * Obtém o valor da propriedade latestEstDeliveryDt.
         * 
         * @return
         *     possible object is
         *     {@link GLogDateTimeType }
         *     
         */
        public GLogDateTimeType getLatestEstDeliveryDt() {
            return latestEstDeliveryDt;
        }

        /**
         * Define o valor da propriedade latestEstDeliveryDt.
         * 
         * @param value
         *     allowed object is
         *     {@link GLogDateTimeType }
         *     
         */
        public void setLatestEstDeliveryDt(GLogDateTimeType value) {
            this.latestEstDeliveryDt = value;
        }

        /**
         * Obtém o valor da propriedade earliestEstPickupDt.
         * 
         * @return
         *     possible object is
         *     {@link GLogDateTimeType }
         *     
         */
        public GLogDateTimeType getEarliestEstPickupDt() {
            return earliestEstPickupDt;
        }

        /**
         * Define o valor da propriedade earliestEstPickupDt.
         * 
         * @param value
         *     allowed object is
         *     {@link GLogDateTimeType }
         *     
         */
        public void setEarliestEstPickupDt(GLogDateTimeType value) {
            this.earliestEstPickupDt = value;
        }

    }

}
