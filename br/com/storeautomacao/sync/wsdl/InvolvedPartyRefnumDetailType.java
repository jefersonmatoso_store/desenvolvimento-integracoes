
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * (Inbound) Reference numbers for the party involved
 *          
 * 
 * <p>Classe Java de InvolvedPartyRefnumDetailType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="InvolvedPartyRefnumDetailType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PartyQualifierGidForRefnum" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="PartyRefnumQualifierGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="PartyRefnumValue" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InvolvedPartyRefnumDetailType", namespace = "http://xmlns.oracle.com/apps/gtm/transmission/v6.4", propOrder = {
    "partyQualifierGidForRefnum",
    "partyRefnumQualifierGid",
    "partyRefnumValue"
})
public class InvolvedPartyRefnumDetailType {

    @XmlElement(name = "PartyQualifierGidForRefnum", required = true)
    protected GLogXMLGidType partyQualifierGidForRefnum;
    @XmlElement(name = "PartyRefnumQualifierGid", required = true)
    protected GLogXMLGidType partyRefnumQualifierGid;
    @XmlElement(name = "PartyRefnumValue", required = true)
    protected String partyRefnumValue;

    /**
     * Obtém o valor da propriedade partyQualifierGidForRefnum.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getPartyQualifierGidForRefnum() {
        return partyQualifierGidForRefnum;
    }

    /**
     * Define o valor da propriedade partyQualifierGidForRefnum.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setPartyQualifierGidForRefnum(GLogXMLGidType value) {
        this.partyQualifierGidForRefnum = value;
    }

    /**
     * Obtém o valor da propriedade partyRefnumQualifierGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getPartyRefnumQualifierGid() {
        return partyRefnumQualifierGid;
    }

    /**
     * Define o valor da propriedade partyRefnumQualifierGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setPartyRefnumQualifierGid(GLogXMLGidType value) {
        this.partyRefnumQualifierGid = value;
    }

    /**
     * Obtém o valor da propriedade partyRefnumValue.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPartyRefnumValue() {
        return partyRefnumValue;
    }

    /**
     * Define o valor da propriedade partyRefnumValue.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPartyRefnumValue(String value) {
        this.partyRefnumValue = value;
    }

}
