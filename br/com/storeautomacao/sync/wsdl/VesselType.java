
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * Specifies vessel information. The CountryCode3Gid element corresponds to the Vessel Country Code.
 *             The LocationRef element corresponds to the vessel port of registry.
 *          
 * 
 * <p>Classe Java de VesselType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="VesselType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="VesselGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="VesselName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Owner" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Operator" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LocationRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LocationRefType" minOccurs="0"/>
 *         &lt;element name="CountryCode3Gid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="OfficialNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CallSign" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IMONumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ClassSocietyNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="YearBuilt" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsInService" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Refnum" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RefnumType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VesselType", propOrder = {
    "vesselGid",
    "vesselName",
    "owner",
    "operator",
    "locationRef",
    "countryCode3Gid",
    "officialNumber",
    "callSign",
    "imoNumber",
    "classSocietyNumber",
    "yearBuilt",
    "isInService",
    "refnum"
})
public class VesselType {

    @XmlElement(name = "VesselGid", required = true)
    protected GLogXMLGidType vesselGid;
    @XmlElement(name = "VesselName", required = true)
    protected String vesselName;
    @XmlElement(name = "Owner")
    protected String owner;
    @XmlElement(name = "Operator")
    protected String operator;
    @XmlElement(name = "LocationRef")
    protected LocationRefType locationRef;
    @XmlElement(name = "CountryCode3Gid")
    protected GLogXMLGidType countryCode3Gid;
    @XmlElement(name = "OfficialNumber")
    protected String officialNumber;
    @XmlElement(name = "CallSign")
    protected String callSign;
    @XmlElement(name = "IMONumber")
    protected String imoNumber;
    @XmlElement(name = "ClassSocietyNumber")
    protected String classSocietyNumber;
    @XmlElement(name = "YearBuilt")
    protected String yearBuilt;
    @XmlElement(name = "IsInService", required = true)
    protected String isInService;
    @XmlElement(name = "Refnum")
    protected List<RefnumType> refnum;

    /**
     * Obtém o valor da propriedade vesselGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getVesselGid() {
        return vesselGid;
    }

    /**
     * Define o valor da propriedade vesselGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setVesselGid(GLogXMLGidType value) {
        this.vesselGid = value;
    }

    /**
     * Obtém o valor da propriedade vesselName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVesselName() {
        return vesselName;
    }

    /**
     * Define o valor da propriedade vesselName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVesselName(String value) {
        this.vesselName = value;
    }

    /**
     * Obtém o valor da propriedade owner.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOwner() {
        return owner;
    }

    /**
     * Define o valor da propriedade owner.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOwner(String value) {
        this.owner = value;
    }

    /**
     * Obtém o valor da propriedade operator.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOperator() {
        return operator;
    }

    /**
     * Define o valor da propriedade operator.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOperator(String value) {
        this.operator = value;
    }

    /**
     * Obtém o valor da propriedade locationRef.
     * 
     * @return
     *     possible object is
     *     {@link LocationRefType }
     *     
     */
    public LocationRefType getLocationRef() {
        return locationRef;
    }

    /**
     * Define o valor da propriedade locationRef.
     * 
     * @param value
     *     allowed object is
     *     {@link LocationRefType }
     *     
     */
    public void setLocationRef(LocationRefType value) {
        this.locationRef = value;
    }

    /**
     * Obtém o valor da propriedade countryCode3Gid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getCountryCode3Gid() {
        return countryCode3Gid;
    }

    /**
     * Define o valor da propriedade countryCode3Gid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setCountryCode3Gid(GLogXMLGidType value) {
        this.countryCode3Gid = value;
    }

    /**
     * Obtém o valor da propriedade officialNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOfficialNumber() {
        return officialNumber;
    }

    /**
     * Define o valor da propriedade officialNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOfficialNumber(String value) {
        this.officialNumber = value;
    }

    /**
     * Obtém o valor da propriedade callSign.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCallSign() {
        return callSign;
    }

    /**
     * Define o valor da propriedade callSign.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCallSign(String value) {
        this.callSign = value;
    }

    /**
     * Obtém o valor da propriedade imoNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIMONumber() {
        return imoNumber;
    }

    /**
     * Define o valor da propriedade imoNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIMONumber(String value) {
        this.imoNumber = value;
    }

    /**
     * Obtém o valor da propriedade classSocietyNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClassSocietyNumber() {
        return classSocietyNumber;
    }

    /**
     * Define o valor da propriedade classSocietyNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClassSocietyNumber(String value) {
        this.classSocietyNumber = value;
    }

    /**
     * Obtém o valor da propriedade yearBuilt.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getYearBuilt() {
        return yearBuilt;
    }

    /**
     * Define o valor da propriedade yearBuilt.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setYearBuilt(String value) {
        this.yearBuilt = value;
    }

    /**
     * Obtém o valor da propriedade isInService.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsInService() {
        return isInService;
    }

    /**
     * Define o valor da propriedade isInService.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsInService(String value) {
        this.isInService = value;
    }

    /**
     * Gets the value of the refnum property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the refnum property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRefnum().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RefnumType }
     * 
     * 
     */
    public List<RefnumType> getRefnum() {
        if (refnum == null) {
            refnum = new ArrayList<RefnumType>();
        }
        return this.refnum;
    }

}
