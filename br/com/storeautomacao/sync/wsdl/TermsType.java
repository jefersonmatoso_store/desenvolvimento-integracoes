
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 *             Specifies the inco terms and discount offered.
 *          
 * 
 * <p>Classe Java de TermsType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="TermsType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IncoTermGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="Discount" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}DiscountType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TermsType", propOrder = {
    "incoTermGid",
    "discount"
})
public class TermsType {

    @XmlElement(name = "IncoTermGid", required = true)
    protected GLogXMLGidType incoTermGid;
    @XmlElement(name = "Discount")
    protected DiscountType discount;

    /**
     * Obtém o valor da propriedade incoTermGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getIncoTermGid() {
        return incoTermGid;
    }

    /**
     * Define o valor da propriedade incoTermGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setIncoTermGid(GLogXMLGidType value) {
        this.incoTermGid = value;
    }

    /**
     * Obtém o valor da propriedade discount.
     * 
     * @return
     *     possible object is
     *     {@link DiscountType }
     *     
     */
    public DiscountType getDiscount() {
        return discount;
    }

    /**
     * Define o valor da propriedade discount.
     * 
     * @param value
     *     allowed object is
     *     {@link DiscountType }
     *     
     */
    public void setDiscount(DiscountType value) {
        this.discount = value;
    }

}
