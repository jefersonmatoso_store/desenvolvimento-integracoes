
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * If the text was retrieved from an item-level template, this represents item context
 *             information needed by downstream systems.
 *          
 * 
 * <p>Classe Java de TextItemType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="TextItemType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TextItemContextType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="TextItemContextGid" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="TextItemContextSeq" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TextItemType", propOrder = {
    "textItemContextType",
    "textItemContextGid",
    "textItemContextSeq"
})
public class TextItemType {

    @XmlElement(name = "TextItemContextType", required = true)
    protected String textItemContextType;
    @XmlElement(name = "TextItemContextGid", required = true)
    protected String textItemContextGid;
    @XmlElement(name = "TextItemContextSeq")
    protected String textItemContextSeq;

    /**
     * Obtém o valor da propriedade textItemContextType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTextItemContextType() {
        return textItemContextType;
    }

    /**
     * Define o valor da propriedade textItemContextType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTextItemContextType(String value) {
        this.textItemContextType = value;
    }

    /**
     * Obtém o valor da propriedade textItemContextGid.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTextItemContextGid() {
        return textItemContextGid;
    }

    /**
     * Define o valor da propriedade textItemContextGid.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTextItemContextGid(String value) {
        this.textItemContextGid = value;
    }

    /**
     * Obtém o valor da propriedade textItemContextSeq.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTextItemContextSeq() {
        return textItemContextSeq;
    }

    /**
     * Define o valor da propriedade textItemContextSeq.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTextItemContextSeq(String value) {
        this.textItemContextSeq = value;
    }

}
