
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * (Inbound) This is not used any more.
 * 
 * <p>Classe Java de ConditionType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="ConditionType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ConditionName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ConditionValue" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ConditionType", namespace = "http://xmlns.oracle.com/apps/gtm/transmission/v6.4", propOrder = {
    "conditionName",
    "conditionValue"
})
public class ConditionType {

    @XmlElement(name = "ConditionName", required = true)
    protected String conditionName;
    @XmlElement(name = "ConditionValue", required = true)
    protected String conditionValue;

    /**
     * Obtém o valor da propriedade conditionName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConditionName() {
        return conditionName;
    }

    /**
     * Define o valor da propriedade conditionName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConditionName(String value) {
        this.conditionName = value;
    }

    /**
     * Obtém o valor da propriedade conditionValue.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConditionValue() {
        return conditionValue;
    }

    /**
     * Define o valor da propriedade conditionValue.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConditionValue(String value) {
        this.conditionValue = value;
    }

}
