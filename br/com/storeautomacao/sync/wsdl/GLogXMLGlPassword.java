
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de GLogXMLGlPassword complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="GLogXMLGlPassword">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GlPassword" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GlPasswordType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GLogXMLGlPassword", propOrder = {
    "glPassword"
})
public class GLogXMLGlPassword {

    @XmlElement(name = "GlPassword", required = true)
    protected GlPasswordType glPassword;

    /**
     * Obtém o valor da propriedade glPassword.
     * 
     * @return
     *     possible object is
     *     {@link GlPasswordType }
     *     
     */
    public GlPasswordType getGlPassword() {
        return glPassword;
    }

    /**
     * Define o valor da propriedade glPassword.
     * 
     * @param value
     *     allowed object is
     *     {@link GlPasswordType }
     *     
     */
    public void setGlPassword(GlPasswordType value) {
        this.glPassword = value;
    }

}
