
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * EquipmentAttribute is a generic qualifier/value pair used to specify an attribute associated with a piece of equipment.
 * 
 * <p>Classe Java de EquipmentAttributeType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="EquipmentAttributeType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="EquipmentAttributeQualifierGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="EquipmentAttributeValue" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EquipmentAttributeType", propOrder = {
    "equipmentAttributeQualifierGid",
    "equipmentAttributeValue"
})
public class EquipmentAttributeType {

    @XmlElement(name = "EquipmentAttributeQualifierGid", required = true)
    protected GLogXMLGidType equipmentAttributeQualifierGid;
    @XmlElement(name = "EquipmentAttributeValue", required = true)
    protected String equipmentAttributeValue;

    /**
     * Obtém o valor da propriedade equipmentAttributeQualifierGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getEquipmentAttributeQualifierGid() {
        return equipmentAttributeQualifierGid;
    }

    /**
     * Define o valor da propriedade equipmentAttributeQualifierGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setEquipmentAttributeQualifierGid(GLogXMLGidType value) {
        this.equipmentAttributeQualifierGid = value;
    }

    /**
     * Obtém o valor da propriedade equipmentAttributeValue.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEquipmentAttributeValue() {
        return equipmentAttributeValue;
    }

    /**
     * Define o valor da propriedade equipmentAttributeValue.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEquipmentAttributeValue(String value) {
        this.equipmentAttributeValue = value;
    }

}
