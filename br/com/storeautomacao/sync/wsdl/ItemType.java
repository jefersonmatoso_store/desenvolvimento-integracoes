
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;


/**
 * 
 *             Item is used to transmit item master data to GLog.
 *          
 * 
 * <p>Classe Java de ItemType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="ItemType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OTMTransactionInOut">
 *       &lt;sequence>
 *         &lt;element name="SendReason" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}SendReasonType" minOccurs="0"/>
 *         &lt;element name="TransactionCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransactionCodeType"/>
 *         &lt;element name="ReplaceChildren" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ReplaceChildrenType" minOccurs="0"/>
 *         &lt;element name="ItemGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="ItemName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EffectiveDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="ExpirationDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="CommodityGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="CommodityName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CommodityProtectiveService" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}CommodityProtectiveServiceType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="NMFCArticleGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="NMFCClassGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="STCCGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="HTSGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="SITCGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="UserDefinedCommodityGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="UDCClassListGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="PreviousItemGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="BrandName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ManufacturedCountryCode3Gid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="IsDrawback" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IATAScrCodeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="AccessorialCodeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="SpecialServiceGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Remark" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RemarkType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="ItemFeature" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ItemFeatureType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Refnum" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RefnumType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Text" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TextType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="UnitOfMeasure" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PricePerUnit" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}PricePerUnitType" minOccurs="0"/>
 *         &lt;element name="Item" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ItemType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="ChildItemCount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="GtmItemClassification" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GtmItemClassificationType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="GtmItemDescription" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GtmItemDescriptionType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="GtmItemCountryOfOrigin" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GtmItemCountryOfOriginType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="GtmItemUomConversion" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GtmItemUomConversionType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Priority" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FlexFieldStrings" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldStringType" minOccurs="0"/>
 *         &lt;element name="FlexFieldNumbers" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldNumberType" minOccurs="0"/>
 *         &lt;element name="FlexFieldDates" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldDateType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ItemType", propOrder = {
    "sendReason",
    "transactionCode",
    "replaceChildren",
    "itemGid",
    "itemName",
    "description",
    "effectiveDate",
    "expirationDate",
    "commodityGid",
    "commodityName",
    "commodityProtectiveService",
    "nmfcArticleGid",
    "nmfcClassGid",
    "stccGid",
    "htsGid",
    "sitcGid",
    "userDefinedCommodityGid",
    "udcClassListGid",
    "previousItemGid",
    "brandName",
    "manufacturedCountryCode3Gid",
    "isDrawback",
    "iataScrCodeGid",
    "accessorialCodeGid",
    "specialServiceGid",
    "remark",
    "itemFeature",
    "refnum",
    "text",
    "unitOfMeasure",
    "pricePerUnit",
    "item",
    "childItemCount",
    "gtmItemClassification",
    "gtmItemDescription",
    "gtmItemCountryOfOrigin",
    "gtmItemUomConversion",
    "priority",
    "flexFieldStrings",
    "flexFieldNumbers",
    "flexFieldDates"
})
public class ItemType
    extends OTMTransactionInOut
{

    @XmlElement(name = "SendReason")
    protected SendReasonType sendReason;
    @XmlElement(name = "TransactionCode", required = true)
    @XmlSchemaType(name = "string")
    protected TransactionCodeType transactionCode;
    @XmlElement(name = "ReplaceChildren")
    protected ReplaceChildrenType replaceChildren;
    @XmlElement(name = "ItemGid", required = true)
    protected GLogXMLGidType itemGid;
    @XmlElement(name = "ItemName")
    protected String itemName;
    @XmlElement(name = "Description")
    protected String description;
    @XmlElement(name = "EffectiveDate")
    protected GLogDateTimeType effectiveDate;
    @XmlElement(name = "ExpirationDate")
    protected GLogDateTimeType expirationDate;
    @XmlElement(name = "CommodityGid")
    protected GLogXMLGidType commodityGid;
    @XmlElement(name = "CommodityName")
    protected String commodityName;
    @XmlElement(name = "CommodityProtectiveService")
    protected List<CommodityProtectiveServiceType> commodityProtectiveService;
    @XmlElement(name = "NMFCArticleGid")
    protected GLogXMLGidType nmfcArticleGid;
    @XmlElement(name = "NMFCClassGid")
    protected GLogXMLGidType nmfcClassGid;
    @XmlElement(name = "STCCGid")
    protected GLogXMLGidType stccGid;
    @XmlElement(name = "HTSGid")
    protected GLogXMLGidType htsGid;
    @XmlElement(name = "SITCGid")
    protected GLogXMLGidType sitcGid;
    @XmlElement(name = "UserDefinedCommodityGid")
    protected GLogXMLGidType userDefinedCommodityGid;
    @XmlElement(name = "UDCClassListGid")
    protected GLogXMLGidType udcClassListGid;
    @XmlElement(name = "PreviousItemGid")
    protected GLogXMLGidType previousItemGid;
    @XmlElement(name = "BrandName")
    protected String brandName;
    @XmlElement(name = "ManufacturedCountryCode3Gid")
    protected GLogXMLGidType manufacturedCountryCode3Gid;
    @XmlElement(name = "IsDrawback")
    protected String isDrawback;
    @XmlElement(name = "IATAScrCodeGid")
    protected GLogXMLGidType iataScrCodeGid;
    @XmlElement(name = "AccessorialCodeGid")
    protected List<GLogXMLGidType> accessorialCodeGid;
    @XmlElement(name = "SpecialServiceGid")
    protected List<GLogXMLGidType> specialServiceGid;
    @XmlElement(name = "Remark")
    protected List<RemarkType> remark;
    @XmlElement(name = "ItemFeature")
    protected List<ItemFeatureType> itemFeature;
    @XmlElement(name = "Refnum")
    protected List<RefnumType> refnum;
    @XmlElement(name = "Text")
    protected List<TextType> text;
    @XmlElement(name = "UnitOfMeasure")
    protected String unitOfMeasure;
    @XmlElement(name = "PricePerUnit")
    protected PricePerUnitType pricePerUnit;
    @XmlElement(name = "Item")
    protected List<ItemType> item;
    @XmlElement(name = "ChildItemCount")
    protected String childItemCount;
    @XmlElement(name = "GtmItemClassification")
    protected List<GtmItemClassificationType> gtmItemClassification;
    @XmlElement(name = "GtmItemDescription")
    protected List<GtmItemDescriptionType> gtmItemDescription;
    @XmlElement(name = "GtmItemCountryOfOrigin")
    protected List<GtmItemCountryOfOriginType> gtmItemCountryOfOrigin;
    @XmlElement(name = "GtmItemUomConversion")
    protected List<GtmItemUomConversionType> gtmItemUomConversion;
    @XmlElement(name = "Priority")
    protected String priority;
    @XmlElement(name = "FlexFieldStrings")
    protected FlexFieldStringType flexFieldStrings;
    @XmlElement(name = "FlexFieldNumbers")
    protected FlexFieldNumberType flexFieldNumbers;
    @XmlElement(name = "FlexFieldDates")
    protected FlexFieldDateType flexFieldDates;

    /**
     * Obtém o valor da propriedade sendReason.
     * 
     * @return
     *     possible object is
     *     {@link SendReasonType }
     *     
     */
    public SendReasonType getSendReason() {
        return sendReason;
    }

    /**
     * Define o valor da propriedade sendReason.
     * 
     * @param value
     *     allowed object is
     *     {@link SendReasonType }
     *     
     */
    public void setSendReason(SendReasonType value) {
        this.sendReason = value;
    }

    /**
     * Obtém o valor da propriedade transactionCode.
     * 
     * @return
     *     possible object is
     *     {@link TransactionCodeType }
     *     
     */
    public TransactionCodeType getTransactionCode() {
        return transactionCode;
    }

    /**
     * Define o valor da propriedade transactionCode.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionCodeType }
     *     
     */
    public void setTransactionCode(TransactionCodeType value) {
        this.transactionCode = value;
    }

    /**
     * Obtém o valor da propriedade replaceChildren.
     * 
     * @return
     *     possible object is
     *     {@link ReplaceChildrenType }
     *     
     */
    public ReplaceChildrenType getReplaceChildren() {
        return replaceChildren;
    }

    /**
     * Define o valor da propriedade replaceChildren.
     * 
     * @param value
     *     allowed object is
     *     {@link ReplaceChildrenType }
     *     
     */
    public void setReplaceChildren(ReplaceChildrenType value) {
        this.replaceChildren = value;
    }

    /**
     * Obtém o valor da propriedade itemGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getItemGid() {
        return itemGid;
    }

    /**
     * Define o valor da propriedade itemGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setItemGid(GLogXMLGidType value) {
        this.itemGid = value;
    }

    /**
     * Obtém o valor da propriedade itemName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItemName() {
        return itemName;
    }

    /**
     * Define o valor da propriedade itemName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItemName(String value) {
        this.itemName = value;
    }

    /**
     * Obtém o valor da propriedade description.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Define o valor da propriedade description.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Obtém o valor da propriedade effectiveDate.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getEffectiveDate() {
        return effectiveDate;
    }

    /**
     * Define o valor da propriedade effectiveDate.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setEffectiveDate(GLogDateTimeType value) {
        this.effectiveDate = value;
    }

    /**
     * Obtém o valor da propriedade expirationDate.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getExpirationDate() {
        return expirationDate;
    }

    /**
     * Define o valor da propriedade expirationDate.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setExpirationDate(GLogDateTimeType value) {
        this.expirationDate = value;
    }

    /**
     * Obtém o valor da propriedade commodityGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getCommodityGid() {
        return commodityGid;
    }

    /**
     * Define o valor da propriedade commodityGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setCommodityGid(GLogXMLGidType value) {
        this.commodityGid = value;
    }

    /**
     * Obtém o valor da propriedade commodityName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCommodityName() {
        return commodityName;
    }

    /**
     * Define o valor da propriedade commodityName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCommodityName(String value) {
        this.commodityName = value;
    }

    /**
     * Gets the value of the commodityProtectiveService property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the commodityProtectiveService property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCommodityProtectiveService().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CommodityProtectiveServiceType }
     * 
     * 
     */
    public List<CommodityProtectiveServiceType> getCommodityProtectiveService() {
        if (commodityProtectiveService == null) {
            commodityProtectiveService = new ArrayList<CommodityProtectiveServiceType>();
        }
        return this.commodityProtectiveService;
    }

    /**
     * Obtém o valor da propriedade nmfcArticleGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getNMFCArticleGid() {
        return nmfcArticleGid;
    }

    /**
     * Define o valor da propriedade nmfcArticleGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setNMFCArticleGid(GLogXMLGidType value) {
        this.nmfcArticleGid = value;
    }

    /**
     * Obtém o valor da propriedade nmfcClassGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getNMFCClassGid() {
        return nmfcClassGid;
    }

    /**
     * Define o valor da propriedade nmfcClassGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setNMFCClassGid(GLogXMLGidType value) {
        this.nmfcClassGid = value;
    }

    /**
     * Obtém o valor da propriedade stccGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getSTCCGid() {
        return stccGid;
    }

    /**
     * Define o valor da propriedade stccGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setSTCCGid(GLogXMLGidType value) {
        this.stccGid = value;
    }

    /**
     * Obtém o valor da propriedade htsGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getHTSGid() {
        return htsGid;
    }

    /**
     * Define o valor da propriedade htsGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setHTSGid(GLogXMLGidType value) {
        this.htsGid = value;
    }

    /**
     * Obtém o valor da propriedade sitcGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getSITCGid() {
        return sitcGid;
    }

    /**
     * Define o valor da propriedade sitcGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setSITCGid(GLogXMLGidType value) {
        this.sitcGid = value;
    }

    /**
     * Obtém o valor da propriedade userDefinedCommodityGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getUserDefinedCommodityGid() {
        return userDefinedCommodityGid;
    }

    /**
     * Define o valor da propriedade userDefinedCommodityGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setUserDefinedCommodityGid(GLogXMLGidType value) {
        this.userDefinedCommodityGid = value;
    }

    /**
     * Obtém o valor da propriedade udcClassListGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getUDCClassListGid() {
        return udcClassListGid;
    }

    /**
     * Define o valor da propriedade udcClassListGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setUDCClassListGid(GLogXMLGidType value) {
        this.udcClassListGid = value;
    }

    /**
     * Obtém o valor da propriedade previousItemGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getPreviousItemGid() {
        return previousItemGid;
    }

    /**
     * Define o valor da propriedade previousItemGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setPreviousItemGid(GLogXMLGidType value) {
        this.previousItemGid = value;
    }

    /**
     * Obtém o valor da propriedade brandName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBrandName() {
        return brandName;
    }

    /**
     * Define o valor da propriedade brandName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBrandName(String value) {
        this.brandName = value;
    }

    /**
     * Obtém o valor da propriedade manufacturedCountryCode3Gid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getManufacturedCountryCode3Gid() {
        return manufacturedCountryCode3Gid;
    }

    /**
     * Define o valor da propriedade manufacturedCountryCode3Gid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setManufacturedCountryCode3Gid(GLogXMLGidType value) {
        this.manufacturedCountryCode3Gid = value;
    }

    /**
     * Obtém o valor da propriedade isDrawback.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsDrawback() {
        return isDrawback;
    }

    /**
     * Define o valor da propriedade isDrawback.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsDrawback(String value) {
        this.isDrawback = value;
    }

    /**
     * Obtém o valor da propriedade iataScrCodeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getIATAScrCodeGid() {
        return iataScrCodeGid;
    }

    /**
     * Define o valor da propriedade iataScrCodeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setIATAScrCodeGid(GLogXMLGidType value) {
        this.iataScrCodeGid = value;
    }

    /**
     * Gets the value of the accessorialCodeGid property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the accessorialCodeGid property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAccessorialCodeGid().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GLogXMLGidType }
     * 
     * 
     */
    public List<GLogXMLGidType> getAccessorialCodeGid() {
        if (accessorialCodeGid == null) {
            accessorialCodeGid = new ArrayList<GLogXMLGidType>();
        }
        return this.accessorialCodeGid;
    }

    /**
     * Gets the value of the specialServiceGid property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the specialServiceGid property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSpecialServiceGid().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GLogXMLGidType }
     * 
     * 
     */
    public List<GLogXMLGidType> getSpecialServiceGid() {
        if (specialServiceGid == null) {
            specialServiceGid = new ArrayList<GLogXMLGidType>();
        }
        return this.specialServiceGid;
    }

    /**
     * Gets the value of the remark property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the remark property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRemark().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RemarkType }
     * 
     * 
     */
    public List<RemarkType> getRemark() {
        if (remark == null) {
            remark = new ArrayList<RemarkType>();
        }
        return this.remark;
    }

    /**
     * Gets the value of the itemFeature property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the itemFeature property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getItemFeature().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ItemFeatureType }
     * 
     * 
     */
    public List<ItemFeatureType> getItemFeature() {
        if (itemFeature == null) {
            itemFeature = new ArrayList<ItemFeatureType>();
        }
        return this.itemFeature;
    }

    /**
     * Gets the value of the refnum property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the refnum property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRefnum().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RefnumType }
     * 
     * 
     */
    public List<RefnumType> getRefnum() {
        if (refnum == null) {
            refnum = new ArrayList<RefnumType>();
        }
        return this.refnum;
    }

    /**
     * Gets the value of the text property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the text property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getText().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TextType }
     * 
     * 
     */
    public List<TextType> getText() {
        if (text == null) {
            text = new ArrayList<TextType>();
        }
        return this.text;
    }

    /**
     * Obtém o valor da propriedade unitOfMeasure.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUnitOfMeasure() {
        return unitOfMeasure;
    }

    /**
     * Define o valor da propriedade unitOfMeasure.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUnitOfMeasure(String value) {
        this.unitOfMeasure = value;
    }

    /**
     * Obtém o valor da propriedade pricePerUnit.
     * 
     * @return
     *     possible object is
     *     {@link PricePerUnitType }
     *     
     */
    public PricePerUnitType getPricePerUnit() {
        return pricePerUnit;
    }

    /**
     * Define o valor da propriedade pricePerUnit.
     * 
     * @param value
     *     allowed object is
     *     {@link PricePerUnitType }
     *     
     */
    public void setPricePerUnit(PricePerUnitType value) {
        this.pricePerUnit = value;
    }

    /**
     * Gets the value of the item property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the item property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getItem().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ItemType }
     * 
     * 
     */
    public List<ItemType> getItem() {
        if (item == null) {
            item = new ArrayList<ItemType>();
        }
        return this.item;
    }

    /**
     * Obtém o valor da propriedade childItemCount.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChildItemCount() {
        return childItemCount;
    }

    /**
     * Define o valor da propriedade childItemCount.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChildItemCount(String value) {
        this.childItemCount = value;
    }

    /**
     * Gets the value of the gtmItemClassification property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the gtmItemClassification property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGtmItemClassification().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GtmItemClassificationType }
     * 
     * 
     */
    public List<GtmItemClassificationType> getGtmItemClassification() {
        if (gtmItemClassification == null) {
            gtmItemClassification = new ArrayList<GtmItemClassificationType>();
        }
        return this.gtmItemClassification;
    }

    /**
     * Gets the value of the gtmItemDescription property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the gtmItemDescription property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGtmItemDescription().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GtmItemDescriptionType }
     * 
     * 
     */
    public List<GtmItemDescriptionType> getGtmItemDescription() {
        if (gtmItemDescription == null) {
            gtmItemDescription = new ArrayList<GtmItemDescriptionType>();
        }
        return this.gtmItemDescription;
    }

    /**
     * Gets the value of the gtmItemCountryOfOrigin property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the gtmItemCountryOfOrigin property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGtmItemCountryOfOrigin().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GtmItemCountryOfOriginType }
     * 
     * 
     */
    public List<GtmItemCountryOfOriginType> getGtmItemCountryOfOrigin() {
        if (gtmItemCountryOfOrigin == null) {
            gtmItemCountryOfOrigin = new ArrayList<GtmItemCountryOfOriginType>();
        }
        return this.gtmItemCountryOfOrigin;
    }

    /**
     * Gets the value of the gtmItemUomConversion property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the gtmItemUomConversion property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGtmItemUomConversion().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GtmItemUomConversionType }
     * 
     * 
     */
    public List<GtmItemUomConversionType> getGtmItemUomConversion() {
        if (gtmItemUomConversion == null) {
            gtmItemUomConversion = new ArrayList<GtmItemUomConversionType>();
        }
        return this.gtmItemUomConversion;
    }

    /**
     * Obtém o valor da propriedade priority.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPriority() {
        return priority;
    }

    /**
     * Define o valor da propriedade priority.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPriority(String value) {
        this.priority = value;
    }

    /**
     * Obtém o valor da propriedade flexFieldStrings.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldStringType }
     *     
     */
    public FlexFieldStringType getFlexFieldStrings() {
        return flexFieldStrings;
    }

    /**
     * Define o valor da propriedade flexFieldStrings.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldStringType }
     *     
     */
    public void setFlexFieldStrings(FlexFieldStringType value) {
        this.flexFieldStrings = value;
    }

    /**
     * Obtém o valor da propriedade flexFieldNumbers.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldNumberType }
     *     
     */
    public FlexFieldNumberType getFlexFieldNumbers() {
        return flexFieldNumbers;
    }

    /**
     * Define o valor da propriedade flexFieldNumbers.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldNumberType }
     *     
     */
    public void setFlexFieldNumbers(FlexFieldNumberType value) {
        this.flexFieldNumbers = value;
    }

    /**
     * Obtém o valor da propriedade flexFieldDates.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldDateType }
     *     
     */
    public FlexFieldDateType getFlexFieldDates() {
        return flexFieldDates;
    }

    /**
     * Define o valor da propriedade flexFieldDates.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldDateType }
     *     
     */
    public void setFlexFieldDates(FlexFieldDateType value) {
        this.flexFieldDates = value;
    }

}
