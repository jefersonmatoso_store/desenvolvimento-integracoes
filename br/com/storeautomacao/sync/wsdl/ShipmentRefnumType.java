
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 *             ShipmentRefnum is an alternate method for identifying a shipment. It consists of a qualifier and a value.
 *          
 * 
 * <p>Classe Java de ShipmentRefnumType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="ShipmentRefnumType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ShipmentRefnumQualifierGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="ShipmentRefnumValue" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ShipmentRefnumType", propOrder = {
    "shipmentRefnumQualifierGid",
    "shipmentRefnumValue"
})
public class ShipmentRefnumType {

    @XmlElement(name = "ShipmentRefnumQualifierGid", required = true)
    protected GLogXMLGidType shipmentRefnumQualifierGid;
    @XmlElement(name = "ShipmentRefnumValue", required = true)
    protected String shipmentRefnumValue;

    /**
     * Obtém o valor da propriedade shipmentRefnumQualifierGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getShipmentRefnumQualifierGid() {
        return shipmentRefnumQualifierGid;
    }

    /**
     * Define o valor da propriedade shipmentRefnumQualifierGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setShipmentRefnumQualifierGid(GLogXMLGidType value) {
        this.shipmentRefnumQualifierGid = value;
    }

    /**
     * Obtém o valor da propriedade shipmentRefnumValue.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipmentRefnumValue() {
        return shipmentRefnumValue;
    }

    /**
     * Define o valor da propriedade shipmentRefnumValue.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipmentRefnumValue(String value) {
        this.shipmentRefnumValue = value;
    }

}
