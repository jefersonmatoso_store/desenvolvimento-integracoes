
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * (Both) Policy details of business transaction
 *          
 * 
 * <p>Classe Java de TransactionPolicyType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="TransactionPolicyType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GtmPolicyQualifierGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="GtmComplianceTemplateGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="ComplianceRuleGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="RuleGroupGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TransactionPolicyType", namespace = "http://xmlns.oracle.com/apps/gtm/transmission/v6.4", propOrder = {
    "gtmPolicyQualifierGid",
    "gtmComplianceTemplateGid",
    "complianceRuleGid",
    "ruleGroupGid"
})
public class TransactionPolicyType {

    @XmlElement(name = "GtmPolicyQualifierGid", required = true)
    protected GLogXMLGidType gtmPolicyQualifierGid;
    @XmlElement(name = "GtmComplianceTemplateGid", required = true)
    protected GLogXMLGidType gtmComplianceTemplateGid;
    @XmlElement(name = "ComplianceRuleGid")
    protected GLogXMLGidType complianceRuleGid;
    @XmlElement(name = "RuleGroupGid")
    protected GLogXMLGidType ruleGroupGid;

    /**
     * Obtém o valor da propriedade gtmPolicyQualifierGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getGtmPolicyQualifierGid() {
        return gtmPolicyQualifierGid;
    }

    /**
     * Define o valor da propriedade gtmPolicyQualifierGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setGtmPolicyQualifierGid(GLogXMLGidType value) {
        this.gtmPolicyQualifierGid = value;
    }

    /**
     * Obtém o valor da propriedade gtmComplianceTemplateGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getGtmComplianceTemplateGid() {
        return gtmComplianceTemplateGid;
    }

    /**
     * Define o valor da propriedade gtmComplianceTemplateGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setGtmComplianceTemplateGid(GLogXMLGidType value) {
        this.gtmComplianceTemplateGid = value;
    }

    /**
     * Obtém o valor da propriedade complianceRuleGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getComplianceRuleGid() {
        return complianceRuleGid;
    }

    /**
     * Define o valor da propriedade complianceRuleGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setComplianceRuleGid(GLogXMLGidType value) {
        this.complianceRuleGid = value;
    }

    /**
     * Obtém o valor da propriedade ruleGroupGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRuleGroupGid() {
        return ruleGroupGid;
    }

    /**
     * Define o valor da propriedade ruleGroupGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRuleGroupGid(GLogXMLGidType value) {
        this.ruleGroupGid = value;
    }

}
