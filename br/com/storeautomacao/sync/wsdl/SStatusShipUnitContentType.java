
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 *             SStatusShipUnitContent is a structure for specifying received ship unit line quantities.
 *          
 * 
 * <p>Classe Java de SStatusShipUnitContentType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="SStatusShipUnitContentType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;choice>
 *           &lt;element name="IntSavedQuery" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}IntSavedQueryType"/>
 *           &lt;element name="LineNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;/choice>
 *         &lt;element name="PackagedItemGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="PackagedItemSpecGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="ReceivedWeightVolume" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}WeightVolumeType" minOccurs="0"/>
 *         &lt;element name="ReceivedPackageItemCount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ReceivedPackagingUnitCount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ReceivedCountPerShipUnit" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SStatusShipUnitContentType", propOrder = {
    "intSavedQuery",
    "lineNumber",
    "packagedItemGid",
    "packagedItemSpecGid",
    "receivedWeightVolume",
    "receivedPackageItemCount",
    "receivedPackagingUnitCount",
    "receivedCountPerShipUnit"
})
public class SStatusShipUnitContentType {

    @XmlElement(name = "IntSavedQuery")
    protected IntSavedQueryType intSavedQuery;
    @XmlElement(name = "LineNumber")
    protected String lineNumber;
    @XmlElement(name = "PackagedItemGid")
    protected GLogXMLGidType packagedItemGid;
    @XmlElement(name = "PackagedItemSpecGid")
    protected GLogXMLGidType packagedItemSpecGid;
    @XmlElement(name = "ReceivedWeightVolume")
    protected WeightVolumeType receivedWeightVolume;
    @XmlElement(name = "ReceivedPackageItemCount")
    protected String receivedPackageItemCount;
    @XmlElement(name = "ReceivedPackagingUnitCount")
    protected String receivedPackagingUnitCount;
    @XmlElement(name = "ReceivedCountPerShipUnit")
    protected String receivedCountPerShipUnit;

    /**
     * Obtém o valor da propriedade intSavedQuery.
     * 
     * @return
     *     possible object is
     *     {@link IntSavedQueryType }
     *     
     */
    public IntSavedQueryType getIntSavedQuery() {
        return intSavedQuery;
    }

    /**
     * Define o valor da propriedade intSavedQuery.
     * 
     * @param value
     *     allowed object is
     *     {@link IntSavedQueryType }
     *     
     */
    public void setIntSavedQuery(IntSavedQueryType value) {
        this.intSavedQuery = value;
    }

    /**
     * Obtém o valor da propriedade lineNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLineNumber() {
        return lineNumber;
    }

    /**
     * Define o valor da propriedade lineNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLineNumber(String value) {
        this.lineNumber = value;
    }

    /**
     * Obtém o valor da propriedade packagedItemGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getPackagedItemGid() {
        return packagedItemGid;
    }

    /**
     * Define o valor da propriedade packagedItemGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setPackagedItemGid(GLogXMLGidType value) {
        this.packagedItemGid = value;
    }

    /**
     * Obtém o valor da propriedade packagedItemSpecGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getPackagedItemSpecGid() {
        return packagedItemSpecGid;
    }

    /**
     * Define o valor da propriedade packagedItemSpecGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setPackagedItemSpecGid(GLogXMLGidType value) {
        this.packagedItemSpecGid = value;
    }

    /**
     * Obtém o valor da propriedade receivedWeightVolume.
     * 
     * @return
     *     possible object is
     *     {@link WeightVolumeType }
     *     
     */
    public WeightVolumeType getReceivedWeightVolume() {
        return receivedWeightVolume;
    }

    /**
     * Define o valor da propriedade receivedWeightVolume.
     * 
     * @param value
     *     allowed object is
     *     {@link WeightVolumeType }
     *     
     */
    public void setReceivedWeightVolume(WeightVolumeType value) {
        this.receivedWeightVolume = value;
    }

    /**
     * Obtém o valor da propriedade receivedPackageItemCount.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReceivedPackageItemCount() {
        return receivedPackageItemCount;
    }

    /**
     * Define o valor da propriedade receivedPackageItemCount.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReceivedPackageItemCount(String value) {
        this.receivedPackageItemCount = value;
    }

    /**
     * Obtém o valor da propriedade receivedPackagingUnitCount.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReceivedPackagingUnitCount() {
        return receivedPackagingUnitCount;
    }

    /**
     * Define o valor da propriedade receivedPackagingUnitCount.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReceivedPackagingUnitCount(String value) {
        this.receivedPackagingUnitCount = value;
    }

    /**
     * Obtém o valor da propriedade receivedCountPerShipUnit.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReceivedCountPerShipUnit() {
        return receivedCountPerShipUnit;
    }

    /**
     * Define o valor da propriedade receivedCountPerShipUnit.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReceivedCountPerShipUnit(String value) {
        this.receivedCountPerShipUnit = value;
    }

}
