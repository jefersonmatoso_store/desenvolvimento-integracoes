
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;


/**
 * Specifies the voyage for creating the consol shipment.
 * 
 * <p>Classe Java de CharterVoyageType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="CharterVoyageType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OTMTransactionInOut">
 *       &lt;sequence>
 *         &lt;element name="IntSavedQuery" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}IntSavedQueryType" minOccurs="0"/>
 *         &lt;element name="CharterVoyageGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="TransactionCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransactionCodeType"/>
 *         &lt;element name="ReplaceChildren" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ReplaceChildrenType" minOccurs="0"/>
 *         &lt;element name="CharterVoyageType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ServiceProviderGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="VoyageName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SourceLocationRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocRefType" minOccurs="0"/>
 *         &lt;element name="DepartureDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="EstDepartureDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="ActDepartureDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="DestinationLocationRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocRefType" minOccurs="0"/>
 *         &lt;element name="ArrivalDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="EstArrivalDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="ActArrivalDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="VesselGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="VoyageProjectName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ProjectSeqNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CapacityCommitTime" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDurationType" minOccurs="0"/>
 *         &lt;element name="BookingFreezeTime" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDurationType" minOccurs="0"/>
 *         &lt;element name="IsCancelled" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FeederVessel" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InvolvedParty" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}InvolvedPartyType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Refnum" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RefnumType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="CharterVoyageStowage" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}CharterVoyageStowageType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CharterVoyageType", propOrder = {
    "intSavedQuery",
    "charterVoyageGid",
    "transactionCode",
    "replaceChildren",
    "charterVoyageType",
    "serviceProviderGid",
    "voyageName",
    "sourceLocationRef",
    "departureDt",
    "estDepartureDt",
    "actDepartureDt",
    "destinationLocationRef",
    "arrivalDt",
    "estArrivalDt",
    "actArrivalDt",
    "vesselGid",
    "voyageProjectName",
    "projectSeqNum",
    "capacityCommitTime",
    "bookingFreezeTime",
    "isCancelled",
    "feederVessel",
    "involvedParty",
    "refnum",
    "charterVoyageStowage"
})
public class CharterVoyageType
    extends OTMTransactionInOut
{

    @XmlElement(name = "IntSavedQuery")
    protected IntSavedQueryType intSavedQuery;
    @XmlElement(name = "CharterVoyageGid")
    protected GLogXMLGidType charterVoyageGid;
    @XmlElement(name = "TransactionCode", required = true)
    @XmlSchemaType(name = "string")
    protected TransactionCodeType transactionCode;
    @XmlElement(name = "ReplaceChildren")
    protected ReplaceChildrenType replaceChildren;
    @XmlElement(name = "CharterVoyageType", required = true)
    protected String charterVoyageType;
    @XmlElement(name = "ServiceProviderGid", required = true)
    protected GLogXMLGidType serviceProviderGid;
    @XmlElement(name = "VoyageName")
    protected String voyageName;
    @XmlElement(name = "SourceLocationRef")
    protected GLogXMLLocRefType sourceLocationRef;
    @XmlElement(name = "DepartureDt")
    protected GLogDateTimeType departureDt;
    @XmlElement(name = "EstDepartureDt")
    protected GLogDateTimeType estDepartureDt;
    @XmlElement(name = "ActDepartureDt")
    protected GLogDateTimeType actDepartureDt;
    @XmlElement(name = "DestinationLocationRef")
    protected GLogXMLLocRefType destinationLocationRef;
    @XmlElement(name = "ArrivalDt")
    protected GLogDateTimeType arrivalDt;
    @XmlElement(name = "EstArrivalDt")
    protected GLogDateTimeType estArrivalDt;
    @XmlElement(name = "ActArrivalDt")
    protected GLogDateTimeType actArrivalDt;
    @XmlElement(name = "VesselGid")
    protected GLogXMLGidType vesselGid;
    @XmlElement(name = "VoyageProjectName")
    protected String voyageProjectName;
    @XmlElement(name = "ProjectSeqNum")
    protected String projectSeqNum;
    @XmlElement(name = "CapacityCommitTime")
    protected GLogXMLDurationType capacityCommitTime;
    @XmlElement(name = "BookingFreezeTime")
    protected GLogXMLDurationType bookingFreezeTime;
    @XmlElement(name = "IsCancelled")
    protected String isCancelled;
    @XmlElement(name = "FeederVessel")
    protected String feederVessel;
    @XmlElement(name = "InvolvedParty")
    protected List<InvolvedPartyType> involvedParty;
    @XmlElement(name = "Refnum")
    protected List<RefnumType> refnum;
    @XmlElement(name = "CharterVoyageStowage")
    protected List<CharterVoyageStowageType> charterVoyageStowage;

    /**
     * Obtém o valor da propriedade intSavedQuery.
     * 
     * @return
     *     possible object is
     *     {@link IntSavedQueryType }
     *     
     */
    public IntSavedQueryType getIntSavedQuery() {
        return intSavedQuery;
    }

    /**
     * Define o valor da propriedade intSavedQuery.
     * 
     * @param value
     *     allowed object is
     *     {@link IntSavedQueryType }
     *     
     */
    public void setIntSavedQuery(IntSavedQueryType value) {
        this.intSavedQuery = value;
    }

    /**
     * Obtém o valor da propriedade charterVoyageGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getCharterVoyageGid() {
        return charterVoyageGid;
    }

    /**
     * Define o valor da propriedade charterVoyageGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setCharterVoyageGid(GLogXMLGidType value) {
        this.charterVoyageGid = value;
    }

    /**
     * Obtém o valor da propriedade transactionCode.
     * 
     * @return
     *     possible object is
     *     {@link TransactionCodeType }
     *     
     */
    public TransactionCodeType getTransactionCode() {
        return transactionCode;
    }

    /**
     * Define o valor da propriedade transactionCode.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionCodeType }
     *     
     */
    public void setTransactionCode(TransactionCodeType value) {
        this.transactionCode = value;
    }

    /**
     * Obtém o valor da propriedade replaceChildren.
     * 
     * @return
     *     possible object is
     *     {@link ReplaceChildrenType }
     *     
     */
    public ReplaceChildrenType getReplaceChildren() {
        return replaceChildren;
    }

    /**
     * Define o valor da propriedade replaceChildren.
     * 
     * @param value
     *     allowed object is
     *     {@link ReplaceChildrenType }
     *     
     */
    public void setReplaceChildren(ReplaceChildrenType value) {
        this.replaceChildren = value;
    }

    /**
     * Obtém o valor da propriedade charterVoyageType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCharterVoyageType() {
        return charterVoyageType;
    }

    /**
     * Define o valor da propriedade charterVoyageType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCharterVoyageType(String value) {
        this.charterVoyageType = value;
    }

    /**
     * Obtém o valor da propriedade serviceProviderGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getServiceProviderGid() {
        return serviceProviderGid;
    }

    /**
     * Define o valor da propriedade serviceProviderGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setServiceProviderGid(GLogXMLGidType value) {
        this.serviceProviderGid = value;
    }

    /**
     * Obtém o valor da propriedade voyageName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVoyageName() {
        return voyageName;
    }

    /**
     * Define o valor da propriedade voyageName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVoyageName(String value) {
        this.voyageName = value;
    }

    /**
     * Obtém o valor da propriedade sourceLocationRef.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public GLogXMLLocRefType getSourceLocationRef() {
        return sourceLocationRef;
    }

    /**
     * Define o valor da propriedade sourceLocationRef.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public void setSourceLocationRef(GLogXMLLocRefType value) {
        this.sourceLocationRef = value;
    }

    /**
     * Obtém o valor da propriedade departureDt.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getDepartureDt() {
        return departureDt;
    }

    /**
     * Define o valor da propriedade departureDt.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setDepartureDt(GLogDateTimeType value) {
        this.departureDt = value;
    }

    /**
     * Obtém o valor da propriedade estDepartureDt.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getEstDepartureDt() {
        return estDepartureDt;
    }

    /**
     * Define o valor da propriedade estDepartureDt.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setEstDepartureDt(GLogDateTimeType value) {
        this.estDepartureDt = value;
    }

    /**
     * Obtém o valor da propriedade actDepartureDt.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getActDepartureDt() {
        return actDepartureDt;
    }

    /**
     * Define o valor da propriedade actDepartureDt.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setActDepartureDt(GLogDateTimeType value) {
        this.actDepartureDt = value;
    }

    /**
     * Obtém o valor da propriedade destinationLocationRef.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public GLogXMLLocRefType getDestinationLocationRef() {
        return destinationLocationRef;
    }

    /**
     * Define o valor da propriedade destinationLocationRef.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public void setDestinationLocationRef(GLogXMLLocRefType value) {
        this.destinationLocationRef = value;
    }

    /**
     * Obtém o valor da propriedade arrivalDt.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getArrivalDt() {
        return arrivalDt;
    }

    /**
     * Define o valor da propriedade arrivalDt.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setArrivalDt(GLogDateTimeType value) {
        this.arrivalDt = value;
    }

    /**
     * Obtém o valor da propriedade estArrivalDt.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getEstArrivalDt() {
        return estArrivalDt;
    }

    /**
     * Define o valor da propriedade estArrivalDt.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setEstArrivalDt(GLogDateTimeType value) {
        this.estArrivalDt = value;
    }

    /**
     * Obtém o valor da propriedade actArrivalDt.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getActArrivalDt() {
        return actArrivalDt;
    }

    /**
     * Define o valor da propriedade actArrivalDt.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setActArrivalDt(GLogDateTimeType value) {
        this.actArrivalDt = value;
    }

    /**
     * Obtém o valor da propriedade vesselGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getVesselGid() {
        return vesselGid;
    }

    /**
     * Define o valor da propriedade vesselGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setVesselGid(GLogXMLGidType value) {
        this.vesselGid = value;
    }

    /**
     * Obtém o valor da propriedade voyageProjectName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVoyageProjectName() {
        return voyageProjectName;
    }

    /**
     * Define o valor da propriedade voyageProjectName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVoyageProjectName(String value) {
        this.voyageProjectName = value;
    }

    /**
     * Obtém o valor da propriedade projectSeqNum.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProjectSeqNum() {
        return projectSeqNum;
    }

    /**
     * Define o valor da propriedade projectSeqNum.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProjectSeqNum(String value) {
        this.projectSeqNum = value;
    }

    /**
     * Obtém o valor da propriedade capacityCommitTime.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public GLogXMLDurationType getCapacityCommitTime() {
        return capacityCommitTime;
    }

    /**
     * Define o valor da propriedade capacityCommitTime.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public void setCapacityCommitTime(GLogXMLDurationType value) {
        this.capacityCommitTime = value;
    }

    /**
     * Obtém o valor da propriedade bookingFreezeTime.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public GLogXMLDurationType getBookingFreezeTime() {
        return bookingFreezeTime;
    }

    /**
     * Define o valor da propriedade bookingFreezeTime.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public void setBookingFreezeTime(GLogXMLDurationType value) {
        this.bookingFreezeTime = value;
    }

    /**
     * Obtém o valor da propriedade isCancelled.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsCancelled() {
        return isCancelled;
    }

    /**
     * Define o valor da propriedade isCancelled.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsCancelled(String value) {
        this.isCancelled = value;
    }

    /**
     * Obtém o valor da propriedade feederVessel.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFeederVessel() {
        return feederVessel;
    }

    /**
     * Define o valor da propriedade feederVessel.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFeederVessel(String value) {
        this.feederVessel = value;
    }

    /**
     * Gets the value of the involvedParty property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the involvedParty property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInvolvedParty().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link InvolvedPartyType }
     * 
     * 
     */
    public List<InvolvedPartyType> getInvolvedParty() {
        if (involvedParty == null) {
            involvedParty = new ArrayList<InvolvedPartyType>();
        }
        return this.involvedParty;
    }

    /**
     * Gets the value of the refnum property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the refnum property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRefnum().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RefnumType }
     * 
     * 
     */
    public List<RefnumType> getRefnum() {
        if (refnum == null) {
            refnum = new ArrayList<RefnumType>();
        }
        return this.refnum;
    }

    /**
     * Gets the value of the charterVoyageStowage property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the charterVoyageStowage property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCharterVoyageStowage().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CharterVoyageStowageType }
     * 
     * 
     */
    public List<CharterVoyageStowageType> getCharterVoyageStowage() {
        if (charterVoyageStowage == null) {
            charterVoyageStowage = new ArrayList<CharterVoyageStowageType>();
        }
        return this.charterVoyageStowage;
    }

}
