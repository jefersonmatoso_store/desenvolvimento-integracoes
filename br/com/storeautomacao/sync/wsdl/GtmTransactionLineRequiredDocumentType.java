
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * (Outbound) Required Documents of transaction line item or declaration line item.
 *          
 * 
 * <p>Classe Java de GtmTransactionLineRequiredDocumentType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="GtmTransactionLineRequiredDocumentType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GtmTransactionLineDocumentRequiredText" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}RequiredTextType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="DocumentDefGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="ComplianceRuleGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="ComplianceRuleGroupGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GtmTransactionLineRequiredDocumentType", namespace = "http://xmlns.oracle.com/apps/gtm/transmission/v6.4", propOrder = {
    "gtmTransactionLineDocumentRequiredText",
    "documentDefGid",
    "complianceRuleGid",
    "complianceRuleGroupGid"
})
public class GtmTransactionLineRequiredDocumentType {

    @XmlElement(name = "GtmTransactionLineDocumentRequiredText")
    protected List<RequiredTextType> gtmTransactionLineDocumentRequiredText;
    @XmlElement(name = "DocumentDefGid", required = true)
    protected GLogXMLGidType documentDefGid;
    @XmlElement(name = "ComplianceRuleGid")
    protected GLogXMLGidType complianceRuleGid;
    @XmlElement(name = "ComplianceRuleGroupGid")
    protected GLogXMLGidType complianceRuleGroupGid;

    /**
     * Gets the value of the gtmTransactionLineDocumentRequiredText property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the gtmTransactionLineDocumentRequiredText property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGtmTransactionLineDocumentRequiredText().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RequiredTextType }
     * 
     * 
     */
    public List<RequiredTextType> getGtmTransactionLineDocumentRequiredText() {
        if (gtmTransactionLineDocumentRequiredText == null) {
            gtmTransactionLineDocumentRequiredText = new ArrayList<RequiredTextType>();
        }
        return this.gtmTransactionLineDocumentRequiredText;
    }

    /**
     * Obtém o valor da propriedade documentDefGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getDocumentDefGid() {
        return documentDefGid;
    }

    /**
     * Define o valor da propriedade documentDefGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setDocumentDefGid(GLogXMLGidType value) {
        this.documentDefGid = value;
    }

    /**
     * Obtém o valor da propriedade complianceRuleGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getComplianceRuleGid() {
        return complianceRuleGid;
    }

    /**
     * Define o valor da propriedade complianceRuleGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setComplianceRuleGid(GLogXMLGidType value) {
        this.complianceRuleGid = value;
    }

    /**
     * Obtém o valor da propriedade complianceRuleGroupGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getComplianceRuleGroupGid() {
        return complianceRuleGroupGid;
    }

    /**
     * Define o valor da propriedade complianceRuleGroupGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setComplianceRuleGroupGid(GLogXMLGidType value) {
        this.complianceRuleGroupGid = value;
    }

}
