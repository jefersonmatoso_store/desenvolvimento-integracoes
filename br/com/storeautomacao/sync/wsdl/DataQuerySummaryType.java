
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * The DataQuerySummary element is used to provide a summary (i.e. primary Id) of the
 *             notification/data/information being sent to an external system. Certain external
 *             systems
 *             may not want GC3 to send large amounts of data when the system is unprepared
 *             to receive it. Alternatively, this provides a mechanism to send only a summary of
 *             the data. The external system
 *             can retrieve the summary data and request the
 *             individual records from GC3 at appropriate times (e.g. idle times, overnight).
 *          
 * 
 * <p>Classe Java de DataQuerySummaryType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="DataQuerySummaryType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OTMTransactionOut">
 *       &lt;sequence>
 *         &lt;element name="TransactionCodeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="SendReason" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}SendReasonType"/>
 *         &lt;element name="DataQueryTypeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="RecordCount" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="DataList" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="DataGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" maxOccurs="unbounded"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataQuerySummaryType", propOrder = {
    "transactionCodeGid",
    "sendReason",
    "dataQueryTypeGid",
    "recordCount",
    "dataList"
})
public class DataQuerySummaryType
    extends OTMTransactionOut
{

    @XmlElement(name = "TransactionCodeGid")
    protected GLogXMLGidType transactionCodeGid;
    @XmlElement(name = "SendReason", required = true)
    protected SendReasonType sendReason;
    @XmlElement(name = "DataQueryTypeGid", required = true)
    protected GLogXMLGidType dataQueryTypeGid;
    @XmlElement(name = "RecordCount", required = true)
    protected String recordCount;
    @XmlElement(name = "DataList")
    protected DataQuerySummaryType.DataList dataList;

    /**
     * Obtém o valor da propriedade transactionCodeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getTransactionCodeGid() {
        return transactionCodeGid;
    }

    /**
     * Define o valor da propriedade transactionCodeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setTransactionCodeGid(GLogXMLGidType value) {
        this.transactionCodeGid = value;
    }

    /**
     * Obtém o valor da propriedade sendReason.
     * 
     * @return
     *     possible object is
     *     {@link SendReasonType }
     *     
     */
    public SendReasonType getSendReason() {
        return sendReason;
    }

    /**
     * Define o valor da propriedade sendReason.
     * 
     * @param value
     *     allowed object is
     *     {@link SendReasonType }
     *     
     */
    public void setSendReason(SendReasonType value) {
        this.sendReason = value;
    }

    /**
     * Obtém o valor da propriedade dataQueryTypeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getDataQueryTypeGid() {
        return dataQueryTypeGid;
    }

    /**
     * Define o valor da propriedade dataQueryTypeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setDataQueryTypeGid(GLogXMLGidType value) {
        this.dataQueryTypeGid = value;
    }

    /**
     * Obtém o valor da propriedade recordCount.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRecordCount() {
        return recordCount;
    }

    /**
     * Define o valor da propriedade recordCount.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRecordCount(String value) {
        this.recordCount = value;
    }

    /**
     * Obtém o valor da propriedade dataList.
     * 
     * @return
     *     possible object is
     *     {@link DataQuerySummaryType.DataList }
     *     
     */
    public DataQuerySummaryType.DataList getDataList() {
        return dataList;
    }

    /**
     * Define o valor da propriedade dataList.
     * 
     * @param value
     *     allowed object is
     *     {@link DataQuerySummaryType.DataList }
     *     
     */
    public void setDataList(DataQuerySummaryType.DataList value) {
        this.dataList = value;
    }


    /**
     * <p>Classe Java de anonymous complex type.
     * 
     * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="DataGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" maxOccurs="unbounded"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "dataGid"
    })
    public static class DataList {

        @XmlElement(name = "DataGid", required = true)
        protected List<GLogXMLGidType> dataGid;

        /**
         * Gets the value of the dataGid property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the dataGid property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getDataGid().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link GLogXMLGidType }
         * 
         * 
         */
        public List<GLogXMLGidType> getDataGid() {
            if (dataGid == null) {
                dataGid = new ArrayList<GLogXMLGidType>();
            }
            return this.dataGid;
        }

    }

}
