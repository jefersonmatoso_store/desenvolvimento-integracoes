
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * Rate Inquiry results when the RIQ Route option is used.
 * 
 * <p>Classe Java de RIQResultRouteType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="RIQResultRouteType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ItineraryGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="StartTimestamp" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TimeWithTimezoneType" minOccurs="0"/>
 *         &lt;element name="EndTimestamp" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TimeWithTimezoneType" minOccurs="0"/>
 *         &lt;element name="TotalActualCost" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/>
 *         &lt;element name="IsFeasible" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RIQRouteShipment" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RIQRouteShipmentType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Perspective" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsOptimalResult" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LeadTime" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDurationType" minOccurs="0"/>
 *         &lt;element name="TotalActualCostPerUOM" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLCostPerUOM" minOccurs="0"/>
 *         &lt;element name="TotalWeightedCostPerUOM" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLCostPerUOM" minOccurs="0"/>
 *         &lt;element name="TotalWeightedCost" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/>
 *         &lt;element name="TransportModeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="ServiceProviderGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="TotalTransitTime" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDurationType" minOccurs="0"/>
 *         &lt;element name="OrigCarrierGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="DelivCarrierGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="EquipmentGroupGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="RouteCodeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="VesselGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="PortOfLoadLocationRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocRefType" minOccurs="0"/>
 *         &lt;element name="PortOfDisLocationRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocRefType" minOccurs="0"/>
 *         &lt;element name="PrimarySourceLocationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="PrimaryDestLocationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="PrimaryTotalCost" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/>
 *         &lt;element name="VoyageGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="PrimaryTransitTime" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDurationType" minOccurs="0"/>
 *         &lt;element name="CommitmentCountPlanned" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CommitmentCountActual" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PrimaryRateOfferingGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="PrimaryRateGeoGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="CommodityGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="ShipFromLocationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="ShipToLocationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="RIQSecondaryCharges" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RIQSecondaryChargesType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RIQResultRouteType", propOrder = {
    "itineraryGid",
    "startTimestamp",
    "endTimestamp",
    "totalActualCost",
    "isFeasible",
    "riqRouteShipment",
    "perspective",
    "isOptimalResult",
    "leadTime",
    "totalActualCostPerUOM",
    "totalWeightedCostPerUOM",
    "totalWeightedCost",
    "transportModeGid",
    "serviceProviderGid",
    "totalTransitTime",
    "origCarrierGid",
    "delivCarrierGid",
    "equipmentGroupGid",
    "routeCodeGid",
    "vesselGid",
    "portOfLoadLocationRef",
    "portOfDisLocationRef",
    "primarySourceLocationGid",
    "primaryDestLocationGid",
    "primaryTotalCost",
    "voyageGid",
    "primaryTransitTime",
    "commitmentCountPlanned",
    "commitmentCountActual",
    "primaryRateOfferingGid",
    "primaryRateGeoGid",
    "commodityGid",
    "shipFromLocationGid",
    "shipToLocationGid",
    "riqSecondaryCharges"
})
public class RIQResultRouteType {

    @XmlElement(name = "ItineraryGid", required = true)
    protected GLogXMLGidType itineraryGid;
    @XmlElement(name = "StartTimestamp")
    protected TimeWithTimezoneType startTimestamp;
    @XmlElement(name = "EndTimestamp")
    protected TimeWithTimezoneType endTimestamp;
    @XmlElement(name = "TotalActualCost")
    protected GLogXMLFinancialAmountType totalActualCost;
    @XmlElement(name = "IsFeasible")
    protected String isFeasible;
    @XmlElement(name = "RIQRouteShipment")
    protected List<RIQRouteShipmentType> riqRouteShipment;
    @XmlElement(name = "Perspective")
    protected String perspective;
    @XmlElement(name = "IsOptimalResult")
    protected String isOptimalResult;
    @XmlElement(name = "LeadTime")
    protected GLogXMLDurationType leadTime;
    @XmlElement(name = "TotalActualCostPerUOM")
    protected GLogXMLCostPerUOM totalActualCostPerUOM;
    @XmlElement(name = "TotalWeightedCostPerUOM")
    protected GLogXMLCostPerUOM totalWeightedCostPerUOM;
    @XmlElement(name = "TotalWeightedCost")
    protected GLogXMLFinancialAmountType totalWeightedCost;
    @XmlElement(name = "TransportModeGid")
    protected GLogXMLGidType transportModeGid;
    @XmlElement(name = "ServiceProviderGid")
    protected GLogXMLGidType serviceProviderGid;
    @XmlElement(name = "TotalTransitTime")
    protected GLogXMLDurationType totalTransitTime;
    @XmlElement(name = "OrigCarrierGid")
    protected GLogXMLGidType origCarrierGid;
    @XmlElement(name = "DelivCarrierGid")
    protected GLogXMLGidType delivCarrierGid;
    @XmlElement(name = "EquipmentGroupGid")
    protected GLogXMLGidType equipmentGroupGid;
    @XmlElement(name = "RouteCodeGid")
    protected GLogXMLGidType routeCodeGid;
    @XmlElement(name = "VesselGid")
    protected GLogXMLGidType vesselGid;
    @XmlElement(name = "PortOfLoadLocationRef")
    protected GLogXMLLocRefType portOfLoadLocationRef;
    @XmlElement(name = "PortOfDisLocationRef")
    protected GLogXMLLocRefType portOfDisLocationRef;
    @XmlElement(name = "PrimarySourceLocationGid")
    protected GLogXMLGidType primarySourceLocationGid;
    @XmlElement(name = "PrimaryDestLocationGid")
    protected GLogXMLGidType primaryDestLocationGid;
    @XmlElement(name = "PrimaryTotalCost")
    protected GLogXMLFinancialAmountType primaryTotalCost;
    @XmlElement(name = "VoyageGid")
    protected GLogXMLGidType voyageGid;
    @XmlElement(name = "PrimaryTransitTime")
    protected GLogXMLDurationType primaryTransitTime;
    @XmlElement(name = "CommitmentCountPlanned")
    protected String commitmentCountPlanned;
    @XmlElement(name = "CommitmentCountActual")
    protected String commitmentCountActual;
    @XmlElement(name = "PrimaryRateOfferingGid")
    protected GLogXMLGidType primaryRateOfferingGid;
    @XmlElement(name = "PrimaryRateGeoGid")
    protected GLogXMLGidType primaryRateGeoGid;
    @XmlElement(name = "CommodityGid")
    protected GLogXMLGidType commodityGid;
    @XmlElement(name = "ShipFromLocationGid")
    protected GLogXMLGidType shipFromLocationGid;
    @XmlElement(name = "ShipToLocationGid")
    protected GLogXMLGidType shipToLocationGid;
    @XmlElement(name = "RIQSecondaryCharges")
    protected RIQSecondaryChargesType riqSecondaryCharges;

    /**
     * Obtém o valor da propriedade itineraryGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getItineraryGid() {
        return itineraryGid;
    }

    /**
     * Define o valor da propriedade itineraryGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setItineraryGid(GLogXMLGidType value) {
        this.itineraryGid = value;
    }

    /**
     * Obtém o valor da propriedade startTimestamp.
     * 
     * @return
     *     possible object is
     *     {@link TimeWithTimezoneType }
     *     
     */
    public TimeWithTimezoneType getStartTimestamp() {
        return startTimestamp;
    }

    /**
     * Define o valor da propriedade startTimestamp.
     * 
     * @param value
     *     allowed object is
     *     {@link TimeWithTimezoneType }
     *     
     */
    public void setStartTimestamp(TimeWithTimezoneType value) {
        this.startTimestamp = value;
    }

    /**
     * Obtém o valor da propriedade endTimestamp.
     * 
     * @return
     *     possible object is
     *     {@link TimeWithTimezoneType }
     *     
     */
    public TimeWithTimezoneType getEndTimestamp() {
        return endTimestamp;
    }

    /**
     * Define o valor da propriedade endTimestamp.
     * 
     * @param value
     *     allowed object is
     *     {@link TimeWithTimezoneType }
     *     
     */
    public void setEndTimestamp(TimeWithTimezoneType value) {
        this.endTimestamp = value;
    }

    /**
     * Obtém o valor da propriedade totalActualCost.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getTotalActualCost() {
        return totalActualCost;
    }

    /**
     * Define o valor da propriedade totalActualCost.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setTotalActualCost(GLogXMLFinancialAmountType value) {
        this.totalActualCost = value;
    }

    /**
     * Obtém o valor da propriedade isFeasible.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsFeasible() {
        return isFeasible;
    }

    /**
     * Define o valor da propriedade isFeasible.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsFeasible(String value) {
        this.isFeasible = value;
    }

    /**
     * Gets the value of the riqRouteShipment property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the riqRouteShipment property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRIQRouteShipment().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RIQRouteShipmentType }
     * 
     * 
     */
    public List<RIQRouteShipmentType> getRIQRouteShipment() {
        if (riqRouteShipment == null) {
            riqRouteShipment = new ArrayList<RIQRouteShipmentType>();
        }
        return this.riqRouteShipment;
    }

    /**
     * Obtém o valor da propriedade perspective.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPerspective() {
        return perspective;
    }

    /**
     * Define o valor da propriedade perspective.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPerspective(String value) {
        this.perspective = value;
    }

    /**
     * Obtém o valor da propriedade isOptimalResult.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsOptimalResult() {
        return isOptimalResult;
    }

    /**
     * Define o valor da propriedade isOptimalResult.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsOptimalResult(String value) {
        this.isOptimalResult = value;
    }

    /**
     * Obtém o valor da propriedade leadTime.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public GLogXMLDurationType getLeadTime() {
        return leadTime;
    }

    /**
     * Define o valor da propriedade leadTime.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public void setLeadTime(GLogXMLDurationType value) {
        this.leadTime = value;
    }

    /**
     * Obtém o valor da propriedade totalActualCostPerUOM.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLCostPerUOM }
     *     
     */
    public GLogXMLCostPerUOM getTotalActualCostPerUOM() {
        return totalActualCostPerUOM;
    }

    /**
     * Define o valor da propriedade totalActualCostPerUOM.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLCostPerUOM }
     *     
     */
    public void setTotalActualCostPerUOM(GLogXMLCostPerUOM value) {
        this.totalActualCostPerUOM = value;
    }

    /**
     * Obtém o valor da propriedade totalWeightedCostPerUOM.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLCostPerUOM }
     *     
     */
    public GLogXMLCostPerUOM getTotalWeightedCostPerUOM() {
        return totalWeightedCostPerUOM;
    }

    /**
     * Define o valor da propriedade totalWeightedCostPerUOM.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLCostPerUOM }
     *     
     */
    public void setTotalWeightedCostPerUOM(GLogXMLCostPerUOM value) {
        this.totalWeightedCostPerUOM = value;
    }

    /**
     * Obtém o valor da propriedade totalWeightedCost.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getTotalWeightedCost() {
        return totalWeightedCost;
    }

    /**
     * Define o valor da propriedade totalWeightedCost.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setTotalWeightedCost(GLogXMLFinancialAmountType value) {
        this.totalWeightedCost = value;
    }

    /**
     * Obtém o valor da propriedade transportModeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getTransportModeGid() {
        return transportModeGid;
    }

    /**
     * Define o valor da propriedade transportModeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setTransportModeGid(GLogXMLGidType value) {
        this.transportModeGid = value;
    }

    /**
     * Obtém o valor da propriedade serviceProviderGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getServiceProviderGid() {
        return serviceProviderGid;
    }

    /**
     * Define o valor da propriedade serviceProviderGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setServiceProviderGid(GLogXMLGidType value) {
        this.serviceProviderGid = value;
    }

    /**
     * Obtém o valor da propriedade totalTransitTime.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public GLogXMLDurationType getTotalTransitTime() {
        return totalTransitTime;
    }

    /**
     * Define o valor da propriedade totalTransitTime.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public void setTotalTransitTime(GLogXMLDurationType value) {
        this.totalTransitTime = value;
    }

    /**
     * Obtém o valor da propriedade origCarrierGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getOrigCarrierGid() {
        return origCarrierGid;
    }

    /**
     * Define o valor da propriedade origCarrierGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setOrigCarrierGid(GLogXMLGidType value) {
        this.origCarrierGid = value;
    }

    /**
     * Obtém o valor da propriedade delivCarrierGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getDelivCarrierGid() {
        return delivCarrierGid;
    }

    /**
     * Define o valor da propriedade delivCarrierGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setDelivCarrierGid(GLogXMLGidType value) {
        this.delivCarrierGid = value;
    }

    /**
     * Obtém o valor da propriedade equipmentGroupGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getEquipmentGroupGid() {
        return equipmentGroupGid;
    }

    /**
     * Define o valor da propriedade equipmentGroupGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setEquipmentGroupGid(GLogXMLGidType value) {
        this.equipmentGroupGid = value;
    }

    /**
     * Obtém o valor da propriedade routeCodeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRouteCodeGid() {
        return routeCodeGid;
    }

    /**
     * Define o valor da propriedade routeCodeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRouteCodeGid(GLogXMLGidType value) {
        this.routeCodeGid = value;
    }

    /**
     * Obtém o valor da propriedade vesselGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getVesselGid() {
        return vesselGid;
    }

    /**
     * Define o valor da propriedade vesselGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setVesselGid(GLogXMLGidType value) {
        this.vesselGid = value;
    }

    /**
     * Obtém o valor da propriedade portOfLoadLocationRef.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public GLogXMLLocRefType getPortOfLoadLocationRef() {
        return portOfLoadLocationRef;
    }

    /**
     * Define o valor da propriedade portOfLoadLocationRef.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public void setPortOfLoadLocationRef(GLogXMLLocRefType value) {
        this.portOfLoadLocationRef = value;
    }

    /**
     * Obtém o valor da propriedade portOfDisLocationRef.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public GLogXMLLocRefType getPortOfDisLocationRef() {
        return portOfDisLocationRef;
    }

    /**
     * Define o valor da propriedade portOfDisLocationRef.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public void setPortOfDisLocationRef(GLogXMLLocRefType value) {
        this.portOfDisLocationRef = value;
    }

    /**
     * Obtém o valor da propriedade primarySourceLocationGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getPrimarySourceLocationGid() {
        return primarySourceLocationGid;
    }

    /**
     * Define o valor da propriedade primarySourceLocationGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setPrimarySourceLocationGid(GLogXMLGidType value) {
        this.primarySourceLocationGid = value;
    }

    /**
     * Obtém o valor da propriedade primaryDestLocationGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getPrimaryDestLocationGid() {
        return primaryDestLocationGid;
    }

    /**
     * Define o valor da propriedade primaryDestLocationGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setPrimaryDestLocationGid(GLogXMLGidType value) {
        this.primaryDestLocationGid = value;
    }

    /**
     * Obtém o valor da propriedade primaryTotalCost.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getPrimaryTotalCost() {
        return primaryTotalCost;
    }

    /**
     * Define o valor da propriedade primaryTotalCost.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setPrimaryTotalCost(GLogXMLFinancialAmountType value) {
        this.primaryTotalCost = value;
    }

    /**
     * Obtém o valor da propriedade voyageGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getVoyageGid() {
        return voyageGid;
    }

    /**
     * Define o valor da propriedade voyageGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setVoyageGid(GLogXMLGidType value) {
        this.voyageGid = value;
    }

    /**
     * Obtém o valor da propriedade primaryTransitTime.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public GLogXMLDurationType getPrimaryTransitTime() {
        return primaryTransitTime;
    }

    /**
     * Define o valor da propriedade primaryTransitTime.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public void setPrimaryTransitTime(GLogXMLDurationType value) {
        this.primaryTransitTime = value;
    }

    /**
     * Obtém o valor da propriedade commitmentCountPlanned.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCommitmentCountPlanned() {
        return commitmentCountPlanned;
    }

    /**
     * Define o valor da propriedade commitmentCountPlanned.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCommitmentCountPlanned(String value) {
        this.commitmentCountPlanned = value;
    }

    /**
     * Obtém o valor da propriedade commitmentCountActual.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCommitmentCountActual() {
        return commitmentCountActual;
    }

    /**
     * Define o valor da propriedade commitmentCountActual.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCommitmentCountActual(String value) {
        this.commitmentCountActual = value;
    }

    /**
     * Obtém o valor da propriedade primaryRateOfferingGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getPrimaryRateOfferingGid() {
        return primaryRateOfferingGid;
    }

    /**
     * Define o valor da propriedade primaryRateOfferingGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setPrimaryRateOfferingGid(GLogXMLGidType value) {
        this.primaryRateOfferingGid = value;
    }

    /**
     * Obtém o valor da propriedade primaryRateGeoGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getPrimaryRateGeoGid() {
        return primaryRateGeoGid;
    }

    /**
     * Define o valor da propriedade primaryRateGeoGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setPrimaryRateGeoGid(GLogXMLGidType value) {
        this.primaryRateGeoGid = value;
    }

    /**
     * Obtém o valor da propriedade commodityGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getCommodityGid() {
        return commodityGid;
    }

    /**
     * Define o valor da propriedade commodityGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setCommodityGid(GLogXMLGidType value) {
        this.commodityGid = value;
    }

    /**
     * Obtém o valor da propriedade shipFromLocationGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getShipFromLocationGid() {
        return shipFromLocationGid;
    }

    /**
     * Define o valor da propriedade shipFromLocationGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setShipFromLocationGid(GLogXMLGidType value) {
        this.shipFromLocationGid = value;
    }

    /**
     * Obtém o valor da propriedade shipToLocationGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getShipToLocationGid() {
        return shipToLocationGid;
    }

    /**
     * Define o valor da propriedade shipToLocationGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setShipToLocationGid(GLogXMLGidType value) {
        this.shipToLocationGid = value;
    }

    /**
     * Obtém o valor da propriedade riqSecondaryCharges.
     * 
     * @return
     *     possible object is
     *     {@link RIQSecondaryChargesType }
     *     
     */
    public RIQSecondaryChargesType getRIQSecondaryCharges() {
        return riqSecondaryCharges;
    }

    /**
     * Define o valor da propriedade riqSecondaryCharges.
     * 
     * @param value
     *     allowed object is
     *     {@link RIQSecondaryChargesType }
     *     
     */
    public void setRIQSecondaryCharges(RIQSecondaryChargesType value) {
        this.riqSecondaryCharges = value;
    }

}
