
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * 
 *             RailStopOff is an element of RailDetail used to specify rail specific stop information on an invoice.
 *          
 * 
 * <p>Classe Java de RailStopOffType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="RailStopOffType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="StopSequence" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="StopReasonGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;choice>
 *           &lt;element name="LocationRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LocationRefType"/>
 *           &lt;element name="LocationRefnum" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LocationRefnumType"/>
 *         &lt;/choice>
 *         &lt;element name="StopRefnum" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}StopRefnumType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RailStopOffType", propOrder = {
    "stopSequence",
    "stopReasonGid",
    "locationRef",
    "locationRefnum",
    "stopRefnum"
})
public class RailStopOffType {

    @XmlElement(name = "StopSequence", required = true)
    protected String stopSequence;
    @XmlElement(name = "StopReasonGid")
    protected GLogXMLGidType stopReasonGid;
    @XmlElement(name = "LocationRef")
    protected LocationRefType locationRef;
    @XmlElement(name = "LocationRefnum")
    protected LocationRefnumType locationRefnum;
    @XmlElement(name = "StopRefnum")
    protected List<StopRefnumType> stopRefnum;

    /**
     * Obtém o valor da propriedade stopSequence.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStopSequence() {
        return stopSequence;
    }

    /**
     * Define o valor da propriedade stopSequence.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStopSequence(String value) {
        this.stopSequence = value;
    }

    /**
     * Obtém o valor da propriedade stopReasonGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getStopReasonGid() {
        return stopReasonGid;
    }

    /**
     * Define o valor da propriedade stopReasonGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setStopReasonGid(GLogXMLGidType value) {
        this.stopReasonGid = value;
    }

    /**
     * Obtém o valor da propriedade locationRef.
     * 
     * @return
     *     possible object is
     *     {@link LocationRefType }
     *     
     */
    public LocationRefType getLocationRef() {
        return locationRef;
    }

    /**
     * Define o valor da propriedade locationRef.
     * 
     * @param value
     *     allowed object is
     *     {@link LocationRefType }
     *     
     */
    public void setLocationRef(LocationRefType value) {
        this.locationRef = value;
    }

    /**
     * Obtém o valor da propriedade locationRefnum.
     * 
     * @return
     *     possible object is
     *     {@link LocationRefnumType }
     *     
     */
    public LocationRefnumType getLocationRefnum() {
        return locationRefnum;
    }

    /**
     * Define o valor da propriedade locationRefnum.
     * 
     * @param value
     *     allowed object is
     *     {@link LocationRefnumType }
     *     
     */
    public void setLocationRefnum(LocationRefnumType value) {
        this.locationRefnum = value;
    }

    /**
     * Gets the value of the stopRefnum property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the stopRefnum property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getStopRefnum().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link StopRefnumType }
     * 
     * 
     */
    public List<StopRefnumType> getStopRefnum() {
        if (stopRefnum == null) {
            stopRefnum = new ArrayList<StopRefnumType>();
        }
        return this.stopRefnum;
    }

}
