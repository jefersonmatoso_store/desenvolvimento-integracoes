
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;


/**
 * A Contact Group represents a list of contacts used for notification.
 * 
 * <p>Classe Java de ContactGroupType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="ContactGroupType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OTMTransactionIn">
 *       &lt;sequence>
 *         &lt;element name="ContactGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="TransactionCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransactionCodeType" minOccurs="0"/>
 *         &lt;element name="ReplaceChildren" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ReplaceChildrenType" minOccurs="0"/>
 *         &lt;element name="LanguageSpoken" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsBroadcast" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="GroupSavedQueryGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="CommunicationMethod" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}CommunicationMethodType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Preference" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}PreferenceType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="IncludeContact" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;choice>
 *                   &lt;element name="ContactGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" maxOccurs="unbounded" minOccurs="0"/>
 *                   &lt;element name="Contact" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ContactType" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/choice>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ContactGroupType", propOrder = {
    "contactGid",
    "transactionCode",
    "replaceChildren",
    "languageSpoken",
    "isBroadcast",
    "groupSavedQueryGid",
    "communicationMethod",
    "preference",
    "includeContact"
})
public class ContactGroupType
    extends OTMTransactionIn
{

    @XmlElement(name = "ContactGid", required = true)
    protected GLogXMLGidType contactGid;
    @XmlElement(name = "TransactionCode")
    @XmlSchemaType(name = "string")
    protected TransactionCodeType transactionCode;
    @XmlElement(name = "ReplaceChildren")
    protected ReplaceChildrenType replaceChildren;
    @XmlElement(name = "LanguageSpoken")
    protected String languageSpoken;
    @XmlElement(name = "IsBroadcast")
    protected String isBroadcast;
    @XmlElement(name = "GroupSavedQueryGid")
    protected GLogXMLGidType groupSavedQueryGid;
    @XmlElement(name = "CommunicationMethod")
    protected List<CommunicationMethodType> communicationMethod;
    @XmlElement(name = "Preference")
    protected List<PreferenceType> preference;
    @XmlElement(name = "IncludeContact")
    protected ContactGroupType.IncludeContact includeContact;

    /**
     * Obtém o valor da propriedade contactGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getContactGid() {
        return contactGid;
    }

    /**
     * Define o valor da propriedade contactGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setContactGid(GLogXMLGidType value) {
        this.contactGid = value;
    }

    /**
     * Obtém o valor da propriedade transactionCode.
     * 
     * @return
     *     possible object is
     *     {@link TransactionCodeType }
     *     
     */
    public TransactionCodeType getTransactionCode() {
        return transactionCode;
    }

    /**
     * Define o valor da propriedade transactionCode.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionCodeType }
     *     
     */
    public void setTransactionCode(TransactionCodeType value) {
        this.transactionCode = value;
    }

    /**
     * Obtém o valor da propriedade replaceChildren.
     * 
     * @return
     *     possible object is
     *     {@link ReplaceChildrenType }
     *     
     */
    public ReplaceChildrenType getReplaceChildren() {
        return replaceChildren;
    }

    /**
     * Define o valor da propriedade replaceChildren.
     * 
     * @param value
     *     allowed object is
     *     {@link ReplaceChildrenType }
     *     
     */
    public void setReplaceChildren(ReplaceChildrenType value) {
        this.replaceChildren = value;
    }

    /**
     * Obtém o valor da propriedade languageSpoken.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLanguageSpoken() {
        return languageSpoken;
    }

    /**
     * Define o valor da propriedade languageSpoken.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLanguageSpoken(String value) {
        this.languageSpoken = value;
    }

    /**
     * Obtém o valor da propriedade isBroadcast.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsBroadcast() {
        return isBroadcast;
    }

    /**
     * Define o valor da propriedade isBroadcast.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsBroadcast(String value) {
        this.isBroadcast = value;
    }

    /**
     * Obtém o valor da propriedade groupSavedQueryGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getGroupSavedQueryGid() {
        return groupSavedQueryGid;
    }

    /**
     * Define o valor da propriedade groupSavedQueryGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setGroupSavedQueryGid(GLogXMLGidType value) {
        this.groupSavedQueryGid = value;
    }

    /**
     * Gets the value of the communicationMethod property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the communicationMethod property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCommunicationMethod().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CommunicationMethodType }
     * 
     * 
     */
    public List<CommunicationMethodType> getCommunicationMethod() {
        if (communicationMethod == null) {
            communicationMethod = new ArrayList<CommunicationMethodType>();
        }
        return this.communicationMethod;
    }

    /**
     * Gets the value of the preference property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the preference property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPreference().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PreferenceType }
     * 
     * 
     */
    public List<PreferenceType> getPreference() {
        if (preference == null) {
            preference = new ArrayList<PreferenceType>();
        }
        return this.preference;
    }

    /**
     * Obtém o valor da propriedade includeContact.
     * 
     * @return
     *     possible object is
     *     {@link ContactGroupType.IncludeContact }
     *     
     */
    public ContactGroupType.IncludeContact getIncludeContact() {
        return includeContact;
    }

    /**
     * Define o valor da propriedade includeContact.
     * 
     * @param value
     *     allowed object is
     *     {@link ContactGroupType.IncludeContact }
     *     
     */
    public void setIncludeContact(ContactGroupType.IncludeContact value) {
        this.includeContact = value;
    }


    /**
     * <p>Classe Java de anonymous complex type.
     * 
     * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;choice>
     *         &lt;element name="ContactGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" maxOccurs="unbounded" minOccurs="0"/>
     *         &lt;element name="Contact" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ContactType" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/choice>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "contactGid",
        "contact"
    })
    public static class IncludeContact {

        @XmlElement(name = "ContactGid")
        protected List<GLogXMLGidType> contactGid;
        @XmlElement(name = "Contact")
        protected List<ContactType> contact;

        /**
         * Gets the value of the contactGid property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the contactGid property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getContactGid().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link GLogXMLGidType }
         * 
         * 
         */
        public List<GLogXMLGidType> getContactGid() {
            if (contactGid == null) {
                contactGid = new ArrayList<GLogXMLGidType>();
            }
            return this.contactGid;
        }

        /**
         * Gets the value of the contact property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the contact property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getContact().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link ContactType }
         * 
         * 
         */
        public List<ContactType> getContact() {
            if (contact == null) {
                contact = new ArrayList<ContactType>();
            }
            return this.contact;
        }

    }

}
