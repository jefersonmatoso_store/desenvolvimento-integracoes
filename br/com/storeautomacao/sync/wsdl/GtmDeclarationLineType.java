
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;


/**
 * 
 *             (Both) Gtm Declaration information for an line item. Element ProductClassification
 *             is now supported both for inbound and outbound. ControlScreeningResult, ReportingResult
 *             RestrictedPartyScreeningResult, GtmTrItemStructureGid would be supported for outbound only.
 *             GtmDeclarationGid is supported only for outbound intiated on declaration line.
 *          
 * 
 * <p>Classe Java de GtmDeclarationLineType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="GtmDeclarationLineType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GtmDeclarationLineGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="IntSavedQuery" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}IntSavedQueryType" minOccurs="0"/>
 *         &lt;element name="TransactionCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransactionCodeType" minOccurs="0"/>
 *         &lt;element name="GtmDeclarationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="ItemRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ItemRefType" minOccurs="0"/>
 *         &lt;element name="PrecedingTransactionLineGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="ParentDeclarationLineGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="CountryOfOrigin" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}CountryCodeType" minOccurs="0"/>
 *         &lt;element name="ManufacturingCountry" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}CountryCodeType" minOccurs="0"/>
 *         &lt;element name="Remark" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RemarkType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="InvolvedParty" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}GtmInvolvedPartyType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Refnum" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RefnumType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="ProductClassification" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}ProductClassificationType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="UserDefinedClassification" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}UserDefinedClassificationType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="ControlScreeningResult" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}ControlScreeningResultType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="ReportingResult" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}ReportingResultType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="RestrictedPartyScreeningResult" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}RestrictedPartyScreeningResultType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Quantity" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}QuantityType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="ReportingQuantity" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}ReportingQuantityType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Currency" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}CurrencyType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Percentage" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}PercentageType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Status" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}StatusType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="GtmTransLineLicense" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}GtmTransLineLicenseType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="GtmTransactionLineRequiredDocument" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}GtmTransactionLineRequiredDocumentType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="GtmTransactionLineRequiredText" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}RequiredTextType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="CalculatedValueResult" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}CalculatedValueResultType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="SanctionControlScreeningResult" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}SanctionControlScreeningResultType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="FlexFieldStrings" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldStringType" minOccurs="0"/>
 *         &lt;element name="FlexFieldNumbers" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldNumberType" minOccurs="0"/>
 *         &lt;element name="FlexFieldDates" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldDateType" minOccurs="0"/>
 *         &lt;element name="GtmTrItemStructureGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="IncoTermGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="IncoTermLocation" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LineDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ValuationMethod" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}ValuationMethodType" minOccurs="0"/>
 *         &lt;element name="TariffPreferenceType" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}TariffPreferenceTypeType" minOccurs="0"/>
 *         &lt;element name="Procedure" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}ProcedureType" minOccurs="0"/>
 *         &lt;element name="PriorProcedure" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}ProcedureType" minOccurs="0"/>
 *         &lt;element name="ProcedureDetail" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}ProcedureDetailType" minOccurs="0"/>
 *         &lt;element name="GtmBond" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}GtmBondType" minOccurs="0"/>
 *         &lt;element name="AdditionalInfo" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}AdditionalInfoType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="ProducedDocument" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}ProducedDocumentType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="TransLineDate" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}TransDateType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="OtherControlScreeningResult" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}ControlScreeningResultGenericType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GtmDeclarationLineType", namespace = "http://xmlns.oracle.com/apps/gtm/transmission/v6.4", propOrder = {
    "gtmDeclarationLineGid",
    "intSavedQuery",
    "transactionCode",
    "gtmDeclarationGid",
    "itemRef",
    "precedingTransactionLineGid",
    "parentDeclarationLineGid",
    "countryOfOrigin",
    "manufacturingCountry",
    "remark",
    "involvedParty",
    "refnum",
    "productClassification",
    "userDefinedClassification",
    "controlScreeningResult",
    "reportingResult",
    "restrictedPartyScreeningResult",
    "quantity",
    "reportingQuantity",
    "currency",
    "percentage",
    "status",
    "gtmTransLineLicense",
    "gtmTransactionLineRequiredDocument",
    "gtmTransactionLineRequiredText",
    "calculatedValueResult",
    "sanctionControlScreeningResult",
    "flexFieldStrings",
    "flexFieldNumbers",
    "flexFieldDates",
    "gtmTrItemStructureGid",
    "incoTermGid",
    "incoTermLocation",
    "lineDescription",
    "valuationMethod",
    "tariffPreferenceType",
    "procedure",
    "priorProcedure",
    "procedureDetail",
    "gtmBond",
    "additionalInfo",
    "producedDocument",
    "transLineDate",
    "otherControlScreeningResult"
})
public class GtmDeclarationLineType {

    @XmlElement(name = "GtmDeclarationLineGid")
    protected GLogXMLGidType gtmDeclarationLineGid;
    @XmlElement(name = "IntSavedQuery")
    protected IntSavedQueryType intSavedQuery;
    @XmlElement(name = "TransactionCode")
    @XmlSchemaType(name = "string")
    protected TransactionCodeType transactionCode;
    @XmlElement(name = "GtmDeclarationGid")
    protected GLogXMLGidType gtmDeclarationGid;
    @XmlElement(name = "ItemRef")
    protected ItemRefType itemRef;
    @XmlElement(name = "PrecedingTransactionLineGid")
    protected GLogXMLGidType precedingTransactionLineGid;
    @XmlElement(name = "ParentDeclarationLineGid")
    protected GLogXMLGidType parentDeclarationLineGid;
    @XmlElement(name = "CountryOfOrigin")
    protected CountryCodeType countryOfOrigin;
    @XmlElement(name = "ManufacturingCountry")
    protected CountryCodeType manufacturingCountry;
    @XmlElement(name = "Remark")
    protected List<RemarkType> remark;
    @XmlElement(name = "InvolvedParty")
    protected List<GtmInvolvedPartyType> involvedParty;
    @XmlElement(name = "Refnum")
    protected List<RefnumType> refnum;
    @XmlElement(name = "ProductClassification")
    protected List<ProductClassificationType> productClassification;
    @XmlElement(name = "UserDefinedClassification")
    protected List<UserDefinedClassificationType> userDefinedClassification;
    @XmlElement(name = "ControlScreeningResult")
    protected List<ControlScreeningResultType> controlScreeningResult;
    @XmlElement(name = "ReportingResult")
    protected List<ReportingResultType> reportingResult;
    @XmlElement(name = "RestrictedPartyScreeningResult")
    protected List<RestrictedPartyScreeningResultType> restrictedPartyScreeningResult;
    @XmlElement(name = "Quantity")
    protected List<QuantityType> quantity;
    @XmlElement(name = "ReportingQuantity")
    protected List<ReportingQuantityType> reportingQuantity;
    @XmlElement(name = "Currency")
    protected List<CurrencyType> currency;
    @XmlElement(name = "Percentage")
    protected List<PercentageType> percentage;
    @XmlElement(name = "Status")
    protected List<StatusType> status;
    @XmlElement(name = "GtmTransLineLicense")
    protected List<GtmTransLineLicenseType> gtmTransLineLicense;
    @XmlElement(name = "GtmTransactionLineRequiredDocument")
    protected List<GtmTransactionLineRequiredDocumentType> gtmTransactionLineRequiredDocument;
    @XmlElement(name = "GtmTransactionLineRequiredText")
    protected List<RequiredTextType> gtmTransactionLineRequiredText;
    @XmlElement(name = "CalculatedValueResult")
    protected List<CalculatedValueResultType> calculatedValueResult;
    @XmlElement(name = "SanctionControlScreeningResult")
    protected List<SanctionControlScreeningResultType> sanctionControlScreeningResult;
    @XmlElement(name = "FlexFieldStrings")
    protected FlexFieldStringType flexFieldStrings;
    @XmlElement(name = "FlexFieldNumbers")
    protected FlexFieldNumberType flexFieldNumbers;
    @XmlElement(name = "FlexFieldDates")
    protected FlexFieldDateType flexFieldDates;
    @XmlElement(name = "GtmTrItemStructureGid")
    protected GLogXMLGidType gtmTrItemStructureGid;
    @XmlElement(name = "IncoTermGid")
    protected GLogXMLGidType incoTermGid;
    @XmlElement(name = "IncoTermLocation")
    protected String incoTermLocation;
    @XmlElement(name = "LineDescription")
    protected String lineDescription;
    @XmlElement(name = "ValuationMethod")
    protected ValuationMethodType valuationMethod;
    @XmlElement(name = "TariffPreferenceType")
    protected TariffPreferenceTypeType tariffPreferenceType;
    @XmlElement(name = "Procedure")
    protected ProcedureType procedure;
    @XmlElement(name = "PriorProcedure")
    protected ProcedureType priorProcedure;
    @XmlElement(name = "ProcedureDetail")
    protected ProcedureDetailType procedureDetail;
    @XmlElement(name = "GtmBond")
    protected GtmBondType gtmBond;
    @XmlElement(name = "AdditionalInfo")
    protected List<AdditionalInfoType> additionalInfo;
    @XmlElement(name = "ProducedDocument")
    protected List<ProducedDocumentType> producedDocument;
    @XmlElement(name = "TransLineDate")
    protected List<TransDateType> transLineDate;
    @XmlElement(name = "OtherControlScreeningResult")
    protected List<ControlScreeningResultGenericType> otherControlScreeningResult;

    /**
     * Obtém o valor da propriedade gtmDeclarationLineGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getGtmDeclarationLineGid() {
        return gtmDeclarationLineGid;
    }

    /**
     * Define o valor da propriedade gtmDeclarationLineGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setGtmDeclarationLineGid(GLogXMLGidType value) {
        this.gtmDeclarationLineGid = value;
    }

    /**
     * Obtém o valor da propriedade intSavedQuery.
     * 
     * @return
     *     possible object is
     *     {@link IntSavedQueryType }
     *     
     */
    public IntSavedQueryType getIntSavedQuery() {
        return intSavedQuery;
    }

    /**
     * Define o valor da propriedade intSavedQuery.
     * 
     * @param value
     *     allowed object is
     *     {@link IntSavedQueryType }
     *     
     */
    public void setIntSavedQuery(IntSavedQueryType value) {
        this.intSavedQuery = value;
    }

    /**
     * Obtém o valor da propriedade transactionCode.
     * 
     * @return
     *     possible object is
     *     {@link TransactionCodeType }
     *     
     */
    public TransactionCodeType getTransactionCode() {
        return transactionCode;
    }

    /**
     * Define o valor da propriedade transactionCode.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionCodeType }
     *     
     */
    public void setTransactionCode(TransactionCodeType value) {
        this.transactionCode = value;
    }

    /**
     * Obtém o valor da propriedade gtmDeclarationGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getGtmDeclarationGid() {
        return gtmDeclarationGid;
    }

    /**
     * Define o valor da propriedade gtmDeclarationGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setGtmDeclarationGid(GLogXMLGidType value) {
        this.gtmDeclarationGid = value;
    }

    /**
     * Obtém o valor da propriedade itemRef.
     * 
     * @return
     *     possible object is
     *     {@link ItemRefType }
     *     
     */
    public ItemRefType getItemRef() {
        return itemRef;
    }

    /**
     * Define o valor da propriedade itemRef.
     * 
     * @param value
     *     allowed object is
     *     {@link ItemRefType }
     *     
     */
    public void setItemRef(ItemRefType value) {
        this.itemRef = value;
    }

    /**
     * Obtém o valor da propriedade precedingTransactionLineGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getPrecedingTransactionLineGid() {
        return precedingTransactionLineGid;
    }

    /**
     * Define o valor da propriedade precedingTransactionLineGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setPrecedingTransactionLineGid(GLogXMLGidType value) {
        this.precedingTransactionLineGid = value;
    }

    /**
     * Obtém o valor da propriedade parentDeclarationLineGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getParentDeclarationLineGid() {
        return parentDeclarationLineGid;
    }

    /**
     * Define o valor da propriedade parentDeclarationLineGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setParentDeclarationLineGid(GLogXMLGidType value) {
        this.parentDeclarationLineGid = value;
    }

    /**
     * Obtém o valor da propriedade countryOfOrigin.
     * 
     * @return
     *     possible object is
     *     {@link CountryCodeType }
     *     
     */
    public CountryCodeType getCountryOfOrigin() {
        return countryOfOrigin;
    }

    /**
     * Define o valor da propriedade countryOfOrigin.
     * 
     * @param value
     *     allowed object is
     *     {@link CountryCodeType }
     *     
     */
    public void setCountryOfOrigin(CountryCodeType value) {
        this.countryOfOrigin = value;
    }

    /**
     * Obtém o valor da propriedade manufacturingCountry.
     * 
     * @return
     *     possible object is
     *     {@link CountryCodeType }
     *     
     */
    public CountryCodeType getManufacturingCountry() {
        return manufacturingCountry;
    }

    /**
     * Define o valor da propriedade manufacturingCountry.
     * 
     * @param value
     *     allowed object is
     *     {@link CountryCodeType }
     *     
     */
    public void setManufacturingCountry(CountryCodeType value) {
        this.manufacturingCountry = value;
    }

    /**
     * Gets the value of the remark property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the remark property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRemark().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RemarkType }
     * 
     * 
     */
    public List<RemarkType> getRemark() {
        if (remark == null) {
            remark = new ArrayList<RemarkType>();
        }
        return this.remark;
    }

    /**
     * Gets the value of the involvedParty property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the involvedParty property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInvolvedParty().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GtmInvolvedPartyType }
     * 
     * 
     */
    public List<GtmInvolvedPartyType> getInvolvedParty() {
        if (involvedParty == null) {
            involvedParty = new ArrayList<GtmInvolvedPartyType>();
        }
        return this.involvedParty;
    }

    /**
     * Gets the value of the refnum property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the refnum property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRefnum().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RefnumType }
     * 
     * 
     */
    public List<RefnumType> getRefnum() {
        if (refnum == null) {
            refnum = new ArrayList<RefnumType>();
        }
        return this.refnum;
    }

    /**
     * Gets the value of the productClassification property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the productClassification property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getProductClassification().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProductClassificationType }
     * 
     * 
     */
    public List<ProductClassificationType> getProductClassification() {
        if (productClassification == null) {
            productClassification = new ArrayList<ProductClassificationType>();
        }
        return this.productClassification;
    }

    /**
     * Gets the value of the userDefinedClassification property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the userDefinedClassification property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getUserDefinedClassification().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link UserDefinedClassificationType }
     * 
     * 
     */
    public List<UserDefinedClassificationType> getUserDefinedClassification() {
        if (userDefinedClassification == null) {
            userDefinedClassification = new ArrayList<UserDefinedClassificationType>();
        }
        return this.userDefinedClassification;
    }

    /**
     * Gets the value of the controlScreeningResult property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the controlScreeningResult property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getControlScreeningResult().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ControlScreeningResultType }
     * 
     * 
     */
    public List<ControlScreeningResultType> getControlScreeningResult() {
        if (controlScreeningResult == null) {
            controlScreeningResult = new ArrayList<ControlScreeningResultType>();
        }
        return this.controlScreeningResult;
    }

    /**
     * Gets the value of the reportingResult property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the reportingResult property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReportingResult().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ReportingResultType }
     * 
     * 
     */
    public List<ReportingResultType> getReportingResult() {
        if (reportingResult == null) {
            reportingResult = new ArrayList<ReportingResultType>();
        }
        return this.reportingResult;
    }

    /**
     * Gets the value of the restrictedPartyScreeningResult property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the restrictedPartyScreeningResult property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRestrictedPartyScreeningResult().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RestrictedPartyScreeningResultType }
     * 
     * 
     */
    public List<RestrictedPartyScreeningResultType> getRestrictedPartyScreeningResult() {
        if (restrictedPartyScreeningResult == null) {
            restrictedPartyScreeningResult = new ArrayList<RestrictedPartyScreeningResultType>();
        }
        return this.restrictedPartyScreeningResult;
    }

    /**
     * Gets the value of the quantity property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the quantity property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getQuantity().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link QuantityType }
     * 
     * 
     */
    public List<QuantityType> getQuantity() {
        if (quantity == null) {
            quantity = new ArrayList<QuantityType>();
        }
        return this.quantity;
    }

    /**
     * Gets the value of the reportingQuantity property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the reportingQuantity property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReportingQuantity().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ReportingQuantityType }
     * 
     * 
     */
    public List<ReportingQuantityType> getReportingQuantity() {
        if (reportingQuantity == null) {
            reportingQuantity = new ArrayList<ReportingQuantityType>();
        }
        return this.reportingQuantity;
    }

    /**
     * Gets the value of the currency property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the currency property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCurrency().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CurrencyType }
     * 
     * 
     */
    public List<CurrencyType> getCurrency() {
        if (currency == null) {
            currency = new ArrayList<CurrencyType>();
        }
        return this.currency;
    }

    /**
     * Gets the value of the percentage property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the percentage property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPercentage().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PercentageType }
     * 
     * 
     */
    public List<PercentageType> getPercentage() {
        if (percentage == null) {
            percentage = new ArrayList<PercentageType>();
        }
        return this.percentage;
    }

    /**
     * Gets the value of the status property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the status property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getStatus().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link StatusType }
     * 
     * 
     */
    public List<StatusType> getStatus() {
        if (status == null) {
            status = new ArrayList<StatusType>();
        }
        return this.status;
    }

    /**
     * Gets the value of the gtmTransLineLicense property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the gtmTransLineLicense property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGtmTransLineLicense().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GtmTransLineLicenseType }
     * 
     * 
     */
    public List<GtmTransLineLicenseType> getGtmTransLineLicense() {
        if (gtmTransLineLicense == null) {
            gtmTransLineLicense = new ArrayList<GtmTransLineLicenseType>();
        }
        return this.gtmTransLineLicense;
    }

    /**
     * Gets the value of the gtmTransactionLineRequiredDocument property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the gtmTransactionLineRequiredDocument property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGtmTransactionLineRequiredDocument().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GtmTransactionLineRequiredDocumentType }
     * 
     * 
     */
    public List<GtmTransactionLineRequiredDocumentType> getGtmTransactionLineRequiredDocument() {
        if (gtmTransactionLineRequiredDocument == null) {
            gtmTransactionLineRequiredDocument = new ArrayList<GtmTransactionLineRequiredDocumentType>();
        }
        return this.gtmTransactionLineRequiredDocument;
    }

    /**
     * Gets the value of the gtmTransactionLineRequiredText property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the gtmTransactionLineRequiredText property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGtmTransactionLineRequiredText().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RequiredTextType }
     * 
     * 
     */
    public List<RequiredTextType> getGtmTransactionLineRequiredText() {
        if (gtmTransactionLineRequiredText == null) {
            gtmTransactionLineRequiredText = new ArrayList<RequiredTextType>();
        }
        return this.gtmTransactionLineRequiredText;
    }

    /**
     * Gets the value of the calculatedValueResult property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the calculatedValueResult property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCalculatedValueResult().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CalculatedValueResultType }
     * 
     * 
     */
    public List<CalculatedValueResultType> getCalculatedValueResult() {
        if (calculatedValueResult == null) {
            calculatedValueResult = new ArrayList<CalculatedValueResultType>();
        }
        return this.calculatedValueResult;
    }

    /**
     * Gets the value of the sanctionControlScreeningResult property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the sanctionControlScreeningResult property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSanctionControlScreeningResult().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SanctionControlScreeningResultType }
     * 
     * 
     */
    public List<SanctionControlScreeningResultType> getSanctionControlScreeningResult() {
        if (sanctionControlScreeningResult == null) {
            sanctionControlScreeningResult = new ArrayList<SanctionControlScreeningResultType>();
        }
        return this.sanctionControlScreeningResult;
    }

    /**
     * Obtém o valor da propriedade flexFieldStrings.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldStringType }
     *     
     */
    public FlexFieldStringType getFlexFieldStrings() {
        return flexFieldStrings;
    }

    /**
     * Define o valor da propriedade flexFieldStrings.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldStringType }
     *     
     */
    public void setFlexFieldStrings(FlexFieldStringType value) {
        this.flexFieldStrings = value;
    }

    /**
     * Obtém o valor da propriedade flexFieldNumbers.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldNumberType }
     *     
     */
    public FlexFieldNumberType getFlexFieldNumbers() {
        return flexFieldNumbers;
    }

    /**
     * Define o valor da propriedade flexFieldNumbers.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldNumberType }
     *     
     */
    public void setFlexFieldNumbers(FlexFieldNumberType value) {
        this.flexFieldNumbers = value;
    }

    /**
     * Obtém o valor da propriedade flexFieldDates.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldDateType }
     *     
     */
    public FlexFieldDateType getFlexFieldDates() {
        return flexFieldDates;
    }

    /**
     * Define o valor da propriedade flexFieldDates.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldDateType }
     *     
     */
    public void setFlexFieldDates(FlexFieldDateType value) {
        this.flexFieldDates = value;
    }

    /**
     * Obtém o valor da propriedade gtmTrItemStructureGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getGtmTrItemStructureGid() {
        return gtmTrItemStructureGid;
    }

    /**
     * Define o valor da propriedade gtmTrItemStructureGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setGtmTrItemStructureGid(GLogXMLGidType value) {
        this.gtmTrItemStructureGid = value;
    }

    /**
     * Obtém o valor da propriedade incoTermGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getIncoTermGid() {
        return incoTermGid;
    }

    /**
     * Define o valor da propriedade incoTermGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setIncoTermGid(GLogXMLGidType value) {
        this.incoTermGid = value;
    }

    /**
     * Obtém o valor da propriedade incoTermLocation.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIncoTermLocation() {
        return incoTermLocation;
    }

    /**
     * Define o valor da propriedade incoTermLocation.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIncoTermLocation(String value) {
        this.incoTermLocation = value;
    }

    /**
     * Obtém o valor da propriedade lineDescription.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLineDescription() {
        return lineDescription;
    }

    /**
     * Define o valor da propriedade lineDescription.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLineDescription(String value) {
        this.lineDescription = value;
    }

    /**
     * Obtém o valor da propriedade valuationMethod.
     * 
     * @return
     *     possible object is
     *     {@link ValuationMethodType }
     *     
     */
    public ValuationMethodType getValuationMethod() {
        return valuationMethod;
    }

    /**
     * Define o valor da propriedade valuationMethod.
     * 
     * @param value
     *     allowed object is
     *     {@link ValuationMethodType }
     *     
     */
    public void setValuationMethod(ValuationMethodType value) {
        this.valuationMethod = value;
    }

    /**
     * Obtém o valor da propriedade tariffPreferenceType.
     * 
     * @return
     *     possible object is
     *     {@link TariffPreferenceTypeType }
     *     
     */
    public TariffPreferenceTypeType getTariffPreferenceType() {
        return tariffPreferenceType;
    }

    /**
     * Define o valor da propriedade tariffPreferenceType.
     * 
     * @param value
     *     allowed object is
     *     {@link TariffPreferenceTypeType }
     *     
     */
    public void setTariffPreferenceType(TariffPreferenceTypeType value) {
        this.tariffPreferenceType = value;
    }

    /**
     * Obtém o valor da propriedade procedure.
     * 
     * @return
     *     possible object is
     *     {@link ProcedureType }
     *     
     */
    public ProcedureType getProcedure() {
        return procedure;
    }

    /**
     * Define o valor da propriedade procedure.
     * 
     * @param value
     *     allowed object is
     *     {@link ProcedureType }
     *     
     */
    public void setProcedure(ProcedureType value) {
        this.procedure = value;
    }

    /**
     * Obtém o valor da propriedade priorProcedure.
     * 
     * @return
     *     possible object is
     *     {@link ProcedureType }
     *     
     */
    public ProcedureType getPriorProcedure() {
        return priorProcedure;
    }

    /**
     * Define o valor da propriedade priorProcedure.
     * 
     * @param value
     *     allowed object is
     *     {@link ProcedureType }
     *     
     */
    public void setPriorProcedure(ProcedureType value) {
        this.priorProcedure = value;
    }

    /**
     * Obtém o valor da propriedade procedureDetail.
     * 
     * @return
     *     possible object is
     *     {@link ProcedureDetailType }
     *     
     */
    public ProcedureDetailType getProcedureDetail() {
        return procedureDetail;
    }

    /**
     * Define o valor da propriedade procedureDetail.
     * 
     * @param value
     *     allowed object is
     *     {@link ProcedureDetailType }
     *     
     */
    public void setProcedureDetail(ProcedureDetailType value) {
        this.procedureDetail = value;
    }

    /**
     * Obtém o valor da propriedade gtmBond.
     * 
     * @return
     *     possible object is
     *     {@link GtmBondType }
     *     
     */
    public GtmBondType getGtmBond() {
        return gtmBond;
    }

    /**
     * Define o valor da propriedade gtmBond.
     * 
     * @param value
     *     allowed object is
     *     {@link GtmBondType }
     *     
     */
    public void setGtmBond(GtmBondType value) {
        this.gtmBond = value;
    }

    /**
     * Gets the value of the additionalInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the additionalInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAdditionalInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AdditionalInfoType }
     * 
     * 
     */
    public List<AdditionalInfoType> getAdditionalInfo() {
        if (additionalInfo == null) {
            additionalInfo = new ArrayList<AdditionalInfoType>();
        }
        return this.additionalInfo;
    }

    /**
     * Gets the value of the producedDocument property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the producedDocument property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getProducedDocument().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProducedDocumentType }
     * 
     * 
     */
    public List<ProducedDocumentType> getProducedDocument() {
        if (producedDocument == null) {
            producedDocument = new ArrayList<ProducedDocumentType>();
        }
        return this.producedDocument;
    }

    /**
     * Gets the value of the transLineDate property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the transLineDate property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTransLineDate().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TransDateType }
     * 
     * 
     */
    public List<TransDateType> getTransLineDate() {
        if (transLineDate == null) {
            transLineDate = new ArrayList<TransDateType>();
        }
        return this.transLineDate;
    }

    /**
     * Gets the value of the otherControlScreeningResult property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the otherControlScreeningResult property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOtherControlScreeningResult().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ControlScreeningResultGenericType }
     * 
     * 
     */
    public List<ControlScreeningResultGenericType> getOtherControlScreeningResult() {
        if (otherControlScreeningResult == null) {
            otherControlScreeningResult = new ArrayList<ControlScreeningResultGenericType>();
        }
        return this.otherControlScreeningResult;
    }

}
