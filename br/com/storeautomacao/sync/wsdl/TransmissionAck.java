
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Classe Java de anonymous complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransmissionDocumentType">
 *       &lt;sequence>
 *         &lt;element name="EchoedTransmissionHeader">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="TransmissionHeader" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransmissionHeaderType"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="TransmissionAckStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TransmissionAckReason" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="StackTrace" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TransmissionText" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Refnum" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RefnumType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="QueryResultInfo" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="GLogXMLElement" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLElementType" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *       &lt;attribute name="Version" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "echoedTransmissionHeader",
    "transmissionAckStatus",
    "transmissionAckReason",
    "stackTrace",
    "transmissionText",
    "refnum",
    "queryResultInfo"
})
@XmlRootElement(name = "TransmissionAck")
public class TransmissionAck
    extends TransmissionDocumentType
{

    @XmlElement(name = "EchoedTransmissionHeader", required = true)
    protected TransmissionAck.EchoedTransmissionHeader echoedTransmissionHeader;
    @XmlElement(name = "TransmissionAckStatus")
    protected String transmissionAckStatus;
    @XmlElement(name = "TransmissionAckReason")
    protected String transmissionAckReason;
    @XmlElement(name = "StackTrace")
    protected String stackTrace;
    @XmlElement(name = "TransmissionText")
    protected String transmissionText;
    @XmlElement(name = "Refnum")
    protected List<RefnumType> refnum;
    @XmlElement(name = "QueryResultInfo")
    protected TransmissionAck.QueryResultInfo queryResultInfo;
    @XmlAttribute(name = "Version")
    protected String version;

    /**
     * Obtém o valor da propriedade echoedTransmissionHeader.
     * 
     * @return
     *     possible object is
     *     {@link TransmissionAck.EchoedTransmissionHeader }
     *     
     */
    public TransmissionAck.EchoedTransmissionHeader getEchoedTransmissionHeader() {
        return echoedTransmissionHeader;
    }

    /**
     * Define o valor da propriedade echoedTransmissionHeader.
     * 
     * @param value
     *     allowed object is
     *     {@link TransmissionAck.EchoedTransmissionHeader }
     *     
     */
    public void setEchoedTransmissionHeader(TransmissionAck.EchoedTransmissionHeader value) {
        this.echoedTransmissionHeader = value;
    }

    /**
     * Obtém o valor da propriedade transmissionAckStatus.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransmissionAckStatus() {
        return transmissionAckStatus;
    }

    /**
     * Define o valor da propriedade transmissionAckStatus.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransmissionAckStatus(String value) {
        this.transmissionAckStatus = value;
    }

    /**
     * Obtém o valor da propriedade transmissionAckReason.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransmissionAckReason() {
        return transmissionAckReason;
    }

    /**
     * Define o valor da propriedade transmissionAckReason.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransmissionAckReason(String value) {
        this.transmissionAckReason = value;
    }

    /**
     * Obtém o valor da propriedade stackTrace.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStackTrace() {
        return stackTrace;
    }

    /**
     * Define o valor da propriedade stackTrace.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStackTrace(String value) {
        this.stackTrace = value;
    }

    /**
     * Obtém o valor da propriedade transmissionText.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransmissionText() {
        return transmissionText;
    }

    /**
     * Define o valor da propriedade transmissionText.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransmissionText(String value) {
        this.transmissionText = value;
    }

    /**
     * Gets the value of the refnum property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the refnum property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRefnum().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RefnumType }
     * 
     * 
     */
    public List<RefnumType> getRefnum() {
        if (refnum == null) {
            refnum = new ArrayList<RefnumType>();
        }
        return this.refnum;
    }

    /**
     * Obtém o valor da propriedade queryResultInfo.
     * 
     * @return
     *     possible object is
     *     {@link TransmissionAck.QueryResultInfo }
     *     
     */
    public TransmissionAck.QueryResultInfo getQueryResultInfo() {
        return queryResultInfo;
    }

    /**
     * Define o valor da propriedade queryResultInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link TransmissionAck.QueryResultInfo }
     *     
     */
    public void setQueryResultInfo(TransmissionAck.QueryResultInfo value) {
        this.queryResultInfo = value;
    }

    /**
     * Obtém o valor da propriedade version.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVersion() {
        return version;
    }

    /**
     * Define o valor da propriedade version.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVersion(String value) {
        this.version = value;
    }


    /**
     * <p>Classe Java de anonymous complex type.
     * 
     * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="TransmissionHeader" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransmissionHeaderType"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "transmissionHeader"
    })
    public static class EchoedTransmissionHeader {

        @XmlElement(name = "TransmissionHeader", required = true)
        protected TransmissionHeaderType transmissionHeader;

        /**
         * Obtém o valor da propriedade transmissionHeader.
         * 
         * @return
         *     possible object is
         *     {@link TransmissionHeaderType }
         *     
         */
        public TransmissionHeaderType getTransmissionHeader() {
            return transmissionHeader;
        }

        /**
         * Define o valor da propriedade transmissionHeader.
         * 
         * @param value
         *     allowed object is
         *     {@link TransmissionHeaderType }
         *     
         */
        public void setTransmissionHeader(TransmissionHeaderType value) {
            this.transmissionHeader = value;
        }

    }


    /**
     * <p>Classe Java de anonymous complex type.
     * 
     * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="GLogXMLElement" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLElementType" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "gLogXMLElement"
    })
    public static class QueryResultInfo {

        @XmlElement(name = "GLogXMLElement")
        protected List<GLogXMLElementType> gLogXMLElement;

        /**
         * Gets the value of the gLogXMLElement property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the gLogXMLElement property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getGLogXMLElement().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link GLogXMLElementType }
         * 
         * 
         */
        public List<GLogXMLElementType> getGLogXMLElement() {
            if (gLogXMLElement == null) {
                gLogXMLElement = new ArrayList<GLogXMLElementType>();
            }
            return this.gLogXMLElement;
        }

    }

}
