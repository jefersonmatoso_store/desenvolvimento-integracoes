
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.*;


/**
 * 
 *             To specify a ShipmentGid, the Prev/NextObjectType would be set to 'S'.
 *          
 * 
 * <p>Classe Java de ShipmentLinkType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="ShipmentLinkType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OTMTransactionIn">
 *       &lt;sequence>
 *         &lt;element name="TransactionCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransactionCodeType"/>
 *         &lt;element name="PrevObjectType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PrevObjectGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="PrevShipmentStopNum" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="StopSequence" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="NextObjectType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="NextObjectGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="NextShipmentStopNum" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="StopSequence" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ShipmentLinkType", propOrder = {
    "transactionCode",
    "prevObjectType",
    "prevObjectGid",
    "prevShipmentStopNum",
    "nextObjectType",
    "nextObjectGid",
    "nextShipmentStopNum"
})
public class ShipmentLinkType
    extends OTMTransactionIn
{

    @XmlElement(name = "TransactionCode", required = true)
    @XmlSchemaType(name = "string")
    protected TransactionCodeType transactionCode;
    @XmlElement(name = "PrevObjectType", required = true)
    protected String prevObjectType;
    @XmlElement(name = "PrevObjectGid", required = true)
    protected GLogXMLGidType prevObjectGid;
    @XmlElement(name = "PrevShipmentStopNum")
    protected ShipmentLinkType.PrevShipmentStopNum prevShipmentStopNum;
    @XmlElement(name = "NextObjectType", required = true)
    protected String nextObjectType;
    @XmlElement(name = "NextObjectGid", required = true)
    protected GLogXMLGidType nextObjectGid;
    @XmlElement(name = "NextShipmentStopNum")
    protected ShipmentLinkType.NextShipmentStopNum nextShipmentStopNum;

    /**
     * Obtém o valor da propriedade transactionCode.
     * 
     * @return
     *     possible object is
     *     {@link TransactionCodeType }
     *     
     */
    public TransactionCodeType getTransactionCode() {
        return transactionCode;
    }

    /**
     * Define o valor da propriedade transactionCode.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionCodeType }
     *     
     */
    public void setTransactionCode(TransactionCodeType value) {
        this.transactionCode = value;
    }

    /**
     * Obtém o valor da propriedade prevObjectType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrevObjectType() {
        return prevObjectType;
    }

    /**
     * Define o valor da propriedade prevObjectType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrevObjectType(String value) {
        this.prevObjectType = value;
    }

    /**
     * Obtém o valor da propriedade prevObjectGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getPrevObjectGid() {
        return prevObjectGid;
    }

    /**
     * Define o valor da propriedade prevObjectGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setPrevObjectGid(GLogXMLGidType value) {
        this.prevObjectGid = value;
    }

    /**
     * Obtém o valor da propriedade prevShipmentStopNum.
     * 
     * @return
     *     possible object is
     *     {@link ShipmentLinkType.PrevShipmentStopNum }
     *     
     */
    public ShipmentLinkType.PrevShipmentStopNum getPrevShipmentStopNum() {
        return prevShipmentStopNum;
    }

    /**
     * Define o valor da propriedade prevShipmentStopNum.
     * 
     * @param value
     *     allowed object is
     *     {@link ShipmentLinkType.PrevShipmentStopNum }
     *     
     */
    public void setPrevShipmentStopNum(ShipmentLinkType.PrevShipmentStopNum value) {
        this.prevShipmentStopNum = value;
    }

    /**
     * Obtém o valor da propriedade nextObjectType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNextObjectType() {
        return nextObjectType;
    }

    /**
     * Define o valor da propriedade nextObjectType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNextObjectType(String value) {
        this.nextObjectType = value;
    }

    /**
     * Obtém o valor da propriedade nextObjectGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getNextObjectGid() {
        return nextObjectGid;
    }

    /**
     * Define o valor da propriedade nextObjectGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setNextObjectGid(GLogXMLGidType value) {
        this.nextObjectGid = value;
    }

    /**
     * Obtém o valor da propriedade nextShipmentStopNum.
     * 
     * @return
     *     possible object is
     *     {@link ShipmentLinkType.NextShipmentStopNum }
     *     
     */
    public ShipmentLinkType.NextShipmentStopNum getNextShipmentStopNum() {
        return nextShipmentStopNum;
    }

    /**
     * Define o valor da propriedade nextShipmentStopNum.
     * 
     * @param value
     *     allowed object is
     *     {@link ShipmentLinkType.NextShipmentStopNum }
     *     
     */
    public void setNextShipmentStopNum(ShipmentLinkType.NextShipmentStopNum value) {
        this.nextShipmentStopNum = value;
    }


    /**
     * <p>Classe Java de anonymous complex type.
     * 
     * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="StopSequence" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "stopSequence"
    })
    public static class NextShipmentStopNum {

        @XmlElement(name = "StopSequence", required = true)
        protected String stopSequence;

        /**
         * Obtém o valor da propriedade stopSequence.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getStopSequence() {
            return stopSequence;
        }

        /**
         * Define o valor da propriedade stopSequence.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setStopSequence(String value) {
            this.stopSequence = value;
        }

    }


    /**
     * <p>Classe Java de anonymous complex type.
     * 
     * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="StopSequence" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "stopSequence"
    })
    public static class PrevShipmentStopNum {

        @XmlElement(name = "StopSequence", required = true)
        protected String stopSequence;

        /**
         * Obtém o valor da propriedade stopSequence.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getStopSequence() {
            return stopSequence;
        }

        /**
         * Define o valor da propriedade stopSequence.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setStopSequence(String value) {
            this.stopSequence = value;
        }

    }

}
