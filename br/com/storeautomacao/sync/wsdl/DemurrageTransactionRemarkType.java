
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Demurrage Transaction Remark
 * 
 * <p>Classe Java de DemurrageTransactionRemarkType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="DemurrageTransactionRemarkType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DmTransactionRemarkQualGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="DmTransactionRemarkText" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DemurrageTransactionRemarkType", propOrder = {
    "dmTransactionRemarkQualGid",
    "dmTransactionRemarkText"
})
public class DemurrageTransactionRemarkType {

    @XmlElement(name = "DmTransactionRemarkQualGid")
    protected GLogXMLGidType dmTransactionRemarkQualGid;
    @XmlElement(name = "DmTransactionRemarkText")
    protected String dmTransactionRemarkText;

    /**
     * Obtém o valor da propriedade dmTransactionRemarkQualGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getDmTransactionRemarkQualGid() {
        return dmTransactionRemarkQualGid;
    }

    /**
     * Define o valor da propriedade dmTransactionRemarkQualGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setDmTransactionRemarkQualGid(GLogXMLGidType value) {
        this.dmTransactionRemarkQualGid = value;
    }

    /**
     * Obtém o valor da propriedade dmTransactionRemarkText.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDmTransactionRemarkText() {
        return dmTransactionRemarkText;
    }

    /**
     * Define o valor da propriedade dmTransactionRemarkText.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDmTransactionRemarkText(String value) {
        this.dmTransactionRemarkText = value;
    }

}
