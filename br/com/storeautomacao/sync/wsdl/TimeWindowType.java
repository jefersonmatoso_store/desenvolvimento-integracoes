
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * TimeWindow is a group of fields for specifying early/late pickup, early/late delivery, and pickup/delivery appointments.
 *             The PickupIsAppt and DeliveryIsAppt elements are only applicable when the TimeWindow is in the TransOrderLine,
 *             TransOrder.ShipUnitDetail.ShipUnit, and Release elements.
 *          
 * 
 * <p>Classe Java de TimeWindowType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="TimeWindowType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="EarlyPickupDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="LatePickupDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="EarlyDeliveryDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="LateDeliveryDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="PickupIsAppt" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DeliveryIsAppt" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TimeWindowType", propOrder = {
    "earlyPickupDt",
    "latePickupDt",
    "earlyDeliveryDt",
    "lateDeliveryDt",
    "pickupIsAppt",
    "deliveryIsAppt"
})
public class TimeWindowType {

    @XmlElement(name = "EarlyPickupDt")
    protected GLogDateTimeType earlyPickupDt;
    @XmlElement(name = "LatePickupDt")
    protected GLogDateTimeType latePickupDt;
    @XmlElement(name = "EarlyDeliveryDt")
    protected GLogDateTimeType earlyDeliveryDt;
    @XmlElement(name = "LateDeliveryDt")
    protected GLogDateTimeType lateDeliveryDt;
    @XmlElement(name = "PickupIsAppt")
    protected String pickupIsAppt;
    @XmlElement(name = "DeliveryIsAppt")
    protected String deliveryIsAppt;

    /**
     * Obtém o valor da propriedade earlyPickupDt.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getEarlyPickupDt() {
        return earlyPickupDt;
    }

    /**
     * Define o valor da propriedade earlyPickupDt.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setEarlyPickupDt(GLogDateTimeType value) {
        this.earlyPickupDt = value;
    }

    /**
     * Obtém o valor da propriedade latePickupDt.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getLatePickupDt() {
        return latePickupDt;
    }

    /**
     * Define o valor da propriedade latePickupDt.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setLatePickupDt(GLogDateTimeType value) {
        this.latePickupDt = value;
    }

    /**
     * Obtém o valor da propriedade earlyDeliveryDt.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getEarlyDeliveryDt() {
        return earlyDeliveryDt;
    }

    /**
     * Define o valor da propriedade earlyDeliveryDt.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setEarlyDeliveryDt(GLogDateTimeType value) {
        this.earlyDeliveryDt = value;
    }

    /**
     * Obtém o valor da propriedade lateDeliveryDt.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getLateDeliveryDt() {
        return lateDeliveryDt;
    }

    /**
     * Define o valor da propriedade lateDeliveryDt.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setLateDeliveryDt(GLogDateTimeType value) {
        this.lateDeliveryDt = value;
    }

    /**
     * Obtém o valor da propriedade pickupIsAppt.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPickupIsAppt() {
        return pickupIsAppt;
    }

    /**
     * Define o valor da propriedade pickupIsAppt.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPickupIsAppt(String value) {
        this.pickupIsAppt = value;
    }

    /**
     * Obtém o valor da propriedade deliveryIsAppt.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeliveryIsAppt() {
        return deliveryIsAppt;
    }

    /**
     * Define o valor da propriedade deliveryIsAppt.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeliveryIsAppt(String value) {
        this.deliveryIsAppt = value;
    }

}
