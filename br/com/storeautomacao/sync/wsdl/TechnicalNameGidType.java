
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Identifies a Hazmat technical name.
 * 
 * <p>Classe Java de TechnicalNameGidType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="TechnicalNameGidType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TechnicalNameGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TechnicalNameGidType", propOrder = {
    "technicalNameGid"
})
public class TechnicalNameGidType {

    @XmlElement(name = "TechnicalNameGid", required = true)
    protected GLogXMLGidType technicalNameGid;

    /**
     * Obtém o valor da propriedade technicalNameGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getTechnicalNameGid() {
        return technicalNameGid;
    }

    /**
     * Define o valor da propriedade technicalNameGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setTechnicalNameGid(GLogXMLGidType value) {
        this.technicalNameGid = value;
    }

}
