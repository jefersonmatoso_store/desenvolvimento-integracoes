
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * Indicates the special services required to for the shipment. Allows you to define remarks for why a special service applies to the associated
 *             shipment.
 *          
 * 
 * <p>Classe Java de ShipmentSpecialServiceType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="ShipmentSpecialServiceType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;choice>
 *           &lt;element name="SpclService" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}SpclServiceType"/>
 *           &lt;element name="SpecialServiceGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;/choice>
 *         &lt;element name="ShipSpclServiceSequence" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ProcessAsFlowThru" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AdjustmentReasonGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="CompletionState" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PayableIndicatorGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="BillableIndicatorGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="ActualOccurTime" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="IsPlanDurFixed" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PlannedDuration" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDurationType" minOccurs="0"/>
 *         &lt;element name="ActualDuration" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDurationType" minOccurs="0"/>
 *         &lt;element name="ActualDistance" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDistanceType" minOccurs="0"/>
 *         &lt;element name="ActualWeight" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLWeightType" minOccurs="0"/>
 *         &lt;element name="ActualVolume" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLVolumeType" minOccurs="0"/>
 *         &lt;element name="ActualShipUnitCount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ActualItemPackageCount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsSystemGenerated" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="StopSequence" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ShipSpclServiceRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ShipSpclServiceRefType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Remark" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RemarkType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ShipmentSpecialServiceType", propOrder = {
    "spclService",
    "specialServiceGid",
    "shipSpclServiceSequence",
    "processAsFlowThru",
    "adjustmentReasonGid",
    "completionState",
    "payableIndicatorGid",
    "billableIndicatorGid",
    "actualOccurTime",
    "isPlanDurFixed",
    "plannedDuration",
    "actualDuration",
    "actualDistance",
    "actualWeight",
    "actualVolume",
    "actualShipUnitCount",
    "actualItemPackageCount",
    "isSystemGenerated",
    "stopSequence",
    "shipSpclServiceRef",
    "remark"
})
public class ShipmentSpecialServiceType {

    @XmlElement(name = "SpclService")
    protected SpclServiceType spclService;
    @XmlElement(name = "SpecialServiceGid")
    protected GLogXMLGidType specialServiceGid;
    @XmlElement(name = "ShipSpclServiceSequence")
    protected String shipSpclServiceSequence;
    @XmlElement(name = "ProcessAsFlowThru")
    protected String processAsFlowThru;
    @XmlElement(name = "AdjustmentReasonGid")
    protected GLogXMLGidType adjustmentReasonGid;
    @XmlElement(name = "CompletionState")
    protected String completionState;
    @XmlElement(name = "PayableIndicatorGid")
    protected GLogXMLGidType payableIndicatorGid;
    @XmlElement(name = "BillableIndicatorGid")
    protected GLogXMLGidType billableIndicatorGid;
    @XmlElement(name = "ActualOccurTime")
    protected GLogDateTimeType actualOccurTime;
    @XmlElement(name = "IsPlanDurFixed")
    protected String isPlanDurFixed;
    @XmlElement(name = "PlannedDuration")
    protected GLogXMLDurationType plannedDuration;
    @XmlElement(name = "ActualDuration")
    protected GLogXMLDurationType actualDuration;
    @XmlElement(name = "ActualDistance")
    protected GLogXMLDistanceType actualDistance;
    @XmlElement(name = "ActualWeight")
    protected GLogXMLWeightType actualWeight;
    @XmlElement(name = "ActualVolume")
    protected GLogXMLVolumeType actualVolume;
    @XmlElement(name = "ActualShipUnitCount")
    protected String actualShipUnitCount;
    @XmlElement(name = "ActualItemPackageCount")
    protected String actualItemPackageCount;
    @XmlElement(name = "IsSystemGenerated")
    protected String isSystemGenerated;
    @XmlElement(name = "StopSequence")
    protected String stopSequence;
    @XmlElement(name = "ShipSpclServiceRef")
    protected List<ShipSpclServiceRefType> shipSpclServiceRef;
    @XmlElement(name = "Remark")
    protected List<RemarkType> remark;

    /**
     * Obtém o valor da propriedade spclService.
     * 
     * @return
     *     possible object is
     *     {@link SpclServiceType }
     *     
     */
    public SpclServiceType getSpclService() {
        return spclService;
    }

    /**
     * Define o valor da propriedade spclService.
     * 
     * @param value
     *     allowed object is
     *     {@link SpclServiceType }
     *     
     */
    public void setSpclService(SpclServiceType value) {
        this.spclService = value;
    }

    /**
     * Obtém o valor da propriedade specialServiceGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getSpecialServiceGid() {
        return specialServiceGid;
    }

    /**
     * Define o valor da propriedade specialServiceGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setSpecialServiceGid(GLogXMLGidType value) {
        this.specialServiceGid = value;
    }

    /**
     * Obtém o valor da propriedade shipSpclServiceSequence.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipSpclServiceSequence() {
        return shipSpclServiceSequence;
    }

    /**
     * Define o valor da propriedade shipSpclServiceSequence.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipSpclServiceSequence(String value) {
        this.shipSpclServiceSequence = value;
    }

    /**
     * Obtém o valor da propriedade processAsFlowThru.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProcessAsFlowThru() {
        return processAsFlowThru;
    }

    /**
     * Define o valor da propriedade processAsFlowThru.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProcessAsFlowThru(String value) {
        this.processAsFlowThru = value;
    }

    /**
     * Obtém o valor da propriedade adjustmentReasonGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getAdjustmentReasonGid() {
        return adjustmentReasonGid;
    }

    /**
     * Define o valor da propriedade adjustmentReasonGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setAdjustmentReasonGid(GLogXMLGidType value) {
        this.adjustmentReasonGid = value;
    }

    /**
     * Obtém o valor da propriedade completionState.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCompletionState() {
        return completionState;
    }

    /**
     * Define o valor da propriedade completionState.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCompletionState(String value) {
        this.completionState = value;
    }

    /**
     * Obtém o valor da propriedade payableIndicatorGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getPayableIndicatorGid() {
        return payableIndicatorGid;
    }

    /**
     * Define o valor da propriedade payableIndicatorGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setPayableIndicatorGid(GLogXMLGidType value) {
        this.payableIndicatorGid = value;
    }

    /**
     * Obtém o valor da propriedade billableIndicatorGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getBillableIndicatorGid() {
        return billableIndicatorGid;
    }

    /**
     * Define o valor da propriedade billableIndicatorGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setBillableIndicatorGid(GLogXMLGidType value) {
        this.billableIndicatorGid = value;
    }

    /**
     * Obtém o valor da propriedade actualOccurTime.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getActualOccurTime() {
        return actualOccurTime;
    }

    /**
     * Define o valor da propriedade actualOccurTime.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setActualOccurTime(GLogDateTimeType value) {
        this.actualOccurTime = value;
    }

    /**
     * Obtém o valor da propriedade isPlanDurFixed.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsPlanDurFixed() {
        return isPlanDurFixed;
    }

    /**
     * Define o valor da propriedade isPlanDurFixed.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsPlanDurFixed(String value) {
        this.isPlanDurFixed = value;
    }

    /**
     * Obtém o valor da propriedade plannedDuration.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public GLogXMLDurationType getPlannedDuration() {
        return plannedDuration;
    }

    /**
     * Define o valor da propriedade plannedDuration.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public void setPlannedDuration(GLogXMLDurationType value) {
        this.plannedDuration = value;
    }

    /**
     * Obtém o valor da propriedade actualDuration.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public GLogXMLDurationType getActualDuration() {
        return actualDuration;
    }

    /**
     * Define o valor da propriedade actualDuration.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public void setActualDuration(GLogXMLDurationType value) {
        this.actualDuration = value;
    }

    /**
     * Obtém o valor da propriedade actualDistance.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDistanceType }
     *     
     */
    public GLogXMLDistanceType getActualDistance() {
        return actualDistance;
    }

    /**
     * Define o valor da propriedade actualDistance.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDistanceType }
     *     
     */
    public void setActualDistance(GLogXMLDistanceType value) {
        this.actualDistance = value;
    }

    /**
     * Obtém o valor da propriedade actualWeight.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLWeightType }
     *     
     */
    public GLogXMLWeightType getActualWeight() {
        return actualWeight;
    }

    /**
     * Define o valor da propriedade actualWeight.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLWeightType }
     *     
     */
    public void setActualWeight(GLogXMLWeightType value) {
        this.actualWeight = value;
    }

    /**
     * Obtém o valor da propriedade actualVolume.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLVolumeType }
     *     
     */
    public GLogXMLVolumeType getActualVolume() {
        return actualVolume;
    }

    /**
     * Define o valor da propriedade actualVolume.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLVolumeType }
     *     
     */
    public void setActualVolume(GLogXMLVolumeType value) {
        this.actualVolume = value;
    }

    /**
     * Obtém o valor da propriedade actualShipUnitCount.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActualShipUnitCount() {
        return actualShipUnitCount;
    }

    /**
     * Define o valor da propriedade actualShipUnitCount.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActualShipUnitCount(String value) {
        this.actualShipUnitCount = value;
    }

    /**
     * Obtém o valor da propriedade actualItemPackageCount.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActualItemPackageCount() {
        return actualItemPackageCount;
    }

    /**
     * Define o valor da propriedade actualItemPackageCount.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActualItemPackageCount(String value) {
        this.actualItemPackageCount = value;
    }

    /**
     * Obtém o valor da propriedade isSystemGenerated.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsSystemGenerated() {
        return isSystemGenerated;
    }

    /**
     * Define o valor da propriedade isSystemGenerated.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsSystemGenerated(String value) {
        this.isSystemGenerated = value;
    }

    /**
     * Obtém o valor da propriedade stopSequence.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStopSequence() {
        return stopSequence;
    }

    /**
     * Define o valor da propriedade stopSequence.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStopSequence(String value) {
        this.stopSequence = value;
    }

    /**
     * Gets the value of the shipSpclServiceRef property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the shipSpclServiceRef property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getShipSpclServiceRef().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ShipSpclServiceRefType }
     * 
     * 
     */
    public List<ShipSpclServiceRefType> getShipSpclServiceRef() {
        if (shipSpclServiceRef == null) {
            shipSpclServiceRef = new ArrayList<ShipSpclServiceRefType>();
        }
        return this.shipSpclServiceRef;
    }

    /**
     * Gets the value of the remark property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the remark property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRemark().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RemarkType }
     * 
     * 
     */
    public List<RemarkType> getRemark() {
        if (remark == null) {
            remark = new ArrayList<RemarkType>();
        }
        return this.remark;
    }

}
