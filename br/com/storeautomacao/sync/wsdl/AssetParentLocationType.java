
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Used to keep a track of asset inventory at locations as opposed to the Packaged Item inventory.
 * 
 * <p>Classe Java de AssetParentLocationType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="AssetParentLocationType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ParentLocationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocGidType"/>
 *         &lt;element name="AssetType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AssetParentLocationType", propOrder = {
    "parentLocationGid",
    "assetType"
})
public class AssetParentLocationType {

    @XmlElement(name = "ParentLocationGid", required = true)
    protected GLogXMLLocGidType parentLocationGid;
    @XmlElement(name = "AssetType", required = true)
    protected String assetType;

    /**
     * Obtém o valor da propriedade parentLocationGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocGidType }
     *     
     */
    public GLogXMLLocGidType getParentLocationGid() {
        return parentLocationGid;
    }

    /**
     * Define o valor da propriedade parentLocationGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocGidType }
     *     
     */
    public void setParentLocationGid(GLogXMLLocGidType value) {
        this.parentLocationGid = value;
    }

    /**
     * Obtém o valor da propriedade assetType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAssetType() {
        return assetType;
    }

    /**
     * Define o valor da propriedade assetType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAssetType(String value) {
        this.assetType = value;
    }

}
