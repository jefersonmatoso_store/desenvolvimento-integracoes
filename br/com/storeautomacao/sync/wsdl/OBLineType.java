
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * This is used to send out the TransOrderLine information. It is initiated via
 *             notification.
 *             This element is supported on the outbound only.
 *          
 * 
 * <p>Classe Java de OBLineType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="OBLineType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OTMTransactionOut">
 *       &lt;sequence>
 *         &lt;element name="SendReason" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}SendReasonType" minOccurs="0"/>
 *         &lt;element name="TransOrderLine" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransOrderLineType"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OBLineType", propOrder = {
    "sendReason",
    "transOrderLine"
})
public class OBLineType
    extends OTMTransactionOut
{

    @XmlElement(name = "SendReason")
    protected SendReasonType sendReason;
    @XmlElement(name = "TransOrderLine", required = true)
    protected TransOrderLineType transOrderLine;

    /**
     * Obtém o valor da propriedade sendReason.
     * 
     * @return
     *     possible object is
     *     {@link SendReasonType }
     *     
     */
    public SendReasonType getSendReason() {
        return sendReason;
    }

    /**
     * Define o valor da propriedade sendReason.
     * 
     * @param value
     *     allowed object is
     *     {@link SendReasonType }
     *     
     */
    public void setSendReason(SendReasonType value) {
        this.sendReason = value;
    }

    /**
     * Obtém o valor da propriedade transOrderLine.
     * 
     * @return
     *     possible object is
     *     {@link TransOrderLineType }
     *     
     */
    public TransOrderLineType getTransOrderLine() {
        return transOrderLine;
    }

    /**
     * Define o valor da propriedade transOrderLine.
     * 
     * @param value
     *     allowed object is
     *     {@link TransOrderLineType }
     *     
     */
    public void setTransOrderLine(TransOrderLineType value) {
        this.transOrderLine = value;
    }

}
