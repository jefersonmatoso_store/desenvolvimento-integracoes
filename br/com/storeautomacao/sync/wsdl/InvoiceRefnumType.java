
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * (Both) InvoiceRefnum is an alternative identifier for an invoice or bill.
 *             It consists of an InvoiceRefnumQualifierGid and an InvoiceRefnumValue.
 *          
 * 
 * <p>Classe Java de InvoiceRefnumType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="InvoiceRefnumType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="InvoiceRefnumQualifierGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="InvoiceRefnumValue" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="IssueDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InvoiceRefnumType", propOrder = {
    "invoiceRefnumQualifierGid",
    "invoiceRefnumValue",
    "issueDt"
})
public class InvoiceRefnumType {

    @XmlElement(name = "InvoiceRefnumQualifierGid", required = true)
    protected GLogXMLGidType invoiceRefnumQualifierGid;
    @XmlElement(name = "InvoiceRefnumValue", required = true)
    protected String invoiceRefnumValue;
    @XmlElement(name = "IssueDt")
    protected GLogDateTimeType issueDt;

    /**
     * Obtém o valor da propriedade invoiceRefnumQualifierGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getInvoiceRefnumQualifierGid() {
        return invoiceRefnumQualifierGid;
    }

    /**
     * Define o valor da propriedade invoiceRefnumQualifierGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setInvoiceRefnumQualifierGid(GLogXMLGidType value) {
        this.invoiceRefnumQualifierGid = value;
    }

    /**
     * Obtém o valor da propriedade invoiceRefnumValue.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInvoiceRefnumValue() {
        return invoiceRefnumValue;
    }

    /**
     * Define o valor da propriedade invoiceRefnumValue.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInvoiceRefnumValue(String value) {
        this.invoiceRefnumValue = value;
    }

    /**
     * Obtém o valor da propriedade issueDt.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getIssueDt() {
        return issueDt;
    }

    /**
     * Define o valor da propriedade issueDt.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setIssueDt(GLogDateTimeType value) {
        this.issueDt = value;
    }

}
