
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Represents the use of a text template for a particular document type.
 * 
 * <p>Classe Java de TextTemplateDocumentDefType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="TextTemplateDocumentDefType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DocumentDefinitionGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="TextTemplateIsMandatory" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TextTemplateDocumentDefType", propOrder = {
    "documentDefinitionGid",
    "textTemplateIsMandatory"
})
public class TextTemplateDocumentDefType {

    @XmlElement(name = "DocumentDefinitionGid", required = true)
    protected GLogXMLGidType documentDefinitionGid;
    @XmlElement(name = "TextTemplateIsMandatory")
    protected String textTemplateIsMandatory;

    /**
     * Obtém o valor da propriedade documentDefinitionGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getDocumentDefinitionGid() {
        return documentDefinitionGid;
    }

    /**
     * Define o valor da propriedade documentDefinitionGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setDocumentDefinitionGid(GLogXMLGidType value) {
        this.documentDefinitionGid = value;
    }

    /**
     * Obtém o valor da propriedade textTemplateIsMandatory.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTextTemplateIsMandatory() {
        return textTemplateIsMandatory;
    }

    /**
     * Define o valor da propriedade textTemplateIsMandatory.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTextTemplateIsMandatory(String value) {
        this.textTemplateIsMandatory = value;
    }

}
