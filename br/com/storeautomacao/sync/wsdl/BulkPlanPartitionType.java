
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * Specifies the bulk plan summary information broken down by transport mode.
 * 
 * <p>Classe Java de BulkPlanPartitionType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="BulkPlanPartitionType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PlanPartitionGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="SubmitTime" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="StartTime" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="EndTime" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="NumOfOrdersSelected" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NumOfOrdersUnassigned" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NumOfOrdersPlanned" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NumOfShipmentsBuilt" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NumOrdersPlannedFailed" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NumOrderMovementsSelected" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NumOrderMovementsUnassigned" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NumOrderMovementsPlanned" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NumOrderMovementsFailed" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TotalCost" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/>
 *         &lt;element name="TotalWeightVolume" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLWeightVolumeType" minOccurs="0"/>
 *         &lt;element name="TotalDistance" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDistanceType" minOccurs="0"/>
 *         &lt;element name="TotalNumStops" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BulkPlanPartitionByMode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}BulkPlanPartitionByModeType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BulkPlanPartitionType", propOrder = {
    "planPartitionGid",
    "submitTime",
    "startTime",
    "endTime",
    "numOfOrdersSelected",
    "numOfOrdersUnassigned",
    "numOfOrdersPlanned",
    "numOfShipmentsBuilt",
    "numOrdersPlannedFailed",
    "numOrderMovementsSelected",
    "numOrderMovementsUnassigned",
    "numOrderMovementsPlanned",
    "numOrderMovementsFailed",
    "totalCost",
    "totalWeightVolume",
    "totalDistance",
    "totalNumStops",
    "bulkPlanPartitionByMode"
})
public class BulkPlanPartitionType {

    @XmlElement(name = "PlanPartitionGid", required = true)
    protected GLogXMLGidType planPartitionGid;
    @XmlElement(name = "SubmitTime")
    protected GLogDateTimeType submitTime;
    @XmlElement(name = "StartTime")
    protected GLogDateTimeType startTime;
    @XmlElement(name = "EndTime")
    protected GLogDateTimeType endTime;
    @XmlElement(name = "NumOfOrdersSelected")
    protected String numOfOrdersSelected;
    @XmlElement(name = "NumOfOrdersUnassigned")
    protected String numOfOrdersUnassigned;
    @XmlElement(name = "NumOfOrdersPlanned")
    protected String numOfOrdersPlanned;
    @XmlElement(name = "NumOfShipmentsBuilt")
    protected String numOfShipmentsBuilt;
    @XmlElement(name = "NumOrdersPlannedFailed")
    protected String numOrdersPlannedFailed;
    @XmlElement(name = "NumOrderMovementsSelected")
    protected String numOrderMovementsSelected;
    @XmlElement(name = "NumOrderMovementsUnassigned")
    protected String numOrderMovementsUnassigned;
    @XmlElement(name = "NumOrderMovementsPlanned")
    protected String numOrderMovementsPlanned;
    @XmlElement(name = "NumOrderMovementsFailed")
    protected String numOrderMovementsFailed;
    @XmlElement(name = "TotalCost")
    protected GLogXMLFinancialAmountType totalCost;
    @XmlElement(name = "TotalWeightVolume")
    protected GLogXMLWeightVolumeType totalWeightVolume;
    @XmlElement(name = "TotalDistance")
    protected GLogXMLDistanceType totalDistance;
    @XmlElement(name = "TotalNumStops")
    protected String totalNumStops;
    @XmlElement(name = "BulkPlanPartitionByMode")
    protected List<BulkPlanPartitionByModeType> bulkPlanPartitionByMode;

    /**
     * Obtém o valor da propriedade planPartitionGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getPlanPartitionGid() {
        return planPartitionGid;
    }

    /**
     * Define o valor da propriedade planPartitionGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setPlanPartitionGid(GLogXMLGidType value) {
        this.planPartitionGid = value;
    }

    /**
     * Obtém o valor da propriedade submitTime.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getSubmitTime() {
        return submitTime;
    }

    /**
     * Define o valor da propriedade submitTime.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setSubmitTime(GLogDateTimeType value) {
        this.submitTime = value;
    }

    /**
     * Obtém o valor da propriedade startTime.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getStartTime() {
        return startTime;
    }

    /**
     * Define o valor da propriedade startTime.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setStartTime(GLogDateTimeType value) {
        this.startTime = value;
    }

    /**
     * Obtém o valor da propriedade endTime.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getEndTime() {
        return endTime;
    }

    /**
     * Define o valor da propriedade endTime.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setEndTime(GLogDateTimeType value) {
        this.endTime = value;
    }

    /**
     * Obtém o valor da propriedade numOfOrdersSelected.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumOfOrdersSelected() {
        return numOfOrdersSelected;
    }

    /**
     * Define o valor da propriedade numOfOrdersSelected.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumOfOrdersSelected(String value) {
        this.numOfOrdersSelected = value;
    }

    /**
     * Obtém o valor da propriedade numOfOrdersUnassigned.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumOfOrdersUnassigned() {
        return numOfOrdersUnassigned;
    }

    /**
     * Define o valor da propriedade numOfOrdersUnassigned.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumOfOrdersUnassigned(String value) {
        this.numOfOrdersUnassigned = value;
    }

    /**
     * Obtém o valor da propriedade numOfOrdersPlanned.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumOfOrdersPlanned() {
        return numOfOrdersPlanned;
    }

    /**
     * Define o valor da propriedade numOfOrdersPlanned.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumOfOrdersPlanned(String value) {
        this.numOfOrdersPlanned = value;
    }

    /**
     * Obtém o valor da propriedade numOfShipmentsBuilt.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumOfShipmentsBuilt() {
        return numOfShipmentsBuilt;
    }

    /**
     * Define o valor da propriedade numOfShipmentsBuilt.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumOfShipmentsBuilt(String value) {
        this.numOfShipmentsBuilt = value;
    }

    /**
     * Obtém o valor da propriedade numOrdersPlannedFailed.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumOrdersPlannedFailed() {
        return numOrdersPlannedFailed;
    }

    /**
     * Define o valor da propriedade numOrdersPlannedFailed.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumOrdersPlannedFailed(String value) {
        this.numOrdersPlannedFailed = value;
    }

    /**
     * Obtém o valor da propriedade numOrderMovementsSelected.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumOrderMovementsSelected() {
        return numOrderMovementsSelected;
    }

    /**
     * Define o valor da propriedade numOrderMovementsSelected.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumOrderMovementsSelected(String value) {
        this.numOrderMovementsSelected = value;
    }

    /**
     * Obtém o valor da propriedade numOrderMovementsUnassigned.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumOrderMovementsUnassigned() {
        return numOrderMovementsUnassigned;
    }

    /**
     * Define o valor da propriedade numOrderMovementsUnassigned.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumOrderMovementsUnassigned(String value) {
        this.numOrderMovementsUnassigned = value;
    }

    /**
     * Obtém o valor da propriedade numOrderMovementsPlanned.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumOrderMovementsPlanned() {
        return numOrderMovementsPlanned;
    }

    /**
     * Define o valor da propriedade numOrderMovementsPlanned.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumOrderMovementsPlanned(String value) {
        this.numOrderMovementsPlanned = value;
    }

    /**
     * Obtém o valor da propriedade numOrderMovementsFailed.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumOrderMovementsFailed() {
        return numOrderMovementsFailed;
    }

    /**
     * Define o valor da propriedade numOrderMovementsFailed.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumOrderMovementsFailed(String value) {
        this.numOrderMovementsFailed = value;
    }

    /**
     * Obtém o valor da propriedade totalCost.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getTotalCost() {
        return totalCost;
    }

    /**
     * Define o valor da propriedade totalCost.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setTotalCost(GLogXMLFinancialAmountType value) {
        this.totalCost = value;
    }

    /**
     * Obtém o valor da propriedade totalWeightVolume.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLWeightVolumeType }
     *     
     */
    public GLogXMLWeightVolumeType getTotalWeightVolume() {
        return totalWeightVolume;
    }

    /**
     * Define o valor da propriedade totalWeightVolume.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLWeightVolumeType }
     *     
     */
    public void setTotalWeightVolume(GLogXMLWeightVolumeType value) {
        this.totalWeightVolume = value;
    }

    /**
     * Obtém o valor da propriedade totalDistance.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDistanceType }
     *     
     */
    public GLogXMLDistanceType getTotalDistance() {
        return totalDistance;
    }

    /**
     * Define o valor da propriedade totalDistance.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDistanceType }
     *     
     */
    public void setTotalDistance(GLogXMLDistanceType value) {
        this.totalDistance = value;
    }

    /**
     * Obtém o valor da propriedade totalNumStops.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTotalNumStops() {
        return totalNumStops;
    }

    /**
     * Define o valor da propriedade totalNumStops.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTotalNumStops(String value) {
        this.totalNumStops = value;
    }

    /**
     * Gets the value of the bulkPlanPartitionByMode property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the bulkPlanPartitionByMode property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBulkPlanPartitionByMode().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BulkPlanPartitionByModeType }
     * 
     * 
     */
    public List<BulkPlanPartitionByModeType> getBulkPlanPartitionByMode() {
        if (bulkPlanPartitionByMode == null) {
            bulkPlanPartitionByMode = new ArrayList<BulkPlanPartitionByModeType>();
        }
        return this.bulkPlanPartitionByMode;
    }

}
