
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 *             This is used to provide statistics about shipments that were selected and linked during a given run of bulk continuous moves.
 *          
 * 
 * <p>Classe Java de BulkContMoveType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="BulkContMoveType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OTMTransactionOut">
 *       &lt;sequence>
 *         &lt;element name="SendReason" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}SendReasonType" minOccurs="0"/>
 *         &lt;element name="BulkContMoveGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="QueryName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="StartTime" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="EndTime" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="NumOfShipmentsSelected" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NumOfShipmentsLinked" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BulkContMoveType", propOrder = {
    "sendReason",
    "bulkContMoveGid",
    "queryName",
    "startTime",
    "endTime",
    "numOfShipmentsSelected",
    "numOfShipmentsLinked"
})
public class BulkContMoveType
    extends OTMTransactionOut
{

    @XmlElement(name = "SendReason")
    protected SendReasonType sendReason;
    @XmlElement(name = "BulkContMoveGid", required = true)
    protected GLogXMLGidType bulkContMoveGid;
    @XmlElement(name = "QueryName")
    protected String queryName;
    @XmlElement(name = "StartTime")
    protected GLogDateTimeType startTime;
    @XmlElement(name = "EndTime")
    protected GLogDateTimeType endTime;
    @XmlElement(name = "NumOfShipmentsSelected")
    protected String numOfShipmentsSelected;
    @XmlElement(name = "NumOfShipmentsLinked")
    protected String numOfShipmentsLinked;

    /**
     * Obtém o valor da propriedade sendReason.
     * 
     * @return
     *     possible object is
     *     {@link SendReasonType }
     *     
     */
    public SendReasonType getSendReason() {
        return sendReason;
    }

    /**
     * Define o valor da propriedade sendReason.
     * 
     * @param value
     *     allowed object is
     *     {@link SendReasonType }
     *     
     */
    public void setSendReason(SendReasonType value) {
        this.sendReason = value;
    }

    /**
     * Obtém o valor da propriedade bulkContMoveGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getBulkContMoveGid() {
        return bulkContMoveGid;
    }

    /**
     * Define o valor da propriedade bulkContMoveGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setBulkContMoveGid(GLogXMLGidType value) {
        this.bulkContMoveGid = value;
    }

    /**
     * Obtém o valor da propriedade queryName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQueryName() {
        return queryName;
    }

    /**
     * Define o valor da propriedade queryName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQueryName(String value) {
        this.queryName = value;
    }

    /**
     * Obtém o valor da propriedade startTime.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getStartTime() {
        return startTime;
    }

    /**
     * Define o valor da propriedade startTime.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setStartTime(GLogDateTimeType value) {
        this.startTime = value;
    }

    /**
     * Obtém o valor da propriedade endTime.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getEndTime() {
        return endTime;
    }

    /**
     * Define o valor da propriedade endTime.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setEndTime(GLogDateTimeType value) {
        this.endTime = value;
    }

    /**
     * Obtém o valor da propriedade numOfShipmentsSelected.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumOfShipmentsSelected() {
        return numOfShipmentsSelected;
    }

    /**
     * Define o valor da propriedade numOfShipmentsSelected.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumOfShipmentsSelected(String value) {
        this.numOfShipmentsSelected = value;
    }

    /**
     * Obtém o valor da propriedade numOfShipmentsLinked.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumOfShipmentsLinked() {
        return numOfShipmentsLinked;
    }

    /**
     * Define o valor da propriedade numOfShipmentsLinked.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumOfShipmentsLinked(String value) {
        this.numOfShipmentsLinked = value;
    }

}
