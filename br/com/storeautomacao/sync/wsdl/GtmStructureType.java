
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;


/**
 * 
 *             Represents Trade Item Structure (bill of material) to identify
 *             components that make up a single sellable product or item.
 * 
 *             Deprecated (6.4.3): TradeDirection. Functionality removed. Value will be ignored and field will be removed in a future release.
 *          
 * 
 * <p>Classe Java de GtmStructureType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="GtmStructureType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OTMTransactionIn">
 *       &lt;sequence>
 *         &lt;element name="GtmStructureGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="TransactionCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransactionCodeType" minOccurs="0"/>
 *         &lt;element name="GtmStructureName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsActive" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="EffectiveDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="ExpirationDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="ItemGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="TradeDirection" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="GtmProdClassTypeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="DefaultCountryOfOriginGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="IntSavedQuery" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}IntSavedQueryType" minOccurs="0"/>
 *         &lt;element name="GtmStructureComponent" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}GtmStructureComponentType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Remark" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RemarkType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="InvolvedParty" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}InvolvedPartyType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Refnum" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RefnumType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="GtmProdClassCodeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="UserDefinedClassification" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}UserDefinedClassificationType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="ClassificationCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GtmStructureType", namespace = "http://xmlns.oracle.com/apps/gtm/transmission/v6.4", propOrder = {
    "gtmStructureGid",
    "transactionCode",
    "gtmStructureName",
    "description",
    "isActive",
    "effectiveDate",
    "expirationDate",
    "itemGid",
    "tradeDirection",
    "gtmProdClassTypeGid",
    "defaultCountryOfOriginGid",
    "intSavedQuery",
    "gtmStructureComponent",
    "remark",
    "involvedParty",
    "refnum",
    "gtmProdClassCodeGid",
    "userDefinedClassification",
    "classificationCode"
})
public class GtmStructureType
    extends OTMTransactionIn
{

    @XmlElement(name = "GtmStructureGid")
    protected GLogXMLGidType gtmStructureGid;
    @XmlElement(name = "TransactionCode")
    @XmlSchemaType(name = "string")
    protected TransactionCodeType transactionCode;
    @XmlElement(name = "GtmStructureName")
    protected String gtmStructureName;
    @XmlElement(name = "Description")
    protected String description;
    @XmlElement(name = "IsActive", required = true)
    protected String isActive;
    @XmlElement(name = "EffectiveDate")
    protected GLogDateTimeType effectiveDate;
    @XmlElement(name = "ExpirationDate")
    protected GLogDateTimeType expirationDate;
    @XmlElement(name = "ItemGid")
    protected GLogXMLGidType itemGid;
    @XmlElement(name = "TradeDirection")
    protected String tradeDirection;
    @XmlElement(name = "GtmProdClassTypeGid")
    protected GLogXMLGidType gtmProdClassTypeGid;
    @XmlElement(name = "DefaultCountryOfOriginGid")
    protected GLogXMLGidType defaultCountryOfOriginGid;
    @XmlElement(name = "IntSavedQuery")
    protected IntSavedQueryType intSavedQuery;
    @XmlElement(name = "GtmStructureComponent")
    protected List<GtmStructureComponentType> gtmStructureComponent;
    @XmlElement(name = "Remark")
    protected List<RemarkType> remark;
    @XmlElement(name = "InvolvedParty")
    protected List<InvolvedPartyType> involvedParty;
    @XmlElement(name = "Refnum")
    protected List<RefnumType> refnum;
    @XmlElement(name = "GtmProdClassCodeGid")
    protected GLogXMLGidType gtmProdClassCodeGid;
    @XmlElement(name = "UserDefinedClassification")
    protected List<UserDefinedClassificationType> userDefinedClassification;
    @XmlElement(name = "ClassificationCode")
    protected String classificationCode;

    /**
     * Obtém o valor da propriedade gtmStructureGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getGtmStructureGid() {
        return gtmStructureGid;
    }

    /**
     * Define o valor da propriedade gtmStructureGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setGtmStructureGid(GLogXMLGidType value) {
        this.gtmStructureGid = value;
    }

    /**
     * Obtém o valor da propriedade transactionCode.
     * 
     * @return
     *     possible object is
     *     {@link TransactionCodeType }
     *     
     */
    public TransactionCodeType getTransactionCode() {
        return transactionCode;
    }

    /**
     * Define o valor da propriedade transactionCode.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionCodeType }
     *     
     */
    public void setTransactionCode(TransactionCodeType value) {
        this.transactionCode = value;
    }

    /**
     * Obtém o valor da propriedade gtmStructureName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGtmStructureName() {
        return gtmStructureName;
    }

    /**
     * Define o valor da propriedade gtmStructureName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGtmStructureName(String value) {
        this.gtmStructureName = value;
    }

    /**
     * Obtém o valor da propriedade description.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Define o valor da propriedade description.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Obtém o valor da propriedade isActive.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsActive() {
        return isActive;
    }

    /**
     * Define o valor da propriedade isActive.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsActive(String value) {
        this.isActive = value;
    }

    /**
     * Obtém o valor da propriedade effectiveDate.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getEffectiveDate() {
        return effectiveDate;
    }

    /**
     * Define o valor da propriedade effectiveDate.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setEffectiveDate(GLogDateTimeType value) {
        this.effectiveDate = value;
    }

    /**
     * Obtém o valor da propriedade expirationDate.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getExpirationDate() {
        return expirationDate;
    }

    /**
     * Define o valor da propriedade expirationDate.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setExpirationDate(GLogDateTimeType value) {
        this.expirationDate = value;
    }

    /**
     * Obtém o valor da propriedade itemGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getItemGid() {
        return itemGid;
    }

    /**
     * Define o valor da propriedade itemGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setItemGid(GLogXMLGidType value) {
        this.itemGid = value;
    }

    /**
     * Obtém o valor da propriedade tradeDirection.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTradeDirection() {
        return tradeDirection;
    }

    /**
     * Define o valor da propriedade tradeDirection.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTradeDirection(String value) {
        this.tradeDirection = value;
    }

    /**
     * Obtém o valor da propriedade gtmProdClassTypeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getGtmProdClassTypeGid() {
        return gtmProdClassTypeGid;
    }

    /**
     * Define o valor da propriedade gtmProdClassTypeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setGtmProdClassTypeGid(GLogXMLGidType value) {
        this.gtmProdClassTypeGid = value;
    }

    /**
     * Obtém o valor da propriedade defaultCountryOfOriginGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getDefaultCountryOfOriginGid() {
        return defaultCountryOfOriginGid;
    }

    /**
     * Define o valor da propriedade defaultCountryOfOriginGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setDefaultCountryOfOriginGid(GLogXMLGidType value) {
        this.defaultCountryOfOriginGid = value;
    }

    /**
     * Obtém o valor da propriedade intSavedQuery.
     * 
     * @return
     *     possible object is
     *     {@link IntSavedQueryType }
     *     
     */
    public IntSavedQueryType getIntSavedQuery() {
        return intSavedQuery;
    }

    /**
     * Define o valor da propriedade intSavedQuery.
     * 
     * @param value
     *     allowed object is
     *     {@link IntSavedQueryType }
     *     
     */
    public void setIntSavedQuery(IntSavedQueryType value) {
        this.intSavedQuery = value;
    }

    /**
     * Gets the value of the gtmStructureComponent property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the gtmStructureComponent property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGtmStructureComponent().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GtmStructureComponentType }
     * 
     * 
     */
    public List<GtmStructureComponentType> getGtmStructureComponent() {
        if (gtmStructureComponent == null) {
            gtmStructureComponent = new ArrayList<GtmStructureComponentType>();
        }
        return this.gtmStructureComponent;
    }

    /**
     * Gets the value of the remark property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the remark property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRemark().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RemarkType }
     * 
     * 
     */
    public List<RemarkType> getRemark() {
        if (remark == null) {
            remark = new ArrayList<RemarkType>();
        }
        return this.remark;
    }

    /**
     * Gets the value of the involvedParty property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the involvedParty property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInvolvedParty().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link InvolvedPartyType }
     * 
     * 
     */
    public List<InvolvedPartyType> getInvolvedParty() {
        if (involvedParty == null) {
            involvedParty = new ArrayList<InvolvedPartyType>();
        }
        return this.involvedParty;
    }

    /**
     * Gets the value of the refnum property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the refnum property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRefnum().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RefnumType }
     * 
     * 
     */
    public List<RefnumType> getRefnum() {
        if (refnum == null) {
            refnum = new ArrayList<RefnumType>();
        }
        return this.refnum;
    }

    /**
     * Obtém o valor da propriedade gtmProdClassCodeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getGtmProdClassCodeGid() {
        return gtmProdClassCodeGid;
    }

    /**
     * Define o valor da propriedade gtmProdClassCodeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setGtmProdClassCodeGid(GLogXMLGidType value) {
        this.gtmProdClassCodeGid = value;
    }

    /**
     * Gets the value of the userDefinedClassification property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the userDefinedClassification property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getUserDefinedClassification().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link UserDefinedClassificationType }
     * 
     * 
     */
    public List<UserDefinedClassificationType> getUserDefinedClassification() {
        if (userDefinedClassification == null) {
            userDefinedClassification = new ArrayList<UserDefinedClassificationType>();
        }
        return this.userDefinedClassification;
    }

    /**
     * Obtém o valor da propriedade classificationCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClassificationCode() {
        return classificationCode;
    }

    /**
     * Define o valor da propriedade classificationCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClassificationCode(String value) {
        this.classificationCode = value;
    }

}
