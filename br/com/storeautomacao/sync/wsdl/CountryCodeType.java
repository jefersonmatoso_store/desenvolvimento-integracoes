
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de CountryCodeType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="CountryCodeType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CountryCode3Gid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="CountryCode2" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CountryCodeType", propOrder = {
    "countryCode3Gid",
    "countryCode2"
})
public class CountryCodeType {

    @XmlElement(name = "CountryCode3Gid", required = true)
    protected GLogXMLGidType countryCode3Gid;
    @XmlElement(name = "CountryCode2", required = true)
    protected String countryCode2;

    /**
     * Obtém o valor da propriedade countryCode3Gid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getCountryCode3Gid() {
        return countryCode3Gid;
    }

    /**
     * Define o valor da propriedade countryCode3Gid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setCountryCode3Gid(GLogXMLGidType value) {
        this.countryCode3Gid = value;
    }

    /**
     * Obtém o valor da propriedade countryCode2.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCountryCode2() {
        return countryCode2;
    }

    /**
     * Define o valor da propriedade countryCode2.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCountryCode2(String value) {
        this.countryCode2 = value;
    }

}
