
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Specifies the international classification for the order release line.
 *          
 * 
 * <p>Classe Java de ReleaseLineIntlClassificationType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="ReleaseLineIntlClassificationType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SequenceNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ImportExportType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="HTSGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="PrimaryRQ" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFlexQuantityType" minOccurs="0"/>
 *         &lt;element name="SecondaryRQ" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFlexQuantityType" minOccurs="0"/>
 *         &lt;element name="TertiaryRQ" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFlexQuantityType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReleaseLineIntlClassificationType", propOrder = {
    "sequenceNumber",
    "importExportType",
    "htsGid",
    "primaryRQ",
    "secondaryRQ",
    "tertiaryRQ"
})
public class ReleaseLineIntlClassificationType {

    @XmlElement(name = "SequenceNumber")
    protected String sequenceNumber;
    @XmlElement(name = "ImportExportType")
    protected String importExportType;
    @XmlElement(name = "HTSGid")
    protected GLogXMLGidType htsGid;
    @XmlElement(name = "PrimaryRQ")
    protected GLogXMLFlexQuantityType primaryRQ;
    @XmlElement(name = "SecondaryRQ")
    protected GLogXMLFlexQuantityType secondaryRQ;
    @XmlElement(name = "TertiaryRQ")
    protected GLogXMLFlexQuantityType tertiaryRQ;

    /**
     * Obtém o valor da propriedade sequenceNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSequenceNumber() {
        return sequenceNumber;
    }

    /**
     * Define o valor da propriedade sequenceNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSequenceNumber(String value) {
        this.sequenceNumber = value;
    }

    /**
     * Obtém o valor da propriedade importExportType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getImportExportType() {
        return importExportType;
    }

    /**
     * Define o valor da propriedade importExportType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setImportExportType(String value) {
        this.importExportType = value;
    }

    /**
     * Obtém o valor da propriedade htsGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getHTSGid() {
        return htsGid;
    }

    /**
     * Define o valor da propriedade htsGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setHTSGid(GLogXMLGidType value) {
        this.htsGid = value;
    }

    /**
     * Obtém o valor da propriedade primaryRQ.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFlexQuantityType }
     *     
     */
    public GLogXMLFlexQuantityType getPrimaryRQ() {
        return primaryRQ;
    }

    /**
     * Define o valor da propriedade primaryRQ.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFlexQuantityType }
     *     
     */
    public void setPrimaryRQ(GLogXMLFlexQuantityType value) {
        this.primaryRQ = value;
    }

    /**
     * Obtém o valor da propriedade secondaryRQ.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFlexQuantityType }
     *     
     */
    public GLogXMLFlexQuantityType getSecondaryRQ() {
        return secondaryRQ;
    }

    /**
     * Define o valor da propriedade secondaryRQ.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFlexQuantityType }
     *     
     */
    public void setSecondaryRQ(GLogXMLFlexQuantityType value) {
        this.secondaryRQ = value;
    }

    /**
     * Obtém o valor da propriedade tertiaryRQ.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFlexQuantityType }
     *     
     */
    public GLogXMLFlexQuantityType getTertiaryRQ() {
        return tertiaryRQ;
    }

    /**
     * Define o valor da propriedade tertiaryRQ.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFlexQuantityType }
     *     
     */
    public void setTertiaryRQ(GLogXMLFlexQuantityType value) {
        this.tertiaryRQ = value;
    }

}
