
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de GlPasswordType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="GlPasswordType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;choice>
 *           &lt;element name="TextPassword" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *           &lt;element name="EncryptedPassword" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;/choice>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GlPasswordType", propOrder = {
    "textPassword",
    "encryptedPassword"
})
public class GlPasswordType {

    @XmlElement(name = "TextPassword")
    protected String textPassword;
    @XmlElement(name = "EncryptedPassword")
    protected String encryptedPassword;

    /**
     * Obtém o valor da propriedade textPassword.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTextPassword() {
        return textPassword;
    }

    /**
     * Define o valor da propriedade textPassword.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTextPassword(String value) {
        this.textPassword = value;
    }

    /**
     * Obtém o valor da propriedade encryptedPassword.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEncryptedPassword() {
        return encryptedPassword;
    }

    /**
     * Define o valor da propriedade encryptedPassword.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEncryptedPassword(String value) {
        this.encryptedPassword = value;
    }

}
