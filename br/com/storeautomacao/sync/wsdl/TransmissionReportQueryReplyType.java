
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Classe Java de TransmissionReportQueryReplyType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="TransmissionReportQueryReplyType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TransmissionReport" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransmissionReportType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TransmissionReportQueryReplyType", propOrder = {
    "transmissionReport"
})
public class TransmissionReportQueryReplyType {

    @XmlElement(name = "TransmissionReport")
    protected List<TransmissionReportType> transmissionReport;

    /**
     * Gets the value of the transmissionReport property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the transmissionReport property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTransmissionReport().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TransmissionReportType }
     * 
     * 
     */
    public List<TransmissionReportType> getTransmissionReport() {
        if (transmissionReport == null) {
            transmissionReport = new ArrayList<TransmissionReportType>();
        }
        return this.transmissionReport;
    }

}
