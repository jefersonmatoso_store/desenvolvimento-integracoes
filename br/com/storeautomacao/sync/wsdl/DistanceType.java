
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Distance is a structure for describing distance.
 *             It includes unit of measure and value.
 *          
 * 
 * <p>Classe Java de DistanceType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="DistanceType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DistanceValue" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="DistanceUOMGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DistanceType", propOrder = {
    "distanceValue",
    "distanceUOMGid"
})
public class DistanceType {

    @XmlElement(name = "DistanceValue", required = true)
    protected String distanceValue;
    @XmlElement(name = "DistanceUOMGid", required = true)
    protected GLogXMLGidType distanceUOMGid;

    /**
     * Obtém o valor da propriedade distanceValue.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDistanceValue() {
        return distanceValue;
    }

    /**
     * Define o valor da propriedade distanceValue.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDistanceValue(String value) {
        this.distanceValue = value;
    }

    /**
     * Obtém o valor da propriedade distanceUOMGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getDistanceUOMGid() {
        return distanceUOMGid;
    }

    /**
     * Define o valor da propriedade distanceUOMGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setDistanceUOMGid(GLogXMLGidType value) {
        this.distanceUOMGid = value;
    }

}
