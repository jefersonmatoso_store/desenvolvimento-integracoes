
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de JobInvolvedPartyType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="JobInvolvedPartyType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="InvolvedParty" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}InvolvedPartyType"/>
 *         &lt;element name="ShippingAgentContactGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "JobInvolvedPartyType", propOrder = {
    "involvedParty",
    "shippingAgentContactGid"
})
public class JobInvolvedPartyType {

    @XmlElement(name = "InvolvedParty", required = true)
    protected InvolvedPartyType involvedParty;
    @XmlElement(name = "ShippingAgentContactGid")
    protected GLogXMLGidType shippingAgentContactGid;

    /**
     * Obtém o valor da propriedade involvedParty.
     * 
     * @return
     *     possible object is
     *     {@link InvolvedPartyType }
     *     
     */
    public InvolvedPartyType getInvolvedParty() {
        return involvedParty;
    }

    /**
     * Define o valor da propriedade involvedParty.
     * 
     * @param value
     *     allowed object is
     *     {@link InvolvedPartyType }
     *     
     */
    public void setInvolvedParty(InvolvedPartyType value) {
        this.involvedParty = value;
    }

    /**
     * Obtém o valor da propriedade shippingAgentContactGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getShippingAgentContactGid() {
        return shippingAgentContactGid;
    }

    /**
     * Define o valor da propriedade shippingAgentContactGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setShippingAgentContactGid(GLogXMLGidType value) {
        this.shippingAgentContactGid = value;
    }

}
