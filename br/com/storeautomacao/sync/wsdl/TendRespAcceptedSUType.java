
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Used to specify the quantities of ship unit(s) for partially accepted tenders.
 * 
 * <p>Classe Java de TendRespAcceptedSUType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="TendRespAcceptedSUType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ShipUnitGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="ShipUnitCount" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="EquipmentInfo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TendRespAcceptedSUType", propOrder = {
    "shipUnitGid",
    "shipUnitCount",
    "equipmentInfo"
})
public class TendRespAcceptedSUType {

    @XmlElement(name = "ShipUnitGid", required = true)
    protected GLogXMLGidType shipUnitGid;
    @XmlElement(name = "ShipUnitCount", required = true)
    protected String shipUnitCount;
    @XmlElement(name = "EquipmentInfo")
    protected String equipmentInfo;

    /**
     * Obtém o valor da propriedade shipUnitGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getShipUnitGid() {
        return shipUnitGid;
    }

    /**
     * Define o valor da propriedade shipUnitGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setShipUnitGid(GLogXMLGidType value) {
        this.shipUnitGid = value;
    }

    /**
     * Obtém o valor da propriedade shipUnitCount.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipUnitCount() {
        return shipUnitCount;
    }

    /**
     * Define o valor da propriedade shipUnitCount.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipUnitCount(String value) {
        this.shipUnitCount = value;
    }

    /**
     * Obtém o valor da propriedade equipmentInfo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEquipmentInfo() {
        return equipmentInfo;
    }

    /**
     * Define o valor da propriedade equipmentInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEquipmentInfo(String value) {
        this.equipmentInfo = value;
    }

}
