
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * 
 *             Specifies the Value Added Tax information for the invoice or bill line item.
 *          
 * 
 * <p>Classe Java de LineItemVatType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="LineItemVatType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SequenceNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="VatCodeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="CountryCode3Gid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="ProvinceCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="VatRate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="VatAmount" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/>
 *         &lt;element name="VatCalcAmount" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/>
 *         &lt;element name="VatOverrideAmount" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/>
 *         &lt;element name="IsCumulative" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LineItemVatCostRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LineItemVatCostRefType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LineItemVatType", propOrder = {
    "sequenceNumber",
    "vatCodeGid",
    "countryCode3Gid",
    "provinceCode",
    "vatRate",
    "vatAmount",
    "vatCalcAmount",
    "vatOverrideAmount",
    "isCumulative",
    "lineItemVatCostRef"
})
public class LineItemVatType {

    @XmlElement(name = "SequenceNumber")
    protected String sequenceNumber;
    @XmlElement(name = "VatCodeGid")
    protected GLogXMLGidType vatCodeGid;
    @XmlElement(name = "CountryCode3Gid")
    protected GLogXMLGidType countryCode3Gid;
    @XmlElement(name = "ProvinceCode")
    protected String provinceCode;
    @XmlElement(name = "VatRate")
    protected String vatRate;
    @XmlElement(name = "VatAmount")
    protected GLogXMLFinancialAmountType vatAmount;
    @XmlElement(name = "VatCalcAmount")
    protected GLogXMLFinancialAmountType vatCalcAmount;
    @XmlElement(name = "VatOverrideAmount")
    protected GLogXMLFinancialAmountType vatOverrideAmount;
    @XmlElement(name = "IsCumulative")
    protected String isCumulative;
    @XmlElement(name = "LineItemVatCostRef")
    protected List<LineItemVatCostRefType> lineItemVatCostRef;

    /**
     * Obtém o valor da propriedade sequenceNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSequenceNumber() {
        return sequenceNumber;
    }

    /**
     * Define o valor da propriedade sequenceNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSequenceNumber(String value) {
        this.sequenceNumber = value;
    }

    /**
     * Obtém o valor da propriedade vatCodeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getVatCodeGid() {
        return vatCodeGid;
    }

    /**
     * Define o valor da propriedade vatCodeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setVatCodeGid(GLogXMLGidType value) {
        this.vatCodeGid = value;
    }

    /**
     * Obtém o valor da propriedade countryCode3Gid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getCountryCode3Gid() {
        return countryCode3Gid;
    }

    /**
     * Define o valor da propriedade countryCode3Gid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setCountryCode3Gid(GLogXMLGidType value) {
        this.countryCode3Gid = value;
    }

    /**
     * Obtém o valor da propriedade provinceCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProvinceCode() {
        return provinceCode;
    }

    /**
     * Define o valor da propriedade provinceCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProvinceCode(String value) {
        this.provinceCode = value;
    }

    /**
     * Obtém o valor da propriedade vatRate.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVatRate() {
        return vatRate;
    }

    /**
     * Define o valor da propriedade vatRate.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVatRate(String value) {
        this.vatRate = value;
    }

    /**
     * Obtém o valor da propriedade vatAmount.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getVatAmount() {
        return vatAmount;
    }

    /**
     * Define o valor da propriedade vatAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setVatAmount(GLogXMLFinancialAmountType value) {
        this.vatAmount = value;
    }

    /**
     * Obtém o valor da propriedade vatCalcAmount.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getVatCalcAmount() {
        return vatCalcAmount;
    }

    /**
     * Define o valor da propriedade vatCalcAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setVatCalcAmount(GLogXMLFinancialAmountType value) {
        this.vatCalcAmount = value;
    }

    /**
     * Obtém o valor da propriedade vatOverrideAmount.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getVatOverrideAmount() {
        return vatOverrideAmount;
    }

    /**
     * Define o valor da propriedade vatOverrideAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setVatOverrideAmount(GLogXMLFinancialAmountType value) {
        this.vatOverrideAmount = value;
    }

    /**
     * Obtém o valor da propriedade isCumulative.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsCumulative() {
        return isCumulative;
    }

    /**
     * Define o valor da propriedade isCumulative.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsCumulative(String value) {
        this.isCumulative = value;
    }

    /**
     * Gets the value of the lineItemVatCostRef property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the lineItemVatCostRef property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLineItemVatCostRef().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LineItemVatCostRefType }
     * 
     * 
     */
    public List<LineItemVatCostRefType> getLineItemVatCostRef() {
        if (lineItemVatCostRef == null) {
            lineItemVatCostRef = new ArrayList<LineItemVatCostRefType>();
        }
        return this.lineItemVatCostRef;
    }

}
