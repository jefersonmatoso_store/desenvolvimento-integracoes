
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.*;


/**
 * <p>Classe Java de anonymous complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransmissionAck"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "transmissionAck"
})
@XmlRootElement(name = "publishResponse", namespace = "http://xmlns.oracle.com/apps/otm/TransmissionService")
public class PublishResponse {

    @XmlElement(name = "TransmissionAck", required = true)
    protected TransmissionAck transmissionAck;

    /**
     * Obtém o valor da propriedade transmissionAck.
     * 
     * @return
     *     possible object is
     *     {@link TransmissionAck }
     *     
     */
    public TransmissionAck getTransmissionAck() {
        return transmissionAck;
    }

    /**
     * Define o valor da propriedade transmissionAck.
     * 
     * @param value
     *     allowed object is
     *     {@link TransmissionAck }
     *     
     */
    public void setTransmissionAck(TransmissionAck value) {
        this.transmissionAck = value;
    }

}
