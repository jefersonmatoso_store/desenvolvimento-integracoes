
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * (Outbound) Restricted party matches to the line party involved in business transaction.
 *          
 * 
 * <p>Classe Java de RestrictedPartyScreeningResultType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="RestrictedPartyScreeningResultType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GtmContact" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}GtmContactType"/>
 *         &lt;element name="IsPassed" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RestrictedPartyScreeningResultType", namespace = "http://xmlns.oracle.com/apps/gtm/transmission/v6.4", propOrder = {
    "gtmContact",
    "isPassed"
})
public class RestrictedPartyScreeningResultType {

    @XmlElement(name = "GtmContact", required = true)
    protected GtmContactType gtmContact;
    @XmlElement(name = "IsPassed", required = true)
    protected String isPassed;

    /**
     * Obtém o valor da propriedade gtmContact.
     * 
     * @return
     *     possible object is
     *     {@link GtmContactType }
     *     
     */
    public GtmContactType getGtmContact() {
        return gtmContact;
    }

    /**
     * Define o valor da propriedade gtmContact.
     * 
     * @param value
     *     allowed object is
     *     {@link GtmContactType }
     *     
     */
    public void setGtmContact(GtmContactType value) {
        this.gtmContact = value;
    }

    /**
     * Obtém o valor da propriedade isPassed.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsPassed() {
        return isPassed;
    }

    /**
     * Define o valor da propriedade isPassed.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsPassed(String value) {
        this.isPassed = value;
    }

}
