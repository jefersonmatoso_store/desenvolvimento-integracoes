
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * Specifies time window preferences for appointment on a location for a service provider.
 * 
 * <p>Classe Java de LocationServProvPrefType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="LocationServProvPrefType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="LocationServProvPrefGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="LocationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="ServiceLocationGid" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="LocationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="ServiceProviderGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="MaxSlotsPerDay" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LocationServProvPrefDetail" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LocationServProvPrefDetailType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LocationServProvPrefType", propOrder = {
    "locationServProvPrefGid",
    "locationGid",
    "serviceLocationGid",
    "serviceProviderGid",
    "maxSlotsPerDay",
    "locationServProvPrefDetail"
})
public class LocationServProvPrefType {

    @XmlElement(name = "LocationServProvPrefGid", required = true)
    protected GLogXMLGidType locationServProvPrefGid;
    @XmlElement(name = "LocationGid")
    protected GLogXMLGidType locationGid;
    @XmlElement(name = "ServiceLocationGid")
    protected LocationServProvPrefType.ServiceLocationGid serviceLocationGid;
    @XmlElement(name = "ServiceProviderGid")
    protected GLogXMLGidType serviceProviderGid;
    @XmlElement(name = "MaxSlotsPerDay")
    protected String maxSlotsPerDay;
    @XmlElement(name = "LocationServProvPrefDetail")
    protected List<LocationServProvPrefDetailType> locationServProvPrefDetail;

    /**
     * Obtém o valor da propriedade locationServProvPrefGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getLocationServProvPrefGid() {
        return locationServProvPrefGid;
    }

    /**
     * Define o valor da propriedade locationServProvPrefGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setLocationServProvPrefGid(GLogXMLGidType value) {
        this.locationServProvPrefGid = value;
    }

    /**
     * Obtém o valor da propriedade locationGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getLocationGid() {
        return locationGid;
    }

    /**
     * Define o valor da propriedade locationGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setLocationGid(GLogXMLGidType value) {
        this.locationGid = value;
    }

    /**
     * Obtém o valor da propriedade serviceLocationGid.
     * 
     * @return
     *     possible object is
     *     {@link LocationServProvPrefType.ServiceLocationGid }
     *     
     */
    public LocationServProvPrefType.ServiceLocationGid getServiceLocationGid() {
        return serviceLocationGid;
    }

    /**
     * Define o valor da propriedade serviceLocationGid.
     * 
     * @param value
     *     allowed object is
     *     {@link LocationServProvPrefType.ServiceLocationGid }
     *     
     */
    public void setServiceLocationGid(LocationServProvPrefType.ServiceLocationGid value) {
        this.serviceLocationGid = value;
    }

    /**
     * Obtém o valor da propriedade serviceProviderGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getServiceProviderGid() {
        return serviceProviderGid;
    }

    /**
     * Define o valor da propriedade serviceProviderGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setServiceProviderGid(GLogXMLGidType value) {
        this.serviceProviderGid = value;
    }

    /**
     * Obtém o valor da propriedade maxSlotsPerDay.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMaxSlotsPerDay() {
        return maxSlotsPerDay;
    }

    /**
     * Define o valor da propriedade maxSlotsPerDay.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMaxSlotsPerDay(String value) {
        this.maxSlotsPerDay = value;
    }

    /**
     * Gets the value of the locationServProvPrefDetail property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the locationServProvPrefDetail property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLocationServProvPrefDetail().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LocationServProvPrefDetailType }
     * 
     * 
     */
    public List<LocationServProvPrefDetailType> getLocationServProvPrefDetail() {
        if (locationServProvPrefDetail == null) {
            locationServProvPrefDetail = new ArrayList<LocationServProvPrefDetailType>();
        }
        return this.locationServProvPrefDetail;
    }


    /**
     * <p>Classe Java de anonymous complex type.
     * 
     * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="LocationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "locationGid"
    })
    public static class ServiceLocationGid {

        @XmlElement(name = "LocationGid", required = true)
        protected GLogXMLGidType locationGid;

        /**
         * Obtém o valor da propriedade locationGid.
         * 
         * @return
         *     possible object is
         *     {@link GLogXMLGidType }
         *     
         */
        public GLogXMLGidType getLocationGid() {
            return locationGid;
        }

        /**
         * Define o valor da propriedade locationGid.
         * 
         * @param value
         *     allowed object is
         *     {@link GLogXMLGidType }
         *     
         */
        public void setLocationGid(GLogXMLGidType value) {
            this.locationGid = value;
        }

    }

}
