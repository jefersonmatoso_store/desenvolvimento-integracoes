
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * Specifies the agent contact for a given agent.
 * 
 * <p>Classe Java de ShippingAgentContactType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="ShippingAgentContactType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ShippingAgentContactGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="ShippingAgent" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ShippingAgentType"/>
 *         &lt;element name="ShippingAgentTypeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="InvolvedPartyQualifierGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="InvolvedPartyLocationRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocRefType" minOccurs="0"/>
 *         &lt;element name="ContactRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ContactRefType"/>
 *         &lt;element name="ComMethodGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="IsActive" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="EffectiveDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType"/>
 *         &lt;element name="ExpirationDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType"/>
 *         &lt;element name="ModeProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="DestLocationRegionGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLRegionGidType" minOccurs="0"/>
 *         &lt;element name="ShipperLocationProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocProfileType" minOccurs="0"/>
 *         &lt;element name="ConsigneeLocationProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocProfileType" minOccurs="0"/>
 *         &lt;element name="IsPrimary" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsInternal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NFRCRuleGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="IsAllowHouseCollect" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MaxHouseCollectAmount" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/>
 *         &lt;element name="MasterBLType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PaymentMethodCodeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="MasterBLConsigneeInfo" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}MasterBLConsigneeInfoType" minOccurs="0"/>
 *         &lt;element name="MasterBLNotifyInfo" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}MasterBLNotifyInfoType" minOccurs="0"/>
 *         &lt;element name="MasterBLAlsoNotifyInfo" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}MasterBLNotifyInfoType" minOccurs="0"/>
 *         &lt;element name="ShippingAgentContactNote" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ShippingAgentContactNoteType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="ShippingAgentContactProfit" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ShippingAgentContactProfitType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ShippingAgentContactType", propOrder = {
    "shippingAgentContactGid",
    "shippingAgent",
    "shippingAgentTypeGid",
    "involvedPartyQualifierGid",
    "involvedPartyLocationRef",
    "contactRef",
    "comMethodGid",
    "isActive",
    "effectiveDate",
    "expirationDate",
    "modeProfileGid",
    "destLocationRegionGid",
    "shipperLocationProfileGid",
    "consigneeLocationProfileGid",
    "isPrimary",
    "isInternal",
    "nfrcRuleGid",
    "isAllowHouseCollect",
    "maxHouseCollectAmount",
    "masterBLType",
    "paymentMethodCodeGid",
    "masterBLConsigneeInfo",
    "masterBLNotifyInfo",
    "masterBLAlsoNotifyInfo",
    "shippingAgentContactNote",
    "shippingAgentContactProfit"
})
public class ShippingAgentContactType {

    @XmlElement(name = "ShippingAgentContactGid", required = true)
    protected GLogXMLGidType shippingAgentContactGid;
    @XmlElement(name = "ShippingAgent", required = true)
    protected ShippingAgentType shippingAgent;
    @XmlElement(name = "ShippingAgentTypeGid")
    protected GLogXMLGidType shippingAgentTypeGid;
    @XmlElement(name = "InvolvedPartyQualifierGid", required = true)
    protected GLogXMLGidType involvedPartyQualifierGid;
    @XmlElement(name = "InvolvedPartyLocationRef")
    protected GLogXMLLocRefType involvedPartyLocationRef;
    @XmlElement(name = "ContactRef", required = true)
    protected ContactRefType contactRef;
    @XmlElement(name = "ComMethodGid", required = true)
    protected GLogXMLGidType comMethodGid;
    @XmlElement(name = "IsActive", required = true)
    protected String isActive;
    @XmlElement(name = "EffectiveDate", required = true)
    protected GLogDateTimeType effectiveDate;
    @XmlElement(name = "ExpirationDate", required = true)
    protected GLogDateTimeType expirationDate;
    @XmlElement(name = "ModeProfileGid")
    protected GLogXMLGidType modeProfileGid;
    @XmlElement(name = "DestLocationRegionGid")
    protected GLogXMLRegionGidType destLocationRegionGid;
    @XmlElement(name = "ShipperLocationProfileGid")
    protected GLogXMLLocProfileType shipperLocationProfileGid;
    @XmlElement(name = "ConsigneeLocationProfileGid")
    protected GLogXMLLocProfileType consigneeLocationProfileGid;
    @XmlElement(name = "IsPrimary")
    protected String isPrimary;
    @XmlElement(name = "IsInternal")
    protected String isInternal;
    @XmlElement(name = "NFRCRuleGid")
    protected GLogXMLGidType nfrcRuleGid;
    @XmlElement(name = "IsAllowHouseCollect")
    protected String isAllowHouseCollect;
    @XmlElement(name = "MaxHouseCollectAmount")
    protected GLogXMLFinancialAmountType maxHouseCollectAmount;
    @XmlElement(name = "MasterBLType")
    protected String masterBLType;
    @XmlElement(name = "PaymentMethodCodeGid")
    protected GLogXMLGidType paymentMethodCodeGid;
    @XmlElement(name = "MasterBLConsigneeInfo")
    protected MasterBLConsigneeInfoType masterBLConsigneeInfo;
    @XmlElement(name = "MasterBLNotifyInfo")
    protected MasterBLNotifyInfoType masterBLNotifyInfo;
    @XmlElement(name = "MasterBLAlsoNotifyInfo")
    protected MasterBLNotifyInfoType masterBLAlsoNotifyInfo;
    @XmlElement(name = "ShippingAgentContactNote")
    protected List<ShippingAgentContactNoteType> shippingAgentContactNote;
    @XmlElement(name = "ShippingAgentContactProfit")
    protected List<ShippingAgentContactProfitType> shippingAgentContactProfit;

    /**
     * Obtém o valor da propriedade shippingAgentContactGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getShippingAgentContactGid() {
        return shippingAgentContactGid;
    }

    /**
     * Define o valor da propriedade shippingAgentContactGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setShippingAgentContactGid(GLogXMLGidType value) {
        this.shippingAgentContactGid = value;
    }

    /**
     * Obtém o valor da propriedade shippingAgent.
     * 
     * @return
     *     possible object is
     *     {@link ShippingAgentType }
     *     
     */
    public ShippingAgentType getShippingAgent() {
        return shippingAgent;
    }

    /**
     * Define o valor da propriedade shippingAgent.
     * 
     * @param value
     *     allowed object is
     *     {@link ShippingAgentType }
     *     
     */
    public void setShippingAgent(ShippingAgentType value) {
        this.shippingAgent = value;
    }

    /**
     * Obtém o valor da propriedade shippingAgentTypeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getShippingAgentTypeGid() {
        return shippingAgentTypeGid;
    }

    /**
     * Define o valor da propriedade shippingAgentTypeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setShippingAgentTypeGid(GLogXMLGidType value) {
        this.shippingAgentTypeGid = value;
    }

    /**
     * Obtém o valor da propriedade involvedPartyQualifierGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getInvolvedPartyQualifierGid() {
        return involvedPartyQualifierGid;
    }

    /**
     * Define o valor da propriedade involvedPartyQualifierGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setInvolvedPartyQualifierGid(GLogXMLGidType value) {
        this.involvedPartyQualifierGid = value;
    }

    /**
     * Obtém o valor da propriedade involvedPartyLocationRef.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public GLogXMLLocRefType getInvolvedPartyLocationRef() {
        return involvedPartyLocationRef;
    }

    /**
     * Define o valor da propriedade involvedPartyLocationRef.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public void setInvolvedPartyLocationRef(GLogXMLLocRefType value) {
        this.involvedPartyLocationRef = value;
    }

    /**
     * Obtém o valor da propriedade contactRef.
     * 
     * @return
     *     possible object is
     *     {@link ContactRefType }
     *     
     */
    public ContactRefType getContactRef() {
        return contactRef;
    }

    /**
     * Define o valor da propriedade contactRef.
     * 
     * @param value
     *     allowed object is
     *     {@link ContactRefType }
     *     
     */
    public void setContactRef(ContactRefType value) {
        this.contactRef = value;
    }

    /**
     * Obtém o valor da propriedade comMethodGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getComMethodGid() {
        return comMethodGid;
    }

    /**
     * Define o valor da propriedade comMethodGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setComMethodGid(GLogXMLGidType value) {
        this.comMethodGid = value;
    }

    /**
     * Obtém o valor da propriedade isActive.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsActive() {
        return isActive;
    }

    /**
     * Define o valor da propriedade isActive.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsActive(String value) {
        this.isActive = value;
    }

    /**
     * Obtém o valor da propriedade effectiveDate.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getEffectiveDate() {
        return effectiveDate;
    }

    /**
     * Define o valor da propriedade effectiveDate.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setEffectiveDate(GLogDateTimeType value) {
        this.effectiveDate = value;
    }

    /**
     * Obtém o valor da propriedade expirationDate.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getExpirationDate() {
        return expirationDate;
    }

    /**
     * Define o valor da propriedade expirationDate.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setExpirationDate(GLogDateTimeType value) {
        this.expirationDate = value;
    }

    /**
     * Obtém o valor da propriedade modeProfileGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getModeProfileGid() {
        return modeProfileGid;
    }

    /**
     * Define o valor da propriedade modeProfileGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setModeProfileGid(GLogXMLGidType value) {
        this.modeProfileGid = value;
    }

    /**
     * Obtém o valor da propriedade destLocationRegionGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLRegionGidType }
     *     
     */
    public GLogXMLRegionGidType getDestLocationRegionGid() {
        return destLocationRegionGid;
    }

    /**
     * Define o valor da propriedade destLocationRegionGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLRegionGidType }
     *     
     */
    public void setDestLocationRegionGid(GLogXMLRegionGidType value) {
        this.destLocationRegionGid = value;
    }

    /**
     * Obtém o valor da propriedade shipperLocationProfileGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocProfileType }
     *     
     */
    public GLogXMLLocProfileType getShipperLocationProfileGid() {
        return shipperLocationProfileGid;
    }

    /**
     * Define o valor da propriedade shipperLocationProfileGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocProfileType }
     *     
     */
    public void setShipperLocationProfileGid(GLogXMLLocProfileType value) {
        this.shipperLocationProfileGid = value;
    }

    /**
     * Obtém o valor da propriedade consigneeLocationProfileGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocProfileType }
     *     
     */
    public GLogXMLLocProfileType getConsigneeLocationProfileGid() {
        return consigneeLocationProfileGid;
    }

    /**
     * Define o valor da propriedade consigneeLocationProfileGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocProfileType }
     *     
     */
    public void setConsigneeLocationProfileGid(GLogXMLLocProfileType value) {
        this.consigneeLocationProfileGid = value;
    }

    /**
     * Obtém o valor da propriedade isPrimary.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsPrimary() {
        return isPrimary;
    }

    /**
     * Define o valor da propriedade isPrimary.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsPrimary(String value) {
        this.isPrimary = value;
    }

    /**
     * Obtém o valor da propriedade isInternal.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsInternal() {
        return isInternal;
    }

    /**
     * Define o valor da propriedade isInternal.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsInternal(String value) {
        this.isInternal = value;
    }

    /**
     * Obtém o valor da propriedade nfrcRuleGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getNFRCRuleGid() {
        return nfrcRuleGid;
    }

    /**
     * Define o valor da propriedade nfrcRuleGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setNFRCRuleGid(GLogXMLGidType value) {
        this.nfrcRuleGid = value;
    }

    /**
     * Obtém o valor da propriedade isAllowHouseCollect.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsAllowHouseCollect() {
        return isAllowHouseCollect;
    }

    /**
     * Define o valor da propriedade isAllowHouseCollect.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsAllowHouseCollect(String value) {
        this.isAllowHouseCollect = value;
    }

    /**
     * Obtém o valor da propriedade maxHouseCollectAmount.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getMaxHouseCollectAmount() {
        return maxHouseCollectAmount;
    }

    /**
     * Define o valor da propriedade maxHouseCollectAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setMaxHouseCollectAmount(GLogXMLFinancialAmountType value) {
        this.maxHouseCollectAmount = value;
    }

    /**
     * Obtém o valor da propriedade masterBLType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMasterBLType() {
        return masterBLType;
    }

    /**
     * Define o valor da propriedade masterBLType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMasterBLType(String value) {
        this.masterBLType = value;
    }

    /**
     * Obtém o valor da propriedade paymentMethodCodeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getPaymentMethodCodeGid() {
        return paymentMethodCodeGid;
    }

    /**
     * Define o valor da propriedade paymentMethodCodeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setPaymentMethodCodeGid(GLogXMLGidType value) {
        this.paymentMethodCodeGid = value;
    }

    /**
     * Obtém o valor da propriedade masterBLConsigneeInfo.
     * 
     * @return
     *     possible object is
     *     {@link MasterBLConsigneeInfoType }
     *     
     */
    public MasterBLConsigneeInfoType getMasterBLConsigneeInfo() {
        return masterBLConsigneeInfo;
    }

    /**
     * Define o valor da propriedade masterBLConsigneeInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link MasterBLConsigneeInfoType }
     *     
     */
    public void setMasterBLConsigneeInfo(MasterBLConsigneeInfoType value) {
        this.masterBLConsigneeInfo = value;
    }

    /**
     * Obtém o valor da propriedade masterBLNotifyInfo.
     * 
     * @return
     *     possible object is
     *     {@link MasterBLNotifyInfoType }
     *     
     */
    public MasterBLNotifyInfoType getMasterBLNotifyInfo() {
        return masterBLNotifyInfo;
    }

    /**
     * Define o valor da propriedade masterBLNotifyInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link MasterBLNotifyInfoType }
     *     
     */
    public void setMasterBLNotifyInfo(MasterBLNotifyInfoType value) {
        this.masterBLNotifyInfo = value;
    }

    /**
     * Obtém o valor da propriedade masterBLAlsoNotifyInfo.
     * 
     * @return
     *     possible object is
     *     {@link MasterBLNotifyInfoType }
     *     
     */
    public MasterBLNotifyInfoType getMasterBLAlsoNotifyInfo() {
        return masterBLAlsoNotifyInfo;
    }

    /**
     * Define o valor da propriedade masterBLAlsoNotifyInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link MasterBLNotifyInfoType }
     *     
     */
    public void setMasterBLAlsoNotifyInfo(MasterBLNotifyInfoType value) {
        this.masterBLAlsoNotifyInfo = value;
    }

    /**
     * Gets the value of the shippingAgentContactNote property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the shippingAgentContactNote property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getShippingAgentContactNote().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ShippingAgentContactNoteType }
     * 
     * 
     */
    public List<ShippingAgentContactNoteType> getShippingAgentContactNote() {
        if (shippingAgentContactNote == null) {
            shippingAgentContactNote = new ArrayList<ShippingAgentContactNoteType>();
        }
        return this.shippingAgentContactNote;
    }

    /**
     * Gets the value of the shippingAgentContactProfit property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the shippingAgentContactProfit property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getShippingAgentContactProfit().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ShippingAgentContactProfitType }
     * 
     * 
     */
    public List<ShippingAgentContactProfitType> getShippingAgentContactProfit() {
        if (shippingAgentContactProfit == null) {
            shippingAgentContactProfit = new ArrayList<ShippingAgentContactProfitType>();
        }
        return this.shippingAgentContactProfit;
    }

}
