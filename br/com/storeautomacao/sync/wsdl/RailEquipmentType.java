
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * 
 *             RailEquipment is an element of RailDetail used to specify rail specific equipment information on an invoice.
 *          
 * 
 * <p>Classe Java de RailEquipmentType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="RailEquipmentType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SequenceNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EquipmentPrefix" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EquipmentNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EquipmentInitialNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SEquipmentGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="EquipmentSeal" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}EquipmentSealType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="EquipmentDescriptionCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Weight" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}WeightType" minOccurs="0"/>
 *         &lt;element name="TareWeight" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLWeightType" minOccurs="0"/>
 *         &lt;element name="Length" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LengthType" minOccurs="0"/>
 *         &lt;element name="Dunnage" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLWeightType" minOccurs="0"/>
 *         &lt;element name="OwnershipCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RailEquipmentType", propOrder = {
    "sequenceNumber",
    "equipmentPrefix",
    "equipmentNumber",
    "equipmentInitialNumber",
    "sEquipmentGid",
    "equipmentSeal",
    "equipmentDescriptionCode",
    "weight",
    "tareWeight",
    "length",
    "dunnage",
    "ownershipCode"
})
public class RailEquipmentType {

    @XmlElement(name = "SequenceNumber")
    protected String sequenceNumber;
    @XmlElement(name = "EquipmentPrefix")
    protected String equipmentPrefix;
    @XmlElement(name = "EquipmentNumber")
    protected String equipmentNumber;
    @XmlElement(name = "EquipmentInitialNumber")
    protected String equipmentInitialNumber;
    @XmlElement(name = "SEquipmentGid")
    protected GLogXMLGidType sEquipmentGid;
    @XmlElement(name = "EquipmentSeal")
    protected List<EquipmentSealType> equipmentSeal;
    @XmlElement(name = "EquipmentDescriptionCode")
    protected String equipmentDescriptionCode;
    @XmlElement(name = "Weight")
    protected WeightType weight;
    @XmlElement(name = "TareWeight")
    protected GLogXMLWeightType tareWeight;
    @XmlElement(name = "Length")
    protected LengthType length;
    @XmlElement(name = "Dunnage")
    protected GLogXMLWeightType dunnage;
    @XmlElement(name = "OwnershipCode")
    protected String ownershipCode;

    /**
     * Obtém o valor da propriedade sequenceNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSequenceNumber() {
        return sequenceNumber;
    }

    /**
     * Define o valor da propriedade sequenceNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSequenceNumber(String value) {
        this.sequenceNumber = value;
    }

    /**
     * Obtém o valor da propriedade equipmentPrefix.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEquipmentPrefix() {
        return equipmentPrefix;
    }

    /**
     * Define o valor da propriedade equipmentPrefix.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEquipmentPrefix(String value) {
        this.equipmentPrefix = value;
    }

    /**
     * Obtém o valor da propriedade equipmentNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEquipmentNumber() {
        return equipmentNumber;
    }

    /**
     * Define o valor da propriedade equipmentNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEquipmentNumber(String value) {
        this.equipmentNumber = value;
    }

    /**
     * Obtém o valor da propriedade equipmentInitialNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEquipmentInitialNumber() {
        return equipmentInitialNumber;
    }

    /**
     * Define o valor da propriedade equipmentInitialNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEquipmentInitialNumber(String value) {
        this.equipmentInitialNumber = value;
    }

    /**
     * Obtém o valor da propriedade sEquipmentGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getSEquipmentGid() {
        return sEquipmentGid;
    }

    /**
     * Define o valor da propriedade sEquipmentGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setSEquipmentGid(GLogXMLGidType value) {
        this.sEquipmentGid = value;
    }

    /**
     * Gets the value of the equipmentSeal property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the equipmentSeal property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEquipmentSeal().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EquipmentSealType }
     * 
     * 
     */
    public List<EquipmentSealType> getEquipmentSeal() {
        if (equipmentSeal == null) {
            equipmentSeal = new ArrayList<EquipmentSealType>();
        }
        return this.equipmentSeal;
    }

    /**
     * Obtém o valor da propriedade equipmentDescriptionCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEquipmentDescriptionCode() {
        return equipmentDescriptionCode;
    }

    /**
     * Define o valor da propriedade equipmentDescriptionCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEquipmentDescriptionCode(String value) {
        this.equipmentDescriptionCode = value;
    }

    /**
     * Obtém o valor da propriedade weight.
     * 
     * @return
     *     possible object is
     *     {@link WeightType }
     *     
     */
    public WeightType getWeight() {
        return weight;
    }

    /**
     * Define o valor da propriedade weight.
     * 
     * @param value
     *     allowed object is
     *     {@link WeightType }
     *     
     */
    public void setWeight(WeightType value) {
        this.weight = value;
    }

    /**
     * Obtém o valor da propriedade tareWeight.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLWeightType }
     *     
     */
    public GLogXMLWeightType getTareWeight() {
        return tareWeight;
    }

    /**
     * Define o valor da propriedade tareWeight.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLWeightType }
     *     
     */
    public void setTareWeight(GLogXMLWeightType value) {
        this.tareWeight = value;
    }

    /**
     * Obtém o valor da propriedade length.
     * 
     * @return
     *     possible object is
     *     {@link LengthType }
     *     
     */
    public LengthType getLength() {
        return length;
    }

    /**
     * Define o valor da propriedade length.
     * 
     * @param value
     *     allowed object is
     *     {@link LengthType }
     *     
     */
    public void setLength(LengthType value) {
        this.length = value;
    }

    /**
     * Obtém o valor da propriedade dunnage.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLWeightType }
     *     
     */
    public GLogXMLWeightType getDunnage() {
        return dunnage;
    }

    /**
     * Define o valor da propriedade dunnage.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLWeightType }
     *     
     */
    public void setDunnage(GLogXMLWeightType value) {
        this.dunnage = value;
    }

    /**
     * Obtém o valor da propriedade ownershipCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOwnershipCode() {
        return ownershipCode;
    }

    /**
     * Define o valor da propriedade ownershipCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOwnershipCode(String value) {
        this.ownershipCode = value;
    }

}
