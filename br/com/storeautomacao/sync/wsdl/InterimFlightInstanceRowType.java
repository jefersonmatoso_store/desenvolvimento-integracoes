
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.*;


/**
 * <p>Classe Java de InterimFlightInstanceRowType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="InterimFlightInstanceRowType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FLIGHT_INSTANCE_ID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FLIGHT_NUMBER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RECORD_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LEAVE_TIME_STAMP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ARRIVAL_TIME_STAMP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SRC_LOCATION_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DEST_LOCATION_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SERVPROV_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SERVICE_CLASS" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="STOP_COUNT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EQUIPMENT_GROUP_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="VEHICLE_TYPE_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InterimFlightInstanceRowType", propOrder = {
    "flightinstanceid",
    "flightnumber",
    "recordcode",
    "leavetimestamp",
    "arrivaltimestamp",
    "srclocationgid",
    "destlocationgid",
    "servprovgid",
    "serviceclass",
    "stopcount",
    "equipmentgroupgid",
    "domainname",
    "insertdate",
    "updatedate",
    "insertuser",
    "updateuser",
    "vehicletypegid"
})
public class InterimFlightInstanceRowType {

    @XmlElement(name = "FLIGHT_INSTANCE_ID")
    protected String flightinstanceid;
    @XmlElement(name = "FLIGHT_NUMBER")
    protected String flightnumber;
    @XmlElement(name = "RECORD_CODE")
    protected String recordcode;
    @XmlElement(name = "LEAVE_TIME_STAMP")
    protected String leavetimestamp;
    @XmlElement(name = "ARRIVAL_TIME_STAMP")
    protected String arrivaltimestamp;
    @XmlElement(name = "SRC_LOCATION_GID")
    protected String srclocationgid;
    @XmlElement(name = "DEST_LOCATION_GID")
    protected String destlocationgid;
    @XmlElement(name = "SERVPROV_GID")
    protected String servprovgid;
    @XmlElement(name = "SERVICE_CLASS")
    protected String serviceclass;
    @XmlElement(name = "STOP_COUNT")
    protected String stopcount;
    @XmlElement(name = "EQUIPMENT_GROUP_GID")
    protected String equipmentgroupgid;
    @XmlElement(name = "DOMAIN_NAME")
    protected String domainname;
    @XmlElement(name = "INSERT_DATE")
    protected String insertdate;
    @XmlElement(name = "UPDATE_DATE")
    protected String updatedate;
    @XmlElement(name = "INSERT_USER")
    protected String insertuser;
    @XmlElement(name = "UPDATE_USER")
    protected String updateuser;
    @XmlElement(name = "VEHICLE_TYPE_GID")
    protected String vehicletypegid;
    @XmlAttribute(name = "num")
    protected String num;

    /**
     * Obtém o valor da propriedade flightinstanceid.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFLIGHTINSTANCEID() {
        return flightinstanceid;
    }

    /**
     * Define o valor da propriedade flightinstanceid.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFLIGHTINSTANCEID(String value) {
        this.flightinstanceid = value;
    }

    /**
     * Obtém o valor da propriedade flightnumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFLIGHTNUMBER() {
        return flightnumber;
    }

    /**
     * Define o valor da propriedade flightnumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFLIGHTNUMBER(String value) {
        this.flightnumber = value;
    }

    /**
     * Obtém o valor da propriedade recordcode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRECORDCODE() {
        return recordcode;
    }

    /**
     * Define o valor da propriedade recordcode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRECORDCODE(String value) {
        this.recordcode = value;
    }

    /**
     * Obtém o valor da propriedade leavetimestamp.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLEAVETIMESTAMP() {
        return leavetimestamp;
    }

    /**
     * Define o valor da propriedade leavetimestamp.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLEAVETIMESTAMP(String value) {
        this.leavetimestamp = value;
    }

    /**
     * Obtém o valor da propriedade arrivaltimestamp.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getARRIVALTIMESTAMP() {
        return arrivaltimestamp;
    }

    /**
     * Define o valor da propriedade arrivaltimestamp.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setARRIVALTIMESTAMP(String value) {
        this.arrivaltimestamp = value;
    }

    /**
     * Obtém o valor da propriedade srclocationgid.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSRCLOCATIONGID() {
        return srclocationgid;
    }

    /**
     * Define o valor da propriedade srclocationgid.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSRCLOCATIONGID(String value) {
        this.srclocationgid = value;
    }

    /**
     * Obtém o valor da propriedade destlocationgid.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDESTLOCATIONGID() {
        return destlocationgid;
    }

    /**
     * Define o valor da propriedade destlocationgid.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDESTLOCATIONGID(String value) {
        this.destlocationgid = value;
    }

    /**
     * Obtém o valor da propriedade servprovgid.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSERVPROVGID() {
        return servprovgid;
    }

    /**
     * Define o valor da propriedade servprovgid.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSERVPROVGID(String value) {
        this.servprovgid = value;
    }

    /**
     * Obtém o valor da propriedade serviceclass.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSERVICECLASS() {
        return serviceclass;
    }

    /**
     * Define o valor da propriedade serviceclass.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSERVICECLASS(String value) {
        this.serviceclass = value;
    }

    /**
     * Obtém o valor da propriedade stopcount.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSTOPCOUNT() {
        return stopcount;
    }

    /**
     * Define o valor da propriedade stopcount.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSTOPCOUNT(String value) {
        this.stopcount = value;
    }

    /**
     * Obtém o valor da propriedade equipmentgroupgid.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEQUIPMENTGROUPGID() {
        return equipmentgroupgid;
    }

    /**
     * Define o valor da propriedade equipmentgroupgid.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEQUIPMENTGROUPGID(String value) {
        this.equipmentgroupgid = value;
    }

    /**
     * Obtém o valor da propriedade domainname.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDOMAINNAME() {
        return domainname;
    }

    /**
     * Define o valor da propriedade domainname.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDOMAINNAME(String value) {
        this.domainname = value;
    }

    /**
     * Obtém o valor da propriedade insertdate.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getINSERTDATE() {
        return insertdate;
    }

    /**
     * Define o valor da propriedade insertdate.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setINSERTDATE(String value) {
        this.insertdate = value;
    }

    /**
     * Obtém o valor da propriedade updatedate.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUPDATEDATE() {
        return updatedate;
    }

    /**
     * Define o valor da propriedade updatedate.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUPDATEDATE(String value) {
        this.updatedate = value;
    }

    /**
     * Obtém o valor da propriedade insertuser.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getINSERTUSER() {
        return insertuser;
    }

    /**
     * Define o valor da propriedade insertuser.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setINSERTUSER(String value) {
        this.insertuser = value;
    }

    /**
     * Obtém o valor da propriedade updateuser.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUPDATEUSER() {
        return updateuser;
    }

    /**
     * Define o valor da propriedade updateuser.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUPDATEUSER(String value) {
        this.updateuser = value;
    }

    /**
     * Obtém o valor da propriedade vehicletypegid.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVEHICLETYPEGID() {
        return vehicletypegid;
    }

    /**
     * Define o valor da propriedade vehicletypegid.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVEHICLETYPEGID(String value) {
        this.vehicletypegid = value;
    }

    /**
     * Obtém o valor da propriedade num.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNum() {
        return num;
    }

    /**
     * Define o valor da propriedade num.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNum(String value) {
        this.num = value;
    }

}
