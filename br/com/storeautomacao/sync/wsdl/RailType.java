
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * Contains elements used to support Rail functionality.
 * 
 * <p>Classe Java de RailType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="RailType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RouteCodeCombinationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="RouteCodeCombination" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLRouteCodeType" minOccurs="0"/>
 *         &lt;element name="RailInterModalPlanGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="CustomerRateCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="COFC_TOFC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RailReturnLocationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocGidType" minOccurs="0"/>
 *         &lt;element name="RailReturnRouteGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RailReturnRouteGidType" minOccurs="0"/>
 *         &lt;element name="Rule11_Indicator" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Rule11SecondShipment" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}Rule11SecondShipmentType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RailType", propOrder = {
    "routeCodeCombinationGid",
    "routeCodeCombination",
    "railInterModalPlanGid",
    "customerRateCode",
    "cofctofc",
    "railReturnLocationGid",
    "railReturnRouteGid",
    "rule11Indicator",
    "rule11SecondShipment"
})
public class RailType {

    @XmlElement(name = "RouteCodeCombinationGid")
    protected GLogXMLGidType routeCodeCombinationGid;
    @XmlElement(name = "RouteCodeCombination")
    protected GLogXMLRouteCodeType routeCodeCombination;
    @XmlElement(name = "RailInterModalPlanGid")
    protected GLogXMLGidType railInterModalPlanGid;
    @XmlElement(name = "CustomerRateCode")
    protected String customerRateCode;
    @XmlElement(name = "COFC_TOFC")
    protected String cofctofc;
    @XmlElement(name = "RailReturnLocationGid")
    protected GLogXMLLocGidType railReturnLocationGid;
    @XmlElement(name = "RailReturnRouteGid")
    protected RailReturnRouteGidType railReturnRouteGid;
    @XmlElement(name = "Rule11_Indicator")
    protected String rule11Indicator;
    @XmlElement(name = "Rule11SecondShipment")
    protected List<Rule11SecondShipmentType> rule11SecondShipment;

    /**
     * Obtém o valor da propriedade routeCodeCombinationGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRouteCodeCombinationGid() {
        return routeCodeCombinationGid;
    }

    /**
     * Define o valor da propriedade routeCodeCombinationGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRouteCodeCombinationGid(GLogXMLGidType value) {
        this.routeCodeCombinationGid = value;
    }

    /**
     * Obtém o valor da propriedade routeCodeCombination.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLRouteCodeType }
     *     
     */
    public GLogXMLRouteCodeType getRouteCodeCombination() {
        return routeCodeCombination;
    }

    /**
     * Define o valor da propriedade routeCodeCombination.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLRouteCodeType }
     *     
     */
    public void setRouteCodeCombination(GLogXMLRouteCodeType value) {
        this.routeCodeCombination = value;
    }

    /**
     * Obtém o valor da propriedade railInterModalPlanGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRailInterModalPlanGid() {
        return railInterModalPlanGid;
    }

    /**
     * Define o valor da propriedade railInterModalPlanGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRailInterModalPlanGid(GLogXMLGidType value) {
        this.railInterModalPlanGid = value;
    }

    /**
     * Obtém o valor da propriedade customerRateCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomerRateCode() {
        return customerRateCode;
    }

    /**
     * Define o valor da propriedade customerRateCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomerRateCode(String value) {
        this.customerRateCode = value;
    }

    /**
     * Obtém o valor da propriedade cofctofc.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCOFCTOFC() {
        return cofctofc;
    }

    /**
     * Define o valor da propriedade cofctofc.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCOFCTOFC(String value) {
        this.cofctofc = value;
    }

    /**
     * Obtém o valor da propriedade railReturnLocationGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocGidType }
     *     
     */
    public GLogXMLLocGidType getRailReturnLocationGid() {
        return railReturnLocationGid;
    }

    /**
     * Define o valor da propriedade railReturnLocationGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocGidType }
     *     
     */
    public void setRailReturnLocationGid(GLogXMLLocGidType value) {
        this.railReturnLocationGid = value;
    }

    /**
     * Obtém o valor da propriedade railReturnRouteGid.
     * 
     * @return
     *     possible object is
     *     {@link RailReturnRouteGidType }
     *     
     */
    public RailReturnRouteGidType getRailReturnRouteGid() {
        return railReturnRouteGid;
    }

    /**
     * Define o valor da propriedade railReturnRouteGid.
     * 
     * @param value
     *     allowed object is
     *     {@link RailReturnRouteGidType }
     *     
     */
    public void setRailReturnRouteGid(RailReturnRouteGidType value) {
        this.railReturnRouteGid = value;
    }

    /**
     * Obtém o valor da propriedade rule11Indicator.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRule11Indicator() {
        return rule11Indicator;
    }

    /**
     * Define o valor da propriedade rule11Indicator.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRule11Indicator(String value) {
        this.rule11Indicator = value;
    }

    /**
     * Gets the value of the rule11SecondShipment property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the rule11SecondShipment property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRule11SecondShipment().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Rule11SecondShipmentType }
     * 
     * 
     */
    public List<Rule11SecondShipmentType> getRule11SecondShipment() {
        if (rule11SecondShipment == null) {
            rule11SecondShipment = new ArrayList<Rule11SecondShipmentType>();
        }
        return this.rule11SecondShipment;
    }

}
