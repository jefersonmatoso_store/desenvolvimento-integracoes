
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * An XLaneNode is either a mileage address or region reference.
 * 
 * <p>Classe Java de XLaneNodeType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="XLaneNodeType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element name="MileageAddress" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}MileageAddressType"/>
 *         &lt;element name="RegionRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RegionRefType"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "XLaneNodeType", propOrder = {
    "mileageAddress",
    "regionRef"
})
public class XLaneNodeType {

    @XmlElement(name = "MileageAddress")
    protected MileageAddressType mileageAddress;
    @XmlElement(name = "RegionRef")
    protected RegionRefType regionRef;

    /**
     * Obtém o valor da propriedade mileageAddress.
     * 
     * @return
     *     possible object is
     *     {@link MileageAddressType }
     *     
     */
    public MileageAddressType getMileageAddress() {
        return mileageAddress;
    }

    /**
     * Define o valor da propriedade mileageAddress.
     * 
     * @param value
     *     allowed object is
     *     {@link MileageAddressType }
     *     
     */
    public void setMileageAddress(MileageAddressType value) {
        this.mileageAddress = value;
    }

    /**
     * Obtém o valor da propriedade regionRef.
     * 
     * @return
     *     possible object is
     *     {@link RegionRefType }
     *     
     */
    public RegionRefType getRegionRef() {
        return regionRef;
    }

    /**
     * Define o valor da propriedade regionRef.
     * 
     * @param value
     *     allowed object is
     *     {@link RegionRefType }
     *     
     */
    public void setRegionRef(RegionRefType value) {
        this.regionRef = value;
    }

}
