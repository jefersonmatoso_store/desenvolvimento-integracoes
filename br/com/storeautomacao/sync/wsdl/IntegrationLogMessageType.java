
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * An IntegrationLogMessage describes an error found in a transmission, transaction or any other type (like Message).
 * 
 * <p>Classe Java de IntegrationLogMessageType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="IntegrationLogMessageType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ILogSeqNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ITransactionNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ObjectGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="WrittenBy" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="IMessageClass" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="IMessageCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="IMessageText" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ProcessingErrorCodeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="DateTimeStamp" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType"/>
 *         &lt;element name="DomainName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="IntegrationLogDetail" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}IntegrationLogDetailType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IntegrationLogMessageType", propOrder = {
    "iLogSeqNo",
    "iTransactionNo",
    "objectGid",
    "writtenBy",
    "iMessageClass",
    "iMessageCode",
    "iMessageText",
    "processingErrorCodeGid",
    "dateTimeStamp",
    "domainName",
    "integrationLogDetail"
})
public class IntegrationLogMessageType {

    @XmlElement(name = "ILogSeqNo", required = true)
    protected String iLogSeqNo;
    @XmlElement(name = "ITransactionNo", required = true)
    protected String iTransactionNo;
    @XmlElement(name = "ObjectGid")
    protected GLogXMLGidType objectGid;
    @XmlElement(name = "WrittenBy", required = true)
    protected String writtenBy;
    @XmlElement(name = "IMessageClass", required = true)
    protected String iMessageClass;
    @XmlElement(name = "IMessageCode", required = true)
    protected String iMessageCode;
    @XmlElement(name = "IMessageText")
    protected String iMessageText;
    @XmlElement(name = "ProcessingErrorCodeGid")
    protected GLogXMLGidType processingErrorCodeGid;
    @XmlElement(name = "DateTimeStamp", required = true)
    protected GLogDateTimeType dateTimeStamp;
    @XmlElement(name = "DomainName", required = true)
    protected String domainName;
    @XmlElement(name = "IntegrationLogDetail")
    protected List<IntegrationLogDetailType> integrationLogDetail;

    /**
     * Obtém o valor da propriedade iLogSeqNo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getILogSeqNo() {
        return iLogSeqNo;
    }

    /**
     * Define o valor da propriedade iLogSeqNo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setILogSeqNo(String value) {
        this.iLogSeqNo = value;
    }

    /**
     * Obtém o valor da propriedade iTransactionNo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getITransactionNo() {
        return iTransactionNo;
    }

    /**
     * Define o valor da propriedade iTransactionNo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setITransactionNo(String value) {
        this.iTransactionNo = value;
    }

    /**
     * Obtém o valor da propriedade objectGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getObjectGid() {
        return objectGid;
    }

    /**
     * Define o valor da propriedade objectGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setObjectGid(GLogXMLGidType value) {
        this.objectGid = value;
    }

    /**
     * Obtém o valor da propriedade writtenBy.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWrittenBy() {
        return writtenBy;
    }

    /**
     * Define o valor da propriedade writtenBy.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWrittenBy(String value) {
        this.writtenBy = value;
    }

    /**
     * Obtém o valor da propriedade iMessageClass.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIMessageClass() {
        return iMessageClass;
    }

    /**
     * Define o valor da propriedade iMessageClass.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIMessageClass(String value) {
        this.iMessageClass = value;
    }

    /**
     * Obtém o valor da propriedade iMessageCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIMessageCode() {
        return iMessageCode;
    }

    /**
     * Define o valor da propriedade iMessageCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIMessageCode(String value) {
        this.iMessageCode = value;
    }

    /**
     * Obtém o valor da propriedade iMessageText.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIMessageText() {
        return iMessageText;
    }

    /**
     * Define o valor da propriedade iMessageText.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIMessageText(String value) {
        this.iMessageText = value;
    }

    /**
     * Obtém o valor da propriedade processingErrorCodeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getProcessingErrorCodeGid() {
        return processingErrorCodeGid;
    }

    /**
     * Define o valor da propriedade processingErrorCodeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setProcessingErrorCodeGid(GLogXMLGidType value) {
        this.processingErrorCodeGid = value;
    }

    /**
     * Obtém o valor da propriedade dateTimeStamp.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getDateTimeStamp() {
        return dateTimeStamp;
    }

    /**
     * Define o valor da propriedade dateTimeStamp.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setDateTimeStamp(GLogDateTimeType value) {
        this.dateTimeStamp = value;
    }

    /**
     * Obtém o valor da propriedade domainName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDomainName() {
        return domainName;
    }

    /**
     * Define o valor da propriedade domainName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDomainName(String value) {
        this.domainName = value;
    }

    /**
     * Gets the value of the integrationLogDetail property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the integrationLogDetail property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getIntegrationLogDetail().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link IntegrationLogDetailType }
     * 
     * 
     */
    public List<IntegrationLogDetailType> getIntegrationLogDetail() {
        if (integrationLogDetail == null) {
            integrationLogDetail = new ArrayList<IntegrationLogDetailType>();
        }
        return this.integrationLogDetail;
    }

}
