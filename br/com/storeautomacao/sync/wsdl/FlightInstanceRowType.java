
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.*;


/**
 * <p>Classe Java de FlightInstanceRowType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="FlightInstanceRowType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FLIGHT_INSTANCE_ID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="LEAVE_TIMESTAMP" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ARRIVAL_TIMESTAMP" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SRC_LOCATION_GID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="DEST_LOCATION_GID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ELAPSED_TIME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="FLIGHT_GID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ELAPSED_TIME_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ELAPSED_TIME_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FLIGHT" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlightType" minOccurs="0"/>
 *         &lt;element name="INTERIM_FLIGHT_INSTANCE" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}InterimFlightInstanceType" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FlightInstanceRowType", propOrder = {
    "flightinstanceid",
    "leavetimestamp",
    "arrivaltimestamp",
    "srclocationgid",
    "destlocationgid",
    "elapsedtime",
    "domainname",
    "flightgid",
    "elapsedtimeuomcode",
    "elapsedtimebase",
    "insertdate",
    "updatedate",
    "insertuser",
    "updateuser",
    "flight",
    "interimflightinstance"
})
public class FlightInstanceRowType {

    @XmlElement(name = "FLIGHT_INSTANCE_ID", required = true)
    protected String flightinstanceid;
    @XmlElement(name = "LEAVE_TIMESTAMP", required = true)
    protected String leavetimestamp;
    @XmlElement(name = "ARRIVAL_TIMESTAMP", required = true)
    protected String arrivaltimestamp;
    @XmlElement(name = "SRC_LOCATION_GID", required = true)
    protected String srclocationgid;
    @XmlElement(name = "DEST_LOCATION_GID", required = true)
    protected String destlocationgid;
    @XmlElement(name = "ELAPSED_TIME")
    protected String elapsedtime;
    @XmlElement(name = "DOMAIN_NAME", required = true)
    protected String domainname;
    @XmlElement(name = "FLIGHT_GID", required = true)
    protected String flightgid;
    @XmlElement(name = "ELAPSED_TIME_UOM_CODE")
    protected String elapsedtimeuomcode;
    @XmlElement(name = "ELAPSED_TIME_BASE")
    protected String elapsedtimebase;
    @XmlElement(name = "INSERT_DATE")
    protected String insertdate;
    @XmlElement(name = "UPDATE_DATE")
    protected String updatedate;
    @XmlElement(name = "INSERT_USER")
    protected String insertuser;
    @XmlElement(name = "UPDATE_USER")
    protected String updateuser;
    @XmlElement(name = "FLIGHT")
    protected FlightType flight;
    @XmlElement(name = "INTERIM_FLIGHT_INSTANCE")
    protected InterimFlightInstanceType interimflightinstance;
    @XmlAttribute(name = "num")
    protected String num;

    /**
     * Obtém o valor da propriedade flightinstanceid.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFLIGHTINSTANCEID() {
        return flightinstanceid;
    }

    /**
     * Define o valor da propriedade flightinstanceid.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFLIGHTINSTANCEID(String value) {
        this.flightinstanceid = value;
    }

    /**
     * Obtém o valor da propriedade leavetimestamp.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLEAVETIMESTAMP() {
        return leavetimestamp;
    }

    /**
     * Define o valor da propriedade leavetimestamp.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLEAVETIMESTAMP(String value) {
        this.leavetimestamp = value;
    }

    /**
     * Obtém o valor da propriedade arrivaltimestamp.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getARRIVALTIMESTAMP() {
        return arrivaltimestamp;
    }

    /**
     * Define o valor da propriedade arrivaltimestamp.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setARRIVALTIMESTAMP(String value) {
        this.arrivaltimestamp = value;
    }

    /**
     * Obtém o valor da propriedade srclocationgid.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSRCLOCATIONGID() {
        return srclocationgid;
    }

    /**
     * Define o valor da propriedade srclocationgid.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSRCLOCATIONGID(String value) {
        this.srclocationgid = value;
    }

    /**
     * Obtém o valor da propriedade destlocationgid.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDESTLOCATIONGID() {
        return destlocationgid;
    }

    /**
     * Define o valor da propriedade destlocationgid.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDESTLOCATIONGID(String value) {
        this.destlocationgid = value;
    }

    /**
     * Obtém o valor da propriedade elapsedtime.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getELAPSEDTIME() {
        return elapsedtime;
    }

    /**
     * Define o valor da propriedade elapsedtime.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setELAPSEDTIME(String value) {
        this.elapsedtime = value;
    }

    /**
     * Obtém o valor da propriedade domainname.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDOMAINNAME() {
        return domainname;
    }

    /**
     * Define o valor da propriedade domainname.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDOMAINNAME(String value) {
        this.domainname = value;
    }

    /**
     * Obtém o valor da propriedade flightgid.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFLIGHTGID() {
        return flightgid;
    }

    /**
     * Define o valor da propriedade flightgid.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFLIGHTGID(String value) {
        this.flightgid = value;
    }

    /**
     * Obtém o valor da propriedade elapsedtimeuomcode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getELAPSEDTIMEUOMCODE() {
        return elapsedtimeuomcode;
    }

    /**
     * Define o valor da propriedade elapsedtimeuomcode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setELAPSEDTIMEUOMCODE(String value) {
        this.elapsedtimeuomcode = value;
    }

    /**
     * Obtém o valor da propriedade elapsedtimebase.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getELAPSEDTIMEBASE() {
        return elapsedtimebase;
    }

    /**
     * Define o valor da propriedade elapsedtimebase.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setELAPSEDTIMEBASE(String value) {
        this.elapsedtimebase = value;
    }

    /**
     * Obtém o valor da propriedade insertdate.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getINSERTDATE() {
        return insertdate;
    }

    /**
     * Define o valor da propriedade insertdate.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setINSERTDATE(String value) {
        this.insertdate = value;
    }

    /**
     * Obtém o valor da propriedade updatedate.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUPDATEDATE() {
        return updatedate;
    }

    /**
     * Define o valor da propriedade updatedate.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUPDATEDATE(String value) {
        this.updatedate = value;
    }

    /**
     * Obtém o valor da propriedade insertuser.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getINSERTUSER() {
        return insertuser;
    }

    /**
     * Define o valor da propriedade insertuser.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setINSERTUSER(String value) {
        this.insertuser = value;
    }

    /**
     * Obtém o valor da propriedade updateuser.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUPDATEUSER() {
        return updateuser;
    }

    /**
     * Define o valor da propriedade updateuser.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUPDATEUSER(String value) {
        this.updateuser = value;
    }

    /**
     * Obtém o valor da propriedade flight.
     * 
     * @return
     *     possible object is
     *     {@link FlightType }
     *     
     */
    public FlightType getFLIGHT() {
        return flight;
    }

    /**
     * Define o valor da propriedade flight.
     * 
     * @param value
     *     allowed object is
     *     {@link FlightType }
     *     
     */
    public void setFLIGHT(FlightType value) {
        this.flight = value;
    }

    /**
     * Obtém o valor da propriedade interimflightinstance.
     * 
     * @return
     *     possible object is
     *     {@link InterimFlightInstanceType }
     *     
     */
    public InterimFlightInstanceType getINTERIMFLIGHTINSTANCE() {
        return interimflightinstance;
    }

    /**
     * Define o valor da propriedade interimflightinstance.
     * 
     * @param value
     *     allowed object is
     *     {@link InterimFlightInstanceType }
     *     
     */
    public void setINTERIMFLIGHTINSTANCE(InterimFlightInstanceType value) {
        this.interimflightinstance = value;
    }

    /**
     * Obtém o valor da propriedade num.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNum() {
        return num;
    }

    /**
     * Define o valor da propriedade num.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNum(String value) {
        this.num = value;
    }

}
