
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Holds classification information of the item.
 * 
 * <p>Classe Java de GtmItemClassificationType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="GtmItemClassificationType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GtmProdClassTypeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="ClassificationCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ClassificationStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ClassificationNotes" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ApproverNotes" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TradeDirection" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="GtmItemClassificationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GtmItemClassificationType", propOrder = {
    "gtmProdClassTypeGid",
    "classificationCode",
    "classificationStatus",
    "classificationNotes",
    "approverNotes",
    "tradeDirection",
    "gtmItemClassificationGid"
})
public class GtmItemClassificationType {

    @XmlElement(name = "GtmProdClassTypeGid", required = true)
    protected GLogXMLGidType gtmProdClassTypeGid;
    @XmlElement(name = "ClassificationCode", required = true)
    protected String classificationCode;
    @XmlElement(name = "ClassificationStatus")
    protected String classificationStatus;
    @XmlElement(name = "ClassificationNotes")
    protected String classificationNotes;
    @XmlElement(name = "ApproverNotes")
    protected String approverNotes;
    @XmlElement(name = "TradeDirection", required = true)
    protected String tradeDirection;
    @XmlElement(name = "GtmItemClassificationGid")
    protected GLogXMLGidType gtmItemClassificationGid;

    /**
     * Obtém o valor da propriedade gtmProdClassTypeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getGtmProdClassTypeGid() {
        return gtmProdClassTypeGid;
    }

    /**
     * Define o valor da propriedade gtmProdClassTypeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setGtmProdClassTypeGid(GLogXMLGidType value) {
        this.gtmProdClassTypeGid = value;
    }

    /**
     * Obtém o valor da propriedade classificationCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClassificationCode() {
        return classificationCode;
    }

    /**
     * Define o valor da propriedade classificationCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClassificationCode(String value) {
        this.classificationCode = value;
    }

    /**
     * Obtém o valor da propriedade classificationStatus.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClassificationStatus() {
        return classificationStatus;
    }

    /**
     * Define o valor da propriedade classificationStatus.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClassificationStatus(String value) {
        this.classificationStatus = value;
    }

    /**
     * Obtém o valor da propriedade classificationNotes.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClassificationNotes() {
        return classificationNotes;
    }

    /**
     * Define o valor da propriedade classificationNotes.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClassificationNotes(String value) {
        this.classificationNotes = value;
    }

    /**
     * Obtém o valor da propriedade approverNotes.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApproverNotes() {
        return approverNotes;
    }

    /**
     * Define o valor da propriedade approverNotes.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApproverNotes(String value) {
        this.approverNotes = value;
    }

    /**
     * Obtém o valor da propriedade tradeDirection.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTradeDirection() {
        return tradeDirection;
    }

    /**
     * Define o valor da propriedade tradeDirection.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTradeDirection(String value) {
        this.tradeDirection = value;
    }

    /**
     * Obtém o valor da propriedade gtmItemClassificationGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getGtmItemClassificationGid() {
        return gtmItemClassificationGid;
    }

    /**
     * Define o valor da propriedade gtmItemClassificationGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setGtmItemClassificationGid(GLogXMLGidType value) {
        this.gtmItemClassificationGid = value;
    }

}
