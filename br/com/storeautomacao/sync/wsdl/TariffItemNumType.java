
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * TariffItemNum is an element of FP_Tariff used to identify the tariff.
 * 
 * <p>Classe Java de TariffItemNumType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="TariffItemNumType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TariffItemNumValue" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ContractSuffix" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TariffItemNumType", propOrder = {
    "tariffItemNumValue",
    "contractSuffix"
})
public class TariffItemNumType {

    @XmlElement(name = "TariffItemNumValue", required = true)
    protected String tariffItemNumValue;
    @XmlElement(name = "ContractSuffix", required = true)
    protected String contractSuffix;

    /**
     * Obtém o valor da propriedade tariffItemNumValue.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTariffItemNumValue() {
        return tariffItemNumValue;
    }

    /**
     * Define o valor da propriedade tariffItemNumValue.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTariffItemNumValue(String value) {
        this.tariffItemNumValue = value;
    }

    /**
     * Obtém o valor da propriedade contractSuffix.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContractSuffix() {
        return contractSuffix;
    }

    /**
     * Define o valor da propriedade contractSuffix.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContractSuffix(String value) {
        this.contractSuffix = value;
    }

}
