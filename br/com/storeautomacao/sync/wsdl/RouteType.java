
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 *             Used to specify carrier and routing sequences and details.
 *             Specifies the service providers responsible for the various legs of a route. The Routes is used primarily for rail carriers.
 *          
 * 
 * <p>Classe Java de RouteType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="RouteType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RouteSequence" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ServiceProviderAlias" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ServiceProviderAliasType"/>
 *         &lt;element name="City" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TransportModeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="IntermodalServiceCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RouteType", propOrder = {
    "routeSequence",
    "serviceProviderAlias",
    "city",
    "transportModeGid",
    "intermodalServiceCode"
})
public class RouteType {

    @XmlElement(name = "RouteSequence", required = true)
    protected String routeSequence;
    @XmlElement(name = "ServiceProviderAlias", required = true)
    protected ServiceProviderAliasType serviceProviderAlias;
    @XmlElement(name = "City")
    protected String city;
    @XmlElement(name = "TransportModeGid")
    protected GLogXMLGidType transportModeGid;
    @XmlElement(name = "IntermodalServiceCode")
    protected String intermodalServiceCode;

    /**
     * Obtém o valor da propriedade routeSequence.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRouteSequence() {
        return routeSequence;
    }

    /**
     * Define o valor da propriedade routeSequence.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRouteSequence(String value) {
        this.routeSequence = value;
    }

    /**
     * Obtém o valor da propriedade serviceProviderAlias.
     * 
     * @return
     *     possible object is
     *     {@link ServiceProviderAliasType }
     *     
     */
    public ServiceProviderAliasType getServiceProviderAlias() {
        return serviceProviderAlias;
    }

    /**
     * Define o valor da propriedade serviceProviderAlias.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceProviderAliasType }
     *     
     */
    public void setServiceProviderAlias(ServiceProviderAliasType value) {
        this.serviceProviderAlias = value;
    }

    /**
     * Obtém o valor da propriedade city.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCity() {
        return city;
    }

    /**
     * Define o valor da propriedade city.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCity(String value) {
        this.city = value;
    }

    /**
     * Obtém o valor da propriedade transportModeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getTransportModeGid() {
        return transportModeGid;
    }

    /**
     * Define o valor da propriedade transportModeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setTransportModeGid(GLogXMLGidType value) {
        this.transportModeGid = value;
    }

    /**
     * Obtém o valor da propriedade intermodalServiceCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIntermodalServiceCode() {
        return intermodalServiceCode;
    }

    /**
     * Define o valor da propriedade intermodalServiceCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIntermodalServiceCode(String value) {
        this.intermodalServiceCode = value;
    }

}
