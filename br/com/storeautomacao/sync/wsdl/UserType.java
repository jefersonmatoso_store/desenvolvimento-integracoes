
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Classe Java de UserType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="UserType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OTMTransactionIn">
 *       &lt;sequence>
 *         &lt;element name="GlUserGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="TransactionCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransactionCodeType"/>
 *         &lt;element name="Nickname" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="UserPassword" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}UserPasswordType" minOccurs="0"/>
 *         &lt;element name="GlAccountPolicyGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="UserRoleGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="UserPreferenceGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="UserMenuLayoutGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="DocumentUseProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="DefaultAAReport" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsExternal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsFromOAM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EffectiveDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="ExpirationDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="UserBIAppGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="UserBIRoleGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserType", propOrder = {
    "glUserGid",
    "transactionCode",
    "nickname",
    "userPassword",
    "glAccountPolicyGid",
    "userRoleGid",
    "userPreferenceGid",
    "userMenuLayoutGid",
    "documentUseProfileGid",
    "defaultAAReport",
    "isExternal",
    "isFromOAM",
    "effectiveDate",
    "expirationDate",
    "userBIAppGid",
    "userBIRoleGid"
})
public class UserType
    extends OTMTransactionIn
{

    @XmlElement(name = "GlUserGid", required = true)
    protected GLogXMLGidType glUserGid;
    @XmlElement(name = "TransactionCode", required = true)
    @XmlSchemaType(name = "string")
    protected TransactionCodeType transactionCode;
    @XmlElement(name = "Nickname")
    protected String nickname;
    @XmlElement(name = "UserPassword")
    protected UserPasswordType userPassword;
    @XmlElement(name = "GlAccountPolicyGid")
    protected GLogXMLGidType glAccountPolicyGid;
    @XmlElement(name = "UserRoleGid")
    protected GLogXMLGidType userRoleGid;
    @XmlElement(name = "UserPreferenceGid")
    protected GLogXMLGidType userPreferenceGid;
    @XmlElement(name = "UserMenuLayoutGid")
    protected List<GLogXMLGidType> userMenuLayoutGid;
    @XmlElement(name = "DocumentUseProfileGid")
    protected GLogXMLGidType documentUseProfileGid;
    @XmlElement(name = "DefaultAAReport")
    protected String defaultAAReport;
    @XmlElement(name = "IsExternal")
    protected String isExternal;
    @XmlElement(name = "IsFromOAM")
    protected String isFromOAM;
    @XmlElement(name = "EffectiveDate")
    protected GLogDateTimeType effectiveDate;
    @XmlElement(name = "ExpirationDate")
    protected GLogDateTimeType expirationDate;
    @XmlElement(name = "UserBIAppGid")
    protected List<GLogXMLGidType> userBIAppGid;
    @XmlElement(name = "UserBIRoleGid")
    protected List<GLogXMLGidType> userBIRoleGid;

    /**
     * Obtém o valor da propriedade glUserGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getGlUserGid() {
        return glUserGid;
    }

    /**
     * Define o valor da propriedade glUserGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setGlUserGid(GLogXMLGidType value) {
        this.glUserGid = value;
    }

    /**
     * Obtém o valor da propriedade transactionCode.
     * 
     * @return
     *     possible object is
     *     {@link TransactionCodeType }
     *     
     */
    public TransactionCodeType getTransactionCode() {
        return transactionCode;
    }

    /**
     * Define o valor da propriedade transactionCode.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionCodeType }
     *     
     */
    public void setTransactionCode(TransactionCodeType value) {
        this.transactionCode = value;
    }

    /**
     * Obtém o valor da propriedade nickname.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNickname() {
        return nickname;
    }

    /**
     * Define o valor da propriedade nickname.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNickname(String value) {
        this.nickname = value;
    }

    /**
     * Obtém o valor da propriedade userPassword.
     * 
     * @return
     *     possible object is
     *     {@link UserPasswordType }
     *     
     */
    public UserPasswordType getUserPassword() {
        return userPassword;
    }

    /**
     * Define o valor da propriedade userPassword.
     * 
     * @param value
     *     allowed object is
     *     {@link UserPasswordType }
     *     
     */
    public void setUserPassword(UserPasswordType value) {
        this.userPassword = value;
    }

    /**
     * Obtém o valor da propriedade glAccountPolicyGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getGlAccountPolicyGid() {
        return glAccountPolicyGid;
    }

    /**
     * Define o valor da propriedade glAccountPolicyGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setGlAccountPolicyGid(GLogXMLGidType value) {
        this.glAccountPolicyGid = value;
    }

    /**
     * Obtém o valor da propriedade userRoleGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getUserRoleGid() {
        return userRoleGid;
    }

    /**
     * Define o valor da propriedade userRoleGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setUserRoleGid(GLogXMLGidType value) {
        this.userRoleGid = value;
    }

    /**
     * Obtém o valor da propriedade userPreferenceGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getUserPreferenceGid() {
        return userPreferenceGid;
    }

    /**
     * Define o valor da propriedade userPreferenceGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setUserPreferenceGid(GLogXMLGidType value) {
        this.userPreferenceGid = value;
    }

    /**
     * Gets the value of the userMenuLayoutGid property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the userMenuLayoutGid property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getUserMenuLayoutGid().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GLogXMLGidType }
     * 
     * 
     */
    public List<GLogXMLGidType> getUserMenuLayoutGid() {
        if (userMenuLayoutGid == null) {
            userMenuLayoutGid = new ArrayList<GLogXMLGidType>();
        }
        return this.userMenuLayoutGid;
    }

    /**
     * Obtém o valor da propriedade documentUseProfileGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getDocumentUseProfileGid() {
        return documentUseProfileGid;
    }

    /**
     * Define o valor da propriedade documentUseProfileGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setDocumentUseProfileGid(GLogXMLGidType value) {
        this.documentUseProfileGid = value;
    }

    /**
     * Obtém o valor da propriedade defaultAAReport.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDefaultAAReport() {
        return defaultAAReport;
    }

    /**
     * Define o valor da propriedade defaultAAReport.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDefaultAAReport(String value) {
        this.defaultAAReport = value;
    }

    /**
     * Obtém o valor da propriedade isExternal.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsExternal() {
        return isExternal;
    }

    /**
     * Define o valor da propriedade isExternal.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsExternal(String value) {
        this.isExternal = value;
    }

    /**
     * Obtém o valor da propriedade isFromOAM.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsFromOAM() {
        return isFromOAM;
    }

    /**
     * Define o valor da propriedade isFromOAM.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsFromOAM(String value) {
        this.isFromOAM = value;
    }

    /**
     * Obtém o valor da propriedade effectiveDate.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getEffectiveDate() {
        return effectiveDate;
    }

    /**
     * Define o valor da propriedade effectiveDate.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setEffectiveDate(GLogDateTimeType value) {
        this.effectiveDate = value;
    }

    /**
     * Obtém o valor da propriedade expirationDate.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getExpirationDate() {
        return expirationDate;
    }

    /**
     * Define o valor da propriedade expirationDate.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setExpirationDate(GLogDateTimeType value) {
        this.expirationDate = value;
    }

    /**
     * Gets the value of the userBIAppGid property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the userBIAppGid property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getUserBIAppGid().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GLogXMLGidType }
     * 
     * 
     */
    public List<GLogXMLGidType> getUserBIAppGid() {
        if (userBIAppGid == null) {
            userBIAppGid = new ArrayList<GLogXMLGidType>();
        }
        return this.userBIAppGid;
    }

    /**
     * Gets the value of the userBIRoleGid property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the userBIRoleGid property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getUserBIRoleGid().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GLogXMLGidType }
     * 
     * 
     */
    public List<GLogXMLGidType> getUserBIRoleGid() {
        if (userBIRoleGid == null) {
            userBIRoleGid = new ArrayList<GLogXMLGidType>();
        }
        return this.userBIRoleGid;
    }

}
