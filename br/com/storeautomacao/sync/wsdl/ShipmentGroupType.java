
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * Specifies the shipment group header information and the associated Shipments in the group.
 *             The ShipmentGroup element is supported for both inbound and outbound messages.
 *             The ShipmentStatus element within the ShipmentGroup is only supported on the outbound.
 *             When creating a ShipmentGroup, there should be at least one FullShipment/Shipment or Partial Shipment in the group.
 *          
 * 
 * <p>Classe Java de ShipmentGroupType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="ShipmentGroupType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OTMTransactionInOut">
 *       &lt;sequence>
 *         &lt;element name="ShipmentGroupHeader" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ShipmentGroupHeaderType"/>
 *         &lt;element name="Shipment" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ShipmentType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="FullShipment" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FullShipmentType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="PartialShipment" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}PartialShipmentType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="SecondaryCharges" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}SecondaryChargesType" minOccurs="0"/>
 *         &lt;element name="ShipmentStatus" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ShipmentStatusType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Appointment" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}AppointmentType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ShipmentGroupType", propOrder = {
    "shipmentGroupHeader",
    "shipment",
    "fullShipment",
    "partialShipment",
    "secondaryCharges",
    "shipmentStatus",
    "appointment"
})
public class ShipmentGroupType
    extends OTMTransactionInOut
{

    @XmlElement(name = "ShipmentGroupHeader", required = true)
    protected ShipmentGroupHeaderType shipmentGroupHeader;
    @XmlElement(name = "Shipment")
    protected List<ShipmentType> shipment;
    @XmlElement(name = "FullShipment")
    protected List<FullShipmentType> fullShipment;
    @XmlElement(name = "PartialShipment")
    protected List<PartialShipmentType> partialShipment;
    @XmlElement(name = "SecondaryCharges")
    protected SecondaryChargesType secondaryCharges;
    @XmlElement(name = "ShipmentStatus")
    protected List<ShipmentStatusType> shipmentStatus;
    @XmlElement(name = "Appointment")
    protected List<AppointmentType> appointment;

    /**
     * Obtém o valor da propriedade shipmentGroupHeader.
     * 
     * @return
     *     possible object is
     *     {@link ShipmentGroupHeaderType }
     *     
     */
    public ShipmentGroupHeaderType getShipmentGroupHeader() {
        return shipmentGroupHeader;
    }

    /**
     * Define o valor da propriedade shipmentGroupHeader.
     * 
     * @param value
     *     allowed object is
     *     {@link ShipmentGroupHeaderType }
     *     
     */
    public void setShipmentGroupHeader(ShipmentGroupHeaderType value) {
        this.shipmentGroupHeader = value;
    }

    /**
     * Gets the value of the shipment property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the shipment property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getShipment().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ShipmentType }
     * 
     * 
     */
    public List<ShipmentType> getShipment() {
        if (shipment == null) {
            shipment = new ArrayList<ShipmentType>();
        }
        return this.shipment;
    }

    /**
     * Gets the value of the fullShipment property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the fullShipment property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFullShipment().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FullShipmentType }
     * 
     * 
     */
    public List<FullShipmentType> getFullShipment() {
        if (fullShipment == null) {
            fullShipment = new ArrayList<FullShipmentType>();
        }
        return this.fullShipment;
    }

    /**
     * Gets the value of the partialShipment property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the partialShipment property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPartialShipment().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PartialShipmentType }
     * 
     * 
     */
    public List<PartialShipmentType> getPartialShipment() {
        if (partialShipment == null) {
            partialShipment = new ArrayList<PartialShipmentType>();
        }
        return this.partialShipment;
    }

    /**
     * Obtém o valor da propriedade secondaryCharges.
     * 
     * @return
     *     possible object is
     *     {@link SecondaryChargesType }
     *     
     */
    public SecondaryChargesType getSecondaryCharges() {
        return secondaryCharges;
    }

    /**
     * Define o valor da propriedade secondaryCharges.
     * 
     * @param value
     *     allowed object is
     *     {@link SecondaryChargesType }
     *     
     */
    public void setSecondaryCharges(SecondaryChargesType value) {
        this.secondaryCharges = value;
    }

    /**
     * Gets the value of the shipmentStatus property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the shipmentStatus property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getShipmentStatus().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ShipmentStatusType }
     * 
     * 
     */
    public List<ShipmentStatusType> getShipmentStatus() {
        if (shipmentStatus == null) {
            shipmentStatus = new ArrayList<ShipmentStatusType>();
        }
        return this.shipmentStatus;
    }

    /**
     * Gets the value of the appointment property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the appointment property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAppointment().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AppointmentType }
     * 
     * 
     */
    public List<AppointmentType> getAppointment() {
        if (appointment == null) {
            appointment = new ArrayList<AppointmentType>();
        }
        return this.appointment;
    }

}
