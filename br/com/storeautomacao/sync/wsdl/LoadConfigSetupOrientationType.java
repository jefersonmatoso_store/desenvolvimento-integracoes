
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Specifies the load configuration set up orientation
 * 
 * <p>Classe Java de LoadConfigSetupOrientationType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="LoadConfigSetupOrientationType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="LoadConfigSetupGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="OrientationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="IsPreferred" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsFloorCompatible" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MaxTopWeight" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLWeightType" minOccurs="0"/>
 *         &lt;element name="MaxLengthOverhang" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLengthType" minOccurs="0"/>
 *         &lt;element name="MaxWidthOverhang" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLWidthType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LoadConfigSetupOrientationType", propOrder = {
    "loadConfigSetupGid",
    "orientationGid",
    "isPreferred",
    "isFloorCompatible",
    "maxTopWeight",
    "maxLengthOverhang",
    "maxWidthOverhang"
})
public class LoadConfigSetupOrientationType {

    @XmlElement(name = "LoadConfigSetupGid", required = true)
    protected GLogXMLGidType loadConfigSetupGid;
    @XmlElement(name = "OrientationGid", required = true)
    protected GLogXMLGidType orientationGid;
    @XmlElement(name = "IsPreferred")
    protected String isPreferred;
    @XmlElement(name = "IsFloorCompatible")
    protected String isFloorCompatible;
    @XmlElement(name = "MaxTopWeight")
    protected GLogXMLWeightType maxTopWeight;
    @XmlElement(name = "MaxLengthOverhang")
    protected GLogXMLLengthType maxLengthOverhang;
    @XmlElement(name = "MaxWidthOverhang")
    protected GLogXMLWidthType maxWidthOverhang;

    /**
     * Obtém o valor da propriedade loadConfigSetupGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getLoadConfigSetupGid() {
        return loadConfigSetupGid;
    }

    /**
     * Define o valor da propriedade loadConfigSetupGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setLoadConfigSetupGid(GLogXMLGidType value) {
        this.loadConfigSetupGid = value;
    }

    /**
     * Obtém o valor da propriedade orientationGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getOrientationGid() {
        return orientationGid;
    }

    /**
     * Define o valor da propriedade orientationGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setOrientationGid(GLogXMLGidType value) {
        this.orientationGid = value;
    }

    /**
     * Obtém o valor da propriedade isPreferred.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsPreferred() {
        return isPreferred;
    }

    /**
     * Define o valor da propriedade isPreferred.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsPreferred(String value) {
        this.isPreferred = value;
    }

    /**
     * Obtém o valor da propriedade isFloorCompatible.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsFloorCompatible() {
        return isFloorCompatible;
    }

    /**
     * Define o valor da propriedade isFloorCompatible.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsFloorCompatible(String value) {
        this.isFloorCompatible = value;
    }

    /**
     * Obtém o valor da propriedade maxTopWeight.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLWeightType }
     *     
     */
    public GLogXMLWeightType getMaxTopWeight() {
        return maxTopWeight;
    }

    /**
     * Define o valor da propriedade maxTopWeight.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLWeightType }
     *     
     */
    public void setMaxTopWeight(GLogXMLWeightType value) {
        this.maxTopWeight = value;
    }

    /**
     * Obtém o valor da propriedade maxLengthOverhang.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLengthType }
     *     
     */
    public GLogXMLLengthType getMaxLengthOverhang() {
        return maxLengthOverhang;
    }

    /**
     * Define o valor da propriedade maxLengthOverhang.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLengthType }
     *     
     */
    public void setMaxLengthOverhang(GLogXMLLengthType value) {
        this.maxLengthOverhang = value;
    }

    /**
     * Obtém o valor da propriedade maxWidthOverhang.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLWidthType }
     *     
     */
    public GLogXMLWidthType getMaxWidthOverhang() {
        return maxWidthOverhang;
    }

    /**
     * Define o valor da propriedade maxWidthOverhang.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLWidthType }
     *     
     */
    public void setMaxWidthOverhang(GLogXMLWidthType value) {
        this.maxWidthOverhang = value;
    }

}
