
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de TransactionCodeType.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;simpleType name="TransactionCodeType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="I"/>
 *     &lt;enumeration value="U"/>
 *     &lt;enumeration value="IU"/>
 *     &lt;enumeration value="UI"/>
 *     &lt;enumeration value="D"/>
 *     &lt;enumeration value="RC"/>
 *     &lt;enumeration value="RP"/>
 *     &lt;enumeration value="R"/>
 *     &lt;enumeration value="II"/>
 *     &lt;enumeration value="DR"/>
 *     &lt;enumeration value="NP"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "TransactionCodeType")
@XmlEnum
public enum TransactionCodeType {

    I,
    U,
    IU,
    UI,
    D,
    RC,
    RP,
    R,
    II,
    DR,
    NP;

    public String value() {
        return name();
    }

    public static TransactionCodeType fromValue(String v) {
        return valueOf(v);
    }

}
