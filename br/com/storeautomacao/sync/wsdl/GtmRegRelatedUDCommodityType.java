
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * (Both) User Defined Commodities Related to Registration
 *          
 * 
 * <p>Classe Java de GtmRegRelatedUDCommodityType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="GtmRegRelatedUDCommodityType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GtmRegistrationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="UserDefinedCommodityGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GtmRegRelatedUDCommodityType", namespace = "http://xmlns.oracle.com/apps/gtm/transmission/v6.4", propOrder = {
    "gtmRegistrationGid",
    "userDefinedCommodityGid"
})
public class GtmRegRelatedUDCommodityType {

    @XmlElement(name = "GtmRegistrationGid", required = true)
    protected GLogXMLGidType gtmRegistrationGid;
    @XmlElement(name = "UserDefinedCommodityGid", required = true)
    protected GLogXMLGidType userDefinedCommodityGid;

    /**
     * Obtém o valor da propriedade gtmRegistrationGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getGtmRegistrationGid() {
        return gtmRegistrationGid;
    }

    /**
     * Define o valor da propriedade gtmRegistrationGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setGtmRegistrationGid(GLogXMLGidType value) {
        this.gtmRegistrationGid = value;
    }

    /**
     * Obtém o valor da propriedade userDefinedCommodityGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getUserDefinedCommodityGid() {
        return userDefinedCommodityGid;
    }

    /**
     * Define o valor da propriedade userDefinedCommodityGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setUserDefinedCommodityGid(GLogXMLGidType value) {
        this.userDefinedCommodityGid = value;
    }

}
