
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * SSStop is a structure containing stop information
 * 
 * <p>Classe Java de SSStopType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="SSStopType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SSStopSequenceNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SSLocation" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}SSLocationType" minOccurs="0"/>
 *         &lt;element name="RefPosition" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="StopType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SSStopType", propOrder = {
    "ssStopSequenceNum",
    "ssLocation",
    "refPosition",
    "stopType"
})
public class SSStopType {

    @XmlElement(name = "SSStopSequenceNum")
    protected String ssStopSequenceNum;
    @XmlElement(name = "SSLocation")
    protected SSLocationType ssLocation;
    @XmlElement(name = "RefPosition")
    protected String refPosition;
    @XmlElement(name = "StopType")
    protected String stopType;

    /**
     * Obtém o valor da propriedade ssStopSequenceNum.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSSStopSequenceNum() {
        return ssStopSequenceNum;
    }

    /**
     * Define o valor da propriedade ssStopSequenceNum.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSSStopSequenceNum(String value) {
        this.ssStopSequenceNum = value;
    }

    /**
     * Obtém o valor da propriedade ssLocation.
     * 
     * @return
     *     possible object is
     *     {@link SSLocationType }
     *     
     */
    public SSLocationType getSSLocation() {
        return ssLocation;
    }

    /**
     * Define o valor da propriedade ssLocation.
     * 
     * @param value
     *     allowed object is
     *     {@link SSLocationType }
     *     
     */
    public void setSSLocation(SSLocationType value) {
        this.ssLocation = value;
    }

    /**
     * Obtém o valor da propriedade refPosition.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRefPosition() {
        return refPosition;
    }

    /**
     * Define o valor da propriedade refPosition.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRefPosition(String value) {
        this.refPosition = value;
    }

    /**
     * Obtém o valor da propriedade stopType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStopType() {
        return stopType;
    }

    /**
     * Define o valor da propriedade stopType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStopType(String value) {
        this.stopType = value;
    }

}
