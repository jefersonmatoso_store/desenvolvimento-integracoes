
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de EventGroupType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="EventGroupType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="EventGroupGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="EventGroupDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EventGroupType", propOrder = {
    "eventGroupGid",
    "eventGroupDescription"
})
public class EventGroupType {

    @XmlElement(name = "EventGroupGid", required = true)
    protected GLogXMLGidType eventGroupGid;
    @XmlElement(name = "EventGroupDescription")
    protected String eventGroupDescription;

    /**
     * Obtém o valor da propriedade eventGroupGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getEventGroupGid() {
        return eventGroupGid;
    }

    /**
     * Define o valor da propriedade eventGroupGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setEventGroupGid(GLogXMLGidType value) {
        this.eventGroupGid = value;
    }

    /**
     * Obtém o valor da propriedade eventGroupDescription.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEventGroupDescription() {
        return eventGroupDescription;
    }

    /**
     * Define o valor da propriedade eventGroupDescription.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEventGroupDescription(String value) {
        this.eventGroupDescription = value;
    }

}
