
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * SkuTransaction is the xml representation of the sku_transaction, sku_transaction_descriptor,
 *             and sku_transaction_d_remark
 *             tables.
 *          
 * 
 * <p>Classe Java de SkuTransactionType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="SkuTransactionType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OTMTransactionInOut">
 *       &lt;sequence>
 *         &lt;element name="SkuTransactionGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="SkuGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="TransactionType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PackagedItemGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="WarehouseLocationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="SupplierCorporationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="OwnerCorporationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="Quantity1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Quantity2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TransactionDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SkuTransactionDescriptor" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}SkuTransactionDescriptorType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SkuTransactionType", propOrder = {
    "skuTransactionGid",
    "skuGid",
    "transactionType",
    "packagedItemGid",
    "warehouseLocationGid",
    "supplierCorporationGid",
    "ownerCorporationGid",
    "quantity1",
    "quantity2",
    "transactionDt",
    "description",
    "skuTransactionDescriptor"
})
public class SkuTransactionType
    extends OTMTransactionInOut
{

    @XmlElement(name = "SkuTransactionGid", required = true)
    protected GLogXMLGidType skuTransactionGid;
    @XmlElement(name = "SkuGid", required = true)
    protected GLogXMLGidType skuGid;
    @XmlElement(name = "TransactionType", required = true)
    protected String transactionType;
    @XmlElement(name = "PackagedItemGid")
    protected GLogXMLGidType packagedItemGid;
    @XmlElement(name = "WarehouseLocationGid")
    protected GLogXMLGidType warehouseLocationGid;
    @XmlElement(name = "SupplierCorporationGid")
    protected GLogXMLGidType supplierCorporationGid;
    @XmlElement(name = "OwnerCorporationGid")
    protected GLogXMLGidType ownerCorporationGid;
    @XmlElement(name = "Quantity1")
    protected String quantity1;
    @XmlElement(name = "Quantity2")
    protected String quantity2;
    @XmlElement(name = "TransactionDt")
    protected GLogDateTimeType transactionDt;
    @XmlElement(name = "Description")
    protected String description;
    @XmlElement(name = "SkuTransactionDescriptor")
    protected List<SkuTransactionDescriptorType> skuTransactionDescriptor;

    /**
     * Obtém o valor da propriedade skuTransactionGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getSkuTransactionGid() {
        return skuTransactionGid;
    }

    /**
     * Define o valor da propriedade skuTransactionGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setSkuTransactionGid(GLogXMLGidType value) {
        this.skuTransactionGid = value;
    }

    /**
     * Obtém o valor da propriedade skuGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getSkuGid() {
        return skuGid;
    }

    /**
     * Define o valor da propriedade skuGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setSkuGid(GLogXMLGidType value) {
        this.skuGid = value;
    }

    /**
     * Obtém o valor da propriedade transactionType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionType() {
        return transactionType;
    }

    /**
     * Define o valor da propriedade transactionType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionType(String value) {
        this.transactionType = value;
    }

    /**
     * Obtém o valor da propriedade packagedItemGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getPackagedItemGid() {
        return packagedItemGid;
    }

    /**
     * Define o valor da propriedade packagedItemGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setPackagedItemGid(GLogXMLGidType value) {
        this.packagedItemGid = value;
    }

    /**
     * Obtém o valor da propriedade warehouseLocationGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getWarehouseLocationGid() {
        return warehouseLocationGid;
    }

    /**
     * Define o valor da propriedade warehouseLocationGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setWarehouseLocationGid(GLogXMLGidType value) {
        this.warehouseLocationGid = value;
    }

    /**
     * Obtém o valor da propriedade supplierCorporationGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getSupplierCorporationGid() {
        return supplierCorporationGid;
    }

    /**
     * Define o valor da propriedade supplierCorporationGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setSupplierCorporationGid(GLogXMLGidType value) {
        this.supplierCorporationGid = value;
    }

    /**
     * Obtém o valor da propriedade ownerCorporationGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getOwnerCorporationGid() {
        return ownerCorporationGid;
    }

    /**
     * Define o valor da propriedade ownerCorporationGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setOwnerCorporationGid(GLogXMLGidType value) {
        this.ownerCorporationGid = value;
    }

    /**
     * Obtém o valor da propriedade quantity1.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQuantity1() {
        return quantity1;
    }

    /**
     * Define o valor da propriedade quantity1.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQuantity1(String value) {
        this.quantity1 = value;
    }

    /**
     * Obtém o valor da propriedade quantity2.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQuantity2() {
        return quantity2;
    }

    /**
     * Define o valor da propriedade quantity2.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQuantity2(String value) {
        this.quantity2 = value;
    }

    /**
     * Obtém o valor da propriedade transactionDt.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getTransactionDt() {
        return transactionDt;
    }

    /**
     * Define o valor da propriedade transactionDt.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setTransactionDt(GLogDateTimeType value) {
        this.transactionDt = value;
    }

    /**
     * Obtém o valor da propriedade description.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Define o valor da propriedade description.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the skuTransactionDescriptor property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the skuTransactionDescriptor property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSkuTransactionDescriptor().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SkuTransactionDescriptorType }
     * 
     * 
     */
    public List<SkuTransactionDescriptorType> getSkuTransactionDescriptor() {
        if (skuTransactionDescriptor == null) {
            skuTransactionDescriptor = new ArrayList<SkuTransactionDescriptorType>();
        }
        return this.skuTransactionDescriptor;
    }

}
