
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Contains data or information relevant to order releases that are specific to a Shipment.
 * 
 * <p>Classe Java de ShipmentOrderReleaseType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="ShipmentOrderReleaseType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ReleaseGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="NumReferenceUnits" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="IsFixedNumReferenceUnits" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="EquipmentRefUnitGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ShipmentOrderReleaseType", propOrder = {
    "releaseGid",
    "numReferenceUnits",
    "isFixedNumReferenceUnits",
    "equipmentRefUnitGid",
    "description"
})
public class ShipmentOrderReleaseType {

    @XmlElement(name = "ReleaseGid", required = true)
    protected GLogXMLGidType releaseGid;
    @XmlElement(name = "NumReferenceUnits", required = true)
    protected String numReferenceUnits;
    @XmlElement(name = "IsFixedNumReferenceUnits", required = true)
    protected String isFixedNumReferenceUnits;
    @XmlElement(name = "EquipmentRefUnitGid")
    protected GLogXMLGidType equipmentRefUnitGid;
    @XmlElement(name = "Description")
    protected String description;

    /**
     * Obtém o valor da propriedade releaseGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getReleaseGid() {
        return releaseGid;
    }

    /**
     * Define o valor da propriedade releaseGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setReleaseGid(GLogXMLGidType value) {
        this.releaseGid = value;
    }

    /**
     * Obtém o valor da propriedade numReferenceUnits.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumReferenceUnits() {
        return numReferenceUnits;
    }

    /**
     * Define o valor da propriedade numReferenceUnits.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumReferenceUnits(String value) {
        this.numReferenceUnits = value;
    }

    /**
     * Obtém o valor da propriedade isFixedNumReferenceUnits.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsFixedNumReferenceUnits() {
        return isFixedNumReferenceUnits;
    }

    /**
     * Define o valor da propriedade isFixedNumReferenceUnits.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsFixedNumReferenceUnits(String value) {
        this.isFixedNumReferenceUnits = value;
    }

    /**
     * Obtém o valor da propriedade equipmentRefUnitGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getEquipmentRefUnitGid() {
        return equipmentRefUnitGid;
    }

    /**
     * Define o valor da propriedade equipmentRefUnitGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setEquipmentRefUnitGid(GLogXMLGidType value) {
        this.equipmentRefUnitGid = value;
    }

    /**
     * Obtém o valor da propriedade description.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Define o valor da propriedade description.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

}
