
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Specifies reference information from the RIQQuery. This includes the source and destination
 *             location details when the location gid is specified in the source and dest
 *             address.
 *          
 * 
 * <p>Classe Java de RIQQueryInfoType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="RIQQueryInfoType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SourceLocRef" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Location" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LocationType"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="DestLocRef" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Location" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LocationType"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RIQQueryInfoType", propOrder = {
    "sourceLocRef",
    "destLocRef"
})
public class RIQQueryInfoType {

    @XmlElement(name = "SourceLocRef")
    protected RIQQueryInfoType.SourceLocRef sourceLocRef;
    @XmlElement(name = "DestLocRef")
    protected RIQQueryInfoType.DestLocRef destLocRef;

    /**
     * Obtém o valor da propriedade sourceLocRef.
     * 
     * @return
     *     possible object is
     *     {@link RIQQueryInfoType.SourceLocRef }
     *     
     */
    public RIQQueryInfoType.SourceLocRef getSourceLocRef() {
        return sourceLocRef;
    }

    /**
     * Define o valor da propriedade sourceLocRef.
     * 
     * @param value
     *     allowed object is
     *     {@link RIQQueryInfoType.SourceLocRef }
     *     
     */
    public void setSourceLocRef(RIQQueryInfoType.SourceLocRef value) {
        this.sourceLocRef = value;
    }

    /**
     * Obtém o valor da propriedade destLocRef.
     * 
     * @return
     *     possible object is
     *     {@link RIQQueryInfoType.DestLocRef }
     *     
     */
    public RIQQueryInfoType.DestLocRef getDestLocRef() {
        return destLocRef;
    }

    /**
     * Define o valor da propriedade destLocRef.
     * 
     * @param value
     *     allowed object is
     *     {@link RIQQueryInfoType.DestLocRef }
     *     
     */
    public void setDestLocRef(RIQQueryInfoType.DestLocRef value) {
        this.destLocRef = value;
    }


    /**
     * <p>Classe Java de anonymous complex type.
     * 
     * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Location" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LocationType"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "location"
    })
    public static class DestLocRef {

        @XmlElement(name = "Location", required = true)
        protected LocationType location;

        /**
         * Obtém o valor da propriedade location.
         * 
         * @return
         *     possible object is
         *     {@link LocationType }
         *     
         */
        public LocationType getLocation() {
            return location;
        }

        /**
         * Define o valor da propriedade location.
         * 
         * @param value
         *     allowed object is
         *     {@link LocationType }
         *     
         */
        public void setLocation(LocationType value) {
            this.location = value;
        }

    }


    /**
     * <p>Classe Java de anonymous complex type.
     * 
     * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Location" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LocationType"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "location"
    })
    public static class SourceLocRef {

        @XmlElement(name = "Location", required = true)
        protected LocationType location;

        /**
         * Obtém o valor da propriedade location.
         * 
         * @return
         *     possible object is
         *     {@link LocationType }
         *     
         */
        public LocationType getLocation() {
            return location;
        }

        /**
         * Define o valor da propriedade location.
         * 
         * @param value
         *     allowed object is
         *     {@link LocationType }
         *     
         */
        public void setLocation(LocationType value) {
            this.location = value;
        }

    }

}
