
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * (Both) Percentage information for an object. It consists of a qualifier and a value.
 *          
 * 
 * <p>Classe Java de PercentageType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="PercentageType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PercentageQualifierGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="PercentageValue" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PercentageType", namespace = "http://xmlns.oracle.com/apps/gtm/transmission/v6.4", propOrder = {
    "percentageQualifierGid",
    "percentageValue"
})
public class PercentageType {

    @XmlElement(name = "PercentageQualifierGid", required = true)
    protected GLogXMLGidType percentageQualifierGid;
    @XmlElement(name = "PercentageValue", required = true)
    protected String percentageValue;

    /**
     * Obtém o valor da propriedade percentageQualifierGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getPercentageQualifierGid() {
        return percentageQualifierGid;
    }

    /**
     * Define o valor da propriedade percentageQualifierGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setPercentageQualifierGid(GLogXMLGidType value) {
        this.percentageQualifierGid = value;
    }

    /**
     * Obtém o valor da propriedade percentageValue.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPercentageValue() {
        return percentageValue;
    }

    /**
     * Define o valor da propriedade percentageValue.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPercentageValue(String value) {
        this.percentageValue = value;
    }

}
