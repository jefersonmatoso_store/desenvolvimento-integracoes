
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 *             (Both) PaymentModeDetail is one of MotorDetail, OceanDetail, RailDetail, or GenericDetail.
 *             Its used to convey detail information for a given mode of transportation or generically. In the case of outbound i.e. in case of bill only GenericDetail is used.
 *          
 * 
 * <p>Classe Java de PaymentModeDetailType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="PaymentModeDetailType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element name="MotorDetail" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}MotorDetailType"/>
 *         &lt;element name="OceanDetail" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OceanDetailType"/>
 *         &lt;element name="RailDetail" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RailDetailType"/>
 *         &lt;element name="GenericDetail" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GenericDetailType"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PaymentModeDetailType", propOrder = {
    "motorDetail",
    "oceanDetail",
    "railDetail",
    "genericDetail"
})
public class PaymentModeDetailType {

    @XmlElement(name = "MotorDetail")
    protected MotorDetailType motorDetail;
    @XmlElement(name = "OceanDetail")
    protected OceanDetailType oceanDetail;
    @XmlElement(name = "RailDetail")
    protected RailDetailType railDetail;
    @XmlElement(name = "GenericDetail")
    protected GenericDetailType genericDetail;

    /**
     * Obtém o valor da propriedade motorDetail.
     * 
     * @return
     *     possible object is
     *     {@link MotorDetailType }
     *     
     */
    public MotorDetailType getMotorDetail() {
        return motorDetail;
    }

    /**
     * Define o valor da propriedade motorDetail.
     * 
     * @param value
     *     allowed object is
     *     {@link MotorDetailType }
     *     
     */
    public void setMotorDetail(MotorDetailType value) {
        this.motorDetail = value;
    }

    /**
     * Obtém o valor da propriedade oceanDetail.
     * 
     * @return
     *     possible object is
     *     {@link OceanDetailType }
     *     
     */
    public OceanDetailType getOceanDetail() {
        return oceanDetail;
    }

    /**
     * Define o valor da propriedade oceanDetail.
     * 
     * @param value
     *     allowed object is
     *     {@link OceanDetailType }
     *     
     */
    public void setOceanDetail(OceanDetailType value) {
        this.oceanDetail = value;
    }

    /**
     * Obtém o valor da propriedade railDetail.
     * 
     * @return
     *     possible object is
     *     {@link RailDetailType }
     *     
     */
    public RailDetailType getRailDetail() {
        return railDetail;
    }

    /**
     * Define o valor da propriedade railDetail.
     * 
     * @param value
     *     allowed object is
     *     {@link RailDetailType }
     *     
     */
    public void setRailDetail(RailDetailType value) {
        this.railDetail = value;
    }

    /**
     * Obtém o valor da propriedade genericDetail.
     * 
     * @return
     *     possible object is
     *     {@link GenericDetailType }
     *     
     */
    public GenericDetailType getGenericDetail() {
        return genericDetail;
    }

    /**
     * Define o valor da propriedade genericDetail.
     * 
     * @param value
     *     allowed object is
     *     {@link GenericDetailType }
     *     
     */
    public void setGenericDetail(GenericDetailType value) {
        this.genericDetail = value;
    }

}
