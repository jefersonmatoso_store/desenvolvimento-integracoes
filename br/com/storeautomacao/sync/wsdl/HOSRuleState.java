
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 *             Status of HOS Rule.
 *          
 * 
 * <p>Classe Java de HOSRuleState complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="HOSRuleState">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="HOSRuleGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="ActivityTime" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDurationType" minOccurs="0"/>
 *         &lt;element name="ActivityTimeRemain" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDurationType" minOccurs="0"/>
 *         &lt;element name="RuleBeginTime" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "HOSRuleState", propOrder = {
    "hosRuleGid",
    "activityTime",
    "activityTimeRemain",
    "ruleBeginTime"
})
public class HOSRuleState {

    @XmlElement(name = "HOSRuleGid")
    protected GLogXMLGidType hosRuleGid;
    @XmlElement(name = "ActivityTime")
    protected GLogXMLDurationType activityTime;
    @XmlElement(name = "ActivityTimeRemain")
    protected GLogXMLDurationType activityTimeRemain;
    @XmlElement(name = "RuleBeginTime")
    protected GLogDateTimeType ruleBeginTime;

    /**
     * Obtém o valor da propriedade hosRuleGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getHOSRuleGid() {
        return hosRuleGid;
    }

    /**
     * Define o valor da propriedade hosRuleGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setHOSRuleGid(GLogXMLGidType value) {
        this.hosRuleGid = value;
    }

    /**
     * Obtém o valor da propriedade activityTime.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public GLogXMLDurationType getActivityTime() {
        return activityTime;
    }

    /**
     * Define o valor da propriedade activityTime.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public void setActivityTime(GLogXMLDurationType value) {
        this.activityTime = value;
    }

    /**
     * Obtém o valor da propriedade activityTimeRemain.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public GLogXMLDurationType getActivityTimeRemain() {
        return activityTimeRemain;
    }

    /**
     * Define o valor da propriedade activityTimeRemain.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public void setActivityTimeRemain(GLogXMLDurationType value) {
        this.activityTimeRemain = value;
    }

    /**
     * Obtém o valor da propriedade ruleBeginTime.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getRuleBeginTime() {
        return ruleBeginTime;
    }

    /**
     * Define o valor da propriedade ruleBeginTime.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setRuleBeginTime(GLogDateTimeType value) {
        this.ruleBeginTime = value;
    }

}
