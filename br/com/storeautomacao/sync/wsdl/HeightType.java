
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Height contains sub-elements for height unit of measure and value.
 * 
 * <p>Classe Java de HeightType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="HeightType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="HeightValue" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="HeightUOMGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "HeightType", propOrder = {
    "heightValue",
    "heightUOMGid"
})
public class HeightType {

    @XmlElement(name = "HeightValue", required = true)
    protected String heightValue;
    @XmlElement(name = "HeightUOMGid", required = true)
    protected GLogXMLGidType heightUOMGid;

    /**
     * Obtém o valor da propriedade heightValue.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHeightValue() {
        return heightValue;
    }

    /**
     * Define o valor da propriedade heightValue.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHeightValue(String value) {
        this.heightValue = value;
    }

    /**
     * Obtém o valor da propriedade heightUOMGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getHeightUOMGid() {
        return heightUOMGid;
    }

    /**
     * Define o valor da propriedade heightUOMGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setHeightUOMGid(GLogXMLGidType value) {
        this.heightUOMGid = value;
    }

}
