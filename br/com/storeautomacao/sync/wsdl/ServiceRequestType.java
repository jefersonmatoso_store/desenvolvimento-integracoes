
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * 
 *             Request/Reply style interface request structure.
 *          
 * 
 * <p>Classe Java de ServiceRequestType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="ServiceRequestType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OTMTransactionIn">
 *       &lt;sequence>
 *         &lt;element name="ServiceName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="RefId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Source" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Attribute" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}AttributeType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="GtmServicePreferenceId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;choice>
 *           &lt;element name="RestrictedParty" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}RestrictedPartyType"/>
 *           &lt;element name="SanctionedTerritory" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}SanctionedTerritoryType"/>
 *           &lt;element name="Classification" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}ClassificationType"/>
 *           &lt;element name="ComplianceRule" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}ComplianceRuleType"/>
 *           &lt;element name="LicenseDetermination" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}LicenseDeterminationType"/>
 *         &lt;/choice>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceRequestType", namespace = "http://xmlns.oracle.com/apps/gtm/transmission/v6.4", propOrder = {
    "serviceName",
    "refId",
    "source",
    "attribute",
    "gtmServicePreferenceId",
    "restrictedParty",
    "sanctionedTerritory",
    "classification",
    "complianceRule",
    "licenseDetermination"
})
public class ServiceRequestType
    extends OTMTransactionIn
{

    @XmlElement(name = "ServiceName", required = true)
    protected String serviceName;
    @XmlElement(name = "RefId")
    protected String refId;
    @XmlElement(name = "Source")
    protected String source;
    @XmlElement(name = "Attribute")
    protected List<AttributeType> attribute;
    @XmlElement(name = "GtmServicePreferenceId")
    protected String gtmServicePreferenceId;
    @XmlElement(name = "RestrictedParty")
    protected RestrictedPartyType restrictedParty;
    @XmlElement(name = "SanctionedTerritory")
    protected SanctionedTerritoryType sanctionedTerritory;
    @XmlElement(name = "Classification")
    protected ClassificationType classification;
    @XmlElement(name = "ComplianceRule")
    protected ComplianceRuleType complianceRule;
    @XmlElement(name = "LicenseDetermination")
    protected LicenseDeterminationType licenseDetermination;

    /**
     * Obtém o valor da propriedade serviceName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceName() {
        return serviceName;
    }

    /**
     * Define o valor da propriedade serviceName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceName(String value) {
        this.serviceName = value;
    }

    /**
     * Obtém o valor da propriedade refId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRefId() {
        return refId;
    }

    /**
     * Define o valor da propriedade refId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRefId(String value) {
        this.refId = value;
    }

    /**
     * Obtém o valor da propriedade source.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSource() {
        return source;
    }

    /**
     * Define o valor da propriedade source.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSource(String value) {
        this.source = value;
    }

    /**
     * Gets the value of the attribute property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the attribute property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAttribute().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AttributeType }
     * 
     * 
     */
    public List<AttributeType> getAttribute() {
        if (attribute == null) {
            attribute = new ArrayList<AttributeType>();
        }
        return this.attribute;
    }

    /**
     * Obtém o valor da propriedade gtmServicePreferenceId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGtmServicePreferenceId() {
        return gtmServicePreferenceId;
    }

    /**
     * Define o valor da propriedade gtmServicePreferenceId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGtmServicePreferenceId(String value) {
        this.gtmServicePreferenceId = value;
    }

    /**
     * Obtém o valor da propriedade restrictedParty.
     * 
     * @return
     *     possible object is
     *     {@link RestrictedPartyType }
     *     
     */
    public RestrictedPartyType getRestrictedParty() {
        return restrictedParty;
    }

    /**
     * Define o valor da propriedade restrictedParty.
     * 
     * @param value
     *     allowed object is
     *     {@link RestrictedPartyType }
     *     
     */
    public void setRestrictedParty(RestrictedPartyType value) {
        this.restrictedParty = value;
    }

    /**
     * Obtém o valor da propriedade sanctionedTerritory.
     * 
     * @return
     *     possible object is
     *     {@link SanctionedTerritoryType }
     *     
     */
    public SanctionedTerritoryType getSanctionedTerritory() {
        return sanctionedTerritory;
    }

    /**
     * Define o valor da propriedade sanctionedTerritory.
     * 
     * @param value
     *     allowed object is
     *     {@link SanctionedTerritoryType }
     *     
     */
    public void setSanctionedTerritory(SanctionedTerritoryType value) {
        this.sanctionedTerritory = value;
    }

    /**
     * Obtém o valor da propriedade classification.
     * 
     * @return
     *     possible object is
     *     {@link ClassificationType }
     *     
     */
    public ClassificationType getClassification() {
        return classification;
    }

    /**
     * Define o valor da propriedade classification.
     * 
     * @param value
     *     allowed object is
     *     {@link ClassificationType }
     *     
     */
    public void setClassification(ClassificationType value) {
        this.classification = value;
    }

    /**
     * Obtém o valor da propriedade complianceRule.
     * 
     * @return
     *     possible object is
     *     {@link ComplianceRuleType }
     *     
     */
    public ComplianceRuleType getComplianceRule() {
        return complianceRule;
    }

    /**
     * Define o valor da propriedade complianceRule.
     * 
     * @param value
     *     allowed object is
     *     {@link ComplianceRuleType }
     *     
     */
    public void setComplianceRule(ComplianceRuleType value) {
        this.complianceRule = value;
    }

    /**
     * Obtém o valor da propriedade licenseDetermination.
     * 
     * @return
     *     possible object is
     *     {@link LicenseDeterminationType }
     *     
     */
    public LicenseDeterminationType getLicenseDetermination() {
        return licenseDetermination;
    }

    /**
     * Define o valor da propriedade licenseDetermination.
     * 
     * @param value
     *     allowed object is
     *     {@link LicenseDeterminationType }
     *     
     */
    public void setLicenseDetermination(LicenseDeterminationType value) {
        this.licenseDetermination = value;
    }

}
