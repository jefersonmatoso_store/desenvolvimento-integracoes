
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Specifies shipment appointment information.
 * 
 * <p>Classe Java de AppointmentInfoType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="AppointmentInfoType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AppointmentGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;choice>
 *           &lt;element name="LocationResourceGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *           &lt;element name="LocationResourceType" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LocationResourceTypeType"/>
 *         &lt;/choice>
 *         &lt;choice>
 *           &lt;element name="ShipmentGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *           &lt;element name="ShipmentGroupGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;/choice>
 *         &lt;element name="StopSequence" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="StartTime" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType"/>
 *         &lt;element name="EndTime" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType"/>
 *         &lt;element name="IsFeasible" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InfeasibleReason" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsFixed" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsBlocked" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ConfirmationNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ApptActivityType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AppointmentInfoType", propOrder = {
    "appointmentGid",
    "locationResourceGid",
    "locationResourceType",
    "shipmentGid",
    "shipmentGroupGid",
    "stopSequence",
    "startTime",
    "endTime",
    "isFeasible",
    "infeasibleReason",
    "isFixed",
    "isBlocked",
    "confirmationNumber",
    "apptActivityType"
})
public class AppointmentInfoType {

    @XmlElement(name = "AppointmentGid")
    protected GLogXMLGidType appointmentGid;
    @XmlElement(name = "LocationResourceGid")
    protected GLogXMLGidType locationResourceGid;
    @XmlElement(name = "LocationResourceType")
    protected LocationResourceTypeType locationResourceType;
    @XmlElement(name = "ShipmentGid")
    protected GLogXMLGidType shipmentGid;
    @XmlElement(name = "ShipmentGroupGid")
    protected GLogXMLGidType shipmentGroupGid;
    @XmlElement(name = "StopSequence")
    protected String stopSequence;
    @XmlElement(name = "StartTime", required = true)
    protected GLogDateTimeType startTime;
    @XmlElement(name = "EndTime", required = true)
    protected GLogDateTimeType endTime;
    @XmlElement(name = "IsFeasible")
    protected String isFeasible;
    @XmlElement(name = "InfeasibleReason")
    protected String infeasibleReason;
    @XmlElement(name = "IsFixed")
    protected String isFixed;
    @XmlElement(name = "IsBlocked")
    protected String isBlocked;
    @XmlElement(name = "ConfirmationNumber")
    protected String confirmationNumber;
    @XmlElement(name = "ApptActivityType")
    protected String apptActivityType;

    /**
     * Obtém o valor da propriedade appointmentGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getAppointmentGid() {
        return appointmentGid;
    }

    /**
     * Define o valor da propriedade appointmentGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setAppointmentGid(GLogXMLGidType value) {
        this.appointmentGid = value;
    }

    /**
     * Obtém o valor da propriedade locationResourceGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getLocationResourceGid() {
        return locationResourceGid;
    }

    /**
     * Define o valor da propriedade locationResourceGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setLocationResourceGid(GLogXMLGidType value) {
        this.locationResourceGid = value;
    }

    /**
     * Obtém o valor da propriedade locationResourceType.
     * 
     * @return
     *     possible object is
     *     {@link LocationResourceTypeType }
     *     
     */
    public LocationResourceTypeType getLocationResourceType() {
        return locationResourceType;
    }

    /**
     * Define o valor da propriedade locationResourceType.
     * 
     * @param value
     *     allowed object is
     *     {@link LocationResourceTypeType }
     *     
     */
    public void setLocationResourceType(LocationResourceTypeType value) {
        this.locationResourceType = value;
    }

    /**
     * Obtém o valor da propriedade shipmentGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getShipmentGid() {
        return shipmentGid;
    }

    /**
     * Define o valor da propriedade shipmentGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setShipmentGid(GLogXMLGidType value) {
        this.shipmentGid = value;
    }

    /**
     * Obtém o valor da propriedade shipmentGroupGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getShipmentGroupGid() {
        return shipmentGroupGid;
    }

    /**
     * Define o valor da propriedade shipmentGroupGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setShipmentGroupGid(GLogXMLGidType value) {
        this.shipmentGroupGid = value;
    }

    /**
     * Obtém o valor da propriedade stopSequence.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStopSequence() {
        return stopSequence;
    }

    /**
     * Define o valor da propriedade stopSequence.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStopSequence(String value) {
        this.stopSequence = value;
    }

    /**
     * Obtém o valor da propriedade startTime.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getStartTime() {
        return startTime;
    }

    /**
     * Define o valor da propriedade startTime.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setStartTime(GLogDateTimeType value) {
        this.startTime = value;
    }

    /**
     * Obtém o valor da propriedade endTime.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getEndTime() {
        return endTime;
    }

    /**
     * Define o valor da propriedade endTime.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setEndTime(GLogDateTimeType value) {
        this.endTime = value;
    }

    /**
     * Obtém o valor da propriedade isFeasible.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsFeasible() {
        return isFeasible;
    }

    /**
     * Define o valor da propriedade isFeasible.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsFeasible(String value) {
        this.isFeasible = value;
    }

    /**
     * Obtém o valor da propriedade infeasibleReason.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInfeasibleReason() {
        return infeasibleReason;
    }

    /**
     * Define o valor da propriedade infeasibleReason.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInfeasibleReason(String value) {
        this.infeasibleReason = value;
    }

    /**
     * Obtém o valor da propriedade isFixed.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsFixed() {
        return isFixed;
    }

    /**
     * Define o valor da propriedade isFixed.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsFixed(String value) {
        this.isFixed = value;
    }

    /**
     * Obtém o valor da propriedade isBlocked.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsBlocked() {
        return isBlocked;
    }

    /**
     * Define o valor da propriedade isBlocked.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsBlocked(String value) {
        this.isBlocked = value;
    }

    /**
     * Obtém o valor da propriedade confirmationNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConfirmationNumber() {
        return confirmationNumber;
    }

    /**
     * Define o valor da propriedade confirmationNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConfirmationNumber(String value) {
        this.confirmationNumber = value;
    }

    /**
     * Obtém o valor da propriedade apptActivityType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApptActivityType() {
        return apptActivityType;
    }

    /**
     * Define o valor da propriedade apptActivityType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApptActivityType(String value) {
        this.apptActivityType = value;
    }

}
