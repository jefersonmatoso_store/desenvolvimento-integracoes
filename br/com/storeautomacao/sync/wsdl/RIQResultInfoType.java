
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Specifies additional rate details information for the RIQ Query Result.
 * 
 * <p>Classe Java de RIQResultInfoType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="RIQResultInfoType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RATE_OFFERING" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RateOfferingType" minOccurs="0"/>
 *         &lt;element name="RATE_GEO" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RateGeoType" minOccurs="0"/>
 *         &lt;element name="RouteCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RouteCodeType" minOccurs="0"/>
 *         &lt;element name="EquipMarks" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}EquipMarksType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RIQResultInfoType", propOrder = {
    "rateoffering",
    "rategeo",
    "routeCode",
    "equipMarks"
})
public class RIQResultInfoType {

    @XmlElement(name = "RATE_OFFERING")
    protected RateOfferingType rateoffering;
    @XmlElement(name = "RATE_GEO")
    protected RateGeoType rategeo;
    @XmlElement(name = "RouteCode")
    protected RouteCodeType routeCode;
    @XmlElement(name = "EquipMarks")
    protected EquipMarksType equipMarks;

    /**
     * Obtém o valor da propriedade rateoffering.
     * 
     * @return
     *     possible object is
     *     {@link RateOfferingType }
     *     
     */
    public RateOfferingType getRATEOFFERING() {
        return rateoffering;
    }

    /**
     * Define o valor da propriedade rateoffering.
     * 
     * @param value
     *     allowed object is
     *     {@link RateOfferingType }
     *     
     */
    public void setRATEOFFERING(RateOfferingType value) {
        this.rateoffering = value;
    }

    /**
     * Obtém o valor da propriedade rategeo.
     * 
     * @return
     *     possible object is
     *     {@link RateGeoType }
     *     
     */
    public RateGeoType getRATEGEO() {
        return rategeo;
    }

    /**
     * Define o valor da propriedade rategeo.
     * 
     * @param value
     *     allowed object is
     *     {@link RateGeoType }
     *     
     */
    public void setRATEGEO(RateGeoType value) {
        this.rategeo = value;
    }

    /**
     * Obtém o valor da propriedade routeCode.
     * 
     * @return
     *     possible object is
     *     {@link RouteCodeType }
     *     
     */
    public RouteCodeType getRouteCode() {
        return routeCode;
    }

    /**
     * Define o valor da propriedade routeCode.
     * 
     * @param value
     *     allowed object is
     *     {@link RouteCodeType }
     *     
     */
    public void setRouteCode(RouteCodeType value) {
        this.routeCode = value;
    }

    /**
     * Obtém o valor da propriedade equipMarks.
     * 
     * @return
     *     possible object is
     *     {@link EquipMarksType }
     *     
     */
    public EquipMarksType getEquipMarks() {
        return equipMarks;
    }

    /**
     * Define o valor da propriedade equipMarks.
     * 
     * @param value
     *     allowed object is
     *     {@link EquipMarksType }
     *     
     */
    public void setEquipMarks(EquipMarksType value) {
        this.equipMarks = value;
    }

}
