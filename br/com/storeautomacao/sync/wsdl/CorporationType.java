
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;


/**
 * Corporation element supports both inbound and outbound integration. Corporation interface is supported such that Corporation can be sent in and out as
 *             an independent transaction.CorporationGid can be left blank, if corporation is being sent as part of Location element. Otherwise, if Corporation is being sent as a
 *             separate transaction, CorproationGid is mandatory.
 *          
 * 
 * <p>Classe Java de CorporationType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="CorporationType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OTMTransactionInOut">
 *       &lt;sequence>
 *         &lt;element name="IntSavedQuery" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}IntSavedQueryType" minOccurs="0"/>
 *         &lt;element name="CorporationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="TransactionCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransactionCodeType" minOccurs="0"/>
 *         &lt;element name="ReplaceChildren" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ReplaceChildrenType" minOccurs="0"/>
 *         &lt;element name="CorporationName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PickupRoutingSeqGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="DropoffRoutingSeqGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="VatRegistration" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}VatRegistrationType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="VatProvincialRegistration" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}VatProvincialRegistrationType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="IsDomainMaster" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsShippingAgentsActive" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsAllowHouseCollect" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MaxHouseCollectAmount" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/>
 *         &lt;element name="InvolvedParty" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}InvolvedPartyType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CorporationType", propOrder = {
    "intSavedQuery",
    "corporationGid",
    "transactionCode",
    "replaceChildren",
    "corporationName",
    "pickupRoutingSeqGid",
    "dropoffRoutingSeqGid",
    "vatRegistration",
    "vatProvincialRegistration",
    "isDomainMaster",
    "isShippingAgentsActive",
    "isAllowHouseCollect",
    "maxHouseCollectAmount",
    "involvedParty"
})
public class CorporationType
    extends OTMTransactionInOut
{

    @XmlElement(name = "IntSavedQuery")
    protected IntSavedQueryType intSavedQuery;
    @XmlElement(name = "CorporationGid")
    protected GLogXMLGidType corporationGid;
    @XmlElement(name = "TransactionCode")
    @XmlSchemaType(name = "string")
    protected TransactionCodeType transactionCode;
    @XmlElement(name = "ReplaceChildren")
    protected ReplaceChildrenType replaceChildren;
    @XmlElement(name = "CorporationName")
    protected String corporationName;
    @XmlElement(name = "PickupRoutingSeqGid")
    protected GLogXMLGidType pickupRoutingSeqGid;
    @XmlElement(name = "DropoffRoutingSeqGid")
    protected GLogXMLGidType dropoffRoutingSeqGid;
    @XmlElement(name = "VatRegistration")
    protected List<VatRegistrationType> vatRegistration;
    @XmlElement(name = "VatProvincialRegistration")
    protected List<VatProvincialRegistrationType> vatProvincialRegistration;
    @XmlElement(name = "IsDomainMaster")
    protected String isDomainMaster;
    @XmlElement(name = "IsShippingAgentsActive")
    protected String isShippingAgentsActive;
    @XmlElement(name = "IsAllowHouseCollect")
    protected String isAllowHouseCollect;
    @XmlElement(name = "MaxHouseCollectAmount")
    protected GLogXMLFinancialAmountType maxHouseCollectAmount;
    @XmlElement(name = "InvolvedParty")
    protected List<InvolvedPartyType> involvedParty;

    /**
     * Obtém o valor da propriedade intSavedQuery.
     * 
     * @return
     *     possible object is
     *     {@link IntSavedQueryType }
     *     
     */
    public IntSavedQueryType getIntSavedQuery() {
        return intSavedQuery;
    }

    /**
     * Define o valor da propriedade intSavedQuery.
     * 
     * @param value
     *     allowed object is
     *     {@link IntSavedQueryType }
     *     
     */
    public void setIntSavedQuery(IntSavedQueryType value) {
        this.intSavedQuery = value;
    }

    /**
     * Obtém o valor da propriedade corporationGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getCorporationGid() {
        return corporationGid;
    }

    /**
     * Define o valor da propriedade corporationGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setCorporationGid(GLogXMLGidType value) {
        this.corporationGid = value;
    }

    /**
     * Obtém o valor da propriedade transactionCode.
     * 
     * @return
     *     possible object is
     *     {@link TransactionCodeType }
     *     
     */
    public TransactionCodeType getTransactionCode() {
        return transactionCode;
    }

    /**
     * Define o valor da propriedade transactionCode.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionCodeType }
     *     
     */
    public void setTransactionCode(TransactionCodeType value) {
        this.transactionCode = value;
    }

    /**
     * Obtém o valor da propriedade replaceChildren.
     * 
     * @return
     *     possible object is
     *     {@link ReplaceChildrenType }
     *     
     */
    public ReplaceChildrenType getReplaceChildren() {
        return replaceChildren;
    }

    /**
     * Define o valor da propriedade replaceChildren.
     * 
     * @param value
     *     allowed object is
     *     {@link ReplaceChildrenType }
     *     
     */
    public void setReplaceChildren(ReplaceChildrenType value) {
        this.replaceChildren = value;
    }

    /**
     * Obtém o valor da propriedade corporationName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorporationName() {
        return corporationName;
    }

    /**
     * Define o valor da propriedade corporationName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorporationName(String value) {
        this.corporationName = value;
    }

    /**
     * Obtém o valor da propriedade pickupRoutingSeqGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getPickupRoutingSeqGid() {
        return pickupRoutingSeqGid;
    }

    /**
     * Define o valor da propriedade pickupRoutingSeqGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setPickupRoutingSeqGid(GLogXMLGidType value) {
        this.pickupRoutingSeqGid = value;
    }

    /**
     * Obtém o valor da propriedade dropoffRoutingSeqGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getDropoffRoutingSeqGid() {
        return dropoffRoutingSeqGid;
    }

    /**
     * Define o valor da propriedade dropoffRoutingSeqGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setDropoffRoutingSeqGid(GLogXMLGidType value) {
        this.dropoffRoutingSeqGid = value;
    }

    /**
     * Gets the value of the vatRegistration property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the vatRegistration property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getVatRegistration().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link VatRegistrationType }
     * 
     * 
     */
    public List<VatRegistrationType> getVatRegistration() {
        if (vatRegistration == null) {
            vatRegistration = new ArrayList<VatRegistrationType>();
        }
        return this.vatRegistration;
    }

    /**
     * Gets the value of the vatProvincialRegistration property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the vatProvincialRegistration property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getVatProvincialRegistration().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link VatProvincialRegistrationType }
     * 
     * 
     */
    public List<VatProvincialRegistrationType> getVatProvincialRegistration() {
        if (vatProvincialRegistration == null) {
            vatProvincialRegistration = new ArrayList<VatProvincialRegistrationType>();
        }
        return this.vatProvincialRegistration;
    }

    /**
     * Obtém o valor da propriedade isDomainMaster.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsDomainMaster() {
        return isDomainMaster;
    }

    /**
     * Define o valor da propriedade isDomainMaster.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsDomainMaster(String value) {
        this.isDomainMaster = value;
    }

    /**
     * Obtém o valor da propriedade isShippingAgentsActive.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsShippingAgentsActive() {
        return isShippingAgentsActive;
    }

    /**
     * Define o valor da propriedade isShippingAgentsActive.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsShippingAgentsActive(String value) {
        this.isShippingAgentsActive = value;
    }

    /**
     * Obtém o valor da propriedade isAllowHouseCollect.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsAllowHouseCollect() {
        return isAllowHouseCollect;
    }

    /**
     * Define o valor da propriedade isAllowHouseCollect.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsAllowHouseCollect(String value) {
        this.isAllowHouseCollect = value;
    }

    /**
     * Obtém o valor da propriedade maxHouseCollectAmount.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getMaxHouseCollectAmount() {
        return maxHouseCollectAmount;
    }

    /**
     * Define o valor da propriedade maxHouseCollectAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setMaxHouseCollectAmount(GLogXMLFinancialAmountType value) {
        this.maxHouseCollectAmount = value;
    }

    /**
     * Gets the value of the involvedParty property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the involvedParty property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInvolvedParty().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link InvolvedPartyType }
     * 
     * 
     */
    public List<InvolvedPartyType> getInvolvedParty() {
        if (involvedParty == null) {
            involvedParty = new ArrayList<InvolvedPartyType>();
        }
        return this.involvedParty;
    }

}
