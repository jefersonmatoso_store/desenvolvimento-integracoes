
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 *             Specifies the relationship between the Order Release Lines and the Invoice Line Item.
 *          
 * 
 * <p>Classe Java de LineItemCostRefType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="LineItemCostRefType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CostReferenceGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="ShipmentCostQualGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LineItemCostRefType", propOrder = {
    "costReferenceGid",
    "shipmentCostQualGid"
})
public class LineItemCostRefType {

    @XmlElement(name = "CostReferenceGid", required = true)
    protected GLogXMLGidType costReferenceGid;
    @XmlElement(name = "ShipmentCostQualGid", required = true)
    protected GLogXMLGidType shipmentCostQualGid;

    /**
     * Obtém o valor da propriedade costReferenceGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getCostReferenceGid() {
        return costReferenceGid;
    }

    /**
     * Define o valor da propriedade costReferenceGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setCostReferenceGid(GLogXMLGidType value) {
        this.costReferenceGid = value;
    }

    /**
     * Obtém o valor da propriedade shipmentCostQualGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getShipmentCostQualGid() {
        return shipmentCostQualGid;
    }

    /**
     * Define o valor da propriedade shipmentCostQualGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setShipmentCostQualGid(GLogXMLGidType value) {
        this.shipmentCostQualGid = value;
    }

}
