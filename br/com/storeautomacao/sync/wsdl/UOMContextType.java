
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de UOMContextType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="UOMContextType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="UOMType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="UOM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UOMContextType", propOrder = {
    "uomType",
    "uom"
})
public class UOMContextType {

    @XmlElement(name = "UOMType")
    protected String uomType;
    @XmlElement(name = "UOM")
    protected String uom;

    /**
     * Obtém o valor da propriedade uomType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUOMType() {
        return uomType;
    }

    /**
     * Define o valor da propriedade uomType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUOMType(String value) {
        this.uomType = value;
    }

    /**
     * Obtém o valor da propriedade uom.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUOM() {
        return uom;
    }

    /**
     * Define o valor da propriedade uom.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUOM(String value) {
        this.uom = value;
    }

}
