
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * VoyageLoc specifies the individual locations within the voyage path.
 * 
 * <p>Classe Java de VoyageLocType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="VoyageLocType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="VoyageLocSequence" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="LocationRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LocationRefType"/>
 *         &lt;element name="DepartureOrArrival" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="DepartureOrArrivalDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType"/>
 *         &lt;element name="EstDepOrArrDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="ActDepOrArrDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VoyageLocType", propOrder = {
    "voyageLocSequence",
    "locationRef",
    "departureOrArrival",
    "departureOrArrivalDt",
    "estDepOrArrDt",
    "actDepOrArrDt"
})
public class VoyageLocType {

    @XmlElement(name = "VoyageLocSequence", required = true)
    protected String voyageLocSequence;
    @XmlElement(name = "LocationRef", required = true)
    protected LocationRefType locationRef;
    @XmlElement(name = "DepartureOrArrival", required = true)
    protected String departureOrArrival;
    @XmlElement(name = "DepartureOrArrivalDt", required = true)
    protected GLogDateTimeType departureOrArrivalDt;
    @XmlElement(name = "EstDepOrArrDt")
    protected GLogDateTimeType estDepOrArrDt;
    @XmlElement(name = "ActDepOrArrDt")
    protected GLogDateTimeType actDepOrArrDt;

    /**
     * Obtém o valor da propriedade voyageLocSequence.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVoyageLocSequence() {
        return voyageLocSequence;
    }

    /**
     * Define o valor da propriedade voyageLocSequence.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVoyageLocSequence(String value) {
        this.voyageLocSequence = value;
    }

    /**
     * Obtém o valor da propriedade locationRef.
     * 
     * @return
     *     possible object is
     *     {@link LocationRefType }
     *     
     */
    public LocationRefType getLocationRef() {
        return locationRef;
    }

    /**
     * Define o valor da propriedade locationRef.
     * 
     * @param value
     *     allowed object is
     *     {@link LocationRefType }
     *     
     */
    public void setLocationRef(LocationRefType value) {
        this.locationRef = value;
    }

    /**
     * Obtém o valor da propriedade departureOrArrival.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDepartureOrArrival() {
        return departureOrArrival;
    }

    /**
     * Define o valor da propriedade departureOrArrival.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDepartureOrArrival(String value) {
        this.departureOrArrival = value;
    }

    /**
     * Obtém o valor da propriedade departureOrArrivalDt.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getDepartureOrArrivalDt() {
        return departureOrArrivalDt;
    }

    /**
     * Define o valor da propriedade departureOrArrivalDt.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setDepartureOrArrivalDt(GLogDateTimeType value) {
        this.departureOrArrivalDt = value;
    }

    /**
     * Obtém o valor da propriedade estDepOrArrDt.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getEstDepOrArrDt() {
        return estDepOrArrDt;
    }

    /**
     * Define o valor da propriedade estDepOrArrDt.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setEstDepOrArrDt(GLogDateTimeType value) {
        this.estDepOrArrDt = value;
    }

    /**
     * Obtém o valor da propriedade actDepOrArrDt.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getActDepOrArrDt() {
        return actDepOrArrDt;
    }

    /**
     * Define o valor da propriedade actDepOrArrDt.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setActDepOrArrDt(GLogDateTimeType value) {
        this.actDepOrArrDt = value;
    }

}
