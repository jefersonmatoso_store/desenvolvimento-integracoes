
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Generic Trade control screening result information.
 *          
 * 
 * <p>Classe Java de ControlScreeningResultGenericType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="ControlScreeningResultGenericType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ControlTypeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="ControlCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ComplianceRuleGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="ComplianceRuleGroupGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="Authorization" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AuthorizationAddedBy" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AuthorizationAddedOn" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ControlScreeningResultGenericType", namespace = "http://xmlns.oracle.com/apps/gtm/transmission/v6.4", propOrder = {
    "controlTypeGid",
    "controlCode",
    "complianceRuleGid",
    "complianceRuleGroupGid",
    "authorization",
    "authorizationAddedBy",
    "authorizationAddedOn"
})
public class ControlScreeningResultGenericType {

    @XmlElement(name = "ControlTypeGid", required = true)
    protected GLogXMLGidType controlTypeGid;
    @XmlElement(name = "ControlCode", required = true)
    protected String controlCode;
    @XmlElement(name = "ComplianceRuleGid", required = true)
    protected GLogXMLGidType complianceRuleGid;
    @XmlElement(name = "ComplianceRuleGroupGid", required = true)
    protected GLogXMLGidType complianceRuleGroupGid;
    @XmlElement(name = "Authorization", required = true)
    protected String authorization;
    @XmlElement(name = "AuthorizationAddedBy", required = true)
    protected String authorizationAddedBy;
    @XmlElement(name = "AuthorizationAddedOn", required = true)
    protected GLogDateTimeType authorizationAddedOn;

    /**
     * Obtém o valor da propriedade controlTypeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getControlTypeGid() {
        return controlTypeGid;
    }

    /**
     * Define o valor da propriedade controlTypeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setControlTypeGid(GLogXMLGidType value) {
        this.controlTypeGid = value;
    }

    /**
     * Obtém o valor da propriedade controlCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getControlCode() {
        return controlCode;
    }

    /**
     * Define o valor da propriedade controlCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setControlCode(String value) {
        this.controlCode = value;
    }

    /**
     * Obtém o valor da propriedade complianceRuleGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getComplianceRuleGid() {
        return complianceRuleGid;
    }

    /**
     * Define o valor da propriedade complianceRuleGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setComplianceRuleGid(GLogXMLGidType value) {
        this.complianceRuleGid = value;
    }

    /**
     * Obtém o valor da propriedade complianceRuleGroupGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getComplianceRuleGroupGid() {
        return complianceRuleGroupGid;
    }

    /**
     * Define o valor da propriedade complianceRuleGroupGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setComplianceRuleGroupGid(GLogXMLGidType value) {
        this.complianceRuleGroupGid = value;
    }

    /**
     * Obtém o valor da propriedade authorization.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAuthorization() {
        return authorization;
    }

    /**
     * Define o valor da propriedade authorization.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAuthorization(String value) {
        this.authorization = value;
    }

    /**
     * Obtém o valor da propriedade authorizationAddedBy.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAuthorizationAddedBy() {
        return authorizationAddedBy;
    }

    /**
     * Define o valor da propriedade authorizationAddedBy.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAuthorizationAddedBy(String value) {
        this.authorizationAddedBy = value;
    }

    /**
     * Obtém o valor da propriedade authorizationAddedOn.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getAuthorizationAddedOn() {
        return authorizationAddedOn;
    }

    /**
     * Define o valor da propriedade authorizationAddedOn.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setAuthorizationAddedOn(GLogDateTimeType value) {
        this.authorizationAddedOn = value;
    }

}
