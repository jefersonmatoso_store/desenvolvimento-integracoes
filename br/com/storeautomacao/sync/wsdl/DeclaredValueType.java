
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 *             Used to define the declared value for the freight.
 *          
 * 
 * <p>Classe Java de DeclaredValueType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="DeclaredValueType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FinancialAmount" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FinancialAmountType"/>
 *         &lt;element name="DeclaredValueQualifierGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DeclaredValueType", propOrder = {
    "financialAmount",
    "declaredValueQualifierGid"
})
public class DeclaredValueType {

    @XmlElement(name = "FinancialAmount", required = true)
    protected FinancialAmountType financialAmount;
    @XmlElement(name = "DeclaredValueQualifierGid", required = true)
    protected GLogXMLGidType declaredValueQualifierGid;

    /**
     * Obtém o valor da propriedade financialAmount.
     * 
     * @return
     *     possible object is
     *     {@link FinancialAmountType }
     *     
     */
    public FinancialAmountType getFinancialAmount() {
        return financialAmount;
    }

    /**
     * Define o valor da propriedade financialAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link FinancialAmountType }
     *     
     */
    public void setFinancialAmount(FinancialAmountType value) {
        this.financialAmount = value;
    }

    /**
     * Obtém o valor da propriedade declaredValueQualifierGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getDeclaredValueQualifierGid() {
        return declaredValueQualifierGid;
    }

    /**
     * Define o valor da propriedade declaredValueQualifierGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setDeclaredValueQualifierGid(GLogXMLGidType value) {
        this.declaredValueQualifierGid = value;
    }

}
