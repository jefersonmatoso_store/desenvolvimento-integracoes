
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * A ShipmentQuery consists of a ShipmentGid. GLog replies to this query by returning the
 *             requested Shipment.
 *          
 * 
 * <p>Classe Java de ShipmentQueryType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="ShipmentQueryType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ShipmentGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="OutXmlProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="IntPreferenceGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ShipmentQueryType", propOrder = {
    "shipmentGid",
    "outXmlProfileGid",
    "intPreferenceGid"
})
public class ShipmentQueryType {

    @XmlElement(name = "ShipmentGid", required = true)
    protected GLogXMLGidType shipmentGid;
    @XmlElement(name = "OutXmlProfileGid")
    protected GLogXMLGidType outXmlProfileGid;
    @XmlElement(name = "IntPreferenceGid")
    protected GLogXMLGidType intPreferenceGid;

    /**
     * Obtém o valor da propriedade shipmentGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getShipmentGid() {
        return shipmentGid;
    }

    /**
     * Define o valor da propriedade shipmentGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setShipmentGid(GLogXMLGidType value) {
        this.shipmentGid = value;
    }

    /**
     * Obtém o valor da propriedade outXmlProfileGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getOutXmlProfileGid() {
        return outXmlProfileGid;
    }

    /**
     * Define o valor da propriedade outXmlProfileGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setOutXmlProfileGid(GLogXMLGidType value) {
        this.outXmlProfileGid = value;
    }

    /**
     * Obtém o valor da propriedade intPreferenceGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getIntPreferenceGid() {
        return intPreferenceGid;
    }

    /**
     * Define o valor da propriedade intPreferenceGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setIntPreferenceGid(GLogXMLGidType value) {
        this.intPreferenceGid = value;
    }

}
