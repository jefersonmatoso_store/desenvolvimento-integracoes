
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 *             The RouteTemplateHdr contains general information about the route template.
 *          
 * 
 * <p>Classe Java de RouteTemplateHdrType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="RouteTemplateHdrType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CreationSource" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsActive" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Perspective" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EffectiveDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType"/>
 *         &lt;element name="ExpirationDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="StartDepotLocRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocRefType" minOccurs="0"/>
 *         &lt;element name="EndDepotLocRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocRefType" minOccurs="0"/>
 *         &lt;element name="ServiceProviderGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="EquipmentGroupProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="TransportModeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="NumInstances" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="IsClosedLoop" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AllowOverlappingShipments" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RouteTempStrategicInfo" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RouteTempStrategicInfoType" minOccurs="0"/>
 *         &lt;element name="UserDefIconInfo" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}UserDefIconInfoType" minOccurs="0"/>
 *         &lt;element name="Indicator" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RouteTemplateHdrType", propOrder = {
    "creationSource",
    "isActive",
    "perspective",
    "effectiveDate",
    "expirationDate",
    "startDepotLocRef",
    "endDepotLocRef",
    "serviceProviderGid",
    "equipmentGroupProfileGid",
    "transportModeGid",
    "numInstances",
    "isClosedLoop",
    "allowOverlappingShipments",
    "routeTempStrategicInfo",
    "userDefIconInfo",
    "indicator"
})
public class RouteTemplateHdrType {

    @XmlElement(name = "CreationSource")
    protected String creationSource;
    @XmlElement(name = "IsActive")
    protected String isActive;
    @XmlElement(name = "Perspective")
    protected String perspective;
    @XmlElement(name = "EffectiveDate", required = true)
    protected GLogDateTimeType effectiveDate;
    @XmlElement(name = "ExpirationDate")
    protected GLogDateTimeType expirationDate;
    @XmlElement(name = "StartDepotLocRef")
    protected GLogXMLLocRefType startDepotLocRef;
    @XmlElement(name = "EndDepotLocRef")
    protected GLogXMLLocRefType endDepotLocRef;
    @XmlElement(name = "ServiceProviderGid")
    protected GLogXMLGidType serviceProviderGid;
    @XmlElement(name = "EquipmentGroupProfileGid")
    protected GLogXMLGidType equipmentGroupProfileGid;
    @XmlElement(name = "TransportModeGid")
    protected GLogXMLGidType transportModeGid;
    @XmlElement(name = "NumInstances", required = true)
    protected String numInstances;
    @XmlElement(name = "IsClosedLoop")
    protected String isClosedLoop;
    @XmlElement(name = "AllowOverlappingShipments")
    protected String allowOverlappingShipments;
    @XmlElement(name = "RouteTempStrategicInfo")
    protected RouteTempStrategicInfoType routeTempStrategicInfo;
    @XmlElement(name = "UserDefIconInfo")
    protected UserDefIconInfoType userDefIconInfo;
    @XmlElement(name = "Indicator")
    protected String indicator;

    /**
     * Obtém o valor da propriedade creationSource.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCreationSource() {
        return creationSource;
    }

    /**
     * Define o valor da propriedade creationSource.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCreationSource(String value) {
        this.creationSource = value;
    }

    /**
     * Obtém o valor da propriedade isActive.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsActive() {
        return isActive;
    }

    /**
     * Define o valor da propriedade isActive.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsActive(String value) {
        this.isActive = value;
    }

    /**
     * Obtém o valor da propriedade perspective.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPerspective() {
        return perspective;
    }

    /**
     * Define o valor da propriedade perspective.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPerspective(String value) {
        this.perspective = value;
    }

    /**
     * Obtém o valor da propriedade effectiveDate.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getEffectiveDate() {
        return effectiveDate;
    }

    /**
     * Define o valor da propriedade effectiveDate.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setEffectiveDate(GLogDateTimeType value) {
        this.effectiveDate = value;
    }

    /**
     * Obtém o valor da propriedade expirationDate.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getExpirationDate() {
        return expirationDate;
    }

    /**
     * Define o valor da propriedade expirationDate.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setExpirationDate(GLogDateTimeType value) {
        this.expirationDate = value;
    }

    /**
     * Obtém o valor da propriedade startDepotLocRef.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public GLogXMLLocRefType getStartDepotLocRef() {
        return startDepotLocRef;
    }

    /**
     * Define o valor da propriedade startDepotLocRef.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public void setStartDepotLocRef(GLogXMLLocRefType value) {
        this.startDepotLocRef = value;
    }

    /**
     * Obtém o valor da propriedade endDepotLocRef.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public GLogXMLLocRefType getEndDepotLocRef() {
        return endDepotLocRef;
    }

    /**
     * Define o valor da propriedade endDepotLocRef.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public void setEndDepotLocRef(GLogXMLLocRefType value) {
        this.endDepotLocRef = value;
    }

    /**
     * Obtém o valor da propriedade serviceProviderGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getServiceProviderGid() {
        return serviceProviderGid;
    }

    /**
     * Define o valor da propriedade serviceProviderGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setServiceProviderGid(GLogXMLGidType value) {
        this.serviceProviderGid = value;
    }

    /**
     * Obtém o valor da propriedade equipmentGroupProfileGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getEquipmentGroupProfileGid() {
        return equipmentGroupProfileGid;
    }

    /**
     * Define o valor da propriedade equipmentGroupProfileGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setEquipmentGroupProfileGid(GLogXMLGidType value) {
        this.equipmentGroupProfileGid = value;
    }

    /**
     * Obtém o valor da propriedade transportModeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getTransportModeGid() {
        return transportModeGid;
    }

    /**
     * Define o valor da propriedade transportModeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setTransportModeGid(GLogXMLGidType value) {
        this.transportModeGid = value;
    }

    /**
     * Obtém o valor da propriedade numInstances.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumInstances() {
        return numInstances;
    }

    /**
     * Define o valor da propriedade numInstances.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumInstances(String value) {
        this.numInstances = value;
    }

    /**
     * Obtém o valor da propriedade isClosedLoop.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsClosedLoop() {
        return isClosedLoop;
    }

    /**
     * Define o valor da propriedade isClosedLoop.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsClosedLoop(String value) {
        this.isClosedLoop = value;
    }

    /**
     * Obtém o valor da propriedade allowOverlappingShipments.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAllowOverlappingShipments() {
        return allowOverlappingShipments;
    }

    /**
     * Define o valor da propriedade allowOverlappingShipments.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAllowOverlappingShipments(String value) {
        this.allowOverlappingShipments = value;
    }

    /**
     * Obtém o valor da propriedade routeTempStrategicInfo.
     * 
     * @return
     *     possible object is
     *     {@link RouteTempStrategicInfoType }
     *     
     */
    public RouteTempStrategicInfoType getRouteTempStrategicInfo() {
        return routeTempStrategicInfo;
    }

    /**
     * Define o valor da propriedade routeTempStrategicInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link RouteTempStrategicInfoType }
     *     
     */
    public void setRouteTempStrategicInfo(RouteTempStrategicInfoType value) {
        this.routeTempStrategicInfo = value;
    }

    /**
     * Obtém o valor da propriedade userDefIconInfo.
     * 
     * @return
     *     possible object is
     *     {@link UserDefIconInfoType }
     *     
     */
    public UserDefIconInfoType getUserDefIconInfo() {
        return userDefIconInfo;
    }

    /**
     * Define o valor da propriedade userDefIconInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link UserDefIconInfoType }
     *     
     */
    public void setUserDefIconInfo(UserDefIconInfoType value) {
        this.userDefIconInfo = value;
    }

    /**
     * Obtém o valor da propriedade indicator.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIndicator() {
        return indicator;
    }

    /**
     * Define o valor da propriedade indicator.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIndicator(String value) {
        this.indicator = value;
    }

}
