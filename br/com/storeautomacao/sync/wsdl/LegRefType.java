
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * LegRef specifies the Leg.
 * 
 * <p>Classe Java de LegRefType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="LegRefType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element name="LegGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="Leg" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LegType"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LegRefType", propOrder = {
    "legGid",
    "leg"
})
public class LegRefType {

    @XmlElement(name = "LegGid")
    protected GLogXMLGidType legGid;
    @XmlElement(name = "Leg")
    protected LegType leg;

    /**
     * Obtém o valor da propriedade legGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getLegGid() {
        return legGid;
    }

    /**
     * Define o valor da propriedade legGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setLegGid(GLogXMLGidType value) {
        this.legGid = value;
    }

    /**
     * Obtém o valor da propriedade leg.
     * 
     * @return
     *     possible object is
     *     {@link LegType }
     *     
     */
    public LegType getLeg() {
        return leg;
    }

    /**
     * Define o valor da propriedade leg.
     * 
     * @param value
     *     allowed object is
     *     {@link LegType }
     *     
     */
    public void setLeg(LegType value) {
        this.leg = value;
    }

}
