
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Specifies the Master B/L Consignee information.
 * 
 * <p>Classe Java de MasterBLConsigneeInfoType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="MasterBLConsigneeInfoType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="MasterBLConsigneeType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ContactRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ContactRefType" minOccurs="0"/>
 *         &lt;element name="ComMethodGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MasterBLConsigneeInfoType", propOrder = {
    "masterBLConsigneeType",
    "contactRef",
    "comMethodGid"
})
public class MasterBLConsigneeInfoType {

    @XmlElement(name = "MasterBLConsigneeType")
    protected String masterBLConsigneeType;
    @XmlElement(name = "ContactRef")
    protected ContactRefType contactRef;
    @XmlElement(name = "ComMethodGid")
    protected GLogXMLGidType comMethodGid;

    /**
     * Obtém o valor da propriedade masterBLConsigneeType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMasterBLConsigneeType() {
        return masterBLConsigneeType;
    }

    /**
     * Define o valor da propriedade masterBLConsigneeType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMasterBLConsigneeType(String value) {
        this.masterBLConsigneeType = value;
    }

    /**
     * Obtém o valor da propriedade contactRef.
     * 
     * @return
     *     possible object is
     *     {@link ContactRefType }
     *     
     */
    public ContactRefType getContactRef() {
        return contactRef;
    }

    /**
     * Define o valor da propriedade contactRef.
     * 
     * @param value
     *     allowed object is
     *     {@link ContactRefType }
     *     
     */
    public void setContactRef(ContactRefType value) {
        this.contactRef = value;
    }

    /**
     * Obtém o valor da propriedade comMethodGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getComMethodGid() {
        return comMethodGid;
    }

    /**
     * Define o valor da propriedade comMethodGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setComMethodGid(GLogXMLGidType value) {
        this.comMethodGid = value;
    }

}
