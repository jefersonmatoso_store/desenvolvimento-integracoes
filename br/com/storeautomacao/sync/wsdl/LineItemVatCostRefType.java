
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 *             Specifies the cost reference information for the VAT Line Items.
 *          
 * 
 * <p>Classe Java de LineItemVatCostRefType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="LineItemVatCostRefType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CostReferenceGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="CostQualGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="VatBasisAmount" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/>
 *         &lt;element name="VatAmount" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LineItemVatCostRefType", propOrder = {
    "costReferenceGid",
    "costQualGid",
    "vatBasisAmount",
    "vatAmount"
})
public class LineItemVatCostRefType {

    @XmlElement(name = "CostReferenceGid", required = true)
    protected GLogXMLGidType costReferenceGid;
    @XmlElement(name = "CostQualGid")
    protected GLogXMLGidType costQualGid;
    @XmlElement(name = "VatBasisAmount")
    protected GLogXMLFinancialAmountType vatBasisAmount;
    @XmlElement(name = "VatAmount")
    protected GLogXMLFinancialAmountType vatAmount;

    /**
     * Obtém o valor da propriedade costReferenceGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getCostReferenceGid() {
        return costReferenceGid;
    }

    /**
     * Define o valor da propriedade costReferenceGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setCostReferenceGid(GLogXMLGidType value) {
        this.costReferenceGid = value;
    }

    /**
     * Obtém o valor da propriedade costQualGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getCostQualGid() {
        return costQualGid;
    }

    /**
     * Define o valor da propriedade costQualGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setCostQualGid(GLogXMLGidType value) {
        this.costQualGid = value;
    }

    /**
     * Obtém o valor da propriedade vatBasisAmount.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getVatBasisAmount() {
        return vatBasisAmount;
    }

    /**
     * Define o valor da propriedade vatBasisAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setVatBasisAmount(GLogXMLFinancialAmountType value) {
        this.vatBasisAmount = value;
    }

    /**
     * Obtém o valor da propriedade vatAmount.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getVatAmount() {
        return vatAmount;
    }

    /**
     * Define o valor da propriedade vatAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setVatAmount(GLogXMLFinancialAmountType value) {
        this.vatAmount = value;
    }

}
