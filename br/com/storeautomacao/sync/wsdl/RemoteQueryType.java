
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de RemoteQueryType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="RemoteQueryType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OTMTransactionIn">
 *       &lt;choice>
 *         &lt;element name="RIQQuery" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RIQQueryType"/>
 *         &lt;element name="ShipmentQuery" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ShipmentQueryType"/>
 *         &lt;element name="TransmissionReportQuery" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransmissionReportQueryType"/>
 *         &lt;element name="GenericQuery" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GenericQueryType"/>
 *         &lt;element name="AppointmentQuery" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}AppointmentQueryType"/>
 *         &lt;element name="OrderRoutingRuleQuery" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OrderRoutingRuleQueryType"/>
 *       &lt;/choice>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RemoteQueryType", propOrder = {
    "riqQuery",
    "shipmentQuery",
    "transmissionReportQuery",
    "genericQuery",
    "appointmentQuery",
    "orderRoutingRuleQuery"
})
public class RemoteQueryType
    extends OTMTransactionIn
{

    @XmlElement(name = "RIQQuery")
    protected RIQQueryType riqQuery;
    @XmlElement(name = "ShipmentQuery")
    protected ShipmentQueryType shipmentQuery;
    @XmlElement(name = "TransmissionReportQuery")
    protected TransmissionReportQueryType transmissionReportQuery;
    @XmlElement(name = "GenericQuery")
    protected GenericQueryType genericQuery;
    @XmlElement(name = "AppointmentQuery")
    protected AppointmentQueryType appointmentQuery;
    @XmlElement(name = "OrderRoutingRuleQuery")
    protected OrderRoutingRuleQueryType orderRoutingRuleQuery;

    /**
     * Obtém o valor da propriedade riqQuery.
     * 
     * @return
     *     possible object is
     *     {@link RIQQueryType }
     *     
     */
    public RIQQueryType getRIQQuery() {
        return riqQuery;
    }

    /**
     * Define o valor da propriedade riqQuery.
     * 
     * @param value
     *     allowed object is
     *     {@link RIQQueryType }
     *     
     */
    public void setRIQQuery(RIQQueryType value) {
        this.riqQuery = value;
    }

    /**
     * Obtém o valor da propriedade shipmentQuery.
     * 
     * @return
     *     possible object is
     *     {@link ShipmentQueryType }
     *     
     */
    public ShipmentQueryType getShipmentQuery() {
        return shipmentQuery;
    }

    /**
     * Define o valor da propriedade shipmentQuery.
     * 
     * @param value
     *     allowed object is
     *     {@link ShipmentQueryType }
     *     
     */
    public void setShipmentQuery(ShipmentQueryType value) {
        this.shipmentQuery = value;
    }

    /**
     * Obtém o valor da propriedade transmissionReportQuery.
     * 
     * @return
     *     possible object is
     *     {@link TransmissionReportQueryType }
     *     
     */
    public TransmissionReportQueryType getTransmissionReportQuery() {
        return transmissionReportQuery;
    }

    /**
     * Define o valor da propriedade transmissionReportQuery.
     * 
     * @param value
     *     allowed object is
     *     {@link TransmissionReportQueryType }
     *     
     */
    public void setTransmissionReportQuery(TransmissionReportQueryType value) {
        this.transmissionReportQuery = value;
    }

    /**
     * Obtém o valor da propriedade genericQuery.
     * 
     * @return
     *     possible object is
     *     {@link GenericQueryType }
     *     
     */
    public GenericQueryType getGenericQuery() {
        return genericQuery;
    }

    /**
     * Define o valor da propriedade genericQuery.
     * 
     * @param value
     *     allowed object is
     *     {@link GenericQueryType }
     *     
     */
    public void setGenericQuery(GenericQueryType value) {
        this.genericQuery = value;
    }

    /**
     * Obtém o valor da propriedade appointmentQuery.
     * 
     * @return
     *     possible object is
     *     {@link AppointmentQueryType }
     *     
     */
    public AppointmentQueryType getAppointmentQuery() {
        return appointmentQuery;
    }

    /**
     * Define o valor da propriedade appointmentQuery.
     * 
     * @param value
     *     allowed object is
     *     {@link AppointmentQueryType }
     *     
     */
    public void setAppointmentQuery(AppointmentQueryType value) {
        this.appointmentQuery = value;
    }

    /**
     * Obtém o valor da propriedade orderRoutingRuleQuery.
     * 
     * @return
     *     possible object is
     *     {@link OrderRoutingRuleQueryType }
     *     
     */
    public OrderRoutingRuleQueryType getOrderRoutingRuleQuery() {
        return orderRoutingRuleQuery;
    }

    /**
     * Define o valor da propriedade orderRoutingRuleQuery.
     * 
     * @param value
     *     allowed object is
     *     {@link OrderRoutingRuleQueryType }
     *     
     */
    public void setOrderRoutingRuleQuery(OrderRoutingRuleQueryType value) {
        this.orderRoutingRuleQuery = value;
    }

}
