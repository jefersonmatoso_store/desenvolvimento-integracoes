
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * 
 *             The OceanDetail provides invoice data specific to ocean carriers.
 *          
 * 
 * <p>Classe Java de OceanDetailType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="OceanDetailType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="VesselInfo" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}VesselInfoType" minOccurs="0"/>
 *         &lt;element name="InvoiceServiceCodeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="DeliveryDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="OceanEquipment" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OceanEquipmentType" maxOccurs="unbounded"/>
 *         &lt;element name="OceanLineItem" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OceanLineItemType" maxOccurs="unbounded"/>
 *         &lt;element name="Port" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}PortType" maxOccurs="unbounded"/>
 *         &lt;element name="LetterofCredit" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LetterOfCreditType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OceanDetailType", propOrder = {
    "vesselInfo",
    "invoiceServiceCodeGid",
    "deliveryDate",
    "oceanEquipment",
    "oceanLineItem",
    "port",
    "letterofCredit"
})
public class OceanDetailType {

    @XmlElement(name = "VesselInfo")
    protected VesselInfoType vesselInfo;
    @XmlElement(name = "InvoiceServiceCodeGid", required = true)
    protected GLogXMLGidType invoiceServiceCodeGid;
    @XmlElement(name = "DeliveryDate")
    protected GLogDateTimeType deliveryDate;
    @XmlElement(name = "OceanEquipment", required = true)
    protected List<OceanEquipmentType> oceanEquipment;
    @XmlElement(name = "OceanLineItem", required = true)
    protected List<OceanLineItemType> oceanLineItem;
    @XmlElement(name = "Port", required = true)
    protected List<PortType> port;
    @XmlElement(name = "LetterofCredit")
    protected LetterOfCreditType letterofCredit;

    /**
     * Obtém o valor da propriedade vesselInfo.
     * 
     * @return
     *     possible object is
     *     {@link VesselInfoType }
     *     
     */
    public VesselInfoType getVesselInfo() {
        return vesselInfo;
    }

    /**
     * Define o valor da propriedade vesselInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link VesselInfoType }
     *     
     */
    public void setVesselInfo(VesselInfoType value) {
        this.vesselInfo = value;
    }

    /**
     * Obtém o valor da propriedade invoiceServiceCodeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getInvoiceServiceCodeGid() {
        return invoiceServiceCodeGid;
    }

    /**
     * Define o valor da propriedade invoiceServiceCodeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setInvoiceServiceCodeGid(GLogXMLGidType value) {
        this.invoiceServiceCodeGid = value;
    }

    /**
     * Obtém o valor da propriedade deliveryDate.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getDeliveryDate() {
        return deliveryDate;
    }

    /**
     * Define o valor da propriedade deliveryDate.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setDeliveryDate(GLogDateTimeType value) {
        this.deliveryDate = value;
    }

    /**
     * Gets the value of the oceanEquipment property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the oceanEquipment property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOceanEquipment().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OceanEquipmentType }
     * 
     * 
     */
    public List<OceanEquipmentType> getOceanEquipment() {
        if (oceanEquipment == null) {
            oceanEquipment = new ArrayList<OceanEquipmentType>();
        }
        return this.oceanEquipment;
    }

    /**
     * Gets the value of the oceanLineItem property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the oceanLineItem property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOceanLineItem().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OceanLineItemType }
     * 
     * 
     */
    public List<OceanLineItemType> getOceanLineItem() {
        if (oceanLineItem == null) {
            oceanLineItem = new ArrayList<OceanLineItemType>();
        }
        return this.oceanLineItem;
    }

    /**
     * Gets the value of the port property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the port property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPort().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PortType }
     * 
     * 
     */
    public List<PortType> getPort() {
        if (port == null) {
            port = new ArrayList<PortType>();
        }
        return this.port;
    }

    /**
     * Obtém o valor da propriedade letterofCredit.
     * 
     * @return
     *     possible object is
     *     {@link LetterOfCreditType }
     *     
     */
    public LetterOfCreditType getLetterofCredit() {
        return letterofCredit;
    }

    /**
     * Define o valor da propriedade letterofCredit.
     * 
     * @param value
     *     allowed object is
     *     {@link LetterOfCreditType }
     *     
     */
    public void setLetterofCredit(LetterOfCreditType value) {
        this.letterofCredit = value;
    }

}
