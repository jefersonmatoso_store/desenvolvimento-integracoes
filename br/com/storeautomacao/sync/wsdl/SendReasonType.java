
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * SendReason is used to indicate the reason the notification/data/information is being sent to the external system.
 * 
 * <p>Classe Java de SendReasonType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="SendReasonType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Remark" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RemarkType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="ProcessControlRequestID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SendReasonGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="ObjectType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="EventReason" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}EventReasonType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SendReasonType", propOrder = {
    "remark",
    "processControlRequestID",
    "sendReasonGid",
    "objectType",
    "eventReason"
})
public class SendReasonType {

    @XmlElement(name = "Remark")
    protected List<RemarkType> remark;
    @XmlElement(name = "ProcessControlRequestID")
    protected String processControlRequestID;
    @XmlElement(name = "SendReasonGid", required = true)
    protected GLogXMLGidType sendReasonGid;
    @XmlElement(name = "ObjectType", required = true)
    protected String objectType;
    @XmlElement(name = "EventReason")
    protected EventReasonType eventReason;

    /**
     * Gets the value of the remark property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the remark property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRemark().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RemarkType }
     * 
     * 
     */
    public List<RemarkType> getRemark() {
        if (remark == null) {
            remark = new ArrayList<RemarkType>();
        }
        return this.remark;
    }

    /**
     * Obtém o valor da propriedade processControlRequestID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProcessControlRequestID() {
        return processControlRequestID;
    }

    /**
     * Define o valor da propriedade processControlRequestID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProcessControlRequestID(String value) {
        this.processControlRequestID = value;
    }

    /**
     * Obtém o valor da propriedade sendReasonGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getSendReasonGid() {
        return sendReasonGid;
    }

    /**
     * Define o valor da propriedade sendReasonGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setSendReasonGid(GLogXMLGidType value) {
        this.sendReasonGid = value;
    }

    /**
     * Obtém o valor da propriedade objectType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getObjectType() {
        return objectType;
    }

    /**
     * Define o valor da propriedade objectType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setObjectType(String value) {
        this.objectType = value;
    }

    /**
     * Obtém o valor da propriedade eventReason.
     * 
     * @return
     *     possible object is
     *     {@link EventReasonType }
     *     
     */
    public EventReasonType getEventReason() {
        return eventReason;
    }

    /**
     * Define o valor da propriedade eventReason.
     * 
     * @param value
     *     allowed object is
     *     {@link EventReasonType }
     *     
     */
    public void setEventReason(EventReasonType value) {
        this.eventReason = value;
    }

}
