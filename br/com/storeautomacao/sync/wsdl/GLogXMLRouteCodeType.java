
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Contains the RouteCode represting the combined RouteCode for the Rule_11 type rail shipments.
 * 
 * <p>Classe Java de GLogXMLRouteCodeType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="GLogXMLRouteCodeType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RouteCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RouteCodeType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GLogXMLRouteCodeType", propOrder = {
    "routeCode"
})
public class GLogXMLRouteCodeType {

    @XmlElement(name = "RouteCode", required = true)
    protected RouteCodeType routeCode;

    /**
     * Obtém o valor da propriedade routeCode.
     * 
     * @return
     *     possible object is
     *     {@link RouteCodeType }
     *     
     */
    public RouteCodeType getRouteCode() {
        return routeCode;
    }

    /**
     * Define o valor da propriedade routeCode.
     * 
     * @param value
     *     allowed object is
     *     {@link RouteCodeType }
     *     
     */
    public void setRouteCode(RouteCodeType value) {
        this.routeCode = value;
    }

}
