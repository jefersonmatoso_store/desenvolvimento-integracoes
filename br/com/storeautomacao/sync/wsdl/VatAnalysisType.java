
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * VAT amounts for each code on the invoice. The VatAnalysis element is outbound only.
 * 
 * <p>Classe Java de VatAnalysisType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="VatAnalysisType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="VatCodeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="TaxAmount" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType"/>
 *         &lt;element name="ExchangeRateInfo" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ExchangeRateInfoType" minOccurs="0"/>
 *         &lt;element name="VatBasisAmount" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/>
 *         &lt;element name="VatRate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VatAnalysisType", propOrder = {
    "vatCodeGid",
    "taxAmount",
    "exchangeRateInfo",
    "vatBasisAmount",
    "vatRate"
})
public class VatAnalysisType {

    @XmlElement(name = "VatCodeGid", required = true)
    protected GLogXMLGidType vatCodeGid;
    @XmlElement(name = "TaxAmount", required = true)
    protected GLogXMLFinancialAmountType taxAmount;
    @XmlElement(name = "ExchangeRateInfo")
    protected ExchangeRateInfoType exchangeRateInfo;
    @XmlElement(name = "VatBasisAmount")
    protected GLogXMLFinancialAmountType vatBasisAmount;
    @XmlElement(name = "VatRate")
    protected String vatRate;

    /**
     * Obtém o valor da propriedade vatCodeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getVatCodeGid() {
        return vatCodeGid;
    }

    /**
     * Define o valor da propriedade vatCodeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setVatCodeGid(GLogXMLGidType value) {
        this.vatCodeGid = value;
    }

    /**
     * Obtém o valor da propriedade taxAmount.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getTaxAmount() {
        return taxAmount;
    }

    /**
     * Define o valor da propriedade taxAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setTaxAmount(GLogXMLFinancialAmountType value) {
        this.taxAmount = value;
    }

    /**
     * Obtém o valor da propriedade exchangeRateInfo.
     * 
     * @return
     *     possible object is
     *     {@link ExchangeRateInfoType }
     *     
     */
    public ExchangeRateInfoType getExchangeRateInfo() {
        return exchangeRateInfo;
    }

    /**
     * Define o valor da propriedade exchangeRateInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link ExchangeRateInfoType }
     *     
     */
    public void setExchangeRateInfo(ExchangeRateInfoType value) {
        this.exchangeRateInfo = value;
    }

    /**
     * Obtém o valor da propriedade vatBasisAmount.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getVatBasisAmount() {
        return vatBasisAmount;
    }

    /**
     * Define o valor da propriedade vatBasisAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setVatBasisAmount(GLogXMLFinancialAmountType value) {
        this.vatBasisAmount = value;
    }

    /**
     * Obtém o valor da propriedade vatRate.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVatRate() {
        return vatRate;
    }

    /**
     * Define o valor da propriedade vatRate.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVatRate(String value) {
        this.vatRate = value;
    }

}
