
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.*;


/**
 * Describes a calender event for a specific driver
 * 
 * <p>Classe Java de DriverCalendarEventType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="DriverCalendarEventType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OTMTransactionIn">
 *       &lt;sequence>
 *         &lt;element name="IntSavedQuery" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}IntSavedQueryType" minOccurs="0"/>
 *         &lt;element name="DriverCalEventGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="TransactionCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransactionCodeType"/>
 *         &lt;element name="DriverGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="EventStartTime" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType"/>
 *         &lt;element name="EventEndTime" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType"/>
 *         &lt;element name="EventLocGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="CalendarEventTypeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DriverCalendarEventType", propOrder = {
    "intSavedQuery",
    "driverCalEventGid",
    "transactionCode",
    "driverGid",
    "eventStartTime",
    "eventEndTime",
    "eventLocGid",
    "calendarEventTypeGid"
})
public class DriverCalendarEventType
    extends OTMTransactionIn
{

    @XmlElement(name = "IntSavedQuery")
    protected IntSavedQueryType intSavedQuery;
    @XmlElement(name = "DriverCalEventGid")
    protected GLogXMLGidType driverCalEventGid;
    @XmlElement(name = "TransactionCode", required = true)
    @XmlSchemaType(name = "string")
    protected TransactionCodeType transactionCode;
    @XmlElement(name = "DriverGid", required = true)
    protected GLogXMLGidType driverGid;
    @XmlElement(name = "EventStartTime", required = true)
    protected GLogDateTimeType eventStartTime;
    @XmlElement(name = "EventEndTime", required = true)
    protected GLogDateTimeType eventEndTime;
    @XmlElement(name = "EventLocGid")
    protected GLogXMLGidType eventLocGid;
    @XmlElement(name = "CalendarEventTypeGid", required = true)
    protected GLogXMLGidType calendarEventTypeGid;

    /**
     * Obtém o valor da propriedade intSavedQuery.
     * 
     * @return
     *     possible object is
     *     {@link IntSavedQueryType }
     *     
     */
    public IntSavedQueryType getIntSavedQuery() {
        return intSavedQuery;
    }

    /**
     * Define o valor da propriedade intSavedQuery.
     * 
     * @param value
     *     allowed object is
     *     {@link IntSavedQueryType }
     *     
     */
    public void setIntSavedQuery(IntSavedQueryType value) {
        this.intSavedQuery = value;
    }

    /**
     * Obtém o valor da propriedade driverCalEventGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getDriverCalEventGid() {
        return driverCalEventGid;
    }

    /**
     * Define o valor da propriedade driverCalEventGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setDriverCalEventGid(GLogXMLGidType value) {
        this.driverCalEventGid = value;
    }

    /**
     * Obtém o valor da propriedade transactionCode.
     * 
     * @return
     *     possible object is
     *     {@link TransactionCodeType }
     *     
     */
    public TransactionCodeType getTransactionCode() {
        return transactionCode;
    }

    /**
     * Define o valor da propriedade transactionCode.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionCodeType }
     *     
     */
    public void setTransactionCode(TransactionCodeType value) {
        this.transactionCode = value;
    }

    /**
     * Obtém o valor da propriedade driverGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getDriverGid() {
        return driverGid;
    }

    /**
     * Define o valor da propriedade driverGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setDriverGid(GLogXMLGidType value) {
        this.driverGid = value;
    }

    /**
     * Obtém o valor da propriedade eventStartTime.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getEventStartTime() {
        return eventStartTime;
    }

    /**
     * Define o valor da propriedade eventStartTime.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setEventStartTime(GLogDateTimeType value) {
        this.eventStartTime = value;
    }

    /**
     * Obtém o valor da propriedade eventEndTime.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getEventEndTime() {
        return eventEndTime;
    }

    /**
     * Define o valor da propriedade eventEndTime.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setEventEndTime(GLogDateTimeType value) {
        this.eventEndTime = value;
    }

    /**
     * Obtém o valor da propriedade eventLocGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getEventLocGid() {
        return eventLocGid;
    }

    /**
     * Define o valor da propriedade eventLocGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setEventLocGid(GLogXMLGidType value) {
        this.eventLocGid = value;
    }

    /**
     * Obtém o valor da propriedade calendarEventTypeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getCalendarEventTypeGid() {
        return calendarEventTypeGid;
    }

    /**
     * Define o valor da propriedade calendarEventTypeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setCalendarEventTypeGid(GLogXMLGidType value) {
        this.calendarEventTypeGid = value;
    }

}
