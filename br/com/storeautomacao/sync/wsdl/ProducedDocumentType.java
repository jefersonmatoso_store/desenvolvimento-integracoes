
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de ProducedDocumentType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="ProducedDocumentType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ProducedDocumentTypeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Code" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ReferenceNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Date" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="ValidationLocation" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LocationRefType" minOccurs="0"/>
 *         &lt;element name="ArchiveLocation" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LocationRefType" minOccurs="0"/>
 *         &lt;element name="ArchiveFormat" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ArchiveDetail" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProducedDocumentType", namespace = "http://xmlns.oracle.com/apps/gtm/transmission/v6.4", propOrder = {
    "producedDocumentTypeGid",
    "name",
    "code",
    "referenceNumber",
    "date",
    "validationLocation",
    "archiveLocation",
    "archiveFormat",
    "archiveDetail"
})
public class ProducedDocumentType {

    @XmlElement(name = "ProducedDocumentTypeGid")
    protected GLogXMLGidType producedDocumentTypeGid;
    @XmlElement(name = "Name")
    protected String name;
    @XmlElement(name = "Code")
    protected String code;
    @XmlElement(name = "ReferenceNumber")
    protected String referenceNumber;
    @XmlElement(name = "Date")
    protected GLogDateTimeType date;
    @XmlElement(name = "ValidationLocation")
    protected LocationRefType validationLocation;
    @XmlElement(name = "ArchiveLocation")
    protected LocationRefType archiveLocation;
    @XmlElement(name = "ArchiveFormat")
    protected String archiveFormat;
    @XmlElement(name = "ArchiveDetail")
    protected String archiveDetail;

    /**
     * Obtém o valor da propriedade producedDocumentTypeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getProducedDocumentTypeGid() {
        return producedDocumentTypeGid;
    }

    /**
     * Define o valor da propriedade producedDocumentTypeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setProducedDocumentTypeGid(GLogXMLGidType value) {
        this.producedDocumentTypeGid = value;
    }

    /**
     * Obtém o valor da propriedade name.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Define o valor da propriedade name.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Obtém o valor da propriedade code.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCode() {
        return code;
    }

    /**
     * Define o valor da propriedade code.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCode(String value) {
        this.code = value;
    }

    /**
     * Obtém o valor da propriedade referenceNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReferenceNumber() {
        return referenceNumber;
    }

    /**
     * Define o valor da propriedade referenceNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReferenceNumber(String value) {
        this.referenceNumber = value;
    }

    /**
     * Obtém o valor da propriedade date.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getDate() {
        return date;
    }

    /**
     * Define o valor da propriedade date.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setDate(GLogDateTimeType value) {
        this.date = value;
    }

    /**
     * Obtém o valor da propriedade validationLocation.
     * 
     * @return
     *     possible object is
     *     {@link LocationRefType }
     *     
     */
    public LocationRefType getValidationLocation() {
        return validationLocation;
    }

    /**
     * Define o valor da propriedade validationLocation.
     * 
     * @param value
     *     allowed object is
     *     {@link LocationRefType }
     *     
     */
    public void setValidationLocation(LocationRefType value) {
        this.validationLocation = value;
    }

    /**
     * Obtém o valor da propriedade archiveLocation.
     * 
     * @return
     *     possible object is
     *     {@link LocationRefType }
     *     
     */
    public LocationRefType getArchiveLocation() {
        return archiveLocation;
    }

    /**
     * Define o valor da propriedade archiveLocation.
     * 
     * @param value
     *     allowed object is
     *     {@link LocationRefType }
     *     
     */
    public void setArchiveLocation(LocationRefType value) {
        this.archiveLocation = value;
    }

    /**
     * Obtém o valor da propriedade archiveFormat.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getArchiveFormat() {
        return archiveFormat;
    }

    /**
     * Define o valor da propriedade archiveFormat.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setArchiveFormat(String value) {
        this.archiveFormat = value;
    }

    /**
     * Obtém o valor da propriedade archiveDetail.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getArchiveDetail() {
        return archiveDetail;
    }

    /**
     * Define o valor da propriedade archiveDetail.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setArchiveDetail(String value) {
        this.archiveDetail = value;
    }

}
