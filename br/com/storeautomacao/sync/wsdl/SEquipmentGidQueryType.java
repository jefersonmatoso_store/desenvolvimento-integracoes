
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Used to query for SEquipmentGid to assign the Shipment.ShipUnit.
 * 
 * <p>Classe Java de SEquipmentGidQueryType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="SEquipmentGidQueryType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element name="SEquipGidMatchOption" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="IntSavedQuery" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}IntSavedQueryType"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SEquipmentGidQueryType", propOrder = {
    "sEquipGidMatchOption",
    "intSavedQuery"
})
public class SEquipmentGidQueryType {

    @XmlElement(name = "SEquipGidMatchOption")
    protected String sEquipGidMatchOption;
    @XmlElement(name = "IntSavedQuery")
    protected IntSavedQueryType intSavedQuery;

    /**
     * Obtém o valor da propriedade sEquipGidMatchOption.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSEquipGidMatchOption() {
        return sEquipGidMatchOption;
    }

    /**
     * Define o valor da propriedade sEquipGidMatchOption.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSEquipGidMatchOption(String value) {
        this.sEquipGidMatchOption = value;
    }

    /**
     * Obtém o valor da propriedade intSavedQuery.
     * 
     * @return
     *     possible object is
     *     {@link IntSavedQueryType }
     *     
     */
    public IntSavedQueryType getIntSavedQuery() {
        return intSavedQuery;
    }

    /**
     * Define o valor da propriedade intSavedQuery.
     * 
     * @param value
     *     allowed object is
     *     {@link IntSavedQueryType }
     *     
     */
    public void setIntSavedQuery(IntSavedQueryType value) {
        this.intSavedQuery = value;
    }

}
