
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * ShipmentStopDetail specifies an activity (pickup, delivery, or other),
 *             and the ship unit that is being processed.
 *          
 * 
 * <p>Classe Java de ShipmentStopDetailType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="ShipmentStopDetailType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Activity" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ActivityDuration" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDurationType" minOccurs="0"/>
 *         &lt;element name="ShipUnitGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="IsPermanent" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InvolvedParty" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}InvolvedPartyType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="ShipUnitDwellTime" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDurationType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ShipmentStopDetailType", propOrder = {
    "activity",
    "activityDuration",
    "shipUnitGid",
    "isPermanent",
    "involvedParty",
    "shipUnitDwellTime"
})
public class ShipmentStopDetailType {

    @XmlElement(name = "Activity", required = true)
    protected String activity;
    @XmlElement(name = "ActivityDuration")
    protected GLogXMLDurationType activityDuration;
    @XmlElement(name = "ShipUnitGid", required = true)
    protected GLogXMLGidType shipUnitGid;
    @XmlElement(name = "IsPermanent")
    protected String isPermanent;
    @XmlElement(name = "InvolvedParty")
    protected List<InvolvedPartyType> involvedParty;
    @XmlElement(name = "ShipUnitDwellTime")
    protected GLogXMLDurationType shipUnitDwellTime;

    /**
     * Obtém o valor da propriedade activity.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActivity() {
        return activity;
    }

    /**
     * Define o valor da propriedade activity.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActivity(String value) {
        this.activity = value;
    }

    /**
     * Obtém o valor da propriedade activityDuration.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public GLogXMLDurationType getActivityDuration() {
        return activityDuration;
    }

    /**
     * Define o valor da propriedade activityDuration.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public void setActivityDuration(GLogXMLDurationType value) {
        this.activityDuration = value;
    }

    /**
     * Obtém o valor da propriedade shipUnitGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getShipUnitGid() {
        return shipUnitGid;
    }

    /**
     * Define o valor da propriedade shipUnitGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setShipUnitGid(GLogXMLGidType value) {
        this.shipUnitGid = value;
    }

    /**
     * Obtém o valor da propriedade isPermanent.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsPermanent() {
        return isPermanent;
    }

    /**
     * Define o valor da propriedade isPermanent.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsPermanent(String value) {
        this.isPermanent = value;
    }

    /**
     * Gets the value of the involvedParty property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the involvedParty property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInvolvedParty().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link InvolvedPartyType }
     * 
     * 
     */
    public List<InvolvedPartyType> getInvolvedParty() {
        if (involvedParty == null) {
            involvedParty = new ArrayList<InvolvedPartyType>();
        }
        return this.involvedParty;
    }

    /**
     * Obtém o valor da propriedade shipUnitDwellTime.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public GLogXMLDurationType getShipUnitDwellTime() {
        return shipUnitDwellTime;
    }

    /**
     * Define o valor da propriedade shipUnitDwellTime.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public void setShipUnitDwellTime(GLogXMLDurationType value) {
        this.shipUnitDwellTime = value;
    }

}
