
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Classe Java de FinancialSystemFeedType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="FinancialSystemFeedType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OTMTransactionOut">
 *       &lt;sequence>
 *         &lt;element name="SendReason" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}SendReasonType" minOccurs="0"/>
 *         &lt;element name="TransOrderGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="SellSideFinancials" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FinancialsType" minOccurs="0"/>
 *         &lt;element name="BuySideFinancials" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FinancialsType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FinancialSystemFeedType", propOrder = {
    "sendReason",
    "transOrderGid",
    "sellSideFinancials",
    "buySideFinancials"
})
public class FinancialSystemFeedType
    extends OTMTransactionOut
{

    @XmlElement(name = "SendReason")
    protected SendReasonType sendReason;
    @XmlElement(name = "TransOrderGid")
    protected List<GLogXMLGidType> transOrderGid;
    @XmlElement(name = "SellSideFinancials")
    protected FinancialsType sellSideFinancials;
    @XmlElement(name = "BuySideFinancials")
    protected FinancialsType buySideFinancials;

    /**
     * Obtém o valor da propriedade sendReason.
     * 
     * @return
     *     possible object is
     *     {@link SendReasonType }
     *     
     */
    public SendReasonType getSendReason() {
        return sendReason;
    }

    /**
     * Define o valor da propriedade sendReason.
     * 
     * @param value
     *     allowed object is
     *     {@link SendReasonType }
     *     
     */
    public void setSendReason(SendReasonType value) {
        this.sendReason = value;
    }

    /**
     * Gets the value of the transOrderGid property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the transOrderGid property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTransOrderGid().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GLogXMLGidType }
     * 
     * 
     */
    public List<GLogXMLGidType> getTransOrderGid() {
        if (transOrderGid == null) {
            transOrderGid = new ArrayList<GLogXMLGidType>();
        }
        return this.transOrderGid;
    }

    /**
     * Obtém o valor da propriedade sellSideFinancials.
     * 
     * @return
     *     possible object is
     *     {@link FinancialsType }
     *     
     */
    public FinancialsType getSellSideFinancials() {
        return sellSideFinancials;
    }

    /**
     * Define o valor da propriedade sellSideFinancials.
     * 
     * @param value
     *     allowed object is
     *     {@link FinancialsType }
     *     
     */
    public void setSellSideFinancials(FinancialsType value) {
        this.sellSideFinancials = value;
    }

    /**
     * Obtém o valor da propriedade buySideFinancials.
     * 
     * @return
     *     possible object is
     *     {@link FinancialsType }
     *     
     */
    public FinancialsType getBuySideFinancials() {
        return buySideFinancials;
    }

    /**
     * Define o valor da propriedade buySideFinancials.
     * 
     * @param value
     *     allowed object is
     *     {@link FinancialsType }
     *     
     */
    public void setBuySideFinancials(FinancialsType value) {
        this.buySideFinancials = value;
    }

}
