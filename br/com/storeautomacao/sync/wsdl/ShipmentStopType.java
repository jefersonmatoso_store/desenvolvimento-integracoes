
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;


/**
 * ShipmentStop is a structure representing a stop on a shipment.
 *             Note: The LocationRef element is required on insert of the ShipmentStop.
 *          
 * 
 * <p>Classe Java de ShipmentStopType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="ShipmentStopType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="QueryByExtStopSeq" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="StopSequence" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ExtStopSequence" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IntSavedQuery" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}IntSavedQueryType" minOccurs="0"/>
 *         &lt;element name="TransactionCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransactionCodeType" minOccurs="0"/>
 *         &lt;element name="StopDuration" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDurationType" minOccurs="0"/>
 *         &lt;element name="IsAppointment" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LocationRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LocationRefType" minOccurs="0"/>
 *         &lt;element name="LocationOverrideRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LocationOverrideRefType" minOccurs="0"/>
 *         &lt;element name="ParentLocationRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocRefType" minOccurs="0"/>
 *         &lt;element name="ArbitraryType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LocationRoleGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="DistFromPrevStop" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDistanceType" minOccurs="0"/>
 *         &lt;element name="IsFixedDistance" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="StopReason" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ArrivalTime" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLEventTimeType" minOccurs="0"/>
 *         &lt;element name="DepartureTime" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLEventTimeType" minOccurs="0"/>
 *         &lt;element name="AppointmentPickup" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="AppointmentDelivery" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="IsPermanent" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsDepot" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AccessorialTime" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDurationType" minOccurs="0"/>
 *         &lt;element name="StopRequirementType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RateServiceGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="VoyageGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="RepetitionScheduleGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="RepetitionSchedStopNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FlightInstanceId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsMotherVessel" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RushHourTime" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDurationType" minOccurs="0"/>
 *         &lt;element name="ShipmentStopDetail" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ShipmentStopDetailType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Refnum" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RefnumType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Remark" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RemarkType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="InvolvedParty" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}InvolvedPartyType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Status" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}StatusType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="ShipStopDebrief" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ShipStopDebriefType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Appointment" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}AppointmentType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="StopType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FlexFieldStrings" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldStringType" minOccurs="0"/>
 *         &lt;element name="FlexFieldNumbers" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldNumberType" minOccurs="0"/>
 *         &lt;element name="FlexFieldDates" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldDateType" minOccurs="0"/>
 *         &lt;element name="AppointmentWindowStart" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="AppointmentWindowEnd" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ShipmentStopType", propOrder = {
    "queryByExtStopSeq",
    "stopSequence",
    "extStopSequence",
    "intSavedQuery",
    "transactionCode",
    "stopDuration",
    "isAppointment",
    "locationRef",
    "locationOverrideRef",
    "parentLocationRef",
    "arbitraryType",
    "locationRoleGid",
    "distFromPrevStop",
    "isFixedDistance",
    "stopReason",
    "arrivalTime",
    "departureTime",
    "appointmentPickup",
    "appointmentDelivery",
    "isPermanent",
    "isDepot",
    "accessorialTime",
    "stopRequirementType",
    "rateServiceGid",
    "voyageGid",
    "repetitionScheduleGid",
    "repetitionSchedStopNo",
    "flightInstanceId",
    "isMotherVessel",
    "rushHourTime",
    "shipmentStopDetail",
    "refnum",
    "remark",
    "involvedParty",
    "status",
    "shipStopDebrief",
    "appointment",
    "stopType",
    "flexFieldStrings",
    "flexFieldNumbers",
    "flexFieldDates",
    "appointmentWindowStart",
    "appointmentWindowEnd"
})
public class ShipmentStopType {

    @XmlElement(name = "QueryByExtStopSeq")
    protected String queryByExtStopSeq;
    @XmlElement(name = "StopSequence", required = true)
    protected String stopSequence;
    @XmlElement(name = "ExtStopSequence")
    protected String extStopSequence;
    @XmlElement(name = "IntSavedQuery")
    protected IntSavedQueryType intSavedQuery;
    @XmlElement(name = "TransactionCode")
    @XmlSchemaType(name = "string")
    protected TransactionCodeType transactionCode;
    @XmlElement(name = "StopDuration")
    protected GLogXMLDurationType stopDuration;
    @XmlElement(name = "IsAppointment")
    protected String isAppointment;
    @XmlElement(name = "LocationRef")
    protected LocationRefType locationRef;
    @XmlElement(name = "LocationOverrideRef")
    protected LocationOverrideRefType locationOverrideRef;
    @XmlElement(name = "ParentLocationRef")
    protected GLogXMLLocRefType parentLocationRef;
    @XmlElement(name = "ArbitraryType")
    protected String arbitraryType;
    @XmlElement(name = "LocationRoleGid")
    protected GLogXMLGidType locationRoleGid;
    @XmlElement(name = "DistFromPrevStop")
    protected GLogXMLDistanceType distFromPrevStop;
    @XmlElement(name = "IsFixedDistance")
    protected String isFixedDistance;
    @XmlElement(name = "StopReason")
    protected String stopReason;
    @XmlElement(name = "ArrivalTime")
    protected GLogXMLEventTimeType arrivalTime;
    @XmlElement(name = "DepartureTime")
    protected GLogXMLEventTimeType departureTime;
    @XmlElement(name = "AppointmentPickup")
    protected GLogDateTimeType appointmentPickup;
    @XmlElement(name = "AppointmentDelivery")
    protected GLogDateTimeType appointmentDelivery;
    @XmlElement(name = "IsPermanent")
    protected String isPermanent;
    @XmlElement(name = "IsDepot")
    protected String isDepot;
    @XmlElement(name = "AccessorialTime")
    protected GLogXMLDurationType accessorialTime;
    @XmlElement(name = "StopRequirementType")
    protected String stopRequirementType;
    @XmlElement(name = "RateServiceGid")
    protected GLogXMLGidType rateServiceGid;
    @XmlElement(name = "VoyageGid")
    protected GLogXMLGidType voyageGid;
    @XmlElement(name = "RepetitionScheduleGid")
    protected GLogXMLGidType repetitionScheduleGid;
    @XmlElement(name = "RepetitionSchedStopNo")
    protected String repetitionSchedStopNo;
    @XmlElement(name = "FlightInstanceId")
    protected String flightInstanceId;
    @XmlElement(name = "IsMotherVessel")
    protected String isMotherVessel;
    @XmlElement(name = "RushHourTime")
    protected GLogXMLDurationType rushHourTime;
    @XmlElement(name = "ShipmentStopDetail")
    protected List<ShipmentStopDetailType> shipmentStopDetail;
    @XmlElement(name = "Refnum")
    protected List<RefnumType> refnum;
    @XmlElement(name = "Remark")
    protected List<RemarkType> remark;
    @XmlElement(name = "InvolvedParty")
    protected List<InvolvedPartyType> involvedParty;
    @XmlElement(name = "Status")
    protected List<StatusType> status;
    @XmlElement(name = "ShipStopDebrief")
    protected List<ShipStopDebriefType> shipStopDebrief;
    @XmlElement(name = "Appointment")
    protected List<AppointmentType> appointment;
    @XmlElement(name = "StopType")
    protected String stopType;
    @XmlElement(name = "FlexFieldStrings")
    protected FlexFieldStringType flexFieldStrings;
    @XmlElement(name = "FlexFieldNumbers")
    protected FlexFieldNumberType flexFieldNumbers;
    @XmlElement(name = "FlexFieldDates")
    protected FlexFieldDateType flexFieldDates;
    @XmlElement(name = "AppointmentWindowStart")
    protected GLogDateTimeType appointmentWindowStart;
    @XmlElement(name = "AppointmentWindowEnd")
    protected GLogDateTimeType appointmentWindowEnd;

    /**
     * Obtém o valor da propriedade queryByExtStopSeq.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQueryByExtStopSeq() {
        return queryByExtStopSeq;
    }

    /**
     * Define o valor da propriedade queryByExtStopSeq.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQueryByExtStopSeq(String value) {
        this.queryByExtStopSeq = value;
    }

    /**
     * Obtém o valor da propriedade stopSequence.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStopSequence() {
        return stopSequence;
    }

    /**
     * Define o valor da propriedade stopSequence.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStopSequence(String value) {
        this.stopSequence = value;
    }

    /**
     * Obtém o valor da propriedade extStopSequence.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExtStopSequence() {
        return extStopSequence;
    }

    /**
     * Define o valor da propriedade extStopSequence.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExtStopSequence(String value) {
        this.extStopSequence = value;
    }

    /**
     * Obtém o valor da propriedade intSavedQuery.
     * 
     * @return
     *     possible object is
     *     {@link IntSavedQueryType }
     *     
     */
    public IntSavedQueryType getIntSavedQuery() {
        return intSavedQuery;
    }

    /**
     * Define o valor da propriedade intSavedQuery.
     * 
     * @param value
     *     allowed object is
     *     {@link IntSavedQueryType }
     *     
     */
    public void setIntSavedQuery(IntSavedQueryType value) {
        this.intSavedQuery = value;
    }

    /**
     * Obtém o valor da propriedade transactionCode.
     * 
     * @return
     *     possible object is
     *     {@link TransactionCodeType }
     *     
     */
    public TransactionCodeType getTransactionCode() {
        return transactionCode;
    }

    /**
     * Define o valor da propriedade transactionCode.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionCodeType }
     *     
     */
    public void setTransactionCode(TransactionCodeType value) {
        this.transactionCode = value;
    }

    /**
     * Obtém o valor da propriedade stopDuration.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public GLogXMLDurationType getStopDuration() {
        return stopDuration;
    }

    /**
     * Define o valor da propriedade stopDuration.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public void setStopDuration(GLogXMLDurationType value) {
        this.stopDuration = value;
    }

    /**
     * Obtém o valor da propriedade isAppointment.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsAppointment() {
        return isAppointment;
    }

    /**
     * Define o valor da propriedade isAppointment.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsAppointment(String value) {
        this.isAppointment = value;
    }

    /**
     * Obtém o valor da propriedade locationRef.
     * 
     * @return
     *     possible object is
     *     {@link LocationRefType }
     *     
     */
    public LocationRefType getLocationRef() {
        return locationRef;
    }

    /**
     * Define o valor da propriedade locationRef.
     * 
     * @param value
     *     allowed object is
     *     {@link LocationRefType }
     *     
     */
    public void setLocationRef(LocationRefType value) {
        this.locationRef = value;
    }

    /**
     * Obtém o valor da propriedade locationOverrideRef.
     * 
     * @return
     *     possible object is
     *     {@link LocationOverrideRefType }
     *     
     */
    public LocationOverrideRefType getLocationOverrideRef() {
        return locationOverrideRef;
    }

    /**
     * Define o valor da propriedade locationOverrideRef.
     * 
     * @param value
     *     allowed object is
     *     {@link LocationOverrideRefType }
     *     
     */
    public void setLocationOverrideRef(LocationOverrideRefType value) {
        this.locationOverrideRef = value;
    }

    /**
     * Obtém o valor da propriedade parentLocationRef.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public GLogXMLLocRefType getParentLocationRef() {
        return parentLocationRef;
    }

    /**
     * Define o valor da propriedade parentLocationRef.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public void setParentLocationRef(GLogXMLLocRefType value) {
        this.parentLocationRef = value;
    }

    /**
     * Obtém o valor da propriedade arbitraryType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getArbitraryType() {
        return arbitraryType;
    }

    /**
     * Define o valor da propriedade arbitraryType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setArbitraryType(String value) {
        this.arbitraryType = value;
    }

    /**
     * Obtém o valor da propriedade locationRoleGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getLocationRoleGid() {
        return locationRoleGid;
    }

    /**
     * Define o valor da propriedade locationRoleGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setLocationRoleGid(GLogXMLGidType value) {
        this.locationRoleGid = value;
    }

    /**
     * Obtém o valor da propriedade distFromPrevStop.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDistanceType }
     *     
     */
    public GLogXMLDistanceType getDistFromPrevStop() {
        return distFromPrevStop;
    }

    /**
     * Define o valor da propriedade distFromPrevStop.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDistanceType }
     *     
     */
    public void setDistFromPrevStop(GLogXMLDistanceType value) {
        this.distFromPrevStop = value;
    }

    /**
     * Obtém o valor da propriedade isFixedDistance.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsFixedDistance() {
        return isFixedDistance;
    }

    /**
     * Define o valor da propriedade isFixedDistance.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsFixedDistance(String value) {
        this.isFixedDistance = value;
    }

    /**
     * Obtém o valor da propriedade stopReason.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStopReason() {
        return stopReason;
    }

    /**
     * Define o valor da propriedade stopReason.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStopReason(String value) {
        this.stopReason = value;
    }

    /**
     * Obtém o valor da propriedade arrivalTime.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLEventTimeType }
     *     
     */
    public GLogXMLEventTimeType getArrivalTime() {
        return arrivalTime;
    }

    /**
     * Define o valor da propriedade arrivalTime.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLEventTimeType }
     *     
     */
    public void setArrivalTime(GLogXMLEventTimeType value) {
        this.arrivalTime = value;
    }

    /**
     * Obtém o valor da propriedade departureTime.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLEventTimeType }
     *     
     */
    public GLogXMLEventTimeType getDepartureTime() {
        return departureTime;
    }

    /**
     * Define o valor da propriedade departureTime.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLEventTimeType }
     *     
     */
    public void setDepartureTime(GLogXMLEventTimeType value) {
        this.departureTime = value;
    }

    /**
     * Obtém o valor da propriedade appointmentPickup.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getAppointmentPickup() {
        return appointmentPickup;
    }

    /**
     * Define o valor da propriedade appointmentPickup.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setAppointmentPickup(GLogDateTimeType value) {
        this.appointmentPickup = value;
    }

    /**
     * Obtém o valor da propriedade appointmentDelivery.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getAppointmentDelivery() {
        return appointmentDelivery;
    }

    /**
     * Define o valor da propriedade appointmentDelivery.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setAppointmentDelivery(GLogDateTimeType value) {
        this.appointmentDelivery = value;
    }

    /**
     * Obtém o valor da propriedade isPermanent.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsPermanent() {
        return isPermanent;
    }

    /**
     * Define o valor da propriedade isPermanent.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsPermanent(String value) {
        this.isPermanent = value;
    }

    /**
     * Obtém o valor da propriedade isDepot.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsDepot() {
        return isDepot;
    }

    /**
     * Define o valor da propriedade isDepot.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsDepot(String value) {
        this.isDepot = value;
    }

    /**
     * Obtém o valor da propriedade accessorialTime.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public GLogXMLDurationType getAccessorialTime() {
        return accessorialTime;
    }

    /**
     * Define o valor da propriedade accessorialTime.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public void setAccessorialTime(GLogXMLDurationType value) {
        this.accessorialTime = value;
    }

    /**
     * Obtém o valor da propriedade stopRequirementType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStopRequirementType() {
        return stopRequirementType;
    }

    /**
     * Define o valor da propriedade stopRequirementType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStopRequirementType(String value) {
        this.stopRequirementType = value;
    }

    /**
     * Obtém o valor da propriedade rateServiceGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRateServiceGid() {
        return rateServiceGid;
    }

    /**
     * Define o valor da propriedade rateServiceGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRateServiceGid(GLogXMLGidType value) {
        this.rateServiceGid = value;
    }

    /**
     * Obtém o valor da propriedade voyageGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getVoyageGid() {
        return voyageGid;
    }

    /**
     * Define o valor da propriedade voyageGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setVoyageGid(GLogXMLGidType value) {
        this.voyageGid = value;
    }

    /**
     * Obtém o valor da propriedade repetitionScheduleGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRepetitionScheduleGid() {
        return repetitionScheduleGid;
    }

    /**
     * Define o valor da propriedade repetitionScheduleGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRepetitionScheduleGid(GLogXMLGidType value) {
        this.repetitionScheduleGid = value;
    }

    /**
     * Obtém o valor da propriedade repetitionSchedStopNo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRepetitionSchedStopNo() {
        return repetitionSchedStopNo;
    }

    /**
     * Define o valor da propriedade repetitionSchedStopNo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRepetitionSchedStopNo(String value) {
        this.repetitionSchedStopNo = value;
    }

    /**
     * Obtém o valor da propriedade flightInstanceId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlightInstanceId() {
        return flightInstanceId;
    }

    /**
     * Define o valor da propriedade flightInstanceId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlightInstanceId(String value) {
        this.flightInstanceId = value;
    }

    /**
     * Obtém o valor da propriedade isMotherVessel.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsMotherVessel() {
        return isMotherVessel;
    }

    /**
     * Define o valor da propriedade isMotherVessel.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsMotherVessel(String value) {
        this.isMotherVessel = value;
    }

    /**
     * Obtém o valor da propriedade rushHourTime.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public GLogXMLDurationType getRushHourTime() {
        return rushHourTime;
    }

    /**
     * Define o valor da propriedade rushHourTime.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public void setRushHourTime(GLogXMLDurationType value) {
        this.rushHourTime = value;
    }

    /**
     * Gets the value of the shipmentStopDetail property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the shipmentStopDetail property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getShipmentStopDetail().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ShipmentStopDetailType }
     * 
     * 
     */
    public List<ShipmentStopDetailType> getShipmentStopDetail() {
        if (shipmentStopDetail == null) {
            shipmentStopDetail = new ArrayList<ShipmentStopDetailType>();
        }
        return this.shipmentStopDetail;
    }

    /**
     * Gets the value of the refnum property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the refnum property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRefnum().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RefnumType }
     * 
     * 
     */
    public List<RefnumType> getRefnum() {
        if (refnum == null) {
            refnum = new ArrayList<RefnumType>();
        }
        return this.refnum;
    }

    /**
     * Gets the value of the remark property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the remark property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRemark().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RemarkType }
     * 
     * 
     */
    public List<RemarkType> getRemark() {
        if (remark == null) {
            remark = new ArrayList<RemarkType>();
        }
        return this.remark;
    }

    /**
     * Gets the value of the involvedParty property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the involvedParty property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInvolvedParty().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link InvolvedPartyType }
     * 
     * 
     */
    public List<InvolvedPartyType> getInvolvedParty() {
        if (involvedParty == null) {
            involvedParty = new ArrayList<InvolvedPartyType>();
        }
        return this.involvedParty;
    }

    /**
     * Gets the value of the status property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the status property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getStatus().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link StatusType }
     * 
     * 
     */
    public List<StatusType> getStatus() {
        if (status == null) {
            status = new ArrayList<StatusType>();
        }
        return this.status;
    }

    /**
     * Gets the value of the shipStopDebrief property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the shipStopDebrief property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getShipStopDebrief().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ShipStopDebriefType }
     * 
     * 
     */
    public List<ShipStopDebriefType> getShipStopDebrief() {
        if (shipStopDebrief == null) {
            shipStopDebrief = new ArrayList<ShipStopDebriefType>();
        }
        return this.shipStopDebrief;
    }

    /**
     * Gets the value of the appointment property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the appointment property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAppointment().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AppointmentType }
     * 
     * 
     */
    public List<AppointmentType> getAppointment() {
        if (appointment == null) {
            appointment = new ArrayList<AppointmentType>();
        }
        return this.appointment;
    }

    /**
     * Obtém o valor da propriedade stopType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStopType() {
        return stopType;
    }

    /**
     * Define o valor da propriedade stopType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStopType(String value) {
        this.stopType = value;
    }

    /**
     * Obtém o valor da propriedade flexFieldStrings.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldStringType }
     *     
     */
    public FlexFieldStringType getFlexFieldStrings() {
        return flexFieldStrings;
    }

    /**
     * Define o valor da propriedade flexFieldStrings.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldStringType }
     *     
     */
    public void setFlexFieldStrings(FlexFieldStringType value) {
        this.flexFieldStrings = value;
    }

    /**
     * Obtém o valor da propriedade flexFieldNumbers.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldNumberType }
     *     
     */
    public FlexFieldNumberType getFlexFieldNumbers() {
        return flexFieldNumbers;
    }

    /**
     * Define o valor da propriedade flexFieldNumbers.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldNumberType }
     *     
     */
    public void setFlexFieldNumbers(FlexFieldNumberType value) {
        this.flexFieldNumbers = value;
    }

    /**
     * Obtém o valor da propriedade flexFieldDates.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldDateType }
     *     
     */
    public FlexFieldDateType getFlexFieldDates() {
        return flexFieldDates;
    }

    /**
     * Define o valor da propriedade flexFieldDates.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldDateType }
     *     
     */
    public void setFlexFieldDates(FlexFieldDateType value) {
        this.flexFieldDates = value;
    }

    /**
     * Obtém o valor da propriedade appointmentWindowStart.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getAppointmentWindowStart() {
        return appointmentWindowStart;
    }

    /**
     * Define o valor da propriedade appointmentWindowStart.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setAppointmentWindowStart(GLogDateTimeType value) {
        this.appointmentWindowStart = value;
    }

    /**
     * Obtém o valor da propriedade appointmentWindowEnd.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getAppointmentWindowEnd() {
        return appointmentWindowEnd;
    }

    /**
     * Define o valor da propriedade appointmentWindowEnd.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setAppointmentWindowEnd(GLogDateTimeType value) {
        this.appointmentWindowEnd = value;
    }

}
