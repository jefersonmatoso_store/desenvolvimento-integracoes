
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * FreightRate specifies rate and charges detail relative to a line item.
 * 
 * <p>Classe Java de FreightRateType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="FreightRateType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FreightRateQualifier" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FreightRateValue" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PaymentMethodCodeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="FreightCharge" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/>
 *         &lt;element name="PrepaidAmount" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/>
 *         &lt;element name="SpecialCharge" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}SpecialChargeType" minOccurs="0"/>
 *         &lt;element name="FP_DeclaredValue" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}DeclaredValueType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FreightRateType", propOrder = {
    "freightRateQualifier",
    "freightRateValue",
    "paymentMethodCodeGid",
    "freightCharge",
    "prepaidAmount",
    "specialCharge",
    "fpDeclaredValue"
})
public class FreightRateType {

    @XmlElement(name = "FreightRateQualifier")
    protected String freightRateQualifier;
    @XmlElement(name = "FreightRateValue")
    protected String freightRateValue;
    @XmlElement(name = "PaymentMethodCodeGid")
    protected GLogXMLGidType paymentMethodCodeGid;
    @XmlElement(name = "FreightCharge")
    protected GLogXMLFinancialAmountType freightCharge;
    @XmlElement(name = "PrepaidAmount")
    protected GLogXMLFinancialAmountType prepaidAmount;
    @XmlElement(name = "SpecialCharge")
    protected SpecialChargeType specialCharge;
    @XmlElement(name = "FP_DeclaredValue")
    protected DeclaredValueType fpDeclaredValue;

    /**
     * Obtém o valor da propriedade freightRateQualifier.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFreightRateQualifier() {
        return freightRateQualifier;
    }

    /**
     * Define o valor da propriedade freightRateQualifier.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFreightRateQualifier(String value) {
        this.freightRateQualifier = value;
    }

    /**
     * Obtém o valor da propriedade freightRateValue.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFreightRateValue() {
        return freightRateValue;
    }

    /**
     * Define o valor da propriedade freightRateValue.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFreightRateValue(String value) {
        this.freightRateValue = value;
    }

    /**
     * Obtém o valor da propriedade paymentMethodCodeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getPaymentMethodCodeGid() {
        return paymentMethodCodeGid;
    }

    /**
     * Define o valor da propriedade paymentMethodCodeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setPaymentMethodCodeGid(GLogXMLGidType value) {
        this.paymentMethodCodeGid = value;
    }

    /**
     * Obtém o valor da propriedade freightCharge.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getFreightCharge() {
        return freightCharge;
    }

    /**
     * Define o valor da propriedade freightCharge.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setFreightCharge(GLogXMLFinancialAmountType value) {
        this.freightCharge = value;
    }

    /**
     * Obtém o valor da propriedade prepaidAmount.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getPrepaidAmount() {
        return prepaidAmount;
    }

    /**
     * Define o valor da propriedade prepaidAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setPrepaidAmount(GLogXMLFinancialAmountType value) {
        this.prepaidAmount = value;
    }

    /**
     * Obtém o valor da propriedade specialCharge.
     * 
     * @return
     *     possible object is
     *     {@link SpecialChargeType }
     *     
     */
    public SpecialChargeType getSpecialCharge() {
        return specialCharge;
    }

    /**
     * Define o valor da propriedade specialCharge.
     * 
     * @param value
     *     allowed object is
     *     {@link SpecialChargeType }
     *     
     */
    public void setSpecialCharge(SpecialChargeType value) {
        this.specialCharge = value;
    }

    /**
     * Obtém o valor da propriedade fpDeclaredValue.
     * 
     * @return
     *     possible object is
     *     {@link DeclaredValueType }
     *     
     */
    public DeclaredValueType getFPDeclaredValue() {
        return fpDeclaredValue;
    }

    /**
     * Define o valor da propriedade fpDeclaredValue.
     * 
     * @param value
     *     allowed object is
     *     {@link DeclaredValueType }
     *     
     */
    public void setFPDeclaredValue(DeclaredValueType value) {
        this.fpDeclaredValue = value;
    }

}
