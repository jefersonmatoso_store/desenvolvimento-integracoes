
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 *             The QuoteHdr contains general information about the quote.
 *          
 * 
 * <p>Classe Java de QuoteHdrType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="QuoteHdrType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RequestDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="IssueDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="ExpireDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="OriginLocation" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}QuoteLocationType"/>
 *         &lt;element name="DestLocation" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}QuoteLocationType"/>
 *         &lt;element name="IsHazardous" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Perspective" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="QuoteOption" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TransportModeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="ServiceProviderGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="AvailableDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="DeliveryByDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="OriginSearchValue" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DestSearchValue" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsCustomerRatesOnly" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ConsolidationTypeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="IncoTermGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="FlexFieldStrings" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldStringType" minOccurs="0"/>
 *         &lt;element name="FlexFieldNumbers" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldNumberType" minOccurs="0"/>
 *         &lt;element name="FlexFieldDates" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldDateType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QuoteHdrType", propOrder = {
    "requestDt",
    "issueDt",
    "expireDt",
    "originLocation",
    "destLocation",
    "isHazardous",
    "perspective",
    "quoteOption",
    "transportModeGid",
    "serviceProviderGid",
    "availableDt",
    "deliveryByDt",
    "originSearchValue",
    "destSearchValue",
    "isCustomerRatesOnly",
    "consolidationTypeGid",
    "incoTermGid",
    "flexFieldStrings",
    "flexFieldNumbers",
    "flexFieldDates"
})
public class QuoteHdrType {

    @XmlElement(name = "RequestDt")
    protected GLogDateTimeType requestDt;
    @XmlElement(name = "IssueDt")
    protected GLogDateTimeType issueDt;
    @XmlElement(name = "ExpireDt")
    protected GLogDateTimeType expireDt;
    @XmlElement(name = "OriginLocation", required = true)
    protected QuoteLocationType originLocation;
    @XmlElement(name = "DestLocation", required = true)
    protected QuoteLocationType destLocation;
    @XmlElement(name = "IsHazardous")
    protected String isHazardous;
    @XmlElement(name = "Perspective")
    protected String perspective;
    @XmlElement(name = "QuoteOption")
    protected String quoteOption;
    @XmlElement(name = "TransportModeGid")
    protected GLogXMLGidType transportModeGid;
    @XmlElement(name = "ServiceProviderGid")
    protected GLogXMLGidType serviceProviderGid;
    @XmlElement(name = "AvailableDt")
    protected GLogDateTimeType availableDt;
    @XmlElement(name = "DeliveryByDt")
    protected GLogDateTimeType deliveryByDt;
    @XmlElement(name = "OriginSearchValue")
    protected String originSearchValue;
    @XmlElement(name = "DestSearchValue")
    protected String destSearchValue;
    @XmlElement(name = "IsCustomerRatesOnly")
    protected String isCustomerRatesOnly;
    @XmlElement(name = "ConsolidationTypeGid")
    protected GLogXMLGidType consolidationTypeGid;
    @XmlElement(name = "IncoTermGid")
    protected GLogXMLGidType incoTermGid;
    @XmlElement(name = "FlexFieldStrings")
    protected FlexFieldStringType flexFieldStrings;
    @XmlElement(name = "FlexFieldNumbers")
    protected FlexFieldNumberType flexFieldNumbers;
    @XmlElement(name = "FlexFieldDates")
    protected FlexFieldDateType flexFieldDates;

    /**
     * Obtém o valor da propriedade requestDt.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getRequestDt() {
        return requestDt;
    }

    /**
     * Define o valor da propriedade requestDt.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setRequestDt(GLogDateTimeType value) {
        this.requestDt = value;
    }

    /**
     * Obtém o valor da propriedade issueDt.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getIssueDt() {
        return issueDt;
    }

    /**
     * Define o valor da propriedade issueDt.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setIssueDt(GLogDateTimeType value) {
        this.issueDt = value;
    }

    /**
     * Obtém o valor da propriedade expireDt.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getExpireDt() {
        return expireDt;
    }

    /**
     * Define o valor da propriedade expireDt.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setExpireDt(GLogDateTimeType value) {
        this.expireDt = value;
    }

    /**
     * Obtém o valor da propriedade originLocation.
     * 
     * @return
     *     possible object is
     *     {@link QuoteLocationType }
     *     
     */
    public QuoteLocationType getOriginLocation() {
        return originLocation;
    }

    /**
     * Define o valor da propriedade originLocation.
     * 
     * @param value
     *     allowed object is
     *     {@link QuoteLocationType }
     *     
     */
    public void setOriginLocation(QuoteLocationType value) {
        this.originLocation = value;
    }

    /**
     * Obtém o valor da propriedade destLocation.
     * 
     * @return
     *     possible object is
     *     {@link QuoteLocationType }
     *     
     */
    public QuoteLocationType getDestLocation() {
        return destLocation;
    }

    /**
     * Define o valor da propriedade destLocation.
     * 
     * @param value
     *     allowed object is
     *     {@link QuoteLocationType }
     *     
     */
    public void setDestLocation(QuoteLocationType value) {
        this.destLocation = value;
    }

    /**
     * Obtém o valor da propriedade isHazardous.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsHazardous() {
        return isHazardous;
    }

    /**
     * Define o valor da propriedade isHazardous.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsHazardous(String value) {
        this.isHazardous = value;
    }

    /**
     * Obtém o valor da propriedade perspective.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPerspective() {
        return perspective;
    }

    /**
     * Define o valor da propriedade perspective.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPerspective(String value) {
        this.perspective = value;
    }

    /**
     * Obtém o valor da propriedade quoteOption.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQuoteOption() {
        return quoteOption;
    }

    /**
     * Define o valor da propriedade quoteOption.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQuoteOption(String value) {
        this.quoteOption = value;
    }

    /**
     * Obtém o valor da propriedade transportModeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getTransportModeGid() {
        return transportModeGid;
    }

    /**
     * Define o valor da propriedade transportModeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setTransportModeGid(GLogXMLGidType value) {
        this.transportModeGid = value;
    }

    /**
     * Obtém o valor da propriedade serviceProviderGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getServiceProviderGid() {
        return serviceProviderGid;
    }

    /**
     * Define o valor da propriedade serviceProviderGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setServiceProviderGid(GLogXMLGidType value) {
        this.serviceProviderGid = value;
    }

    /**
     * Obtém o valor da propriedade availableDt.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getAvailableDt() {
        return availableDt;
    }

    /**
     * Define o valor da propriedade availableDt.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setAvailableDt(GLogDateTimeType value) {
        this.availableDt = value;
    }

    /**
     * Obtém o valor da propriedade deliveryByDt.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getDeliveryByDt() {
        return deliveryByDt;
    }

    /**
     * Define o valor da propriedade deliveryByDt.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setDeliveryByDt(GLogDateTimeType value) {
        this.deliveryByDt = value;
    }

    /**
     * Obtém o valor da propriedade originSearchValue.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOriginSearchValue() {
        return originSearchValue;
    }

    /**
     * Define o valor da propriedade originSearchValue.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOriginSearchValue(String value) {
        this.originSearchValue = value;
    }

    /**
     * Obtém o valor da propriedade destSearchValue.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDestSearchValue() {
        return destSearchValue;
    }

    /**
     * Define o valor da propriedade destSearchValue.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDestSearchValue(String value) {
        this.destSearchValue = value;
    }

    /**
     * Obtém o valor da propriedade isCustomerRatesOnly.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsCustomerRatesOnly() {
        return isCustomerRatesOnly;
    }

    /**
     * Define o valor da propriedade isCustomerRatesOnly.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsCustomerRatesOnly(String value) {
        this.isCustomerRatesOnly = value;
    }

    /**
     * Obtém o valor da propriedade consolidationTypeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getConsolidationTypeGid() {
        return consolidationTypeGid;
    }

    /**
     * Define o valor da propriedade consolidationTypeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setConsolidationTypeGid(GLogXMLGidType value) {
        this.consolidationTypeGid = value;
    }

    /**
     * Obtém o valor da propriedade incoTermGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getIncoTermGid() {
        return incoTermGid;
    }

    /**
     * Define o valor da propriedade incoTermGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setIncoTermGid(GLogXMLGidType value) {
        this.incoTermGid = value;
    }

    /**
     * Obtém o valor da propriedade flexFieldStrings.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldStringType }
     *     
     */
    public FlexFieldStringType getFlexFieldStrings() {
        return flexFieldStrings;
    }

    /**
     * Define o valor da propriedade flexFieldStrings.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldStringType }
     *     
     */
    public void setFlexFieldStrings(FlexFieldStringType value) {
        this.flexFieldStrings = value;
    }

    /**
     * Obtém o valor da propriedade flexFieldNumbers.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldNumberType }
     *     
     */
    public FlexFieldNumberType getFlexFieldNumbers() {
        return flexFieldNumbers;
    }

    /**
     * Define o valor da propriedade flexFieldNumbers.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldNumberType }
     *     
     */
    public void setFlexFieldNumbers(FlexFieldNumberType value) {
        this.flexFieldNumbers = value;
    }

    /**
     * Obtém o valor da propriedade flexFieldDates.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldDateType }
     *     
     */
    public FlexFieldDateType getFlexFieldDates() {
        return flexFieldDates;
    }

    /**
     * Define o valor da propriedade flexFieldDates.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldDateType }
     *     
     */
    public void setFlexFieldDates(FlexFieldDateType value) {
        this.flexFieldDates = value;
    }

}
