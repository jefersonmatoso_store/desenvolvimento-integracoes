
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * TransmissionHeader contains header level information for a Transmission, such as Transmission
 *             Type, Sender Reference ID.
 * 
 *             Deprecated 6.4.2: UserName, Password. See Integration Guide for full details.
 *          
 * 
 * <p>Classe Java de TransmissionHeaderType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="TransmissionHeaderType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Version" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TransmissionType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="QueryReplyFormat" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="StagingInfo" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}StagingInfoType" minOccurs="0"/>
 *         &lt;element name="TransmissionCreateDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="TransactionCount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SenderHostName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ReceiverHostName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SenderSystemID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="UserName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Password" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}PasswordType" minOccurs="0"/>
 *         &lt;element name="SenderTransmissionNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ReferenceTransmissionNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SuppressTransmissionAck" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AckSpec" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}AckSpecType" minOccurs="0"/>
 *         &lt;element name="IsProcessInSequence" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="StopProcessOnError" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="GLogXMLElementName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ProcessGrouping" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ProcessGroupingType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="NotifyInfo" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}NotifyInfoType" minOccurs="0"/>
 *         &lt;element name="Refnum" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RefnumType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="DataQueuePriority" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DataQueueGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TransmissionHeaderType", propOrder = {
    "version",
    "transmissionType",
    "queryReplyFormat",
    "stagingInfo",
    "transmissionCreateDt",
    "transactionCount",
    "senderHostName",
    "receiverHostName",
    "senderSystemID",
    "userName",
    "password",
    "senderTransmissionNo",
    "referenceTransmissionNo",
    "suppressTransmissionAck",
    "ackSpec",
    "isProcessInSequence",
    "stopProcessOnError",
    "gLogXMLElementName",
    "processGrouping",
    "notifyInfo",
    "refnum",
    "dataQueuePriority",
    "dataQueueGid"
})
public class TransmissionHeaderType {

    @XmlElement(name = "Version")
    protected String version;
    @XmlElement(name = "TransmissionType")
    protected String transmissionType;
    @XmlElement(name = "QueryReplyFormat")
    protected String queryReplyFormat;
    @XmlElement(name = "StagingInfo")
    protected StagingInfoType stagingInfo;
    @XmlElement(name = "TransmissionCreateDt")
    protected GLogDateTimeType transmissionCreateDt;
    @XmlElement(name = "TransactionCount")
    protected String transactionCount;
    @XmlElement(name = "SenderHostName")
    protected String senderHostName;
    @XmlElement(name = "ReceiverHostName")
    protected String receiverHostName;
    @XmlElement(name = "SenderSystemID")
    protected String senderSystemID;
    @XmlElement(name = "UserName")
    protected String userName;
    @XmlElement(name = "Password")
    protected PasswordType password;
    @XmlElement(name = "SenderTransmissionNo")
    protected String senderTransmissionNo;
    @XmlElement(name = "ReferenceTransmissionNo")
    protected String referenceTransmissionNo;
    @XmlElement(name = "SuppressTransmissionAck")
    protected String suppressTransmissionAck;
    @XmlElement(name = "AckSpec")
    protected AckSpecType ackSpec;
    @XmlElement(name = "IsProcessInSequence")
    protected String isProcessInSequence;
    @XmlElement(name = "StopProcessOnError")
    protected String stopProcessOnError;
    @XmlElement(name = "GLogXMLElementName")
    protected String gLogXMLElementName;
    @XmlElement(name = "ProcessGrouping")
    protected List<ProcessGroupingType> processGrouping;
    @XmlElement(name = "NotifyInfo")
    protected NotifyInfoType notifyInfo;
    @XmlElement(name = "Refnum")
    protected List<RefnumType> refnum;
    @XmlElement(name = "DataQueuePriority")
    protected String dataQueuePriority;
    @XmlElement(name = "DataQueueGid")
    protected GLogXMLGidType dataQueueGid;

    /**
     * Obtém o valor da propriedade version.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVersion() {
        return version;
    }

    /**
     * Define o valor da propriedade version.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVersion(String value) {
        this.version = value;
    }

    /**
     * Obtém o valor da propriedade transmissionType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransmissionType() {
        return transmissionType;
    }

    /**
     * Define o valor da propriedade transmissionType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransmissionType(String value) {
        this.transmissionType = value;
    }

    /**
     * Obtém o valor da propriedade queryReplyFormat.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQueryReplyFormat() {
        return queryReplyFormat;
    }

    /**
     * Define o valor da propriedade queryReplyFormat.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQueryReplyFormat(String value) {
        this.queryReplyFormat = value;
    }

    /**
     * Obtém o valor da propriedade stagingInfo.
     * 
     * @return
     *     possible object is
     *     {@link StagingInfoType }
     *     
     */
    public StagingInfoType getStagingInfo() {
        return stagingInfo;
    }

    /**
     * Define o valor da propriedade stagingInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link StagingInfoType }
     *     
     */
    public void setStagingInfo(StagingInfoType value) {
        this.stagingInfo = value;
    }

    /**
     * Obtém o valor da propriedade transmissionCreateDt.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getTransmissionCreateDt() {
        return transmissionCreateDt;
    }

    /**
     * Define o valor da propriedade transmissionCreateDt.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setTransmissionCreateDt(GLogDateTimeType value) {
        this.transmissionCreateDt = value;
    }

    /**
     * Obtém o valor da propriedade transactionCount.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionCount() {
        return transactionCount;
    }

    /**
     * Define o valor da propriedade transactionCount.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionCount(String value) {
        this.transactionCount = value;
    }

    /**
     * Obtém o valor da propriedade senderHostName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSenderHostName() {
        return senderHostName;
    }

    /**
     * Define o valor da propriedade senderHostName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSenderHostName(String value) {
        this.senderHostName = value;
    }

    /**
     * Obtém o valor da propriedade receiverHostName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReceiverHostName() {
        return receiverHostName;
    }

    /**
     * Define o valor da propriedade receiverHostName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReceiverHostName(String value) {
        this.receiverHostName = value;
    }

    /**
     * Obtém o valor da propriedade senderSystemID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSenderSystemID() {
        return senderSystemID;
    }

    /**
     * Define o valor da propriedade senderSystemID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSenderSystemID(String value) {
        this.senderSystemID = value;
    }

    /**
     * Obtém o valor da propriedade userName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserName() {
        return userName;
    }

    /**
     * Define o valor da propriedade userName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserName(String value) {
        this.userName = value;
    }

    /**
     * Obtém o valor da propriedade password.
     * 
     * @return
     *     possible object is
     *     {@link PasswordType }
     *     
     */
    public PasswordType getPassword() {
        return password;
    }

    /**
     * Define o valor da propriedade password.
     * 
     * @param value
     *     allowed object is
     *     {@link PasswordType }
     *     
     */
    public void setPassword(PasswordType value) {
        this.password = value;
    }

    /**
     * Obtém o valor da propriedade senderTransmissionNo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSenderTransmissionNo() {
        return senderTransmissionNo;
    }

    /**
     * Define o valor da propriedade senderTransmissionNo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSenderTransmissionNo(String value) {
        this.senderTransmissionNo = value;
    }

    /**
     * Obtém o valor da propriedade referenceTransmissionNo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReferenceTransmissionNo() {
        return referenceTransmissionNo;
    }

    /**
     * Define o valor da propriedade referenceTransmissionNo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReferenceTransmissionNo(String value) {
        this.referenceTransmissionNo = value;
    }

    /**
     * Obtém o valor da propriedade suppressTransmissionAck.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSuppressTransmissionAck() {
        return suppressTransmissionAck;
    }

    /**
     * Define o valor da propriedade suppressTransmissionAck.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSuppressTransmissionAck(String value) {
        this.suppressTransmissionAck = value;
    }

    /**
     * Obtém o valor da propriedade ackSpec.
     * 
     * @return
     *     possible object is
     *     {@link AckSpecType }
     *     
     */
    public AckSpecType getAckSpec() {
        return ackSpec;
    }

    /**
     * Define o valor da propriedade ackSpec.
     * 
     * @param value
     *     allowed object is
     *     {@link AckSpecType }
     *     
     */
    public void setAckSpec(AckSpecType value) {
        this.ackSpec = value;
    }

    /**
     * Obtém o valor da propriedade isProcessInSequence.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsProcessInSequence() {
        return isProcessInSequence;
    }

    /**
     * Define o valor da propriedade isProcessInSequence.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsProcessInSequence(String value) {
        this.isProcessInSequence = value;
    }

    /**
     * Obtém o valor da propriedade stopProcessOnError.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStopProcessOnError() {
        return stopProcessOnError;
    }

    /**
     * Define o valor da propriedade stopProcessOnError.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStopProcessOnError(String value) {
        this.stopProcessOnError = value;
    }

    /**
     * Obtém o valor da propriedade gLogXMLElementName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGLogXMLElementName() {
        return gLogXMLElementName;
    }

    /**
     * Define o valor da propriedade gLogXMLElementName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGLogXMLElementName(String value) {
        this.gLogXMLElementName = value;
    }

    /**
     * Gets the value of the processGrouping property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the processGrouping property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getProcessGrouping().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProcessGroupingType }
     * 
     * 
     */
    public List<ProcessGroupingType> getProcessGrouping() {
        if (processGrouping == null) {
            processGrouping = new ArrayList<ProcessGroupingType>();
        }
        return this.processGrouping;
    }

    /**
     * Obtém o valor da propriedade notifyInfo.
     * 
     * @return
     *     possible object is
     *     {@link NotifyInfoType }
     *     
     */
    public NotifyInfoType getNotifyInfo() {
        return notifyInfo;
    }

    /**
     * Define o valor da propriedade notifyInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link NotifyInfoType }
     *     
     */
    public void setNotifyInfo(NotifyInfoType value) {
        this.notifyInfo = value;
    }

    /**
     * Gets the value of the refnum property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the refnum property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRefnum().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RefnumType }
     * 
     * 
     */
    public List<RefnumType> getRefnum() {
        if (refnum == null) {
            refnum = new ArrayList<RefnumType>();
        }
        return this.refnum;
    }

    /**
     * Obtém o valor da propriedade dataQueuePriority.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataQueuePriority() {
        return dataQueuePriority;
    }

    /**
     * Define o valor da propriedade dataQueuePriority.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataQueuePriority(String value) {
        this.dataQueuePriority = value;
    }

    /**
     * Obtém o valor da propriedade dataQueueGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getDataQueueGid() {
        return dataQueueGid;
    }

    /**
     * Define o valor da propriedade dataQueueGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setDataQueueGid(GLogXMLGidType value) {
        this.dataQueueGid = value;
    }

}
