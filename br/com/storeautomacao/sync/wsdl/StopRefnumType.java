
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * StopRefnum is a qualifier/value pair used to refer to a particular stop.
 * 
 * <p>Classe Java de StopRefnumType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="StopRefnumType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="StopRefnumQualifierGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="StopRefnumValue" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "StopRefnumType", propOrder = {
    "stopRefnumQualifierGid",
    "stopRefnumValue"
})
public class StopRefnumType {

    @XmlElement(name = "StopRefnumQualifierGid", required = true)
    protected GLogXMLGidType stopRefnumQualifierGid;
    @XmlElement(name = "StopRefnumValue", required = true)
    protected String stopRefnumValue;

    /**
     * Obtém o valor da propriedade stopRefnumQualifierGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getStopRefnumQualifierGid() {
        return stopRefnumQualifierGid;
    }

    /**
     * Define o valor da propriedade stopRefnumQualifierGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setStopRefnumQualifierGid(GLogXMLGidType value) {
        this.stopRefnumQualifierGid = value;
    }

    /**
     * Obtém o valor da propriedade stopRefnumValue.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStopRefnumValue() {
        return stopRefnumValue;
    }

    /**
     * Define o valor da propriedade stopRefnumValue.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStopRefnumValue(String value) {
        this.stopRefnumValue = value;
    }

}
