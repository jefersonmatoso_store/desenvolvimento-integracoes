
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * (Inbound) ComplianceRule is request for the compliance screening. This interface is for inbound only.
 *          
 * 
 * <p>Classe Java de ComplianceRuleType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="ComplianceRuleType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RuleGroupGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="TransactionDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="CommodityGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="UserDefinedCommodityGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="ProductInfo" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}ProductInfoType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="InvolvedPartyDetail" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}InvolvedPartyDetailType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="CategoryTypeCode" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}CategoryTypeCodeType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Condition" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}ConditionType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="TerritoryInfo" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}TerritoryInfoType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="RegionInfo" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}RegionInfoType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="InvolvedPartyRefnumDetail" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}InvolvedPartyRefnumDetailType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="InvolvedPartyRemarkDetail" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}InvolvedPartyRemarkDetailType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ComplianceRuleType", namespace = "http://xmlns.oracle.com/apps/gtm/transmission/v6.4", propOrder = {
    "ruleGroupGid",
    "transactionDate",
    "commodityGid",
    "userDefinedCommodityGid",
    "productInfo",
    "involvedPartyDetail",
    "categoryTypeCode",
    "condition",
    "territoryInfo",
    "regionInfo",
    "involvedPartyRefnumDetail",
    "involvedPartyRemarkDetail"
})
public class ComplianceRuleType {

    @XmlElement(name = "RuleGroupGid", required = true)
    protected GLogXMLGidType ruleGroupGid;
    @XmlElement(name = "TransactionDate")
    protected GLogDateTimeType transactionDate;
    @XmlElement(name = "CommodityGid")
    protected GLogXMLGidType commodityGid;
    @XmlElement(name = "UserDefinedCommodityGid")
    protected GLogXMLGidType userDefinedCommodityGid;
    @XmlElement(name = "ProductInfo")
    protected List<ProductInfoType> productInfo;
    @XmlElement(name = "InvolvedPartyDetail")
    protected List<InvolvedPartyDetailType> involvedPartyDetail;
    @XmlElement(name = "CategoryTypeCode")
    protected List<CategoryTypeCodeType> categoryTypeCode;
    @XmlElement(name = "Condition")
    protected List<ConditionType> condition;
    @XmlElement(name = "TerritoryInfo")
    protected List<TerritoryInfoType> territoryInfo;
    @XmlElement(name = "RegionInfo")
    protected List<RegionInfoType> regionInfo;
    @XmlElement(name = "InvolvedPartyRefnumDetail")
    protected List<InvolvedPartyRefnumDetailType> involvedPartyRefnumDetail;
    @XmlElement(name = "InvolvedPartyRemarkDetail")
    protected List<InvolvedPartyRemarkDetailType> involvedPartyRemarkDetail;

    /**
     * Obtém o valor da propriedade ruleGroupGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRuleGroupGid() {
        return ruleGroupGid;
    }

    /**
     * Define o valor da propriedade ruleGroupGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRuleGroupGid(GLogXMLGidType value) {
        this.ruleGroupGid = value;
    }

    /**
     * Obtém o valor da propriedade transactionDate.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getTransactionDate() {
        return transactionDate;
    }

    /**
     * Define o valor da propriedade transactionDate.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setTransactionDate(GLogDateTimeType value) {
        this.transactionDate = value;
    }

    /**
     * Obtém o valor da propriedade commodityGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getCommodityGid() {
        return commodityGid;
    }

    /**
     * Define o valor da propriedade commodityGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setCommodityGid(GLogXMLGidType value) {
        this.commodityGid = value;
    }

    /**
     * Obtém o valor da propriedade userDefinedCommodityGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getUserDefinedCommodityGid() {
        return userDefinedCommodityGid;
    }

    /**
     * Define o valor da propriedade userDefinedCommodityGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setUserDefinedCommodityGid(GLogXMLGidType value) {
        this.userDefinedCommodityGid = value;
    }

    /**
     * Gets the value of the productInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the productInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getProductInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProductInfoType }
     * 
     * 
     */
    public List<ProductInfoType> getProductInfo() {
        if (productInfo == null) {
            productInfo = new ArrayList<ProductInfoType>();
        }
        return this.productInfo;
    }

    /**
     * Gets the value of the involvedPartyDetail property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the involvedPartyDetail property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInvolvedPartyDetail().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link InvolvedPartyDetailType }
     * 
     * 
     */
    public List<InvolvedPartyDetailType> getInvolvedPartyDetail() {
        if (involvedPartyDetail == null) {
            involvedPartyDetail = new ArrayList<InvolvedPartyDetailType>();
        }
        return this.involvedPartyDetail;
    }

    /**
     * Gets the value of the categoryTypeCode property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the categoryTypeCode property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCategoryTypeCode().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CategoryTypeCodeType }
     * 
     * 
     */
    public List<CategoryTypeCodeType> getCategoryTypeCode() {
        if (categoryTypeCode == null) {
            categoryTypeCode = new ArrayList<CategoryTypeCodeType>();
        }
        return this.categoryTypeCode;
    }

    /**
     * Gets the value of the condition property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the condition property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCondition().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ConditionType }
     * 
     * 
     */
    public List<ConditionType> getCondition() {
        if (condition == null) {
            condition = new ArrayList<ConditionType>();
        }
        return this.condition;
    }

    /**
     * Gets the value of the territoryInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the territoryInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTerritoryInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TerritoryInfoType }
     * 
     * 
     */
    public List<TerritoryInfoType> getTerritoryInfo() {
        if (territoryInfo == null) {
            territoryInfo = new ArrayList<TerritoryInfoType>();
        }
        return this.territoryInfo;
    }

    /**
     * Gets the value of the regionInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the regionInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRegionInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RegionInfoType }
     * 
     * 
     */
    public List<RegionInfoType> getRegionInfo() {
        if (regionInfo == null) {
            regionInfo = new ArrayList<RegionInfoType>();
        }
        return this.regionInfo;
    }

    /**
     * Gets the value of the involvedPartyRefnumDetail property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the involvedPartyRefnumDetail property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInvolvedPartyRefnumDetail().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link InvolvedPartyRefnumDetailType }
     * 
     * 
     */
    public List<InvolvedPartyRefnumDetailType> getInvolvedPartyRefnumDetail() {
        if (involvedPartyRefnumDetail == null) {
            involvedPartyRefnumDetail = new ArrayList<InvolvedPartyRefnumDetailType>();
        }
        return this.involvedPartyRefnumDetail;
    }

    /**
     * Gets the value of the involvedPartyRemarkDetail property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the involvedPartyRemarkDetail property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInvolvedPartyRemarkDetail().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link InvolvedPartyRemarkDetailType }
     * 
     * 
     */
    public List<InvolvedPartyRemarkDetailType> getInvolvedPartyRemarkDetail() {
        if (involvedPartyRemarkDetail == null) {
            involvedPartyRemarkDetail = new ArrayList<InvolvedPartyRemarkDetailType>();
        }
        return this.involvedPartyRemarkDetail;
    }

}
