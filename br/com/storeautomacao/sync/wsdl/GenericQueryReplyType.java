
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Classe Java de GenericQueryReplyType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="GenericQueryReplyType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GLogXMLElement" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLElementType" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GenericQueryReplyType", propOrder = {
    "gLogXMLElement"
})
public class GenericQueryReplyType {

    @XmlElement(name = "GLogXMLElement", required = true)
    protected List<GLogXMLElementType> gLogXMLElement;

    /**
     * Gets the value of the gLogXMLElement property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the gLogXMLElement property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGLogXMLElement().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GLogXMLElementType }
     * 
     * 
     */
    public List<GLogXMLElementType> getGLogXMLElement() {
        if (gLogXMLElement == null) {
            gLogXMLElement = new ArrayList<GLogXMLElementType>();
        }
        return this.gLogXMLElement;
    }

}
