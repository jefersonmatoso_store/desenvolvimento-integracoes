
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Temperature is a structure for specifying temperature in a particular unit of measure.
 * 
 * <p>Classe Java de TemperatureType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="TemperatureType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TemperatureValue" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="TemperatureUOMGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TemperatureType", propOrder = {
    "temperatureValue",
    "temperatureUOMGid"
})
public class TemperatureType {

    @XmlElement(name = "TemperatureValue", required = true)
    protected String temperatureValue;
    @XmlElement(name = "TemperatureUOMGid", required = true)
    protected GLogXMLGidType temperatureUOMGid;

    /**
     * Obtém o valor da propriedade temperatureValue.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTemperatureValue() {
        return temperatureValue;
    }

    /**
     * Define o valor da propriedade temperatureValue.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTemperatureValue(String value) {
        this.temperatureValue = value;
    }

    /**
     * Obtém o valor da propriedade temperatureUOMGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getTemperatureUOMGid() {
        return temperatureUOMGid;
    }

    /**
     * Define o valor da propriedade temperatureUOMGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setTemperatureUOMGid(GLogXMLGidType value) {
        this.temperatureUOMGid = value;
    }

}
