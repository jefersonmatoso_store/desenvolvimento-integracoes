
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Provides a breakdown of costs (including cost type, amount, and associated codes).
 *          
 * 
 * <p>Classe Java de CostDetailsType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="CostDetailsType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CostType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Cost" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType"/>
 *         &lt;element name="IsWeighted" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AccessorialCodeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="CostDetailInfo" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}CostDetailInfoType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CostDetailsType", propOrder = {
    "costType",
    "cost",
    "isWeighted",
    "accessorialCodeGid",
    "costDetailInfo"
})
public class CostDetailsType {

    @XmlElement(name = "CostType", required = true)
    protected String costType;
    @XmlElement(name = "Cost", required = true)
    protected GLogXMLFinancialAmountType cost;
    @XmlElement(name = "IsWeighted")
    protected String isWeighted;
    @XmlElement(name = "AccessorialCodeGid")
    protected GLogXMLGidType accessorialCodeGid;
    @XmlElement(name = "CostDetailInfo")
    protected CostDetailInfoType costDetailInfo;

    /**
     * Obtém o valor da propriedade costType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCostType() {
        return costType;
    }

    /**
     * Define o valor da propriedade costType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCostType(String value) {
        this.costType = value;
    }

    /**
     * Obtém o valor da propriedade cost.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getCost() {
        return cost;
    }

    /**
     * Define o valor da propriedade cost.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setCost(GLogXMLFinancialAmountType value) {
        this.cost = value;
    }

    /**
     * Obtém o valor da propriedade isWeighted.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsWeighted() {
        return isWeighted;
    }

    /**
     * Define o valor da propriedade isWeighted.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsWeighted(String value) {
        this.isWeighted = value;
    }

    /**
     * Obtém o valor da propriedade accessorialCodeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getAccessorialCodeGid() {
        return accessorialCodeGid;
    }

    /**
     * Define o valor da propriedade accessorialCodeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setAccessorialCodeGid(GLogXMLGidType value) {
        this.accessorialCodeGid = value;
    }

    /**
     * Obtém o valor da propriedade costDetailInfo.
     * 
     * @return
     *     possible object is
     *     {@link CostDetailInfoType }
     *     
     */
    public CostDetailInfoType getCostDetailInfo() {
        return costDetailInfo;
    }

    /**
     * Define o valor da propriedade costDetailInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link CostDetailInfoType }
     *     
     */
    public void setCostDetailInfo(CostDetailInfoType value) {
        this.costDetailInfo = value;
    }

}
