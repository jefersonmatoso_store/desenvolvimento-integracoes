
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Classe Java de RateOfferingType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="RateOfferingType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OTMTransactionOut">
 *       &lt;sequence>
 *         &lt;element name="SendReason" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}SendReasonType" minOccurs="0"/>
 *         &lt;element name="RATE_OFFERING_ROW" maxOccurs="unbounded">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="RATE_OFFERING_GID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="RATE_OFFERING_XID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="RATE_OFFERING_TYPE_GID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="RATE_OFFERING_DESC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="SERVPROV_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="RATE_GROUP_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="CURRENCY_GID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="TRANSPORT_MODE_GID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="USER_CLASSIFICATION1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="USER_CLASSIFICATION2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="USER_CLASSIFICATION3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="USER_CLASSIFICATION4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="EQUIPMENT_GROUP_PROFILE_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="RATE_SERVICE_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="RATE_VERSION_GID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="MAX_DISTANCE_CONSTRAINT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="MAX_DIST_CONSTRAINT_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="MAX_DIST_CONSTRAINT_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="MIN_WEIGHT_CONSTRAINT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="MIN_WEIGHT_CONSTRAINT_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="MIN_WEIGHT_CONSTRAINT_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="MAX_WEIGHT_CONSTRAINT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="MAX_WEIGHT_CONSTRAINT_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="MAX_WEIGHT_CONSTRAINT_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="MIN_VOLUME_CONSTRAINT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="MIN_VOLUME_CONSTRAINT_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="MIN_VOLUME_CONSTRAINT_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="MAX_VOLUME_CONSTRAINT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="MAX_VOLUME_CONSTRAINT_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="MAX_VOLUME_CONSTRAINT_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="MAX_SHIP_UNIT_WEIGHT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="MAX_SHIP_UNIT_WEIGHT_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="MAX_SHIP_UNIT_WEIGHT_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="TARIFF_NAME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="MIN_COST" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="MIN_COST_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="MIN_COST_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="RATE_DISTANCE_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="RATE_QUALITY_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="RATE_CLASSIFICATION_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="EXTERNAL_RATING_ENGINE_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="CIRCUITY_ALLOWANCE_PERCENT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="TOTAL_STOPS_CONSTRAINT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="PICKUP_STOPS_CONSTRAINT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="DELIVERY_STOPS_CONSTRAINT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="CIRCUITY_DISTANCE_COST" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="CIRCUITY_DISTANCE_COST_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="CIRCUITY_DISTANCE_COST_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="MAX_CIRCUITY_PERCENT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="MAX_CIRCUITY_DISTANCE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="MAX_CIRCUITY_DISTANCE_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="MAX_CIRCUITY_DISTANCE_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="STOPS_INCLUDED_IN_RATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="FLEX_COMMODITY_PROFILE_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="SHIPPER_MIN_VALUE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="SHIPPER_MIN_VALUE_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="SHIPPER_MIN_VALUE_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="MIN_SHIP_UNIT_SPEC_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="EXCHANGE_RATE_GID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="COMMODITY_USAGE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="FAK_RATE_AS" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="FAK_FLEX_COMMODITY_VALUE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="RM_ABSOLUTE_MIN_COST" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="RM_ABSOLUTE_MIN_COST_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="RM_ABSOLUTE_MIN_COST_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="MAX_COST" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="MAX_COST_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="MAX_COST_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="MIN_STOPS" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="SHORT_LINE_COST" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="SHORT_LINE_COST_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="SHORT_LINE_COST_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="MIN_LENGTH_CONSTRAINT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="MIN_LENGTH_CONSTRAINT_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="MIN_LENGTH_CONSTRAINT_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="MAX_LENGTH_CONSTRAINT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="MAX_LENGTH_CONSTRAINT_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="MAX_LENGTH_CONSTRAINT_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="MIN_WIDTH_CONSTRAINT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="MIN_WIDTH_CONSTRAINT_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="MIN_WIDTH_CONSTRAINT_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="MAX_WIDTH_CONSTRAINT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="MAX_WIDTH_CONSTRAINT_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="MAX_WIDTH_CONSTRAINT_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="MIN_HEIGHT_CONSTRAINT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="MIN_HEIGHT_CONSTRAINT_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="MIN_HEIGHT_CONSTRAINT_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="MAX_HEIGHT_CONSTRAINT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="MAX_HEIGHT_CONSTRAINT_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="MAX_HEIGHT_CONSTRAINT_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="MIN_GIRTH_CONSTRAINT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="MIN_GIRTH_CONSTRAINT_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="MIN_GIRTH_CONSTRAINT_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="MAX_GIRTH_CONSTRAINT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="MAX_GIRTH_CONSTRAINT_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="MAX_GIRTH_CONSTRAINT_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="WEIGHT_BREAK_PROFILE_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="MULTI_STOP_COST_METHOD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="WEIGHT_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="VOLUME_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="DISTANCE_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="DIM_RATE_FACTOR_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="PERSPECTIVE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="CORPORATION_PROFILE_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="CAPACITY_GROUP_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="CM_MAX_NUM_SHIPMENTS" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="CM_IS_SAME_EQUIP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="CM_PREV_SHIPMENT_PERCENT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="CM_IS_PERCENT_OF_DISTANCE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="TARIFF_REFNUM_QUALIFIER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="TARIFF_REFNUM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="TARIFF_PUB_AUTHORITY" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="TARIFF_REG_AGENCY_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="TARIFF_AGENCY_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="TARIFF_ISSUING_CARRIER_ID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="TARIFF_REFNUM_SUFFIX" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="TARIFF_SUPPLEMENT_ID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="TARIFF_EFFECTIVE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ALLOW_UNCOSTED_LINE_ITEMS" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="RAIL_INTER_MODAL_PLAN_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="CUSTOMER_RATE_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="COFC_TOFC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="IS_ACTIVE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="HANDLES_UNKNOWN_SHIPPER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="USES_TIME_BASED_RATES" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="EXPIRE_MARK_ID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="MIN_DISTANCE_CONSTRAINT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="MIN_DISTANCE_CONSTRNT_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="MIN_DISTANCE_CONSTRAINT_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="REGION_GROUP_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="IS_DEPOT_APPLICABLE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="RECALCULATE_COST" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="TRACK_CAPACITY_USAGE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ROUNDING_TYPE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ROUNDING_INTERVAL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ROUNDING_FIELDS_LEVEL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ROUNDING_APPLICATION" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="IS_CONTRACT_RATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="IS_DIRECT_ONLY_RATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="HAZARDOUS_RATE_TYPE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="USE_TACT_AS_DISPLAY_RATE_1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="USE_TACT_AS_DISPLAY_RATE_2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="USE_TACT_AS_DISPLAY_RATE_3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="DOMAIN_PROFILE_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="IS_ROUTE_EXECUTION_RATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="MIN_TENDER_LEAD_TIME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="MIN_TENDER_LEAD_TIME_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="MIN_TENDER_LEAD_TIME_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="MAX_TENDER_LEAD_TIME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="MAX_TENDER_LEAD_TIME_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="MAX_TENDER_LEAD_TIME_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="PACKAGE_WEIGHT_MIN" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="PACKAGE_WEIGHT_MIN_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="PACKAGE_WEIGHT_MIN_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="MAX_AVERAGE_PKG_WEIGHT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="MAX_AVERAGE_PKG_WEIGH_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="MAX_AVERAGE_PKG_WEIGH_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="PACKAGE_COUNT_METHOD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="MAX_SHIPUNIT_LINE_PKG_WEIGHT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="MAX_SHIPUNIT_LINE_PKG_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="MAX_SHIPUNIT_LINE_PKG_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="MAX_NUM_OF_SHIPMENT_SEGMENTS" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="RO_TIME_PERIOD_DEF_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="EXT_RE_FIELDSET_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="RATE_OFFERING_STOPS" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence maxOccurs="unbounded" minOccurs="0">
 *                             &lt;element name="RATE_OFFERING_STOPS_ROW" maxOccurs="unbounded">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="RATE_OFFERING_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                       &lt;element name="LOW_STOP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                       &lt;element name="HIGH_STOP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                       &lt;element name="PER_STOP_COST" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                       &lt;element name="PER_STOP_COST_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                       &lt;element name="PER_STOP_COST_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                       &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                       &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                       &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                       &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                       &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                     &lt;/sequence>
 *                                     &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="RATE_OFFERING_ACCESSORIAL" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="RATE_OFFERING_ACCESSORIAL_ROW" maxOccurs="unbounded" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="ACCESSORIAL_COST_GID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="RATE_OFFERING_GID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="ACCESSORIAL_CODE_GID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                       &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                       &lt;element name="ACCESSORIAL_COST" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ACCESSORIAL_COST_TYPE" minOccurs="0"/>
 *                                     &lt;/sequence>
 *                                     &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="RATE_OFFERING_COMMENT" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence maxOccurs="unbounded" minOccurs="0">
 *                             &lt;element name="RATE_OFFERING_COMMENT_ROW" maxOccurs="unbounded">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="RATE_OFFERING_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                       &lt;element name="COMMENT_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                       &lt;element name="ENTERED_BY" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                       &lt;element name="THE_COMMENT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                       &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                       &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                       &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                       &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                       &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                     &lt;/sequence>
 *                                     &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="RATE_RULES_AND_TERMS" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="RATE_RULES_AND_TERMS_ROW" maxOccurs="unbounded" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="RATE_OFFERING_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                       &lt;element name="RULE_NUMBER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                       &lt;element name="RULE_TITLE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                       &lt;element name="RULE_DESC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                       &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                       &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                       &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                       &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                       &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                     &lt;/sequence>
 *                                     &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="RO_SPECIAL_SERVICE_ACCESSORIAL" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="RO_SPECIAL_SERVICE_ACCESSORIAL_ROW" maxOccurs="unbounded" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="ACCESSORIAL_COST_GID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="RATE_OFFERING_GID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="ACCESSORIAL_CODE_GID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="SPECIAL_SERVICE_GID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                       &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                       &lt;element name="ACCESSORIAL_COST" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ACCESSORIAL_COST_TYPE" minOccurs="0"/>
 *                                     &lt;/sequence>
 *                                     &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="RATE_OFFERING_INV_PARTY" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="RATE_OFFERING_INV_PARTY_ROW" maxOccurs="unbounded" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="RATE_OFFERING_GID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="INVOLVED_PARTY_QUAL_GID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="INVOLVED_PARTY_CONTACT_GID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="COM_METHOD_GID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                     &lt;/sequence>
 *                                     &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="ATTRIBUTE1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ATTRIBUTE2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ATTRIBUTE3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ATTRIBUTE4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ATTRIBUTE5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ATTRIBUTE6" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ATTRIBUTE7" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ATTRIBUTE8" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ATTRIBUTE9" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ATTRIBUTE10" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ATTRIBUTE11" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ATTRIBUTE12" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ATTRIBUTE13" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ATTRIBUTE14" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ATTRIBUTE15" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ATTRIBUTE16" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ATTRIBUTE17" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ATTRIBUTE18" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ATTRIBUTE19" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ATTRIBUTE20" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ATTRIBUTE_NUMBER1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ATTRIBUTE_NUMBER2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ATTRIBUTE_NUMBER3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ATTRIBUTE_NUMBER4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ATTRIBUTE_NUMBER5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ATTRIBUTE_NUMBER6" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ATTRIBUTE_NUMBER7" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ATTRIBUTE_NUMBER8" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ATTRIBUTE_NUMBER9" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ATTRIBUTE_NUMBER10" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ATTRIBUTE_DATE1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ATTRIBUTE_DATE2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ATTRIBUTE_DATE3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ATTRIBUTE_DATE4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ATTRIBUTE_DATE5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ATTRIBUTE_DATE6" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ATTRIBUTE_DATE7" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ATTRIBUTE_DATE8" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ATTRIBUTE_DATE9" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ATTRIBUTE_DATE10" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                 &lt;/sequence>
 *                 &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" />
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RateOfferingType", propOrder = {
    "sendReason",
    "rateofferingrow"
})
public class RateOfferingType
    extends OTMTransactionOut
{

    @XmlElement(name = "SendReason")
    protected SendReasonType sendReason;
    @XmlElement(name = "RATE_OFFERING_ROW", required = true)
    protected List<RateOfferingType.RATEOFFERINGROW> rateofferingrow;

    /**
     * Obtém o valor da propriedade sendReason.
     * 
     * @return
     *     possible object is
     *     {@link SendReasonType }
     *     
     */
    public SendReasonType getSendReason() {
        return sendReason;
    }

    /**
     * Define o valor da propriedade sendReason.
     * 
     * @param value
     *     allowed object is
     *     {@link SendReasonType }
     *     
     */
    public void setSendReason(SendReasonType value) {
        this.sendReason = value;
    }

    /**
     * Gets the value of the rateofferingrow property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the rateofferingrow property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRATEOFFERINGROW().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RateOfferingType.RATEOFFERINGROW }
     * 
     * 
     */
    public List<RateOfferingType.RATEOFFERINGROW> getRATEOFFERINGROW() {
        if (rateofferingrow == null) {
            rateofferingrow = new ArrayList<RateOfferingType.RATEOFFERINGROW>();
        }
        return this.rateofferingrow;
    }


    /**
     * <p>Classe Java de anonymous complex type.
     * 
     * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="RATE_OFFERING_GID" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="RATE_OFFERING_XID" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="RATE_OFFERING_TYPE_GID" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="RATE_OFFERING_DESC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="SERVPROV_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="RATE_GROUP_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="CURRENCY_GID" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="TRANSPORT_MODE_GID" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="USER_CLASSIFICATION1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="USER_CLASSIFICATION2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="USER_CLASSIFICATION3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="USER_CLASSIFICATION4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="EQUIPMENT_GROUP_PROFILE_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="RATE_SERVICE_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="RATE_VERSION_GID" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="MAX_DISTANCE_CONSTRAINT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="MAX_DIST_CONSTRAINT_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="MAX_DIST_CONSTRAINT_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="MIN_WEIGHT_CONSTRAINT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="MIN_WEIGHT_CONSTRAINT_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="MIN_WEIGHT_CONSTRAINT_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="MAX_WEIGHT_CONSTRAINT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="MAX_WEIGHT_CONSTRAINT_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="MAX_WEIGHT_CONSTRAINT_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="MIN_VOLUME_CONSTRAINT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="MIN_VOLUME_CONSTRAINT_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="MIN_VOLUME_CONSTRAINT_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="MAX_VOLUME_CONSTRAINT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="MAX_VOLUME_CONSTRAINT_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="MAX_VOLUME_CONSTRAINT_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="MAX_SHIP_UNIT_WEIGHT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="MAX_SHIP_UNIT_WEIGHT_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="MAX_SHIP_UNIT_WEIGHT_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="TARIFF_NAME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="MIN_COST" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="MIN_COST_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="MIN_COST_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="RATE_DISTANCE_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="RATE_QUALITY_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="RATE_CLASSIFICATION_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="EXTERNAL_RATING_ENGINE_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="CIRCUITY_ALLOWANCE_PERCENT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="TOTAL_STOPS_CONSTRAINT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="PICKUP_STOPS_CONSTRAINT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="DELIVERY_STOPS_CONSTRAINT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="CIRCUITY_DISTANCE_COST" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="CIRCUITY_DISTANCE_COST_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="CIRCUITY_DISTANCE_COST_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="MAX_CIRCUITY_PERCENT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="MAX_CIRCUITY_DISTANCE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="MAX_CIRCUITY_DISTANCE_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="MAX_CIRCUITY_DISTANCE_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="STOPS_INCLUDED_IN_RATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="FLEX_COMMODITY_PROFILE_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="SHIPPER_MIN_VALUE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="SHIPPER_MIN_VALUE_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="SHIPPER_MIN_VALUE_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="MIN_SHIP_UNIT_SPEC_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="EXCHANGE_RATE_GID" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="COMMODITY_USAGE" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="FAK_RATE_AS" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="FAK_FLEX_COMMODITY_VALUE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="RM_ABSOLUTE_MIN_COST" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="RM_ABSOLUTE_MIN_COST_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="RM_ABSOLUTE_MIN_COST_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="MAX_COST" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="MAX_COST_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="MAX_COST_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="MIN_STOPS" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="SHORT_LINE_COST" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="SHORT_LINE_COST_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="SHORT_LINE_COST_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="MIN_LENGTH_CONSTRAINT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="MIN_LENGTH_CONSTRAINT_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="MIN_LENGTH_CONSTRAINT_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="MAX_LENGTH_CONSTRAINT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="MAX_LENGTH_CONSTRAINT_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="MAX_LENGTH_CONSTRAINT_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="MIN_WIDTH_CONSTRAINT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="MIN_WIDTH_CONSTRAINT_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="MIN_WIDTH_CONSTRAINT_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="MAX_WIDTH_CONSTRAINT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="MAX_WIDTH_CONSTRAINT_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="MAX_WIDTH_CONSTRAINT_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="MIN_HEIGHT_CONSTRAINT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="MIN_HEIGHT_CONSTRAINT_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="MIN_HEIGHT_CONSTRAINT_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="MAX_HEIGHT_CONSTRAINT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="MAX_HEIGHT_CONSTRAINT_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="MAX_HEIGHT_CONSTRAINT_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="MIN_GIRTH_CONSTRAINT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="MIN_GIRTH_CONSTRAINT_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="MIN_GIRTH_CONSTRAINT_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="MAX_GIRTH_CONSTRAINT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="MAX_GIRTH_CONSTRAINT_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="MAX_GIRTH_CONSTRAINT_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="WEIGHT_BREAK_PROFILE_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="MULTI_STOP_COST_METHOD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="WEIGHT_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="VOLUME_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="DISTANCE_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="DIM_RATE_FACTOR_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="PERSPECTIVE" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="CORPORATION_PROFILE_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="CAPACITY_GROUP_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="CM_MAX_NUM_SHIPMENTS" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="CM_IS_SAME_EQUIP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="CM_PREV_SHIPMENT_PERCENT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="CM_IS_PERCENT_OF_DISTANCE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="TARIFF_REFNUM_QUALIFIER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="TARIFF_REFNUM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="TARIFF_PUB_AUTHORITY" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="TARIFF_REG_AGENCY_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="TARIFF_AGENCY_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="TARIFF_ISSUING_CARRIER_ID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="TARIFF_REFNUM_SUFFIX" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="TARIFF_SUPPLEMENT_ID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="TARIFF_EFFECTIVE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ALLOW_UNCOSTED_LINE_ITEMS" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="RAIL_INTER_MODAL_PLAN_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="CUSTOMER_RATE_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="COFC_TOFC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="IS_ACTIVE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="HANDLES_UNKNOWN_SHIPPER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="USES_TIME_BASED_RATES" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="EXPIRE_MARK_ID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="MIN_DISTANCE_CONSTRAINT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="MIN_DISTANCE_CONSTRNT_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="MIN_DISTANCE_CONSTRAINT_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="REGION_GROUP_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="IS_DEPOT_APPLICABLE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="RECALCULATE_COST" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="TRACK_CAPACITY_USAGE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ROUNDING_TYPE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ROUNDING_INTERVAL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ROUNDING_FIELDS_LEVEL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ROUNDING_APPLICATION" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="IS_CONTRACT_RATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="IS_DIRECT_ONLY_RATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="HAZARDOUS_RATE_TYPE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="USE_TACT_AS_DISPLAY_RATE_1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="USE_TACT_AS_DISPLAY_RATE_2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="USE_TACT_AS_DISPLAY_RATE_3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="DOMAIN_PROFILE_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="IS_ROUTE_EXECUTION_RATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="MIN_TENDER_LEAD_TIME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="MIN_TENDER_LEAD_TIME_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="MIN_TENDER_LEAD_TIME_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="MAX_TENDER_LEAD_TIME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="MAX_TENDER_LEAD_TIME_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="MAX_TENDER_LEAD_TIME_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="PACKAGE_WEIGHT_MIN" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="PACKAGE_WEIGHT_MIN_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="PACKAGE_WEIGHT_MIN_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="MAX_AVERAGE_PKG_WEIGHT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="MAX_AVERAGE_PKG_WEIGH_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="MAX_AVERAGE_PKG_WEIGH_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="PACKAGE_COUNT_METHOD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="MAX_SHIPUNIT_LINE_PKG_WEIGHT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="MAX_SHIPUNIT_LINE_PKG_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="MAX_SHIPUNIT_LINE_PKG_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="MAX_NUM_OF_SHIPMENT_SEGMENTS" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="RO_TIME_PERIOD_DEF_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="EXT_RE_FIELDSET_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="RATE_OFFERING_STOPS" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence maxOccurs="unbounded" minOccurs="0">
     *                   &lt;element name="RATE_OFFERING_STOPS_ROW" maxOccurs="unbounded">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="RATE_OFFERING_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                             &lt;element name="LOW_STOP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                             &lt;element name="HIGH_STOP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                             &lt;element name="PER_STOP_COST" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                             &lt;element name="PER_STOP_COST_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                             &lt;element name="PER_STOP_COST_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                             &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                             &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                             &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                             &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                             &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                           &lt;/sequence>
     *                           &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="RATE_OFFERING_ACCESSORIAL" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="RATE_OFFERING_ACCESSORIAL_ROW" maxOccurs="unbounded" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="ACCESSORIAL_COST_GID" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="RATE_OFFERING_GID" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="ACCESSORIAL_CODE_GID" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                             &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                             &lt;element name="ACCESSORIAL_COST" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ACCESSORIAL_COST_TYPE" minOccurs="0"/>
     *                           &lt;/sequence>
     *                           &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="RATE_OFFERING_COMMENT" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence maxOccurs="unbounded" minOccurs="0">
     *                   &lt;element name="RATE_OFFERING_COMMENT_ROW" maxOccurs="unbounded">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="RATE_OFFERING_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                             &lt;element name="COMMENT_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                             &lt;element name="ENTERED_BY" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                             &lt;element name="THE_COMMENT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                             &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                             &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                             &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                             &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                             &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                           &lt;/sequence>
     *                           &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="RATE_RULES_AND_TERMS" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="RATE_RULES_AND_TERMS_ROW" maxOccurs="unbounded" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="RATE_OFFERING_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                             &lt;element name="RULE_NUMBER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                             &lt;element name="RULE_TITLE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                             &lt;element name="RULE_DESC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                             &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                             &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                             &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                             &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                             &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                           &lt;/sequence>
     *                           &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="RO_SPECIAL_SERVICE_ACCESSORIAL" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="RO_SPECIAL_SERVICE_ACCESSORIAL_ROW" maxOccurs="unbounded" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="ACCESSORIAL_COST_GID" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="RATE_OFFERING_GID" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="ACCESSORIAL_CODE_GID" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="SPECIAL_SERVICE_GID" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                             &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                             &lt;element name="ACCESSORIAL_COST" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ACCESSORIAL_COST_TYPE" minOccurs="0"/>
     *                           &lt;/sequence>
     *                           &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="RATE_OFFERING_INV_PARTY" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="RATE_OFFERING_INV_PARTY_ROW" maxOccurs="unbounded" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="RATE_OFFERING_GID" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="INVOLVED_PARTY_QUAL_GID" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="INVOLVED_PARTY_CONTACT_GID" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="COM_METHOD_GID" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                           &lt;/sequence>
     *                           &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="ATTRIBUTE1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE6" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE7" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE8" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE9" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE10" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE11" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE12" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE13" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE14" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE15" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE16" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE17" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE18" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE19" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE20" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE_NUMBER1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE_NUMBER2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE_NUMBER3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE_NUMBER4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE_NUMBER5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE_NUMBER6" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE_NUMBER7" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE_NUMBER8" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE_NUMBER9" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE_NUMBER10" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE_DATE1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE_DATE2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE_DATE3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE_DATE4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE_DATE5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE_DATE6" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE_DATE7" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE_DATE8" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE_DATE9" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE_DATE10" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *       &lt;/sequence>
     *       &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" />
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "rateofferinggid",
        "rateofferingxid",
        "rateofferingtypegid",
        "rateofferingdesc",
        "servprovgid",
        "rategroupgid",
        "currencygid",
        "transportmodegid",
        "userclassification1",
        "userclassification2",
        "userclassification3",
        "userclassification4",
        "equipmentgroupprofilegid",
        "rateservicegid",
        "rateversiongid",
        "maxdistanceconstraint",
        "maxdistconstraintuomcode",
        "maxdistconstraintbase",
        "minweightconstraint",
        "minweightconstraintuomcode",
        "minweightconstraintbase",
        "maxweightconstraint",
        "maxweightconstraintuomcode",
        "maxweightconstraintbase",
        "minvolumeconstraint",
        "minvolumeconstraintuomcode",
        "minvolumeconstraintbase",
        "maxvolumeconstraint",
        "maxvolumeconstraintuomcode",
        "maxvolumeconstraintbase",
        "maxshipunitweight",
        "maxshipunitweightuomcode",
        "maxshipunitweightbase",
        "tariffname",
        "mincost",
        "mincostgid",
        "mincostbase",
        "ratedistancegid",
        "ratequalitygid",
        "rateclassificationgid",
        "externalratingenginegid",
        "circuityallowancepercent",
        "totalstopsconstraint",
        "pickupstopsconstraint",
        "deliverystopsconstraint",
        "circuitydistancecost",
        "circuitydistancecostgid",
        "circuitydistancecostbase",
        "maxcircuitypercent",
        "maxcircuitydistance",
        "maxcircuitydistanceuomcode",
        "maxcircuitydistancebase",
        "stopsincludedinrate",
        "flexcommodityprofilegid",
        "shipperminvalue",
        "shipperminvaluegid",
        "shipperminvaluebase",
        "minshipunitspecgid",
        "exchangerategid",
        "domainname",
        "commodityusage",
        "fakrateas",
        "fakflexcommodityvalue",
        "rmabsolutemincost",
        "rmabsolutemincostgid",
        "rmabsolutemincostbase",
        "maxcost",
        "maxcostgid",
        "maxcostbase",
        "minstops",
        "shortlinecost",
        "shortlinecostgid",
        "shortlinecostbase",
        "minlengthconstraint",
        "minlengthconstraintuomcode",
        "minlengthconstraintbase",
        "maxlengthconstraint",
        "maxlengthconstraintuomcode",
        "maxlengthconstraintbase",
        "minwidthconstraint",
        "minwidthconstraintuomcode",
        "minwidthconstraintbase",
        "maxwidthconstraint",
        "maxwidthconstraintuomcode",
        "maxwidthconstraintbase",
        "minheightconstraint",
        "minheightconstraintuomcode",
        "minheightconstraintbase",
        "maxheightconstraint",
        "maxheightconstraintuomcode",
        "maxheightconstraintbase",
        "mingirthconstraint",
        "mingirthconstraintuomcode",
        "mingirthconstraintbase",
        "maxgirthconstraint",
        "maxgirthconstraintuomcode",
        "maxgirthconstraintbase",
        "weightbreakprofilegid",
        "multistopcostmethod",
        "weightuomcode",
        "volumeuomcode",
        "distanceuomcode",
        "dimratefactorgid",
        "perspective",
        "corporationprofilegid",
        "capacitygroupgid",
        "cmmaxnumshipments",
        "cmissameequip",
        "cmprevshipmentpercent",
        "cmispercentofdistance",
        "tariffrefnumqualifier",
        "tariffrefnum",
        "tariffpubauthority",
        "tariffregagencycode",
        "tariffagencycode",
        "tariffissuingcarrierid",
        "tariffrefnumsuffix",
        "tariffsupplementid",
        "tariffeffectivedate",
        "allowuncostedlineitems",
        "railintermodalplangid",
        "customerratecode",
        "cofctofc",
        "isactive",
        "handlesunknownshipper",
        "usestimebasedrates",
        "expiremarkid",
        "mindistanceconstraint",
        "mindistanceconstrntuomcode",
        "mindistanceconstraintbase",
        "regiongroupgid",
        "isdepotapplicable",
        "recalculatecost",
        "trackcapacityusage",
        "roundingtype",
        "roundinginterval",
        "roundingfieldslevel",
        "roundingapplication",
        "iscontractrate",
        "isdirectonlyrate",
        "hazardousratetype",
        "usetactasdisplayrate1",
        "usetactasdisplayrate2",
        "usetactasdisplayrate3",
        "domainprofilegid",
        "isrouteexecutionrate",
        "mintenderleadtime",
        "mintenderleadtimeuomcode",
        "mintenderleadtimebase",
        "maxtenderleadtime",
        "maxtenderleadtimeuomcode",
        "maxtenderleadtimebase",
        "packageweightmin",
        "packageweightminuomcode",
        "packageweightminbase",
        "maxaveragepkgweight",
        "maxaveragepkgweighuomcode",
        "maxaveragepkgweighbase",
        "packagecountmethod",
        "maxshipunitlinepkgweight",
        "maxshipunitlinepkguomcode",
        "maxshipunitlinepkgbase",
        "maxnumofshipmentsegments",
        "rotimeperioddefgid",
        "extrefieldsetgid",
        "insertuser",
        "insertdate",
        "updateuser",
        "updatedate",
        "rateofferingstops",
        "rateofferingaccessorial",
        "rateofferingcomment",
        "raterulesandterms",
        "rospecialserviceaccessorial",
        "rateofferinginvparty",
        "attribute1",
        "attribute2",
        "attribute3",
        "attribute4",
        "attribute5",
        "attribute6",
        "attribute7",
        "attribute8",
        "attribute9",
        "attribute10",
        "attribute11",
        "attribute12",
        "attribute13",
        "attribute14",
        "attribute15",
        "attribute16",
        "attribute17",
        "attribute18",
        "attribute19",
        "attribute20",
        "attributenumber1",
        "attributenumber2",
        "attributenumber3",
        "attributenumber4",
        "attributenumber5",
        "attributenumber6",
        "attributenumber7",
        "attributenumber8",
        "attributenumber9",
        "attributenumber10",
        "attributedate1",
        "attributedate2",
        "attributedate3",
        "attributedate4",
        "attributedate5",
        "attributedate6",
        "attributedate7",
        "attributedate8",
        "attributedate9",
        "attributedate10"
    })
    public static class RATEOFFERINGROW {

        @XmlElement(name = "RATE_OFFERING_GID", required = true)
        protected String rateofferinggid;
        @XmlElement(name = "RATE_OFFERING_XID", required = true)
        protected String rateofferingxid;
        @XmlElement(name = "RATE_OFFERING_TYPE_GID", required = true)
        protected String rateofferingtypegid;
        @XmlElement(name = "RATE_OFFERING_DESC")
        protected String rateofferingdesc;
        @XmlElement(name = "SERVPROV_GID")
        protected String servprovgid;
        @XmlElement(name = "RATE_GROUP_GID")
        protected String rategroupgid;
        @XmlElement(name = "CURRENCY_GID", required = true)
        protected String currencygid;
        @XmlElement(name = "TRANSPORT_MODE_GID", required = true)
        protected String transportmodegid;
        @XmlElement(name = "USER_CLASSIFICATION1")
        protected String userclassification1;
        @XmlElement(name = "USER_CLASSIFICATION2")
        protected String userclassification2;
        @XmlElement(name = "USER_CLASSIFICATION3")
        protected String userclassification3;
        @XmlElement(name = "USER_CLASSIFICATION4")
        protected String userclassification4;
        @XmlElement(name = "EQUIPMENT_GROUP_PROFILE_GID")
        protected String equipmentgroupprofilegid;
        @XmlElement(name = "RATE_SERVICE_GID")
        protected String rateservicegid;
        @XmlElement(name = "RATE_VERSION_GID", required = true)
        protected String rateversiongid;
        @XmlElement(name = "MAX_DISTANCE_CONSTRAINT")
        protected String maxdistanceconstraint;
        @XmlElement(name = "MAX_DIST_CONSTRAINT_UOM_CODE")
        protected String maxdistconstraintuomcode;
        @XmlElement(name = "MAX_DIST_CONSTRAINT_BASE")
        protected String maxdistconstraintbase;
        @XmlElement(name = "MIN_WEIGHT_CONSTRAINT")
        protected String minweightconstraint;
        @XmlElement(name = "MIN_WEIGHT_CONSTRAINT_UOM_CODE")
        protected String minweightconstraintuomcode;
        @XmlElement(name = "MIN_WEIGHT_CONSTRAINT_BASE")
        protected String minweightconstraintbase;
        @XmlElement(name = "MAX_WEIGHT_CONSTRAINT")
        protected String maxweightconstraint;
        @XmlElement(name = "MAX_WEIGHT_CONSTRAINT_UOM_CODE")
        protected String maxweightconstraintuomcode;
        @XmlElement(name = "MAX_WEIGHT_CONSTRAINT_BASE")
        protected String maxweightconstraintbase;
        @XmlElement(name = "MIN_VOLUME_CONSTRAINT")
        protected String minvolumeconstraint;
        @XmlElement(name = "MIN_VOLUME_CONSTRAINT_UOM_CODE")
        protected String minvolumeconstraintuomcode;
        @XmlElement(name = "MIN_VOLUME_CONSTRAINT_BASE")
        protected String minvolumeconstraintbase;
        @XmlElement(name = "MAX_VOLUME_CONSTRAINT")
        protected String maxvolumeconstraint;
        @XmlElement(name = "MAX_VOLUME_CONSTRAINT_UOM_CODE")
        protected String maxvolumeconstraintuomcode;
        @XmlElement(name = "MAX_VOLUME_CONSTRAINT_BASE")
        protected String maxvolumeconstraintbase;
        @XmlElement(name = "MAX_SHIP_UNIT_WEIGHT")
        protected String maxshipunitweight;
        @XmlElement(name = "MAX_SHIP_UNIT_WEIGHT_UOM_CODE")
        protected String maxshipunitweightuomcode;
        @XmlElement(name = "MAX_SHIP_UNIT_WEIGHT_BASE")
        protected String maxshipunitweightbase;
        @XmlElement(name = "TARIFF_NAME")
        protected String tariffname;
        @XmlElement(name = "MIN_COST")
        protected String mincost;
        @XmlElement(name = "MIN_COST_GID")
        protected String mincostgid;
        @XmlElement(name = "MIN_COST_BASE")
        protected String mincostbase;
        @XmlElement(name = "RATE_DISTANCE_GID")
        protected String ratedistancegid;
        @XmlElement(name = "RATE_QUALITY_GID")
        protected String ratequalitygid;
        @XmlElement(name = "RATE_CLASSIFICATION_GID")
        protected String rateclassificationgid;
        @XmlElement(name = "EXTERNAL_RATING_ENGINE_GID")
        protected String externalratingenginegid;
        @XmlElement(name = "CIRCUITY_ALLOWANCE_PERCENT")
        protected String circuityallowancepercent;
        @XmlElement(name = "TOTAL_STOPS_CONSTRAINT")
        protected String totalstopsconstraint;
        @XmlElement(name = "PICKUP_STOPS_CONSTRAINT")
        protected String pickupstopsconstraint;
        @XmlElement(name = "DELIVERY_STOPS_CONSTRAINT")
        protected String deliverystopsconstraint;
        @XmlElement(name = "CIRCUITY_DISTANCE_COST")
        protected String circuitydistancecost;
        @XmlElement(name = "CIRCUITY_DISTANCE_COST_GID")
        protected String circuitydistancecostgid;
        @XmlElement(name = "CIRCUITY_DISTANCE_COST_BASE")
        protected String circuitydistancecostbase;
        @XmlElement(name = "MAX_CIRCUITY_PERCENT")
        protected String maxcircuitypercent;
        @XmlElement(name = "MAX_CIRCUITY_DISTANCE")
        protected String maxcircuitydistance;
        @XmlElement(name = "MAX_CIRCUITY_DISTANCE_UOM_CODE")
        protected String maxcircuitydistanceuomcode;
        @XmlElement(name = "MAX_CIRCUITY_DISTANCE_BASE")
        protected String maxcircuitydistancebase;
        @XmlElement(name = "STOPS_INCLUDED_IN_RATE")
        protected String stopsincludedinrate;
        @XmlElement(name = "FLEX_COMMODITY_PROFILE_GID")
        protected String flexcommodityprofilegid;
        @XmlElement(name = "SHIPPER_MIN_VALUE")
        protected String shipperminvalue;
        @XmlElement(name = "SHIPPER_MIN_VALUE_GID")
        protected String shipperminvaluegid;
        @XmlElement(name = "SHIPPER_MIN_VALUE_BASE")
        protected String shipperminvaluebase;
        @XmlElement(name = "MIN_SHIP_UNIT_SPEC_GID")
        protected String minshipunitspecgid;
        @XmlElement(name = "EXCHANGE_RATE_GID", required = true)
        protected String exchangerategid;
        @XmlElement(name = "DOMAIN_NAME", required = true)
        protected String domainname;
        @XmlElement(name = "COMMODITY_USAGE", required = true)
        protected String commodityusage;
        @XmlElement(name = "FAK_RATE_AS", required = true)
        protected String fakrateas;
        @XmlElement(name = "FAK_FLEX_COMMODITY_VALUE")
        protected String fakflexcommodityvalue;
        @XmlElement(name = "RM_ABSOLUTE_MIN_COST")
        protected String rmabsolutemincost;
        @XmlElement(name = "RM_ABSOLUTE_MIN_COST_GID")
        protected String rmabsolutemincostgid;
        @XmlElement(name = "RM_ABSOLUTE_MIN_COST_BASE")
        protected String rmabsolutemincostbase;
        @XmlElement(name = "MAX_COST")
        protected String maxcost;
        @XmlElement(name = "MAX_COST_GID")
        protected String maxcostgid;
        @XmlElement(name = "MAX_COST_BASE")
        protected String maxcostbase;
        @XmlElement(name = "MIN_STOPS")
        protected String minstops;
        @XmlElement(name = "SHORT_LINE_COST")
        protected String shortlinecost;
        @XmlElement(name = "SHORT_LINE_COST_GID")
        protected String shortlinecostgid;
        @XmlElement(name = "SHORT_LINE_COST_BASE")
        protected String shortlinecostbase;
        @XmlElement(name = "MIN_LENGTH_CONSTRAINT")
        protected String minlengthconstraint;
        @XmlElement(name = "MIN_LENGTH_CONSTRAINT_UOM_CODE")
        protected String minlengthconstraintuomcode;
        @XmlElement(name = "MIN_LENGTH_CONSTRAINT_BASE")
        protected String minlengthconstraintbase;
        @XmlElement(name = "MAX_LENGTH_CONSTRAINT")
        protected String maxlengthconstraint;
        @XmlElement(name = "MAX_LENGTH_CONSTRAINT_UOM_CODE")
        protected String maxlengthconstraintuomcode;
        @XmlElement(name = "MAX_LENGTH_CONSTRAINT_BASE")
        protected String maxlengthconstraintbase;
        @XmlElement(name = "MIN_WIDTH_CONSTRAINT")
        protected String minwidthconstraint;
        @XmlElement(name = "MIN_WIDTH_CONSTRAINT_UOM_CODE")
        protected String minwidthconstraintuomcode;
        @XmlElement(name = "MIN_WIDTH_CONSTRAINT_BASE")
        protected String minwidthconstraintbase;
        @XmlElement(name = "MAX_WIDTH_CONSTRAINT")
        protected String maxwidthconstraint;
        @XmlElement(name = "MAX_WIDTH_CONSTRAINT_UOM_CODE")
        protected String maxwidthconstraintuomcode;
        @XmlElement(name = "MAX_WIDTH_CONSTRAINT_BASE")
        protected String maxwidthconstraintbase;
        @XmlElement(name = "MIN_HEIGHT_CONSTRAINT")
        protected String minheightconstraint;
        @XmlElement(name = "MIN_HEIGHT_CONSTRAINT_UOM_CODE")
        protected String minheightconstraintuomcode;
        @XmlElement(name = "MIN_HEIGHT_CONSTRAINT_BASE")
        protected String minheightconstraintbase;
        @XmlElement(name = "MAX_HEIGHT_CONSTRAINT")
        protected String maxheightconstraint;
        @XmlElement(name = "MAX_HEIGHT_CONSTRAINT_UOM_CODE")
        protected String maxheightconstraintuomcode;
        @XmlElement(name = "MAX_HEIGHT_CONSTRAINT_BASE")
        protected String maxheightconstraintbase;
        @XmlElement(name = "MIN_GIRTH_CONSTRAINT")
        protected String mingirthconstraint;
        @XmlElement(name = "MIN_GIRTH_CONSTRAINT_UOM_CODE")
        protected String mingirthconstraintuomcode;
        @XmlElement(name = "MIN_GIRTH_CONSTRAINT_BASE")
        protected String mingirthconstraintbase;
        @XmlElement(name = "MAX_GIRTH_CONSTRAINT")
        protected String maxgirthconstraint;
        @XmlElement(name = "MAX_GIRTH_CONSTRAINT_UOM_CODE")
        protected String maxgirthconstraintuomcode;
        @XmlElement(name = "MAX_GIRTH_CONSTRAINT_BASE")
        protected String maxgirthconstraintbase;
        @XmlElement(name = "WEIGHT_BREAK_PROFILE_GID")
        protected String weightbreakprofilegid;
        @XmlElement(name = "MULTI_STOP_COST_METHOD")
        protected String multistopcostmethod;
        @XmlElement(name = "WEIGHT_UOM_CODE")
        protected String weightuomcode;
        @XmlElement(name = "VOLUME_UOM_CODE")
        protected String volumeuomcode;
        @XmlElement(name = "DISTANCE_UOM_CODE")
        protected String distanceuomcode;
        @XmlElement(name = "DIM_RATE_FACTOR_GID")
        protected String dimratefactorgid;
        @XmlElement(name = "PERSPECTIVE", required = true)
        protected String perspective;
        @XmlElement(name = "CORPORATION_PROFILE_GID")
        protected String corporationprofilegid;
        @XmlElement(name = "CAPACITY_GROUP_GID")
        protected String capacitygroupgid;
        @XmlElement(name = "CM_MAX_NUM_SHIPMENTS")
        protected String cmmaxnumshipments;
        @XmlElement(name = "CM_IS_SAME_EQUIP")
        protected String cmissameequip;
        @XmlElement(name = "CM_PREV_SHIPMENT_PERCENT")
        protected String cmprevshipmentpercent;
        @XmlElement(name = "CM_IS_PERCENT_OF_DISTANCE")
        protected String cmispercentofdistance;
        @XmlElement(name = "TARIFF_REFNUM_QUALIFIER")
        protected String tariffrefnumqualifier;
        @XmlElement(name = "TARIFF_REFNUM")
        protected String tariffrefnum;
        @XmlElement(name = "TARIFF_PUB_AUTHORITY")
        protected String tariffpubauthority;
        @XmlElement(name = "TARIFF_REG_AGENCY_CODE")
        protected String tariffregagencycode;
        @XmlElement(name = "TARIFF_AGENCY_CODE")
        protected String tariffagencycode;
        @XmlElement(name = "TARIFF_ISSUING_CARRIER_ID")
        protected String tariffissuingcarrierid;
        @XmlElement(name = "TARIFF_REFNUM_SUFFIX")
        protected String tariffrefnumsuffix;
        @XmlElement(name = "TARIFF_SUPPLEMENT_ID")
        protected String tariffsupplementid;
        @XmlElement(name = "TARIFF_EFFECTIVE_DATE")
        protected String tariffeffectivedate;
        @XmlElement(name = "ALLOW_UNCOSTED_LINE_ITEMS", required = true)
        protected String allowuncostedlineitems;
        @XmlElement(name = "RAIL_INTER_MODAL_PLAN_GID")
        protected String railintermodalplangid;
        @XmlElement(name = "CUSTOMER_RATE_CODE")
        protected String customerratecode;
        @XmlElement(name = "COFC_TOFC")
        protected String cofctofc;
        @XmlElement(name = "IS_ACTIVE")
        protected String isactive;
        @XmlElement(name = "HANDLES_UNKNOWN_SHIPPER")
        protected String handlesunknownshipper;
        @XmlElement(name = "USES_TIME_BASED_RATES")
        protected String usestimebasedrates;
        @XmlElement(name = "EXPIRE_MARK_ID")
        protected String expiremarkid;
        @XmlElement(name = "MIN_DISTANCE_CONSTRAINT")
        protected String mindistanceconstraint;
        @XmlElement(name = "MIN_DISTANCE_CONSTRNT_UOM_CODE")
        protected String mindistanceconstrntuomcode;
        @XmlElement(name = "MIN_DISTANCE_CONSTRAINT_BASE")
        protected String mindistanceconstraintbase;
        @XmlElement(name = "REGION_GROUP_GID")
        protected String regiongroupgid;
        @XmlElement(name = "IS_DEPOT_APPLICABLE")
        protected String isdepotapplicable;
        @XmlElement(name = "RECALCULATE_COST")
        protected String recalculatecost;
        @XmlElement(name = "TRACK_CAPACITY_USAGE")
        protected String trackcapacityusage;
        @XmlElement(name = "ROUNDING_TYPE")
        protected String roundingtype;
        @XmlElement(name = "ROUNDING_INTERVAL")
        protected String roundinginterval;
        @XmlElement(name = "ROUNDING_FIELDS_LEVEL")
        protected String roundingfieldslevel;
        @XmlElement(name = "ROUNDING_APPLICATION")
        protected String roundingapplication;
        @XmlElement(name = "IS_CONTRACT_RATE")
        protected String iscontractrate;
        @XmlElement(name = "IS_DIRECT_ONLY_RATE")
        protected String isdirectonlyrate;
        @XmlElement(name = "HAZARDOUS_RATE_TYPE")
        protected String hazardousratetype;
        @XmlElement(name = "USE_TACT_AS_DISPLAY_RATE_1")
        protected String usetactasdisplayrate1;
        @XmlElement(name = "USE_TACT_AS_DISPLAY_RATE_2")
        protected String usetactasdisplayrate2;
        @XmlElement(name = "USE_TACT_AS_DISPLAY_RATE_3")
        protected String usetactasdisplayrate3;
        @XmlElement(name = "DOMAIN_PROFILE_GID")
        protected String domainprofilegid;
        @XmlElement(name = "IS_ROUTE_EXECUTION_RATE")
        protected String isrouteexecutionrate;
        @XmlElement(name = "MIN_TENDER_LEAD_TIME")
        protected String mintenderleadtime;
        @XmlElement(name = "MIN_TENDER_LEAD_TIME_UOM_CODE")
        protected String mintenderleadtimeuomcode;
        @XmlElement(name = "MIN_TENDER_LEAD_TIME_BASE")
        protected String mintenderleadtimebase;
        @XmlElement(name = "MAX_TENDER_LEAD_TIME")
        protected String maxtenderleadtime;
        @XmlElement(name = "MAX_TENDER_LEAD_TIME_UOM_CODE")
        protected String maxtenderleadtimeuomcode;
        @XmlElement(name = "MAX_TENDER_LEAD_TIME_BASE")
        protected String maxtenderleadtimebase;
        @XmlElement(name = "PACKAGE_WEIGHT_MIN")
        protected String packageweightmin;
        @XmlElement(name = "PACKAGE_WEIGHT_MIN_UOM_CODE")
        protected String packageweightminuomcode;
        @XmlElement(name = "PACKAGE_WEIGHT_MIN_BASE")
        protected String packageweightminbase;
        @XmlElement(name = "MAX_AVERAGE_PKG_WEIGHT")
        protected String maxaveragepkgweight;
        @XmlElement(name = "MAX_AVERAGE_PKG_WEIGH_UOM_CODE")
        protected String maxaveragepkgweighuomcode;
        @XmlElement(name = "MAX_AVERAGE_PKG_WEIGH_BASE")
        protected String maxaveragepkgweighbase;
        @XmlElement(name = "PACKAGE_COUNT_METHOD")
        protected String packagecountmethod;
        @XmlElement(name = "MAX_SHIPUNIT_LINE_PKG_WEIGHT")
        protected String maxshipunitlinepkgweight;
        @XmlElement(name = "MAX_SHIPUNIT_LINE_PKG_UOM_CODE")
        protected String maxshipunitlinepkguomcode;
        @XmlElement(name = "MAX_SHIPUNIT_LINE_PKG_BASE")
        protected String maxshipunitlinepkgbase;
        @XmlElement(name = "MAX_NUM_OF_SHIPMENT_SEGMENTS")
        protected String maxnumofshipmentsegments;
        @XmlElement(name = "RO_TIME_PERIOD_DEF_GID")
        protected String rotimeperioddefgid;
        @XmlElement(name = "EXT_RE_FIELDSET_GID")
        protected String extrefieldsetgid;
        @XmlElement(name = "INSERT_USER", required = true)
        protected String insertuser;
        @XmlElement(name = "INSERT_DATE", required = true)
        protected String insertdate;
        @XmlElement(name = "UPDATE_USER")
        protected String updateuser;
        @XmlElement(name = "UPDATE_DATE")
        protected String updatedate;
        @XmlElement(name = "RATE_OFFERING_STOPS")
        protected RateOfferingType.RATEOFFERINGROW.RATEOFFERINGSTOPS rateofferingstops;
        @XmlElement(name = "RATE_OFFERING_ACCESSORIAL")
        protected RateOfferingType.RATEOFFERINGROW.RATEOFFERINGACCESSORIAL rateofferingaccessorial;
        @XmlElement(name = "RATE_OFFERING_COMMENT")
        protected RateOfferingType.RATEOFFERINGROW.RATEOFFERINGCOMMENT rateofferingcomment;
        @XmlElement(name = "RATE_RULES_AND_TERMS")
        protected RateOfferingType.RATEOFFERINGROW.RATERULESANDTERMS raterulesandterms;
        @XmlElement(name = "RO_SPECIAL_SERVICE_ACCESSORIAL")
        protected RateOfferingType.RATEOFFERINGROW.ROSPECIALSERVICEACCESSORIAL rospecialserviceaccessorial;
        @XmlElement(name = "RATE_OFFERING_INV_PARTY")
        protected RateOfferingType.RATEOFFERINGROW.RATEOFFERINGINVPARTY rateofferinginvparty;
        @XmlElement(name = "ATTRIBUTE1")
        protected String attribute1;
        @XmlElement(name = "ATTRIBUTE2")
        protected String attribute2;
        @XmlElement(name = "ATTRIBUTE3")
        protected String attribute3;
        @XmlElement(name = "ATTRIBUTE4")
        protected String attribute4;
        @XmlElement(name = "ATTRIBUTE5")
        protected String attribute5;
        @XmlElement(name = "ATTRIBUTE6")
        protected String attribute6;
        @XmlElement(name = "ATTRIBUTE7")
        protected String attribute7;
        @XmlElement(name = "ATTRIBUTE8")
        protected String attribute8;
        @XmlElement(name = "ATTRIBUTE9")
        protected String attribute9;
        @XmlElement(name = "ATTRIBUTE10")
        protected String attribute10;
        @XmlElement(name = "ATTRIBUTE11")
        protected String attribute11;
        @XmlElement(name = "ATTRIBUTE12")
        protected String attribute12;
        @XmlElement(name = "ATTRIBUTE13")
        protected String attribute13;
        @XmlElement(name = "ATTRIBUTE14")
        protected String attribute14;
        @XmlElement(name = "ATTRIBUTE15")
        protected String attribute15;
        @XmlElement(name = "ATTRIBUTE16")
        protected String attribute16;
        @XmlElement(name = "ATTRIBUTE17")
        protected String attribute17;
        @XmlElement(name = "ATTRIBUTE18")
        protected String attribute18;
        @XmlElement(name = "ATTRIBUTE19")
        protected String attribute19;
        @XmlElement(name = "ATTRIBUTE20")
        protected String attribute20;
        @XmlElement(name = "ATTRIBUTE_NUMBER1")
        protected String attributenumber1;
        @XmlElement(name = "ATTRIBUTE_NUMBER2")
        protected String attributenumber2;
        @XmlElement(name = "ATTRIBUTE_NUMBER3")
        protected String attributenumber3;
        @XmlElement(name = "ATTRIBUTE_NUMBER4")
        protected String attributenumber4;
        @XmlElement(name = "ATTRIBUTE_NUMBER5")
        protected String attributenumber5;
        @XmlElement(name = "ATTRIBUTE_NUMBER6")
        protected String attributenumber6;
        @XmlElement(name = "ATTRIBUTE_NUMBER7")
        protected String attributenumber7;
        @XmlElement(name = "ATTRIBUTE_NUMBER8")
        protected String attributenumber8;
        @XmlElement(name = "ATTRIBUTE_NUMBER9")
        protected String attributenumber9;
        @XmlElement(name = "ATTRIBUTE_NUMBER10")
        protected String attributenumber10;
        @XmlElement(name = "ATTRIBUTE_DATE1")
        protected String attributedate1;
        @XmlElement(name = "ATTRIBUTE_DATE2")
        protected String attributedate2;
        @XmlElement(name = "ATTRIBUTE_DATE3")
        protected String attributedate3;
        @XmlElement(name = "ATTRIBUTE_DATE4")
        protected String attributedate4;
        @XmlElement(name = "ATTRIBUTE_DATE5")
        protected String attributedate5;
        @XmlElement(name = "ATTRIBUTE_DATE6")
        protected String attributedate6;
        @XmlElement(name = "ATTRIBUTE_DATE7")
        protected String attributedate7;
        @XmlElement(name = "ATTRIBUTE_DATE8")
        protected String attributedate8;
        @XmlElement(name = "ATTRIBUTE_DATE9")
        protected String attributedate9;
        @XmlElement(name = "ATTRIBUTE_DATE10")
        protected String attributedate10;
        @XmlAttribute(name = "num")
        protected String num;

        /**
         * Obtém o valor da propriedade rateofferinggid.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRATEOFFERINGGID() {
            return rateofferinggid;
        }

        /**
         * Define o valor da propriedade rateofferinggid.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRATEOFFERINGGID(String value) {
            this.rateofferinggid = value;
        }

        /**
         * Obtém o valor da propriedade rateofferingxid.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRATEOFFERINGXID() {
            return rateofferingxid;
        }

        /**
         * Define o valor da propriedade rateofferingxid.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRATEOFFERINGXID(String value) {
            this.rateofferingxid = value;
        }

        /**
         * Obtém o valor da propriedade rateofferingtypegid.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRATEOFFERINGTYPEGID() {
            return rateofferingtypegid;
        }

        /**
         * Define o valor da propriedade rateofferingtypegid.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRATEOFFERINGTYPEGID(String value) {
            this.rateofferingtypegid = value;
        }

        /**
         * Obtém o valor da propriedade rateofferingdesc.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRATEOFFERINGDESC() {
            return rateofferingdesc;
        }

        /**
         * Define o valor da propriedade rateofferingdesc.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRATEOFFERINGDESC(String value) {
            this.rateofferingdesc = value;
        }

        /**
         * Obtém o valor da propriedade servprovgid.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSERVPROVGID() {
            return servprovgid;
        }

        /**
         * Define o valor da propriedade servprovgid.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSERVPROVGID(String value) {
            this.servprovgid = value;
        }

        /**
         * Obtém o valor da propriedade rategroupgid.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRATEGROUPGID() {
            return rategroupgid;
        }

        /**
         * Define o valor da propriedade rategroupgid.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRATEGROUPGID(String value) {
            this.rategroupgid = value;
        }

        /**
         * Obtém o valor da propriedade currencygid.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCURRENCYGID() {
            return currencygid;
        }

        /**
         * Define o valor da propriedade currencygid.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCURRENCYGID(String value) {
            this.currencygid = value;
        }

        /**
         * Obtém o valor da propriedade transportmodegid.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTRANSPORTMODEGID() {
            return transportmodegid;
        }

        /**
         * Define o valor da propriedade transportmodegid.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTRANSPORTMODEGID(String value) {
            this.transportmodegid = value;
        }

        /**
         * Obtém o valor da propriedade userclassification1.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getUSERCLASSIFICATION1() {
            return userclassification1;
        }

        /**
         * Define o valor da propriedade userclassification1.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setUSERCLASSIFICATION1(String value) {
            this.userclassification1 = value;
        }

        /**
         * Obtém o valor da propriedade userclassification2.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getUSERCLASSIFICATION2() {
            return userclassification2;
        }

        /**
         * Define o valor da propriedade userclassification2.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setUSERCLASSIFICATION2(String value) {
            this.userclassification2 = value;
        }

        /**
         * Obtém o valor da propriedade userclassification3.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getUSERCLASSIFICATION3() {
            return userclassification3;
        }

        /**
         * Define o valor da propriedade userclassification3.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setUSERCLASSIFICATION3(String value) {
            this.userclassification3 = value;
        }

        /**
         * Obtém o valor da propriedade userclassification4.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getUSERCLASSIFICATION4() {
            return userclassification4;
        }

        /**
         * Define o valor da propriedade userclassification4.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setUSERCLASSIFICATION4(String value) {
            this.userclassification4 = value;
        }

        /**
         * Obtém o valor da propriedade equipmentgroupprofilegid.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEQUIPMENTGROUPPROFILEGID() {
            return equipmentgroupprofilegid;
        }

        /**
         * Define o valor da propriedade equipmentgroupprofilegid.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEQUIPMENTGROUPPROFILEGID(String value) {
            this.equipmentgroupprofilegid = value;
        }

        /**
         * Obtém o valor da propriedade rateservicegid.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRATESERVICEGID() {
            return rateservicegid;
        }

        /**
         * Define o valor da propriedade rateservicegid.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRATESERVICEGID(String value) {
            this.rateservicegid = value;
        }

        /**
         * Obtém o valor da propriedade rateversiongid.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRATEVERSIONGID() {
            return rateversiongid;
        }

        /**
         * Define o valor da propriedade rateversiongid.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRATEVERSIONGID(String value) {
            this.rateversiongid = value;
        }

        /**
         * Obtém o valor da propriedade maxdistanceconstraint.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMAXDISTANCECONSTRAINT() {
            return maxdistanceconstraint;
        }

        /**
         * Define o valor da propriedade maxdistanceconstraint.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMAXDISTANCECONSTRAINT(String value) {
            this.maxdistanceconstraint = value;
        }

        /**
         * Obtém o valor da propriedade maxdistconstraintuomcode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMAXDISTCONSTRAINTUOMCODE() {
            return maxdistconstraintuomcode;
        }

        /**
         * Define o valor da propriedade maxdistconstraintuomcode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMAXDISTCONSTRAINTUOMCODE(String value) {
            this.maxdistconstraintuomcode = value;
        }

        /**
         * Obtém o valor da propriedade maxdistconstraintbase.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMAXDISTCONSTRAINTBASE() {
            return maxdistconstraintbase;
        }

        /**
         * Define o valor da propriedade maxdistconstraintbase.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMAXDISTCONSTRAINTBASE(String value) {
            this.maxdistconstraintbase = value;
        }

        /**
         * Obtém o valor da propriedade minweightconstraint.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMINWEIGHTCONSTRAINT() {
            return minweightconstraint;
        }

        /**
         * Define o valor da propriedade minweightconstraint.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMINWEIGHTCONSTRAINT(String value) {
            this.minweightconstraint = value;
        }

        /**
         * Obtém o valor da propriedade minweightconstraintuomcode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMINWEIGHTCONSTRAINTUOMCODE() {
            return minweightconstraintuomcode;
        }

        /**
         * Define o valor da propriedade minweightconstraintuomcode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMINWEIGHTCONSTRAINTUOMCODE(String value) {
            this.minweightconstraintuomcode = value;
        }

        /**
         * Obtém o valor da propriedade minweightconstraintbase.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMINWEIGHTCONSTRAINTBASE() {
            return minweightconstraintbase;
        }

        /**
         * Define o valor da propriedade minweightconstraintbase.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMINWEIGHTCONSTRAINTBASE(String value) {
            this.minweightconstraintbase = value;
        }

        /**
         * Obtém o valor da propriedade maxweightconstraint.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMAXWEIGHTCONSTRAINT() {
            return maxweightconstraint;
        }

        /**
         * Define o valor da propriedade maxweightconstraint.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMAXWEIGHTCONSTRAINT(String value) {
            this.maxweightconstraint = value;
        }

        /**
         * Obtém o valor da propriedade maxweightconstraintuomcode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMAXWEIGHTCONSTRAINTUOMCODE() {
            return maxweightconstraintuomcode;
        }

        /**
         * Define o valor da propriedade maxweightconstraintuomcode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMAXWEIGHTCONSTRAINTUOMCODE(String value) {
            this.maxweightconstraintuomcode = value;
        }

        /**
         * Obtém o valor da propriedade maxweightconstraintbase.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMAXWEIGHTCONSTRAINTBASE() {
            return maxweightconstraintbase;
        }

        /**
         * Define o valor da propriedade maxweightconstraintbase.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMAXWEIGHTCONSTRAINTBASE(String value) {
            this.maxweightconstraintbase = value;
        }

        /**
         * Obtém o valor da propriedade minvolumeconstraint.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMINVOLUMECONSTRAINT() {
            return minvolumeconstraint;
        }

        /**
         * Define o valor da propriedade minvolumeconstraint.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMINVOLUMECONSTRAINT(String value) {
            this.minvolumeconstraint = value;
        }

        /**
         * Obtém o valor da propriedade minvolumeconstraintuomcode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMINVOLUMECONSTRAINTUOMCODE() {
            return minvolumeconstraintuomcode;
        }

        /**
         * Define o valor da propriedade minvolumeconstraintuomcode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMINVOLUMECONSTRAINTUOMCODE(String value) {
            this.minvolumeconstraintuomcode = value;
        }

        /**
         * Obtém o valor da propriedade minvolumeconstraintbase.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMINVOLUMECONSTRAINTBASE() {
            return minvolumeconstraintbase;
        }

        /**
         * Define o valor da propriedade minvolumeconstraintbase.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMINVOLUMECONSTRAINTBASE(String value) {
            this.minvolumeconstraintbase = value;
        }

        /**
         * Obtém o valor da propriedade maxvolumeconstraint.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMAXVOLUMECONSTRAINT() {
            return maxvolumeconstraint;
        }

        /**
         * Define o valor da propriedade maxvolumeconstraint.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMAXVOLUMECONSTRAINT(String value) {
            this.maxvolumeconstraint = value;
        }

        /**
         * Obtém o valor da propriedade maxvolumeconstraintuomcode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMAXVOLUMECONSTRAINTUOMCODE() {
            return maxvolumeconstraintuomcode;
        }

        /**
         * Define o valor da propriedade maxvolumeconstraintuomcode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMAXVOLUMECONSTRAINTUOMCODE(String value) {
            this.maxvolumeconstraintuomcode = value;
        }

        /**
         * Obtém o valor da propriedade maxvolumeconstraintbase.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMAXVOLUMECONSTRAINTBASE() {
            return maxvolumeconstraintbase;
        }

        /**
         * Define o valor da propriedade maxvolumeconstraintbase.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMAXVOLUMECONSTRAINTBASE(String value) {
            this.maxvolumeconstraintbase = value;
        }

        /**
         * Obtém o valor da propriedade maxshipunitweight.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMAXSHIPUNITWEIGHT() {
            return maxshipunitweight;
        }

        /**
         * Define o valor da propriedade maxshipunitweight.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMAXSHIPUNITWEIGHT(String value) {
            this.maxshipunitweight = value;
        }

        /**
         * Obtém o valor da propriedade maxshipunitweightuomcode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMAXSHIPUNITWEIGHTUOMCODE() {
            return maxshipunitweightuomcode;
        }

        /**
         * Define o valor da propriedade maxshipunitweightuomcode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMAXSHIPUNITWEIGHTUOMCODE(String value) {
            this.maxshipunitweightuomcode = value;
        }

        /**
         * Obtém o valor da propriedade maxshipunitweightbase.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMAXSHIPUNITWEIGHTBASE() {
            return maxshipunitweightbase;
        }

        /**
         * Define o valor da propriedade maxshipunitweightbase.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMAXSHIPUNITWEIGHTBASE(String value) {
            this.maxshipunitweightbase = value;
        }

        /**
         * Obtém o valor da propriedade tariffname.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTARIFFNAME() {
            return tariffname;
        }

        /**
         * Define o valor da propriedade tariffname.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTARIFFNAME(String value) {
            this.tariffname = value;
        }

        /**
         * Obtém o valor da propriedade mincost.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMINCOST() {
            return mincost;
        }

        /**
         * Define o valor da propriedade mincost.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMINCOST(String value) {
            this.mincost = value;
        }

        /**
         * Obtém o valor da propriedade mincostgid.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMINCOSTGID() {
            return mincostgid;
        }

        /**
         * Define o valor da propriedade mincostgid.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMINCOSTGID(String value) {
            this.mincostgid = value;
        }

        /**
         * Obtém o valor da propriedade mincostbase.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMINCOSTBASE() {
            return mincostbase;
        }

        /**
         * Define o valor da propriedade mincostbase.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMINCOSTBASE(String value) {
            this.mincostbase = value;
        }

        /**
         * Obtém o valor da propriedade ratedistancegid.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRATEDISTANCEGID() {
            return ratedistancegid;
        }

        /**
         * Define o valor da propriedade ratedistancegid.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRATEDISTANCEGID(String value) {
            this.ratedistancegid = value;
        }

        /**
         * Obtém o valor da propriedade ratequalitygid.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRATEQUALITYGID() {
            return ratequalitygid;
        }

        /**
         * Define o valor da propriedade ratequalitygid.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRATEQUALITYGID(String value) {
            this.ratequalitygid = value;
        }

        /**
         * Obtém o valor da propriedade rateclassificationgid.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRATECLASSIFICATIONGID() {
            return rateclassificationgid;
        }

        /**
         * Define o valor da propriedade rateclassificationgid.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRATECLASSIFICATIONGID(String value) {
            this.rateclassificationgid = value;
        }

        /**
         * Obtém o valor da propriedade externalratingenginegid.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEXTERNALRATINGENGINEGID() {
            return externalratingenginegid;
        }

        /**
         * Define o valor da propriedade externalratingenginegid.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEXTERNALRATINGENGINEGID(String value) {
            this.externalratingenginegid = value;
        }

        /**
         * Obtém o valor da propriedade circuityallowancepercent.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCIRCUITYALLOWANCEPERCENT() {
            return circuityallowancepercent;
        }

        /**
         * Define o valor da propriedade circuityallowancepercent.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCIRCUITYALLOWANCEPERCENT(String value) {
            this.circuityallowancepercent = value;
        }

        /**
         * Obtém o valor da propriedade totalstopsconstraint.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTOTALSTOPSCONSTRAINT() {
            return totalstopsconstraint;
        }

        /**
         * Define o valor da propriedade totalstopsconstraint.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTOTALSTOPSCONSTRAINT(String value) {
            this.totalstopsconstraint = value;
        }

        /**
         * Obtém o valor da propriedade pickupstopsconstraint.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPICKUPSTOPSCONSTRAINT() {
            return pickupstopsconstraint;
        }

        /**
         * Define o valor da propriedade pickupstopsconstraint.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPICKUPSTOPSCONSTRAINT(String value) {
            this.pickupstopsconstraint = value;
        }

        /**
         * Obtém o valor da propriedade deliverystopsconstraint.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDELIVERYSTOPSCONSTRAINT() {
            return deliverystopsconstraint;
        }

        /**
         * Define o valor da propriedade deliverystopsconstraint.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDELIVERYSTOPSCONSTRAINT(String value) {
            this.deliverystopsconstraint = value;
        }

        /**
         * Obtém o valor da propriedade circuitydistancecost.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCIRCUITYDISTANCECOST() {
            return circuitydistancecost;
        }

        /**
         * Define o valor da propriedade circuitydistancecost.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCIRCUITYDISTANCECOST(String value) {
            this.circuitydistancecost = value;
        }

        /**
         * Obtém o valor da propriedade circuitydistancecostgid.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCIRCUITYDISTANCECOSTGID() {
            return circuitydistancecostgid;
        }

        /**
         * Define o valor da propriedade circuitydistancecostgid.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCIRCUITYDISTANCECOSTGID(String value) {
            this.circuitydistancecostgid = value;
        }

        /**
         * Obtém o valor da propriedade circuitydistancecostbase.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCIRCUITYDISTANCECOSTBASE() {
            return circuitydistancecostbase;
        }

        /**
         * Define o valor da propriedade circuitydistancecostbase.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCIRCUITYDISTANCECOSTBASE(String value) {
            this.circuitydistancecostbase = value;
        }

        /**
         * Obtém o valor da propriedade maxcircuitypercent.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMAXCIRCUITYPERCENT() {
            return maxcircuitypercent;
        }

        /**
         * Define o valor da propriedade maxcircuitypercent.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMAXCIRCUITYPERCENT(String value) {
            this.maxcircuitypercent = value;
        }

        /**
         * Obtém o valor da propriedade maxcircuitydistance.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMAXCIRCUITYDISTANCE() {
            return maxcircuitydistance;
        }

        /**
         * Define o valor da propriedade maxcircuitydistance.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMAXCIRCUITYDISTANCE(String value) {
            this.maxcircuitydistance = value;
        }

        /**
         * Obtém o valor da propriedade maxcircuitydistanceuomcode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMAXCIRCUITYDISTANCEUOMCODE() {
            return maxcircuitydistanceuomcode;
        }

        /**
         * Define o valor da propriedade maxcircuitydistanceuomcode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMAXCIRCUITYDISTANCEUOMCODE(String value) {
            this.maxcircuitydistanceuomcode = value;
        }

        /**
         * Obtém o valor da propriedade maxcircuitydistancebase.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMAXCIRCUITYDISTANCEBASE() {
            return maxcircuitydistancebase;
        }

        /**
         * Define o valor da propriedade maxcircuitydistancebase.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMAXCIRCUITYDISTANCEBASE(String value) {
            this.maxcircuitydistancebase = value;
        }

        /**
         * Obtém o valor da propriedade stopsincludedinrate.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSTOPSINCLUDEDINRATE() {
            return stopsincludedinrate;
        }

        /**
         * Define o valor da propriedade stopsincludedinrate.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSTOPSINCLUDEDINRATE(String value) {
            this.stopsincludedinrate = value;
        }

        /**
         * Obtém o valor da propriedade flexcommodityprofilegid.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFLEXCOMMODITYPROFILEGID() {
            return flexcommodityprofilegid;
        }

        /**
         * Define o valor da propriedade flexcommodityprofilegid.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFLEXCOMMODITYPROFILEGID(String value) {
            this.flexcommodityprofilegid = value;
        }

        /**
         * Obtém o valor da propriedade shipperminvalue.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSHIPPERMINVALUE() {
            return shipperminvalue;
        }

        /**
         * Define o valor da propriedade shipperminvalue.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSHIPPERMINVALUE(String value) {
            this.shipperminvalue = value;
        }

        /**
         * Obtém o valor da propriedade shipperminvaluegid.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSHIPPERMINVALUEGID() {
            return shipperminvaluegid;
        }

        /**
         * Define o valor da propriedade shipperminvaluegid.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSHIPPERMINVALUEGID(String value) {
            this.shipperminvaluegid = value;
        }

        /**
         * Obtém o valor da propriedade shipperminvaluebase.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSHIPPERMINVALUEBASE() {
            return shipperminvaluebase;
        }

        /**
         * Define o valor da propriedade shipperminvaluebase.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSHIPPERMINVALUEBASE(String value) {
            this.shipperminvaluebase = value;
        }

        /**
         * Obtém o valor da propriedade minshipunitspecgid.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMINSHIPUNITSPECGID() {
            return minshipunitspecgid;
        }

        /**
         * Define o valor da propriedade minshipunitspecgid.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMINSHIPUNITSPECGID(String value) {
            this.minshipunitspecgid = value;
        }

        /**
         * Obtém o valor da propriedade exchangerategid.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEXCHANGERATEGID() {
            return exchangerategid;
        }

        /**
         * Define o valor da propriedade exchangerategid.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEXCHANGERATEGID(String value) {
            this.exchangerategid = value;
        }

        /**
         * Obtém o valor da propriedade domainname.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDOMAINNAME() {
            return domainname;
        }

        /**
         * Define o valor da propriedade domainname.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDOMAINNAME(String value) {
            this.domainname = value;
        }

        /**
         * Obtém o valor da propriedade commodityusage.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCOMMODITYUSAGE() {
            return commodityusage;
        }

        /**
         * Define o valor da propriedade commodityusage.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCOMMODITYUSAGE(String value) {
            this.commodityusage = value;
        }

        /**
         * Obtém o valor da propriedade fakrateas.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFAKRATEAS() {
            return fakrateas;
        }

        /**
         * Define o valor da propriedade fakrateas.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFAKRATEAS(String value) {
            this.fakrateas = value;
        }

        /**
         * Obtém o valor da propriedade fakflexcommodityvalue.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFAKFLEXCOMMODITYVALUE() {
            return fakflexcommodityvalue;
        }

        /**
         * Define o valor da propriedade fakflexcommodityvalue.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFAKFLEXCOMMODITYVALUE(String value) {
            this.fakflexcommodityvalue = value;
        }

        /**
         * Obtém o valor da propriedade rmabsolutemincost.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRMABSOLUTEMINCOST() {
            return rmabsolutemincost;
        }

        /**
         * Define o valor da propriedade rmabsolutemincost.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRMABSOLUTEMINCOST(String value) {
            this.rmabsolutemincost = value;
        }

        /**
         * Obtém o valor da propriedade rmabsolutemincostgid.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRMABSOLUTEMINCOSTGID() {
            return rmabsolutemincostgid;
        }

        /**
         * Define o valor da propriedade rmabsolutemincostgid.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRMABSOLUTEMINCOSTGID(String value) {
            this.rmabsolutemincostgid = value;
        }

        /**
         * Obtém o valor da propriedade rmabsolutemincostbase.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRMABSOLUTEMINCOSTBASE() {
            return rmabsolutemincostbase;
        }

        /**
         * Define o valor da propriedade rmabsolutemincostbase.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRMABSOLUTEMINCOSTBASE(String value) {
            this.rmabsolutemincostbase = value;
        }

        /**
         * Obtém o valor da propriedade maxcost.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMAXCOST() {
            return maxcost;
        }

        /**
         * Define o valor da propriedade maxcost.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMAXCOST(String value) {
            this.maxcost = value;
        }

        /**
         * Obtém o valor da propriedade maxcostgid.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMAXCOSTGID() {
            return maxcostgid;
        }

        /**
         * Define o valor da propriedade maxcostgid.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMAXCOSTGID(String value) {
            this.maxcostgid = value;
        }

        /**
         * Obtém o valor da propriedade maxcostbase.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMAXCOSTBASE() {
            return maxcostbase;
        }

        /**
         * Define o valor da propriedade maxcostbase.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMAXCOSTBASE(String value) {
            this.maxcostbase = value;
        }

        /**
         * Obtém o valor da propriedade minstops.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMINSTOPS() {
            return minstops;
        }

        /**
         * Define o valor da propriedade minstops.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMINSTOPS(String value) {
            this.minstops = value;
        }

        /**
         * Obtém o valor da propriedade shortlinecost.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSHORTLINECOST() {
            return shortlinecost;
        }

        /**
         * Define o valor da propriedade shortlinecost.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSHORTLINECOST(String value) {
            this.shortlinecost = value;
        }

        /**
         * Obtém o valor da propriedade shortlinecostgid.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSHORTLINECOSTGID() {
            return shortlinecostgid;
        }

        /**
         * Define o valor da propriedade shortlinecostgid.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSHORTLINECOSTGID(String value) {
            this.shortlinecostgid = value;
        }

        /**
         * Obtém o valor da propriedade shortlinecostbase.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSHORTLINECOSTBASE() {
            return shortlinecostbase;
        }

        /**
         * Define o valor da propriedade shortlinecostbase.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSHORTLINECOSTBASE(String value) {
            this.shortlinecostbase = value;
        }

        /**
         * Obtém o valor da propriedade minlengthconstraint.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMINLENGTHCONSTRAINT() {
            return minlengthconstraint;
        }

        /**
         * Define o valor da propriedade minlengthconstraint.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMINLENGTHCONSTRAINT(String value) {
            this.minlengthconstraint = value;
        }

        /**
         * Obtém o valor da propriedade minlengthconstraintuomcode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMINLENGTHCONSTRAINTUOMCODE() {
            return minlengthconstraintuomcode;
        }

        /**
         * Define o valor da propriedade minlengthconstraintuomcode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMINLENGTHCONSTRAINTUOMCODE(String value) {
            this.minlengthconstraintuomcode = value;
        }

        /**
         * Obtém o valor da propriedade minlengthconstraintbase.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMINLENGTHCONSTRAINTBASE() {
            return minlengthconstraintbase;
        }

        /**
         * Define o valor da propriedade minlengthconstraintbase.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMINLENGTHCONSTRAINTBASE(String value) {
            this.minlengthconstraintbase = value;
        }

        /**
         * Obtém o valor da propriedade maxlengthconstraint.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMAXLENGTHCONSTRAINT() {
            return maxlengthconstraint;
        }

        /**
         * Define o valor da propriedade maxlengthconstraint.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMAXLENGTHCONSTRAINT(String value) {
            this.maxlengthconstraint = value;
        }

        /**
         * Obtém o valor da propriedade maxlengthconstraintuomcode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMAXLENGTHCONSTRAINTUOMCODE() {
            return maxlengthconstraintuomcode;
        }

        /**
         * Define o valor da propriedade maxlengthconstraintuomcode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMAXLENGTHCONSTRAINTUOMCODE(String value) {
            this.maxlengthconstraintuomcode = value;
        }

        /**
         * Obtém o valor da propriedade maxlengthconstraintbase.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMAXLENGTHCONSTRAINTBASE() {
            return maxlengthconstraintbase;
        }

        /**
         * Define o valor da propriedade maxlengthconstraintbase.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMAXLENGTHCONSTRAINTBASE(String value) {
            this.maxlengthconstraintbase = value;
        }

        /**
         * Obtém o valor da propriedade minwidthconstraint.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMINWIDTHCONSTRAINT() {
            return minwidthconstraint;
        }

        /**
         * Define o valor da propriedade minwidthconstraint.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMINWIDTHCONSTRAINT(String value) {
            this.minwidthconstraint = value;
        }

        /**
         * Obtém o valor da propriedade minwidthconstraintuomcode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMINWIDTHCONSTRAINTUOMCODE() {
            return minwidthconstraintuomcode;
        }

        /**
         * Define o valor da propriedade minwidthconstraintuomcode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMINWIDTHCONSTRAINTUOMCODE(String value) {
            this.minwidthconstraintuomcode = value;
        }

        /**
         * Obtém o valor da propriedade minwidthconstraintbase.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMINWIDTHCONSTRAINTBASE() {
            return minwidthconstraintbase;
        }

        /**
         * Define o valor da propriedade minwidthconstraintbase.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMINWIDTHCONSTRAINTBASE(String value) {
            this.minwidthconstraintbase = value;
        }

        /**
         * Obtém o valor da propriedade maxwidthconstraint.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMAXWIDTHCONSTRAINT() {
            return maxwidthconstraint;
        }

        /**
         * Define o valor da propriedade maxwidthconstraint.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMAXWIDTHCONSTRAINT(String value) {
            this.maxwidthconstraint = value;
        }

        /**
         * Obtém o valor da propriedade maxwidthconstraintuomcode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMAXWIDTHCONSTRAINTUOMCODE() {
            return maxwidthconstraintuomcode;
        }

        /**
         * Define o valor da propriedade maxwidthconstraintuomcode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMAXWIDTHCONSTRAINTUOMCODE(String value) {
            this.maxwidthconstraintuomcode = value;
        }

        /**
         * Obtém o valor da propriedade maxwidthconstraintbase.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMAXWIDTHCONSTRAINTBASE() {
            return maxwidthconstraintbase;
        }

        /**
         * Define o valor da propriedade maxwidthconstraintbase.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMAXWIDTHCONSTRAINTBASE(String value) {
            this.maxwidthconstraintbase = value;
        }

        /**
         * Obtém o valor da propriedade minheightconstraint.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMINHEIGHTCONSTRAINT() {
            return minheightconstraint;
        }

        /**
         * Define o valor da propriedade minheightconstraint.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMINHEIGHTCONSTRAINT(String value) {
            this.minheightconstraint = value;
        }

        /**
         * Obtém o valor da propriedade minheightconstraintuomcode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMINHEIGHTCONSTRAINTUOMCODE() {
            return minheightconstraintuomcode;
        }

        /**
         * Define o valor da propriedade minheightconstraintuomcode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMINHEIGHTCONSTRAINTUOMCODE(String value) {
            this.minheightconstraintuomcode = value;
        }

        /**
         * Obtém o valor da propriedade minheightconstraintbase.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMINHEIGHTCONSTRAINTBASE() {
            return minheightconstraintbase;
        }

        /**
         * Define o valor da propriedade minheightconstraintbase.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMINHEIGHTCONSTRAINTBASE(String value) {
            this.minheightconstraintbase = value;
        }

        /**
         * Obtém o valor da propriedade maxheightconstraint.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMAXHEIGHTCONSTRAINT() {
            return maxheightconstraint;
        }

        /**
         * Define o valor da propriedade maxheightconstraint.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMAXHEIGHTCONSTRAINT(String value) {
            this.maxheightconstraint = value;
        }

        /**
         * Obtém o valor da propriedade maxheightconstraintuomcode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMAXHEIGHTCONSTRAINTUOMCODE() {
            return maxheightconstraintuomcode;
        }

        /**
         * Define o valor da propriedade maxheightconstraintuomcode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMAXHEIGHTCONSTRAINTUOMCODE(String value) {
            this.maxheightconstraintuomcode = value;
        }

        /**
         * Obtém o valor da propriedade maxheightconstraintbase.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMAXHEIGHTCONSTRAINTBASE() {
            return maxheightconstraintbase;
        }

        /**
         * Define o valor da propriedade maxheightconstraintbase.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMAXHEIGHTCONSTRAINTBASE(String value) {
            this.maxheightconstraintbase = value;
        }

        /**
         * Obtém o valor da propriedade mingirthconstraint.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMINGIRTHCONSTRAINT() {
            return mingirthconstraint;
        }

        /**
         * Define o valor da propriedade mingirthconstraint.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMINGIRTHCONSTRAINT(String value) {
            this.mingirthconstraint = value;
        }

        /**
         * Obtém o valor da propriedade mingirthconstraintuomcode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMINGIRTHCONSTRAINTUOMCODE() {
            return mingirthconstraintuomcode;
        }

        /**
         * Define o valor da propriedade mingirthconstraintuomcode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMINGIRTHCONSTRAINTUOMCODE(String value) {
            this.mingirthconstraintuomcode = value;
        }

        /**
         * Obtém o valor da propriedade mingirthconstraintbase.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMINGIRTHCONSTRAINTBASE() {
            return mingirthconstraintbase;
        }

        /**
         * Define o valor da propriedade mingirthconstraintbase.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMINGIRTHCONSTRAINTBASE(String value) {
            this.mingirthconstraintbase = value;
        }

        /**
         * Obtém o valor da propriedade maxgirthconstraint.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMAXGIRTHCONSTRAINT() {
            return maxgirthconstraint;
        }

        /**
         * Define o valor da propriedade maxgirthconstraint.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMAXGIRTHCONSTRAINT(String value) {
            this.maxgirthconstraint = value;
        }

        /**
         * Obtém o valor da propriedade maxgirthconstraintuomcode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMAXGIRTHCONSTRAINTUOMCODE() {
            return maxgirthconstraintuomcode;
        }

        /**
         * Define o valor da propriedade maxgirthconstraintuomcode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMAXGIRTHCONSTRAINTUOMCODE(String value) {
            this.maxgirthconstraintuomcode = value;
        }

        /**
         * Obtém o valor da propriedade maxgirthconstraintbase.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMAXGIRTHCONSTRAINTBASE() {
            return maxgirthconstraintbase;
        }

        /**
         * Define o valor da propriedade maxgirthconstraintbase.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMAXGIRTHCONSTRAINTBASE(String value) {
            this.maxgirthconstraintbase = value;
        }

        /**
         * Obtém o valor da propriedade weightbreakprofilegid.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getWEIGHTBREAKPROFILEGID() {
            return weightbreakprofilegid;
        }

        /**
         * Define o valor da propriedade weightbreakprofilegid.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setWEIGHTBREAKPROFILEGID(String value) {
            this.weightbreakprofilegid = value;
        }

        /**
         * Obtém o valor da propriedade multistopcostmethod.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMULTISTOPCOSTMETHOD() {
            return multistopcostmethod;
        }

        /**
         * Define o valor da propriedade multistopcostmethod.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMULTISTOPCOSTMETHOD(String value) {
            this.multistopcostmethod = value;
        }

        /**
         * Obtém o valor da propriedade weightuomcode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getWEIGHTUOMCODE() {
            return weightuomcode;
        }

        /**
         * Define o valor da propriedade weightuomcode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setWEIGHTUOMCODE(String value) {
            this.weightuomcode = value;
        }

        /**
         * Obtém o valor da propriedade volumeuomcode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getVOLUMEUOMCODE() {
            return volumeuomcode;
        }

        /**
         * Define o valor da propriedade volumeuomcode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setVOLUMEUOMCODE(String value) {
            this.volumeuomcode = value;
        }

        /**
         * Obtém o valor da propriedade distanceuomcode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDISTANCEUOMCODE() {
            return distanceuomcode;
        }

        /**
         * Define o valor da propriedade distanceuomcode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDISTANCEUOMCODE(String value) {
            this.distanceuomcode = value;
        }

        /**
         * Obtém o valor da propriedade dimratefactorgid.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDIMRATEFACTORGID() {
            return dimratefactorgid;
        }

        /**
         * Define o valor da propriedade dimratefactorgid.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDIMRATEFACTORGID(String value) {
            this.dimratefactorgid = value;
        }

        /**
         * Obtém o valor da propriedade perspective.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPERSPECTIVE() {
            return perspective;
        }

        /**
         * Define o valor da propriedade perspective.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPERSPECTIVE(String value) {
            this.perspective = value;
        }

        /**
         * Obtém o valor da propriedade corporationprofilegid.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCORPORATIONPROFILEGID() {
            return corporationprofilegid;
        }

        /**
         * Define o valor da propriedade corporationprofilegid.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCORPORATIONPROFILEGID(String value) {
            this.corporationprofilegid = value;
        }

        /**
         * Obtém o valor da propriedade capacitygroupgid.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCAPACITYGROUPGID() {
            return capacitygroupgid;
        }

        /**
         * Define o valor da propriedade capacitygroupgid.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCAPACITYGROUPGID(String value) {
            this.capacitygroupgid = value;
        }

        /**
         * Obtém o valor da propriedade cmmaxnumshipments.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCMMAXNUMSHIPMENTS() {
            return cmmaxnumshipments;
        }

        /**
         * Define o valor da propriedade cmmaxnumshipments.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCMMAXNUMSHIPMENTS(String value) {
            this.cmmaxnumshipments = value;
        }

        /**
         * Obtém o valor da propriedade cmissameequip.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCMISSAMEEQUIP() {
            return cmissameequip;
        }

        /**
         * Define o valor da propriedade cmissameequip.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCMISSAMEEQUIP(String value) {
            this.cmissameequip = value;
        }

        /**
         * Obtém o valor da propriedade cmprevshipmentpercent.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCMPREVSHIPMENTPERCENT() {
            return cmprevshipmentpercent;
        }

        /**
         * Define o valor da propriedade cmprevshipmentpercent.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCMPREVSHIPMENTPERCENT(String value) {
            this.cmprevshipmentpercent = value;
        }

        /**
         * Obtém o valor da propriedade cmispercentofdistance.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCMISPERCENTOFDISTANCE() {
            return cmispercentofdistance;
        }

        /**
         * Define o valor da propriedade cmispercentofdistance.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCMISPERCENTOFDISTANCE(String value) {
            this.cmispercentofdistance = value;
        }

        /**
         * Obtém o valor da propriedade tariffrefnumqualifier.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTARIFFREFNUMQUALIFIER() {
            return tariffrefnumqualifier;
        }

        /**
         * Define o valor da propriedade tariffrefnumqualifier.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTARIFFREFNUMQUALIFIER(String value) {
            this.tariffrefnumqualifier = value;
        }

        /**
         * Obtém o valor da propriedade tariffrefnum.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTARIFFREFNUM() {
            return tariffrefnum;
        }

        /**
         * Define o valor da propriedade tariffrefnum.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTARIFFREFNUM(String value) {
            this.tariffrefnum = value;
        }

        /**
         * Obtém o valor da propriedade tariffpubauthority.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTARIFFPUBAUTHORITY() {
            return tariffpubauthority;
        }

        /**
         * Define o valor da propriedade tariffpubauthority.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTARIFFPUBAUTHORITY(String value) {
            this.tariffpubauthority = value;
        }

        /**
         * Obtém o valor da propriedade tariffregagencycode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTARIFFREGAGENCYCODE() {
            return tariffregagencycode;
        }

        /**
         * Define o valor da propriedade tariffregagencycode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTARIFFREGAGENCYCODE(String value) {
            this.tariffregagencycode = value;
        }

        /**
         * Obtém o valor da propriedade tariffagencycode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTARIFFAGENCYCODE() {
            return tariffagencycode;
        }

        /**
         * Define o valor da propriedade tariffagencycode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTARIFFAGENCYCODE(String value) {
            this.tariffagencycode = value;
        }

        /**
         * Obtém o valor da propriedade tariffissuingcarrierid.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTARIFFISSUINGCARRIERID() {
            return tariffissuingcarrierid;
        }

        /**
         * Define o valor da propriedade tariffissuingcarrierid.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTARIFFISSUINGCARRIERID(String value) {
            this.tariffissuingcarrierid = value;
        }

        /**
         * Obtém o valor da propriedade tariffrefnumsuffix.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTARIFFREFNUMSUFFIX() {
            return tariffrefnumsuffix;
        }

        /**
         * Define o valor da propriedade tariffrefnumsuffix.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTARIFFREFNUMSUFFIX(String value) {
            this.tariffrefnumsuffix = value;
        }

        /**
         * Obtém o valor da propriedade tariffsupplementid.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTARIFFSUPPLEMENTID() {
            return tariffsupplementid;
        }

        /**
         * Define o valor da propriedade tariffsupplementid.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTARIFFSUPPLEMENTID(String value) {
            this.tariffsupplementid = value;
        }

        /**
         * Obtém o valor da propriedade tariffeffectivedate.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTARIFFEFFECTIVEDATE() {
            return tariffeffectivedate;
        }

        /**
         * Define o valor da propriedade tariffeffectivedate.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTARIFFEFFECTIVEDATE(String value) {
            this.tariffeffectivedate = value;
        }

        /**
         * Obtém o valor da propriedade allowuncostedlineitems.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getALLOWUNCOSTEDLINEITEMS() {
            return allowuncostedlineitems;
        }

        /**
         * Define o valor da propriedade allowuncostedlineitems.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setALLOWUNCOSTEDLINEITEMS(String value) {
            this.allowuncostedlineitems = value;
        }

        /**
         * Obtém o valor da propriedade railintermodalplangid.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRAILINTERMODALPLANGID() {
            return railintermodalplangid;
        }

        /**
         * Define o valor da propriedade railintermodalplangid.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRAILINTERMODALPLANGID(String value) {
            this.railintermodalplangid = value;
        }

        /**
         * Obtém o valor da propriedade customerratecode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCUSTOMERRATECODE() {
            return customerratecode;
        }

        /**
         * Define o valor da propriedade customerratecode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCUSTOMERRATECODE(String value) {
            this.customerratecode = value;
        }

        /**
         * Obtém o valor da propriedade cofctofc.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCOFCTOFC() {
            return cofctofc;
        }

        /**
         * Define o valor da propriedade cofctofc.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCOFCTOFC(String value) {
            this.cofctofc = value;
        }

        /**
         * Obtém o valor da propriedade isactive.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getISACTIVE() {
            return isactive;
        }

        /**
         * Define o valor da propriedade isactive.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setISACTIVE(String value) {
            this.isactive = value;
        }

        /**
         * Obtém o valor da propriedade handlesunknownshipper.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getHANDLESUNKNOWNSHIPPER() {
            return handlesunknownshipper;
        }

        /**
         * Define o valor da propriedade handlesunknownshipper.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setHANDLESUNKNOWNSHIPPER(String value) {
            this.handlesunknownshipper = value;
        }

        /**
         * Obtém o valor da propriedade usestimebasedrates.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getUSESTIMEBASEDRATES() {
            return usestimebasedrates;
        }

        /**
         * Define o valor da propriedade usestimebasedrates.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setUSESTIMEBASEDRATES(String value) {
            this.usestimebasedrates = value;
        }

        /**
         * Obtém o valor da propriedade expiremarkid.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEXPIREMARKID() {
            return expiremarkid;
        }

        /**
         * Define o valor da propriedade expiremarkid.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEXPIREMARKID(String value) {
            this.expiremarkid = value;
        }

        /**
         * Obtém o valor da propriedade mindistanceconstraint.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMINDISTANCECONSTRAINT() {
            return mindistanceconstraint;
        }

        /**
         * Define o valor da propriedade mindistanceconstraint.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMINDISTANCECONSTRAINT(String value) {
            this.mindistanceconstraint = value;
        }

        /**
         * Obtém o valor da propriedade mindistanceconstrntuomcode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMINDISTANCECONSTRNTUOMCODE() {
            return mindistanceconstrntuomcode;
        }

        /**
         * Define o valor da propriedade mindistanceconstrntuomcode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMINDISTANCECONSTRNTUOMCODE(String value) {
            this.mindistanceconstrntuomcode = value;
        }

        /**
         * Obtém o valor da propriedade mindistanceconstraintbase.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMINDISTANCECONSTRAINTBASE() {
            return mindistanceconstraintbase;
        }

        /**
         * Define o valor da propriedade mindistanceconstraintbase.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMINDISTANCECONSTRAINTBASE(String value) {
            this.mindistanceconstraintbase = value;
        }

        /**
         * Obtém o valor da propriedade regiongroupgid.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getREGIONGROUPGID() {
            return regiongroupgid;
        }

        /**
         * Define o valor da propriedade regiongroupgid.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setREGIONGROUPGID(String value) {
            this.regiongroupgid = value;
        }

        /**
         * Obtém o valor da propriedade isdepotapplicable.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getISDEPOTAPPLICABLE() {
            return isdepotapplicable;
        }

        /**
         * Define o valor da propriedade isdepotapplicable.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setISDEPOTAPPLICABLE(String value) {
            this.isdepotapplicable = value;
        }

        /**
         * Obtém o valor da propriedade recalculatecost.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRECALCULATECOST() {
            return recalculatecost;
        }

        /**
         * Define o valor da propriedade recalculatecost.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRECALCULATECOST(String value) {
            this.recalculatecost = value;
        }

        /**
         * Obtém o valor da propriedade trackcapacityusage.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTRACKCAPACITYUSAGE() {
            return trackcapacityusage;
        }

        /**
         * Define o valor da propriedade trackcapacityusage.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTRACKCAPACITYUSAGE(String value) {
            this.trackcapacityusage = value;
        }

        /**
         * Obtém o valor da propriedade roundingtype.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getROUNDINGTYPE() {
            return roundingtype;
        }

        /**
         * Define o valor da propriedade roundingtype.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setROUNDINGTYPE(String value) {
            this.roundingtype = value;
        }

        /**
         * Obtém o valor da propriedade roundinginterval.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getROUNDINGINTERVAL() {
            return roundinginterval;
        }

        /**
         * Define o valor da propriedade roundinginterval.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setROUNDINGINTERVAL(String value) {
            this.roundinginterval = value;
        }

        /**
         * Obtém o valor da propriedade roundingfieldslevel.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getROUNDINGFIELDSLEVEL() {
            return roundingfieldslevel;
        }

        /**
         * Define o valor da propriedade roundingfieldslevel.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setROUNDINGFIELDSLEVEL(String value) {
            this.roundingfieldslevel = value;
        }

        /**
         * Obtém o valor da propriedade roundingapplication.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getROUNDINGAPPLICATION() {
            return roundingapplication;
        }

        /**
         * Define o valor da propriedade roundingapplication.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setROUNDINGAPPLICATION(String value) {
            this.roundingapplication = value;
        }

        /**
         * Obtém o valor da propriedade iscontractrate.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getISCONTRACTRATE() {
            return iscontractrate;
        }

        /**
         * Define o valor da propriedade iscontractrate.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setISCONTRACTRATE(String value) {
            this.iscontractrate = value;
        }

        /**
         * Obtém o valor da propriedade isdirectonlyrate.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getISDIRECTONLYRATE() {
            return isdirectonlyrate;
        }

        /**
         * Define o valor da propriedade isdirectonlyrate.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setISDIRECTONLYRATE(String value) {
            this.isdirectonlyrate = value;
        }

        /**
         * Obtém o valor da propriedade hazardousratetype.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getHAZARDOUSRATETYPE() {
            return hazardousratetype;
        }

        /**
         * Define o valor da propriedade hazardousratetype.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setHAZARDOUSRATETYPE(String value) {
            this.hazardousratetype = value;
        }

        /**
         * Obtém o valor da propriedade usetactasdisplayrate1.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getUSETACTASDISPLAYRATE1() {
            return usetactasdisplayrate1;
        }

        /**
         * Define o valor da propriedade usetactasdisplayrate1.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setUSETACTASDISPLAYRATE1(String value) {
            this.usetactasdisplayrate1 = value;
        }

        /**
         * Obtém o valor da propriedade usetactasdisplayrate2.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getUSETACTASDISPLAYRATE2() {
            return usetactasdisplayrate2;
        }

        /**
         * Define o valor da propriedade usetactasdisplayrate2.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setUSETACTASDISPLAYRATE2(String value) {
            this.usetactasdisplayrate2 = value;
        }

        /**
         * Obtém o valor da propriedade usetactasdisplayrate3.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getUSETACTASDISPLAYRATE3() {
            return usetactasdisplayrate3;
        }

        /**
         * Define o valor da propriedade usetactasdisplayrate3.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setUSETACTASDISPLAYRATE3(String value) {
            this.usetactasdisplayrate3 = value;
        }

        /**
         * Obtém o valor da propriedade domainprofilegid.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDOMAINPROFILEGID() {
            return domainprofilegid;
        }

        /**
         * Define o valor da propriedade domainprofilegid.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDOMAINPROFILEGID(String value) {
            this.domainprofilegid = value;
        }

        /**
         * Obtém o valor da propriedade isrouteexecutionrate.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getISROUTEEXECUTIONRATE() {
            return isrouteexecutionrate;
        }

        /**
         * Define o valor da propriedade isrouteexecutionrate.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setISROUTEEXECUTIONRATE(String value) {
            this.isrouteexecutionrate = value;
        }

        /**
         * Obtém o valor da propriedade mintenderleadtime.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMINTENDERLEADTIME() {
            return mintenderleadtime;
        }

        /**
         * Define o valor da propriedade mintenderleadtime.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMINTENDERLEADTIME(String value) {
            this.mintenderleadtime = value;
        }

        /**
         * Obtém o valor da propriedade mintenderleadtimeuomcode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMINTENDERLEADTIMEUOMCODE() {
            return mintenderleadtimeuomcode;
        }

        /**
         * Define o valor da propriedade mintenderleadtimeuomcode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMINTENDERLEADTIMEUOMCODE(String value) {
            this.mintenderleadtimeuomcode = value;
        }

        /**
         * Obtém o valor da propriedade mintenderleadtimebase.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMINTENDERLEADTIMEBASE() {
            return mintenderleadtimebase;
        }

        /**
         * Define o valor da propriedade mintenderleadtimebase.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMINTENDERLEADTIMEBASE(String value) {
            this.mintenderleadtimebase = value;
        }

        /**
         * Obtém o valor da propriedade maxtenderleadtime.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMAXTENDERLEADTIME() {
            return maxtenderleadtime;
        }

        /**
         * Define o valor da propriedade maxtenderleadtime.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMAXTENDERLEADTIME(String value) {
            this.maxtenderleadtime = value;
        }

        /**
         * Obtém o valor da propriedade maxtenderleadtimeuomcode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMAXTENDERLEADTIMEUOMCODE() {
            return maxtenderleadtimeuomcode;
        }

        /**
         * Define o valor da propriedade maxtenderleadtimeuomcode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMAXTENDERLEADTIMEUOMCODE(String value) {
            this.maxtenderleadtimeuomcode = value;
        }

        /**
         * Obtém o valor da propriedade maxtenderleadtimebase.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMAXTENDERLEADTIMEBASE() {
            return maxtenderleadtimebase;
        }

        /**
         * Define o valor da propriedade maxtenderleadtimebase.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMAXTENDERLEADTIMEBASE(String value) {
            this.maxtenderleadtimebase = value;
        }

        /**
         * Obtém o valor da propriedade packageweightmin.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPACKAGEWEIGHTMIN() {
            return packageweightmin;
        }

        /**
         * Define o valor da propriedade packageweightmin.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPACKAGEWEIGHTMIN(String value) {
            this.packageweightmin = value;
        }

        /**
         * Obtém o valor da propriedade packageweightminuomcode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPACKAGEWEIGHTMINUOMCODE() {
            return packageweightminuomcode;
        }

        /**
         * Define o valor da propriedade packageweightminuomcode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPACKAGEWEIGHTMINUOMCODE(String value) {
            this.packageweightminuomcode = value;
        }

        /**
         * Obtém o valor da propriedade packageweightminbase.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPACKAGEWEIGHTMINBASE() {
            return packageweightminbase;
        }

        /**
         * Define o valor da propriedade packageweightminbase.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPACKAGEWEIGHTMINBASE(String value) {
            this.packageweightminbase = value;
        }

        /**
         * Obtém o valor da propriedade maxaveragepkgweight.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMAXAVERAGEPKGWEIGHT() {
            return maxaveragepkgweight;
        }

        /**
         * Define o valor da propriedade maxaveragepkgweight.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMAXAVERAGEPKGWEIGHT(String value) {
            this.maxaveragepkgweight = value;
        }

        /**
         * Obtém o valor da propriedade maxaveragepkgweighuomcode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMAXAVERAGEPKGWEIGHUOMCODE() {
            return maxaveragepkgweighuomcode;
        }

        /**
         * Define o valor da propriedade maxaveragepkgweighuomcode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMAXAVERAGEPKGWEIGHUOMCODE(String value) {
            this.maxaveragepkgweighuomcode = value;
        }

        /**
         * Obtém o valor da propriedade maxaveragepkgweighbase.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMAXAVERAGEPKGWEIGHBASE() {
            return maxaveragepkgweighbase;
        }

        /**
         * Define o valor da propriedade maxaveragepkgweighbase.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMAXAVERAGEPKGWEIGHBASE(String value) {
            this.maxaveragepkgweighbase = value;
        }

        /**
         * Obtém o valor da propriedade packagecountmethod.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPACKAGECOUNTMETHOD() {
            return packagecountmethod;
        }

        /**
         * Define o valor da propriedade packagecountmethod.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPACKAGECOUNTMETHOD(String value) {
            this.packagecountmethod = value;
        }

        /**
         * Obtém o valor da propriedade maxshipunitlinepkgweight.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMAXSHIPUNITLINEPKGWEIGHT() {
            return maxshipunitlinepkgweight;
        }

        /**
         * Define o valor da propriedade maxshipunitlinepkgweight.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMAXSHIPUNITLINEPKGWEIGHT(String value) {
            this.maxshipunitlinepkgweight = value;
        }

        /**
         * Obtém o valor da propriedade maxshipunitlinepkguomcode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMAXSHIPUNITLINEPKGUOMCODE() {
            return maxshipunitlinepkguomcode;
        }

        /**
         * Define o valor da propriedade maxshipunitlinepkguomcode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMAXSHIPUNITLINEPKGUOMCODE(String value) {
            this.maxshipunitlinepkguomcode = value;
        }

        /**
         * Obtém o valor da propriedade maxshipunitlinepkgbase.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMAXSHIPUNITLINEPKGBASE() {
            return maxshipunitlinepkgbase;
        }

        /**
         * Define o valor da propriedade maxshipunitlinepkgbase.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMAXSHIPUNITLINEPKGBASE(String value) {
            this.maxshipunitlinepkgbase = value;
        }

        /**
         * Obtém o valor da propriedade maxnumofshipmentsegments.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMAXNUMOFSHIPMENTSEGMENTS() {
            return maxnumofshipmentsegments;
        }

        /**
         * Define o valor da propriedade maxnumofshipmentsegments.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMAXNUMOFSHIPMENTSEGMENTS(String value) {
            this.maxnumofshipmentsegments = value;
        }

        /**
         * Obtém o valor da propriedade rotimeperioddefgid.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getROTIMEPERIODDEFGID() {
            return rotimeperioddefgid;
        }

        /**
         * Define o valor da propriedade rotimeperioddefgid.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setROTIMEPERIODDEFGID(String value) {
            this.rotimeperioddefgid = value;
        }

        /**
         * Obtém o valor da propriedade extrefieldsetgid.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEXTREFIELDSETGID() {
            return extrefieldsetgid;
        }

        /**
         * Define o valor da propriedade extrefieldsetgid.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEXTREFIELDSETGID(String value) {
            this.extrefieldsetgid = value;
        }

        /**
         * Obtém o valor da propriedade insertuser.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getINSERTUSER() {
            return insertuser;
        }

        /**
         * Define o valor da propriedade insertuser.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setINSERTUSER(String value) {
            this.insertuser = value;
        }

        /**
         * Obtém o valor da propriedade insertdate.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getINSERTDATE() {
            return insertdate;
        }

        /**
         * Define o valor da propriedade insertdate.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setINSERTDATE(String value) {
            this.insertdate = value;
        }

        /**
         * Obtém o valor da propriedade updateuser.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getUPDATEUSER() {
            return updateuser;
        }

        /**
         * Define o valor da propriedade updateuser.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setUPDATEUSER(String value) {
            this.updateuser = value;
        }

        /**
         * Obtém o valor da propriedade updatedate.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getUPDATEDATE() {
            return updatedate;
        }

        /**
         * Define o valor da propriedade updatedate.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setUPDATEDATE(String value) {
            this.updatedate = value;
        }

        /**
         * Obtém o valor da propriedade rateofferingstops.
         * 
         * @return
         *     possible object is
         *     {@link RateOfferingType.RATEOFFERINGROW.RATEOFFERINGSTOPS }
         *     
         */
        public RateOfferingType.RATEOFFERINGROW.RATEOFFERINGSTOPS getRATEOFFERINGSTOPS() {
            return rateofferingstops;
        }

        /**
         * Define o valor da propriedade rateofferingstops.
         * 
         * @param value
         *     allowed object is
         *     {@link RateOfferingType.RATEOFFERINGROW.RATEOFFERINGSTOPS }
         *     
         */
        public void setRATEOFFERINGSTOPS(RateOfferingType.RATEOFFERINGROW.RATEOFFERINGSTOPS value) {
            this.rateofferingstops = value;
        }

        /**
         * Obtém o valor da propriedade rateofferingaccessorial.
         * 
         * @return
         *     possible object is
         *     {@link RateOfferingType.RATEOFFERINGROW.RATEOFFERINGACCESSORIAL }
         *     
         */
        public RateOfferingType.RATEOFFERINGROW.RATEOFFERINGACCESSORIAL getRATEOFFERINGACCESSORIAL() {
            return rateofferingaccessorial;
        }

        /**
         * Define o valor da propriedade rateofferingaccessorial.
         * 
         * @param value
         *     allowed object is
         *     {@link RateOfferingType.RATEOFFERINGROW.RATEOFFERINGACCESSORIAL }
         *     
         */
        public void setRATEOFFERINGACCESSORIAL(RateOfferingType.RATEOFFERINGROW.RATEOFFERINGACCESSORIAL value) {
            this.rateofferingaccessorial = value;
        }

        /**
         * Obtém o valor da propriedade rateofferingcomment.
         * 
         * @return
         *     possible object is
         *     {@link RateOfferingType.RATEOFFERINGROW.RATEOFFERINGCOMMENT }
         *     
         */
        public RateOfferingType.RATEOFFERINGROW.RATEOFFERINGCOMMENT getRATEOFFERINGCOMMENT() {
            return rateofferingcomment;
        }

        /**
         * Define o valor da propriedade rateofferingcomment.
         * 
         * @param value
         *     allowed object is
         *     {@link RateOfferingType.RATEOFFERINGROW.RATEOFFERINGCOMMENT }
         *     
         */
        public void setRATEOFFERINGCOMMENT(RateOfferingType.RATEOFFERINGROW.RATEOFFERINGCOMMENT value) {
            this.rateofferingcomment = value;
        }

        /**
         * Obtém o valor da propriedade raterulesandterms.
         * 
         * @return
         *     possible object is
         *     {@link RateOfferingType.RATEOFFERINGROW.RATERULESANDTERMS }
         *     
         */
        public RateOfferingType.RATEOFFERINGROW.RATERULESANDTERMS getRATERULESANDTERMS() {
            return raterulesandterms;
        }

        /**
         * Define o valor da propriedade raterulesandterms.
         * 
         * @param value
         *     allowed object is
         *     {@link RateOfferingType.RATEOFFERINGROW.RATERULESANDTERMS }
         *     
         */
        public void setRATERULESANDTERMS(RateOfferingType.RATEOFFERINGROW.RATERULESANDTERMS value) {
            this.raterulesandterms = value;
        }

        /**
         * Obtém o valor da propriedade rospecialserviceaccessorial.
         * 
         * @return
         *     possible object is
         *     {@link RateOfferingType.RATEOFFERINGROW.ROSPECIALSERVICEACCESSORIAL }
         *     
         */
        public RateOfferingType.RATEOFFERINGROW.ROSPECIALSERVICEACCESSORIAL getROSPECIALSERVICEACCESSORIAL() {
            return rospecialserviceaccessorial;
        }

        /**
         * Define o valor da propriedade rospecialserviceaccessorial.
         * 
         * @param value
         *     allowed object is
         *     {@link RateOfferingType.RATEOFFERINGROW.ROSPECIALSERVICEACCESSORIAL }
         *     
         */
        public void setROSPECIALSERVICEACCESSORIAL(RateOfferingType.RATEOFFERINGROW.ROSPECIALSERVICEACCESSORIAL value) {
            this.rospecialserviceaccessorial = value;
        }

        /**
         * Obtém o valor da propriedade rateofferinginvparty.
         * 
         * @return
         *     possible object is
         *     {@link RateOfferingType.RATEOFFERINGROW.RATEOFFERINGINVPARTY }
         *     
         */
        public RateOfferingType.RATEOFFERINGROW.RATEOFFERINGINVPARTY getRATEOFFERINGINVPARTY() {
            return rateofferinginvparty;
        }

        /**
         * Define o valor da propriedade rateofferinginvparty.
         * 
         * @param value
         *     allowed object is
         *     {@link RateOfferingType.RATEOFFERINGROW.RATEOFFERINGINVPARTY }
         *     
         */
        public void setRATEOFFERINGINVPARTY(RateOfferingType.RATEOFFERINGROW.RATEOFFERINGINVPARTY value) {
            this.rateofferinginvparty = value;
        }

        /**
         * Obtém o valor da propriedade attribute1.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE1() {
            return attribute1;
        }

        /**
         * Define o valor da propriedade attribute1.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE1(String value) {
            this.attribute1 = value;
        }

        /**
         * Obtém o valor da propriedade attribute2.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE2() {
            return attribute2;
        }

        /**
         * Define o valor da propriedade attribute2.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE2(String value) {
            this.attribute2 = value;
        }

        /**
         * Obtém o valor da propriedade attribute3.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE3() {
            return attribute3;
        }

        /**
         * Define o valor da propriedade attribute3.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE3(String value) {
            this.attribute3 = value;
        }

        /**
         * Obtém o valor da propriedade attribute4.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE4() {
            return attribute4;
        }

        /**
         * Define o valor da propriedade attribute4.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE4(String value) {
            this.attribute4 = value;
        }

        /**
         * Obtém o valor da propriedade attribute5.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE5() {
            return attribute5;
        }

        /**
         * Define o valor da propriedade attribute5.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE5(String value) {
            this.attribute5 = value;
        }

        /**
         * Obtém o valor da propriedade attribute6.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE6() {
            return attribute6;
        }

        /**
         * Define o valor da propriedade attribute6.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE6(String value) {
            this.attribute6 = value;
        }

        /**
         * Obtém o valor da propriedade attribute7.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE7() {
            return attribute7;
        }

        /**
         * Define o valor da propriedade attribute7.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE7(String value) {
            this.attribute7 = value;
        }

        /**
         * Obtém o valor da propriedade attribute8.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE8() {
            return attribute8;
        }

        /**
         * Define o valor da propriedade attribute8.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE8(String value) {
            this.attribute8 = value;
        }

        /**
         * Obtém o valor da propriedade attribute9.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE9() {
            return attribute9;
        }

        /**
         * Define o valor da propriedade attribute9.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE9(String value) {
            this.attribute9 = value;
        }

        /**
         * Obtém o valor da propriedade attribute10.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE10() {
            return attribute10;
        }

        /**
         * Define o valor da propriedade attribute10.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE10(String value) {
            this.attribute10 = value;
        }

        /**
         * Obtém o valor da propriedade attribute11.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE11() {
            return attribute11;
        }

        /**
         * Define o valor da propriedade attribute11.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE11(String value) {
            this.attribute11 = value;
        }

        /**
         * Obtém o valor da propriedade attribute12.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE12() {
            return attribute12;
        }

        /**
         * Define o valor da propriedade attribute12.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE12(String value) {
            this.attribute12 = value;
        }

        /**
         * Obtém o valor da propriedade attribute13.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE13() {
            return attribute13;
        }

        /**
         * Define o valor da propriedade attribute13.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE13(String value) {
            this.attribute13 = value;
        }

        /**
         * Obtém o valor da propriedade attribute14.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE14() {
            return attribute14;
        }

        /**
         * Define o valor da propriedade attribute14.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE14(String value) {
            this.attribute14 = value;
        }

        /**
         * Obtém o valor da propriedade attribute15.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE15() {
            return attribute15;
        }

        /**
         * Define o valor da propriedade attribute15.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE15(String value) {
            this.attribute15 = value;
        }

        /**
         * Obtém o valor da propriedade attribute16.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE16() {
            return attribute16;
        }

        /**
         * Define o valor da propriedade attribute16.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE16(String value) {
            this.attribute16 = value;
        }

        /**
         * Obtém o valor da propriedade attribute17.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE17() {
            return attribute17;
        }

        /**
         * Define o valor da propriedade attribute17.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE17(String value) {
            this.attribute17 = value;
        }

        /**
         * Obtém o valor da propriedade attribute18.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE18() {
            return attribute18;
        }

        /**
         * Define o valor da propriedade attribute18.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE18(String value) {
            this.attribute18 = value;
        }

        /**
         * Obtém o valor da propriedade attribute19.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE19() {
            return attribute19;
        }

        /**
         * Define o valor da propriedade attribute19.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE19(String value) {
            this.attribute19 = value;
        }

        /**
         * Obtém o valor da propriedade attribute20.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE20() {
            return attribute20;
        }

        /**
         * Define o valor da propriedade attribute20.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE20(String value) {
            this.attribute20 = value;
        }

        /**
         * Obtém o valor da propriedade attributenumber1.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTENUMBER1() {
            return attributenumber1;
        }

        /**
         * Define o valor da propriedade attributenumber1.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTENUMBER1(String value) {
            this.attributenumber1 = value;
        }

        /**
         * Obtém o valor da propriedade attributenumber2.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTENUMBER2() {
            return attributenumber2;
        }

        /**
         * Define o valor da propriedade attributenumber2.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTENUMBER2(String value) {
            this.attributenumber2 = value;
        }

        /**
         * Obtém o valor da propriedade attributenumber3.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTENUMBER3() {
            return attributenumber3;
        }

        /**
         * Define o valor da propriedade attributenumber3.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTENUMBER3(String value) {
            this.attributenumber3 = value;
        }

        /**
         * Obtém o valor da propriedade attributenumber4.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTENUMBER4() {
            return attributenumber4;
        }

        /**
         * Define o valor da propriedade attributenumber4.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTENUMBER4(String value) {
            this.attributenumber4 = value;
        }

        /**
         * Obtém o valor da propriedade attributenumber5.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTENUMBER5() {
            return attributenumber5;
        }

        /**
         * Define o valor da propriedade attributenumber5.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTENUMBER5(String value) {
            this.attributenumber5 = value;
        }

        /**
         * Obtém o valor da propriedade attributenumber6.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTENUMBER6() {
            return attributenumber6;
        }

        /**
         * Define o valor da propriedade attributenumber6.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTENUMBER6(String value) {
            this.attributenumber6 = value;
        }

        /**
         * Obtém o valor da propriedade attributenumber7.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTENUMBER7() {
            return attributenumber7;
        }

        /**
         * Define o valor da propriedade attributenumber7.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTENUMBER7(String value) {
            this.attributenumber7 = value;
        }

        /**
         * Obtém o valor da propriedade attributenumber8.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTENUMBER8() {
            return attributenumber8;
        }

        /**
         * Define o valor da propriedade attributenumber8.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTENUMBER8(String value) {
            this.attributenumber8 = value;
        }

        /**
         * Obtém o valor da propriedade attributenumber9.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTENUMBER9() {
            return attributenumber9;
        }

        /**
         * Define o valor da propriedade attributenumber9.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTENUMBER9(String value) {
            this.attributenumber9 = value;
        }

        /**
         * Obtém o valor da propriedade attributenumber10.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTENUMBER10() {
            return attributenumber10;
        }

        /**
         * Define o valor da propriedade attributenumber10.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTENUMBER10(String value) {
            this.attributenumber10 = value;
        }

        /**
         * Obtém o valor da propriedade attributedate1.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTEDATE1() {
            return attributedate1;
        }

        /**
         * Define o valor da propriedade attributedate1.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTEDATE1(String value) {
            this.attributedate1 = value;
        }

        /**
         * Obtém o valor da propriedade attributedate2.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTEDATE2() {
            return attributedate2;
        }

        /**
         * Define o valor da propriedade attributedate2.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTEDATE2(String value) {
            this.attributedate2 = value;
        }

        /**
         * Obtém o valor da propriedade attributedate3.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTEDATE3() {
            return attributedate3;
        }

        /**
         * Define o valor da propriedade attributedate3.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTEDATE3(String value) {
            this.attributedate3 = value;
        }

        /**
         * Obtém o valor da propriedade attributedate4.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTEDATE4() {
            return attributedate4;
        }

        /**
         * Define o valor da propriedade attributedate4.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTEDATE4(String value) {
            this.attributedate4 = value;
        }

        /**
         * Obtém o valor da propriedade attributedate5.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTEDATE5() {
            return attributedate5;
        }

        /**
         * Define o valor da propriedade attributedate5.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTEDATE5(String value) {
            this.attributedate5 = value;
        }

        /**
         * Obtém o valor da propriedade attributedate6.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTEDATE6() {
            return attributedate6;
        }

        /**
         * Define o valor da propriedade attributedate6.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTEDATE6(String value) {
            this.attributedate6 = value;
        }

        /**
         * Obtém o valor da propriedade attributedate7.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTEDATE7() {
            return attributedate7;
        }

        /**
         * Define o valor da propriedade attributedate7.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTEDATE7(String value) {
            this.attributedate7 = value;
        }

        /**
         * Obtém o valor da propriedade attributedate8.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTEDATE8() {
            return attributedate8;
        }

        /**
         * Define o valor da propriedade attributedate8.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTEDATE8(String value) {
            this.attributedate8 = value;
        }

        /**
         * Obtém o valor da propriedade attributedate9.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTEDATE9() {
            return attributedate9;
        }

        /**
         * Define o valor da propriedade attributedate9.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTEDATE9(String value) {
            this.attributedate9 = value;
        }

        /**
         * Obtém o valor da propriedade attributedate10.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTEDATE10() {
            return attributedate10;
        }

        /**
         * Define o valor da propriedade attributedate10.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTEDATE10(String value) {
            this.attributedate10 = value;
        }

        /**
         * Obtém o valor da propriedade num.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNum() {
            return num;
        }

        /**
         * Define o valor da propriedade num.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNum(String value) {
            this.num = value;
        }


        /**
         * <p>Classe Java de anonymous complex type.
         * 
         * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="RATE_OFFERING_ACCESSORIAL_ROW" maxOccurs="unbounded" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="ACCESSORIAL_COST_GID" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="RATE_OFFERING_GID" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="ACCESSORIAL_CODE_GID" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                   &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                   &lt;element name="ACCESSORIAL_COST" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ACCESSORIAL_COST_TYPE" minOccurs="0"/>
         *                 &lt;/sequence>
         *                 &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" />
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "rateofferingaccessorialrow"
        })
        public static class RATEOFFERINGACCESSORIAL {

            @XmlElement(name = "RATE_OFFERING_ACCESSORIAL_ROW")
            protected List<RateOfferingType.RATEOFFERINGROW.RATEOFFERINGACCESSORIAL.RATEOFFERINGACCESSORIALROW> rateofferingaccessorialrow;

            /**
             * Gets the value of the rateofferingaccessorialrow property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the rateofferingaccessorialrow property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getRATEOFFERINGACCESSORIALROW().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link RateOfferingType.RATEOFFERINGROW.RATEOFFERINGACCESSORIAL.RATEOFFERINGACCESSORIALROW }
             * 
             * 
             */
            public List<RateOfferingType.RATEOFFERINGROW.RATEOFFERINGACCESSORIAL.RATEOFFERINGACCESSORIALROW> getRATEOFFERINGACCESSORIALROW() {
                if (rateofferingaccessorialrow == null) {
                    rateofferingaccessorialrow = new ArrayList<RateOfferingType.RATEOFFERINGROW.RATEOFFERINGACCESSORIAL.RATEOFFERINGACCESSORIALROW>();
                }
                return this.rateofferingaccessorialrow;
            }


            /**
             * <p>Classe Java de anonymous complex type.
             * 
             * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="ACCESSORIAL_COST_GID" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="RATE_OFFERING_GID" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="ACCESSORIAL_CODE_GID" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *         &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *         &lt;element name="ACCESSORIAL_COST" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ACCESSORIAL_COST_TYPE" minOccurs="0"/>
             *       &lt;/sequence>
             *       &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" />
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "accessorialcostgid",
                "rateofferinggid",
                "accessorialcodegid",
                "domainname",
                "insertuser",
                "insertdate",
                "updateuser",
                "updatedate",
                "accessorialcost"
            })
            public static class RATEOFFERINGACCESSORIALROW {

                @XmlElement(name = "ACCESSORIAL_COST_GID", required = true)
                protected String accessorialcostgid;
                @XmlElement(name = "RATE_OFFERING_GID", required = true)
                protected String rateofferinggid;
                @XmlElement(name = "ACCESSORIAL_CODE_GID", required = true)
                protected String accessorialcodegid;
                @XmlElement(name = "DOMAIN_NAME", required = true)
                protected String domainname;
                @XmlElement(name = "INSERT_USER", required = true)
                protected String insertuser;
                @XmlElement(name = "INSERT_DATE", required = true)
                protected String insertdate;
                @XmlElement(name = "UPDATE_USER")
                protected String updateuser;
                @XmlElement(name = "UPDATE_DATE")
                protected String updatedate;
                @XmlElement(name = "ACCESSORIAL_COST")
                protected ACCESSORIALCOSTTYPE accessorialcost;
                @XmlAttribute(name = "num")
                protected String num;

                /**
                 * Obtém o valor da propriedade accessorialcostgid.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getACCESSORIALCOSTGID() {
                    return accessorialcostgid;
                }

                /**
                 * Define o valor da propriedade accessorialcostgid.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setACCESSORIALCOSTGID(String value) {
                    this.accessorialcostgid = value;
                }

                /**
                 * Obtém o valor da propriedade rateofferinggid.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getRATEOFFERINGGID() {
                    return rateofferinggid;
                }

                /**
                 * Define o valor da propriedade rateofferinggid.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setRATEOFFERINGGID(String value) {
                    this.rateofferinggid = value;
                }

                /**
                 * Obtém o valor da propriedade accessorialcodegid.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getACCESSORIALCODEGID() {
                    return accessorialcodegid;
                }

                /**
                 * Define o valor da propriedade accessorialcodegid.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setACCESSORIALCODEGID(String value) {
                    this.accessorialcodegid = value;
                }

                /**
                 * Obtém o valor da propriedade domainname.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDOMAINNAME() {
                    return domainname;
                }

                /**
                 * Define o valor da propriedade domainname.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDOMAINNAME(String value) {
                    this.domainname = value;
                }

                /**
                 * Obtém o valor da propriedade insertuser.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getINSERTUSER() {
                    return insertuser;
                }

                /**
                 * Define o valor da propriedade insertuser.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setINSERTUSER(String value) {
                    this.insertuser = value;
                }

                /**
                 * Obtém o valor da propriedade insertdate.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getINSERTDATE() {
                    return insertdate;
                }

                /**
                 * Define o valor da propriedade insertdate.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setINSERTDATE(String value) {
                    this.insertdate = value;
                }

                /**
                 * Obtém o valor da propriedade updateuser.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getUPDATEUSER() {
                    return updateuser;
                }

                /**
                 * Define o valor da propriedade updateuser.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setUPDATEUSER(String value) {
                    this.updateuser = value;
                }

                /**
                 * Obtém o valor da propriedade updatedate.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getUPDATEDATE() {
                    return updatedate;
                }

                /**
                 * Define o valor da propriedade updatedate.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setUPDATEDATE(String value) {
                    this.updatedate = value;
                }

                /**
                 * Obtém o valor da propriedade accessorialcost.
                 * 
                 * @return
                 *     possible object is
                 *     {@link ACCESSORIALCOSTTYPE }
                 *     
                 */
                public ACCESSORIALCOSTTYPE getACCESSORIALCOST() {
                    return accessorialcost;
                }

                /**
                 * Define o valor da propriedade accessorialcost.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link ACCESSORIALCOSTTYPE }
                 *     
                 */
                public void setACCESSORIALCOST(ACCESSORIALCOSTTYPE value) {
                    this.accessorialcost = value;
                }

                /**
                 * Obtém o valor da propriedade num.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getNum() {
                    return num;
                }

                /**
                 * Define o valor da propriedade num.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setNum(String value) {
                    this.num = value;
                }

            }

        }


        /**
         * <p>Classe Java de anonymous complex type.
         * 
         * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence maxOccurs="unbounded" minOccurs="0">
         *         &lt;element name="RATE_OFFERING_COMMENT_ROW" maxOccurs="unbounded">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="RATE_OFFERING_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                   &lt;element name="COMMENT_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                   &lt;element name="ENTERED_BY" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                   &lt;element name="THE_COMMENT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                   &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                   &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                   &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                   &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                   &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                 &lt;/sequence>
         *                 &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" />
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "rateofferingcommentrow"
        })
        public static class RATEOFFERINGCOMMENT {

            @XmlElement(name = "RATE_OFFERING_COMMENT_ROW")
            protected List<RateOfferingType.RATEOFFERINGROW.RATEOFFERINGCOMMENT.RATEOFFERINGCOMMENTROW> rateofferingcommentrow;

            /**
             * Gets the value of the rateofferingcommentrow property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the rateofferingcommentrow property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getRATEOFFERINGCOMMENTROW().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link RateOfferingType.RATEOFFERINGROW.RATEOFFERINGCOMMENT.RATEOFFERINGCOMMENTROW }
             * 
             * 
             */
            public List<RateOfferingType.RATEOFFERINGROW.RATEOFFERINGCOMMENT.RATEOFFERINGCOMMENTROW> getRATEOFFERINGCOMMENTROW() {
                if (rateofferingcommentrow == null) {
                    rateofferingcommentrow = new ArrayList<RateOfferingType.RATEOFFERINGROW.RATEOFFERINGCOMMENT.RATEOFFERINGCOMMENTROW>();
                }
                return this.rateofferingcommentrow;
            }


            /**
             * <p>Classe Java de anonymous complex type.
             * 
             * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="RATE_OFFERING_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *         &lt;element name="COMMENT_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *         &lt;element name="ENTERED_BY" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *         &lt;element name="THE_COMMENT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *         &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *         &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *         &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *         &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *         &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *       &lt;/sequence>
             *       &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" />
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "rateofferinggid",
                "commentdate",
                "enteredby",
                "thecomment",
                "domainname",
                "insertuser",
                "insertdate",
                "updateuser",
                "updatedate"
            })
            public static class RATEOFFERINGCOMMENTROW {

                @XmlElement(name = "RATE_OFFERING_GID")
                protected String rateofferinggid;
                @XmlElement(name = "COMMENT_DATE")
                protected String commentdate;
                @XmlElement(name = "ENTERED_BY")
                protected String enteredby;
                @XmlElement(name = "THE_COMMENT")
                protected String thecomment;
                @XmlElement(name = "DOMAIN_NAME")
                protected String domainname;
                @XmlElement(name = "INSERT_USER")
                protected String insertuser;
                @XmlElement(name = "INSERT_DATE")
                protected String insertdate;
                @XmlElement(name = "UPDATE_USER")
                protected String updateuser;
                @XmlElement(name = "UPDATE_DATE")
                protected String updatedate;
                @XmlAttribute(name = "num")
                protected String num;

                /**
                 * Obtém o valor da propriedade rateofferinggid.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getRATEOFFERINGGID() {
                    return rateofferinggid;
                }

                /**
                 * Define o valor da propriedade rateofferinggid.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setRATEOFFERINGGID(String value) {
                    this.rateofferinggid = value;
                }

                /**
                 * Obtém o valor da propriedade commentdate.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCOMMENTDATE() {
                    return commentdate;
                }

                /**
                 * Define o valor da propriedade commentdate.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCOMMENTDATE(String value) {
                    this.commentdate = value;
                }

                /**
                 * Obtém o valor da propriedade enteredby.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getENTEREDBY() {
                    return enteredby;
                }

                /**
                 * Define o valor da propriedade enteredby.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setENTEREDBY(String value) {
                    this.enteredby = value;
                }

                /**
                 * Obtém o valor da propriedade thecomment.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getTHECOMMENT() {
                    return thecomment;
                }

                /**
                 * Define o valor da propriedade thecomment.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setTHECOMMENT(String value) {
                    this.thecomment = value;
                }

                /**
                 * Obtém o valor da propriedade domainname.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDOMAINNAME() {
                    return domainname;
                }

                /**
                 * Define o valor da propriedade domainname.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDOMAINNAME(String value) {
                    this.domainname = value;
                }

                /**
                 * Obtém o valor da propriedade insertuser.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getINSERTUSER() {
                    return insertuser;
                }

                /**
                 * Define o valor da propriedade insertuser.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setINSERTUSER(String value) {
                    this.insertuser = value;
                }

                /**
                 * Obtém o valor da propriedade insertdate.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getINSERTDATE() {
                    return insertdate;
                }

                /**
                 * Define o valor da propriedade insertdate.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setINSERTDATE(String value) {
                    this.insertdate = value;
                }

                /**
                 * Obtém o valor da propriedade updateuser.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getUPDATEUSER() {
                    return updateuser;
                }

                /**
                 * Define o valor da propriedade updateuser.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setUPDATEUSER(String value) {
                    this.updateuser = value;
                }

                /**
                 * Obtém o valor da propriedade updatedate.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getUPDATEDATE() {
                    return updatedate;
                }

                /**
                 * Define o valor da propriedade updatedate.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setUPDATEDATE(String value) {
                    this.updatedate = value;
                }

                /**
                 * Obtém o valor da propriedade num.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getNum() {
                    return num;
                }

                /**
                 * Define o valor da propriedade num.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setNum(String value) {
                    this.num = value;
                }

            }

        }


        /**
         * <p>Classe Java de anonymous complex type.
         * 
         * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="RATE_OFFERING_INV_PARTY_ROW" maxOccurs="unbounded" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="RATE_OFFERING_GID" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="INVOLVED_PARTY_QUAL_GID" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="INVOLVED_PARTY_CONTACT_GID" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="COM_METHOD_GID" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                 &lt;/sequence>
         *                 &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" />
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "rateofferinginvpartyrow"
        })
        public static class RATEOFFERINGINVPARTY {

            @XmlElement(name = "RATE_OFFERING_INV_PARTY_ROW")
            protected List<RateOfferingType.RATEOFFERINGROW.RATEOFFERINGINVPARTY.RATEOFFERINGINVPARTYROW> rateofferinginvpartyrow;

            /**
             * Gets the value of the rateofferinginvpartyrow property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the rateofferinginvpartyrow property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getRATEOFFERINGINVPARTYROW().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link RateOfferingType.RATEOFFERINGROW.RATEOFFERINGINVPARTY.RATEOFFERINGINVPARTYROW }
             * 
             * 
             */
            public List<RateOfferingType.RATEOFFERINGROW.RATEOFFERINGINVPARTY.RATEOFFERINGINVPARTYROW> getRATEOFFERINGINVPARTYROW() {
                if (rateofferinginvpartyrow == null) {
                    rateofferinginvpartyrow = new ArrayList<RateOfferingType.RATEOFFERINGROW.RATEOFFERINGINVPARTY.RATEOFFERINGINVPARTYROW>();
                }
                return this.rateofferinginvpartyrow;
            }


            /**
             * <p>Classe Java de anonymous complex type.
             * 
             * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="RATE_OFFERING_GID" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="INVOLVED_PARTY_QUAL_GID" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="INVOLVED_PARTY_CONTACT_GID" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="COM_METHOD_GID" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *       &lt;/sequence>
             *       &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" />
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "rateofferinggid",
                "involvedpartyqualgid",
                "involvedpartycontactgid",
                "commethodgid"
            })
            public static class RATEOFFERINGINVPARTYROW {

                @XmlElement(name = "RATE_OFFERING_GID", required = true)
                protected String rateofferinggid;
                @XmlElement(name = "INVOLVED_PARTY_QUAL_GID", required = true)
                protected String involvedpartyqualgid;
                @XmlElement(name = "INVOLVED_PARTY_CONTACT_GID", required = true)
                protected String involvedpartycontactgid;
                @XmlElement(name = "COM_METHOD_GID", required = true)
                protected String commethodgid;
                @XmlAttribute(name = "num")
                protected String num;

                /**
                 * Obtém o valor da propriedade rateofferinggid.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getRATEOFFERINGGID() {
                    return rateofferinggid;
                }

                /**
                 * Define o valor da propriedade rateofferinggid.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setRATEOFFERINGGID(String value) {
                    this.rateofferinggid = value;
                }

                /**
                 * Obtém o valor da propriedade involvedpartyqualgid.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getINVOLVEDPARTYQUALGID() {
                    return involvedpartyqualgid;
                }

                /**
                 * Define o valor da propriedade involvedpartyqualgid.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setINVOLVEDPARTYQUALGID(String value) {
                    this.involvedpartyqualgid = value;
                }

                /**
                 * Obtém o valor da propriedade involvedpartycontactgid.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getINVOLVEDPARTYCONTACTGID() {
                    return involvedpartycontactgid;
                }

                /**
                 * Define o valor da propriedade involvedpartycontactgid.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setINVOLVEDPARTYCONTACTGID(String value) {
                    this.involvedpartycontactgid = value;
                }

                /**
                 * Obtém o valor da propriedade commethodgid.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCOMMETHODGID() {
                    return commethodgid;
                }

                /**
                 * Define o valor da propriedade commethodgid.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCOMMETHODGID(String value) {
                    this.commethodgid = value;
                }

                /**
                 * Obtém o valor da propriedade num.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getNum() {
                    return num;
                }

                /**
                 * Define o valor da propriedade num.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setNum(String value) {
                    this.num = value;
                }

            }

        }


        /**
         * <p>Classe Java de anonymous complex type.
         * 
         * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence maxOccurs="unbounded" minOccurs="0">
         *         &lt;element name="RATE_OFFERING_STOPS_ROW" maxOccurs="unbounded">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="RATE_OFFERING_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                   &lt;element name="LOW_STOP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                   &lt;element name="HIGH_STOP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                   &lt;element name="PER_STOP_COST" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                   &lt;element name="PER_STOP_COST_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                   &lt;element name="PER_STOP_COST_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                   &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                   &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                   &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                   &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                   &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                 &lt;/sequence>
         *                 &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" />
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "rateofferingstopsrow"
        })
        public static class RATEOFFERINGSTOPS {

            @XmlElement(name = "RATE_OFFERING_STOPS_ROW")
            protected List<RateOfferingType.RATEOFFERINGROW.RATEOFFERINGSTOPS.RATEOFFERINGSTOPSROW> rateofferingstopsrow;

            /**
             * Gets the value of the rateofferingstopsrow property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the rateofferingstopsrow property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getRATEOFFERINGSTOPSROW().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link RateOfferingType.RATEOFFERINGROW.RATEOFFERINGSTOPS.RATEOFFERINGSTOPSROW }
             * 
             * 
             */
            public List<RateOfferingType.RATEOFFERINGROW.RATEOFFERINGSTOPS.RATEOFFERINGSTOPSROW> getRATEOFFERINGSTOPSROW() {
                if (rateofferingstopsrow == null) {
                    rateofferingstopsrow = new ArrayList<RateOfferingType.RATEOFFERINGROW.RATEOFFERINGSTOPS.RATEOFFERINGSTOPSROW>();
                }
                return this.rateofferingstopsrow;
            }


            /**
             * <p>Classe Java de anonymous complex type.
             * 
             * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="RATE_OFFERING_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *         &lt;element name="LOW_STOP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *         &lt;element name="HIGH_STOP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *         &lt;element name="PER_STOP_COST" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *         &lt;element name="PER_STOP_COST_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *         &lt;element name="PER_STOP_COST_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *         &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *         &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *         &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *         &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *         &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *       &lt;/sequence>
             *       &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" />
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "rateofferinggid",
                "lowstop",
                "highstop",
                "perstopcost",
                "perstopcostgid",
                "perstopcostbase",
                "domainname",
                "insertuser",
                "insertdate",
                "updateuser",
                "updatedate"
            })
            public static class RATEOFFERINGSTOPSROW {

                @XmlElement(name = "RATE_OFFERING_GID")
                protected String rateofferinggid;
                @XmlElement(name = "LOW_STOP")
                protected String lowstop;
                @XmlElement(name = "HIGH_STOP")
                protected String highstop;
                @XmlElement(name = "PER_STOP_COST")
                protected String perstopcost;
                @XmlElement(name = "PER_STOP_COST_GID")
                protected String perstopcostgid;
                @XmlElement(name = "PER_STOP_COST_BASE")
                protected String perstopcostbase;
                @XmlElement(name = "DOMAIN_NAME")
                protected String domainname;
                @XmlElement(name = "INSERT_USER")
                protected String insertuser;
                @XmlElement(name = "INSERT_DATE")
                protected String insertdate;
                @XmlElement(name = "UPDATE_USER")
                protected String updateuser;
                @XmlElement(name = "UPDATE_DATE")
                protected String updatedate;
                @XmlAttribute(name = "num")
                protected String num;

                /**
                 * Obtém o valor da propriedade rateofferinggid.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getRATEOFFERINGGID() {
                    return rateofferinggid;
                }

                /**
                 * Define o valor da propriedade rateofferinggid.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setRATEOFFERINGGID(String value) {
                    this.rateofferinggid = value;
                }

                /**
                 * Obtém o valor da propriedade lowstop.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getLOWSTOP() {
                    return lowstop;
                }

                /**
                 * Define o valor da propriedade lowstop.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setLOWSTOP(String value) {
                    this.lowstop = value;
                }

                /**
                 * Obtém o valor da propriedade highstop.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getHIGHSTOP() {
                    return highstop;
                }

                /**
                 * Define o valor da propriedade highstop.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setHIGHSTOP(String value) {
                    this.highstop = value;
                }

                /**
                 * Obtém o valor da propriedade perstopcost.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getPERSTOPCOST() {
                    return perstopcost;
                }

                /**
                 * Define o valor da propriedade perstopcost.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setPERSTOPCOST(String value) {
                    this.perstopcost = value;
                }

                /**
                 * Obtém o valor da propriedade perstopcostgid.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getPERSTOPCOSTGID() {
                    return perstopcostgid;
                }

                /**
                 * Define o valor da propriedade perstopcostgid.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setPERSTOPCOSTGID(String value) {
                    this.perstopcostgid = value;
                }

                /**
                 * Obtém o valor da propriedade perstopcostbase.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getPERSTOPCOSTBASE() {
                    return perstopcostbase;
                }

                /**
                 * Define o valor da propriedade perstopcostbase.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setPERSTOPCOSTBASE(String value) {
                    this.perstopcostbase = value;
                }

                /**
                 * Obtém o valor da propriedade domainname.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDOMAINNAME() {
                    return domainname;
                }

                /**
                 * Define o valor da propriedade domainname.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDOMAINNAME(String value) {
                    this.domainname = value;
                }

                /**
                 * Obtém o valor da propriedade insertuser.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getINSERTUSER() {
                    return insertuser;
                }

                /**
                 * Define o valor da propriedade insertuser.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setINSERTUSER(String value) {
                    this.insertuser = value;
                }

                /**
                 * Obtém o valor da propriedade insertdate.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getINSERTDATE() {
                    return insertdate;
                }

                /**
                 * Define o valor da propriedade insertdate.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setINSERTDATE(String value) {
                    this.insertdate = value;
                }

                /**
                 * Obtém o valor da propriedade updateuser.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getUPDATEUSER() {
                    return updateuser;
                }

                /**
                 * Define o valor da propriedade updateuser.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setUPDATEUSER(String value) {
                    this.updateuser = value;
                }

                /**
                 * Obtém o valor da propriedade updatedate.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getUPDATEDATE() {
                    return updatedate;
                }

                /**
                 * Define o valor da propriedade updatedate.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setUPDATEDATE(String value) {
                    this.updatedate = value;
                }

                /**
                 * Obtém o valor da propriedade num.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getNum() {
                    return num;
                }

                /**
                 * Define o valor da propriedade num.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setNum(String value) {
                    this.num = value;
                }

            }

        }


        /**
         * <p>Classe Java de anonymous complex type.
         * 
         * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="RATE_RULES_AND_TERMS_ROW" maxOccurs="unbounded" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="RATE_OFFERING_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                   &lt;element name="RULE_NUMBER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                   &lt;element name="RULE_TITLE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                   &lt;element name="RULE_DESC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                   &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                   &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                   &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                   &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                   &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                 &lt;/sequence>
         *                 &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" />
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "raterulesandtermsrow"
        })
        public static class RATERULESANDTERMS {

            @XmlElement(name = "RATE_RULES_AND_TERMS_ROW")
            protected List<RateOfferingType.RATEOFFERINGROW.RATERULESANDTERMS.RATERULESANDTERMSROW> raterulesandtermsrow;

            /**
             * Gets the value of the raterulesandtermsrow property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the raterulesandtermsrow property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getRATERULESANDTERMSROW().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link RateOfferingType.RATEOFFERINGROW.RATERULESANDTERMS.RATERULESANDTERMSROW }
             * 
             * 
             */
            public List<RateOfferingType.RATEOFFERINGROW.RATERULESANDTERMS.RATERULESANDTERMSROW> getRATERULESANDTERMSROW() {
                if (raterulesandtermsrow == null) {
                    raterulesandtermsrow = new ArrayList<RateOfferingType.RATEOFFERINGROW.RATERULESANDTERMS.RATERULESANDTERMSROW>();
                }
                return this.raterulesandtermsrow;
            }


            /**
             * <p>Classe Java de anonymous complex type.
             * 
             * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="RATE_OFFERING_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *         &lt;element name="RULE_NUMBER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *         &lt;element name="RULE_TITLE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *         &lt;element name="RULE_DESC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *         &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *         &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *         &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *         &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *         &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *       &lt;/sequence>
             *       &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" />
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "rateofferinggid",
                "rulenumber",
                "ruletitle",
                "ruledesc",
                "domainname",
                "insertuser",
                "insertdate",
                "updateuser",
                "updatedate"
            })
            public static class RATERULESANDTERMSROW {

                @XmlElement(name = "RATE_OFFERING_GID")
                protected String rateofferinggid;
                @XmlElement(name = "RULE_NUMBER")
                protected String rulenumber;
                @XmlElement(name = "RULE_TITLE")
                protected String ruletitle;
                @XmlElement(name = "RULE_DESC")
                protected String ruledesc;
                @XmlElement(name = "DOMAIN_NAME")
                protected String domainname;
                @XmlElement(name = "INSERT_USER")
                protected String insertuser;
                @XmlElement(name = "INSERT_DATE")
                protected String insertdate;
                @XmlElement(name = "UPDATE_USER")
                protected String updateuser;
                @XmlElement(name = "UPDATE_DATE")
                protected String updatedate;
                @XmlAttribute(name = "num")
                protected String num;

                /**
                 * Obtém o valor da propriedade rateofferinggid.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getRATEOFFERINGGID() {
                    return rateofferinggid;
                }

                /**
                 * Define o valor da propriedade rateofferinggid.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setRATEOFFERINGGID(String value) {
                    this.rateofferinggid = value;
                }

                /**
                 * Obtém o valor da propriedade rulenumber.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getRULENUMBER() {
                    return rulenumber;
                }

                /**
                 * Define o valor da propriedade rulenumber.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setRULENUMBER(String value) {
                    this.rulenumber = value;
                }

                /**
                 * Obtém o valor da propriedade ruletitle.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getRULETITLE() {
                    return ruletitle;
                }

                /**
                 * Define o valor da propriedade ruletitle.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setRULETITLE(String value) {
                    this.ruletitle = value;
                }

                /**
                 * Obtém o valor da propriedade ruledesc.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getRULEDESC() {
                    return ruledesc;
                }

                /**
                 * Define o valor da propriedade ruledesc.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setRULEDESC(String value) {
                    this.ruledesc = value;
                }

                /**
                 * Obtém o valor da propriedade domainname.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDOMAINNAME() {
                    return domainname;
                }

                /**
                 * Define o valor da propriedade domainname.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDOMAINNAME(String value) {
                    this.domainname = value;
                }

                /**
                 * Obtém o valor da propriedade insertuser.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getINSERTUSER() {
                    return insertuser;
                }

                /**
                 * Define o valor da propriedade insertuser.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setINSERTUSER(String value) {
                    this.insertuser = value;
                }

                /**
                 * Obtém o valor da propriedade insertdate.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getINSERTDATE() {
                    return insertdate;
                }

                /**
                 * Define o valor da propriedade insertdate.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setINSERTDATE(String value) {
                    this.insertdate = value;
                }

                /**
                 * Obtém o valor da propriedade updateuser.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getUPDATEUSER() {
                    return updateuser;
                }

                /**
                 * Define o valor da propriedade updateuser.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setUPDATEUSER(String value) {
                    this.updateuser = value;
                }

                /**
                 * Obtém o valor da propriedade updatedate.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getUPDATEDATE() {
                    return updatedate;
                }

                /**
                 * Define o valor da propriedade updatedate.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setUPDATEDATE(String value) {
                    this.updatedate = value;
                }

                /**
                 * Obtém o valor da propriedade num.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getNum() {
                    return num;
                }

                /**
                 * Define o valor da propriedade num.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setNum(String value) {
                    this.num = value;
                }

            }

        }


        /**
         * <p>Classe Java de anonymous complex type.
         * 
         * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="RO_SPECIAL_SERVICE_ACCESSORIAL_ROW" maxOccurs="unbounded" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="ACCESSORIAL_COST_GID" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="RATE_OFFERING_GID" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="ACCESSORIAL_CODE_GID" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="SPECIAL_SERVICE_GID" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                   &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                   &lt;element name="ACCESSORIAL_COST" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ACCESSORIAL_COST_TYPE" minOccurs="0"/>
         *                 &lt;/sequence>
         *                 &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" />
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "rospecialserviceaccessorialrow"
        })
        public static class ROSPECIALSERVICEACCESSORIAL {

            @XmlElement(name = "RO_SPECIAL_SERVICE_ACCESSORIAL_ROW")
            protected List<RateOfferingType.RATEOFFERINGROW.ROSPECIALSERVICEACCESSORIAL.ROSPECIALSERVICEACCESSORIALROW> rospecialserviceaccessorialrow;

            /**
             * Gets the value of the rospecialserviceaccessorialrow property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the rospecialserviceaccessorialrow property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getROSPECIALSERVICEACCESSORIALROW().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link RateOfferingType.RATEOFFERINGROW.ROSPECIALSERVICEACCESSORIAL.ROSPECIALSERVICEACCESSORIALROW }
             * 
             * 
             */
            public List<RateOfferingType.RATEOFFERINGROW.ROSPECIALSERVICEACCESSORIAL.ROSPECIALSERVICEACCESSORIALROW> getROSPECIALSERVICEACCESSORIALROW() {
                if (rospecialserviceaccessorialrow == null) {
                    rospecialserviceaccessorialrow = new ArrayList<RateOfferingType.RATEOFFERINGROW.ROSPECIALSERVICEACCESSORIAL.ROSPECIALSERVICEACCESSORIALROW>();
                }
                return this.rospecialserviceaccessorialrow;
            }


            /**
             * <p>Classe Java de anonymous complex type.
             * 
             * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="ACCESSORIAL_COST_GID" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="RATE_OFFERING_GID" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="ACCESSORIAL_CODE_GID" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="SPECIAL_SERVICE_GID" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *         &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *         &lt;element name="ACCESSORIAL_COST" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ACCESSORIAL_COST_TYPE" minOccurs="0"/>
             *       &lt;/sequence>
             *       &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" />
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "accessorialcostgid",
                "rateofferinggid",
                "accessorialcodegid",
                "specialservicegid",
                "domainname",
                "insertuser",
                "insertdate",
                "updateuser",
                "updatedate",
                "accessorialcost"
            })
            public static class ROSPECIALSERVICEACCESSORIALROW {

                @XmlElement(name = "ACCESSORIAL_COST_GID", required = true)
                protected String accessorialcostgid;
                @XmlElement(name = "RATE_OFFERING_GID", required = true)
                protected String rateofferinggid;
                @XmlElement(name = "ACCESSORIAL_CODE_GID", required = true)
                protected String accessorialcodegid;
                @XmlElement(name = "SPECIAL_SERVICE_GID", required = true)
                protected String specialservicegid;
                @XmlElement(name = "DOMAIN_NAME", required = true)
                protected String domainname;
                @XmlElement(name = "INSERT_USER", required = true)
                protected String insertuser;
                @XmlElement(name = "INSERT_DATE", required = true)
                protected String insertdate;
                @XmlElement(name = "UPDATE_USER")
                protected String updateuser;
                @XmlElement(name = "UPDATE_DATE")
                protected String updatedate;
                @XmlElement(name = "ACCESSORIAL_COST")
                protected ACCESSORIALCOSTTYPE accessorialcost;
                @XmlAttribute(name = "num")
                protected String num;

                /**
                 * Obtém o valor da propriedade accessorialcostgid.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getACCESSORIALCOSTGID() {
                    return accessorialcostgid;
                }

                /**
                 * Define o valor da propriedade accessorialcostgid.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setACCESSORIALCOSTGID(String value) {
                    this.accessorialcostgid = value;
                }

                /**
                 * Obtém o valor da propriedade rateofferinggid.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getRATEOFFERINGGID() {
                    return rateofferinggid;
                }

                /**
                 * Define o valor da propriedade rateofferinggid.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setRATEOFFERINGGID(String value) {
                    this.rateofferinggid = value;
                }

                /**
                 * Obtém o valor da propriedade accessorialcodegid.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getACCESSORIALCODEGID() {
                    return accessorialcodegid;
                }

                /**
                 * Define o valor da propriedade accessorialcodegid.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setACCESSORIALCODEGID(String value) {
                    this.accessorialcodegid = value;
                }

                /**
                 * Obtém o valor da propriedade specialservicegid.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getSPECIALSERVICEGID() {
                    return specialservicegid;
                }

                /**
                 * Define o valor da propriedade specialservicegid.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setSPECIALSERVICEGID(String value) {
                    this.specialservicegid = value;
                }

                /**
                 * Obtém o valor da propriedade domainname.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDOMAINNAME() {
                    return domainname;
                }

                /**
                 * Define o valor da propriedade domainname.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDOMAINNAME(String value) {
                    this.domainname = value;
                }

                /**
                 * Obtém o valor da propriedade insertuser.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getINSERTUSER() {
                    return insertuser;
                }

                /**
                 * Define o valor da propriedade insertuser.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setINSERTUSER(String value) {
                    this.insertuser = value;
                }

                /**
                 * Obtém o valor da propriedade insertdate.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getINSERTDATE() {
                    return insertdate;
                }

                /**
                 * Define o valor da propriedade insertdate.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setINSERTDATE(String value) {
                    this.insertdate = value;
                }

                /**
                 * Obtém o valor da propriedade updateuser.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getUPDATEUSER() {
                    return updateuser;
                }

                /**
                 * Define o valor da propriedade updateuser.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setUPDATEUSER(String value) {
                    this.updateuser = value;
                }

                /**
                 * Obtém o valor da propriedade updatedate.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getUPDATEDATE() {
                    return updatedate;
                }

                /**
                 * Define o valor da propriedade updatedate.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setUPDATEDATE(String value) {
                    this.updatedate = value;
                }

                /**
                 * Obtém o valor da propriedade accessorialcost.
                 * 
                 * @return
                 *     possible object is
                 *     {@link ACCESSORIALCOSTTYPE }
                 *     
                 */
                public ACCESSORIALCOSTTYPE getACCESSORIALCOST() {
                    return accessorialcost;
                }

                /**
                 * Define o valor da propriedade accessorialcost.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link ACCESSORIALCOSTTYPE }
                 *     
                 */
                public void setACCESSORIALCOST(ACCESSORIALCOSTTYPE value) {
                    this.accessorialcost = value;
                }

                /**
                 * Obtém o valor da propriedade num.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getNum() {
                    return num;
                }

                /**
                 * Define o valor da propriedade num.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setNum(String value) {
                    this.num = value;
                }

            }

        }

    }

}
