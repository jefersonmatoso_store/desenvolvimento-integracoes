
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.*;


/**
 * 
 *             Used to identify hazardous materials information that is item centric, or related to an item.
 *          
 * 
 * <p>Classe Java de HazmatItemType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="HazmatItemType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OTMTransactionInOut">
 *       &lt;sequence>
 *         &lt;element name="HazmatItemGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="TransactionCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransactionCodeType"/>
 *         &lt;element name="PackagedItemGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="RegionGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="HazmatModeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="HazmatGenericGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="HazmatGeneric" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}HazmatGenericType" minOccurs="0"/>
 *         &lt;element name="HazmatIndicator" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="HotIndicator" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ResidueIndicator" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="DotExemptionGid" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="HazmatAprovalExemptionGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="AdditionalTransportMsg1Gid" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="HazmatTransportMsgGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="AdditionalTransportMsg2Gid" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="HazmatTransportMsgGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="Stcc49CodeGid">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="STCCGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="CompetentAuthorityCodeGid" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="HazmatAprovalExemptionGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="HazmatCommonInfo" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}HazmatCommonInfoType" minOccurs="0"/>
 *         &lt;element name="ProperShippingNamePrefix" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsPoison" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "HazmatItemType", propOrder = {
    "hazmatItemGid",
    "transactionCode",
    "packagedItemGid",
    "regionGid",
    "hazmatModeGid",
    "hazmatGenericGid",
    "hazmatGeneric",
    "hazmatIndicator",
    "hotIndicator",
    "residueIndicator",
    "dotExemptionGid",
    "additionalTransportMsg1Gid",
    "additionalTransportMsg2Gid",
    "stcc49CodeGid",
    "competentAuthorityCodeGid",
    "hazmatCommonInfo",
    "properShippingNamePrefix",
    "isPoison"
})
public class HazmatItemType
    extends OTMTransactionInOut
{

    @XmlElement(name = "HazmatItemGid", required = true)
    protected GLogXMLGidType hazmatItemGid;
    @XmlElement(name = "TransactionCode", required = true)
    @XmlSchemaType(name = "string")
    protected TransactionCodeType transactionCode;
    @XmlElement(name = "PackagedItemGid")
    protected GLogXMLGidType packagedItemGid;
    @XmlElement(name = "RegionGid", required = true)
    protected GLogXMLGidType regionGid;
    @XmlElement(name = "HazmatModeGid", required = true)
    protected GLogXMLGidType hazmatModeGid;
    @XmlElement(name = "HazmatGenericGid")
    protected GLogXMLGidType hazmatGenericGid;
    @XmlElement(name = "HazmatGeneric")
    protected HazmatGenericType hazmatGeneric;
    @XmlElement(name = "HazmatIndicator", required = true)
    protected String hazmatIndicator;
    @XmlElement(name = "HotIndicator", required = true)
    protected String hotIndicator;
    @XmlElement(name = "ResidueIndicator", required = true)
    protected String residueIndicator;
    @XmlElement(name = "DotExemptionGid")
    protected HazmatItemType.DotExemptionGid dotExemptionGid;
    @XmlElement(name = "AdditionalTransportMsg1Gid")
    protected HazmatItemType.AdditionalTransportMsg1Gid additionalTransportMsg1Gid;
    @XmlElement(name = "AdditionalTransportMsg2Gid")
    protected HazmatItemType.AdditionalTransportMsg2Gid additionalTransportMsg2Gid;
    @XmlElement(name = "Stcc49CodeGid", required = true)
    protected HazmatItemType.Stcc49CodeGid stcc49CodeGid;
    @XmlElement(name = "CompetentAuthorityCodeGid")
    protected HazmatItemType.CompetentAuthorityCodeGid competentAuthorityCodeGid;
    @XmlElement(name = "HazmatCommonInfo")
    protected HazmatCommonInfoType hazmatCommonInfo;
    @XmlElement(name = "ProperShippingNamePrefix")
    protected String properShippingNamePrefix;
    @XmlElement(name = "IsPoison")
    protected String isPoison;

    /**
     * Obtém o valor da propriedade hazmatItemGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getHazmatItemGid() {
        return hazmatItemGid;
    }

    /**
     * Define o valor da propriedade hazmatItemGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setHazmatItemGid(GLogXMLGidType value) {
        this.hazmatItemGid = value;
    }

    /**
     * Obtém o valor da propriedade transactionCode.
     * 
     * @return
     *     possible object is
     *     {@link TransactionCodeType }
     *     
     */
    public TransactionCodeType getTransactionCode() {
        return transactionCode;
    }

    /**
     * Define o valor da propriedade transactionCode.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionCodeType }
     *     
     */
    public void setTransactionCode(TransactionCodeType value) {
        this.transactionCode = value;
    }

    /**
     * Obtém o valor da propriedade packagedItemGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getPackagedItemGid() {
        return packagedItemGid;
    }

    /**
     * Define o valor da propriedade packagedItemGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setPackagedItemGid(GLogXMLGidType value) {
        this.packagedItemGid = value;
    }

    /**
     * Obtém o valor da propriedade regionGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRegionGid() {
        return regionGid;
    }

    /**
     * Define o valor da propriedade regionGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRegionGid(GLogXMLGidType value) {
        this.regionGid = value;
    }

    /**
     * Obtém o valor da propriedade hazmatModeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getHazmatModeGid() {
        return hazmatModeGid;
    }

    /**
     * Define o valor da propriedade hazmatModeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setHazmatModeGid(GLogXMLGidType value) {
        this.hazmatModeGid = value;
    }

    /**
     * Obtém o valor da propriedade hazmatGenericGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getHazmatGenericGid() {
        return hazmatGenericGid;
    }

    /**
     * Define o valor da propriedade hazmatGenericGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setHazmatGenericGid(GLogXMLGidType value) {
        this.hazmatGenericGid = value;
    }

    /**
     * Obtém o valor da propriedade hazmatGeneric.
     * 
     * @return
     *     possible object is
     *     {@link HazmatGenericType }
     *     
     */
    public HazmatGenericType getHazmatGeneric() {
        return hazmatGeneric;
    }

    /**
     * Define o valor da propriedade hazmatGeneric.
     * 
     * @param value
     *     allowed object is
     *     {@link HazmatGenericType }
     *     
     */
    public void setHazmatGeneric(HazmatGenericType value) {
        this.hazmatGeneric = value;
    }

    /**
     * Obtém o valor da propriedade hazmatIndicator.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHazmatIndicator() {
        return hazmatIndicator;
    }

    /**
     * Define o valor da propriedade hazmatIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHazmatIndicator(String value) {
        this.hazmatIndicator = value;
    }

    /**
     * Obtém o valor da propriedade hotIndicator.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHotIndicator() {
        return hotIndicator;
    }

    /**
     * Define o valor da propriedade hotIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHotIndicator(String value) {
        this.hotIndicator = value;
    }

    /**
     * Obtém o valor da propriedade residueIndicator.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResidueIndicator() {
        return residueIndicator;
    }

    /**
     * Define o valor da propriedade residueIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResidueIndicator(String value) {
        this.residueIndicator = value;
    }

    /**
     * Obtém o valor da propriedade dotExemptionGid.
     * 
     * @return
     *     possible object is
     *     {@link HazmatItemType.DotExemptionGid }
     *     
     */
    public HazmatItemType.DotExemptionGid getDotExemptionGid() {
        return dotExemptionGid;
    }

    /**
     * Define o valor da propriedade dotExemptionGid.
     * 
     * @param value
     *     allowed object is
     *     {@link HazmatItemType.DotExemptionGid }
     *     
     */
    public void setDotExemptionGid(HazmatItemType.DotExemptionGid value) {
        this.dotExemptionGid = value;
    }

    /**
     * Obtém o valor da propriedade additionalTransportMsg1Gid.
     * 
     * @return
     *     possible object is
     *     {@link HazmatItemType.AdditionalTransportMsg1Gid }
     *     
     */
    public HazmatItemType.AdditionalTransportMsg1Gid getAdditionalTransportMsg1Gid() {
        return additionalTransportMsg1Gid;
    }

    /**
     * Define o valor da propriedade additionalTransportMsg1Gid.
     * 
     * @param value
     *     allowed object is
     *     {@link HazmatItemType.AdditionalTransportMsg1Gid }
     *     
     */
    public void setAdditionalTransportMsg1Gid(HazmatItemType.AdditionalTransportMsg1Gid value) {
        this.additionalTransportMsg1Gid = value;
    }

    /**
     * Obtém o valor da propriedade additionalTransportMsg2Gid.
     * 
     * @return
     *     possible object is
     *     {@link HazmatItemType.AdditionalTransportMsg2Gid }
     *     
     */
    public HazmatItemType.AdditionalTransportMsg2Gid getAdditionalTransportMsg2Gid() {
        return additionalTransportMsg2Gid;
    }

    /**
     * Define o valor da propriedade additionalTransportMsg2Gid.
     * 
     * @param value
     *     allowed object is
     *     {@link HazmatItemType.AdditionalTransportMsg2Gid }
     *     
     */
    public void setAdditionalTransportMsg2Gid(HazmatItemType.AdditionalTransportMsg2Gid value) {
        this.additionalTransportMsg2Gid = value;
    }

    /**
     * Obtém o valor da propriedade stcc49CodeGid.
     * 
     * @return
     *     possible object is
     *     {@link HazmatItemType.Stcc49CodeGid }
     *     
     */
    public HazmatItemType.Stcc49CodeGid getStcc49CodeGid() {
        return stcc49CodeGid;
    }

    /**
     * Define o valor da propriedade stcc49CodeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link HazmatItemType.Stcc49CodeGid }
     *     
     */
    public void setStcc49CodeGid(HazmatItemType.Stcc49CodeGid value) {
        this.stcc49CodeGid = value;
    }

    /**
     * Obtém o valor da propriedade competentAuthorityCodeGid.
     * 
     * @return
     *     possible object is
     *     {@link HazmatItemType.CompetentAuthorityCodeGid }
     *     
     */
    public HazmatItemType.CompetentAuthorityCodeGid getCompetentAuthorityCodeGid() {
        return competentAuthorityCodeGid;
    }

    /**
     * Define o valor da propriedade competentAuthorityCodeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link HazmatItemType.CompetentAuthorityCodeGid }
     *     
     */
    public void setCompetentAuthorityCodeGid(HazmatItemType.CompetentAuthorityCodeGid value) {
        this.competentAuthorityCodeGid = value;
    }

    /**
     * Obtém o valor da propriedade hazmatCommonInfo.
     * 
     * @return
     *     possible object is
     *     {@link HazmatCommonInfoType }
     *     
     */
    public HazmatCommonInfoType getHazmatCommonInfo() {
        return hazmatCommonInfo;
    }

    /**
     * Define o valor da propriedade hazmatCommonInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link HazmatCommonInfoType }
     *     
     */
    public void setHazmatCommonInfo(HazmatCommonInfoType value) {
        this.hazmatCommonInfo = value;
    }

    /**
     * Obtém o valor da propriedade properShippingNamePrefix.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProperShippingNamePrefix() {
        return properShippingNamePrefix;
    }

    /**
     * Define o valor da propriedade properShippingNamePrefix.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProperShippingNamePrefix(String value) {
        this.properShippingNamePrefix = value;
    }

    /**
     * Obtém o valor da propriedade isPoison.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsPoison() {
        return isPoison;
    }

    /**
     * Define o valor da propriedade isPoison.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsPoison(String value) {
        this.isPoison = value;
    }


    /**
     * <p>Classe Java de anonymous complex type.
     * 
     * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="HazmatTransportMsgGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "hazmatTransportMsgGid"
    })
    public static class AdditionalTransportMsg1Gid {

        @XmlElement(name = "HazmatTransportMsgGid", required = true)
        protected GLogXMLGidType hazmatTransportMsgGid;

        /**
         * Obtém o valor da propriedade hazmatTransportMsgGid.
         * 
         * @return
         *     possible object is
         *     {@link GLogXMLGidType }
         *     
         */
        public GLogXMLGidType getHazmatTransportMsgGid() {
            return hazmatTransportMsgGid;
        }

        /**
         * Define o valor da propriedade hazmatTransportMsgGid.
         * 
         * @param value
         *     allowed object is
         *     {@link GLogXMLGidType }
         *     
         */
        public void setHazmatTransportMsgGid(GLogXMLGidType value) {
            this.hazmatTransportMsgGid = value;
        }

    }


    /**
     * <p>Classe Java de anonymous complex type.
     * 
     * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="HazmatTransportMsgGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "hazmatTransportMsgGid"
    })
    public static class AdditionalTransportMsg2Gid {

        @XmlElement(name = "HazmatTransportMsgGid", required = true)
        protected GLogXMLGidType hazmatTransportMsgGid;

        /**
         * Obtém o valor da propriedade hazmatTransportMsgGid.
         * 
         * @return
         *     possible object is
         *     {@link GLogXMLGidType }
         *     
         */
        public GLogXMLGidType getHazmatTransportMsgGid() {
            return hazmatTransportMsgGid;
        }

        /**
         * Define o valor da propriedade hazmatTransportMsgGid.
         * 
         * @param value
         *     allowed object is
         *     {@link GLogXMLGidType }
         *     
         */
        public void setHazmatTransportMsgGid(GLogXMLGidType value) {
            this.hazmatTransportMsgGid = value;
        }

    }


    /**
     * <p>Classe Java de anonymous complex type.
     * 
     * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="HazmatAprovalExemptionGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "hazmatAprovalExemptionGid"
    })
    public static class CompetentAuthorityCodeGid {

        @XmlElement(name = "HazmatAprovalExemptionGid", required = true)
        protected GLogXMLGidType hazmatAprovalExemptionGid;

        /**
         * Obtém o valor da propriedade hazmatAprovalExemptionGid.
         * 
         * @return
         *     possible object is
         *     {@link GLogXMLGidType }
         *     
         */
        public GLogXMLGidType getHazmatAprovalExemptionGid() {
            return hazmatAprovalExemptionGid;
        }

        /**
         * Define o valor da propriedade hazmatAprovalExemptionGid.
         * 
         * @param value
         *     allowed object is
         *     {@link GLogXMLGidType }
         *     
         */
        public void setHazmatAprovalExemptionGid(GLogXMLGidType value) {
            this.hazmatAprovalExemptionGid = value;
        }

    }


    /**
     * <p>Classe Java de anonymous complex type.
     * 
     * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="HazmatAprovalExemptionGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "hazmatAprovalExemptionGid"
    })
    public static class DotExemptionGid {

        @XmlElement(name = "HazmatAprovalExemptionGid", required = true)
        protected GLogXMLGidType hazmatAprovalExemptionGid;

        /**
         * Obtém o valor da propriedade hazmatAprovalExemptionGid.
         * 
         * @return
         *     possible object is
         *     {@link GLogXMLGidType }
         *     
         */
        public GLogXMLGidType getHazmatAprovalExemptionGid() {
            return hazmatAprovalExemptionGid;
        }

        /**
         * Define o valor da propriedade hazmatAprovalExemptionGid.
         * 
         * @param value
         *     allowed object is
         *     {@link GLogXMLGidType }
         *     
         */
        public void setHazmatAprovalExemptionGid(GLogXMLGidType value) {
            this.hazmatAprovalExemptionGid = value;
        }

    }


    /**
     * <p>Classe Java de anonymous complex type.
     * 
     * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="STCCGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "stccGid"
    })
    public static class Stcc49CodeGid {

        @XmlElement(name = "STCCGid", required = true)
        protected GLogXMLGidType stccGid;

        /**
         * Obtém o valor da propriedade stccGid.
         * 
         * @return
         *     possible object is
         *     {@link GLogXMLGidType }
         *     
         */
        public GLogXMLGidType getSTCCGid() {
            return stccGid;
        }

        /**
         * Define o valor da propriedade stccGid.
         * 
         * @param value
         *     allowed object is
         *     {@link GLogXMLGidType }
         *     
         */
        public void setSTCCGid(GLogXMLGidType value) {
            this.stccGid = value;
        }

    }

}
