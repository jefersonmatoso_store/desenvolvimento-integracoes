
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Specifies the load configuration result for each piece of the ship unit.
 *             This is only applicable when the ShipUnit is in the Shipment.
 *          
 * 
 * <p>Classe Java de ShipUnitPieceType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="ShipUnitPieceType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PieceNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="LoadingSequence" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="StackingLayer" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="OrientationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="XOrdinate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLengthType"/>
 *         &lt;element name="YOrdinate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLengthType"/>
 *         &lt;element name="ZOrdinate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLengthType"/>
 *         &lt;element name="LoadConfig3DPatternGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="PatternInstanceCount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ShipUnitPieceType", propOrder = {
    "pieceNumber",
    "loadingSequence",
    "stackingLayer",
    "orientationGid",
    "xOrdinate",
    "yOrdinate",
    "zOrdinate",
    "loadConfig3DPatternGid",
    "patternInstanceCount"
})
public class ShipUnitPieceType {

    @XmlElement(name = "PieceNumber", required = true)
    protected String pieceNumber;
    @XmlElement(name = "LoadingSequence", required = true)
    protected String loadingSequence;
    @XmlElement(name = "StackingLayer", required = true)
    protected String stackingLayer;
    @XmlElement(name = "OrientationGid", required = true)
    protected GLogXMLGidType orientationGid;
    @XmlElement(name = "XOrdinate", required = true)
    protected GLogXMLLengthType xOrdinate;
    @XmlElement(name = "YOrdinate", required = true)
    protected GLogXMLLengthType yOrdinate;
    @XmlElement(name = "ZOrdinate", required = true)
    protected GLogXMLLengthType zOrdinate;
    @XmlElement(name = "LoadConfig3DPatternGid")
    protected GLogXMLGidType loadConfig3DPatternGid;
    @XmlElement(name = "PatternInstanceCount")
    protected String patternInstanceCount;

    /**
     * Obtém o valor da propriedade pieceNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPieceNumber() {
        return pieceNumber;
    }

    /**
     * Define o valor da propriedade pieceNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPieceNumber(String value) {
        this.pieceNumber = value;
    }

    /**
     * Obtém o valor da propriedade loadingSequence.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLoadingSequence() {
        return loadingSequence;
    }

    /**
     * Define o valor da propriedade loadingSequence.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLoadingSequence(String value) {
        this.loadingSequence = value;
    }

    /**
     * Obtém o valor da propriedade stackingLayer.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStackingLayer() {
        return stackingLayer;
    }

    /**
     * Define o valor da propriedade stackingLayer.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStackingLayer(String value) {
        this.stackingLayer = value;
    }

    /**
     * Obtém o valor da propriedade orientationGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getOrientationGid() {
        return orientationGid;
    }

    /**
     * Define o valor da propriedade orientationGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setOrientationGid(GLogXMLGidType value) {
        this.orientationGid = value;
    }

    /**
     * Obtém o valor da propriedade xOrdinate.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLengthType }
     *     
     */
    public GLogXMLLengthType getXOrdinate() {
        return xOrdinate;
    }

    /**
     * Define o valor da propriedade xOrdinate.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLengthType }
     *     
     */
    public void setXOrdinate(GLogXMLLengthType value) {
        this.xOrdinate = value;
    }

    /**
     * Obtém o valor da propriedade yOrdinate.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLengthType }
     *     
     */
    public GLogXMLLengthType getYOrdinate() {
        return yOrdinate;
    }

    /**
     * Define o valor da propriedade yOrdinate.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLengthType }
     *     
     */
    public void setYOrdinate(GLogXMLLengthType value) {
        this.yOrdinate = value;
    }

    /**
     * Obtém o valor da propriedade zOrdinate.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLengthType }
     *     
     */
    public GLogXMLLengthType getZOrdinate() {
        return zOrdinate;
    }

    /**
     * Define o valor da propriedade zOrdinate.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLengthType }
     *     
     */
    public void setZOrdinate(GLogXMLLengthType value) {
        this.zOrdinate = value;
    }

    /**
     * Obtém o valor da propriedade loadConfig3DPatternGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getLoadConfig3DPatternGid() {
        return loadConfig3DPatternGid;
    }

    /**
     * Define o valor da propriedade loadConfig3DPatternGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setLoadConfig3DPatternGid(GLogXMLGidType value) {
        this.loadConfig3DPatternGid = value;
    }

    /**
     * Obtém o valor da propriedade patternInstanceCount.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPatternInstanceCount() {
        return patternInstanceCount;
    }

    /**
     * Define o valor da propriedade patternInstanceCount.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPatternInstanceCount(String value) {
        this.patternInstanceCount = value;
    }

}
