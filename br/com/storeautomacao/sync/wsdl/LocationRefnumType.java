
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * LocationRefnum is a location reference number,
 *             which provides an alternate means for identifying a location.
 *             It consists of a qualifier and a value.
 *          
 * 
 * <p>Classe Java de LocationRefnumType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="LocationRefnumType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="LocationRefnumQualifierGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="LocationRefnumValue" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LocationRefnumType", propOrder = {
    "locationRefnumQualifierGid",
    "locationRefnumValue"
})
public class LocationRefnumType {

    @XmlElement(name = "LocationRefnumQualifierGid", required = true)
    protected GLogXMLGidType locationRefnumQualifierGid;
    @XmlElement(name = "LocationRefnumValue", required = true)
    protected String locationRefnumValue;

    /**
     * Obtém o valor da propriedade locationRefnumQualifierGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getLocationRefnumQualifierGid() {
        return locationRefnumQualifierGid;
    }

    /**
     * Define o valor da propriedade locationRefnumQualifierGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setLocationRefnumQualifierGid(GLogXMLGidType value) {
        this.locationRefnumQualifierGid = value;
    }

    /**
     * Obtém o valor da propriedade locationRefnumValue.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLocationRefnumValue() {
        return locationRefnumValue;
    }

    /**
     * Define o valor da propriedade locationRefnumValue.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLocationRefnumValue(String value) {
        this.locationRefnumValue = value;
    }

}
