
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * Contains a list of child elements to be replaced when using the RC TransactionCode. If this element is not
 *             specified within the object when the TransactionCode is RC, then all of the child objects will be replaced.
 *             Refer to TransactionCode element.
 *          
 * 
 * <p>Classe Java de ReplaceChildrenType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="ReplaceChildrenType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ManagedChild" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReplaceChildrenType", propOrder = {
    "managedChild"
})
public class ReplaceChildrenType {

    @XmlElement(name = "ManagedChild")
    protected List<String> managedChild;

    /**
     * Gets the value of the managedChild property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the managedChild property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getManagedChild().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getManagedChild() {
        if (managedChild == null) {
            managedChild = new ArrayList<String>();
        }
        return this.managedChild;
    }

}
