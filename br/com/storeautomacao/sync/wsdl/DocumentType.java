
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Classe Java de DocumentType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="DocumentType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OTMTransactionInOut">
 *       &lt;sequence>
 *         &lt;element name="DocumentGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="TransactionCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransactionCodeType" minOccurs="0"/>
 *         &lt;element name="ReplaceChildren" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ReplaceChildrenType" minOccurs="0"/>
 *         &lt;element name="DocumentDefinitionGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="ContentManagementSystemGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="DocumentOwner" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}DocumentOwnerType" minOccurs="0"/>
 *         &lt;element name="Indicator" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AddNewRevision" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Annotation" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Status" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}StatusType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Location" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LocationType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="DocumentContent" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}DocumentContentType" minOccurs="0"/>
 *         &lt;element name="DocumentContentParam" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}DocumentContentParamType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DocumentType", propOrder = {
    "documentGid",
    "transactionCode",
    "replaceChildren",
    "documentDefinitionGid",
    "contentManagementSystemGid",
    "documentOwner",
    "indicator",
    "addNewRevision",
    "annotation",
    "status",
    "location",
    "documentContent",
    "documentContentParam"
})
public class DocumentType
    extends OTMTransactionInOut
{

    @XmlElement(name = "DocumentGid")
    protected GLogXMLGidType documentGid;
    @XmlElement(name = "TransactionCode")
    @XmlSchemaType(name = "string")
    protected TransactionCodeType transactionCode;
    @XmlElement(name = "ReplaceChildren")
    protected ReplaceChildrenType replaceChildren;
    @XmlElement(name = "DocumentDefinitionGid")
    protected GLogXMLGidType documentDefinitionGid;
    @XmlElement(name = "ContentManagementSystemGid")
    protected GLogXMLGidType contentManagementSystemGid;
    @XmlElement(name = "DocumentOwner")
    protected DocumentOwnerType documentOwner;
    @XmlElement(name = "Indicator")
    protected String indicator;
    @XmlElement(name = "AddNewRevision")
    protected String addNewRevision;
    @XmlElement(name = "Annotation")
    protected String annotation;
    @XmlElement(name = "Status")
    protected List<StatusType> status;
    @XmlElement(name = "Location")
    protected List<LocationType> location;
    @XmlElement(name = "DocumentContent")
    protected DocumentContentType documentContent;
    @XmlElement(name = "DocumentContentParam")
    protected List<DocumentContentParamType> documentContentParam;

    /**
     * Obtém o valor da propriedade documentGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getDocumentGid() {
        return documentGid;
    }

    /**
     * Define o valor da propriedade documentGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setDocumentGid(GLogXMLGidType value) {
        this.documentGid = value;
    }

    /**
     * Obtém o valor da propriedade transactionCode.
     * 
     * @return
     *     possible object is
     *     {@link TransactionCodeType }
     *     
     */
    public TransactionCodeType getTransactionCode() {
        return transactionCode;
    }

    /**
     * Define o valor da propriedade transactionCode.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionCodeType }
     *     
     */
    public void setTransactionCode(TransactionCodeType value) {
        this.transactionCode = value;
    }

    /**
     * Obtém o valor da propriedade replaceChildren.
     * 
     * @return
     *     possible object is
     *     {@link ReplaceChildrenType }
     *     
     */
    public ReplaceChildrenType getReplaceChildren() {
        return replaceChildren;
    }

    /**
     * Define o valor da propriedade replaceChildren.
     * 
     * @param value
     *     allowed object is
     *     {@link ReplaceChildrenType }
     *     
     */
    public void setReplaceChildren(ReplaceChildrenType value) {
        this.replaceChildren = value;
    }

    /**
     * Obtém o valor da propriedade documentDefinitionGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getDocumentDefinitionGid() {
        return documentDefinitionGid;
    }

    /**
     * Define o valor da propriedade documentDefinitionGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setDocumentDefinitionGid(GLogXMLGidType value) {
        this.documentDefinitionGid = value;
    }

    /**
     * Obtém o valor da propriedade contentManagementSystemGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getContentManagementSystemGid() {
        return contentManagementSystemGid;
    }

    /**
     * Define o valor da propriedade contentManagementSystemGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setContentManagementSystemGid(GLogXMLGidType value) {
        this.contentManagementSystemGid = value;
    }

    /**
     * Obtém o valor da propriedade documentOwner.
     * 
     * @return
     *     possible object is
     *     {@link DocumentOwnerType }
     *     
     */
    public DocumentOwnerType getDocumentOwner() {
        return documentOwner;
    }

    /**
     * Define o valor da propriedade documentOwner.
     * 
     * @param value
     *     allowed object is
     *     {@link DocumentOwnerType }
     *     
     */
    public void setDocumentOwner(DocumentOwnerType value) {
        this.documentOwner = value;
    }

    /**
     * Obtém o valor da propriedade indicator.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIndicator() {
        return indicator;
    }

    /**
     * Define o valor da propriedade indicator.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIndicator(String value) {
        this.indicator = value;
    }

    /**
     * Obtém o valor da propriedade addNewRevision.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddNewRevision() {
        return addNewRevision;
    }

    /**
     * Define o valor da propriedade addNewRevision.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddNewRevision(String value) {
        this.addNewRevision = value;
    }

    /**
     * Obtém o valor da propriedade annotation.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAnnotation() {
        return annotation;
    }

    /**
     * Define o valor da propriedade annotation.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAnnotation(String value) {
        this.annotation = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the status property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getStatus().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link StatusType }
     * 
     * 
     */
    public List<StatusType> getStatus() {
        if (status == null) {
            status = new ArrayList<StatusType>();
        }
        return this.status;
    }

    /**
     * Gets the value of the location property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the location property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLocation().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LocationType }
     * 
     * 
     */
    public List<LocationType> getLocation() {
        if (location == null) {
            location = new ArrayList<LocationType>();
        }
        return this.location;
    }

    /**
     * Obtém o valor da propriedade documentContent.
     * 
     * @return
     *     possible object is
     *     {@link DocumentContentType }
     *     
     */
    public DocumentContentType getDocumentContent() {
        return documentContent;
    }

    /**
     * Define o valor da propriedade documentContent.
     * 
     * @param value
     *     allowed object is
     *     {@link DocumentContentType }
     *     
     */
    public void setDocumentContent(DocumentContentType value) {
        this.documentContent = value;
    }

    /**
     * Gets the value of the documentContentParam property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the documentContentParam property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDocumentContentParam().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DocumentContentParamType }
     * 
     * 
     */
    public List<DocumentContentParamType> getDocumentContentParam() {
        if (documentContentParam == null) {
            documentContentParam = new ArrayList<DocumentContentParamType>();
        }
        return this.documentContentParam;
    }

}
