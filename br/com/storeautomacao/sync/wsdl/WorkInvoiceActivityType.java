
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 *             Represents Lines of Work Invoice; List of activities performed by driver for the shipment.
 *          
 * 
 * <p>Classe Java de WorkInvoiceActivityType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="WorkInvoiceActivityType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ActivitySequence" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ActivityDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="SpecialServiceGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="SourceLocationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="DestLocationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="ActivityDistance" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDistanceType" minOccurs="0"/>
 *         &lt;element name="ActivityDuration" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDurationType" minOccurs="0"/>
 *         &lt;element name="WeightVolume" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}WeightVolumeType" minOccurs="0"/>
 *         &lt;element name="ShipUnitCount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ItemPackageCount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PayableIndicatorGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WorkInvoiceActivityType", propOrder = {
    "activitySequence",
    "activityDate",
    "specialServiceGid",
    "sourceLocationGid",
    "destLocationGid",
    "activityDistance",
    "activityDuration",
    "weightVolume",
    "shipUnitCount",
    "itemPackageCount",
    "payableIndicatorGid"
})
public class WorkInvoiceActivityType {

    @XmlElement(name = "ActivitySequence", required = true)
    protected String activitySequence;
    @XmlElement(name = "ActivityDate")
    protected GLogDateTimeType activityDate;
    @XmlElement(name = "SpecialServiceGid")
    protected GLogXMLGidType specialServiceGid;
    @XmlElement(name = "SourceLocationGid")
    protected GLogXMLGidType sourceLocationGid;
    @XmlElement(name = "DestLocationGid")
    protected GLogXMLGidType destLocationGid;
    @XmlElement(name = "ActivityDistance")
    protected GLogXMLDistanceType activityDistance;
    @XmlElement(name = "ActivityDuration")
    protected GLogXMLDurationType activityDuration;
    @XmlElement(name = "WeightVolume")
    protected WeightVolumeType weightVolume;
    @XmlElement(name = "ShipUnitCount")
    protected String shipUnitCount;
    @XmlElement(name = "ItemPackageCount")
    protected String itemPackageCount;
    @XmlElement(name = "PayableIndicatorGid")
    protected GLogXMLGidType payableIndicatorGid;

    /**
     * Obtém o valor da propriedade activitySequence.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActivitySequence() {
        return activitySequence;
    }

    /**
     * Define o valor da propriedade activitySequence.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActivitySequence(String value) {
        this.activitySequence = value;
    }

    /**
     * Obtém o valor da propriedade activityDate.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getActivityDate() {
        return activityDate;
    }

    /**
     * Define o valor da propriedade activityDate.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setActivityDate(GLogDateTimeType value) {
        this.activityDate = value;
    }

    /**
     * Obtém o valor da propriedade specialServiceGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getSpecialServiceGid() {
        return specialServiceGid;
    }

    /**
     * Define o valor da propriedade specialServiceGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setSpecialServiceGid(GLogXMLGidType value) {
        this.specialServiceGid = value;
    }

    /**
     * Obtém o valor da propriedade sourceLocationGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getSourceLocationGid() {
        return sourceLocationGid;
    }

    /**
     * Define o valor da propriedade sourceLocationGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setSourceLocationGid(GLogXMLGidType value) {
        this.sourceLocationGid = value;
    }

    /**
     * Obtém o valor da propriedade destLocationGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getDestLocationGid() {
        return destLocationGid;
    }

    /**
     * Define o valor da propriedade destLocationGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setDestLocationGid(GLogXMLGidType value) {
        this.destLocationGid = value;
    }

    /**
     * Obtém o valor da propriedade activityDistance.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDistanceType }
     *     
     */
    public GLogXMLDistanceType getActivityDistance() {
        return activityDistance;
    }

    /**
     * Define o valor da propriedade activityDistance.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDistanceType }
     *     
     */
    public void setActivityDistance(GLogXMLDistanceType value) {
        this.activityDistance = value;
    }

    /**
     * Obtém o valor da propriedade activityDuration.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public GLogXMLDurationType getActivityDuration() {
        return activityDuration;
    }

    /**
     * Define o valor da propriedade activityDuration.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public void setActivityDuration(GLogXMLDurationType value) {
        this.activityDuration = value;
    }

    /**
     * Obtém o valor da propriedade weightVolume.
     * 
     * @return
     *     possible object is
     *     {@link WeightVolumeType }
     *     
     */
    public WeightVolumeType getWeightVolume() {
        return weightVolume;
    }

    /**
     * Define o valor da propriedade weightVolume.
     * 
     * @param value
     *     allowed object is
     *     {@link WeightVolumeType }
     *     
     */
    public void setWeightVolume(WeightVolumeType value) {
        this.weightVolume = value;
    }

    /**
     * Obtém o valor da propriedade shipUnitCount.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipUnitCount() {
        return shipUnitCount;
    }

    /**
     * Define o valor da propriedade shipUnitCount.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipUnitCount(String value) {
        this.shipUnitCount = value;
    }

    /**
     * Obtém o valor da propriedade itemPackageCount.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItemPackageCount() {
        return itemPackageCount;
    }

    /**
     * Define o valor da propriedade itemPackageCount.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItemPackageCount(String value) {
        this.itemPackageCount = value;
    }

    /**
     * Obtém o valor da propriedade payableIndicatorGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getPayableIndicatorGid() {
        return payableIndicatorGid;
    }

    /**
     * Define o valor da propriedade payableIndicatorGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setPayableIndicatorGid(GLogXMLGidType value) {
        this.payableIndicatorGid = value;
    }

}
