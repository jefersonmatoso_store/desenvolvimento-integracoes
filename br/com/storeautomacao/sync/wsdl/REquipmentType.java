
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;


/**
 * REquipment specifies equipments associated with a Release.
 * 
 * <p>Classe Java de REquipmentType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="REquipmentType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="REquipmentSequence" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="IntSavedQuery" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}IntSavedQueryType" minOccurs="0"/>
 *         &lt;element name="TransactionCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransactionCodeType" minOccurs="0"/>
 *         &lt;element name="EquipmentGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="EquipmentGroupGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="EquipmentTypeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="EquipmentInitial" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EquipmentNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EquipmentInitialNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EquipmentSeal" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}EquipmentSealType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "REquipmentType", propOrder = {
    "rEquipmentSequence",
    "intSavedQuery",
    "transactionCode",
    "equipmentGid",
    "equipmentGroupGid",
    "equipmentTypeGid",
    "equipmentInitial",
    "equipmentNumber",
    "equipmentInitialNumber",
    "equipmentSeal"
})
public class REquipmentType {

    @XmlElement(name = "REquipmentSequence", required = true)
    protected String rEquipmentSequence;
    @XmlElement(name = "IntSavedQuery")
    protected IntSavedQueryType intSavedQuery;
    @XmlElement(name = "TransactionCode")
    @XmlSchemaType(name = "string")
    protected TransactionCodeType transactionCode;
    @XmlElement(name = "EquipmentGid")
    protected GLogXMLGidType equipmentGid;
    @XmlElement(name = "EquipmentGroupGid", required = true)
    protected GLogXMLGidType equipmentGroupGid;
    @XmlElement(name = "EquipmentTypeGid")
    protected GLogXMLGidType equipmentTypeGid;
    @XmlElement(name = "EquipmentInitial")
    protected String equipmentInitial;
    @XmlElement(name = "EquipmentNumber")
    protected String equipmentNumber;
    @XmlElement(name = "EquipmentInitialNumber")
    protected String equipmentInitialNumber;
    @XmlElement(name = "EquipmentSeal")
    protected List<EquipmentSealType> equipmentSeal;

    /**
     * Obtém o valor da propriedade rEquipmentSequence.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getREquipmentSequence() {
        return rEquipmentSequence;
    }

    /**
     * Define o valor da propriedade rEquipmentSequence.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setREquipmentSequence(String value) {
        this.rEquipmentSequence = value;
    }

    /**
     * Obtém o valor da propriedade intSavedQuery.
     * 
     * @return
     *     possible object is
     *     {@link IntSavedQueryType }
     *     
     */
    public IntSavedQueryType getIntSavedQuery() {
        return intSavedQuery;
    }

    /**
     * Define o valor da propriedade intSavedQuery.
     * 
     * @param value
     *     allowed object is
     *     {@link IntSavedQueryType }
     *     
     */
    public void setIntSavedQuery(IntSavedQueryType value) {
        this.intSavedQuery = value;
    }

    /**
     * Obtém o valor da propriedade transactionCode.
     * 
     * @return
     *     possible object is
     *     {@link TransactionCodeType }
     *     
     */
    public TransactionCodeType getTransactionCode() {
        return transactionCode;
    }

    /**
     * Define o valor da propriedade transactionCode.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionCodeType }
     *     
     */
    public void setTransactionCode(TransactionCodeType value) {
        this.transactionCode = value;
    }

    /**
     * Obtém o valor da propriedade equipmentGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getEquipmentGid() {
        return equipmentGid;
    }

    /**
     * Define o valor da propriedade equipmentGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setEquipmentGid(GLogXMLGidType value) {
        this.equipmentGid = value;
    }

    /**
     * Obtém o valor da propriedade equipmentGroupGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getEquipmentGroupGid() {
        return equipmentGroupGid;
    }

    /**
     * Define o valor da propriedade equipmentGroupGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setEquipmentGroupGid(GLogXMLGidType value) {
        this.equipmentGroupGid = value;
    }

    /**
     * Obtém o valor da propriedade equipmentTypeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getEquipmentTypeGid() {
        return equipmentTypeGid;
    }

    /**
     * Define o valor da propriedade equipmentTypeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setEquipmentTypeGid(GLogXMLGidType value) {
        this.equipmentTypeGid = value;
    }

    /**
     * Obtém o valor da propriedade equipmentInitial.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEquipmentInitial() {
        return equipmentInitial;
    }

    /**
     * Define o valor da propriedade equipmentInitial.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEquipmentInitial(String value) {
        this.equipmentInitial = value;
    }

    /**
     * Obtém o valor da propriedade equipmentNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEquipmentNumber() {
        return equipmentNumber;
    }

    /**
     * Define o valor da propriedade equipmentNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEquipmentNumber(String value) {
        this.equipmentNumber = value;
    }

    /**
     * Obtém o valor da propriedade equipmentInitialNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEquipmentInitialNumber() {
        return equipmentInitialNumber;
    }

    /**
     * Define o valor da propriedade equipmentInitialNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEquipmentInitialNumber(String value) {
        this.equipmentInitialNumber = value;
    }

    /**
     * Gets the value of the equipmentSeal property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the equipmentSeal property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEquipmentSeal().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EquipmentSealType }
     * 
     * 
     */
    public List<EquipmentSealType> getEquipmentSeal() {
        if (equipmentSeal == null) {
            equipmentSeal = new ArrayList<EquipmentSealType>();
        }
        return this.equipmentSeal;
    }

}
