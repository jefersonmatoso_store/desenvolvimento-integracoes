
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Specifies the relationship between an Invoice Line Item and the approve amounts on the Voucher.
 * 
 * <p>Classe Java de VoucherInvoiceLineItemType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="VoucherInvoiceLineItemType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="InvoiceGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="AssignedNum" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AmountToPay" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType"/>
 *         &lt;element name="AdjustmentReasonGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VoucherInvoiceLineItemType", propOrder = {
    "invoiceGid",
    "assignedNum",
    "amountToPay",
    "adjustmentReasonGid"
})
public class VoucherInvoiceLineItemType {

    @XmlElement(name = "InvoiceGid", required = true)
    protected GLogXMLGidType invoiceGid;
    @XmlElement(name = "AssignedNum", required = true)
    protected String assignedNum;
    @XmlElement(name = "AmountToPay", required = true)
    protected GLogXMLFinancialAmountType amountToPay;
    @XmlElement(name = "AdjustmentReasonGid")
    protected GLogXMLGidType adjustmentReasonGid;

    /**
     * Obtém o valor da propriedade invoiceGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getInvoiceGid() {
        return invoiceGid;
    }

    /**
     * Define o valor da propriedade invoiceGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setInvoiceGid(GLogXMLGidType value) {
        this.invoiceGid = value;
    }

    /**
     * Obtém o valor da propriedade assignedNum.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAssignedNum() {
        return assignedNum;
    }

    /**
     * Define o valor da propriedade assignedNum.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAssignedNum(String value) {
        this.assignedNum = value;
    }

    /**
     * Obtém o valor da propriedade amountToPay.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getAmountToPay() {
        return amountToPay;
    }

    /**
     * Define o valor da propriedade amountToPay.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setAmountToPay(GLogXMLFinancialAmountType value) {
        this.amountToPay = value;
    }

    /**
     * Obtém o valor da propriedade adjustmentReasonGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getAdjustmentReasonGid() {
        return adjustmentReasonGid;
    }

    /**
     * Define o valor da propriedade adjustmentReasonGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setAdjustmentReasonGid(GLogXMLGidType value) {
        this.adjustmentReasonGid = value;
    }

}
