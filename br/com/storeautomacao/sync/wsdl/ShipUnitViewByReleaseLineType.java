
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Provides a ReleaseLine centric view of the ShipUnit(s) on the Shipment.
 * 
 * <p>Classe Java de ShipUnitViewByReleaseLineType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="ShipUnitViewByReleaseLineType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ReleaseLineGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="ShipUnitView" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ShipUnitViewType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ShipUnitViewByReleaseLineType", propOrder = {
    "releaseLineGid",
    "shipUnitView"
})
public class ShipUnitViewByReleaseLineType {

    @XmlElement(name = "ReleaseLineGid", required = true)
    protected GLogXMLGidType releaseLineGid;
    @XmlElement(name = "ShipUnitView", required = true)
    protected ShipUnitViewType shipUnitView;

    /**
     * Obtém o valor da propriedade releaseLineGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getReleaseLineGid() {
        return releaseLineGid;
    }

    /**
     * Define o valor da propriedade releaseLineGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setReleaseLineGid(GLogXMLGidType value) {
        this.releaseLineGid = value;
    }

    /**
     * Obtém o valor da propriedade shipUnitView.
     * 
     * @return
     *     possible object is
     *     {@link ShipUnitViewType }
     *     
     */
    public ShipUnitViewType getShipUnitView() {
        return shipUnitView;
    }

    /**
     * Define o valor da propriedade shipUnitView.
     * 
     * @param value
     *     allowed object is
     *     {@link ShipUnitViewType }
     *     
     */
    public void setShipUnitView(ShipUnitViewType value) {
        this.shipUnitView = value;
    }

}
