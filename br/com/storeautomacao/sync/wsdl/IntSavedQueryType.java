
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * Integration Saved Queries provide a means to query for Gid(s) using a combination of SQL and XPath
 *             expressions referencing information in the XML. A basic example of an Integration Saved Query that
 *             uses the ShipUnit Content Tracking Tag number in the XML to lookup the Gid in the database would resemble the following:
 * 
 *             select distinct s_ship_unit_gid from s_ship_unit_line where tracking_tag1 = '{ShipUnit/ShipUnitContent[1]/ItemQuantity/ItemTag1}'
 * 
 *             The Xpath expression in the braces, which referes to the ItemQuantity/ItemTag element in the first
 *             ShipUnitContent element of the ShipUnit, will be replaced with the value in the XML document before
 *             the query is initiated.
 *          
 * 
 * <p>Classe Java de IntSavedQueryType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="IntSavedQueryType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IntSavedQueryGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="IntSavedQueryArg" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="ArgName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="ArgValue" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="IsMultiMatch" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MaximumMatch" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NoDataFoundAction" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IntSavedQueryType", propOrder = {
    "intSavedQueryGid",
    "intSavedQueryArg",
    "isMultiMatch",
    "maximumMatch",
    "noDataFoundAction"
})
public class IntSavedQueryType {

    @XmlElement(name = "IntSavedQueryGid", required = true)
    protected GLogXMLGidType intSavedQueryGid;
    @XmlElement(name = "IntSavedQueryArg")
    protected List<IntSavedQueryType.IntSavedQueryArg> intSavedQueryArg;
    @XmlElement(name = "IsMultiMatch")
    protected String isMultiMatch;
    @XmlElement(name = "MaximumMatch")
    protected String maximumMatch;
    @XmlElement(name = "NoDataFoundAction")
    protected String noDataFoundAction;

    /**
     * Obtém o valor da propriedade intSavedQueryGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getIntSavedQueryGid() {
        return intSavedQueryGid;
    }

    /**
     * Define o valor da propriedade intSavedQueryGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setIntSavedQueryGid(GLogXMLGidType value) {
        this.intSavedQueryGid = value;
    }

    /**
     * Gets the value of the intSavedQueryArg property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the intSavedQueryArg property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getIntSavedQueryArg().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link IntSavedQueryType.IntSavedQueryArg }
     * 
     * 
     */
    public List<IntSavedQueryType.IntSavedQueryArg> getIntSavedQueryArg() {
        if (intSavedQueryArg == null) {
            intSavedQueryArg = new ArrayList<IntSavedQueryType.IntSavedQueryArg>();
        }
        return this.intSavedQueryArg;
    }

    /**
     * Obtém o valor da propriedade isMultiMatch.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsMultiMatch() {
        return isMultiMatch;
    }

    /**
     * Define o valor da propriedade isMultiMatch.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsMultiMatch(String value) {
        this.isMultiMatch = value;
    }

    /**
     * Obtém o valor da propriedade maximumMatch.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMaximumMatch() {
        return maximumMatch;
    }

    /**
     * Define o valor da propriedade maximumMatch.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMaximumMatch(String value) {
        this.maximumMatch = value;
    }

    /**
     * Obtém o valor da propriedade noDataFoundAction.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNoDataFoundAction() {
        return noDataFoundAction;
    }

    /**
     * Define o valor da propriedade noDataFoundAction.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNoDataFoundAction(String value) {
        this.noDataFoundAction = value;
    }


    /**
     * <p>Classe Java de anonymous complex type.
     * 
     * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="ArgName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="ArgValue" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "argName",
        "argValue"
    })
    public static class IntSavedQueryArg {

        @XmlElement(name = "ArgName", required = true)
        protected String argName;
        @XmlElement(name = "ArgValue", required = true)
        protected String argValue;

        /**
         * Obtém o valor da propriedade argName.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getArgName() {
            return argName;
        }

        /**
         * Define o valor da propriedade argName.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setArgName(String value) {
            this.argName = value;
        }

        /**
         * Obtém o valor da propriedade argValue.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getArgValue() {
            return argValue;
        }

        /**
         * Define o valor da propriedade argValue.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setArgValue(String value) {
            this.argValue = value;
        }

    }

}
