
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * The ShipmentHeader2 element contains additional shipment header information.
 *             It is defined only to limit the size of the ShipmentHeader element.
 *          
 * 
 * <p>Classe Java de ShipmentHeader2Type complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="ShipmentHeader2Type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IsAutoMergeConsolidate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Perspective" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsPrimary" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ItineraryGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="ParentLegGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="FlightInstanceId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IntermediaryCorporationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="ShipmentAsWork" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FeasibilityCodeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="CheckTimeConstraint" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CheckCostConstraint" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CheckCapacityConstraint" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CmName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CmSequenceNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CmCost" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/>
 *         &lt;element name="CmEmptyDistance" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDistanceType" minOccurs="0"/>
 *         &lt;element name="CmTotalShipmentNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CmPrevDestLocationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocGidType" minOccurs="0"/>
 *         &lt;element name="CmNextSourceLocationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocGidType" minOccurs="0"/>
 *         &lt;element name="HazmatModeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="HazmatRegionGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLRegionGidType" minOccurs="0"/>
 *         &lt;element name="WeighCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Rule7" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ShipmentReleased" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ShipmentTypeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="DimWeight" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLWeightType" minOccurs="0"/>
 *         &lt;element name="ChargeableWeight" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLWeightType" minOccurs="0"/>
 *         &lt;element name="Rail" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RailType" minOccurs="0"/>
 *         &lt;element name="SecondaryChargeReference" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}SecondaryChargeReferenceType" minOccurs="0"/>
 *         &lt;element name="IsPreload" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsToBeHeld" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AutoGenerateRelease" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BulkPlanGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="DriverAssignBulkPlanGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="EquipmentAssignBulkPlanGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="BulkContMoveGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="InTrailerBuild" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LastEventGroupGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="RepetitionScheduleGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="ScheduleType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="WeightUtil" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="VolumeUtil" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EquipRefUnitUtil" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="HasAppointments" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="JobGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="IsCreditNote" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NFRCRuleGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="ConsolGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="DutyPaid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsMemoBL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsProfitSplit" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsAdvancedCharge" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AppointmentPriorityGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="ShippingAgentContactGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="LoadConfigEngineTypeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="FixedServiceDays" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SightingLocGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="SightingDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="PrevSightingLocGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="PrevSightingDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="NumEquipmentOrdered" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CombinationEquipmentGrpGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="ShipmentGroupHeader" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ShipmentGroupHeaderType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="SrcArbLevelOfServiceGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="DestArbLevelOfServiceGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ShipmentHeader2Type", propOrder = {
    "isAutoMergeConsolidate",
    "perspective",
    "isPrimary",
    "itineraryGid",
    "parentLegGid",
    "flightInstanceId",
    "intermediaryCorporationGid",
    "shipmentAsWork",
    "feasibilityCodeGid",
    "checkTimeConstraint",
    "checkCostConstraint",
    "checkCapacityConstraint",
    "cmName",
    "cmSequenceNum",
    "cmCost",
    "cmEmptyDistance",
    "cmTotalShipmentNum",
    "cmPrevDestLocationGid",
    "cmNextSourceLocationGid",
    "hazmatModeGid",
    "hazmatRegionGid",
    "weighCode",
    "rule7",
    "shipmentReleased",
    "shipmentTypeGid",
    "dimWeight",
    "chargeableWeight",
    "rail",
    "secondaryChargeReference",
    "isPreload",
    "isToBeHeld",
    "autoGenerateRelease",
    "bulkPlanGid",
    "driverAssignBulkPlanGid",
    "equipmentAssignBulkPlanGid",
    "bulkContMoveGid",
    "inTrailerBuild",
    "lastEventGroupGid",
    "repetitionScheduleGid",
    "scheduleType",
    "weightUtil",
    "volumeUtil",
    "equipRefUnitUtil",
    "hasAppointments",
    "jobGid",
    "isCreditNote",
    "nfrcRuleGid",
    "consolGid",
    "dutyPaid",
    "isMemoBL",
    "isProfitSplit",
    "isAdvancedCharge",
    "appointmentPriorityGid",
    "shippingAgentContactGid",
    "loadConfigEngineTypeGid",
    "fixedServiceDays",
    "sightingLocGid",
    "sightingDate",
    "prevSightingLocGid",
    "prevSightingDate",
    "numEquipmentOrdered",
    "combinationEquipmentGrpGid",
    "shipmentGroupHeader",
    "srcArbLevelOfServiceGid",
    "destArbLevelOfServiceGid"
})
public class ShipmentHeader2Type {

    @XmlElement(name = "IsAutoMergeConsolidate")
    protected String isAutoMergeConsolidate;
    @XmlElement(name = "Perspective")
    protected String perspective;
    @XmlElement(name = "IsPrimary")
    protected String isPrimary;
    @XmlElement(name = "ItineraryGid")
    protected GLogXMLGidType itineraryGid;
    @XmlElement(name = "ParentLegGid")
    protected GLogXMLGidType parentLegGid;
    @XmlElement(name = "FlightInstanceId")
    protected String flightInstanceId;
    @XmlElement(name = "IntermediaryCorporationGid")
    protected GLogXMLGidType intermediaryCorporationGid;
    @XmlElement(name = "ShipmentAsWork")
    protected String shipmentAsWork;
    @XmlElement(name = "FeasibilityCodeGid")
    protected GLogXMLGidType feasibilityCodeGid;
    @XmlElement(name = "CheckTimeConstraint")
    protected String checkTimeConstraint;
    @XmlElement(name = "CheckCostConstraint")
    protected String checkCostConstraint;
    @XmlElement(name = "CheckCapacityConstraint")
    protected String checkCapacityConstraint;
    @XmlElement(name = "CmName")
    protected String cmName;
    @XmlElement(name = "CmSequenceNum")
    protected String cmSequenceNum;
    @XmlElement(name = "CmCost")
    protected GLogXMLFinancialAmountType cmCost;
    @XmlElement(name = "CmEmptyDistance")
    protected GLogXMLDistanceType cmEmptyDistance;
    @XmlElement(name = "CmTotalShipmentNum")
    protected String cmTotalShipmentNum;
    @XmlElement(name = "CmPrevDestLocationGid")
    protected GLogXMLLocGidType cmPrevDestLocationGid;
    @XmlElement(name = "CmNextSourceLocationGid")
    protected GLogXMLLocGidType cmNextSourceLocationGid;
    @XmlElement(name = "HazmatModeGid")
    protected GLogXMLGidType hazmatModeGid;
    @XmlElement(name = "HazmatRegionGid")
    protected GLogXMLRegionGidType hazmatRegionGid;
    @XmlElement(name = "WeighCode")
    protected String weighCode;
    @XmlElement(name = "Rule7")
    protected String rule7;
    @XmlElement(name = "ShipmentReleased")
    protected String shipmentReleased;
    @XmlElement(name = "ShipmentTypeGid")
    protected GLogXMLGidType shipmentTypeGid;
    @XmlElement(name = "DimWeight")
    protected GLogXMLWeightType dimWeight;
    @XmlElement(name = "ChargeableWeight")
    protected GLogXMLWeightType chargeableWeight;
    @XmlElement(name = "Rail")
    protected RailType rail;
    @XmlElement(name = "SecondaryChargeReference")
    protected SecondaryChargeReferenceType secondaryChargeReference;
    @XmlElement(name = "IsPreload")
    protected String isPreload;
    @XmlElement(name = "IsToBeHeld")
    protected String isToBeHeld;
    @XmlElement(name = "AutoGenerateRelease")
    protected String autoGenerateRelease;
    @XmlElement(name = "BulkPlanGid")
    protected GLogXMLGidType bulkPlanGid;
    @XmlElement(name = "DriverAssignBulkPlanGid")
    protected GLogXMLGidType driverAssignBulkPlanGid;
    @XmlElement(name = "EquipmentAssignBulkPlanGid")
    protected GLogXMLGidType equipmentAssignBulkPlanGid;
    @XmlElement(name = "BulkContMoveGid")
    protected GLogXMLGidType bulkContMoveGid;
    @XmlElement(name = "InTrailerBuild")
    protected String inTrailerBuild;
    @XmlElement(name = "LastEventGroupGid")
    protected GLogXMLGidType lastEventGroupGid;
    @XmlElement(name = "RepetitionScheduleGid")
    protected GLogXMLGidType repetitionScheduleGid;
    @XmlElement(name = "ScheduleType")
    protected String scheduleType;
    @XmlElement(name = "WeightUtil")
    protected String weightUtil;
    @XmlElement(name = "VolumeUtil")
    protected String volumeUtil;
    @XmlElement(name = "EquipRefUnitUtil")
    protected String equipRefUnitUtil;
    @XmlElement(name = "HasAppointments")
    protected String hasAppointments;
    @XmlElement(name = "JobGid")
    protected GLogXMLGidType jobGid;
    @XmlElement(name = "IsCreditNote")
    protected String isCreditNote;
    @XmlElement(name = "NFRCRuleGid")
    protected GLogXMLGidType nfrcRuleGid;
    @XmlElement(name = "ConsolGid")
    protected GLogXMLGidType consolGid;
    @XmlElement(name = "DutyPaid")
    protected String dutyPaid;
    @XmlElement(name = "IsMemoBL")
    protected String isMemoBL;
    @XmlElement(name = "IsProfitSplit")
    protected String isProfitSplit;
    @XmlElement(name = "IsAdvancedCharge")
    protected String isAdvancedCharge;
    @XmlElement(name = "AppointmentPriorityGid")
    protected GLogXMLGidType appointmentPriorityGid;
    @XmlElement(name = "ShippingAgentContactGid")
    protected GLogXMLGidType shippingAgentContactGid;
    @XmlElement(name = "LoadConfigEngineTypeGid")
    protected GLogXMLGidType loadConfigEngineTypeGid;
    @XmlElement(name = "FixedServiceDays")
    protected String fixedServiceDays;
    @XmlElement(name = "SightingLocGid")
    protected GLogXMLGidType sightingLocGid;
    @XmlElement(name = "SightingDate")
    protected GLogDateTimeType sightingDate;
    @XmlElement(name = "PrevSightingLocGid")
    protected GLogXMLGidType prevSightingLocGid;
    @XmlElement(name = "PrevSightingDate")
    protected GLogDateTimeType prevSightingDate;
    @XmlElement(name = "NumEquipmentOrdered")
    protected String numEquipmentOrdered;
    @XmlElement(name = "CombinationEquipmentGrpGid")
    protected GLogXMLGidType combinationEquipmentGrpGid;
    @XmlElement(name = "ShipmentGroupHeader")
    protected List<ShipmentGroupHeaderType> shipmentGroupHeader;
    @XmlElement(name = "SrcArbLevelOfServiceGid")
    protected GLogXMLGidType srcArbLevelOfServiceGid;
    @XmlElement(name = "DestArbLevelOfServiceGid")
    protected GLogXMLGidType destArbLevelOfServiceGid;

    /**
     * Obtém o valor da propriedade isAutoMergeConsolidate.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsAutoMergeConsolidate() {
        return isAutoMergeConsolidate;
    }

    /**
     * Define o valor da propriedade isAutoMergeConsolidate.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsAutoMergeConsolidate(String value) {
        this.isAutoMergeConsolidate = value;
    }

    /**
     * Obtém o valor da propriedade perspective.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPerspective() {
        return perspective;
    }

    /**
     * Define o valor da propriedade perspective.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPerspective(String value) {
        this.perspective = value;
    }

    /**
     * Obtém o valor da propriedade isPrimary.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsPrimary() {
        return isPrimary;
    }

    /**
     * Define o valor da propriedade isPrimary.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsPrimary(String value) {
        this.isPrimary = value;
    }

    /**
     * Obtém o valor da propriedade itineraryGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getItineraryGid() {
        return itineraryGid;
    }

    /**
     * Define o valor da propriedade itineraryGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setItineraryGid(GLogXMLGidType value) {
        this.itineraryGid = value;
    }

    /**
     * Obtém o valor da propriedade parentLegGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getParentLegGid() {
        return parentLegGid;
    }

    /**
     * Define o valor da propriedade parentLegGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setParentLegGid(GLogXMLGidType value) {
        this.parentLegGid = value;
    }

    /**
     * Obtém o valor da propriedade flightInstanceId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlightInstanceId() {
        return flightInstanceId;
    }

    /**
     * Define o valor da propriedade flightInstanceId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlightInstanceId(String value) {
        this.flightInstanceId = value;
    }

    /**
     * Obtém o valor da propriedade intermediaryCorporationGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getIntermediaryCorporationGid() {
        return intermediaryCorporationGid;
    }

    /**
     * Define o valor da propriedade intermediaryCorporationGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setIntermediaryCorporationGid(GLogXMLGidType value) {
        this.intermediaryCorporationGid = value;
    }

    /**
     * Obtém o valor da propriedade shipmentAsWork.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipmentAsWork() {
        return shipmentAsWork;
    }

    /**
     * Define o valor da propriedade shipmentAsWork.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipmentAsWork(String value) {
        this.shipmentAsWork = value;
    }

    /**
     * Obtém o valor da propriedade feasibilityCodeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getFeasibilityCodeGid() {
        return feasibilityCodeGid;
    }

    /**
     * Define o valor da propriedade feasibilityCodeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setFeasibilityCodeGid(GLogXMLGidType value) {
        this.feasibilityCodeGid = value;
    }

    /**
     * Obtém o valor da propriedade checkTimeConstraint.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCheckTimeConstraint() {
        return checkTimeConstraint;
    }

    /**
     * Define o valor da propriedade checkTimeConstraint.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCheckTimeConstraint(String value) {
        this.checkTimeConstraint = value;
    }

    /**
     * Obtém o valor da propriedade checkCostConstraint.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCheckCostConstraint() {
        return checkCostConstraint;
    }

    /**
     * Define o valor da propriedade checkCostConstraint.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCheckCostConstraint(String value) {
        this.checkCostConstraint = value;
    }

    /**
     * Obtém o valor da propriedade checkCapacityConstraint.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCheckCapacityConstraint() {
        return checkCapacityConstraint;
    }

    /**
     * Define o valor da propriedade checkCapacityConstraint.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCheckCapacityConstraint(String value) {
        this.checkCapacityConstraint = value;
    }

    /**
     * Obtém o valor da propriedade cmName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCmName() {
        return cmName;
    }

    /**
     * Define o valor da propriedade cmName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCmName(String value) {
        this.cmName = value;
    }

    /**
     * Obtém o valor da propriedade cmSequenceNum.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCmSequenceNum() {
        return cmSequenceNum;
    }

    /**
     * Define o valor da propriedade cmSequenceNum.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCmSequenceNum(String value) {
        this.cmSequenceNum = value;
    }

    /**
     * Obtém o valor da propriedade cmCost.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getCmCost() {
        return cmCost;
    }

    /**
     * Define o valor da propriedade cmCost.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setCmCost(GLogXMLFinancialAmountType value) {
        this.cmCost = value;
    }

    /**
     * Obtém o valor da propriedade cmEmptyDistance.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDistanceType }
     *     
     */
    public GLogXMLDistanceType getCmEmptyDistance() {
        return cmEmptyDistance;
    }

    /**
     * Define o valor da propriedade cmEmptyDistance.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDistanceType }
     *     
     */
    public void setCmEmptyDistance(GLogXMLDistanceType value) {
        this.cmEmptyDistance = value;
    }

    /**
     * Obtém o valor da propriedade cmTotalShipmentNum.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCmTotalShipmentNum() {
        return cmTotalShipmentNum;
    }

    /**
     * Define o valor da propriedade cmTotalShipmentNum.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCmTotalShipmentNum(String value) {
        this.cmTotalShipmentNum = value;
    }

    /**
     * Obtém o valor da propriedade cmPrevDestLocationGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocGidType }
     *     
     */
    public GLogXMLLocGidType getCmPrevDestLocationGid() {
        return cmPrevDestLocationGid;
    }

    /**
     * Define o valor da propriedade cmPrevDestLocationGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocGidType }
     *     
     */
    public void setCmPrevDestLocationGid(GLogXMLLocGidType value) {
        this.cmPrevDestLocationGid = value;
    }

    /**
     * Obtém o valor da propriedade cmNextSourceLocationGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocGidType }
     *     
     */
    public GLogXMLLocGidType getCmNextSourceLocationGid() {
        return cmNextSourceLocationGid;
    }

    /**
     * Define o valor da propriedade cmNextSourceLocationGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocGidType }
     *     
     */
    public void setCmNextSourceLocationGid(GLogXMLLocGidType value) {
        this.cmNextSourceLocationGid = value;
    }

    /**
     * Obtém o valor da propriedade hazmatModeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getHazmatModeGid() {
        return hazmatModeGid;
    }

    /**
     * Define o valor da propriedade hazmatModeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setHazmatModeGid(GLogXMLGidType value) {
        this.hazmatModeGid = value;
    }

    /**
     * Obtém o valor da propriedade hazmatRegionGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLRegionGidType }
     *     
     */
    public GLogXMLRegionGidType getHazmatRegionGid() {
        return hazmatRegionGid;
    }

    /**
     * Define o valor da propriedade hazmatRegionGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLRegionGidType }
     *     
     */
    public void setHazmatRegionGid(GLogXMLRegionGidType value) {
        this.hazmatRegionGid = value;
    }

    /**
     * Obtém o valor da propriedade weighCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWeighCode() {
        return weighCode;
    }

    /**
     * Define o valor da propriedade weighCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWeighCode(String value) {
        this.weighCode = value;
    }

    /**
     * Obtém o valor da propriedade rule7.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRule7() {
        return rule7;
    }

    /**
     * Define o valor da propriedade rule7.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRule7(String value) {
        this.rule7 = value;
    }

    /**
     * Obtém o valor da propriedade shipmentReleased.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipmentReleased() {
        return shipmentReleased;
    }

    /**
     * Define o valor da propriedade shipmentReleased.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipmentReleased(String value) {
        this.shipmentReleased = value;
    }

    /**
     * Obtém o valor da propriedade shipmentTypeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getShipmentTypeGid() {
        return shipmentTypeGid;
    }

    /**
     * Define o valor da propriedade shipmentTypeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setShipmentTypeGid(GLogXMLGidType value) {
        this.shipmentTypeGid = value;
    }

    /**
     * Obtém o valor da propriedade dimWeight.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLWeightType }
     *     
     */
    public GLogXMLWeightType getDimWeight() {
        return dimWeight;
    }

    /**
     * Define o valor da propriedade dimWeight.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLWeightType }
     *     
     */
    public void setDimWeight(GLogXMLWeightType value) {
        this.dimWeight = value;
    }

    /**
     * Obtém o valor da propriedade chargeableWeight.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLWeightType }
     *     
     */
    public GLogXMLWeightType getChargeableWeight() {
        return chargeableWeight;
    }

    /**
     * Define o valor da propriedade chargeableWeight.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLWeightType }
     *     
     */
    public void setChargeableWeight(GLogXMLWeightType value) {
        this.chargeableWeight = value;
    }

    /**
     * Obtém o valor da propriedade rail.
     * 
     * @return
     *     possible object is
     *     {@link RailType }
     *     
     */
    public RailType getRail() {
        return rail;
    }

    /**
     * Define o valor da propriedade rail.
     * 
     * @param value
     *     allowed object is
     *     {@link RailType }
     *     
     */
    public void setRail(RailType value) {
        this.rail = value;
    }

    /**
     * Obtém o valor da propriedade secondaryChargeReference.
     * 
     * @return
     *     possible object is
     *     {@link SecondaryChargeReferenceType }
     *     
     */
    public SecondaryChargeReferenceType getSecondaryChargeReference() {
        return secondaryChargeReference;
    }

    /**
     * Define o valor da propriedade secondaryChargeReference.
     * 
     * @param value
     *     allowed object is
     *     {@link SecondaryChargeReferenceType }
     *     
     */
    public void setSecondaryChargeReference(SecondaryChargeReferenceType value) {
        this.secondaryChargeReference = value;
    }

    /**
     * Obtém o valor da propriedade isPreload.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsPreload() {
        return isPreload;
    }

    /**
     * Define o valor da propriedade isPreload.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsPreload(String value) {
        this.isPreload = value;
    }

    /**
     * Obtém o valor da propriedade isToBeHeld.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsToBeHeld() {
        return isToBeHeld;
    }

    /**
     * Define o valor da propriedade isToBeHeld.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsToBeHeld(String value) {
        this.isToBeHeld = value;
    }

    /**
     * Obtém o valor da propriedade autoGenerateRelease.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAutoGenerateRelease() {
        return autoGenerateRelease;
    }

    /**
     * Define o valor da propriedade autoGenerateRelease.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAutoGenerateRelease(String value) {
        this.autoGenerateRelease = value;
    }

    /**
     * Obtém o valor da propriedade bulkPlanGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getBulkPlanGid() {
        return bulkPlanGid;
    }

    /**
     * Define o valor da propriedade bulkPlanGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setBulkPlanGid(GLogXMLGidType value) {
        this.bulkPlanGid = value;
    }

    /**
     * Obtém o valor da propriedade driverAssignBulkPlanGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getDriverAssignBulkPlanGid() {
        return driverAssignBulkPlanGid;
    }

    /**
     * Define o valor da propriedade driverAssignBulkPlanGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setDriverAssignBulkPlanGid(GLogXMLGidType value) {
        this.driverAssignBulkPlanGid = value;
    }

    /**
     * Obtém o valor da propriedade equipmentAssignBulkPlanGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getEquipmentAssignBulkPlanGid() {
        return equipmentAssignBulkPlanGid;
    }

    /**
     * Define o valor da propriedade equipmentAssignBulkPlanGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setEquipmentAssignBulkPlanGid(GLogXMLGidType value) {
        this.equipmentAssignBulkPlanGid = value;
    }

    /**
     * Obtém o valor da propriedade bulkContMoveGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getBulkContMoveGid() {
        return bulkContMoveGid;
    }

    /**
     * Define o valor da propriedade bulkContMoveGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setBulkContMoveGid(GLogXMLGidType value) {
        this.bulkContMoveGid = value;
    }

    /**
     * Obtém o valor da propriedade inTrailerBuild.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInTrailerBuild() {
        return inTrailerBuild;
    }

    /**
     * Define o valor da propriedade inTrailerBuild.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInTrailerBuild(String value) {
        this.inTrailerBuild = value;
    }

    /**
     * Obtém o valor da propriedade lastEventGroupGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getLastEventGroupGid() {
        return lastEventGroupGid;
    }

    /**
     * Define o valor da propriedade lastEventGroupGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setLastEventGroupGid(GLogXMLGidType value) {
        this.lastEventGroupGid = value;
    }

    /**
     * Obtém o valor da propriedade repetitionScheduleGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRepetitionScheduleGid() {
        return repetitionScheduleGid;
    }

    /**
     * Define o valor da propriedade repetitionScheduleGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRepetitionScheduleGid(GLogXMLGidType value) {
        this.repetitionScheduleGid = value;
    }

    /**
     * Obtém o valor da propriedade scheduleType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getScheduleType() {
        return scheduleType;
    }

    /**
     * Define o valor da propriedade scheduleType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setScheduleType(String value) {
        this.scheduleType = value;
    }

    /**
     * Obtém o valor da propriedade weightUtil.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWeightUtil() {
        return weightUtil;
    }

    /**
     * Define o valor da propriedade weightUtil.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWeightUtil(String value) {
        this.weightUtil = value;
    }

    /**
     * Obtém o valor da propriedade volumeUtil.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVolumeUtil() {
        return volumeUtil;
    }

    /**
     * Define o valor da propriedade volumeUtil.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVolumeUtil(String value) {
        this.volumeUtil = value;
    }

    /**
     * Obtém o valor da propriedade equipRefUnitUtil.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEquipRefUnitUtil() {
        return equipRefUnitUtil;
    }

    /**
     * Define o valor da propriedade equipRefUnitUtil.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEquipRefUnitUtil(String value) {
        this.equipRefUnitUtil = value;
    }

    /**
     * Obtém o valor da propriedade hasAppointments.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHasAppointments() {
        return hasAppointments;
    }

    /**
     * Define o valor da propriedade hasAppointments.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHasAppointments(String value) {
        this.hasAppointments = value;
    }

    /**
     * Obtém o valor da propriedade jobGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getJobGid() {
        return jobGid;
    }

    /**
     * Define o valor da propriedade jobGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setJobGid(GLogXMLGidType value) {
        this.jobGid = value;
    }

    /**
     * Obtém o valor da propriedade isCreditNote.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsCreditNote() {
        return isCreditNote;
    }

    /**
     * Define o valor da propriedade isCreditNote.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsCreditNote(String value) {
        this.isCreditNote = value;
    }

    /**
     * Obtém o valor da propriedade nfrcRuleGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getNFRCRuleGid() {
        return nfrcRuleGid;
    }

    /**
     * Define o valor da propriedade nfrcRuleGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setNFRCRuleGid(GLogXMLGidType value) {
        this.nfrcRuleGid = value;
    }

    /**
     * Obtém o valor da propriedade consolGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getConsolGid() {
        return consolGid;
    }

    /**
     * Define o valor da propriedade consolGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setConsolGid(GLogXMLGidType value) {
        this.consolGid = value;
    }

    /**
     * Obtém o valor da propriedade dutyPaid.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDutyPaid() {
        return dutyPaid;
    }

    /**
     * Define o valor da propriedade dutyPaid.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDutyPaid(String value) {
        this.dutyPaid = value;
    }

    /**
     * Obtém o valor da propriedade isMemoBL.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsMemoBL() {
        return isMemoBL;
    }

    /**
     * Define o valor da propriedade isMemoBL.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsMemoBL(String value) {
        this.isMemoBL = value;
    }

    /**
     * Obtém o valor da propriedade isProfitSplit.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsProfitSplit() {
        return isProfitSplit;
    }

    /**
     * Define o valor da propriedade isProfitSplit.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsProfitSplit(String value) {
        this.isProfitSplit = value;
    }

    /**
     * Obtém o valor da propriedade isAdvancedCharge.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsAdvancedCharge() {
        return isAdvancedCharge;
    }

    /**
     * Define o valor da propriedade isAdvancedCharge.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsAdvancedCharge(String value) {
        this.isAdvancedCharge = value;
    }

    /**
     * Obtém o valor da propriedade appointmentPriorityGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getAppointmentPriorityGid() {
        return appointmentPriorityGid;
    }

    /**
     * Define o valor da propriedade appointmentPriorityGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setAppointmentPriorityGid(GLogXMLGidType value) {
        this.appointmentPriorityGid = value;
    }

    /**
     * Obtém o valor da propriedade shippingAgentContactGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getShippingAgentContactGid() {
        return shippingAgentContactGid;
    }

    /**
     * Define o valor da propriedade shippingAgentContactGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setShippingAgentContactGid(GLogXMLGidType value) {
        this.shippingAgentContactGid = value;
    }

    /**
     * Obtém o valor da propriedade loadConfigEngineTypeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getLoadConfigEngineTypeGid() {
        return loadConfigEngineTypeGid;
    }

    /**
     * Define o valor da propriedade loadConfigEngineTypeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setLoadConfigEngineTypeGid(GLogXMLGidType value) {
        this.loadConfigEngineTypeGid = value;
    }

    /**
     * Obtém o valor da propriedade fixedServiceDays.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFixedServiceDays() {
        return fixedServiceDays;
    }

    /**
     * Define o valor da propriedade fixedServiceDays.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFixedServiceDays(String value) {
        this.fixedServiceDays = value;
    }

    /**
     * Obtém o valor da propriedade sightingLocGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getSightingLocGid() {
        return sightingLocGid;
    }

    /**
     * Define o valor da propriedade sightingLocGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setSightingLocGid(GLogXMLGidType value) {
        this.sightingLocGid = value;
    }

    /**
     * Obtém o valor da propriedade sightingDate.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getSightingDate() {
        return sightingDate;
    }

    /**
     * Define o valor da propriedade sightingDate.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setSightingDate(GLogDateTimeType value) {
        this.sightingDate = value;
    }

    /**
     * Obtém o valor da propriedade prevSightingLocGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getPrevSightingLocGid() {
        return prevSightingLocGid;
    }

    /**
     * Define o valor da propriedade prevSightingLocGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setPrevSightingLocGid(GLogXMLGidType value) {
        this.prevSightingLocGid = value;
    }

    /**
     * Obtém o valor da propriedade prevSightingDate.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getPrevSightingDate() {
        return prevSightingDate;
    }

    /**
     * Define o valor da propriedade prevSightingDate.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setPrevSightingDate(GLogDateTimeType value) {
        this.prevSightingDate = value;
    }

    /**
     * Obtém o valor da propriedade numEquipmentOrdered.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumEquipmentOrdered() {
        return numEquipmentOrdered;
    }

    /**
     * Define o valor da propriedade numEquipmentOrdered.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumEquipmentOrdered(String value) {
        this.numEquipmentOrdered = value;
    }

    /**
     * Obtém o valor da propriedade combinationEquipmentGrpGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getCombinationEquipmentGrpGid() {
        return combinationEquipmentGrpGid;
    }

    /**
     * Define o valor da propriedade combinationEquipmentGrpGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setCombinationEquipmentGrpGid(GLogXMLGidType value) {
        this.combinationEquipmentGrpGid = value;
    }

    /**
     * Gets the value of the shipmentGroupHeader property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the shipmentGroupHeader property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getShipmentGroupHeader().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ShipmentGroupHeaderType }
     * 
     * 
     */
    public List<ShipmentGroupHeaderType> getShipmentGroupHeader() {
        if (shipmentGroupHeader == null) {
            shipmentGroupHeader = new ArrayList<ShipmentGroupHeaderType>();
        }
        return this.shipmentGroupHeader;
    }

    /**
     * Obtém o valor da propriedade srcArbLevelOfServiceGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getSrcArbLevelOfServiceGid() {
        return srcArbLevelOfServiceGid;
    }

    /**
     * Define o valor da propriedade srcArbLevelOfServiceGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setSrcArbLevelOfServiceGid(GLogXMLGidType value) {
        this.srcArbLevelOfServiceGid = value;
    }

    /**
     * Obtém o valor da propriedade destArbLevelOfServiceGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getDestArbLevelOfServiceGid() {
        return destArbLevelOfServiceGid;
    }

    /**
     * Define o valor da propriedade destArbLevelOfServiceGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setDestArbLevelOfServiceGid(GLogXMLGidType value) {
        this.destArbLevelOfServiceGid = value;
    }

}
