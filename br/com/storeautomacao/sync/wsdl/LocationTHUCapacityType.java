
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * The Location THU capacity is a structure specifying the capacity that a transport handling unit may hold when it is being delivered to this location for
 *             this role. If defined the maximum values here override those that are specified on the Ship Unit Specification table that defines the transport handling unit.
 *          
 * 
 * <p>Classe Java de LocationTHUCapacityType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="LocationTHUCapacityType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TransportHandlingUnitGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="MaxLength" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLengthType" minOccurs="0"/>
 *         &lt;element name="MaxWidth" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLWidthType" minOccurs="0"/>
 *         &lt;element name="MaxHeight" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLHeightType" minOccurs="0"/>
 *         &lt;element name="MaxWeight" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLWeightType" minOccurs="0"/>
 *         &lt;element name="MaxVolume" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLVolumeType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LocationTHUCapacityType", propOrder = {
    "transportHandlingUnitGid",
    "maxLength",
    "maxWidth",
    "maxHeight",
    "maxWeight",
    "maxVolume"
})
public class LocationTHUCapacityType {

    @XmlElement(name = "TransportHandlingUnitGid")
    protected GLogXMLGidType transportHandlingUnitGid;
    @XmlElement(name = "MaxLength")
    protected GLogXMLLengthType maxLength;
    @XmlElement(name = "MaxWidth")
    protected GLogXMLWidthType maxWidth;
    @XmlElement(name = "MaxHeight")
    protected GLogXMLHeightType maxHeight;
    @XmlElement(name = "MaxWeight")
    protected GLogXMLWeightType maxWeight;
    @XmlElement(name = "MaxVolume")
    protected GLogXMLVolumeType maxVolume;

    /**
     * Obtém o valor da propriedade transportHandlingUnitGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getTransportHandlingUnitGid() {
        return transportHandlingUnitGid;
    }

    /**
     * Define o valor da propriedade transportHandlingUnitGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setTransportHandlingUnitGid(GLogXMLGidType value) {
        this.transportHandlingUnitGid = value;
    }

    /**
     * Obtém o valor da propriedade maxLength.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLengthType }
     *     
     */
    public GLogXMLLengthType getMaxLength() {
        return maxLength;
    }

    /**
     * Define o valor da propriedade maxLength.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLengthType }
     *     
     */
    public void setMaxLength(GLogXMLLengthType value) {
        this.maxLength = value;
    }

    /**
     * Obtém o valor da propriedade maxWidth.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLWidthType }
     *     
     */
    public GLogXMLWidthType getMaxWidth() {
        return maxWidth;
    }

    /**
     * Define o valor da propriedade maxWidth.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLWidthType }
     *     
     */
    public void setMaxWidth(GLogXMLWidthType value) {
        this.maxWidth = value;
    }

    /**
     * Obtém o valor da propriedade maxHeight.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLHeightType }
     *     
     */
    public GLogXMLHeightType getMaxHeight() {
        return maxHeight;
    }

    /**
     * Define o valor da propriedade maxHeight.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLHeightType }
     *     
     */
    public void setMaxHeight(GLogXMLHeightType value) {
        this.maxHeight = value;
    }

    /**
     * Obtém o valor da propriedade maxWeight.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLWeightType }
     *     
     */
    public GLogXMLWeightType getMaxWeight() {
        return maxWeight;
    }

    /**
     * Define o valor da propriedade maxWeight.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLWeightType }
     *     
     */
    public void setMaxWeight(GLogXMLWeightType value) {
        this.maxWeight = value;
    }

    /**
     * Obtém o valor da propriedade maxVolume.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLVolumeType }
     *     
     */
    public GLogXMLVolumeType getMaxVolume() {
        return maxVolume;
    }

    /**
     * Define o valor da propriedade maxVolume.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLVolumeType }
     *     
     */
    public void setMaxVolume(GLogXMLVolumeType value) {
        this.maxVolume = value;
    }

}
