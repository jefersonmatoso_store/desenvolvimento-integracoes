
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de StatusCodeType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="StatusCodeType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="StatusCodeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="StatusCodeDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "StatusCodeType", propOrder = {
    "statusCodeGid",
    "statusCodeDescription"
})
public class StatusCodeType {

    @XmlElement(name = "StatusCodeGid", required = true)
    protected GLogXMLGidType statusCodeGid;
    @XmlElement(name = "StatusCodeDescription")
    protected String statusCodeDescription;

    /**
     * Obtém o valor da propriedade statusCodeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getStatusCodeGid() {
        return statusCodeGid;
    }

    /**
     * Define o valor da propriedade statusCodeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setStatusCodeGid(GLogXMLGidType value) {
        this.statusCodeGid = value;
    }

    /**
     * Obtém o valor da propriedade statusCodeDescription.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatusCodeDescription() {
        return statusCodeDescription;
    }

    /**
     * Define o valor da propriedade statusCodeDescription.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatusCodeDescription(String value) {
        this.statusCodeDescription = value;
    }

}
