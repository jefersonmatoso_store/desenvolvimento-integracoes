
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;


/**
 * 
 *             The property glog.integration.Claim.Location.includeContactGroupLocations controls whether the location(s) for the
 *             ContactGroup Contacts should be included
 *          
 * 
 * <p>Classe Java de ClaimType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="ClaimType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OTMTransactionInOut">
 *       &lt;sequence>
 *         &lt;element name="ClaimGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="TransactionCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransactionCodeType"/>
 *         &lt;element name="ReplaceChildren" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ReplaceChildrenType" minOccurs="0"/>
 *         &lt;element name="DamageInformation" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ReportDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="IncidentDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="DamageNotifyPointGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="DamageCauseGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="DamageNatureGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="DamageValue" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/>
 *         &lt;element name="IsDamageValueFixed" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SizeOfLoss" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/>
 *         &lt;element name="IsSizeOfLossFixed" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ShipmentGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="NotificationDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="NotifyPartyContact" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLContactRefType" minOccurs="0"/>
 *         &lt;element name="LiablePartyContact" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLContactRefType" minOccurs="0"/>
 *         &lt;element name="IsSelfRecondition" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InvolvedParty" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}InvolvedPartyType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="ClaimCost" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ClaimCostType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="ClaimLineItem" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ClaimLineItemType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="ClaimNote" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ClaimNoteType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Refnum" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RefnumType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Remark" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RemarkType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Status" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}StatusType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Location" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LocationType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="FlexFieldStrings" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldStringType" minOccurs="0"/>
 *         &lt;element name="FlexFieldNumbers" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldNumberType" minOccurs="0"/>
 *         &lt;element name="FlexFieldDates" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldDateType" minOccurs="0"/>
 *         &lt;element name="FlexFieldCurrencies" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldCurrencyType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ClaimType", propOrder = {
    "claimGid",
    "transactionCode",
    "replaceChildren",
    "damageInformation",
    "reportDt",
    "incidentDt",
    "damageNotifyPointGid",
    "damageCauseGid",
    "damageNatureGid",
    "damageValue",
    "isDamageValueFixed",
    "sizeOfLoss",
    "isSizeOfLossFixed",
    "shipmentGid",
    "notificationDt",
    "notifyPartyContact",
    "liablePartyContact",
    "isSelfRecondition",
    "involvedParty",
    "claimCost",
    "claimLineItem",
    "claimNote",
    "refnum",
    "remark",
    "status",
    "location",
    "flexFieldStrings",
    "flexFieldNumbers",
    "flexFieldDates",
    "flexFieldCurrencies"
})
public class ClaimType
    extends OTMTransactionInOut
{

    @XmlElement(name = "ClaimGid", required = true)
    protected GLogXMLGidType claimGid;
    @XmlElement(name = "TransactionCode", required = true)
    @XmlSchemaType(name = "string")
    protected TransactionCodeType transactionCode;
    @XmlElement(name = "ReplaceChildren")
    protected ReplaceChildrenType replaceChildren;
    @XmlElement(name = "DamageInformation")
    protected String damageInformation;
    @XmlElement(name = "ReportDt")
    protected GLogDateTimeType reportDt;
    @XmlElement(name = "IncidentDt")
    protected GLogDateTimeType incidentDt;
    @XmlElement(name = "DamageNotifyPointGid", required = true)
    protected GLogXMLGidType damageNotifyPointGid;
    @XmlElement(name = "DamageCauseGid")
    protected GLogXMLGidType damageCauseGid;
    @XmlElement(name = "DamageNatureGid")
    protected GLogXMLGidType damageNatureGid;
    @XmlElement(name = "DamageValue")
    protected GLogXMLFinancialAmountType damageValue;
    @XmlElement(name = "IsDamageValueFixed")
    protected String isDamageValueFixed;
    @XmlElement(name = "SizeOfLoss")
    protected GLogXMLFinancialAmountType sizeOfLoss;
    @XmlElement(name = "IsSizeOfLossFixed")
    protected String isSizeOfLossFixed;
    @XmlElement(name = "ShipmentGid")
    protected GLogXMLGidType shipmentGid;
    @XmlElement(name = "NotificationDt")
    protected GLogDateTimeType notificationDt;
    @XmlElement(name = "NotifyPartyContact")
    protected GLogXMLContactRefType notifyPartyContact;
    @XmlElement(name = "LiablePartyContact")
    protected GLogXMLContactRefType liablePartyContact;
    @XmlElement(name = "IsSelfRecondition")
    protected String isSelfRecondition;
    @XmlElement(name = "InvolvedParty")
    protected List<InvolvedPartyType> involvedParty;
    @XmlElement(name = "ClaimCost")
    protected List<ClaimCostType> claimCost;
    @XmlElement(name = "ClaimLineItem")
    protected List<ClaimLineItemType> claimLineItem;
    @XmlElement(name = "ClaimNote")
    protected List<ClaimNoteType> claimNote;
    @XmlElement(name = "Refnum")
    protected List<RefnumType> refnum;
    @XmlElement(name = "Remark")
    protected List<RemarkType> remark;
    @XmlElement(name = "Status")
    protected List<StatusType> status;
    @XmlElement(name = "Location")
    protected List<LocationType> location;
    @XmlElement(name = "FlexFieldStrings")
    protected FlexFieldStringType flexFieldStrings;
    @XmlElement(name = "FlexFieldNumbers")
    protected FlexFieldNumberType flexFieldNumbers;
    @XmlElement(name = "FlexFieldDates")
    protected FlexFieldDateType flexFieldDates;
    @XmlElement(name = "FlexFieldCurrencies")
    protected FlexFieldCurrencyType flexFieldCurrencies;

    /**
     * Obtém o valor da propriedade claimGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getClaimGid() {
        return claimGid;
    }

    /**
     * Define o valor da propriedade claimGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setClaimGid(GLogXMLGidType value) {
        this.claimGid = value;
    }

    /**
     * Obtém o valor da propriedade transactionCode.
     * 
     * @return
     *     possible object is
     *     {@link TransactionCodeType }
     *     
     */
    public TransactionCodeType getTransactionCode() {
        return transactionCode;
    }

    /**
     * Define o valor da propriedade transactionCode.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionCodeType }
     *     
     */
    public void setTransactionCode(TransactionCodeType value) {
        this.transactionCode = value;
    }

    /**
     * Obtém o valor da propriedade replaceChildren.
     * 
     * @return
     *     possible object is
     *     {@link ReplaceChildrenType }
     *     
     */
    public ReplaceChildrenType getReplaceChildren() {
        return replaceChildren;
    }

    /**
     * Define o valor da propriedade replaceChildren.
     * 
     * @param value
     *     allowed object is
     *     {@link ReplaceChildrenType }
     *     
     */
    public void setReplaceChildren(ReplaceChildrenType value) {
        this.replaceChildren = value;
    }

    /**
     * Obtém o valor da propriedade damageInformation.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDamageInformation() {
        return damageInformation;
    }

    /**
     * Define o valor da propriedade damageInformation.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDamageInformation(String value) {
        this.damageInformation = value;
    }

    /**
     * Obtém o valor da propriedade reportDt.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getReportDt() {
        return reportDt;
    }

    /**
     * Define o valor da propriedade reportDt.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setReportDt(GLogDateTimeType value) {
        this.reportDt = value;
    }

    /**
     * Obtém o valor da propriedade incidentDt.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getIncidentDt() {
        return incidentDt;
    }

    /**
     * Define o valor da propriedade incidentDt.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setIncidentDt(GLogDateTimeType value) {
        this.incidentDt = value;
    }

    /**
     * Obtém o valor da propriedade damageNotifyPointGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getDamageNotifyPointGid() {
        return damageNotifyPointGid;
    }

    /**
     * Define o valor da propriedade damageNotifyPointGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setDamageNotifyPointGid(GLogXMLGidType value) {
        this.damageNotifyPointGid = value;
    }

    /**
     * Obtém o valor da propriedade damageCauseGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getDamageCauseGid() {
        return damageCauseGid;
    }

    /**
     * Define o valor da propriedade damageCauseGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setDamageCauseGid(GLogXMLGidType value) {
        this.damageCauseGid = value;
    }

    /**
     * Obtém o valor da propriedade damageNatureGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getDamageNatureGid() {
        return damageNatureGid;
    }

    /**
     * Define o valor da propriedade damageNatureGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setDamageNatureGid(GLogXMLGidType value) {
        this.damageNatureGid = value;
    }

    /**
     * Obtém o valor da propriedade damageValue.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getDamageValue() {
        return damageValue;
    }

    /**
     * Define o valor da propriedade damageValue.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setDamageValue(GLogXMLFinancialAmountType value) {
        this.damageValue = value;
    }

    /**
     * Obtém o valor da propriedade isDamageValueFixed.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsDamageValueFixed() {
        return isDamageValueFixed;
    }

    /**
     * Define o valor da propriedade isDamageValueFixed.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsDamageValueFixed(String value) {
        this.isDamageValueFixed = value;
    }

    /**
     * Obtém o valor da propriedade sizeOfLoss.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getSizeOfLoss() {
        return sizeOfLoss;
    }

    /**
     * Define o valor da propriedade sizeOfLoss.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setSizeOfLoss(GLogXMLFinancialAmountType value) {
        this.sizeOfLoss = value;
    }

    /**
     * Obtém o valor da propriedade isSizeOfLossFixed.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsSizeOfLossFixed() {
        return isSizeOfLossFixed;
    }

    /**
     * Define o valor da propriedade isSizeOfLossFixed.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsSizeOfLossFixed(String value) {
        this.isSizeOfLossFixed = value;
    }

    /**
     * Obtém o valor da propriedade shipmentGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getShipmentGid() {
        return shipmentGid;
    }

    /**
     * Define o valor da propriedade shipmentGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setShipmentGid(GLogXMLGidType value) {
        this.shipmentGid = value;
    }

    /**
     * Obtém o valor da propriedade notificationDt.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getNotificationDt() {
        return notificationDt;
    }

    /**
     * Define o valor da propriedade notificationDt.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setNotificationDt(GLogDateTimeType value) {
        this.notificationDt = value;
    }

    /**
     * Obtém o valor da propriedade notifyPartyContact.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLContactRefType }
     *     
     */
    public GLogXMLContactRefType getNotifyPartyContact() {
        return notifyPartyContact;
    }

    /**
     * Define o valor da propriedade notifyPartyContact.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLContactRefType }
     *     
     */
    public void setNotifyPartyContact(GLogXMLContactRefType value) {
        this.notifyPartyContact = value;
    }

    /**
     * Obtém o valor da propriedade liablePartyContact.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLContactRefType }
     *     
     */
    public GLogXMLContactRefType getLiablePartyContact() {
        return liablePartyContact;
    }

    /**
     * Define o valor da propriedade liablePartyContact.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLContactRefType }
     *     
     */
    public void setLiablePartyContact(GLogXMLContactRefType value) {
        this.liablePartyContact = value;
    }

    /**
     * Obtém o valor da propriedade isSelfRecondition.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsSelfRecondition() {
        return isSelfRecondition;
    }

    /**
     * Define o valor da propriedade isSelfRecondition.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsSelfRecondition(String value) {
        this.isSelfRecondition = value;
    }

    /**
     * Gets the value of the involvedParty property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the involvedParty property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInvolvedParty().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link InvolvedPartyType }
     * 
     * 
     */
    public List<InvolvedPartyType> getInvolvedParty() {
        if (involvedParty == null) {
            involvedParty = new ArrayList<InvolvedPartyType>();
        }
        return this.involvedParty;
    }

    /**
     * Gets the value of the claimCost property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the claimCost property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getClaimCost().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ClaimCostType }
     * 
     * 
     */
    public List<ClaimCostType> getClaimCost() {
        if (claimCost == null) {
            claimCost = new ArrayList<ClaimCostType>();
        }
        return this.claimCost;
    }

    /**
     * Gets the value of the claimLineItem property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the claimLineItem property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getClaimLineItem().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ClaimLineItemType }
     * 
     * 
     */
    public List<ClaimLineItemType> getClaimLineItem() {
        if (claimLineItem == null) {
            claimLineItem = new ArrayList<ClaimLineItemType>();
        }
        return this.claimLineItem;
    }

    /**
     * Gets the value of the claimNote property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the claimNote property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getClaimNote().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ClaimNoteType }
     * 
     * 
     */
    public List<ClaimNoteType> getClaimNote() {
        if (claimNote == null) {
            claimNote = new ArrayList<ClaimNoteType>();
        }
        return this.claimNote;
    }

    /**
     * Gets the value of the refnum property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the refnum property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRefnum().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RefnumType }
     * 
     * 
     */
    public List<RefnumType> getRefnum() {
        if (refnum == null) {
            refnum = new ArrayList<RefnumType>();
        }
        return this.refnum;
    }

    /**
     * Gets the value of the remark property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the remark property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRemark().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RemarkType }
     * 
     * 
     */
    public List<RemarkType> getRemark() {
        if (remark == null) {
            remark = new ArrayList<RemarkType>();
        }
        return this.remark;
    }

    /**
     * Gets the value of the status property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the status property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getStatus().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link StatusType }
     * 
     * 
     */
    public List<StatusType> getStatus() {
        if (status == null) {
            status = new ArrayList<StatusType>();
        }
        return this.status;
    }

    /**
     * Gets the value of the location property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the location property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLocation().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LocationType }
     * 
     * 
     */
    public List<LocationType> getLocation() {
        if (location == null) {
            location = new ArrayList<LocationType>();
        }
        return this.location;
    }

    /**
     * Obtém o valor da propriedade flexFieldStrings.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldStringType }
     *     
     */
    public FlexFieldStringType getFlexFieldStrings() {
        return flexFieldStrings;
    }

    /**
     * Define o valor da propriedade flexFieldStrings.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldStringType }
     *     
     */
    public void setFlexFieldStrings(FlexFieldStringType value) {
        this.flexFieldStrings = value;
    }

    /**
     * Obtém o valor da propriedade flexFieldNumbers.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldNumberType }
     *     
     */
    public FlexFieldNumberType getFlexFieldNumbers() {
        return flexFieldNumbers;
    }

    /**
     * Define o valor da propriedade flexFieldNumbers.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldNumberType }
     *     
     */
    public void setFlexFieldNumbers(FlexFieldNumberType value) {
        this.flexFieldNumbers = value;
    }

    /**
     * Obtém o valor da propriedade flexFieldDates.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldDateType }
     *     
     */
    public FlexFieldDateType getFlexFieldDates() {
        return flexFieldDates;
    }

    /**
     * Define o valor da propriedade flexFieldDates.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldDateType }
     *     
     */
    public void setFlexFieldDates(FlexFieldDateType value) {
        this.flexFieldDates = value;
    }

    /**
     * Obtém o valor da propriedade flexFieldCurrencies.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldCurrencyType }
     *     
     */
    public FlexFieldCurrencyType getFlexFieldCurrencies() {
        return flexFieldCurrencies;
    }

    /**
     * Define o valor da propriedade flexFieldCurrencies.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldCurrencyType }
     *     
     */
    public void setFlexFieldCurrencies(FlexFieldCurrencyType value) {
        this.flexFieldCurrencies = value;
    }

}
