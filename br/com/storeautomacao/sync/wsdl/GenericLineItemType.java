
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * 
 *             GenericLineItem is used to convey line item information pertaining to motor, ocean or rail carriers.
 *          
 * 
 * <p>Classe Java de GenericLineItemType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="GenericLineItemType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AssignedNum" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="LineItemRefNum" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LineItemRefNumType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="CompartmentIDCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CommonInvoiceLineElements" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}CommonInvoiceLineElementsType"/>
 *         &lt;element name="CommercialInvoiceData" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}CommercialInvoiceDataType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="ExportImportLicense" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ExportImportLicenseType" minOccurs="0"/>
 *         &lt;element name="LineItemCostRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LineItemCostRefType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="CostTypeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="BillableIndicatorGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GenericLineItemType", propOrder = {
    "assignedNum",
    "lineItemRefNum",
    "compartmentIDCode",
    "commonInvoiceLineElements",
    "commercialInvoiceData",
    "exportImportLicense",
    "lineItemCostRef",
    "costTypeGid",
    "billableIndicatorGid"
})
public class GenericLineItemType {

    @XmlElement(name = "AssignedNum", required = true)
    protected String assignedNum;
    @XmlElement(name = "LineItemRefNum")
    protected List<LineItemRefNumType> lineItemRefNum;
    @XmlElement(name = "CompartmentIDCode")
    protected String compartmentIDCode;
    @XmlElement(name = "CommonInvoiceLineElements", required = true)
    protected CommonInvoiceLineElementsType commonInvoiceLineElements;
    @XmlElement(name = "CommercialInvoiceData")
    protected List<CommercialInvoiceDataType> commercialInvoiceData;
    @XmlElement(name = "ExportImportLicense")
    protected ExportImportLicenseType exportImportLicense;
    @XmlElement(name = "LineItemCostRef")
    protected List<LineItemCostRefType> lineItemCostRef;
    @XmlElement(name = "CostTypeGid")
    protected GLogXMLGidType costTypeGid;
    @XmlElement(name = "BillableIndicatorGid")
    protected GLogXMLGidType billableIndicatorGid;

    /**
     * Obtém o valor da propriedade assignedNum.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAssignedNum() {
        return assignedNum;
    }

    /**
     * Define o valor da propriedade assignedNum.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAssignedNum(String value) {
        this.assignedNum = value;
    }

    /**
     * Gets the value of the lineItemRefNum property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the lineItemRefNum property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLineItemRefNum().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LineItemRefNumType }
     * 
     * 
     */
    public List<LineItemRefNumType> getLineItemRefNum() {
        if (lineItemRefNum == null) {
            lineItemRefNum = new ArrayList<LineItemRefNumType>();
        }
        return this.lineItemRefNum;
    }

    /**
     * Obtém o valor da propriedade compartmentIDCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCompartmentIDCode() {
        return compartmentIDCode;
    }

    /**
     * Define o valor da propriedade compartmentIDCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCompartmentIDCode(String value) {
        this.compartmentIDCode = value;
    }

    /**
     * Obtém o valor da propriedade commonInvoiceLineElements.
     * 
     * @return
     *     possible object is
     *     {@link CommonInvoiceLineElementsType }
     *     
     */
    public CommonInvoiceLineElementsType getCommonInvoiceLineElements() {
        return commonInvoiceLineElements;
    }

    /**
     * Define o valor da propriedade commonInvoiceLineElements.
     * 
     * @param value
     *     allowed object is
     *     {@link CommonInvoiceLineElementsType }
     *     
     */
    public void setCommonInvoiceLineElements(CommonInvoiceLineElementsType value) {
        this.commonInvoiceLineElements = value;
    }

    /**
     * Gets the value of the commercialInvoiceData property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the commercialInvoiceData property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCommercialInvoiceData().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CommercialInvoiceDataType }
     * 
     * 
     */
    public List<CommercialInvoiceDataType> getCommercialInvoiceData() {
        if (commercialInvoiceData == null) {
            commercialInvoiceData = new ArrayList<CommercialInvoiceDataType>();
        }
        return this.commercialInvoiceData;
    }

    /**
     * Obtém o valor da propriedade exportImportLicense.
     * 
     * @return
     *     possible object is
     *     {@link ExportImportLicenseType }
     *     
     */
    public ExportImportLicenseType getExportImportLicense() {
        return exportImportLicense;
    }

    /**
     * Define o valor da propriedade exportImportLicense.
     * 
     * @param value
     *     allowed object is
     *     {@link ExportImportLicenseType }
     *     
     */
    public void setExportImportLicense(ExportImportLicenseType value) {
        this.exportImportLicense = value;
    }

    /**
     * Gets the value of the lineItemCostRef property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the lineItemCostRef property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLineItemCostRef().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LineItemCostRefType }
     * 
     * 
     */
    public List<LineItemCostRefType> getLineItemCostRef() {
        if (lineItemCostRef == null) {
            lineItemCostRef = new ArrayList<LineItemCostRefType>();
        }
        return this.lineItemCostRef;
    }

    /**
     * Obtém o valor da propriedade costTypeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getCostTypeGid() {
        return costTypeGid;
    }

    /**
     * Define o valor da propriedade costTypeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setCostTypeGid(GLogXMLGidType value) {
        this.costTypeGid = value;
    }

    /**
     * Obtém o valor da propriedade billableIndicatorGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getBillableIndicatorGid() {
        return billableIndicatorGid;
    }

    /**
     * Define o valor da propriedade billableIndicatorGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setBillableIndicatorGid(GLogXMLGidType value) {
        this.billableIndicatorGid = value;
    }

}
