
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * 
 *             (outbound) Contains a list of child inoices for a consolidated invoice.
 *             There is the option of outputting the Payment or the Billing, which contains the Shipment information.
 *             The default output is the Payment element. To specify that the billing should be output, suppress the
 *             payment option labeled 'billing.ChildPaymentsPayment' in the outbound integration profile.
 *          
 * 
 * <p>Classe Java de ChildPaymentsType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="ChildPaymentsType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element name="Payment" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}PaymentType" maxOccurs="unbounded"/>
 *         &lt;element name="Billing" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}BillingType" maxOccurs="unbounded"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ChildPaymentsType", propOrder = {
    "payment",
    "billing"
})
public class ChildPaymentsType {

    @XmlElement(name = "Payment")
    protected List<PaymentType> payment;
    @XmlElement(name = "Billing")
    protected List<BillingType> billing;

    /**
     * Gets the value of the payment property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the payment property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPayment().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PaymentType }
     * 
     * 
     */
    public List<PaymentType> getPayment() {
        if (payment == null) {
            payment = new ArrayList<PaymentType>();
        }
        return this.payment;
    }

    /**
     * Gets the value of the billing property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the billing property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBilling().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BillingType }
     * 
     * 
     */
    public List<BillingType> getBilling() {
        if (billing == null) {
            billing = new ArrayList<BillingType>();
        }
        return this.billing;
    }

}
