
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Classe Java de InterimFlightInstanceType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="InterimFlightInstanceType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence maxOccurs="unbounded" minOccurs="0">
 *         &lt;element name="INTERIM_FLIGHT_INSTANCE_ROW" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}InterimFlightInstanceRowType" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InterimFlightInstanceType", propOrder = {
    "interimflightinstancerow"
})
public class InterimFlightInstanceType {

    @XmlElement(name = "INTERIM_FLIGHT_INSTANCE_ROW")
    protected List<InterimFlightInstanceRowType> interimflightinstancerow;

    /**
     * Gets the value of the interimflightinstancerow property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the interimflightinstancerow property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getINTERIMFLIGHTINSTANCEROW().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link InterimFlightInstanceRowType }
     * 
     * 
     */
    public List<InterimFlightInstanceRowType> getINTERIMFLIGHTINSTANCEROW() {
        if (interimflightinstancerow == null) {
            interimflightinstancerow = new ArrayList<InterimFlightInstanceRowType>();
        }
        return this.interimflightinstancerow;
    }

}
