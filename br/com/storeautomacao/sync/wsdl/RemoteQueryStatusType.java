
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * RemoteQueryStatus is used to return status information as part of a remote query reply.
 *          
 * 
 * <p>Classe Java de RemoteQueryStatusType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="RemoteQueryStatusType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RemoteQueryStatusCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="RemoteQueryStatusSubject" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="RemoteQueryStatusMessage" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="StackTrace" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TransactionReport" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransactionReportType" minOccurs="0"/>
 *         &lt;element name="Perspective" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RemoteQueryStatusType", propOrder = {
    "remoteQueryStatusCode",
    "remoteQueryStatusSubject",
    "remoteQueryStatusMessage",
    "stackTrace",
    "transactionReport",
    "perspective"
})
public class RemoteQueryStatusType {

    @XmlElement(name = "RemoteQueryStatusCode", required = true)
    protected String remoteQueryStatusCode;
    @XmlElement(name = "RemoteQueryStatusSubject", required = true)
    protected String remoteQueryStatusSubject;
    @XmlElement(name = "RemoteQueryStatusMessage")
    protected String remoteQueryStatusMessage;
    @XmlElement(name = "StackTrace")
    protected String stackTrace;
    @XmlElement(name = "TransactionReport")
    protected TransactionReportType transactionReport;
    @XmlElement(name = "Perspective")
    protected String perspective;

    /**
     * Obtém o valor da propriedade remoteQueryStatusCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRemoteQueryStatusCode() {
        return remoteQueryStatusCode;
    }

    /**
     * Define o valor da propriedade remoteQueryStatusCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRemoteQueryStatusCode(String value) {
        this.remoteQueryStatusCode = value;
    }

    /**
     * Obtém o valor da propriedade remoteQueryStatusSubject.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRemoteQueryStatusSubject() {
        return remoteQueryStatusSubject;
    }

    /**
     * Define o valor da propriedade remoteQueryStatusSubject.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRemoteQueryStatusSubject(String value) {
        this.remoteQueryStatusSubject = value;
    }

    /**
     * Obtém o valor da propriedade remoteQueryStatusMessage.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRemoteQueryStatusMessage() {
        return remoteQueryStatusMessage;
    }

    /**
     * Define o valor da propriedade remoteQueryStatusMessage.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRemoteQueryStatusMessage(String value) {
        this.remoteQueryStatusMessage = value;
    }

    /**
     * Obtém o valor da propriedade stackTrace.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStackTrace() {
        return stackTrace;
    }

    /**
     * Define o valor da propriedade stackTrace.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStackTrace(String value) {
        this.stackTrace = value;
    }

    /**
     * Obtém o valor da propriedade transactionReport.
     * 
     * @return
     *     possible object is
     *     {@link TransactionReportType }
     *     
     */
    public TransactionReportType getTransactionReport() {
        return transactionReport;
    }

    /**
     * Define o valor da propriedade transactionReport.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionReportType }
     *     
     */
    public void setTransactionReport(TransactionReportType value) {
        this.transactionReport = value;
    }

    /**
     * Obtém o valor da propriedade perspective.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPerspective() {
        return perspective;
    }

    /**
     * Define o valor da propriedade perspective.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPerspective(String value) {
        this.perspective = value;
    }

}
