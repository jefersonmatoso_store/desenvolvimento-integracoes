
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * Used to specify information modified by the service provider as part of the booking.
 * 
 * <p>Classe Java de ConditionalBookingType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="ConditionalBookingType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CondBookingFieldInfo" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}CondBookingFieldInfoType" maxOccurs="unbounded"/>
 *         &lt;element name="Remark" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RemarkType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="ServprovEquipment" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ServprovEquipmentType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ConditionalBookingType", propOrder = {
    "condBookingFieldInfo",
    "remark",
    "servprovEquipment"
})
public class ConditionalBookingType {

    @XmlElement(name = "CondBookingFieldInfo", required = true)
    protected List<CondBookingFieldInfoType> condBookingFieldInfo;
    @XmlElement(name = "Remark")
    protected List<RemarkType> remark;
    @XmlElement(name = "ServprovEquipment")
    protected List<ServprovEquipmentType> servprovEquipment;

    /**
     * Gets the value of the condBookingFieldInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the condBookingFieldInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCondBookingFieldInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CondBookingFieldInfoType }
     * 
     * 
     */
    public List<CondBookingFieldInfoType> getCondBookingFieldInfo() {
        if (condBookingFieldInfo == null) {
            condBookingFieldInfo = new ArrayList<CondBookingFieldInfoType>();
        }
        return this.condBookingFieldInfo;
    }

    /**
     * Gets the value of the remark property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the remark property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRemark().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RemarkType }
     * 
     * 
     */
    public List<RemarkType> getRemark() {
        if (remark == null) {
            remark = new ArrayList<RemarkType>();
        }
        return this.remark;
    }

    /**
     * Gets the value of the servprovEquipment property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the servprovEquipment property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getServprovEquipment().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ServprovEquipmentType }
     * 
     * 
     */
    public List<ServprovEquipmentType> getServprovEquipment() {
        if (servprovEquipment == null) {
            servprovEquipment = new ArrayList<ServprovEquipmentType>();
        }
        return this.servprovEquipment;
    }

}
