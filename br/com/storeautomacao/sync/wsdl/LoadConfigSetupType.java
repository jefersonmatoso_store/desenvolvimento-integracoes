
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * Specifies the load configuration set up
 * 
 * <p>Classe Java de LoadConfigSetupType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="LoadConfigSetupType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="LoadConfigSetupGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="StackingIndex" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MaxStackingHeight" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLHeightType" minOccurs="0"/>
 *         &lt;element name="MaxStackingLayers" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsStackableAbove" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsStackableBelow" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MaxUnsupportedAreaPercent" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PatternConfigProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="StackingRule" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LoadConfigSetupOrientation" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LoadConfigSetupOrientationType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LoadConfigSetupType", propOrder = {
    "loadConfigSetupGid",
    "stackingIndex",
    "maxStackingHeight",
    "maxStackingLayers",
    "isStackableAbove",
    "isStackableBelow",
    "maxUnsupportedAreaPercent",
    "patternConfigProfileGid",
    "stackingRule",
    "loadConfigSetupOrientation"
})
public class LoadConfigSetupType {

    @XmlElement(name = "LoadConfigSetupGid", required = true)
    protected GLogXMLGidType loadConfigSetupGid;
    @XmlElement(name = "StackingIndex")
    protected String stackingIndex;
    @XmlElement(name = "MaxStackingHeight")
    protected GLogXMLHeightType maxStackingHeight;
    @XmlElement(name = "MaxStackingLayers")
    protected String maxStackingLayers;
    @XmlElement(name = "IsStackableAbove")
    protected String isStackableAbove;
    @XmlElement(name = "IsStackableBelow")
    protected String isStackableBelow;
    @XmlElement(name = "MaxUnsupportedAreaPercent")
    protected String maxUnsupportedAreaPercent;
    @XmlElement(name = "PatternConfigProfileGid")
    protected GLogXMLGidType patternConfigProfileGid;
    @XmlElement(name = "StackingRule")
    protected String stackingRule;
    @XmlElement(name = "LoadConfigSetupOrientation")
    protected List<LoadConfigSetupOrientationType> loadConfigSetupOrientation;

    /**
     * Obtém o valor da propriedade loadConfigSetupGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getLoadConfigSetupGid() {
        return loadConfigSetupGid;
    }

    /**
     * Define o valor da propriedade loadConfigSetupGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setLoadConfigSetupGid(GLogXMLGidType value) {
        this.loadConfigSetupGid = value;
    }

    /**
     * Obtém o valor da propriedade stackingIndex.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStackingIndex() {
        return stackingIndex;
    }

    /**
     * Define o valor da propriedade stackingIndex.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStackingIndex(String value) {
        this.stackingIndex = value;
    }

    /**
     * Obtém o valor da propriedade maxStackingHeight.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLHeightType }
     *     
     */
    public GLogXMLHeightType getMaxStackingHeight() {
        return maxStackingHeight;
    }

    /**
     * Define o valor da propriedade maxStackingHeight.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLHeightType }
     *     
     */
    public void setMaxStackingHeight(GLogXMLHeightType value) {
        this.maxStackingHeight = value;
    }

    /**
     * Obtém o valor da propriedade maxStackingLayers.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMaxStackingLayers() {
        return maxStackingLayers;
    }

    /**
     * Define o valor da propriedade maxStackingLayers.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMaxStackingLayers(String value) {
        this.maxStackingLayers = value;
    }

    /**
     * Obtém o valor da propriedade isStackableAbove.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsStackableAbove() {
        return isStackableAbove;
    }

    /**
     * Define o valor da propriedade isStackableAbove.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsStackableAbove(String value) {
        this.isStackableAbove = value;
    }

    /**
     * Obtém o valor da propriedade isStackableBelow.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsStackableBelow() {
        return isStackableBelow;
    }

    /**
     * Define o valor da propriedade isStackableBelow.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsStackableBelow(String value) {
        this.isStackableBelow = value;
    }

    /**
     * Obtém o valor da propriedade maxUnsupportedAreaPercent.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMaxUnsupportedAreaPercent() {
        return maxUnsupportedAreaPercent;
    }

    /**
     * Define o valor da propriedade maxUnsupportedAreaPercent.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMaxUnsupportedAreaPercent(String value) {
        this.maxUnsupportedAreaPercent = value;
    }

    /**
     * Obtém o valor da propriedade patternConfigProfileGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getPatternConfigProfileGid() {
        return patternConfigProfileGid;
    }

    /**
     * Define o valor da propriedade patternConfigProfileGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setPatternConfigProfileGid(GLogXMLGidType value) {
        this.patternConfigProfileGid = value;
    }

    /**
     * Obtém o valor da propriedade stackingRule.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStackingRule() {
        return stackingRule;
    }

    /**
     * Define o valor da propriedade stackingRule.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStackingRule(String value) {
        this.stackingRule = value;
    }

    /**
     * Gets the value of the loadConfigSetupOrientation property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the loadConfigSetupOrientation property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLoadConfigSetupOrientation().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LoadConfigSetupOrientationType }
     * 
     * 
     */
    public List<LoadConfigSetupOrientationType> getLoadConfigSetupOrientation() {
        if (loadConfigSetupOrientation == null) {
            loadConfigSetupOrientation = new ArrayList<LoadConfigSetupOrientationType>();
        }
        return this.loadConfigSetupOrientation;
    }

}
