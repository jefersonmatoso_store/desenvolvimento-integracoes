
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;


/**
 * Shipment Consolidator.
 * 
 *             Note: The CharterVoyage element is not output when the Consol is within the CharterVoyageStowage element.
 *          
 * 
 * <p>Classe Java de ConsolType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="ConsolType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OTMTransactionInOut">
 *       &lt;sequence>
 *         &lt;element name="ConsolGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="TransactionCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransactionCodeType"/>
 *         &lt;element name="ReplaceChildren" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ReplaceChildrenType" minOccurs="0"/>
 *         &lt;element name="ConsolType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CharterVoyageGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="FlightInstanceId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="StowageModeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="HazmatType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ItineraryProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="AllocatedWeightVolume" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}WeightVolumeType" minOccurs="0"/>
 *         &lt;element name="AllocatedNumTEU" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AllocatedNumFEU" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MaxWeightVolume" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}WeightVolumeType" minOccurs="0"/>
 *         &lt;element name="MaxNumTEU" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MaxNumFEU" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CommittedWeightVolume" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}WeightVolumeType" minOccurs="0"/>
 *         &lt;element name="CommittedNumTEU" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CommittedNumFEU" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BookedWeightVolume" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}WeightVolumeType" minOccurs="0"/>
 *         &lt;element name="BookedNumTEU" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BookedNumFEU" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ProducedWeightVolume" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}WeightVolumeType" minOccurs="0"/>
 *         &lt;element name="ProducedNumTEU" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ProducedNumFEU" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Perspective" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ConsolidationTypeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="Status" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}StatusType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Refnum" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RefnumType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Remark" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RemarkType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;choice minOccurs="0">
 *           &lt;element name="ShipmentGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" maxOccurs="unbounded" minOccurs="0"/>
 *           &lt;element name="Shipment" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ShipmentType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;/choice>
 *         &lt;element name="CharterVoyage" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}CharterVoyageType" minOccurs="0"/>
 *         &lt;element name="FlexFieldStrings" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldStringType" minOccurs="0"/>
 *         &lt;element name="FlexFieldNumbers" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldNumberType" minOccurs="0"/>
 *         &lt;element name="FlexFieldDates" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldDateType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ConsolType", propOrder = {
    "consolGid",
    "transactionCode",
    "replaceChildren",
    "consolType",
    "charterVoyageGid",
    "flightInstanceId",
    "stowageModeGid",
    "hazmatType",
    "itineraryProfileGid",
    "allocatedWeightVolume",
    "allocatedNumTEU",
    "allocatedNumFEU",
    "maxWeightVolume",
    "maxNumTEU",
    "maxNumFEU",
    "committedWeightVolume",
    "committedNumTEU",
    "committedNumFEU",
    "bookedWeightVolume",
    "bookedNumTEU",
    "bookedNumFEU",
    "producedWeightVolume",
    "producedNumTEU",
    "producedNumFEU",
    "perspective",
    "consolidationTypeGid",
    "status",
    "refnum",
    "remark",
    "shipmentGid",
    "shipment",
    "charterVoyage",
    "flexFieldStrings",
    "flexFieldNumbers",
    "flexFieldDates"
})
public class ConsolType
    extends OTMTransactionInOut
{

    @XmlElement(name = "ConsolGid", required = true)
    protected GLogXMLGidType consolGid;
    @XmlElement(name = "TransactionCode", required = true)
    @XmlSchemaType(name = "string")
    protected TransactionCodeType transactionCode;
    @XmlElement(name = "ReplaceChildren")
    protected ReplaceChildrenType replaceChildren;
    @XmlElement(name = "ConsolType")
    protected String consolType;
    @XmlElement(name = "CharterVoyageGid")
    protected GLogXMLGidType charterVoyageGid;
    @XmlElement(name = "FlightInstanceId")
    protected String flightInstanceId;
    @XmlElement(name = "StowageModeGid")
    protected GLogXMLGidType stowageModeGid;
    @XmlElement(name = "HazmatType")
    protected String hazmatType;
    @XmlElement(name = "ItineraryProfileGid")
    protected GLogXMLGidType itineraryProfileGid;
    @XmlElement(name = "AllocatedWeightVolume")
    protected WeightVolumeType allocatedWeightVolume;
    @XmlElement(name = "AllocatedNumTEU")
    protected String allocatedNumTEU;
    @XmlElement(name = "AllocatedNumFEU")
    protected String allocatedNumFEU;
    @XmlElement(name = "MaxWeightVolume")
    protected WeightVolumeType maxWeightVolume;
    @XmlElement(name = "MaxNumTEU")
    protected String maxNumTEU;
    @XmlElement(name = "MaxNumFEU")
    protected String maxNumFEU;
    @XmlElement(name = "CommittedWeightVolume")
    protected WeightVolumeType committedWeightVolume;
    @XmlElement(name = "CommittedNumTEU")
    protected String committedNumTEU;
    @XmlElement(name = "CommittedNumFEU")
    protected String committedNumFEU;
    @XmlElement(name = "BookedWeightVolume")
    protected WeightVolumeType bookedWeightVolume;
    @XmlElement(name = "BookedNumTEU")
    protected String bookedNumTEU;
    @XmlElement(name = "BookedNumFEU")
    protected String bookedNumFEU;
    @XmlElement(name = "ProducedWeightVolume")
    protected WeightVolumeType producedWeightVolume;
    @XmlElement(name = "ProducedNumTEU")
    protected String producedNumTEU;
    @XmlElement(name = "ProducedNumFEU")
    protected String producedNumFEU;
    @XmlElement(name = "Perspective")
    protected String perspective;
    @XmlElement(name = "ConsolidationTypeGid")
    protected GLogXMLGidType consolidationTypeGid;
    @XmlElement(name = "Status")
    protected List<StatusType> status;
    @XmlElement(name = "Refnum")
    protected List<RefnumType> refnum;
    @XmlElement(name = "Remark")
    protected List<RemarkType> remark;
    @XmlElement(name = "ShipmentGid")
    protected List<GLogXMLGidType> shipmentGid;
    @XmlElement(name = "Shipment")
    protected List<ShipmentType> shipment;
    @XmlElement(name = "CharterVoyage")
    protected CharterVoyageType charterVoyage;
    @XmlElement(name = "FlexFieldStrings")
    protected FlexFieldStringType flexFieldStrings;
    @XmlElement(name = "FlexFieldNumbers")
    protected FlexFieldNumberType flexFieldNumbers;
    @XmlElement(name = "FlexFieldDates")
    protected FlexFieldDateType flexFieldDates;

    /**
     * Obtém o valor da propriedade consolGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getConsolGid() {
        return consolGid;
    }

    /**
     * Define o valor da propriedade consolGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setConsolGid(GLogXMLGidType value) {
        this.consolGid = value;
    }

    /**
     * Obtém o valor da propriedade transactionCode.
     * 
     * @return
     *     possible object is
     *     {@link TransactionCodeType }
     *     
     */
    public TransactionCodeType getTransactionCode() {
        return transactionCode;
    }

    /**
     * Define o valor da propriedade transactionCode.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionCodeType }
     *     
     */
    public void setTransactionCode(TransactionCodeType value) {
        this.transactionCode = value;
    }

    /**
     * Obtém o valor da propriedade replaceChildren.
     * 
     * @return
     *     possible object is
     *     {@link ReplaceChildrenType }
     *     
     */
    public ReplaceChildrenType getReplaceChildren() {
        return replaceChildren;
    }

    /**
     * Define o valor da propriedade replaceChildren.
     * 
     * @param value
     *     allowed object is
     *     {@link ReplaceChildrenType }
     *     
     */
    public void setReplaceChildren(ReplaceChildrenType value) {
        this.replaceChildren = value;
    }

    /**
     * Obtém o valor da propriedade consolType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConsolType() {
        return consolType;
    }

    /**
     * Define o valor da propriedade consolType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConsolType(String value) {
        this.consolType = value;
    }

    /**
     * Obtém o valor da propriedade charterVoyageGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getCharterVoyageGid() {
        return charterVoyageGid;
    }

    /**
     * Define o valor da propriedade charterVoyageGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setCharterVoyageGid(GLogXMLGidType value) {
        this.charterVoyageGid = value;
    }

    /**
     * Obtém o valor da propriedade flightInstanceId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlightInstanceId() {
        return flightInstanceId;
    }

    /**
     * Define o valor da propriedade flightInstanceId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlightInstanceId(String value) {
        this.flightInstanceId = value;
    }

    /**
     * Obtém o valor da propriedade stowageModeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getStowageModeGid() {
        return stowageModeGid;
    }

    /**
     * Define o valor da propriedade stowageModeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setStowageModeGid(GLogXMLGidType value) {
        this.stowageModeGid = value;
    }

    /**
     * Obtém o valor da propriedade hazmatType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHazmatType() {
        return hazmatType;
    }

    /**
     * Define o valor da propriedade hazmatType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHazmatType(String value) {
        this.hazmatType = value;
    }

    /**
     * Obtém o valor da propriedade itineraryProfileGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getItineraryProfileGid() {
        return itineraryProfileGid;
    }

    /**
     * Define o valor da propriedade itineraryProfileGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setItineraryProfileGid(GLogXMLGidType value) {
        this.itineraryProfileGid = value;
    }

    /**
     * Obtém o valor da propriedade allocatedWeightVolume.
     * 
     * @return
     *     possible object is
     *     {@link WeightVolumeType }
     *     
     */
    public WeightVolumeType getAllocatedWeightVolume() {
        return allocatedWeightVolume;
    }

    /**
     * Define o valor da propriedade allocatedWeightVolume.
     * 
     * @param value
     *     allowed object is
     *     {@link WeightVolumeType }
     *     
     */
    public void setAllocatedWeightVolume(WeightVolumeType value) {
        this.allocatedWeightVolume = value;
    }

    /**
     * Obtém o valor da propriedade allocatedNumTEU.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAllocatedNumTEU() {
        return allocatedNumTEU;
    }

    /**
     * Define o valor da propriedade allocatedNumTEU.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAllocatedNumTEU(String value) {
        this.allocatedNumTEU = value;
    }

    /**
     * Obtém o valor da propriedade allocatedNumFEU.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAllocatedNumFEU() {
        return allocatedNumFEU;
    }

    /**
     * Define o valor da propriedade allocatedNumFEU.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAllocatedNumFEU(String value) {
        this.allocatedNumFEU = value;
    }

    /**
     * Obtém o valor da propriedade maxWeightVolume.
     * 
     * @return
     *     possible object is
     *     {@link WeightVolumeType }
     *     
     */
    public WeightVolumeType getMaxWeightVolume() {
        return maxWeightVolume;
    }

    /**
     * Define o valor da propriedade maxWeightVolume.
     * 
     * @param value
     *     allowed object is
     *     {@link WeightVolumeType }
     *     
     */
    public void setMaxWeightVolume(WeightVolumeType value) {
        this.maxWeightVolume = value;
    }

    /**
     * Obtém o valor da propriedade maxNumTEU.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMaxNumTEU() {
        return maxNumTEU;
    }

    /**
     * Define o valor da propriedade maxNumTEU.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMaxNumTEU(String value) {
        this.maxNumTEU = value;
    }

    /**
     * Obtém o valor da propriedade maxNumFEU.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMaxNumFEU() {
        return maxNumFEU;
    }

    /**
     * Define o valor da propriedade maxNumFEU.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMaxNumFEU(String value) {
        this.maxNumFEU = value;
    }

    /**
     * Obtém o valor da propriedade committedWeightVolume.
     * 
     * @return
     *     possible object is
     *     {@link WeightVolumeType }
     *     
     */
    public WeightVolumeType getCommittedWeightVolume() {
        return committedWeightVolume;
    }

    /**
     * Define o valor da propriedade committedWeightVolume.
     * 
     * @param value
     *     allowed object is
     *     {@link WeightVolumeType }
     *     
     */
    public void setCommittedWeightVolume(WeightVolumeType value) {
        this.committedWeightVolume = value;
    }

    /**
     * Obtém o valor da propriedade committedNumTEU.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCommittedNumTEU() {
        return committedNumTEU;
    }

    /**
     * Define o valor da propriedade committedNumTEU.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCommittedNumTEU(String value) {
        this.committedNumTEU = value;
    }

    /**
     * Obtém o valor da propriedade committedNumFEU.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCommittedNumFEU() {
        return committedNumFEU;
    }

    /**
     * Define o valor da propriedade committedNumFEU.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCommittedNumFEU(String value) {
        this.committedNumFEU = value;
    }

    /**
     * Obtém o valor da propriedade bookedWeightVolume.
     * 
     * @return
     *     possible object is
     *     {@link WeightVolumeType }
     *     
     */
    public WeightVolumeType getBookedWeightVolume() {
        return bookedWeightVolume;
    }

    /**
     * Define o valor da propriedade bookedWeightVolume.
     * 
     * @param value
     *     allowed object is
     *     {@link WeightVolumeType }
     *     
     */
    public void setBookedWeightVolume(WeightVolumeType value) {
        this.bookedWeightVolume = value;
    }

    /**
     * Obtém o valor da propriedade bookedNumTEU.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBookedNumTEU() {
        return bookedNumTEU;
    }

    /**
     * Define o valor da propriedade bookedNumTEU.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBookedNumTEU(String value) {
        this.bookedNumTEU = value;
    }

    /**
     * Obtém o valor da propriedade bookedNumFEU.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBookedNumFEU() {
        return bookedNumFEU;
    }

    /**
     * Define o valor da propriedade bookedNumFEU.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBookedNumFEU(String value) {
        this.bookedNumFEU = value;
    }

    /**
     * Obtém o valor da propriedade producedWeightVolume.
     * 
     * @return
     *     possible object is
     *     {@link WeightVolumeType }
     *     
     */
    public WeightVolumeType getProducedWeightVolume() {
        return producedWeightVolume;
    }

    /**
     * Define o valor da propriedade producedWeightVolume.
     * 
     * @param value
     *     allowed object is
     *     {@link WeightVolumeType }
     *     
     */
    public void setProducedWeightVolume(WeightVolumeType value) {
        this.producedWeightVolume = value;
    }

    /**
     * Obtém o valor da propriedade producedNumTEU.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProducedNumTEU() {
        return producedNumTEU;
    }

    /**
     * Define o valor da propriedade producedNumTEU.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProducedNumTEU(String value) {
        this.producedNumTEU = value;
    }

    /**
     * Obtém o valor da propriedade producedNumFEU.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProducedNumFEU() {
        return producedNumFEU;
    }

    /**
     * Define o valor da propriedade producedNumFEU.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProducedNumFEU(String value) {
        this.producedNumFEU = value;
    }

    /**
     * Obtém o valor da propriedade perspective.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPerspective() {
        return perspective;
    }

    /**
     * Define o valor da propriedade perspective.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPerspective(String value) {
        this.perspective = value;
    }

    /**
     * Obtém o valor da propriedade consolidationTypeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getConsolidationTypeGid() {
        return consolidationTypeGid;
    }

    /**
     * Define o valor da propriedade consolidationTypeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setConsolidationTypeGid(GLogXMLGidType value) {
        this.consolidationTypeGid = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the status property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getStatus().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link StatusType }
     * 
     * 
     */
    public List<StatusType> getStatus() {
        if (status == null) {
            status = new ArrayList<StatusType>();
        }
        return this.status;
    }

    /**
     * Gets the value of the refnum property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the refnum property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRefnum().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RefnumType }
     * 
     * 
     */
    public List<RefnumType> getRefnum() {
        if (refnum == null) {
            refnum = new ArrayList<RefnumType>();
        }
        return this.refnum;
    }

    /**
     * Gets the value of the remark property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the remark property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRemark().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RemarkType }
     * 
     * 
     */
    public List<RemarkType> getRemark() {
        if (remark == null) {
            remark = new ArrayList<RemarkType>();
        }
        return this.remark;
    }

    /**
     * Gets the value of the shipmentGid property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the shipmentGid property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getShipmentGid().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GLogXMLGidType }
     * 
     * 
     */
    public List<GLogXMLGidType> getShipmentGid() {
        if (shipmentGid == null) {
            shipmentGid = new ArrayList<GLogXMLGidType>();
        }
        return this.shipmentGid;
    }

    /**
     * Gets the value of the shipment property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the shipment property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getShipment().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ShipmentType }
     * 
     * 
     */
    public List<ShipmentType> getShipment() {
        if (shipment == null) {
            shipment = new ArrayList<ShipmentType>();
        }
        return this.shipment;
    }

    /**
     * Obtém o valor da propriedade charterVoyage.
     * 
     * @return
     *     possible object is
     *     {@link CharterVoyageType }
     *     
     */
    public CharterVoyageType getCharterVoyage() {
        return charterVoyage;
    }

    /**
     * Define o valor da propriedade charterVoyage.
     * 
     * @param value
     *     allowed object is
     *     {@link CharterVoyageType }
     *     
     */
    public void setCharterVoyage(CharterVoyageType value) {
        this.charterVoyage = value;
    }

    /**
     * Obtém o valor da propriedade flexFieldStrings.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldStringType }
     *     
     */
    public FlexFieldStringType getFlexFieldStrings() {
        return flexFieldStrings;
    }

    /**
     * Define o valor da propriedade flexFieldStrings.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldStringType }
     *     
     */
    public void setFlexFieldStrings(FlexFieldStringType value) {
        this.flexFieldStrings = value;
    }

    /**
     * Obtém o valor da propriedade flexFieldNumbers.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldNumberType }
     *     
     */
    public FlexFieldNumberType getFlexFieldNumbers() {
        return flexFieldNumbers;
    }

    /**
     * Define o valor da propriedade flexFieldNumbers.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldNumberType }
     *     
     */
    public void setFlexFieldNumbers(FlexFieldNumberType value) {
        this.flexFieldNumbers = value;
    }

    /**
     * Obtém o valor da propriedade flexFieldDates.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldDateType }
     *     
     */
    public FlexFieldDateType getFlexFieldDates() {
        return flexFieldDates;
    }

    /**
     * Define o valor da propriedade flexFieldDates.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldDateType }
     *     
     */
    public void setFlexFieldDates(FlexFieldDateType value) {
        this.flexFieldDates = value;
    }

}
