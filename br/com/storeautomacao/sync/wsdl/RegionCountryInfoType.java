
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * (Both) Region or Country Information.
 *          
 * 
 * <p>Classe Java de RegionCountryInfoType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="RegionCountryInfoType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RegionQualifier" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="RegionId" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="CountryCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RegionCountryInfoType", namespace = "http://xmlns.oracle.com/apps/gtm/transmission/v6.4", propOrder = {
    "regionQualifier",
    "regionId",
    "countryCode"
})
public class RegionCountryInfoType {

    @XmlElement(name = "RegionQualifier", required = true)
    protected GLogXMLGidType regionQualifier;
    @XmlElement(name = "RegionId")
    protected GLogXMLGidType regionId;
    @XmlElement(name = "CountryCode")
    protected GLogXMLGidType countryCode;

    /**
     * Obtém o valor da propriedade regionQualifier.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRegionQualifier() {
        return regionQualifier;
    }

    /**
     * Define o valor da propriedade regionQualifier.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRegionQualifier(GLogXMLGidType value) {
        this.regionQualifier = value;
    }

    /**
     * Obtém o valor da propriedade regionId.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRegionId() {
        return regionId;
    }

    /**
     * Define o valor da propriedade regionId.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRegionId(GLogXMLGidType value) {
        this.regionId = value;
    }

    /**
     * Obtém o valor da propriedade countryCode.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getCountryCode() {
        return countryCode;
    }

    /**
     * Define o valor da propriedade countryCode.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setCountryCode(GLogXMLGidType value) {
        this.countryCode = value;
    }

}
