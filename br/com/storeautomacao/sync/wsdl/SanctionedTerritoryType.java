
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * 
 *             (Inbound) SanctionedTerritory is the embargo screening request triggered via ServiceRequest integration interface.
 *          
 * 
 * <p>Classe Java de SanctionedTerritoryType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="SanctionedTerritoryType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RuleGroupGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="LocationInfo" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}LocationInfoType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="TransactionDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="InvolvedPartyRefnumDetail" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}InvolvedPartyRefnumDetailType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="InvolvedPartyRemarkDetail" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}InvolvedPartyRemarkDetailType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SanctionedTerritoryType", namespace = "http://xmlns.oracle.com/apps/gtm/transmission/v6.4", propOrder = {
    "ruleGroupGid",
    "locationInfo",
    "transactionDate",
    "involvedPartyRefnumDetail",
    "involvedPartyRemarkDetail"
})
public class SanctionedTerritoryType {

    @XmlElement(name = "RuleGroupGid")
    protected GLogXMLGidType ruleGroupGid;
    @XmlElement(name = "LocationInfo")
    protected List<LocationInfoType> locationInfo;
    @XmlElement(name = "TransactionDate")
    protected GLogDateTimeType transactionDate;
    @XmlElement(name = "InvolvedPartyRefnumDetail")
    protected List<InvolvedPartyRefnumDetailType> involvedPartyRefnumDetail;
    @XmlElement(name = "InvolvedPartyRemarkDetail")
    protected List<InvolvedPartyRemarkDetailType> involvedPartyRemarkDetail;

    /**
     * Obtém o valor da propriedade ruleGroupGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRuleGroupGid() {
        return ruleGroupGid;
    }

    /**
     * Define o valor da propriedade ruleGroupGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRuleGroupGid(GLogXMLGidType value) {
        this.ruleGroupGid = value;
    }

    /**
     * Gets the value of the locationInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the locationInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLocationInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LocationInfoType }
     * 
     * 
     */
    public List<LocationInfoType> getLocationInfo() {
        if (locationInfo == null) {
            locationInfo = new ArrayList<LocationInfoType>();
        }
        return this.locationInfo;
    }

    /**
     * Obtém o valor da propriedade transactionDate.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getTransactionDate() {
        return transactionDate;
    }

    /**
     * Define o valor da propriedade transactionDate.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setTransactionDate(GLogDateTimeType value) {
        this.transactionDate = value;
    }

    /**
     * Gets the value of the involvedPartyRefnumDetail property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the involvedPartyRefnumDetail property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInvolvedPartyRefnumDetail().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link InvolvedPartyRefnumDetailType }
     * 
     * 
     */
    public List<InvolvedPartyRefnumDetailType> getInvolvedPartyRefnumDetail() {
        if (involvedPartyRefnumDetail == null) {
            involvedPartyRefnumDetail = new ArrayList<InvolvedPartyRefnumDetailType>();
        }
        return this.involvedPartyRefnumDetail;
    }

    /**
     * Gets the value of the involvedPartyRemarkDetail property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the involvedPartyRemarkDetail property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInvolvedPartyRemarkDetail().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link InvolvedPartyRemarkDetailType }
     * 
     * 
     */
    public List<InvolvedPartyRemarkDetailType> getInvolvedPartyRemarkDetail() {
        if (involvedPartyRemarkDetail == null) {
            involvedPartyRemarkDetail = new ArrayList<InvolvedPartyRemarkDetailType>();
        }
        return this.involvedPartyRemarkDetail;
    }

}
