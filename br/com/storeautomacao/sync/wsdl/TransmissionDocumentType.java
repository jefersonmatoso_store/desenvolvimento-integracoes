
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 *             This type is essentially a 'marker' type used to identify the top level XML documents that can be sent to, or received
 *             from OTM and GTM.
 *          
 * 
 * <p>Classe Java de TransmissionDocumentType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="TransmissionDocumentType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TransmissionDocumentType")
@XmlSeeAlso({
    TransmissionReportType.class,
    GLogXMLTransactionType.class,
    TransmissionAck.class,
    Transmission.class
})
public abstract class TransmissionDocumentType {


}
