
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * (Inbound) LicenseDetermination is request for the license screening.
 *          
 * 
 * <p>Classe Java de LicenseDeterminationType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="LicenseDeterminationType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ItemGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="CommodityGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="UserDefinedCommodityGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="ControlTypeCode" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}ControlTypeCodeType" maxOccurs="unbounded"/>
 *         &lt;element name="PortInfo" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}PortInfoType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="InvolvedPartyDetail" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}InvolvedPartyDetailType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="RegionCountryInfo" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}RegionCountryInfoType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="CategoryTypeCode" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}CategoryTypeCodeType" maxOccurs="unbounded"/>
 *         &lt;element name="TransDateInfo" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}TransDateType" maxOccurs="unbounded"/>
 *         &lt;element name="ProductClassification" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}ProductClassificationType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="ItemRemarks" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}GtmRemarksType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="TransLineRemarks" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}GtmRemarksType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LicenseDeterminationType", namespace = "http://xmlns.oracle.com/apps/gtm/transmission/v6.4", propOrder = {
    "itemGid",
    "commodityGid",
    "userDefinedCommodityGid",
    "controlTypeCode",
    "portInfo",
    "involvedPartyDetail",
    "regionCountryInfo",
    "categoryTypeCode",
    "transDateInfo",
    "productClassification",
    "itemRemarks",
    "transLineRemarks"
})
public class LicenseDeterminationType {

    @XmlElement(name = "ItemGid")
    protected GLogXMLGidType itemGid;
    @XmlElement(name = "CommodityGid")
    protected GLogXMLGidType commodityGid;
    @XmlElement(name = "UserDefinedCommodityGid")
    protected GLogXMLGidType userDefinedCommodityGid;
    @XmlElement(name = "ControlTypeCode", required = true)
    protected List<ControlTypeCodeType> controlTypeCode;
    @XmlElement(name = "PortInfo")
    protected List<PortInfoType> portInfo;
    @XmlElement(name = "InvolvedPartyDetail")
    protected List<InvolvedPartyDetailType> involvedPartyDetail;
    @XmlElement(name = "RegionCountryInfo")
    protected List<RegionCountryInfoType> regionCountryInfo;
    @XmlElement(name = "CategoryTypeCode", required = true)
    protected List<CategoryTypeCodeType> categoryTypeCode;
    @XmlElement(name = "TransDateInfo", required = true)
    protected List<TransDateType> transDateInfo;
    @XmlElement(name = "ProductClassification")
    protected List<ProductClassificationType> productClassification;
    @XmlElement(name = "ItemRemarks")
    protected List<GtmRemarksType> itemRemarks;
    @XmlElement(name = "TransLineRemarks")
    protected List<GtmRemarksType> transLineRemarks;

    /**
     * Obtém o valor da propriedade itemGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getItemGid() {
        return itemGid;
    }

    /**
     * Define o valor da propriedade itemGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setItemGid(GLogXMLGidType value) {
        this.itemGid = value;
    }

    /**
     * Obtém o valor da propriedade commodityGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getCommodityGid() {
        return commodityGid;
    }

    /**
     * Define o valor da propriedade commodityGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setCommodityGid(GLogXMLGidType value) {
        this.commodityGid = value;
    }

    /**
     * Obtém o valor da propriedade userDefinedCommodityGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getUserDefinedCommodityGid() {
        return userDefinedCommodityGid;
    }

    /**
     * Define o valor da propriedade userDefinedCommodityGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setUserDefinedCommodityGid(GLogXMLGidType value) {
        this.userDefinedCommodityGid = value;
    }

    /**
     * Gets the value of the controlTypeCode property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the controlTypeCode property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getControlTypeCode().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ControlTypeCodeType }
     * 
     * 
     */
    public List<ControlTypeCodeType> getControlTypeCode() {
        if (controlTypeCode == null) {
            controlTypeCode = new ArrayList<ControlTypeCodeType>();
        }
        return this.controlTypeCode;
    }

    /**
     * Gets the value of the portInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the portInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPortInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PortInfoType }
     * 
     * 
     */
    public List<PortInfoType> getPortInfo() {
        if (portInfo == null) {
            portInfo = new ArrayList<PortInfoType>();
        }
        return this.portInfo;
    }

    /**
     * Gets the value of the involvedPartyDetail property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the involvedPartyDetail property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInvolvedPartyDetail().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link InvolvedPartyDetailType }
     * 
     * 
     */
    public List<InvolvedPartyDetailType> getInvolvedPartyDetail() {
        if (involvedPartyDetail == null) {
            involvedPartyDetail = new ArrayList<InvolvedPartyDetailType>();
        }
        return this.involvedPartyDetail;
    }

    /**
     * Gets the value of the regionCountryInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the regionCountryInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRegionCountryInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RegionCountryInfoType }
     * 
     * 
     */
    public List<RegionCountryInfoType> getRegionCountryInfo() {
        if (regionCountryInfo == null) {
            regionCountryInfo = new ArrayList<RegionCountryInfoType>();
        }
        return this.regionCountryInfo;
    }

    /**
     * Gets the value of the categoryTypeCode property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the categoryTypeCode property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCategoryTypeCode().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CategoryTypeCodeType }
     * 
     * 
     */
    public List<CategoryTypeCodeType> getCategoryTypeCode() {
        if (categoryTypeCode == null) {
            categoryTypeCode = new ArrayList<CategoryTypeCodeType>();
        }
        return this.categoryTypeCode;
    }

    /**
     * Gets the value of the transDateInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the transDateInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTransDateInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TransDateType }
     * 
     * 
     */
    public List<TransDateType> getTransDateInfo() {
        if (transDateInfo == null) {
            transDateInfo = new ArrayList<TransDateType>();
        }
        return this.transDateInfo;
    }

    /**
     * Gets the value of the productClassification property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the productClassification property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getProductClassification().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProductClassificationType }
     * 
     * 
     */
    public List<ProductClassificationType> getProductClassification() {
        if (productClassification == null) {
            productClassification = new ArrayList<ProductClassificationType>();
        }
        return this.productClassification;
    }

    /**
     * Gets the value of the itemRemarks property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the itemRemarks property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getItemRemarks().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GtmRemarksType }
     * 
     * 
     */
    public List<GtmRemarksType> getItemRemarks() {
        if (itemRemarks == null) {
            itemRemarks = new ArrayList<GtmRemarksType>();
        }
        return this.itemRemarks;
    }

    /**
     * Gets the value of the transLineRemarks property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the transLineRemarks property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTransLineRemarks().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GtmRemarksType }
     * 
     * 
     */
    public List<GtmRemarksType> getTransLineRemarks() {
        if (transLineRemarks == null) {
            transLineRemarks = new ArrayList<GtmRemarksType>();
        }
        return this.transLineRemarks;
    }

}
