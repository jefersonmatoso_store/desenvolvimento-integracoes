
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * SSLocation is a structure containing various shipment status location information.
 * 
 * <p>Classe Java de SSLocationType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="SSLocationType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PortLocationFunctionCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LocationRefnumQualifierGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="LocationID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LocationName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EventCity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EventState" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SPLCCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EventCountry" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Latitude" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Longitude" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TerminalName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="GatePierDoorNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AEIIndicator" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ERPC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SSLocationType", propOrder = {
    "portLocationFunctionCode",
    "locationRefnumQualifierGid",
    "locationID",
    "locationName",
    "eventCity",
    "eventState",
    "splcCode",
    "eventCountry",
    "latitude",
    "longitude",
    "terminalName",
    "gatePierDoorNum",
    "aeiIndicator",
    "erpc"
})
public class SSLocationType {

    @XmlElement(name = "PortLocationFunctionCode")
    protected String portLocationFunctionCode;
    @XmlElement(name = "LocationRefnumQualifierGid")
    protected GLogXMLGidType locationRefnumQualifierGid;
    @XmlElement(name = "LocationID")
    protected String locationID;
    @XmlElement(name = "LocationName")
    protected String locationName;
    @XmlElement(name = "EventCity")
    protected String eventCity;
    @XmlElement(name = "EventState")
    protected String eventState;
    @XmlElement(name = "SPLCCode")
    protected String splcCode;
    @XmlElement(name = "EventCountry")
    protected String eventCountry;
    @XmlElement(name = "Latitude")
    protected String latitude;
    @XmlElement(name = "Longitude")
    protected String longitude;
    @XmlElement(name = "TerminalName")
    protected String terminalName;
    @XmlElement(name = "GatePierDoorNum")
    protected String gatePierDoorNum;
    @XmlElement(name = "AEIIndicator")
    protected String aeiIndicator;
    @XmlElement(name = "ERPC")
    protected String erpc;

    /**
     * Obtém o valor da propriedade portLocationFunctionCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPortLocationFunctionCode() {
        return portLocationFunctionCode;
    }

    /**
     * Define o valor da propriedade portLocationFunctionCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPortLocationFunctionCode(String value) {
        this.portLocationFunctionCode = value;
    }

    /**
     * Obtém o valor da propriedade locationRefnumQualifierGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getLocationRefnumQualifierGid() {
        return locationRefnumQualifierGid;
    }

    /**
     * Define o valor da propriedade locationRefnumQualifierGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setLocationRefnumQualifierGid(GLogXMLGidType value) {
        this.locationRefnumQualifierGid = value;
    }

    /**
     * Obtém o valor da propriedade locationID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLocationID() {
        return locationID;
    }

    /**
     * Define o valor da propriedade locationID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLocationID(String value) {
        this.locationID = value;
    }

    /**
     * Obtém o valor da propriedade locationName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLocationName() {
        return locationName;
    }

    /**
     * Define o valor da propriedade locationName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLocationName(String value) {
        this.locationName = value;
    }

    /**
     * Obtém o valor da propriedade eventCity.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEventCity() {
        return eventCity;
    }

    /**
     * Define o valor da propriedade eventCity.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEventCity(String value) {
        this.eventCity = value;
    }

    /**
     * Obtém o valor da propriedade eventState.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEventState() {
        return eventState;
    }

    /**
     * Define o valor da propriedade eventState.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEventState(String value) {
        this.eventState = value;
    }

    /**
     * Obtém o valor da propriedade splcCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSPLCCode() {
        return splcCode;
    }

    /**
     * Define o valor da propriedade splcCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSPLCCode(String value) {
        this.splcCode = value;
    }

    /**
     * Obtém o valor da propriedade eventCountry.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEventCountry() {
        return eventCountry;
    }

    /**
     * Define o valor da propriedade eventCountry.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEventCountry(String value) {
        this.eventCountry = value;
    }

    /**
     * Obtém o valor da propriedade latitude.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLatitude() {
        return latitude;
    }

    /**
     * Define o valor da propriedade latitude.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLatitude(String value) {
        this.latitude = value;
    }

    /**
     * Obtém o valor da propriedade longitude.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLongitude() {
        return longitude;
    }

    /**
     * Define o valor da propriedade longitude.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLongitude(String value) {
        this.longitude = value;
    }

    /**
     * Obtém o valor da propriedade terminalName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTerminalName() {
        return terminalName;
    }

    /**
     * Define o valor da propriedade terminalName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTerminalName(String value) {
        this.terminalName = value;
    }

    /**
     * Obtém o valor da propriedade gatePierDoorNum.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGatePierDoorNum() {
        return gatePierDoorNum;
    }

    /**
     * Define o valor da propriedade gatePierDoorNum.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGatePierDoorNum(String value) {
        this.gatePierDoorNum = value;
    }

    /**
     * Obtém o valor da propriedade aeiIndicator.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAEIIndicator() {
        return aeiIndicator;
    }

    /**
     * Define o valor da propriedade aeiIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAEIIndicator(String value) {
        this.aeiIndicator = value;
    }

    /**
     * Obtém o valor da propriedade erpc.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getERPC() {
        return erpc;
    }

    /**
     * Define o valor da propriedade erpc.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setERPC(String value) {
        this.erpc = value;
    }

}
