
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * A TransactionReport contains a transaction number followed by 0 or more integration log messages.
 * 
 * <p>Classe Java de TransactionReportType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="TransactionReportType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ITransactionNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="IntegrationLogMessage" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}IntegrationLogMessageType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TransactionReportType", propOrder = {
    "iTransactionNo",
    "integrationLogMessage"
})
public class TransactionReportType {

    @XmlElement(name = "ITransactionNo", required = true)
    protected String iTransactionNo;
    @XmlElement(name = "IntegrationLogMessage")
    protected List<IntegrationLogMessageType> integrationLogMessage;

    /**
     * Obtém o valor da propriedade iTransactionNo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getITransactionNo() {
        return iTransactionNo;
    }

    /**
     * Define o valor da propriedade iTransactionNo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setITransactionNo(String value) {
        this.iTransactionNo = value;
    }

    /**
     * Gets the value of the integrationLogMessage property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the integrationLogMessage property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getIntegrationLogMessage().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link IntegrationLogMessageType }
     * 
     * 
     */
    public List<IntegrationLogMessageType> getIntegrationLogMessage() {
        if (integrationLogMessage == null) {
            integrationLogMessage = new ArrayList<IntegrationLogMessageType>();
        }
        return this.integrationLogMessage;
    }

}
