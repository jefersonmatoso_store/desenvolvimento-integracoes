
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * This element is used to identify an item and its packaging information.
 * 
 * <p>Classe Java de PackagedItemType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="PackagedItemType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Packaging" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}PackagingType"/>
 *         &lt;element name="Item" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ItemType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PackagedItemType", propOrder = {
    "packaging",
    "item"
})
public class PackagedItemType {

    @XmlElement(name = "Packaging", required = true)
    protected PackagingType packaging;
    @XmlElement(name = "Item", required = true)
    protected ItemType item;

    /**
     * Obtém o valor da propriedade packaging.
     * 
     * @return
     *     possible object is
     *     {@link PackagingType }
     *     
     */
    public PackagingType getPackaging() {
        return packaging;
    }

    /**
     * Define o valor da propriedade packaging.
     * 
     * @param value
     *     allowed object is
     *     {@link PackagingType }
     *     
     */
    public void setPackaging(PackagingType value) {
        this.packaging = value;
    }

    /**
     * Obtém o valor da propriedade item.
     * 
     * @return
     *     possible object is
     *     {@link ItemType }
     *     
     */
    public ItemType getItem() {
        return item;
    }

    /**
     * Define o valor da propriedade item.
     * 
     * @param value
     *     allowed object is
     *     {@link ItemType }
     *     
     */
    public void setItem(ItemType value) {
        this.item = value;
    }

}
