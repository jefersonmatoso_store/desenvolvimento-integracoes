
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * Corresponds to the sku_descriptor table.
 * 
 * <p>Classe Java de SkuDescriptorType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="SkuDescriptorType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SkuDescriptorSeq" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SkuDescriptorType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SkuDescriptorValue" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SkuDescriptorQuantity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ParentSkuDescriptorSeq" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="UserDefinedXml" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Remark" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RemarkType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SkuDescriptorType", propOrder = {
    "skuDescriptorSeq",
    "skuDescriptorType",
    "skuDescriptorValue",
    "skuDescriptorQuantity",
    "parentSkuDescriptorSeq",
    "userDefinedXml",
    "remark"
})
public class SkuDescriptorType {

    @XmlElement(name = "SkuDescriptorSeq", required = true)
    protected String skuDescriptorSeq;
    @XmlElement(name = "SkuDescriptorType")
    protected String skuDescriptorType;
    @XmlElement(name = "SkuDescriptorValue")
    protected String skuDescriptorValue;
    @XmlElement(name = "SkuDescriptorQuantity")
    protected String skuDescriptorQuantity;
    @XmlElement(name = "ParentSkuDescriptorSeq")
    protected String parentSkuDescriptorSeq;
    @XmlElement(name = "UserDefinedXml")
    protected String userDefinedXml;
    @XmlElement(name = "Remark")
    protected List<RemarkType> remark;

    /**
     * Obtém o valor da propriedade skuDescriptorSeq.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSkuDescriptorSeq() {
        return skuDescriptorSeq;
    }

    /**
     * Define o valor da propriedade skuDescriptorSeq.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSkuDescriptorSeq(String value) {
        this.skuDescriptorSeq = value;
    }

    /**
     * Obtém o valor da propriedade skuDescriptorType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSkuDescriptorType() {
        return skuDescriptorType;
    }

    /**
     * Define o valor da propriedade skuDescriptorType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSkuDescriptorType(String value) {
        this.skuDescriptorType = value;
    }

    /**
     * Obtém o valor da propriedade skuDescriptorValue.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSkuDescriptorValue() {
        return skuDescriptorValue;
    }

    /**
     * Define o valor da propriedade skuDescriptorValue.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSkuDescriptorValue(String value) {
        this.skuDescriptorValue = value;
    }

    /**
     * Obtém o valor da propriedade skuDescriptorQuantity.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSkuDescriptorQuantity() {
        return skuDescriptorQuantity;
    }

    /**
     * Define o valor da propriedade skuDescriptorQuantity.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSkuDescriptorQuantity(String value) {
        this.skuDescriptorQuantity = value;
    }

    /**
     * Obtém o valor da propriedade parentSkuDescriptorSeq.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getParentSkuDescriptorSeq() {
        return parentSkuDescriptorSeq;
    }

    /**
     * Define o valor da propriedade parentSkuDescriptorSeq.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setParentSkuDescriptorSeq(String value) {
        this.parentSkuDescriptorSeq = value;
    }

    /**
     * Obtém o valor da propriedade userDefinedXml.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserDefinedXml() {
        return userDefinedXml;
    }

    /**
     * Define o valor da propriedade userDefinedXml.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserDefinedXml(String value) {
        this.userDefinedXml = value;
    }

    /**
     * Gets the value of the remark property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the remark property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRemark().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RemarkType }
     * 
     * 
     */
    public List<RemarkType> getRemark() {
        if (remark == null) {
            remark = new ArrayList<RemarkType>();
        }
        return this.remark;
    }

}
