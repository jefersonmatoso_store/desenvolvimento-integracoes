
package br.com.storeautomacao.sync.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * 
 *             Specifies a line item for the claim.
 *          
 * 
 * <p>Classe Java de ClaimLineItemType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="ClaimLineItemType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SequenceNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ReleaseLineGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="SerialNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DamageTypeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="TransportHandlingUnitRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLShipUnitSpecRefType" minOccurs="0"/>
 *         &lt;element name="PricePerUnit" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}PricePerUnitType" minOccurs="0"/>
 *         &lt;element name="PricePerUnitUOM" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}UOMContextType" minOccurs="0"/>
 *         &lt;element name="SizeOfLoss" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/>
 *         &lt;element name="DamagedWeightVolume" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}WeightVolumeType" minOccurs="0"/>
 *         &lt;element name="DamagedQuantity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DamagedFraction" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OriginalWeightVolume" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}WeightVolumeType" minOccurs="0"/>
 *         &lt;element name="OriginalQuantity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NewWeightVolume" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}WeightVolumeType" minOccurs="0"/>
 *         &lt;element name="NewQuantity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RemovedWeightVolume" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}WeightVolumeType" minOccurs="0"/>
 *         &lt;element name="RemovedQuantity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RemovedFraction" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OrigCylinderDiameter" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDiameterType" minOccurs="0"/>
 *         &lt;element name="OrigCylinderCoreDiameter" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDiameterType" minOccurs="0"/>
 *         &lt;element name="RemovedCylinderDepth" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLengthType" minOccurs="0"/>
 *         &lt;element name="NewCylinderDiameter" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDiameterType" minOccurs="0"/>
 *         &lt;element name="DeclaredValue" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/>
 *         &lt;element name="PackageStatusGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="ClaimCost" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ClaimCostType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Refnum" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RefnumType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ClaimLineItemType", propOrder = {
    "sequenceNumber",
    "releaseLineGid",
    "serialNumber",
    "damageTypeGid",
    "transportHandlingUnitRef",
    "pricePerUnit",
    "pricePerUnitUOM",
    "sizeOfLoss",
    "damagedWeightVolume",
    "damagedQuantity",
    "damagedFraction",
    "originalWeightVolume",
    "originalQuantity",
    "newWeightVolume",
    "newQuantity",
    "removedWeightVolume",
    "removedQuantity",
    "removedFraction",
    "origCylinderDiameter",
    "origCylinderCoreDiameter",
    "removedCylinderDepth",
    "newCylinderDiameter",
    "declaredValue",
    "packageStatusGid",
    "claimCost",
    "refnum"
})
public class ClaimLineItemType {

    @XmlElement(name = "SequenceNumber")
    protected String sequenceNumber;
    @XmlElement(name = "ReleaseLineGid")
    protected GLogXMLGidType releaseLineGid;
    @XmlElement(name = "SerialNumber")
    protected String serialNumber;
    @XmlElement(name = "DamageTypeGid")
    protected GLogXMLGidType damageTypeGid;
    @XmlElement(name = "TransportHandlingUnitRef")
    protected GLogXMLShipUnitSpecRefType transportHandlingUnitRef;
    @XmlElement(name = "PricePerUnit")
    protected PricePerUnitType pricePerUnit;
    @XmlElement(name = "PricePerUnitUOM")
    protected UOMContextType pricePerUnitUOM;
    @XmlElement(name = "SizeOfLoss")
    protected GLogXMLFinancialAmountType sizeOfLoss;
    @XmlElement(name = "DamagedWeightVolume")
    protected WeightVolumeType damagedWeightVolume;
    @XmlElement(name = "DamagedQuantity")
    protected String damagedQuantity;
    @XmlElement(name = "DamagedFraction")
    protected String damagedFraction;
    @XmlElement(name = "OriginalWeightVolume")
    protected WeightVolumeType originalWeightVolume;
    @XmlElement(name = "OriginalQuantity")
    protected String originalQuantity;
    @XmlElement(name = "NewWeightVolume")
    protected WeightVolumeType newWeightVolume;
    @XmlElement(name = "NewQuantity")
    protected String newQuantity;
    @XmlElement(name = "RemovedWeightVolume")
    protected WeightVolumeType removedWeightVolume;
    @XmlElement(name = "RemovedQuantity")
    protected String removedQuantity;
    @XmlElement(name = "RemovedFraction")
    protected String removedFraction;
    @XmlElement(name = "OrigCylinderDiameter")
    protected GLogXMLDiameterType origCylinderDiameter;
    @XmlElement(name = "OrigCylinderCoreDiameter")
    protected GLogXMLDiameterType origCylinderCoreDiameter;
    @XmlElement(name = "RemovedCylinderDepth")
    protected GLogXMLLengthType removedCylinderDepth;
    @XmlElement(name = "NewCylinderDiameter")
    protected GLogXMLDiameterType newCylinderDiameter;
    @XmlElement(name = "DeclaredValue")
    protected GLogXMLFinancialAmountType declaredValue;
    @XmlElement(name = "PackageStatusGid")
    protected GLogXMLGidType packageStatusGid;
    @XmlElement(name = "ClaimCost")
    protected List<ClaimCostType> claimCost;
    @XmlElement(name = "Refnum")
    protected List<RefnumType> refnum;

    /**
     * Obtém o valor da propriedade sequenceNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSequenceNumber() {
        return sequenceNumber;
    }

    /**
     * Define o valor da propriedade sequenceNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSequenceNumber(String value) {
        this.sequenceNumber = value;
    }

    /**
     * Obtém o valor da propriedade releaseLineGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getReleaseLineGid() {
        return releaseLineGid;
    }

    /**
     * Define o valor da propriedade releaseLineGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setReleaseLineGid(GLogXMLGidType value) {
        this.releaseLineGid = value;
    }

    /**
     * Obtém o valor da propriedade serialNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSerialNumber() {
        return serialNumber;
    }

    /**
     * Define o valor da propriedade serialNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSerialNumber(String value) {
        this.serialNumber = value;
    }

    /**
     * Obtém o valor da propriedade damageTypeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getDamageTypeGid() {
        return damageTypeGid;
    }

    /**
     * Define o valor da propriedade damageTypeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setDamageTypeGid(GLogXMLGidType value) {
        this.damageTypeGid = value;
    }

    /**
     * Obtém o valor da propriedade transportHandlingUnitRef.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLShipUnitSpecRefType }
     *     
     */
    public GLogXMLShipUnitSpecRefType getTransportHandlingUnitRef() {
        return transportHandlingUnitRef;
    }

    /**
     * Define o valor da propriedade transportHandlingUnitRef.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLShipUnitSpecRefType }
     *     
     */
    public void setTransportHandlingUnitRef(GLogXMLShipUnitSpecRefType value) {
        this.transportHandlingUnitRef = value;
    }

    /**
     * Obtém o valor da propriedade pricePerUnit.
     * 
     * @return
     *     possible object is
     *     {@link PricePerUnitType }
     *     
     */
    public PricePerUnitType getPricePerUnit() {
        return pricePerUnit;
    }

    /**
     * Define o valor da propriedade pricePerUnit.
     * 
     * @param value
     *     allowed object is
     *     {@link PricePerUnitType }
     *     
     */
    public void setPricePerUnit(PricePerUnitType value) {
        this.pricePerUnit = value;
    }

    /**
     * Obtém o valor da propriedade pricePerUnitUOM.
     * 
     * @return
     *     possible object is
     *     {@link UOMContextType }
     *     
     */
    public UOMContextType getPricePerUnitUOM() {
        return pricePerUnitUOM;
    }

    /**
     * Define o valor da propriedade pricePerUnitUOM.
     * 
     * @param value
     *     allowed object is
     *     {@link UOMContextType }
     *     
     */
    public void setPricePerUnitUOM(UOMContextType value) {
        this.pricePerUnitUOM = value;
    }

    /**
     * Obtém o valor da propriedade sizeOfLoss.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getSizeOfLoss() {
        return sizeOfLoss;
    }

    /**
     * Define o valor da propriedade sizeOfLoss.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setSizeOfLoss(GLogXMLFinancialAmountType value) {
        this.sizeOfLoss = value;
    }

    /**
     * Obtém o valor da propriedade damagedWeightVolume.
     * 
     * @return
     *     possible object is
     *     {@link WeightVolumeType }
     *     
     */
    public WeightVolumeType getDamagedWeightVolume() {
        return damagedWeightVolume;
    }

    /**
     * Define o valor da propriedade damagedWeightVolume.
     * 
     * @param value
     *     allowed object is
     *     {@link WeightVolumeType }
     *     
     */
    public void setDamagedWeightVolume(WeightVolumeType value) {
        this.damagedWeightVolume = value;
    }

    /**
     * Obtém o valor da propriedade damagedQuantity.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDamagedQuantity() {
        return damagedQuantity;
    }

    /**
     * Define o valor da propriedade damagedQuantity.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDamagedQuantity(String value) {
        this.damagedQuantity = value;
    }

    /**
     * Obtém o valor da propriedade damagedFraction.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDamagedFraction() {
        return damagedFraction;
    }

    /**
     * Define o valor da propriedade damagedFraction.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDamagedFraction(String value) {
        this.damagedFraction = value;
    }

    /**
     * Obtém o valor da propriedade originalWeightVolume.
     * 
     * @return
     *     possible object is
     *     {@link WeightVolumeType }
     *     
     */
    public WeightVolumeType getOriginalWeightVolume() {
        return originalWeightVolume;
    }

    /**
     * Define o valor da propriedade originalWeightVolume.
     * 
     * @param value
     *     allowed object is
     *     {@link WeightVolumeType }
     *     
     */
    public void setOriginalWeightVolume(WeightVolumeType value) {
        this.originalWeightVolume = value;
    }

    /**
     * Obtém o valor da propriedade originalQuantity.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOriginalQuantity() {
        return originalQuantity;
    }

    /**
     * Define o valor da propriedade originalQuantity.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOriginalQuantity(String value) {
        this.originalQuantity = value;
    }

    /**
     * Obtém o valor da propriedade newWeightVolume.
     * 
     * @return
     *     possible object is
     *     {@link WeightVolumeType }
     *     
     */
    public WeightVolumeType getNewWeightVolume() {
        return newWeightVolume;
    }

    /**
     * Define o valor da propriedade newWeightVolume.
     * 
     * @param value
     *     allowed object is
     *     {@link WeightVolumeType }
     *     
     */
    public void setNewWeightVolume(WeightVolumeType value) {
        this.newWeightVolume = value;
    }

    /**
     * Obtém o valor da propriedade newQuantity.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNewQuantity() {
        return newQuantity;
    }

    /**
     * Define o valor da propriedade newQuantity.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNewQuantity(String value) {
        this.newQuantity = value;
    }

    /**
     * Obtém o valor da propriedade removedWeightVolume.
     * 
     * @return
     *     possible object is
     *     {@link WeightVolumeType }
     *     
     */
    public WeightVolumeType getRemovedWeightVolume() {
        return removedWeightVolume;
    }

    /**
     * Define o valor da propriedade removedWeightVolume.
     * 
     * @param value
     *     allowed object is
     *     {@link WeightVolumeType }
     *     
     */
    public void setRemovedWeightVolume(WeightVolumeType value) {
        this.removedWeightVolume = value;
    }

    /**
     * Obtém o valor da propriedade removedQuantity.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRemovedQuantity() {
        return removedQuantity;
    }

    /**
     * Define o valor da propriedade removedQuantity.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRemovedQuantity(String value) {
        this.removedQuantity = value;
    }

    /**
     * Obtém o valor da propriedade removedFraction.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRemovedFraction() {
        return removedFraction;
    }

    /**
     * Define o valor da propriedade removedFraction.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRemovedFraction(String value) {
        this.removedFraction = value;
    }

    /**
     * Obtém o valor da propriedade origCylinderDiameter.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDiameterType }
     *     
     */
    public GLogXMLDiameterType getOrigCylinderDiameter() {
        return origCylinderDiameter;
    }

    /**
     * Define o valor da propriedade origCylinderDiameter.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDiameterType }
     *     
     */
    public void setOrigCylinderDiameter(GLogXMLDiameterType value) {
        this.origCylinderDiameter = value;
    }

    /**
     * Obtém o valor da propriedade origCylinderCoreDiameter.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDiameterType }
     *     
     */
    public GLogXMLDiameterType getOrigCylinderCoreDiameter() {
        return origCylinderCoreDiameter;
    }

    /**
     * Define o valor da propriedade origCylinderCoreDiameter.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDiameterType }
     *     
     */
    public void setOrigCylinderCoreDiameter(GLogXMLDiameterType value) {
        this.origCylinderCoreDiameter = value;
    }

    /**
     * Obtém o valor da propriedade removedCylinderDepth.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLengthType }
     *     
     */
    public GLogXMLLengthType getRemovedCylinderDepth() {
        return removedCylinderDepth;
    }

    /**
     * Define o valor da propriedade removedCylinderDepth.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLengthType }
     *     
     */
    public void setRemovedCylinderDepth(GLogXMLLengthType value) {
        this.removedCylinderDepth = value;
    }

    /**
     * Obtém o valor da propriedade newCylinderDiameter.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDiameterType }
     *     
     */
    public GLogXMLDiameterType getNewCylinderDiameter() {
        return newCylinderDiameter;
    }

    /**
     * Define o valor da propriedade newCylinderDiameter.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDiameterType }
     *     
     */
    public void setNewCylinderDiameter(GLogXMLDiameterType value) {
        this.newCylinderDiameter = value;
    }

    /**
     * Obtém o valor da propriedade declaredValue.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getDeclaredValue() {
        return declaredValue;
    }

    /**
     * Define o valor da propriedade declaredValue.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setDeclaredValue(GLogXMLFinancialAmountType value) {
        this.declaredValue = value;
    }

    /**
     * Obtém o valor da propriedade packageStatusGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getPackageStatusGid() {
        return packageStatusGid;
    }

    /**
     * Define o valor da propriedade packageStatusGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setPackageStatusGid(GLogXMLGidType value) {
        this.packageStatusGid = value;
    }

    /**
     * Gets the value of the claimCost property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the claimCost property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getClaimCost().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ClaimCostType }
     * 
     * 
     */
    public List<ClaimCostType> getClaimCost() {
        if (claimCost == null) {
            claimCost = new ArrayList<ClaimCostType>();
        }
        return this.claimCost;
    }

    /**
     * Gets the value of the refnum property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the refnum property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRefnum().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RefnumType }
     * 
     * 
     */
    public List<RefnumType> getRefnum() {
        if (refnum == null) {
            refnum = new ArrayList<RefnumType>();
        }
        return this.refnum;
    }

}
