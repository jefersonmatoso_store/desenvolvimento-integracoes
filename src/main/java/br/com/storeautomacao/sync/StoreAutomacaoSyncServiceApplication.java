package br.com.storeautomacao.sync;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StoreAutomacaoSyncServiceApplication {

	public static void main(String[] args) {

		SpringApplication mApplication = new SpringApplication(
				StoreAutomacaoSyncServiceApplication.class);
		//mApplication.addListeners(new ApplicationPidFileWriter("application.pid"));
		mApplication.run(args);
	}

}
