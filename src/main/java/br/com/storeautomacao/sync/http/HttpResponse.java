package br.com.storeautomacao.sync.http;

import java.io.Serializable;

/**
 * Created by eduardoabreu on 17/10/16.
 */
public class HttpResponse extends HeaderHttpResponse implements Serializable {

    private Object Object;

    public java.lang.Object getObject() {
        return Object;
    }

    public void setObject(java.lang.Object object) {
        Object = object;
    }

}
