package br.com.storeautomacao.sync.http.model.checklist;

/**
 * Created by alexandredeabreu on 07/07/2017.
 */
public class ActivityRequest {

    private String idActivity;
    private String status;

    public String getIdActivity() {
        return idActivity;
    }

    public void setIdActivity(String idActivity) {
        this.idActivity = idActivity;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}