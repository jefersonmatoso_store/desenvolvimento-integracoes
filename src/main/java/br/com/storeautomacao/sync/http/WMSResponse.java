package br.com.storeautomacao.sync.http;

import com.fasterxml.jackson.annotation.JsonProperty;

public class WMSResponse {

    @JsonProperty("success")
    private Boolean success;

    @JsonProperty("response")
    private Response response;

    public Response getResponse() {
        return response;
    }

    public void setResponse(Response response) {
        this.response = response;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }
}
