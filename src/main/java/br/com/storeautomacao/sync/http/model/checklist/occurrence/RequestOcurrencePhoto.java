package br.com.storeautomacao.sync.http.model.checklist.occurrence;

public class RequestOcurrencePhoto {

	private String id;
	private String idOcurrence;
	private String base64String;
	private String typeMedia;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getIdOcurrence() {
		return idOcurrence;
	}

	public void setIdOcurrence(String idOcurrence) {
		this.idOcurrence = idOcurrence;
	}

	public String getBase64String() {
		return base64String;
	}

	public void setBase64String(String base64String) {
		this.base64String = base64String;
	}

	public String getTypeMedia() {
		return typeMedia;
	}

	public void setTypeMedia(String typeMedia) {
		this.typeMedia = typeMedia;
	}
}
