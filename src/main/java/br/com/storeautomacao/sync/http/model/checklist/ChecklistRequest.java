package br.com.storeautomacao.sync.http.model.checklist;

import java.util.List;

/**
 * Created by alexandredeabreu on 07/07/2017.
 */
public class ChecklistRequest  {

    private List<ActivityRequest> activityRequest = null;
    private String idChecklist;

    public List<ActivityRequest> getActivityRequest() {
        return activityRequest;
    }

    public void setActivityRequest(List<ActivityRequest> activityRequest) {
        this.activityRequest = activityRequest;
    }

    public String getIdChecklist() {
        return idChecklist;
    }

    public void setIdChecklist(String idChecklist) {
        this.idChecklist = idChecklist;
    }
}
