package br.com.storeautomacao.sync.service;

import br.com.storeautomacao.sync.dto.ItemDto;
import br.com.storeautomacao.sync.dto.ProdutoDto;
import br.com.storeautomacao.sync.model.entity.Produto;
import br.com.storeautomacao.sync.model.entity.ProdutoComponente;
import br.com.storeautomacao.sync.model.entity.TipoUc;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 *
 */
@Service
public interface ProdutoService {
    List<Produto> obterListaProdutoPorSequenciaIntegracao(String sequenciaIntegracao);
    TipoUc obterTipoUcPorCodigoProdutoESequenciaIntegracao(String sequenciaIntegracao, String codigoProduto);
    List<ProdutoComponente> obterProdutoComponentePorCodigoProdutoESequenciaIntegracao(String sequenciaIntegracao, String codigoProduto);
    String cadastrarProduto(ProdutoDto produtoDto);
    String cadastrarItem(ItemDto itemDto);
    List<ProdutoDto> obterTodosProdutos();
}
