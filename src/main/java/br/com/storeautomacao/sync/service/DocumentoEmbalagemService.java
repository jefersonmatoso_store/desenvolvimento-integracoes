package br.com.storeautomacao.sync.service;

import org.springframework.stereotype.Service;

import java.math.BigDecimal;

/**
 *
 */
public interface DocumentoEmbalagemService {
    BigDecimal gravarDocumentoEmbalagem(String sql);
}
