package br.com.storeautomacao.sync.service;

import br.com.storeautomacao.sync.dto.PushMessageDTO;
import br.com.storeautomacao.sync.model.entity.PushMessage;
import com.google.android.gcm.server.InvalidRequestException;
import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.Result;
import com.google.android.gcm.server.Sender;
import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.io.IOException;

/**
 * Created by eduardoabreu on 06/10/16.
 *
 * @deprecated REFACTORING: Utilizar o futuro serviço 'nicbrain - mensageria'
 *
 */
@Service
public class GcmMessageServerService {

    static Logger logger = LoggerFactory.getLogger(GcmMessageServerService.class);

    public boolean sendMessage(PushMessage pushMessage){
        final String GCM_API_KEY = "AAAALKJTgBo:APA91bFiPY6OJn-44rJPpUjAYqyY0Xb2Fh0CEilrVN5eFYOAunZw7HuaWPP9Zrb9c5nham00RLYzIKsYgMpqZl6D1ht0wKTMdjrpHGSMCo1s8vJ--rYvJ-a3-Qv7q6PFbp8zn1u0CnYGFgNcdP15Bf3I2uBeN0XdPw";
        final int retries = 3;
        final String notificationToken = pushMessage.getToken();
        Sender sender = new Sender(GCM_API_KEY);

        logger.info("## sendMessage 1 ##");

        Message msg = new Message.Builder()
                .collapseKey("message")
                .addData("title", pushMessage.getTitle())
                .addData("subtitle", pushMessage.getSubtitle())
                .addData("message", pushMessage.getMessage())
                .addData("key", pushMessage.getKey())
                .build();

        System.out.println("Enviando mensagem ");
        try {
            Result result = sender.send(msg, notificationToken, retries);

            if (StringUtils.isEmpty(result.getErrorCodeName())) {
                System.out.println("GCM Notification is successfully 1 ");
                return true;
            }

        } catch (InvalidRequestException e) {
            System.out.println("GCM Notification is sent successfully 3 " + e.getMessage());
        } catch (IOException e) {
            System.out.println("GCM Notification is sent successfully 4 " + e.getMessage());
        }

        return false;
    }


    public boolean sendMessageDTO(PushMessageDTO pushMessageDTO, String token){
        final String GCM_API_KEY = "AAAALKJTgBo:APA91bFiPY6OJn-44rJPpUjAYqyY0Xb2Fh0CEilrVN5eFYOAunZw7HuaWPP9Zrb9c5nham00RLYzIKsYgMpqZl6D1ht0wKTMdjrpHGSMCo1s8vJ--rYvJ-a3-Qv7q6PFbp8zn1u0CnYGFgNcdP15Bf3I2uBeN0XdPw";
        final int retries = 3;
        final String notificationToken = token;
        Sender sender = new Sender(GCM_API_KEY);

        logger.info("## sendMessage 1 ##");

        Gson gson = new Gson();

        String jsonInString = gson.toJson(pushMessageDTO);

        Message msg = new Message.Builder()
                .collapseKey("message")
                .addData("stringObject", jsonInString)
                .build();

        System.out.println("Enviando mensagem ");
        try {
            Result result = sender.send(msg, notificationToken, retries);

            if (StringUtils.isEmpty(result.getErrorCodeName())) {
                System.out.println("GCM Notification is successfully 1 ");
                return true;
            }

        } catch (InvalidRequestException e) {
            System.out.println("GCM Notification is sent successfully 3 " + e.getMessage());
        } catch (IOException e) {
            System.out.println("GCM Notification is sent successfully 4 " + e.getMessage());
        }

        return false;
    }

}
