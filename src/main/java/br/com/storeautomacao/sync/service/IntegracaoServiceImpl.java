package br.com.storeautomacao.sync.service;

import br.com.storeautomacao.sync.connector.RestApiConnector;
import br.com.storeautomacao.sync.dao.IntegracaoDao;
import br.com.storeautomacao.sync.dto.SaleDto;
import br.com.storeautomacao.sync.model.entity.*;
import br.com.storeautomacao.sync.model.enuns.EstadoIntegracaoEnum;
import br.com.storeautomacao.sync.model.wms.ConfirmacaoConferencia;
import br.com.storeautomacao.sync.model.wms.Header;
import br.com.storeautomacao.sync.model.wms.Inventory_history;
import br.com.storeautomacao.sync.model.wms.ListOfInventoryHistories;
import br.com.storeautomacao.sync.model.wms.PurchaseOrder.ListOfPurchaseOrders;
import br.com.storeautomacao.sync.model.wms.PurchaseOrder.OrdemDeCompra;
import br.com.storeautomacao.sync.model.wms.Shipment.LgfDataShipped;
import br.com.storeautomacao.sync.model.wms.Shipment.ListOfShippedLoads;
import br.com.storeautomacao.sync.model.wms.Shipment.ObStop;
import br.com.storeautomacao.sync.model.wms.Shipment.ShippedLoad;
import br.com.storeautomacao.sync.repository.ConferenciaRepository;
import br.com.storeautomacao.sync.repository.DocumentoRepository;
import br.com.storeautomacao.sync.repository.IntegracaoRepository;
import br.com.storeautomacao.sync.repository.MercadoriaRepository;
import br.com.storeautomacao.sync.util.SqlUtil;
import br.com.storeautomacao.sync.util.XmlUtil;
import br.com.storeautomacao.sync.wsdl.otm.ItemType;
import br.com.storeautomacao.sync.wsdl.otm.LocationType;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.naming.NoNameCoder;
import com.thoughtworks.xstream.io.xml.DomDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static br.com.storeautomacao.sync.util.Constantes.ESTADOINTEGRACAO_REALIZADA_2;

@Service
public class IntegracaoServiceImpl implements IntegracaoService {

    static Logger logger = LoggerFactory.getLogger(IntegracaoServiceImpl.class);

    @Autowired
    private RestApiConnector connector;

    @Autowired
    IntegracaoDao integracaoDao;

    @Autowired
    DocumentoRepository documentoRepository;

    @Autowired
    IntegracaoRepository integracaoRepository;

    @Autowired
    MercadoriaRepository mercadoriaRepository;

    @Autowired
    ConferenciaRepository conferenciaRepository;

    @Autowired
    MercadoriaService mercadoriaService;

    @Autowired
    ProdutoService produtoService;

    @Override
    public Sistema test() {
        Sistema sistema = new Sistema();
        List<Sistema> listSistema = integracaoDao.testeConnect();
        sistema = listSistema.get(0);
        return sistema;
    }


    /**
     * #################################
     * PROCESSO DE ENTRADA DE MERCADORIA
     * #################################
     */


    @Override
    public List<Integracao> entradaMercadoria() {
        List<Integracao> listaIntegracao = integracaoRepository.listaIntegracaoEntradaMercadoriaPrevisaoDeEntrada();
        List<Integracao> listaIntegracaoExecutada = new ArrayList<>();

        Documento documento = new Documento();
        List<DocumentoDetalhe> listaDocumentoDetalhe = new ArrayList<>();

        for (Integracao integracao : listaIntegracao) {

            try{
                documento = documentoRepository.obterDocumentoPorSequenciaIntegracao(integracao.getSequenciaIntegracao());

                String xmlVendor = XmlUtil.getXmlVendor(documento, integracao);
                ResponseEntity<String> response = connector.connectorSendXmlWms(xmlVendor);

                // Envia XML Vendor
                if (HttpStatus.OK.equals(response.getStatusCode())) {
                    listaDocumentoDetalhe = documentoRepository.obterDocumentoDetalhePorSequenciaIntegracao(integracao.getSequenciaIntegracao());
                    String xmlPO = XmlUtil.getXmlPO(documento, listaDocumentoDetalhe, integracao);
                    response = connector.connectorSendXmlWms(xmlPO);
                }

                // Envia XML PO - Pedido de entrada de mercadoria
                if (HttpStatus.OK.equals(response.getStatusCode())) {

                    Mercadoria m = mercadoriaRepository.obterMercadoriaPorNumeroDocumento(documento.getNumeroDocumento());

                    if(m != null){
                        m.setEstadoIntegracao("CANCELADA");
                        integracaoRepository.atualizarOperacaoEntrada(m);
                    }

                    Mercadoria entradaMercadoriaEmAndamento = new Mercadoria();
                    entradaMercadoriaEmAndamento.setSequenciaIntegracao(integracao.getSequenciaIntegracao());
                    entradaMercadoriaEmAndamento.setDocumento(documento);
                    entradaMercadoriaEmAndamento.setNumeroDocumento(documento.getNumeroDocumento());
                    entradaMercadoriaEmAndamento.setDocumentoDetalhe(listaDocumentoDetalhe);

                    int quantidadeTotal = 0;
                    for (DocumentoDetalhe documentoDetalhe : listaDocumentoDetalhe) {
                        quantidadeTotal += documentoDetalhe.getQuantidadeMovimento().intValue();
                    }

                    entradaMercadoriaEmAndamento.setQuantidadeMovimento(new BigDecimal(quantidadeTotal));
                    entradaMercadoriaEmAndamento.setEstadoIntegracao("EM_ANDAMENTO");
                    integracaoRepository.salvarOperacaoEntrada(entradaMercadoriaEmAndamento);

                    // atualizar banco oraint
                    integracao.setEstadoIntegracao(new BigDecimal(ESTADOINTEGRACAO_REALIZADA_2));
                    integracaoRepository.atualizar(SqlUtil.getSqlUpdateIntegracao(integracao));

                    listaIntegracaoExecutada.add(integracao);

                } else {

                    Mercadoria entradaMercadoriaEmAndamento = new Mercadoria();
                    entradaMercadoriaEmAndamento.setSequenciaIntegracao(integracao.getSequenciaIntegracao());
                    entradaMercadoriaEmAndamento.setDocumento(documento);
                    entradaMercadoriaEmAndamento.setDocumentoDetalhe(listaDocumentoDetalhe);
                    BigDecimal quantidadeTotal = new BigDecimal(0);
                    for (DocumentoDetalhe documentoDetalhe : listaDocumentoDetalhe) {
                        quantidadeTotal.add(documentoDetalhe.getQuantidadeMovimento());
                    }
                    entradaMercadoriaEmAndamento.setQuantidadeMovimento(quantidadeTotal);
                    entradaMercadoriaEmAndamento.setEstadoIntegracao("PENDENTE");

                    integracaoRepository.salvarOperacaoEntrada(entradaMercadoriaEmAndamento);

                }
            } catch (Exception ex){

            }



        }

        return listaIntegracaoExecutada;
    }

    /**
     * Integracao 152
     * @param confirmacaoConferencia
     * @return
     */
    @Override
    public String conferenciaPedido(ConfirmacaoConferencia confirmacaoConferencia) {

        Mercadoria mercadoria = null;
        List<Conferencia> listaConferencia = null;
        int quantidadeTotalConferida = 0;
        String estadoIntegracao = EstadoIntegracaoEnum.NAO_EXECUTADA.toString();

        List<ListOfInventoryHistories> ListOfInventoryHistories = confirmacaoConferencia.getListOfInventoryHistories();

        for (ListOfInventoryHistories listOfInventoryHistories : ListOfInventoryHistories) {

            List<Inventory_history> listInventoryHistory = listOfInventoryHistories.getInventory_history();

            for (Inventory_history inventoryHistory : listInventoryHistory) {

                //**  ########## Abre: Conferencia em andamento ################## **/

                if(!inventoryHistory.getActivity_code().equalsIgnoreCase("27")){
                    mercadoria = mercadoriaRepository.obterMercadoriaPorNumeroDocumento(inventoryHistory.getPo_nbr());

                    logger.info("++++++++ CONFERIR Purchase Order numero {} = " , inventoryHistory.getPo_nbr());

                    //Salvar Conferencia DESCOMENTAR
                    conferenciaRepository.salvarConferencia(mercadoria, inventoryHistory);
                }

                //Dados da mercadoria

                logger.info("++++++++ CONFERIR Salvou conferencia {} = " , inventoryHistory.getPo_nbr());

                logger.info("++++++++ CONFERIR Obter a lista de conferencia {} = " , inventoryHistory.getPo_nbr());
                //Obter Lista de conferencia executada
                listaConferencia = conferenciaRepository.obterListaMercadoriaPorNumeroDocumento(inventoryHistory.getPo_nbr(), String.valueOf(mercadoria.getSequenciaIntegracao()));

                // TODO somar total de itens conferidos e comparar com o quantidade total mercadorias do mongo
                for (Conferencia conferencia : listaConferencia) {
                    quantidadeTotalConferida += conferencia.getQuantidadeMovimento().intValue();
                }

                BigDecimal totalConferida = BigDecimal.valueOf(quantidadeTotalConferida);
                //**  Fecha: Conferencia em andamento **/
                logger.info("++++++++ CONFERIR Conferrido ate o momento = " + totalConferida);

                //**  Abre: Conferencia finalizada **/

                // TODO QUANDO ACABOU A CONFERENCIA - TODOS ITENS CONFERIDOS
                // TODO somar total de itens conferidos e comparar com o quantidade total mercadorias do mongo

                if (totalConferida.intValue() >= mercadoria.getQuantidadeMovimento().intValue() || inventoryHistory.getActivity_code().equalsIgnoreCase("27")) {
                    logger.info("++++++++ CONFERIR Total conferido = " + totalConferida);

                    estadoIntegracao = EstadoIntegracaoEnum.FINALIZADA.toString();
                    Integracao integracao = null;
                    Documento documentoParaSalvar = null;
                    List<DocumentoDetalhe> listaDocumentoDetalheParaSalvar = new ArrayList<>();
                    List<DocumentoDetalhe> listaDocumentoDetalheDoOracle = new ArrayList<>();

                    List<DocumentoDetalheKey> listaKey = new ArrayList<>();

                    //TODO modificar o status NO FINAL(13/11)

                    BigDecimal sequenciaIntegracao = null;
                    int cont = 1;
                    for (Conferencia conferencia : listaConferencia) {
                        sequenciaIntegracao = conferencia.getSequenciaIntegracao();

                        if (cont == 1) {
                            listaDocumentoDetalheDoOracle = documentoRepository.obterDocumentoDetalhePorSequenciaIntegracao(conferencia.getSequenciaIntegracao());
                        }
                        Optional<DocumentoDetalhe> docOptional = listaDocumentoDetalheDoOracle.stream().filter(documentoDetalhe1 -> documentoDetalhe1.getSequenciaDetalhe().equals(new BigDecimal(conferencia.getSequenciaDocumentoDetalhe()))).findFirst();
                        String estadoMercadoria = "OK";
                        if (docOptional.isPresent()) {
                            DocumentoDetalhe documentoDetalhe = docOptional.get();
                            documentoDetalhe.setQuantidadeMovimento(conferencia.getQuantidadeMovimento());
                            if (!conferencia.getEstadoMercadoria().equalsIgnoreCase("")) {
                                estadoMercadoria = conferencia.getEstadoMercadoria();
                            } else {
                                estadoMercadoria = "OK";
                            }
                            documentoDetalhe.setClasseProduto(estadoMercadoria);
                            listaKey.add(new DocumentoDetalheKey(estadoMercadoria, Integer.valueOf(conferencia.getSequenciaDocumentoDetalhe()), conferencia.getQuantidadeMovimento().intValue()));
                        }
                    }


                    int i = 1;
                    BigDecimal quantidadeConferida = null;

                    for (DocumentoDetalheKey key : listaKey) {


                        Optional<DocumentoDetalhe> listaDocumentoDetalhe = listaDocumentoDetalheParaSalvar.stream().filter(documentoDetalhe1 -> documentoDetalhe1.getSequenciaDetalhe().equals(new BigDecimal(key.getSequenciaDetalhe()))).findFirst();
                        if (listaDocumentoDetalhe.isPresent()) {
                            DocumentoDetalhe detalhe = listaDocumentoDetalhe.get();

                            if (detalhe.getClasseProduto().equalsIgnoreCase(key.getClasseProduto())) {
                                //detalhe.setClasseProduto(key.getClasseProduto());

                                int totalJaConferida = detalhe.getQuantidadeMovimento().intValue();
                                int totalConferindo = key.getQuantidade();
                                int soma = totalJaConferida + totalConferindo;
                                detalhe.setQuantidadeMovimento(new BigDecimal(soma));
                                //detalhe.setSequenciaDetalheNovo(i);
                                //listaDocumentoDetalheParaSalvar.add(detalhe);


                            } else {
                                Optional<DocumentoDetalhe> listaDocumentoDetalheNovo = listaDocumentoDetalheDoOracle.stream().filter(documentoDetalhe1 -> documentoDetalhe1.getSequenciaDetalhe().equals(new BigDecimal(key.getSequenciaDetalhe()))).findFirst();
                                if (listaDocumentoDetalheNovo.isPresent()) {
                                    DocumentoDetalhe detalheSalvo = listaDocumentoDetalheNovo.get();
                                    DocumentoDetalhe detalhe2 = null;
                                    try {
                                        detalhe2 = (DocumentoDetalhe) detalheSalvo.clone();
                                        detalhe2.setClasseProduto(key.getClasseProduto());
                                        quantidadeConferida = detalhe2.getQuantidadeMovimento();
                                        quantidadeConferida.add(new BigDecimal(key.getQuantidade()));
                                        detalhe2.setQuantidadeMovimento(quantidadeConferida);
                                        detalhe2.setSequenciaDetalheNovo(i);
                                        listaDocumentoDetalheParaSalvar.add(detalhe2);
                                        i++;
                                    } catch (CloneNotSupportedException e) {
                                        e.printStackTrace();
                                    }

                                }
                            }

                        } else {
                            Optional<DocumentoDetalhe> listaDocumentoDetalheNovo = listaDocumentoDetalheDoOracle.stream().filter(documentoDetalhe1 -> documentoDetalhe1.getSequenciaDetalhe().equals(new BigDecimal(key.getSequenciaDetalhe()))).findFirst();
                            if (listaDocumentoDetalheNovo.isPresent()) {
                                DocumentoDetalhe detalhe = listaDocumentoDetalheNovo.get();
                                detalhe.setClasseProduto(key.getClasseProduto());
                                detalhe.setQuantidadeMovimento(new BigDecimal(key.getQuantidade()));
                                detalhe.setSequenciaDetalheNovo(i);
                                listaDocumentoDetalheParaSalvar.add(detalhe);
                                i++;
                            }
                        }


                    }

                    logger.info("++++++++ CONFERIR Salvar nova integracao ");
                    //Salvar integracao
                    Integracao novaIntegracao = new Integracao();
                    Date date = new Date();

                    DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
                    long time = date.getTime();


                    novaIntegracao.setDataLog(new Timestamp(time));
                    novaIntegracao.setDataProcessamento(new Timestamp(time));
                    // Verificar se e cancelamento
                    if(inventoryHistory.getActivity_code().equalsIgnoreCase("27")){
                        novaIntegracao.setTipoIntegracao(BigDecimal.valueOf(251));
                    } else {
                        novaIntegracao.setTipoIntegracao(BigDecimal.valueOf(152));
                    }

                    novaIntegracao.setEstadoIntegracao(BigDecimal.valueOf(1));

                    BigDecimal novaSequenciaIntegracao = integracaoRepository.salvarIntegracao(SqlUtil.getSqlInsertIntegracao(null, novaIntegracao));

                    logger.info("++++++++ CONFERIR Integracao salva -> " + novaSequenciaIntegracao);

                    BigDecimal antigaSequenciaIntegracaoParaCancelamento = BigDecimal.valueOf(0);
                    if(inventoryHistory.getActivity_code().equalsIgnoreCase("27")){


                        documentoParaSalvar = documentoRepository.obterDocumentoParaCancelamento(inventoryHistory.getOrder_nbr(), inventoryHistory.getFacility_code(), BigDecimal.valueOf(203));
                        antigaSequenciaIntegracaoParaCancelamento = documentoParaSalvar.getSequenciaIntegracao();
                        documentoParaSalvar.setSequenciaIntegracao(novaSequenciaIntegracao);

                        documentoParaSalvar.setTipoIntegracao(BigDecimal.valueOf(251));


                        mercadoria.setEstadoIntegracao("CANCELADA");
                        integracaoRepository.salvarOperacaoEntrada(mercadoria);


                    } else {
                        documentoParaSalvar = documentoRepository.obterDocumentoPorSequenciaIntegracaoNumeroDocumentoCodigoDepositante(sequenciaIntegracao, inventoryHistory.getPo_nbr(), inventoryHistory.getFacility_code());
                        documentoParaSalvar.setSequenciaIntegracao(novaSequenciaIntegracao);
                        documentoParaSalvar.setTipoIntegracao(BigDecimal.valueOf(152));
                    }

                    documentoParaSalvar.setDataMovimento(new Date());



                    try {
                        TimeZone tz = TimeZone.getTimeZone("America/Sao_Paulo");
                        Calendar cal = Calendar.getInstance(tz);
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                        sdf.setCalendar(cal);
                        cal.setTime(sdf.parse(inventoryHistory.getCreate_ts()));
                        Date dateConverted = cal.getTime();
                        documentoParaSalvar.setDataPrevisaoMovimento(dateConverted);
                    } catch (ParseException e) {
                        documentoParaSalvar.setDataPrevisaoMovimento(new Date());
                    }

                    //2019-01-18T14:31:11

                    novaIntegracao.setSequenciaIntegracao(novaSequenciaIntegracao);

                    Documento docTaSalvo = documentoRepository.obterDocumentoParaCancelamento(inventoryHistory.getOrder_nbr(), inventoryHistory.getFacility_code(), BigDecimal.valueOf(152));

                    if(docTaSalvo == null){
                        integracaoRepository.salvarDocumento(SqlUtil.getSqlInsertDocumento(documentoParaSalvar, novaIntegracao));
                        logger.info("++++++++ CONFERIR Documento salvo -> " + documentoParaSalvar.getNumeroDocumento());
                    }



                    if(inventoryHistory.getActivity_code().equalsIgnoreCase("27")){



                        listaDocumentoDetalheParaSalvar = documentoRepository.obterDocumentoDetalhePorSequenciaIntegracao(antigaSequenciaIntegracaoParaCancelamento);
                    }

                    for (DocumentoDetalhe docDetalhe : listaDocumentoDetalheParaSalvar) {
                        docDetalhe.setSequenciaIntegracao(novaSequenciaIntegracao);
                        docDetalhe.setValorUnitario(BigDecimal.valueOf(1));
                        String facilityCode = listInventoryHistory.get(0).getFacility_code();
                        System.out.print(docDetalhe.getClasseProduto() + " Sequencia " + docDetalhe.getSequenciaDetalheNovo() + " Quandtidade " + docDetalhe.getQuantidadeMovimento());

                        DocumentoDetalhe docDetakheTaSalvo = documentoRepository.obterDocumentoDetalheSalvo(novaSequenciaIntegracao, docDetalhe.getSequenciaDetalhe());
                        if(inventoryHistory.getActivity_code().equalsIgnoreCase("27")){
                            docDetalhe.setTipoIntegracao("251");
                        } else {
                            docDetalhe.setTipoIntegracao("152");
                        }

                        Integracao nIntegracao = new Integracao();
                        nIntegracao.setSequenciaIntegracao(novaSequenciaIntegracao);
                        logger.info("++++++++ CONFERIR Documento detalhe salvo numero integracao -> " + nIntegracao.getSequenciaIntegracao());
                        logger.info("++++++++ CONFERIR Documento detalhe salvo quantidade -> " + docDetalhe.getQuantidadeMovimento());
                        if(docDetakheTaSalvo == null){
                            integracaoRepository.salvarDocumento(SqlUtil.getSqlInsertIntegracaoDtl(documentoParaSalvar, docDetalhe, nIntegracao, facilityCode));
                        }

                    }


                    // atualizar banco oraint


                    Integracao antigaIntegracao = new Integracao();
                    Date mDate = new Date();

                    long newTime = mDate.getTime();


                    antigaIntegracao.setDataLog(new Timestamp(newTime));
                    antigaIntegracao.setDataProcessamento(new Timestamp(newTime));
                    antigaIntegracao.setEstadoIntegracao(new BigDecimal(2));
                    antigaIntegracao.setSequenciaIntegracao(sequenciaIntegracao);
                    logger.info("++++++++ INTEGRACAO executada ++++++++++" + sequenciaIntegracao);
                    //integracaoRepository.atualizar(SqlUtil.getSqlUpdateIntegracao(antigaIntegracao));
                    logger.info("++++++++ ORAINT ATUALIZADO ++++++++++");
                    //**  Fecha: Conferencia finalizada **/
                    estadoIntegracao = EstadoIntegracaoEnum.FINALIZADA.toString();

                    mercadoria.setEstadoIntegracao(estadoIntegracao);
                    mercadoriaRepository.salvarMercadoriaEmMovimento(mercadoria);


                } else {
                    quantidadeTotalConferida = 0;
                    estadoIntegracao = EstadoIntegracaoEnum.EM_ANDAMENTO.toString();
                }

            }

        }


        return estadoIntegracao;
    }

    private String salvarAtualizarIntegracao() {


        return "OK";
    }

    /**
     * #################################
     * PROCESSO DE SAIDA DE MERCADORIA
     * #################################
     */


    @Override
    public List<Integracao> listaIntegracaoLogix() {

        // Obter as integraçoes de saida de mercadoria na fila para serem executadas
        List<Integracao> listaIntegracao = integracaoRepository.listaIntegracaoLogix();
        List<Integracao> listaIntegracaoExecutada = new ArrayList<>();

        // Obter documentos das integracoes de saida
        Documento documento = new Documento();
        List<DocumentoDetalhe> listaDocumentoDetalhe = new ArrayList<>();
        int total = listaIntegracao.size();
        for (Integracao integracao : listaIntegracao) {

            logger.info("INICIANDO INTEGRACAO LOGIX. Quantidade para processar -> {} ", total);
            // Buscar o produto passsando sequencia integracao
            // Buscar o TipoUC passsando sequencia integracao
            // Buscar o Produto componente passsando sequencia integracao

            //String xmlPO = XmlUtil.getXmlPO(documento, listaDocumentoDetalhe, integracao);
            ResponseEntity<String> response = null;


            List<Produto> produtos = produtoService.obterListaProdutoPorSequenciaIntegracao(integracao.getSequenciaIntegracao().toString()); // TESTE "9927126"
            logger.info("LOGIX #### Iniciando processamento da integracao : {} ####", integracao.getSequenciaIntegracao());
            for (Produto produto : produtos) {

                TipoUc tipoUc = produtoService.obterTipoUcPorCodigoProdutoESequenciaIntegracao(produto.getSequenciaIntegracao().toString(), produto.getCodigoProduto());

                if(tipoUc == null){
                    tipoUc = new TipoUc();
                    tipoUc.setComprimentoProduto(BigDecimal.valueOf(1));
                    tipoUc.setLarguraProduto(BigDecimal.valueOf(1));
                    tipoUc.setAlturaProduto(BigDecimal.valueOf(1));
                    tipoUc.setPesoLiquido(BigDecimal.valueOf(1));
                }

                List<ProdutoComponente> produtoComponentes = produtoService.obterProdutoComponentePorCodigoProdutoESequenciaIntegracao(produto.getSequenciaIntegracao().toString(), produto.getCodigoProduto());
                BigDecimal somaQuantidadeComponentes = BigDecimal.valueOf(0);
                int somarQtdComponente = 0;
                if(produtoComponentes.size() > 0){
                    for (ProdutoComponente produtoComponente : produtoComponentes) {
                        somarQtdComponente += produtoComponente.getQuantidadeComponente().intValue();
                        somaQuantidadeComponentes.add(produtoComponente.getQuantidadeComponente());
                    }
                }

                String stringQuantidadeComponentes = String.valueOf(somarQtdComponente);
                String qtdTratada = "";


                if(stringQuantidadeComponentes.length() == 1 ){
                    qtdTratada = "000" + stringQuantidadeComponentes;
                } else  if(stringQuantidadeComponentes.length() == 2 ) {
                    qtdTratada = "00" + stringQuantidadeComponentes;
                } else  if(stringQuantidadeComponentes.length() == 3 ) {
                    qtdTratada = "0" + stringQuantidadeComponentes;
                } else  if(stringQuantidadeComponentes.length() == 0 ) {
                    qtdTratada = "0000";
                }

                //TODO Montar o Item
                String xmlItem = XmlUtil.
                        getXmlItem(integracao, produto, tipoUc, qtdTratada);
                //logger.info("LOGIX #### Iniciando processamento da integracao : {} ####", integracao.getSequenciaIntegracao());

                // Enviar Item para o WMS
                response = connector.connectorSendXmlWms(xmlItem);
                //logger.info("Enviar Item para o WMS : {} ", response);
                //logger.info("Item enviado pro WMS : {} ", integracao.getSequenciaIntegracao());

                //TODO Montar o ItemPrepack

                String xmlItemPrePack = XmlUtil.getXmlItemPrePack(integracao, produto, produtoComponentes);
                //logger.info("XML xmlItemPrePack : {} ", xmlItemPrePack);

                // Enviar Item PrePack para o WMS
                connector.connectorSendXmlWms(xmlItemPrePack);
                //logger.info("Enviar ItemPrePack para o WMS : {} ", response);

            }
            if(response != null){
                if (HttpStatus.OK.equals(response.getStatusCode())) {

                    Mercadoria mercadoria = new Mercadoria();
                    mercadoria.setSequenciaIntegracao(integracao.getSequenciaIntegracao());
                    mercadoria.setNumeroDocumento(documento.getNumeroDocumento());
                    mercadoria.setDocumento(documento);
                    mercadoria.setDocumentoDetalhe(listaDocumentoDetalhe);

                    //TODO

                    //mercadoria.setQuantidadeMovimento(listaDocumentoDetalhe.get(0).getQuantidadeMovimento());
                    mercadoria.setEstadoIntegracao("SAIDA");

                    //  integracaoRepository.salvarOperacaoEntrada(mercadoria);
                    listaIntegracaoExecutada.add(integracao);
                    logger.info("##### Concluindo integracao : {} ####", integracao.getSequenciaIntegracao());
                    //integracao.setEstadoIntegracao(new BigDecimal(1));
                    integracao.setReferencia("1");
                    integracaoRepository.atualizar(SqlUtil.getSqlUpdateIntegracaoLogix(integracao));

                } else {

                    Mercadoria mercadoria = new Mercadoria();
                    mercadoria.setSequenciaIntegracao(integracao.getSequenciaIntegracao());
                    mercadoria.setNumeroDocumento(documento.getNumeroDocumento());
                    mercadoria.setDocumento(documento);
                    //TODO
                    //mercadoria.setDocumentoDetalhe(listaDocumentoDetalhe.get(0));
                    mercadoria.setQuantidadeMovimento(listaDocumentoDetalhe.get(0).getQuantidadeMovimento());
                    mercadoria.setEstadoIntegracao("PENDENTE");

                    //    integracaoRepository.salvarOperacaoEntrada(mercadoria);

                }
            }else {
                integracao.setReferencia("3");
                integracaoRepository.atualizar(SqlUtil.getSqlUpdateIntegracaoLogix(integracao));
            }

            total--;
        }

        logger.info("Integracao LOGIX Finalizada. Total processado: {} ", listaIntegracaoExecutada.size());
        return listaIntegracaoExecutada;
    }

    private LocationType preencherLocation(Documento documento, DocumentoDetalhe documentoDetalhe) {
        LocationType locationType = new LocationType();
        // Mapear os campos para preencher o item

        return locationType;
    }


    private ItemType preencherItem(Documento documento, DocumentoDetalhe documentoDetalhe) {
        ItemType itemType = new ItemType();
        // Mapear os campos para preencher o item
        return itemType;
    }


    public boolean SalvarConferencia() {
        Conferencia conferencia = new Conferencia();
        //conferencia.se
        return true;
    }


    @Override
    public String pedidoCompraPOCDrogariaSaoPaulo(String xmlPO) {

        // ISSO AQUI É UMA ENTRADA DE MERCADORIA NO ARMAZEM

        //1 Envia XML PO - Pedido de entrada de mercadoria pro WMS
        connector.connectorSendXmlWms(xmlPO);

        OrdemDeCompra ordemDeCompra = null;
        try {
            XStream xstream = new XStream();
            xstream.processAnnotations(OrdemDeCompra.class);
            xstream.processAnnotations(Header.class);
            String n = xmlPO.replace("\n", "").replace("\t", "");
            ordemDeCompra = (OrdemDeCompra) xstream.fromXML(n);

            logger.info("Recebendo XML -> " + xmlPO);

        } catch (Exception e) {
            logger.error("Erro na conversão do XML", e.getMessage());
        }

        //2 Sync monta xml IB Shipment;
        List<ListOfPurchaseOrders> listOfPurchaseOrders = ordemDeCompra.getListOfPurchaseOrders();
        String xmlIbShipment = XmlUtil.getXmlIbShipment(listOfPurchaseOrders);
        logger.info("XML xmlIbShipment: {}", xmlIbShipment);

        //ordemDeCompra.getListOfPurchaseOrders().get(0).getPurchase_order().getPurchase_order_hdr().getFacility_code()

        //3 Sync envia xml IB Shipment para WMS
        ResponseEntity<String> responseEntityShipment = connector.connectorSendXmlWms(xmlIbShipment);
        logger.info("Resultado responseEntityShipment: {}", responseEntityShipment);

        //4 Sync monta xml Appointment;
        //a.      Campo <duration> será calculado através da media de tempo de recebimento por Kilo do fornecedor; // Du -
        //b.      Campo <planned_start_ts> será montado com a concatenação do campo <ship_date> do xml da PO, com o horário fixo “11:00:00”
        String xmlAppointment = XmlUtil.getXmlAppointment(listOfPurchaseOrders);
        logger.info("XML xmlAppointment: {}", xmlAppointment);


        //5 Sync envia xml Appointment para WMS Cloud
        ResponseEntity<String> responseEntityAppointment = connector.connectorSendXmlWms(xmlAppointment);
        logger.info("Resultado responseEntityAppointment: {}", responseEntityAppointment);

        return null;
    }

    @Override
    public String conferenciaPedidoDrogariaSaoPaulo(ConfirmacaoConferencia confirmacaoConferencia) {

        Mercadoria mercadoria = null;
        List<Conferencia> listaConferencia = null;
        int quantidadeTotalConferida = 0;
        String estadoIntegracao = EstadoIntegracaoEnum.NAO_EXECUTADA.toString();

        List<ListOfInventoryHistories> ListOfInventoryHistories = confirmacaoConferencia.getListOfInventoryHistories();

        for (ListOfInventoryHistories listOfInventoryHistories : ListOfInventoryHistories) {

            List<Inventory_history> listInventoryHistory = listOfInventoryHistories.getInventory_history();

            for (Inventory_history inventoryHistory : listInventoryHistory) {

                //**  ########## Abre: Conferencia em andamento ################## **/

                //Dados da mercadoria
                mercadoria = mercadoriaRepository.obterMercadoriaPorNumeroDocumento(inventoryHistory.getPo_nbr());

                //Salvar mercadoria conferida
                conferenciaRepository.salvarConferencia(mercadoria, inventoryHistory);

                // TODO SALVAR DATA DO INICIO DA CONFERENCIA(create_ts da primeira conferencia)
                //WMS Cloud envia xml de confirmação do recebimento (mesmo utilizado para o projeto GAV);
                //a.Sync deverá observar a tag <create_ts> de todos os XML enviados para confirmar. Deverá verificar qual o intervalo de tempo entre o primeiro <create_ts> recebido até o último recebido (quando atingir a quantidade total esperado pelo PO.
                //Data inicial = inventoryHistory.getCreate_ts();


                //Obter Lista de conferencia executada
                listaConferencia = conferenciaRepository.obterListaMercadoriaPorNumeroDocumento(inventoryHistory.getPo_nbr(),String.valueOf(mercadoria.getSequenciaIntegracao()) );

                //somar total de itens conferidos e comparar com o quantidade total mercadorias do mongo
                for (Conferencia conferencia : listaConferencia) {
                    quantidadeTotalConferida += conferencia.getQuantidadeMovimento().intValue();
                }

                BigDecimal totalConferida = BigDecimal.valueOf(quantidadeTotalConferida);
                //**  Fecha: Conferencia em andamento **/


                //**  Abre: Conferencia finalizada **/

                // somar total de itens conferidos e comparar com o quantidade total mercadorias do mongo
                if (totalConferida.equals(mercadoria.getQuantidadeMovimento())) {


                    // TODO SALVAR DATA DO FIM DA CONFERENCIA(create_ts da ultima conferencia)
                    //Data inicial = inventoryHistory.getCreate_ts();


                    // TODO Somar todos os pesos dos itens conferidos
                    Double pesoTotal = 0d;
                    for (Conferencia conferencia : listaConferencia) {
                        //pesoTotal += conferencia.getPeso();
                    }

                    // TODO Montar o xml verify com media de tempo de descarga e codigo do vendor
                    // Montar a media = quantidade de tempo / peso total conferido


                    // TODO Enviar pro WMS


                    //**  Fecha: Conferencia finalizada **/
                    estadoIntegracao = EstadoIntegracaoEnum.FINALIZADA.toString();

                    mercadoria.setEstadoIntegracao(estadoIntegracao);
                    mercadoriaRepository.salvarMercadoriaEmMovimento(mercadoria);


                } else {
                    quantidadeTotalConferida = 0;
                    estadoIntegracao = EstadoIntegracaoEnum.EM_ANDAMENTO.toString();
                }

            }

        }


        return estadoIntegracao;
    }

    @Override
    public boolean finalizarCompra(SaleDto saleDto) {
        String xmlSale = XmlUtil.getXmlSale(saleDto);
        logger.info("XML xmlSale: {}", xmlSale);
        // envia xml xmlSale para WMS Cloud
        ResponseEntity<String> response = connector.connectorSendXmlWms(xmlSale);
        logger.info("Resultado xmlSale: {}", response);

        return HttpStatus.OK.equals(response.getStatusCode());
    }

    @Override
    public String salvarEmbalagem(String strXmlShipped) {

        LgfDataShipped shipped = null;
        logger.info("Recebendo XML - Carga enviada");

        try {
            XStream xstream = new XStream(new DomDriver("UTF_8", new NoNameCoder()));
            xstream.processAnnotations(LgfDataShipped.class);
            xstream.processAnnotations(Header.class);
            shipped = (LgfDataShipped) xstream.fromXML(strXmlShipped);

            ListOfShippedLoads listOfShippedLoads = shipped.getListOfShippedLoads();

            List<ShippedLoad> shippedLoads = listOfShippedLoads.getShippedLoads();

            List<Embalagem> embalagems = new ArrayList<>();

            for (ShippedLoad shippedLoad : shippedLoads) {

                Embalagem embalagem = new Embalagem();
                embalagem.setFacility_code(shippedLoad.getLoad().getFacility_code());
                embalagem.setCompany_code(shippedLoad.getLoad().getCompany_code());
               // embalagem.setOb_lpn_nbr(shippedLoad.getOb_stop().getOb_lpn_nbr());
               // embalagem.setOb_lpn_weight(shippedLoad.getOb_stop().getOb_lpn_weight());
                embalagems.add(embalagem);
            }

            return integracaoRepository.salvarEmbalagem(embalagems);

        } catch (Exception e) {
            logger.info("Problema ao coverter XML -> " + e.getMessage());
        }

        return null;

    }

    @Override
    public List<Embalagem> obterEmbalagens(String code) {
        return integracaoRepository.obterEmbalagens(code);
    }

    //203
    @Override
    public List<Integracao> saidaMercadoria() {

        // Obter as integraçoes de saida de mercadoria na fila para serem executadas
        List<Integracao> listaIntegracao = integracaoRepository.listaIntegracaoEntradaMercadoriaPrevisaoDeSaida();
        List<Integracao> listaIntegracaoExecutada = new ArrayList<>();

        logger.info("############# QUANTIDADE PARA INTEGRAR ########### >>> " + listaIntegracao.size());
        // Obter documentos das integracoes de saida
        Documento documento = new Documento();
        List<DocumentoDetalhe> listaDocumentoDetalhe = new ArrayList<>();

        for (Integracao integracao : listaIntegracao) {

            documento = documentoRepository.obterDocumentoPorSequenciaIntegracao(integracao.getSequenciaIntegracao());
            ResponseEntity<String> response = null;
            if ( documento != null){
                listaDocumentoDetalhe = documentoRepository.obterDocumentoDetalhePorSequenciaIntegracao(integracao.getSequenciaIntegracao());

                // Buscar o produto passsando sequencia integracao
                // Buscar o TipoUC passsando sequenciae integracao
                // Buscar o Produto componente passsando sequencia integracao

                //String xmlPO = XmlUtil.getXmlPO(documento, listaDocumentoDetalhe, integracao);


                BigDecimal somaQuantidadeMovimento = BigDecimal.valueOf(0);
                if(listaDocumentoDetalhe.size() > 0){
                    for (DocumentoDetalhe documentoDetalhe : listaDocumentoDetalhe) {
                        somaQuantidadeMovimento.add(documentoDetalhe.getQuantidadeMovimento());
                    }
                }

                String stringQuantidadeMovimento = String.valueOf(somaQuantidadeMovimento);
                String stringQuantidadeMovimentoTratado = "";


                if(stringQuantidadeMovimento.length() == 1 ){
                    stringQuantidadeMovimentoTratado = "0" + somaQuantidadeMovimento;
                }

                if(listaDocumentoDetalhe.size() > 0 && documento != null){
                    String xmlOrderRelease = XmlUtil.getXmlOrder(integracao, documento, listaDocumentoDetalhe, stringQuantidadeMovimentoTratado);

                    String novoXml = xmlOrderRelease.replace("__", "_");

                    response = connector.connectorSendXmlWms(novoXml);

                    if (response != null &&HttpStatus.OK.equals(response.getStatusCode())) {

                        Mercadoria mercadoria = new Mercadoria();
                        mercadoria.setSequenciaIntegracao(integracao.getSequenciaIntegracao());
                        mercadoria.setNumeroDocumento(documento.getNumeroDocumento());
                        mercadoria.setDocumento(documento);
                        mercadoria.setDocumentoDetalhe(listaDocumentoDetalhe);

                        //TODO

                        //mercadoria.setQuantidadeMovimento(listaDocumentoDetalhe.get(0).getQuantidadeMovimento());
                        mercadoria.setEstadoIntegracao("SAIDA");

                        //  integracaoRepository.salvarOperacaoEntrada(mercadoria);
                        listaIntegracaoExecutada.add(integracao);

                        integracao.setEstadoIntegracao(new BigDecimal(2));
                        integracaoRepository.atualizar(SqlUtil.getSqlUpdateIntegracao(integracao));

                        logger.info("############# INTEGRACAO REALIZADA ########### >>> " + integracao.getSequenciaIntegracao());

                    }
                }

            }

            //response = connector.connectorSendXmlOtm(xmlOrderRelease);

           // List<Produto> produtos = produtoService.obterListaProdutoPorSequenciaIntegracao(integracao.getSequenciaIntegracao().toString()); // TESTE "9927126"

//            for (Produto produto : produtos) {
//
//                TipoUc tipoUc = produtoService.obterTipoUcPorCodigoProdutoESequenciaIntegracao(produto.getSequenciaIntegracao().toString(), produto.getCodigoProduto());
//
//                //TODO Montar o Item
//                String xmlItem = XmlUtil.
//                        getXmlItem(integracao, produto, tipoUc, "0000");
//             //   logger.info("XML xmlItem : {} ", xmlItem);
//
//                // Enviar Item para o WMS
//                //response = connector.connectorSendXmlWms(xmlItem);
//                //logger.info("Enviar Item para o WMS : {} ", response);
//
//                //TODO Montar o ItemPrepack
//                List<ProdutoComponente> produtoComponentes = produtoService.obterProdutoComponentePorCodigoProdutoESequenciaIntegracao(produto.getSequenciaIntegracao().toString(), produto.getCodigoProduto());
//                String xmlItemPrePack = XmlUtil.getXmlItemPrePack(integracao, produto, produtoComponentes);
//                //logger.info("XML xmlItemPrePack : {} ", xmlItemPrePack);
//
//                // Enviar Item PrePack para o WMS
//               // response = connector.connectorSendXmlWms(xmlItemPrePack);
//                //logger.info("Enviar ItemPrePack para o WMS : {} ", response);
//
//            }




        }


        return null;
    }



    @Override
    public String shipload252(String strXmlShipped) {

        LgfDataShipped shipped = null;
        logger.info("Recebendo XML - Shipload recebido");

        try {
            XStream xstream = new XStream(new DomDriver("UTF_8", new NoNameCoder()));
            xstream.processAnnotations(LgfDataShipped.class);
            xstream.processAnnotations(Header.class);
            shipped = (LgfDataShipped) xstream.fromXML(strXmlShipped);

            ListOfShippedLoads listOfShippedLoads = shipped.getListOfShippedLoads();

            List<ShippedLoad> shippedLoads = listOfShippedLoads.getShippedLoads();



            BigDecimal antigaIntegracao = null;

            Documento documento = null;
            DocumentoDetalhe documentoDetalhe = null;
            List<DocumentoDetalhe> listDocumentoDetalhe = null;
            List<Documento> listDocumento = new ArrayList<>();

            List<Embalagem> listEmbalagem = new ArrayList<>();


            for (ShippedLoad shippedLoad : shippedLoads) {

                List<ObStop> listObStop = shippedLoad.getOb_stop();
                int count = 1;
                for (ObStop obStop : listObStop) {
                    Documento documentoJaFoiSalvo = documentoRepository.obterDocumentoDIntegracao203(obStop.getOrder_nbr(), obStop.getOrder_hdr_cust_field_5(), shippedLoad.getLoad().getFacility_code(), BigDecimal.valueOf(252));

                    //DOCUMENTO
                    if(documentoJaFoiSalvo == null){
                        documento = documentoRepository.obterDocumentoDIntegracao203(obStop.getOrder_nbr(), obStop.getOrder_hdr_cust_field_5(), shippedLoad.getLoad().getFacility_code(), BigDecimal.valueOf(203));
                        antigaIntegracao = documento.getSequenciaIntegracao();
                        documento.setTipoEntrega(obStop.getOb_lpn_type());

                       // documento.setSequenciaIntegracao(novaSequenciaIntegracao);
                        documento.setTipoIntegracao(BigDecimal.valueOf(252));
                        listDocumento.add(documento);


                        //Salva documento


                    }


                    DocumentoDetalhe docDetalhe = documentoRepository.obterDocumentoDetalhePorSequenciaIntegracaoNumedoDocumento(antigaIntegracao, documento.getSequenciaDocumento());

                    //EMBALAGEM

                    Embalagem embalagem = new Embalagem();
                    embalagem.setFacility_code(shippedLoad.getLoad().getFacility_code());
                    embalagem.setCompany_code(shippedLoad.getLoad().getCompany_code());
                    embalagem.setOb_lpn_nbr(obStop.getOb_lpn_nbr());
                    embalagem.setOb_lpn_weight(obStop.getOb_lpn_weight());
                    embalagem.setLpnType(obStop.getOb_lpn_type());
                    embalagem.setDocumentoDetalhe(docDetalhe);
                    listEmbalagem.add(embalagem);
                    //Salva Documento embalagem



                    //DOCUMENTO DETALHE
                    listDocumentoDetalhe = documentoRepository.obterListaDocumentoDetalhePorSequenciaIntegracaoNumedoDocumento(antigaIntegracao, obStop.getOrder_nbr());
                    // docDetalhe.setTipoIntegracao("252");



                }

                //Salvar integracao
                Integracao novaIntegracao = new Integracao();
                Date date = new Date();

                DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
                long time = date.getTime();


                novaIntegracao.setDataLog(new Timestamp(time));
                novaIntegracao.setDataProcessamento(new Timestamp(time));
                novaIntegracao.setTipoIntegracao(BigDecimal.valueOf(252));
                novaIntegracao.setEstadoIntegracao(BigDecimal.valueOf(1));


                BigDecimal novaSequenciaIntegracao = integracaoRepository.salvarIntegracao(SqlUtil.getSqlInsertIntegracao(null, novaIntegracao));



                for (Documento doc : listDocumento) {
                    Documento documentoJaFoiSalvo = documentoRepository.obterDocumentoDIntegracao203(doc.getNumeroDocumento(), doc.getAgrupador(), shippedLoad.getLoad().getFacility_code(), BigDecimal.valueOf(252));

                    //DOCUMENTO
                    if(documentoJaFoiSalvo == null) {
                        doc.setSequenciaIntegracao(novaSequenciaIntegracao);
                        integracaoRepository.salvarDocumento(SqlUtil.getSqlInsertDocumento(doc, novaIntegracao));
                    }
                }

                Integracao integracao = new Integracao();
                integracao.setSequenciaIntegracao(novaSequenciaIntegracao);
                int seqDet = 1;
                for (DocumentoDetalhe docDetalhe : listDocumentoDetalhe) {
                    docDetalhe.setTipoIntegracao("252");
                    docDetalhe.setSequenciaDetalhe(BigDecimal.valueOf(seqDet));
                    integracaoRepository.salvarDocumento(SqlUtil.getSqlInsertIntegracaoDtl(documento, docDetalhe, integracao, String.valueOf(documento.getCodigoEstabelecimento())));
                    seqDet++;
                }

                int i = 1;
                for (Embalagem embalagem : listEmbalagem) {
                    integracaoRepository.salvarDocumento(SqlUtil.getSqlInsertDocumentoEmbalagem(embalagem, integracao, embalagem.getDocumentoDetalhe(), i));
                    i++;
                }



                //ROMANEIO
//                Romaneio romaneio = new Romaneio();
//                romaneio.setSequenciaIntegracao(novaSequenciaIntegracao);
//                romaneio.setSequenciaRomaneio(BigDecimal.valueOf(1));
//                romaneio.setTipoIntegracao(BigDecimal.valueOf(252));
//                romaneio.setNumeroRomaneio("059");
//                romaneio.setEnderecoDoca("DC10");
//                // Salva romaneio
//                integracaoRepository.salvarDocumento(SqlUtil.getSqlInsertRomaneio(romaneio));


            }


            return null;

        } catch (Exception e) {
            logger.info("Problema ao coverter XML -> " + e.getMessage());
        }

        return null;

    }



}
