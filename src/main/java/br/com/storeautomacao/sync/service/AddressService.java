package br.com.storeautomacao.sync.service;

import br.com.storeautomacao.sync.model.entity.Address;
import br.com.storeautomacao.sync.repository.AddressRepository;
import com.google.maps.GeoApiContext;
import com.google.maps.GeocodingApi;
import com.google.maps.model.GeocodingResult;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by alexandre on 12/09/17.
 */
@Service
public class AddressService {

    @Autowired
    private AddressRepository repository;

    @Value("${google.maps.server.api.key}")
    private String googleMapsServerApiKey;


    public String create(Address address) {
        this.setGeoCoordinate(address);
        return repository.create(address);
    }

    public List<Address> findAll() { return repository.findAll(); }

    public Address findById(String id) {return repository.findById(id); }

    public List<Address> findByText(String text) { return repository.findByText(text); }

    /**
     * Retorna as coordenadas geográficas de um endereço, baseado na API do Google.
     *
     * @param streetAddress
     *
     * @return
     */
    @Async
    public double[] findCoordinates(String streetAddress) {
        double[] coordinates = null;
        try {
            if (streetAddress != null) {
                GeoApiContext context = new GeoApiContext.Builder().apiKey(googleMapsServerApiKey).build();
                GeocodingResult[] results = GeocodingApi.geocode(
                        context,
                        streetAddress).await();

                if (results != null && results.length > 0) {
                    coordinates =
                            new double[] {results[0].geometry.location.lat, results[0].geometry.location.lng};
                }
            }
        } catch (Exception e) {
            coordinates = null;
        }

        return coordinates;
    }

    /**
     * Define as coordenadas geográficas de um endereço, caso este nao possua.
     *
     * @param address
     */
    private void setGeoCoordinate(Address address) {
        if (address != null
                && StringUtils.isEmpty(address.getLatitude())
                && StringUtils.isEmpty(address.getLogitude())) {
            double[] coordinates = findCoordinates(address.getFormattedAddress());
            if (coordinates != null) {
                address.setCoordinates(coordinates);
            }
        }
    }

    public String update(Address address) {
        this.setGeoCoordinate(address);
        return repository.create(address);
    }

    public void remove(String id) { repository.remove(id); }

    public void remove(Address address) {
        repository.remove(address);
    }
}
