package br.com.storeautomacao.sync.service.impl;

import br.com.storeautomacao.sync.repository.RomaneioRepository;
import br.com.storeautomacao.sync.service.AbstractService;
import br.com.storeautomacao.sync.service.RomaneioService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

/**
 * Created by iomar on 21/11/18.
 */
public class RomaneioServiceImpl extends AbstractService implements RomaneioService {

    static Logger logger = LoggerFactory.getLogger(RomaneioServiceImpl.class);

    @Autowired
    RomaneioRepository romaneioRepository;

    @Override
    public BigDecimal gravarRomaneio(String sql) {
        return romaneioRepository.gravarRomaneio(sql);
    }
}
