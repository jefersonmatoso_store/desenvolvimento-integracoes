package br.com.storeautomacao.sync.service;

import br.com.storeautomacao.sync.dto.ItemDto;
import br.com.storeautomacao.sync.dto.ProdutoDto;
import br.com.storeautomacao.sync.model.entity.Produto;
import br.com.storeautomacao.sync.model.entity.ProdutoComponente;
import br.com.storeautomacao.sync.model.entity.TipoUc;
import br.com.storeautomacao.sync.repository.ProdutoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 *
 */
@Service
public class ProdutoServiceImpl extends AbstractService implements ProdutoService {

    @Autowired
    private ProdutoRepository produtoRepository;

    @Override
    public List<Produto> obterListaProdutoPorSequenciaIntegracao(String sequenciaIntegracao) {
        return produtoRepository.obterListaProdutoPorSequenciaIntegracao(sequenciaIntegracao);
    }

    @Override
    public TipoUc obterTipoUcPorCodigoProdutoESequenciaIntegracao(String sequenciaIntegracao, String codigoProduto) {
        return produtoRepository.obterTipoUcPorCodigoProdutoESequenciaIntegracao(sequenciaIntegracao, codigoProduto);
    }

    @Override
    public List<ProdutoComponente> obterProdutoComponentePorCodigoProdutoESequenciaIntegracao(String sequenciaIntegracao, String codigoProduto) {
        return produtoRepository.obterProdutosComponentesPorCodigoProdutoESequenciaIntegracao(sequenciaIntegracao, codigoProduto);
    }

    @Override
    public String cadastrarProduto(ProdutoDto produtoDto) {
        return produtoRepository.cadastrarProduto(produtoDto);
    }

    @Override
    public String cadastrarItem(ItemDto itemDto) {
        return produtoRepository.cadastrarItem(itemDto);
    }

    @Override
    public List<ProdutoDto> obterTodosProdutos() {
        return produtoRepository.obterTodosProdutos();
    }

}
