package br.com.storeautomacao.sync.service;

import br.com.storeautomacao.sync.model.entity.UploadImage;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by alexandre on 20/03/18.
 */
public abstract class AbstractService {

    @Value("${PATH.BASE}")
    private String PATH;

    @Value("${URL}")
    private String URL;

    protected void uploadImage(UploadImage entity) {
        if (entity != null && CollectionUtils.isNotEmpty(entity.getBase64Images())) {
            for (String base64Image : entity.getBase64Images()) {
                if (StringUtils.isNotEmpty(base64Image)) {
                    try {

                        byte[] imageBytes = javax.xml.bind.DatatypeConverter.parseBase64Binary(base64Image);
                        String fileName = "img_" + System.currentTimeMillis();
                        File diretorio = new File(PATH + entity.getId());
                        if (!diretorio.exists()) {
                            diretorio.mkdir();
                        }

                        FileOutputStream out = new FileOutputStream(PATH + diretorio.getName() + File.separator + fileName + ".png");
                        out.write(imageBytes);
                        out.close();

                        entity.addUrlImages(URL + entity.getId() + File.separator + fileName + ".png");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            entity.cleanBase64Imagens();
        }
    }

}
