package br.com.storeautomacao.sync.service;

import org.springframework.stereotype.Service;

import java.math.BigDecimal;

/**
 *
 */
public interface RomaneioService {
    BigDecimal gravarRomaneio(String sql);
}
