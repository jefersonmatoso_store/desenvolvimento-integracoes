package br.com.storeautomacao.sync.service;

import br.com.storeautomacao.sync.model.entity.Mail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.internet.MimeMessage;

/**
 * Created by alexandre on 13/09/17.
 */
@Service
public class MailService {

    static Logger logger = LoggerFactory.getLogger(MailService.class);

    @Autowired
    private JavaMailSender javaMailSender;

    public boolean sendMail(Mail message) {
        try {
            MimeMessage mail = javaMailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(mail, true);
            helper.setTo(message.getTo());
            helper.setSubject(message.getSubject());
            helper.setText(message.getBody().toString() , true);
            javaMailSender.send(mail);
            return true;
        } catch (Exception e) {
            logger.error("Erro sender envio email: " + e.getCause());
        }
        return false;
    }
}
