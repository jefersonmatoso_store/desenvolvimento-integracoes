package br.com.storeautomacao.sync.service;

import br.com.storeautomacao.sync.model.entity.Mercadoria;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by eduardo
 */
@Service
public interface MercadoriaService {

    List<Mercadoria> listaTodasMercadorias();

}
