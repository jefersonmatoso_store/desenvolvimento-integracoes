package br.com.storeautomacao.sync.service;

import br.com.storeautomacao.sync.model.entity.DocumentoDetalheSequencia;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

/**
 *
 */
@Service
public interface DocumentoDetalheSequenciaService {
    BigDecimal gravarDocumentoDetalheSequencia(String sql);
    List<DocumentoDetalheSequencia> obterDocumentoDetalheSequenciaPorSequenciaIntegracao(String sequenciaIntegracao);
}
