package br.com.storeautomacao.sync.service.impl;

import br.com.storeautomacao.sync.model.entity.DocumentoDetalheSequencia;
import br.com.storeautomacao.sync.repository.DocumentoDetalheSequenciaRepository;
import br.com.storeautomacao.sync.service.AbstractService;
import br.com.storeautomacao.sync.service.DocumentoDetalheSequenciaService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by iomar on 21/11/18.
 */
@Service
public class DocumentoDetalheSequenciaServiceImpl extends AbstractService implements DocumentoDetalheSequenciaService {

    static Logger logger = LoggerFactory.getLogger(DocumentoDetalheSequenciaServiceImpl.class);

    @Autowired
    DocumentoDetalheSequenciaRepository documentoDetalheSequenciaRepository;

    @Override
    public BigDecimal gravarDocumentoDetalheSequencia(String sql) {
        return documentoDetalheSequenciaRepository.gravarDocumentoDetalheSequencia(sql);
    }

    @Override
    public List<DocumentoDetalheSequencia> obterDocumentoDetalheSequenciaPorSequenciaIntegracao(String sequenciaIntegracao) {
        return documentoDetalheSequenciaRepository.obterDocumentoDetalheSequenciaPorSequenciaIntegracao(sequenciaIntegracao);
    }

}
