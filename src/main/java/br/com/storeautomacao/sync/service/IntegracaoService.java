package br.com.storeautomacao.sync.service;

import br.com.storeautomacao.sync.dto.ItemDto;
import br.com.storeautomacao.sync.dto.SaleDto;
import br.com.storeautomacao.sync.model.entity.Embalagem;
import br.com.storeautomacao.sync.model.entity.Integracao;
import br.com.storeautomacao.sync.model.entity.Sistema;
import br.com.storeautomacao.sync.model.wms.ConfirmacaoConferencia;
import br.com.storeautomacao.sync.model.wms.Shipment.LgfDataShipped;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by eduardo on 13/08/18.
 */
@Service
public interface IntegracaoService {


    Sistema test();

    List<Integracao> entradaMercadoria();

    String conferenciaPedido(ConfirmacaoConferencia confirmacaoConferencia);

    boolean finalizarCompra(SaleDto saleDto);

    List<Integracao> saidaMercadoria();

    String pedidoCompraPOCDrogariaSaoPaulo(String xmlPO);

    String conferenciaPedidoDrogariaSaoPaulo(ConfirmacaoConferencia confirmacaoConferencia);

    String salvarEmbalagem(String shipped);

    List<Embalagem> obterEmbalagens(String code);

    List<Integracao> listaIntegracaoLogix();

    String shipload252(String strXmlShipped);

}
