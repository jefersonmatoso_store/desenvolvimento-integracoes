package br.com.storeautomacao.sync.service;

import br.com.storeautomacao.sync.model.entity.Person;
import br.com.storeautomacao.sync.model.entity.Profile;
import br.com.storeautomacao.sync.model.entity.ProfileType;
import br.com.storeautomacao.sync.repository.ProfileRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;

/**
 *
 */
@Service
public class ProfileService {

    @Autowired
    private ProfileRepository repository;

    @Autowired
    private PersonService personService;


    public String create(Profile profile) {
        this.updatePerson(profile);
        return repository.create(profile);
    }

    public List<Profile> findAll() {
        return repository.findAll();
    }


    public List<Profile> findByPerson(Collection<Person> persons) { return repository.findByPerson(persons); }

    public List<Profile> findByType(ProfileType type) { return repository.findByType(type); }

    public Profile findById(String id) { return repository.findById(id); }

    public Profile findByIdPerson(String idPerson) { return repository.findByIdPerson(idPerson); }

    public String update(Profile profile) {
        this.updatePerson(profile);
        return repository.create(profile); }

    public void remove(String id) { repository.remove(findById(id)); }

    public void remove(Profile profile) {
        this.removePerson(profile);
        repository.remove(profile);
    }

    private void updatePerson(Profile profile) {
        if (profile != null && profile.getPerson() != null) {
            Person person = profile.getPerson();
            if (StringUtils.isEmpty(person.getId())) {
                personService.create(person);
            } else {
               // personService.update(person);
            }
        }
    }

    private void removePerson(Profile profile) {
        if (profile != null && profile.getPerson() != null) {
            //personService.remove(profile.getPerson());
        }
    }

}
