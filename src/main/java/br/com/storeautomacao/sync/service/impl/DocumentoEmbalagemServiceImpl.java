package br.com.storeautomacao.sync.service.impl;

import br.com.storeautomacao.sync.repository.DocumentoEmbalagemRepository;
import br.com.storeautomacao.sync.service.AbstractService;
import br.com.storeautomacao.sync.service.DocumentoEmbalagemService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

/**
 * Created by iomar on 21/11/18.
 */
public class DocumentoEmbalagemServiceImpl extends AbstractService implements DocumentoEmbalagemService {

    static Logger logger = LoggerFactory.getLogger(DocumentoEmbalagemServiceImpl.class);

    @Autowired
    DocumentoEmbalagemRepository documentoEmbalagemRepository;

    @Override
    public BigDecimal gravarDocumentoEmbalagem(String sql) {
        return documentoEmbalagemRepository.gravarDocumentoEmbalagem(sql);
    }
}
