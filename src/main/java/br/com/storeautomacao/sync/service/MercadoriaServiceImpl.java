package br.com.storeautomacao.sync.service;

import br.com.storeautomacao.sync.model.entity.Mercadoria;
import br.com.storeautomacao.sync.repository.IntegracaoRepository;
import br.com.storeautomacao.sync.repository.MercadoriaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by eduardo
 */
@Service
public class MercadoriaServiceImpl extends AbstractService implements MercadoriaService {

    @Autowired
    private IntegracaoRepository repository;

    @Autowired
    MercadoriaRepository mercadoriaRepository;



    @Override
    public List<Mercadoria> listaTodasMercadorias() {
        return mercadoriaRepository.obterListaEntradaMercadoria();
    }
}
