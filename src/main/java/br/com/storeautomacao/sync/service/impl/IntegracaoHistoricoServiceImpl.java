package br.com.storeautomacao.sync.service.impl;

import br.com.storeautomacao.sync.repository.IntegracaoHistoricoRepository;
import br.com.storeautomacao.sync.service.AbstractService;
import br.com.storeautomacao.sync.service.IntegracaoHistoricoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

/**
 * Created by iomar on 21/11/18.
 */
public class IntegracaoHistoricoServiceImpl extends AbstractService implements IntegracaoHistoricoService {

    static Logger logger = LoggerFactory.getLogger(IntegracaoHistoricoServiceImpl.class);

    @Autowired
    IntegracaoHistoricoRepository integracaoHistoricoRepository;

    @Override
    public BigDecimal gravarIntegracaoHistorico(String sql) {
        return integracaoHistoricoRepository.gravarIntegracaoHistorico(sql);
    }

}