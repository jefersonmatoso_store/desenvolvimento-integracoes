package br.com.storeautomacao.sync.service;

import br.com.storeautomacao.sync.model.entity.Address;
import br.com.storeautomacao.sync.model.entity.Person;
import br.com.storeautomacao.sync.repository.PersonRepository;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by alexandre on 30/07/17.
 */
@Service
public class PersonService extends AbstractService {

    @Autowired
    private PersonRepository repository;

    @Autowired
    private AddressService addressService;



    public String create(Person person) {

        // Persistindo os objetos relacionados:
        // FIXME: Incluir o algoritmo de geracao do hash de reconhecimento facial.
//        person.setHashFacialRecognition();

        String id = repository.create(person);
        if (StringUtils.isNotEmpty(id)) {
            this.uploadImage(person);
            repository.update(person);
        }


        return id;
    }

    public List<Person> findAll() { return repository.findAll(); }

    public Person findById(String id) { return repository.findById(id); }

    public List<Person> findByFirstName(String firstName) { return repository.findByFirstName(firstName); }

    public List<Person> findByLastName(String lastName) { return repository.findByLastName(lastName); }

    public List<Person> findByNamePiece(String namePiece) { return repository.findByNamePiece(namePiece); }

    public List<Person> findByEmail(String email) { return repository.findByEmail(email); }

    public Person findByFacialRecognition(String facialRecognition) { return repository.findByFacialRecognition(facialRecognition); }



    /** Atualização simples da <b>Pessoa</b>, sem tratar os relacionamentos*/
    public String simpleUpdate(Person person) { return repository.update(person); }





    private void updateAddress(Person person) {
        if (person != null && CollectionUtils.isNotEmpty(person.getAddresses())) {
            for (Address address : person.getAddresses() ) {
                addressService.create(address);
            }
        }
    }


    private void removeAddress(Person person) {
        if (person != null && CollectionUtils.isNotEmpty(person.getAddresses())) {
            if (person != null && CollectionUtils.isNotEmpty(person.getAddresses())) {
                for (Address address : person.getAddresses() ) {
                    if (StringUtils.isNotEmpty(address.getId())) {
                        addressService.remove(address);
                    }
                }
            }
        }
    }


}
