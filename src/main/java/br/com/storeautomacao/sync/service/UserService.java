package br.com.storeautomacao.sync.service;

import br.com.storeautomacao.sync.dto.UserDTO;
import br.com.storeautomacao.sync.exception.ServiceException;
import br.com.storeautomacao.sync.model.entity.*;
import br.com.storeautomacao.sync.repository.PersonRepository;
import br.com.storeautomacao.sync.repository.UserRepository;
import br.com.storeautomacao.sync.security.service.JsonWebTokenService;
import br.com.storeautomacao.sync.util.SecurityUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ProfileService profileService;

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private JsonWebTokenService jsonWebTokenService;

    @Autowired
    private MailService mailService;

    @Async
    public String create(User user) {
        profileService.create(user.getProfile());
        return userRepository.create(user);

    }

    @Async
    public String register(User user, String idPerson) {
        String idUser = null;
        Person person = personRepository.findById(idPerson);
        if (person != null) {
            Profile profile = new Profile();
            profile.setPerson(person);
            profile.setPicture(person.getPhoto());
            profile.setProfileType(ProfileType.INQUILINO);
            profile.setName(person.getFullname());
            profileService.create(profile);

            if (StringUtils.isNotEmpty(profile.getId())) {
                user.setProfile(profile);
                idUser = create(user);
            }

            if (StringUtils.isNotEmpty(idUser)) {
                //invitationService.confirmNewUserInvitation(person);
            }
        }

        return idUser;
    }

    @Async
    public String update(User user) {
        profileService.create(user.getProfile());
        return userRepository.update(user);
    }

    @Async
    public boolean updatePasswordFields(UserDTO dto, User user) {
        if (dto == null || StringUtils.isEmpty(dto.getUserId())) {
            throw new ServiceException("Usuário inválido");
        }

        boolean isUpdated = false;
        if (StringUtils.isNotEmpty(dto.getNewPassword())) {
            if (!StringUtils.equals(dto.getNewPassword(), dto.getConfirmNewPassword())) {
                throw new ServiceException("A nova senha foi digitada incorretamente");
            }

            User currentUser = findByIdUser(user.getId());
            if (!StringUtils.equals(
                    SecurityUtil.encript(dto.getCurrentPassword()),
                    user.getPassword() )) {
                throw new ServiceException("A Senha atual foi digitada incorretamente!");
            }

            User userUpdated = userRepository.updatePassword(
                    dto.getUserId(),
                    SecurityUtil.encript(dto.getNewPassword()));
            if (userUpdated == null) {
                throw new ServiceException("Usuário inexistente!");
            }

            isUpdated = true;
        }

        if (StringUtils.isNotEmpty(dto.getCheckinPassword())) {
            if (!StringUtils.equals(dto.getCheckinPassword(), dto.getConfirmCheckinPassword())) {
                throw new ServiceException("A nova senha foi digitada incorretamente");
            }

            User userUpdated = userRepository.updateCheckinPassword(
                    dto.getUserId(),
                    SecurityUtil.encript(dto.getCheckinPassword()));
            if (userUpdated == null) {
                throw new ServiceException("Usuário inexistente!");
            }

            isUpdated = true;
        }

        return isUpdated;
    }

    public User findByIdUser(String idUser) {
        return userRepository.findById(idUser);
    }

    public User findByIdProfile(String idProfile) {
        return userRepository.findByIdProfile(idProfile);
    }

    public List<User> findAll() {
        return userRepository.findAll();
    }

    public List<User> findAll(Collection<Person> persons) {
        List<Profile> profiles = profileService.findByPerson(persons);
        return userRepository.findAll(profiles);
    }

    public boolean deleteById(String idUser) {
        try {
            return userRepository.deleteById(idUser);
        } catch (Exception ex) {
            return false;
        }
    }

    @Async
    public User validLogin(User user) {

        User userLogged = userRepository.findUserByUsernameAndPassword(user.getUserName(), user.getPassword());
        if (userLogged != null && StringUtils.isNotEmpty(userLogged.getId())) {
            String token =  jsonWebTokenService.getToken(user.getUserName(), user.getPassword());
            userLogged.setAuthenticationToken(token);
        }

        return userLogged;

    }

    public User findUserByUserIdAndCheckinPassword(String userId, String checkinPassword) {
        return userRepository.findUserByUserIdAndCheckinPassword(userId, checkinPassword);
    }

    public User findByUsername(String username) { return userRepository.findByUsername(username); }

    public List<User> findAllVigilants() {
        List<Profile> profiles = profileService.findByType(ProfileType.VIGILANT);
        return userRepository.findAll(profiles);
    }

    @Async
    public boolean sendEmail(String idPerson) {

        try {
            Person person = personRepository.findById(idPerson);

            Profile profile = profileService.findByIdPerson(person.getId());

            if (profile == null){
                // convite para criar usuario
               // NewUserInvitation invitation = new NewUserInvitation();
                //invitation.setPerson(person);
                //String id = invitationService.createNewUserInvitation(invitation);

                //if (StringUtils.isNotEmpty(id)) {
                   // return true;
               // }

            } else {

                User user = findByIdProfile(profile.getId());

                if (StringUtils.isNotEmpty(person.getId())) {
                    Mail mail = new Mail();
                    mail.setSubject("Solicitção de Usuário e Senha");
                    mail.setBody(buildBodyEmail(user));
                    mail.setTo(person.getEmail());

                    return mailService.sendMail(mail);
                }

                return true;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    private StringBuffer buildBodyEmail(User user) {

        String newPassword = SecurityUtil.getRandomPassword(8);

        String password = newPassword;

        userRepository.updatePassword(user.getId(), SecurityUtil.encript(newPassword));

        Person person = user.getProfile().getPerson();
        StringBuffer sb = new StringBuffer();
        sb.append("<img src=\"http://condominio.nicbrain.cloud/img/logo.png\"><br />");
        sb.append("<div style=\"color:#1f487d;width:630px;font-family:Calibri;font-size:15px;line-height:22px;text-align: justify;margin-top:20px;\" >");

        sb.append("Caro(a) " + person.getFullname() + ", <br /><br />");

        sb.append("Seguem seus dados de acesso ao sistema Nicbrain Condominio. <br />");

        sb.append("Link: http://condominio.nicbrain.cloud/#/pages/login<br /><br /><br />");
        sb.append("Login: " + user.getUserName() + "<br />");
        sb.append("Senha: " + password + "<br /><br />");

        sb.append("Atenciosamente, <br /><br />");

        sb.append("Nicbrain<br />");
        sb.append("</div>");

        return sb;
    }

}
