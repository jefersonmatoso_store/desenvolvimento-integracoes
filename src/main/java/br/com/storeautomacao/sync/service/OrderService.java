package br.com.storeautomacao.sync.service;

import br.com.storeautomacao.sync.dto.PushMessageDTO;
import br.com.storeautomacao.sync.dto.RequestOrderPhotoDTO;
import br.com.storeautomacao.sync.model.entity.Order;
import br.com.storeautomacao.sync.model.enuns.TypePushEnum;
import br.com.storeautomacao.sync.repository.OrderRepository;
import com.google.gson.Gson;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

/**
 *
 */
@Service
public class OrderService extends AbstractService {

    @Autowired
    private OrderRepository repository;

    @Autowired
    private GcmMessageServerService mensageriaService;

    @Async
    public String create(Order order) {
        Gson gson = new Gson();
        String jsonInString = gson.toJson(order);
        String retorno = repository.create(order);
        if (StringUtils.isNotEmpty(retorno)) {
            this.update(order);

            // TODO REFACTORING:  Transformar o PushMessageDTO em Interface para remover o uso o if (e tornar outros objetos concretos aptos a disparar notificações
            if (StringUtils.isNotEmpty(order.getDeviceTokenRecipient())) {
                PushMessageDTO pushMessageDTO = new PushMessageDTO();
                pushMessageDTO.setTitle("Recebimento de encomenda");
                pushMessageDTO.setSubtitle(order.getNameObject());
                pushMessageDTO.setMessage("Você recebeu uma encomenda, favor retirar na administração");
                pushMessageDTO.setStringObject(jsonInString);
                pushMessageDTO.setType(TypePushEnum.ORDER);
                mensageriaService.sendMessageDTO(pushMessageDTO, order.getDeviceTokenRecipient());
            }
        }

        return retorno;
    }

    public List<Order> findAll() { return repository.listAll(); }

    public Order findById(String id) { return repository.findById(id); }

    public List<Order> findByName(String name) {
        return repository.findByName(name);
    }

    @Async
    public String update(Order order) {
        uploadImage(order);
        return repository.update(order);
    }

    @Async
    public void remove(String id) { repository.remove(id); }

    @Async
    public void remove(Order order) {
        repository.remove(order);
    }

    @Async
    public boolean uploadPhoto(RequestOrderPhotoDTO dto) throws Exception {
        boolean result = false;
        if (dto != null && StringUtils.isNotEmpty(dto.getIdOrder())
                && StringUtils.isNotEmpty(dto.getBase64String())) {

            Order order = repository.findById(dto.getIdOrder());
            order.addBase64Image(dto.getBase64String());

            uploadImage(order);
            repository.update(order);

            result = true;
        }

        return result;
    }

    public boolean deleteAll() {
        return repository.deleteAll();
    };

    public boolean pickUpOrder(String id){
        Order order = repository.pickUpOrder(id);
        return order != null && order.getTakenDate() != null;
    }

    public List<Order> findBy(String nameRecipient, LocalDate ldStart, LocalDate ldEnd) {
        return repository.findBy(nameRecipient, ldStart, ldEnd);
    }

    public List<Order> findByIdUserLogado(String idUsuario) {
        return repository.findByIdUserLogado(idUsuario) ;
    }

}
