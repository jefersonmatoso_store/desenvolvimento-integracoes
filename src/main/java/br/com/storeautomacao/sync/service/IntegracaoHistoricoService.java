package br.com.storeautomacao.sync.service;

import org.springframework.stereotype.Service;

import java.math.BigDecimal;

/**
 * Created by iomar on 21/11/18.
 */
public interface IntegracaoHistoricoService {
    BigDecimal gravarIntegracaoHistorico(String sql);
}