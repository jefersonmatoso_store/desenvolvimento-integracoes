package br.com.storeautomacao.sync.connector;

import br.com.storeautomacao.sync.util.XmlUtil;
import br.com.storeautomacao.sync.wsdl.otm.*;
import com.thoughtworks.xstream.XStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBIntrospector;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

@Service
public class SoapConnectorImpl implements SoapConnector{

    static Logger logger = LoggerFactory.getLogger(SoapConnector.class);

    private static String URI = "otmgtm-test-a572805.otm.us2.oraclecloud.com/";
    private static String SERVICE_PATH = "GC3Services/TransmissionService";

    @Override
    public String http_client() throws Exception {



        System.out.println("Invoke service using direct HTTP call with Basic Auth");
        String payload =
                "<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\n" +
                        "    <soap:Header>\n" +
                        "        <fmw-context xmlns=\"http://xmlns.oracle.com/fmw/context/1.0\"/>\n" +
                        "    </soap:Header>\n" +
                        "    <soap:Body>\n" +
                        "        <ns1:findExpense xmlns:ns1=\"http://xmlns.oracle.com/apps/financials/expenses/shared/common/expenseExternalService/types/\">\n" +
                        "            <ns1:findCriteria xmlns:ns2=\"http://xmlns.oracle.com/adf/svc/types/\">\n" +
                        "                <ns2:fetchStart>0</ns2:fetchStart>\n" +
                        "                <ns2:fetchSize>2</ns2:fetchSize>\n" +
                        "                <ns2:excludeAttribute>true</ns2:excludeAttribute>\n" +
                        "            </ns1:findCriteria>\n" +
                        "            <ns1:findControl xmlns:ns2=\"http://xmlns.oracle.com/adf/svc/types/\">\n" +
                        "                <ns2:retrieveAllTranslations>true</ns2:retrieveAllTranslations>\n" +
                        "            </ns1:findControl>\n" +
                        "        </ns1:findExpense>\n" +
                        "    </soap:Body>\n" +
                        "</soap:Envelope>";



//        httpPost("https://" + URI + SERVICE_PATH + "", teste(),
//                "GAVE.INTEGRATION:intgav2018");

        String xmlOrderRelease = XmlUtil.getTransmissionHeader(XmlUtil.getOrderReleaseOTM());
        String xmlLocation = XmlUtil.getTransmissionHeader(XmlUtil.getXmlLocation());


        return httpPost("https://otmgtm-test-a572805.otm.us2.oraclecloud.com/GC3/glog.integration.servlet.WMServlet", xmlLocation,
                "GAVE.INTEGRATION:intgav2018");
    }

    @Override
    public String httpPost(String destUrl, String postData, String authStr) throws Exception {
        URL url = new URL(destUrl);
        HttpURLConnection conn = (HttpURLConnection)url.openConnection();
        if (conn == null) {
            return null;
        }
        conn.setRequestProperty("Content-Type", "text/xml; charset=UTF-8");
        conn.setDoOutput(true);
        conn.setDoInput(true);
        conn.setUseCaches(false);
        conn.setFollowRedirects(true);
        conn.setAllowUserInteraction(false);
        conn.setRequestMethod("POST");

        byte[] authBytes = authStr.getBytes("UTF-8");
        String auth = com.sun.org.apache.xml.internal.security.utils.Base64.encode(authBytes);
        conn.setRequestProperty("Authorization", "Basic " + auth);


        System.out.println("post data size:" + postData.length());


        OutputStream out = conn.getOutputStream();
        OutputStreamWriter writer = new OutputStreamWriter(out, "UTF-8");
        writer.write(postData);
        writer.close();
        out.close();

        System.out.println("connection status: " + conn.getResponseCode() +
                "; connection response: " +
                conn.getResponseMessage());

        InputStream in = conn.getInputStream();
        InputStreamReader iReader = new InputStreamReader(in);
        BufferedReader bReader = new BufferedReader(iReader);

        String line;
        String response = "";
        System.out.println("==================Service response: ================ ");
        while ((line = bReader.readLine()) != null) {
            System.out.println(line);
            response += line;
        }
        iReader.close();
        bReader.close();
        in.close();
        conn.disconnect();


        return response;
    }



    @Override
    public String sendXml(String destUrl, String postData, String authStr) throws Exception {


        try {


            TransmissionService transmissionService = new TransmissionService();
            Transmission transmission = new Transmission();

            TransmissionHeaderType header = new TransmissionHeaderType();
            header.setUserName("GAVE.INTEGRATION");
            PasswordType pass = new PasswordType();
            pass.setValue("intgav2018");
            header.setPassword(pass);
            transmission.setTransmissionHeader(header);

            List<GLogXMLElementType> gLogXMLElement = new ArrayList<>();

            String xml = XmlUtil.getOrderReleaseOTM();
            XStream xstream = new XStream();
            xstream.processAnnotations(ReleaseType.class);
           // ReleaseType xmlOrderRelease = (ReleaseType) xstream.fromXML(xml);

            GLogXMLElementType gLogXMLElementType = new GLogXMLElementType();


            /**
             * JAXB
             */
            JAXBContext jc = JAXBContext.newInstance(ReleaseType.class);
            Unmarshaller unmarshaller = jc.createUnmarshaller();
            JAXBIntrospector jaxbIntrospector = jc.createJAXBIntrospector();

            Object object = unmarshaller.unmarshal(new StringReader(xml));
            System.out.println(object.getClass());
            System.out.println(jaxbIntrospector.getValue(object).getClass());

            JAXBElement<ReleaseType> jaxbElement = unmarshaller.unmarshal(new StreamSource(new StringReader(xml)), ReleaseType.class);
            /**
             * JAXB
             */


            gLogXMLElementType.setGLogXMLTransaction(jaxbElement);
            // gLogXMLElement.add(gLogXMLElementType);


            Transmission.TransmissionBody transmissionBody = new Transmission.TransmissionBody();
            transmissionBody.getGLogXMLElement().add(gLogXMLElementType);


            transmission.setTransmissionBody(transmissionBody);

            xstream.processAnnotations(Transmission.class);
            xstream.processAnnotations(Transmission.TransmissionBody.class);
            xstream.processAnnotations(GLogXMLElementType.class);
            xstream.processAnnotations(ReleaseType.class);
            xstream.processAnnotations(StatusType.class);
            xstream.processAnnotations(ReleaseLineType.class);
            xstream.processAnnotations(ShipUnitType.class);
            xstream.processAnnotations(ShipUnitContentType.class);
            xstream.processAnnotations(ReleaseRefnumType.class);
            String xmlConverted = xstream.toXML(transmission);

            return httpPost("https://otmgtm-test-a572805.otm.us2.oraclecloud.com/GC3/glog.integration.servlet.WMServlet", xmlConverted,
                    "GAVE.INTEGRATION:intgav2018");

            //TransmissionPortType transmissionPortType = transmissionService.getTransmission();

            //transmissionPortType.publish(transmission);

        } catch (Exception ex){
            logger.error(ex.getMessage());
        }





        //service = new GlbQryDetAnalisePortfV2QueryService();
        //port = service.getGlbQryDetAnalisePortfV2QueryService();
        //sessionId = port.login(login);

        return null;
    }


    private String teste(){

        String str = "<?xml version=\"1.0\"?>" +
                "<otm:Location xmlns:gtm=\"http://xmlns.oracle.com/apps/gtm/transmission/v6.4\" xmlns:otm=\"http://xmlns.oracle.com/apps/otm/transmission/v6.4\">" +
                "<otm:SendReason>" +
                "<otm:Remark>" +
                "<otm:RemarkSequence>1</otm:RemarkSequence>" +
                "<otm:RemarkQualifierGid>" +
                "<otm:Gid>" +
                "<otm:Xid>QUERY TYPE</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:RemarkQualifierGid>" +
                "<otm:RemarkText>LOCATION</otm:RemarkText>" +
                "</otm:Remark>" +
                "<otm:SendReasonGid>" +
                "<otm:Gid>" +
                "<otm:Xid>SEND INTEGRATION</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:SendReasonGid>" +
                "<otm:ObjectType>LOCATION</otm:ObjectType>" +
                "</otm:SendReason>" +
                "<otm:TransactionCode>NP</otm:TransactionCode>" +
                "<otm:LocationGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>100</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:LocationGid>" +
                "<otm:LocationName>100AVENIDA</otm:LocationName>" +
                "<otm:IsTemplate>N</otm:IsTemplate>" +
                "<otm:Address>" +
                "<otm:City>CAMPO GRANDE</otm:City>" +
                "<otm:Province>MATO GROSSO DO SUL</otm:Province>" +
                "<otm:ProvinceCode>MS</otm:ProvinceCode>" +
                "<otm:PostalCode>79117-010</otm:PostalCode>" +
                "<otm:CountryCode3Gid>" +
                "<otm:Gid>" +
                "<otm:Xid>BR</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:CountryCode3Gid>" +
                "<otm:CountryCode>" +
                "<otm:CountryCode3Gid>" +
                "<otm:Gid>" +
                "<otm:Xid>BR</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:CountryCode3Gid>" +
                "</otm:CountryCode>" +
                "<otm:TimeZoneGid>" +
                "<otm:Gid>" +
                "<otm:Xid>Brazil/East</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:TimeZoneGid>" +
                "<otm:Latitude>-20.43482</otm:Latitude>" +
                "<otm:Longitude>-54.62852</otm:Longitude>" +
                "</otm:Address>" +
                "<otm:IsTemporary>N</otm:IsTemporary>" +
                "<otm:LocationRole>" +
                "<otm:LocationRoleGid>" +
                "<otm:Gid>" +
                "<otm:Xid>SHIPFROM/SHIPTO</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:LocationRoleGid>" +
                "<otm:XDockIsInboundBias>N</otm:XDockIsInboundBias>" +
                "<otm:CreatePoolHandlingShipment>N</otm:CreatePoolHandlingShipment>" +
                "<otm:CreateXDockHandlingShipment>N</otm:CreateXDockHandlingShipment>" +
                "<otm:IsMixedFreightTHUAllowed>N</otm:IsMixedFreightTHUAllowed>" +
                "</otm:LocationRole>" +
                "<otm:IsMakeAppointmentBeforePlan>N</otm:IsMakeAppointmentBeforePlan>" +
                "<otm:IsShipperKnown>N</otm:IsShipperKnown>" +
                "<otm:IsAddressValid>U</otm:IsAddressValid>" +
                "<otm:IsLTLSplitable>Y</otm:IsLTLSplitable>" +
                "<otm:ApptObjectType>S</otm:ApptObjectType>" +
                "<otm:ExcludeFromRouteExec>N</otm:ExcludeFromRouteExec>" +
                "<otm:UseApptPriority>N</otm:UseApptPriority>" +
                "<otm:SchedLowPriorityAppt>N</otm:SchedLowPriorityAppt>" +
                "<otm:EnforceTimeWindowAppt>Y</otm:EnforceTimeWindowAppt>" +
                "<otm:SchedInfeasibleAppt>Y</otm:SchedInfeasibleAppt>" +
                "<otm:AllowDriverRest>Y</otm:AllowDriverRest>" +
                "<otm:IsFixedAddress>N</otm:IsFixedAddress>" +
                "<otm:PrimaryAddressLineSeq>1</otm:PrimaryAddressLineSeq>" +
                "<otm:FlexFieldStrings/>" +
                "<otm:FlexFieldNumbers/>" +
                "<otm:FlexFieldDates/>" +
                "<otm:IsActive>Y</otm:IsActive>" +
                "</otm:Location>";

        return str;
    }
}
