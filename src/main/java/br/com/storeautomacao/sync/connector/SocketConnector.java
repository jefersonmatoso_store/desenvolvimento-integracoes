package br.com.storeautomacao.sync.connector;

/**
 * Created by eduardoabreu on 29/01/17.
 */
public interface SocketConnector {

    void connectorSocketIo(String type)
            throws Exception;

}
