package br.com.storeautomacao.sync.connector;

public interface SoapConnector {

    String http_client() throws Exception;

    String httpPost(String destUrl, String postData,
                    String authStr) throws Exception;


    String sendXml(String destUrl, String postData,
                    String authStr) throws Exception;

}
