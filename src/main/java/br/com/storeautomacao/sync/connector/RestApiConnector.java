package br.com.storeautomacao.sync.connector;

import br.com.storeautomacao.sync.exception.StoreAutomacaoSyncServiceException;
import br.com.storeautomacao.sync.model.entity.*;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;

import java.util.List;


public interface RestApiConnector {

	Token login(final Credencial credencial);

	HttpEntity<Object> connectorPostHttp(Object objectDTO, final String uri)
			throws StoreAutomacaoSyncServiceException;

	HttpEntity<String> connectorGetHttp(Object objectDTO, final String uri)
			throws StoreAutomacaoSyncServiceException;

	List<MissingCustomerOrder> getMissingCustomerOrder(Token token);

	List<ListOrderItem> getCustomerListOrderItem(Token token, String idCustomerOrder);

	void deletaItem(List<Picked> listPicked, Token token);

	HttpEntity<TemperaturaResponse> connectorGetHttpTemp(Object objectDTO, final String uri)
			throws StoreAutomacaoSyncServiceException;

	ResponseEntity<String> connectorSendXmlWms(String strXml)
			throws StoreAutomacaoSyncServiceException;

    ResponseEntity<String> connectorSendWms(Embalagem embalagem)
            throws StoreAutomacaoSyncServiceException;

	ResponseEntity<String> connectorSendXmlOtm(String strXml) throws StoreAutomacaoSyncServiceException;
}
