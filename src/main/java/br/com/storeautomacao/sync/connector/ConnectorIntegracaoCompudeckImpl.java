package br.com.storeautomacao.sync.connector;

import br.com.storeautomacao.sync.exception.StoreAutomacaoSyncServiceException;
import br.com.storeautomacao.sync.model.entity.*;
import org.apache.tomcat.util.codec.binary.Base64;
import org.eclipse.paho.client.mqttv3.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

/**
 * Created by iomar on 04/12/18.
 */
@Service
public class ConnectorIntegracaoCompudeckImpl implements ConectorIntegracaoCompudeck  {

    static Logger logger = LoggerFactory.getLogger(ConnectorIntegracaoCompudeckImpl.class);

    private IMqttClient client;

    public ConnectorIntegracaoCompudeckImpl(){

        try {

            String publisherId = UUID.randomUUID().toString();
            client = new MqttClient("tcp://cloud.compudeck.com.br:1883", publisherId);

            MqttConnectOptions options = new MqttConnectOptions();
            options.setUserName("compudeck");
            options.setPassword("compudeck".toCharArray());
            client.connect(options);

        } catch (MqttException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void ligar() {
        try {
            client.publish("esteira", new MqttMessage("ligar".getBytes()));
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void desligar() {
        try {
            client.publish("esteira", new MqttMessage("desligar".getBytes()));
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }
}
