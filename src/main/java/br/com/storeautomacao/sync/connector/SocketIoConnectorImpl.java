package br.com.storeautomacao.sync.connector;

import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Created by eduardoabreu on 30/01/17.
 */
@Component
public class SocketIoConnectorImpl implements SocketConnector{

    private Socket socket;

    @Value("${URL.BASE.SOCKET}")
    private String URL_BASE_SOCKET;

    @Override
    public void connectorSocketIo(String type) throws Exception {
        socket = IO.socket(URL_BASE_SOCKET);
        socket.on(Socket.EVENT_CONNECT, new Emitter.Listener() {

            @Override
            public void call(Object... args) {
                socket.emit(type, "ok");
                socket.disconnect();
            }

        }).on("event", new Emitter.Listener() {

            @Override
            public void call(Object... args) {}

        }).on(Socket.EVENT_DISCONNECT, new Emitter.Listener() {

            @Override
            public void call(Object... args) {}

        });
        socket.connect();
    }
}
