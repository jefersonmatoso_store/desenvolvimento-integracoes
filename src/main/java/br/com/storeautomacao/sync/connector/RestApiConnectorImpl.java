package br.com.storeautomacao.sync.connector;

import br.com.storeautomacao.sync.exception.StoreAutomacaoSyncServiceException;
import br.com.storeautomacao.sync.model.entity.*;
import org.apache.tomcat.util.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;

/**
 * Created by eduardoabreu on 20/10/16.
 */
@Service
public class RestApiConnectorImpl implements RestApiConnector  {

    static Logger logger = LoggerFactory.getLogger(RestApiConnectorImpl.class);

    @Value("${URL.BASE.PUSH}")
    private String URL_BASE;

    public Token login(final Credencial credencial)  {

        logger.info("Login");
        HttpHeaders requestHeaders = new HttpHeaders();

        requestHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

        HttpEntity<?> requestEntity = new HttpEntity<Object>(requestHeaders);

        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        String url =  "http://storeskin.app.storeautomacao.com.br/skin-api/api/authentication/user";
        logger.info("URL: " + url);
        Token response = null;
        try {
            response = restTemplate.postForObject(url, credencial, Token.class);

        } catch (Exception e) {
            logger.info("Erro: " + e.getMessage());
            e.getMessage();

        }

        return response;
    }

    @Override
    public HttpEntity<Object> connectorPostHttp(Object objectDTO, final String uri)
            throws StoreAutomacaoSyncServiceException {

        try {

            logger.info("URL: " + uri);
            RestTemplate restTemplate = new RestTemplate();
            HttpHeaders headers = new HttpHeaders();
            headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
            //headers.set("Token", "nicbrain-supervisor-service");
            HttpEntity<Object> entity = new HttpEntity<Object>(objectDTO, headers);
            HttpEntity<Object> result = restTemplate.exchange(uri, HttpMethod.POST, entity, Object.class);

            return result;

        }catch (Exception ex){
            System.out.println("PROBLEMA ---- " + ex.getMessage());
            return null;
        }
    }




    @Override
    public HttpEntity<String> connectorGetHttp(Object objectDTO, final String uri)
            throws StoreAutomacaoSyncServiceException {

        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.set("Token", "nicbrain-supervisor-service");
        HttpEntity<Object> entity = new HttpEntity<Object>(objectDTO, headers);
        HttpEntity<String> result = restTemplate.exchange(uri, HttpMethod.GET, entity, String.class);

        return result;
    }

    @Override
    public List<MissingCustomerOrder> getMissingCustomerOrder(Token token) {

        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.set("Authorization", "Bearer "+token.getToken());
        HttpEntity<Object> responseEntity = new HttpEntity<Object>(headers);
        String uri =  "http://storeskin.app.storeautomacao.com.br/skin-api/api/pick-up/missing-customer-orders";

        ResponseEntity<List<MissingCustomerOrder>> response = restTemplate.exchange(
                uri,
                HttpMethod.GET,
                responseEntity,
                new ParameterizedTypeReference<List<MissingCustomerOrder>>(){});
        List<MissingCustomerOrder> missingCustomerOrders = response.getBody();

        return missingCustomerOrders;

    }

    @Override
    public List<ListOrderItem> getCustomerListOrderItem(Token token, String idCustomerOrder) {
        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.set("Authorization", "Bearer "+token.getToken());
        HttpEntity<Object> responseEntity = new HttpEntity<Object>(headers);
        String uri =  "http://storeskin.app.storeautomacao.com.br/skin-api/api/pick-up/customerOrder/"+idCustomerOrder+"/items";

        ResponseEntity<List<ListOrderItem>> response = restTemplate.exchange(
                uri,
                HttpMethod.GET,
                responseEntity,
                new ParameterizedTypeReference<List<ListOrderItem>>(){});
        List<ListOrderItem> listOrderItem = response.getBody();

        return listOrderItem;
    }

    @Override
    public void deletaItem(List<Picked> listPicked, Token token) {

        try {

            String uri = "http://storeskin.app.storeautomacao.com.br/skin-api/api/picked";
            RestTemplate restTemplate = new RestTemplate();
            HttpHeaders headers = new HttpHeaders();
            headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.set("Authorization", "Bearer "+token.getToken());
            HttpEntity<Object> entity = new HttpEntity<Object>(listPicked, headers);
            //HttpEntity<Object> result = restTemplate.exchange(uri, HttpMethod.POST, entity, Object.class);

            //restTemplate.postForObject(uri, entity, Token.class);

            ResponseEntity<Void> response = restTemplate.postForEntity(uri, entity ,Void.class);
            System.out.println(response);

        }catch (Exception ex){
            System.out.println("PROBLEMA ---- " + ex.getMessage());

        }
    }

    @Override
    public HttpEntity<TemperaturaResponse> connectorGetHttpTemp(Object objectDTO, String macAdress) throws StoreAutomacaoSyncServiceException {

        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.set("Token", "nicbrain-supervisor-service");
        String uri = "http://52.32.208.65:8001/api/devices/" + macAdress + "/abstraction_data";
        HttpEntity<Object> entity = new HttpEntity<Object>(headers);
        HttpEntity<TemperaturaResponse> result = restTemplate.exchange(uri, HttpMethod.GET, entity, TemperaturaResponse.class);

        return result;
    }

    @Override
    public ResponseEntity<String> connectorSendXmlWms(String strXml) throws StoreAutomacaoSyncServiceException {

        RestTemplate restTemplate = new RestTemplate();

        //String plainCreds = "wmsapi:Storeautomacao2018"; //GAV hml
        String plainCreds = "GAVINTAPI:GAV@2018"; // prd
        byte[] plainCredsBytes = plainCreds.getBytes();
        byte[] base64CredsBytes = Base64.encodeBase64(plainCredsBytes);
        String base64Creds = new String(base64CredsBytes);

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Basic " + base64Creds);
        //String url = "https://ta1.wms.ocs.oraclecloud.com/grupoavenida_test/wms/api/init_stage_interface/"; //hml
        String url = "https://a25.logfireapps.com/grupoavenida/wms/api/init_stage_interface/"; //prd

        MultiValueMap<String, String> map= new LinkedMultiValueMap<String, String>();
        map.add("xml_data", strXml);

        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);

        //HttpEntity<String> request = new HttpEntity<String>(xmlString, headers);
        ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.POST, request, String.class);
        String str = response.getBody();

        return response;
    }

    @Override
    public ResponseEntity<String> connectorSendXmlOtm(String strXml) throws StoreAutomacaoSyncServiceException {

        RestTemplate restTemplate = new RestTemplate();
        String plainCreds = "GAVE.INTEGRATION:intgav2018";
        byte[] plainCredsBytes = plainCreds.getBytes();
        byte[] base64CredsBytes = Base64.encodeBase64(plainCredsBytes);
        String base64Creds = new String(base64CredsBytes);

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Basic " + base64Creds);
        String url = "https://otmgtm-test-a572805.otm.us2.oraclecloud.com/GC3/glog.integration.servlet.WMServlet";

        MultiValueMap<String, String> map= new LinkedMultiValueMap<String, String>();
        map.add("xml_data", strXml);

        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);

        //HttpEntity<String> request = new HttpEntity<String>(xmlString, headers);
        ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.POST, request, String.class);
        String str = response.getBody();

        return response;
    }

    @Override
    public ResponseEntity<String> connectorSendWms(Embalagem embalagem) throws StoreAutomacaoSyncServiceException {

        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        String plainCreds = "wmsapi:storeautomacao2018";
        byte[] plainCredsBytes = plainCreds.getBytes();
        byte[] base64CredsBytes = Base64.encodeBase64(plainCredsBytes);
        String base64Creds = new String(base64CredsBytes);

        headers.add("Authorization", "Basic " + base64Creds);
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        MultiValueMap<String, String> map= new LinkedMultiValueMap<String, String>();

        map.add("oblpn_nbr", embalagem.getOb_lpn_nbr()); // "OBLPN26111806"
        map.add("facility_code",embalagem.getFacility_code()); //"J3PL"
        map.add("company_code", embalagem.getCompany_code()); //  "59456277000176"

        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);

        String url = "https://ta2.wms.ocs.oraclecloud.com/storeautomacao_test/wms/api/assign_and_load_oblpn/";

        ResponseEntity<String> response = null;
        try {
            response = restTemplate.postForEntity(url, request , String.class );
        } catch (RestClientException e) {
            e.printStackTrace();
        }

        return response;

    }
}
