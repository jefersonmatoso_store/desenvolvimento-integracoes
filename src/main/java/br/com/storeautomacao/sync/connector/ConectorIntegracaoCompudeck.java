package br.com.storeautomacao.sync.connector;

/**
 * Created by iomar on 04/12/18.
 */
public interface ConectorIntegracaoCompudeck {
    void ligar();
    void desligar();
}
