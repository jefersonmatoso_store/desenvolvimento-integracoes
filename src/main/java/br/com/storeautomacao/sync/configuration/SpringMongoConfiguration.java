package br.com.storeautomacao.sync.configuration;

import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoDbFactory;

import java.util.Arrays;

@Configuration
@PropertySource("classpath:application.properties")
public class SpringMongoConfiguration {

    private String mongodbUrl;

    private int mongodbPort;

    private String mongodbName;

    private String mongodbUserName;

    private String mongodbPassword;

    @Autowired
    private Environment environment;

    public @Bean
    MongoDbFactory mongoDbFactory() throws Exception {

        mongodbUrl = environment.getProperty("mongodb.url");

        mongodbPort = Integer.valueOf(environment.getProperty("mongodb.port"));

        mongodbName = environment.getProperty("mongodb.name");

        mongodbUserName = environment.getProperty("mongodb.username");

        mongodbPassword = environment.getProperty("mongodb.password");

        MongoCredential credential = MongoCredential.createCredential(mongodbUserName, mongodbName, mongodbPassword.toCharArray());

        ServerAddress serverAddress = new ServerAddress(mongodbUrl, mongodbPort);

        MongoClient m = new MongoClient(serverAddress, Arrays.asList(credential));

        return new SimpleMongoDbFactory(m, mongodbName);

    }

    public @Bean
    MongoTemplate mongoTemplate() throws Exception {

        MongoTemplate mongoTemplate = new MongoTemplate(mongoDbFactory());

        return mongoTemplate;

    }

}
