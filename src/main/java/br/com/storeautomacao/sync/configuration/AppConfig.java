package br.com.storeautomacao.sync.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.datasource.init.DataSourceInitializer;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;


    @Configuration
    @ComponentScan
    @EnableTransactionManagement
    @PropertySource(value = { "classpath:application.properties" })
    public class AppConfig
    {
        @Autowired
        private Environment env;

        @Value("${init-db:false}")
        private String initDatabase;

        @Bean
        public static PropertySourcesPlaceholderConfigurer placeHolderConfigurer()
        {
            return new PropertySourcesPlaceholderConfigurer();
        }

        @Bean
        public JdbcTemplate jdbcTemplate(DataSource dataSource)
        {
            return new JdbcTemplate(dataSource);
        }

        @Bean
        public PlatformTransactionManager transactionManager(DataSource dataSource)
        {
            return new DataSourceTransactionManager(dataSource);
        }

        @Bean
        public DataSource dataSource()
        {
            /*DriverManagerDataSource dataSource = new DriverManagerDataSource();
            dataSource.setDriverClassName("com.mysql.jdbc.Driver");
            dataSource.setUrl("jdbc:mysql://192.168.1.54:3306/esl");
            dataSource.setUsername("esl_app");
            dataSource.setPassword("123456");
            return dataSource;*/

            DriverManagerDataSource ds = new DriverManagerDataSource();
            ds.setDriverClassName(oracle.jdbc.driver.OracleDriver.class.getName());
            //Dev
            /*ds.setUrl("jdbc:oracle:thin:@(DESCRIPTION =\n" +
                    "    (ADDRESS_LIST =\n" +
                    "      (ADDRESS = (PROTOCOL = TCP)(HOST = 144.22.86.125)(PORT = 1521))\n" +
                    "    )\n" +
                    "    (CONNECT_DATA =\n" +
                    "      (SERVER = DEDICATED)\n" +
                    "      (SERVICE_NAME = XE)\n" +
                    "    )\n" +
                    "  )");*/
              //Hml
//            ds.setUrl("jdbc:oracle:thin:@(DESCRIPTION =\n" +
//                    "    (ADDRESS_LIST =\n" +
//                    "      (ADDRESS = (PROTOCOL = TCP)(HOST = 172.16.10.50)(PORT = 1521))\n" +
//                    "    )\n" +
//                    "    (CONNECT_DATA =\n" +
//                    "      (SID = dev)\n" +
//                    "    )\n" +
//                    "  )");
            //Prod

            ds.setUrl("jdbc:oracle:thin:@(DESCRIPTION =\n" +
                    "    (ADDRESS_LIST =\n" +
                    "      (ADDRESS = (PROTOCOL = TCP)(HOST = 10.150.88.12)(PORT = 1521))\n" +
                    "    )\n" +
                    "    (CONNECT_DATA =\n" +
                    "      (SERVICE_NAME = dbcaseli)\n" +
                    "    )\n" +
                    "  )");
            ds.setUsername("oraint");
            //ds.setUsername("ORAINT");
            ds.setPassword("usersystem");
            return ds;
        }

        @Bean
        public DataSourceInitializer dataSourceInitializer(DataSource dataSource)
        {
            DataSourceInitializer dataSourceInitializer = new DataSourceInitializer();
            dataSourceInitializer.setDataSource(dataSource);
            ResourceDatabasePopulator databasePopulator = new ResourceDatabasePopulator();
            databasePopulator.addScript(new ClassPathResource("data.sql"));
            dataSourceInitializer.setDatabasePopulator(databasePopulator);
            dataSourceInitializer.setEnabled(Boolean.parseBoolean(initDatabase));
            return dataSourceInitializer;
        }
    }


