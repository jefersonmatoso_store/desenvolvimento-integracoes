package br.com.storeautomacao.sync.util;

import br.com.storeautomacao.sync.model.entity.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SqlUtil {

    static Logger logger = LoggerFactory.getLogger(SqlUtil.class);

    public static String getXmlBody(String item_alternate_code, String part_a, String description, String barcode, String unit_cost, String unit_length, String unit_width) {

        String xmlString = "<?xml version=\"1.0\" encoding=\"utf-8\"?><LgfData> <Header> <DocumentVersion>9.0.0</DocumentVersion> <OriginSystem>JEF</OriginSystem> <ClientEnvCode>JEF</ClientEnvCode> <ParentCompanyCode>C01</ParentCompanyCode> <Entity>item</Entity> <TimeStamp>2012-12-13T12:12:12</TimeStamp> <MessageId>123</MessageId> </Header> <ListOfItems> <item> <company_code>59456277000176</company_code> <item_alternate_code>" + item_alternate_code + "</item_alternate_code> <part_a>" + part_a + "</part_a> <part_b></part_b> <part_c></part_c> <part_d></part_d> <part_e></part_e> <part_f></part_f> <pre_pack_code></pre_pack_code> <action_code>UPDATE</action_code> <description>" + description + "</description> <barcode>" + barcode + "</barcode> <unit_cost>" + unit_cost + "</unit_cost> <unit_length>" + unit_length + "</unit_length> <unit_width>" + unit_width + "</unit_width> <unit_height></unit_height> <unit_weight></unit_weight> <unit_volume></unit_volume> <hazmat></hazmat> <recv_type></recv_type> <ob_lpn_type></ob_lpn_type> <catch_weight_method></catch_weight_method> <order_consolidation_attr></order_consolidation_attr> <season_code></season_code> <brand_code></brand_code> <cust_attr_1></cust_attr_1> <cust_attr_2></cust_attr_2> <retail_price></retail_price> <net_cost></net_cost> <currency_code></currency_code> <std_pack_qty></std_pack_qty> <std_pack_length></std_pack_length> <std_pack_width></std_pack_width> <std_pack_height></std_pack_height> <std_pack_weight></std_pack_weight> <std_pack_volume></std_pack_volume> <std_case_qty></std_case_qty> <max_case_qty></max_case_qty> <std_case_length></std_case_length> <std_case_width></std_case_width> <std_case_height></std_case_height> <std_case_weight></std_case_weight> <std_case_volume></std_case_volume> <dimension1></dimension1> <dimension2></dimension2> <dimension3></dimension3> <hierarchy1_code></hierarchy1_code> <hierarchy1_description></hierarchy1_description> <hierarchy2_code></hierarchy2_code> <hierarchy2_description></hierarchy2_description> <hierarchy3_code></hierarchy3_code> <hierarchy3_description></hierarchy3_description> <hierarchy4_code></hierarchy4_code> <hierarchy4_description></hierarchy4_description> <hierarchy5_code></hierarchy5_code> <hierarchy5_description></hierarchy5_description> <group_code></group_code> <group_description></group_description> <external_style></external_style> <vas_group_code></vas_group_code> <short_descr></short_descr> <putaway_type></putaway_type> <conveyable></conveyable> <stackability_code></stackability_code> <sortable></sortable> <min_dispatch_uom></min_dispatch_uom> <product_life></product_life> <percent_acceptable_product_life></percent_acceptable_product_life> <lpns_per_tier></lpns_per_tier> <tiers_per_pallet></tiers_per_pallet> <velocity_code></velocity_code> <req_batch_nbr_flg></req_batch_nbr_flg> <serial_nbr_tracking></serial_nbr_tracking> <regularity_code></regularity_code> <harmonized_tariff_code></harmonized_tariff_code> <harmonized_tariff_description></harmonized_tariff_description> <full_oblpn_type></full_oblpn_type> <case_oblpn_type></case_oblpn_type> <pack_oblpn_type></pack_oblpn_type> <description_2></description_2> <description_3></description_3> <invn_attr_a_tracking></invn_attr_a_tracking> <invn_attr_a_dflt_value></invn_attr_a_dflt_value> <invn_attr_b_tracking></invn_attr_b_tracking> <invn_attr_b_dflt_value></invn_attr_b_dflt_value> <invn_attr_c_tracking></invn_attr_c_tracking> <invn_attr_c_dflt_value></invn_attr_c_dflt_value> <nmfc_code></nmfc_code> <conversion_factor></conversion_factor> <invn_attr_d_tracking></invn_attr_d_tracking> <invn_attr_e_tracking></invn_attr_e_tracking> <invn_attr_f_tracking></invn_attr_f_tracking> <invn_attr_g_tracking></invn_attr_g_tracking> <host_aware_item_flg>true</host_aware_item_flg> <packing_tolerance_percent></packing_tolerance_percent> <un_number></un_number> <un_class></un_class> <un_description></un_description> <packing_group></packing_group> <proper_shipping_name></proper_shipping_name> <excepted_qty_instr></excepted_qty_instr> <limited_qty_flg></limited_qty_flg> <fulldg_flg></fulldg_flg> <hazard_statement></hazard_statement> <shipping_temperature_instr></shipping_temperature_instr> <carrier_commodity_description></carrier_commodity_description> <hazmat_packaging_description></hazmat_packaging_description> <shipping_conversion_factor></shipping_conversion_factor> <shipping_uom></shipping_uom> <handle_decimal_qty_flg></handle_decimal_qty_flg> </item> </ListOfItems> </LgfData>";

        return xmlString;
    }

    public static String getSqlInsertIntegracao(Documento documento, Integracao integracao) {

        String sql = "INSERT INTO INTEGRACAO" +
                "(" +
                "SEQUENCIAINTEGRACAO," +
                "TIPOINTEGRACAO," +
                "ESTADOINTEGRACAO," +
                "DATALOG," +
                "DATAPROCESSAMENTO," +
                "SEQUENCIARELACIONADA," +
                "REFERENCIA," +
                "CODIGOPROVEDOR," +
                "DATAAGENDAMENTO" +
                ") VALUES (" +
                "" + "SEQ_INTEGRACAO.nextval" + "," +
                "" + integracao.getTipoIntegracao() + "," +
                "" + integracao.getEstadoIntegracao() + "," +
                "TO_DATE('" + DateUtil.converterDate(integracao.getDataLog()) + "')," +
                "TO_DATE('" + DateUtil.converterDate(integracao.getDataProcessamento()) + "')," +
                "NULL," +
                "NULL," +
                "NULL," +
                "NULL" +
                ")";

        return sql;

    }

    public static String getSqlInsertDocumento(Documento documento, Integracao integracao) {
        String lpnType = null;
        if(documento.getTipoEntrega() != null){
            lpnType = documento.getTipoEntrega();
        }

        String sql = "INSERT INTO DOCUMENTO" +
                "(" +
                "SEQUENCIAINTEGRACAO," +
                "SEQUENCIADOCUMENTO," +
                "TIPOINTEGRACAO," +
                "CODIGOESTABELECIMENTO," +
                "CODIGODEPOSITANTE," +
                "TIPODOCUMENTO," +
                "SERIEDOCUMENTO," +
                "NUMERODOCUMENTO," +
                "MODELODOCUMENTO," +
                "CODIGOEMPRESA," +
                "CODIGOENTREGA," +
                "CODIGOTRANSPORTADORA," +
                "NATUREZAOPERACAO," +
                "DESCRICAONATUREZAOPERACAO," +
                "CFOP," +
                "CONHECIMENTOTRANSPORTE," +
                "DATAEMISSAO," +
                "DATAPREVISAOMOVIMENTO," +
                "VALORTOTALDOCUMENTO," +
                "VALORTOTALPRODUTO," +
                "VALORICMS," +
                "VALORICMSSUB," +
                "VALORBASEIPI," +
                "VALORIPI," +
                "VALORFRETE," +
                "VALORBASEICMS," +
                "VALORBASEICMSSUB," +
                "VALORSEGURO," +
                "VALORDESCONTO," +
                "VALORACRESCIMO," +
                "ESPECIEVOLUME," +
                "QUANTIDADEVOLUME," +
                "MARCAVOLUME," +
                "NUMEROVOLUME," +
                "PESOBRUTO," +
                "PESOLIQUIDO," +
                "TIPOFRETE," +
                "PLACAVEICULO," +
                "UFVEICULO," +
                "NUMERODOCUMENTORELACIONADO," +
                "CNPJCPFDEPOSITANTE," +
                "CNPJCPFEMPRESA," +
                "NOMEEMPRESA," +
                "ENDERECOEMPRESA," +
                "BAIRROEMPRESA," +
                "MUNICIPIOEMPRESA," +
                "UFEMPRESA," +
                "CEPEMPRESA," +
                "INSCRICAOEMPRESA," +
                "TIPOEMPRESA," +
                "TIPOPESSOAEMPRESA," +
                "CNPJCPFTRANSPORTADORA," +
                "NOMETRANSPORTADORA," +
                "ENDERECOTRANSPORTADORA," +
                "BAIRROTRANSPORTADORA," +
                "MUNICIPIOTRANSPORTADORA," +
                "UFTRANSPORTADORA," +
                "CEPTRANSPORTADORA," +
                "INSCRICAOTRANSPORTADORA," +
                "TIPOTRANSPORTADORA," +
                "TIPOPESSOATRANSPORTADORA," +
                "CNPJCPFENTREGA," +
                "NOMEENTREGA," +
                "ENDERECOENTREGA," +
                "BAIRROENTREGA," +
                "MUNICIPIOENTREGA," +
                "UFENTREGA," +
                "CEPENTREGA," +
                "INSCRICAOENTREGA," +
                "TIPOENTREGA," + // TIPO ENTREGA
                "TIPOPESSOAENTREGA," +
                "REGIAO," +
                "DESCRICAOREGIAO," +
                "AGRUPADOR," +
                "DATAENTREGA," +
                "VOLUMECUBICO," +
                "CODIGOEMPRESADESTINO," +
                "CNPJCPFDESTINO," +
                "NOMEDESTINO," +
                "ENDERECODESTINO," +
                "BAIRRODESTINO," +
                "MUNICIPIODESTINO," +
                "UFDESTINO," +
                "CEPDESTINO," +
                "INSCRICAODESTINO," +
                "TIPODESTINO," +
                "TIPOPESSOADESTINO," +
                "CODIGOTRANSPORTADORAREDES," +
                "CNPJCPFTRANSPORTADORAREDES," +
                "NOMETRANSPORTADORAREDES," +
                "ENDERECOTRANSPORTADORAREDES," +
                "BAIRROTRANSPORTADORAREDES," +
                "MUNICIPIOTRANSPORTADORAREDES," +
                "UFTRANSPORTADORAREDES," +
                "CEPTRANSPORTADORAREDES," +
                "INSCRICAOTRANSPORTADORAREDES," +
                "TIPOTRANSPORTADORAREDES," +
                "TIPOPESSOATRANSPORTADORAREDES," +
                "INFORMACAOADICIONAL1," +
                "INFORMACAOADICIONAL2," +
                "INFORMACAOADICIONAL3," +
                "VALORMINIMO," +
                "FATPARCIALORDEMSEPARACAO," +
                "ENDERECODOCA," +
                "ROTA," +
                "SUBROTA," +
                "SEQUENCIAPEDIDO," +
                "OBSERVACAO," +
                "DOCUMENTOQUALIDADE," +
                "CODIGODOCUMENTO," +
                "INIBIRFRACIONAMENTO," +
                "DATAMOVIMENTO," +
                "ESPECIEDOCUMENTO," +
                "CONTRIBUINTEICMSEMPRESA," +
                "NFECHAVEACESSO," +
                "COMPLEMENTOEMPRESA," +
                "COMPLEMENTOTRANSPORTADORA," +
                "COMPLEMENTOENTREGA," +
                "COMPLEMENTOTRANSPORTADORAREDES," +
                "CODIGOIBGEEMPRESA," +
                "CODIGOIBGEENTREGA," +
                "CODIGOIBGETRANSPORTADORA," +
                "CODIGOIBGETRANSPORTADORAREDES," +
                "DESCRICAOMENSAGEMCONTRIBUINTE," +
                "CNPJCPFESTABELECIMENTO," +
                "CODIGOIBGEDESTINO," +
                "DATAAGENDAMENTO," +
                "REGRAENDERECO," +
                "BLOQUEARCANCELAMENTO," +
                "NFEESTADO," +
                "NFEDESCRICAOESTADO," +
                "CODIGOFINALIDADE," +
                "NUMEROENDERECOEMPRESA," +
                "ALIQUOTAISS," +
                "VALORISS," +
                "TIPOCONTRIBUINTEEMPRESA," +
                "TIPODOCUMENTOFISCAL," +
                "CNAEFISCALEMPRESA," +
                "INFORMACAOADICIONALFISCO," +
                "REGIMEESPECIALTRIBUTACAO," +
                "DESCREGIMEESPECIALTRIBUTACAO," +
                "NUMEROENDERECOENTREGA," +
                "NUMEROENDERECODESTINO," +
                "NUMEROENDERECOTRANSPORTADORA," +
                "NUMEROENDERECOTRANSPREDES," +
                "VALORISSRETIDO," +
                "CNAEFISCALDESTINO," +
                "CNAEFISCALENTREGA," +
                "CNAEFISCALTRANSPORTADORA," +
                "CNAEFISCALTRANSPREDES," +
                "NATUREZAOPERACAOIMPRESSAO," +
                "REGIMEESPTRIBEMPRESA," +
                "DESCREGIMEESPTRIBEMPRESA," +
                "REGIMEESPTRIBDEPOSITANTE," +
                "DESCREGIMEESPTRIBDEPOSITANTE," +
                "REGIMEESPTRIBTRANSP," +
                "DESCREGIMEESPTRIBTRANSP," +
                "REGIMEESPTRIBDESTINO," +
                "DESCREGIMEESPTRIBDESTINO," +
                "REGIMEESPTRIBENTREGA," +
                "DESCREGIMEESPTRIBENTREGA," +
                "REGIMEESPTRIBTRANSPREDES," +
                "DESCREGIMEESPTRIBTRANSPREDES," +
                "CODIGOCONSIGNATARIO," +
                "CNPJCPFCONSIGNATARIO," +
                "CNAEFISCALCONSIGNATARIO," +
                "NOMECONSIGNATARIO," +
                "ENDERECOCONSIGNATARIO," +
                "NUMEROENDERECOCONSIGNATARIO," +
                "COMPLEMENTOCONSIGNATARIO," +
                "BAIRROCONSIGNATARIO," +
                "MUNICIPIOCONSIGNATARIO," +
                "UFCONSIGNATARIO," +
                "CEPCONSIGNATARIO," +
                "INSCRICAOCONSIGNATARIO," +
                "TIPOPESSOACONSIGNATARIO," +
                "TIPOEMPRESACONSIGNATARIO," +
                "CODIGOIBGECONSIGNATARIO," +
                "COMPLEMENTODESTINO," +
                "INDICADORIEEMPRESA," +
                "INDICADORIEENTREGA," +
                "INDICADORIEDESTINO," +
                "INDICADORIETRANSPORTADORA," +
                "INDICADORIETRANSPORTADORAREDES," +
                "CONTRIBUINTEICMSENTREGA," +
                "CONTRIBUINTEICMSDESTINO," +
                "CONTRIBUINTEICMSTRANSPORTADORA," +
                "CONTRIBUINTEICMSTRANSPREDES," +
                "TIPOCONTRIBUINTEENTREGA," +
                "TIPOCONTRIBUINTEDESTINO," +
                "TIPOCONTRIBUINTETRANSPORTADORA," +
                "TIPOCONTRIBUINTETRANSPREDES," +
                "SEQUENCIAENTREGA," +
                "FONEEMPRESA," +
                "FONETRANSPORTADORA," +
                "FONEEMPRESAENTREGA," +
                "FONEEMPRESADESTINO," +
                "FONETRANSPORTADORAREDES," +
                "NFEPROTOCOLO," +
                "NFEDIGESTVALUE," +
                "NFEDATARECEBIMENTO," +
                "NFEAMBIENTE," +
                "DESCRICAOAMBIENTE," +
                "NFEID," +
                "NFETIPOEMISSAO," +
                "DESCRICAOTIPOEMISSAO," +
                "NFETIPOIMPRESSAO," +
                "DESCRICAOTIPOIMPRESSAO," +
                "NFEDATAREGISTRODPEC," +
                "NFEREGISTRODPEC," +
                "NFEJUSTIFICATIVA," +
                "NFEVERSAO," +
                "NFEDATAAUTORIZACAO," +
                "NFEUFEMBARQUE," +
                "NFELOCALEMBARQUE," +
                "NFECONSUMIDORFINAL," +
                "NFEINDICADORPRESENCIAL," +
                "NFELOCALDESPACHO," +
                "NFETIPOOPERACAO," +
                "DESCRICAONFETIPOOPERACAO," +
                "NFECHAVEACESSOREFERENCIA," +
                "NFEIMPRIMIR," +
                "NFEESTADOANTERIOR," +
                "VALORSERVICO" +
                ") VALUES (" +
                "" + documento.getSequenciaIntegracao() + "," +
                "1," + // SEQUENCIADOCUMENTO" +
                "" + documento.getTipoIntegracao() + "," + // TIPOINTEGRACAO" +
                "" + documento.getCodigoEstabelecimento() + "," + // CODIGOESTABELECIMENTO" +
                "'" + documento.getCodigoDepositante() + "'," + // CODIGODEPOSITANTE" +
                "'" + documento.getTipoDocumento() + "'," + // TIPODOCUMENTO" +
                "'" + documento.getSerieDocumento() + "'," + // SERIEDOCUMENTO" +
                "'" + documento.getNumeroDocumento() + "'," + // NUMERODOCUMENTO" +
                "NULL," + // MODELODOCUMENTO" +
                "'" + documento.getCodigoEmpresa() + "'," + // CODIGOEMPRESA" +
                "NULL," + // CODIGOENTREGA" +
                "NULL," + // CODIGOTRANSPORTADORA" +
                "'" + documento.getNaturezaOperacao() + "'," + // NATUREZAOPERACAO" +
                "'" + documento.getDescricaoNaturezaOperacao() + "'," + // DESCRICAONATUREZAOPERACAO" +
                "'" + documento.getCfop() + "'," + // CFOP" +
                "NULL," + // CONHECIMENTOTRANSPORTE" +
                "TO_DATE('" + DateUtil.converterDate(documento.getDataEmissao()) + "')," + // DATAEMISSAO" +
                "TO_DATE('" + DateUtil.converterDate(documento.getDataPrevisaoMovimento()) + "')," + // DATAPREVISAOMOVIMENTO" +
                "0," + // VALORTOTALDOCUMENTO" +
                "0," + // VALORTOTALPRODUTO" +
                "NULL," +
                "NULL," +
                "NULL," +
                "NULL," +
                "NULL," +
                "NULL," +
                "NULL," +
                "NULL," +
                "NULL," +
                "NULL," +
                "NULL," +
                "NULL," +
                "NULL," +
                "NULL," +
                "0," +
                "0," +
                "NULL," +
                "NULL," +
                "NULL," +
                "NULL," +
                "NULL," +
                "NULL," +
                "NULL," +
                "NULL," +
                "NULL," +
                "NULL," +
                "NULL," +
                "NULL," +
                "NULL," +
                "NULL," +
                "NULL," +
                "NULL," +
                "NULL," +
                "NULL," +
                "NULL," +
                "NULL," +
                "NULL," +
                "NULL," +
                "NULL," +
                "NULL," +
                "NULL," +
                "NULL," +
                "NULL," +
                "NULL," +
                "NULL," +
                "NULL," +
                "NULL," +
                "NULL," +
                "NULL," +
                "'" + lpnType + "'," + // AGRUPADOR" +
                "NULL," +
                "NULL," +
                "NULL," +
                "" + documento.getAgrupador() + "," + // AGRUPADOR" +
                "NULL," +
                "NULL," +
                "NULL," +
                "NULL," +
                "NULL," +
                "NULL," +
                "NULL," +
                "NULL," +
                "NULL," +
                "NULL," +
                "NULL," + // INSCRICAODESTINO" +
                "NULL," + // TIPODESTINO" +
                "NULL," + // TIPOPESSOADESTINO" +
                "NULL," + // CODIGOTRANSPORTADORAREDES" +
                "NULL," + // CNPJCPFTRANSPORTADORAREDES" +
                "NULL," + // NOMETRANSPORTADORAREDES" +
                "NULL," + // ENDERECOTRANSPORTADORAREDES" +
                "NULL," + // BAIRROTRANSPORTADORAREDES" +
                "NULL," + // MUNICIPIOTRANSPORTADORAREDES" +
                "NULL," + // UFTRANSPORTADORAREDES" +
                "NULL," + // CEPTRANSPORTADORAREDES" +
                "NULL," + // INSCRICAOTRANSPORTADORAREDES" +
                "NULL," + // TIPOTRANSPORTADORAREDES" +
                "NULL," + // TIPOPESSOATRANSPORTADORAREDES" +
                "NULL," + // INFORMACAOADICIONAL1" +
                "NULL," + // INFORMACAOADICIONAL2" +
                "NULL," + // INFORMACAOADICIONAL3" +
                "NULL," + // VALORMINIMO" +
                "NULL," + // FATPARCIALORDEMSEPARACAO" +
                "NULL," + // ENDERECODOCA" +
                "NULL," + // ROTA" +
                "NULL," + // SUBROTA" +
                "NULL," + // SEQUENCIAPEDIDO" +
                "NULL," + // OBSERVACAO" +
                "NULL," + // DOCUMENTOQUALIDADE" +
                "NULL," + // CODIGODOCUMENTO" +
                "NULL," + // INIBIRFRACIONAMENTO" +
                "TO_DATE('" + DateUtil.converterDate(documento.getDataMovimento()) + "')," + // DATAMOVIMENTO" +
                "NULL," + // ESPECIEDOCUMENTO" +
                "NULL," + // CONTRIBUINTEICMSEMPRESA" +
                "NULL," + // NFECHAVEACESSO" +
                "NULL," + // COMPLEMENTOEMPRESA" +
                "NULL," + // COMPLEMENTOTRANSPORTADORA" +
                "NULL," + // COMPLEMENTOENTREGA" +
                "NULL," + // COMPLEMENTOTRANSPORTADORAREDES" +
                "NULL," + // CODIGOIBGEEMPRESA" +
                "NULL," + // CODIGOIBGEENTREGA" +
                "NULL," + // CODIGOIBGETRANSPORTADORA" +
                "NULL," + // CODIGOIBGETRANSPORTADORAREDES" +
                "NULL," + // DESCRICAOMENSAGEMCONTRIBUINTE" +
                "NULL," + // CNPJCPFESTABELECIMENTO" +
                "NULL," + // CODIGOIBGEDESTINO" +
                "NULL," + // DATAAGENDAMENTO" +
                "NULL," + // REGRAENDERECO" +
                "NULL," + // BLOQUEARCANCELAMENTO" +
                "NULL," + // NFEESTADO" +
                "NULL," + // NFEDESCRICAOESTADO" +
                "NULL," + // CODIGOFINALIDADE" +
                "NULL," + // NUMEROENDERECOEMPRESA" +
                "NULL," + // ALIQUOTAISS" +
                "NULL," + // VALORISS" +
                "NULL," + // TIPOCONTRIBUINTEEMPRESA" +
                "NULL," + // TIPODOCUMENTOFISCAL" +
                "NULL," + // CNAEFISCALEMPRESA" +
                "NULL," + // INFORMACAOADICIONALFISCO" +
                "NULL," + // REGIMEESPECIALTRIBUTACAO" +
                "NULL," + // DESCREGIMEESPECIALTRIBUTACAO" +
                "NULL," + // NUMEROENDERECOENTREGA" +
                "NULL," + // NUMEROENDERECODESTINO" +
                "NULL," + // NUMEROENDERECOTRANSPORTADORA" +
                "NULL," + // NUMEROENDERECOTRANSPREDES" +
                "NULL," + // VALORISSRETIDO" +
                "NULL," + // CNAEFISCALDESTINO" +
                "NULL," + // CNAEFISCALENTREGA" +
                "NULL," + // CNAEFISCALTRANSPORTADORA" +
                "NULL," + // CNAEFISCALTRANSPREDES" +
                "NULL," + // NATUREZAOPERACAOIMPRESSAO" +
                "NULL," + // REGIMEESPTRIBEMPRESA" +
                "NULL," + // DESCREGIMEESPTRIBEMPRESA" +
                "NULL," + // REGIMEESPTRIBDEPOSITANTE" +
                "NULL," + // DESCREGIMEESPTRIBDEPOSITANTE" +
                "NULL," + // REGIMEESPTRIBTRANSP" +
                "NULL," + // DESCREGIMEESPTRIBTRANSP" +
                "NULL," + // REGIMEESPTRIBDESTINO" +
                "NULL," + // DESCREGIMEESPTRIBDESTINO" +
                "NULL," + // REGIMEESPTRIBENTREGA" +
                "NULL," + // DESCREGIMEESPTRIBENTREGA" +
                "NULL," + // REGIMEESPTRIBTRANSPREDES" +
                "NULL," + // DESCREGIMEESPTRIBTRANSPREDES" +
                "NULL," + // CODIGOCONSIGNATARIO" +
                "NULL," + // CNPJCPFCONSIGNATARIO" +
                "NULL," + // CNAEFISCALCONSIGNATARIO" +
                "NULL," + // NOMECONSIGNATARIO" +
                "NULL," + // ENDERECOCONSIGNATARIO" +
                "NULL," + // NUMEROENDERECOCONSIGNATARIO" +
                "NULL," + // COMPLEMENTOCONSIGNATARIO" +
                "NULL," + // BAIRROCONSIGNATARIO" +
                "NULL," + // MUNICIPIOCONSIGNATARIO" +
                "NULL," + // UFCONSIGNATARIO" +
                "NULL," + // CEPCONSIGNATARIO" +
                "NULL," + // INSCRICAOCONSIGNATARIO" +
                "NULL," + // TIPOPESSOACONSIGNATARIO" +
                "NULL," + // TIPOEMPRESACONSIGNATARIO" +
                "NULL," + // CODIGOIBGECONSIGNATARIO" +
                "NULL," + // COMPLEMENTODESTINO" +
                "NULL," + // INDICADORIEEMPRESA" +
                "NULL," + // INDICADORIEENTREGA" +
                "NULL," + // INDICADORIEDESTINO" +
                "NULL," + // INDICADORIETRANSPORTADORA" +
                "NULL," + // INDICADORIETRANSPORTADORAREDES" +
                "NULL," + // CONTRIBUINTEICMSENTREGA" +
                "NULL," + // CONTRIBUINTEICMSDESTINO" +
                "NULL," + // CONTRIBUINTEICMSTRANSPORTADORA" +
                "NULL," + // CONTRIBUINTEICMSTRANSPREDES" +
                "NULL," + // TIPOCONTRIBUINTEENTREGA" +
                "NULL," + // TIPOCONTRIBUINTEDESTINO" +
                "NULL," + // TIPOCONTRIBUINTETRANSPORTADORA" +
                "NULL," + // TIPOCONTRIBUINTETRANSPREDES" +
                "NULL," + // SEQUENCIAENTREGA" +
                "NULL," + // FONEEMPRESA" +
                "NULL," + // FONETRANSPORTADORA" +
                "NULL," + // FONEEMPRESAENTREGA" +
                "NULL," + // FONEEMPRESADESTINO" +
                "NULL," + // FONETRANSPORTADORAREDES" +
                "NULL," + // NFEPROTOCOLO" +
                "NULL," + // NFEDIGESTVALUE" +
                "NULL," + // NFEDATARECEBIMENTO" +
                "NULL," + // NFEAMBIENTE" +
                "NULL," + // DESCRICAOAMBIENTE" +
                "NULL," + // NFEID" +
                "NULL," + // NFETIPOEMISSAO" +
                "NULL," + // DESCRICAOTIPOEMISSAO" +
                "NULL," + // NFETIPOIMPRESSAO" +
                "NULL," + // DESCRICAOTIPOIMPRESSAO" +
                "NULL," + // NFEDATAREGISTRODPEC" +
                "NULL," + // NFEREGISTRODPEC" +
                "NULL," + // NFEJUSTIFICATIVA" +
                "NULL," + // NFEVERSAO" +
                "NULL," + // NFEDATAAUTORIZACAO" +
                "NULL," + // NFEUFEMBARQUE" +
                "NULL," + // NFELOCALEMBARQUE" +
                "NULL," + // NFECONSUMIDORFINAL" +
                "NULL," + // NFEINDICADORPRESENCIAL" +
                "NULL," + // NFELOCALDESPACHO" +
                "NULL," + // NFETIPOOPERACAO" +
                "NULL," + // DESCRICAONFETIPOOPERACAO" +
                "NULL," + // NFECHAVEACESSOREFERENCIA" +
                "NULL," + // NFEIMPRIMIR" +
                "NULL," + // NFEESTADOANTERIOR" +
                "NULL" + // VALORSERVICO" +
                ")";

        return sql;
    }

    public static String getSqlInsertIntegracaoDtl(Documento documento, DocumentoDetalhe documentoDetalhe, Integracao integracao, String facilytyCode) {

        String sql = "INSERT INTO DOCUMENTODETALHE" +
                "(" +
                "SEQUENCIAINTEGRACAO," +
                "SEQUENCIADOCUMENTO," +
                "SEQUENCIADETALHE," +
                "TIPOINTEGRACAO," +
                "CODIGOEMPRESA," +
                "CODIGOPRODUTO," +
                "TIPOUC," +
                "FATORTIPOUC," +
                "CLASSEPRODUTO," +
                "CLASSEDESTINO," +
                "QUANTIDADEMOVIMENTO," +
                "ALIQUOTAICMS," +
                "ALIQUOTAICMSSUB," +
                "ALIQUOTAIPI," +
                "VALORUNITARIO," +
                "TIPOLOGISTICO," +
                "DADOLOGISTICO," +
                "CLASSIFICACAOFISCAL," +
                "SITUACAOTRIBUTARIA," +
                "VALORDESCONTO," +
                "VALORACRESCIMO," +
                "NATUREZAOPERACAO," +
                "DESCRICAONATUREZAOPERACAO," +
                "CFOP," +
                "VOLUMECUBICO," +
                "INFORMACAOADICIONAL1," +
                "INFORMACAOADICIONAL2," +
                "INFORMACAOADICIONAL3," +
                "ORDEMSEPARACAO," +
                "NUMEROPEDIDOSEPARACAO," +
                "DATAPEDIDOSEPARACAO," +
                "CODIGOEMPRESARELAC," +
                "TIPODOCUMENTORELAC," +
                "SERIEDOCUMENTORELAC," +
                "NUMERODOCUMENTORELAC," +
                "LOTERELAC," +
                "LOTE," +
                "FATORBASEICMS," +
                "ALIQUOTAICMSREDUCAO," +
                "FATORBASEIPI," +
                "ALIQUOTAIPIREDUCAO," +
                "VALORUNITARIOFISCAL," +
                "VALORSERVICO," +
                "OBSERVACAO," +
                "NFEINFORMACAOADICIONAL," +
                "PESOLIQUIDO," +
                "PESOBRUTO," +
                "CODIGOTRIBUTACAOMUNICIPAL," +
                "CODIGOLISTASERVICO," +
                "DESCRICAOLISTASERVICO," +
                "VALORICMS," +
                "VALORBASEICMS," +
                "VALORICMSST," +
                "VALORBASEICMSST," +
                "ALIQUOTAISS," +
                "VALORISS," +
                "SITTRIBUTARIAORIGEM," +
                "SITTRIBUTARIAICMS," +
                "VALORIPI," +
                "VALORBASEIPI," +
                "NATUREZAOPERACAOIMPRESSAO," +
                "SEQUENCIA," +
                "SEQUENCIARELAC," +
                "IDCLASSEEMPRESA," +
                "VALORFRETE," +
                "VALORSEGURO" +
                ") VALUES (" +
                "" + integracao.getSequenciaIntegracao() + "," + // SEQUENCIAINTEGRACAO" +
                "1," + // SEQUENCIADOCUMENTO" +
                "" + documentoDetalhe.getSequenciaDetalhe() + "," + // SEQUENCIADETALHE" +
                "" + documentoDetalhe.getTipoIntegracao() + "," + // TIPOINTEGRACAO" +
                "'" + facilytyCode + "'," + // CODIGOEMPRESA" +
                "'" + documentoDetalhe.getCodigoProduto() + "'," + // CODIGOPRODUTO" +
                "'UN'," + // TIPOUC" +
                "1," + // FATORTIPOUC" +
                "'" + documentoDetalhe.getClasseProduto() + "'," + // CLASSEPRODUTO" +
                "NULL," + // CLASSEDESTINO" +
                "" + documentoDetalhe.getQuantidadeMovimento() + "," + // QUANTIDADEMOVIMENTO" +
                "0," + // ALIQUOTAICMS" +
                "0," + // ALIQUOTAICMSSUB" +
                "0," + // ALIQUOTAIPI" +
                "" + documentoDetalhe.getValorUnitario() + "," + // QUANTIDADEMOVIMENTO" +
                "NULL," + // TIPOLOGISTICO" +
                "NULL," + // DADOLOGISTICO" +
                "NULL," + // CLASSIFICACAOFISCAL" +
                "NULL," + // SITUACAOTRIBUTARIA" +
                "NULL," + // VALORDESCONTO" +
                "0," + // VALORACRESCIMO" +
                "NULL," + // NATUREZAOPERACAO" +
                "NULL," + // DESCRICAONATUREZAOPERACAO" +
                "NULL," + // CFOP" +
                "NULL," + // VOLUMECUBICO" +
                "NULL," + // INFORMACAOADICIONAL1" +
                "NULL," + // INFORMACAOADICIONAL2" +
                "NULL," + // INFORMACAOADICIONAL3" +
                "NULL," + // ORDEMSEPARACAO" +
                "NULL," + // NUMEROPEDIDOSEPARACAO" +
                "NULL," + // DATAPEDIDOSEPARACAO" +
                "NULL," + // CODIGOEMPRESARELAC" +
                "NULL," + // TIPODOCUMENTORELAC" +
                "NULL," + // SERIEDOCUMENTORELAC" +
                "NULL," + // NUMERODOCUMENTORELAC" +
                "NULL," + // LOTERELAC" +
                "NULL," + // LOTE" +
                "NULL," + // FATORBASEICMS" +
                "NULL," + // ALIQUOTAICMSREDUCAO" +
                "NULL," + // FATORBASEIPI" +
                "NULL," + // ALIQUOTAIPIREDUCAO" +
                "NULL," + // VALORUNITARIOFISCAL" +
                "NULL," + // VALORSERVICO" +
                "NULL," + // OBSERVACAO" +
                "NULL," + // NFEINFORMACAOADICIONAL" +
                "NULL," + // PESOLIQUIDO" +
                "NULL," + // PESOBRUTO" +
                "NULL," + // CODIGOTRIBUTACAOMUNICIPAL" +
                "NULL," + // CODIGOLISTASERVICO" +
                "NULL," + // DESCRICAOLISTASERVICO" +
                "NULL," + // VALORICMS" +
                "NULL," + // VALORBASEICMS" +
                "NULL," + // VALORICMSST" +
                "NULL," + // VALORBASEICMSST" +
                "NULL," + // ALIQUOTAISS" +
                "NULL," + // VALORISS" +
                "NULL," + // SITTRIBUTARIAORIGEM" +
                "NULL," + // SITTRIBUTARIAICMS" +
                "NULL," + // VALORIPI" +
                "NULL," + // VALORBASEIPI" +
                "NULL," + // NATUREZAOPERACAOIMPRESSAO" +
                "NULL," + // SEQUENCIA" +
                "NULL," + // SEQUENCIARELAC" +
                "NULL," + // IDCLASSEEMPRESA" +
                "NULL," + // VALORFRETE" +
                "NULL" + // VALORSEGURO" +
                ")";

        return sql;

    }

    /**
     * Integracao 252
     * @param integracao
     * @return
     */
    public static String getSqlInsertIntegracaoDtl252(Documento documento, DocumentoDetalhe documentoDetalhe, Integracao integracao, String facilytyCode) {

        String sql = "INSERT INTO DOCUMENTODETALHE" +
                "(" +
                "SEQUENCIAINTEGRACAO," +
                "SEQUENCIADOCUMENTO," +
                "SEQUENCIADETALHE," +
                "TIPOINTEGRACAO," +
                "CODIGOEMPRESA," +
                "CODIGOPRODUTO," +
                "TIPOUC," +
                "FATORTIPOUC," +
                "CLASSEPRODUTO," +
                "CLASSEDESTINO," +
                "QUANTIDADEMOVIMENTO," +
                "ALIQUOTAICMS," +
                "ALIQUOTAICMSSUB," +
                "ALIQUOTAIPI," +
                "VALORUNITARIO," +
                "TIPOLOGISTICO," +
                "DADOLOGISTICO," +
                "CLASSIFICACAOFISCAL," +
                "SITUACAOTRIBUTARIA," +
                "VALORDESCONTO," +
                "VALORACRESCIMO," +
                "NATUREZAOPERACAO," +
                "DESCRICAONATUREZAOPERACAO," +
                "CFOP," +
                "VOLUMECUBICO," +
                "INFORMACAOADICIONAL1," +
                "INFORMACAOADICIONAL2," +
                "INFORMACAOADICIONAL3," +
                "ORDEMSEPARACAO," +
                "NUMEROPEDIDOSEPARACAO," +
                "DATAPEDIDOSEPARACAO," +
                "CODIGOEMPRESARELAC," +
                "TIPODOCUMENTORELAC," +
                "SERIEDOCUMENTORELAC," +
                "NUMERODOCUMENTORELAC," +
                "LOTERELAC," +
                "LOTE," +
                "FATORBASEICMS," +
                "ALIQUOTAICMSREDUCAO," +
                "FATORBASEIPI," +
                "ALIQUOTAIPIREDUCAO," +
                "VALORUNITARIOFISCAL," +
                "VALORSERVICO," +
                "OBSERVACAO," +
                "NFEINFORMACAOADICIONAL," +
                "PESOLIQUIDO," +
                "PESOBRUTO," +
                "CODIGOTRIBUTACAOMUNICIPAL," +
                "CODIGOLISTASERVICO," +
                "DESCRICAOLISTASERVICO," +
                "VALORICMS," +
                "VALORBASEICMS," +
                "VALORICMSST," +
                "VALORBASEICMSST," +
                "ALIQUOTAISS," +
                "VALORISS," +
                "SITTRIBUTARIAORIGEM," +
                "SITTRIBUTARIAICMS," +
                "VALORIPI," +
                "VALORBASEIPI," +
                "NATUREZAOPERACAOIMPRESSAO," +
                "SEQUENCIA," +
                "SEQUENCIARELAC," +
                "IDCLASSEEMPRESA," +
                "VALORFRETE," +
                "VALORSEGURO" +
                ") VALUES (" +
                "" + documentoDetalhe.getSequenciaIntegracao() + "," + // SEQUENCIAINTEGRACAO" +
                "1," + // SEQUENCIADOCUMENTO" +
                "" + documentoDetalhe.getSequenciaDetalheNovo() + "," + // SEQUENCIADETALHE" +
                "" + documentoDetalhe.getTipoIntegracao() + "," + // TIPOINTEGRACAO" +
                "'" + facilytyCode + "'," + // CODIGOEMPRESA" +
                "'" + documentoDetalhe.getCodigoProduto() + "'," + // CODIGOPRODUTO" +
                "'UN'," + // TIPOUC" +
                "1," + // FATORTIPOUC" +
                "'" + documentoDetalhe.getClasseProduto() + "'," + // CLASSEPRODUTO" +
                "NULL," + // CLASSEDESTINO" +
                "" + documentoDetalhe.getQuantidadeMovimento() + "," + // QUANTIDADEMOVIMENTO" +
                "0," + // ALIQUOTAICMS" +
                "0," + // ALIQUOTAICMSSUB" +
                "0," + // ALIQUOTAIPI" +
                "" + documentoDetalhe.getValorUnitario() + "," + // VALOR UNITARIO" +
                "NULL," + // TIPOLOGISTICO" +
                "NULL," + // DADOLOGISTICO" +
                "NULL," + // CLASSIFICACAOFISCAL" +
                "NULL," + // SITUACAOTRIBUTARIA" +
                "NULL," + // VALORDESCONTO" +
                "0," + // VALORACRESCIMO" +
                "'6152'," + // NATUREZAOPERACAO" +
                "'TRANSFERENCIA MERC. ADQ.TERCEI'," + // DESCRICAONATUREZAOPERACAO" +
                "NULL," + // CFOP" +
                "NULL," + // VOLUMECUBICO" +
                "NULL," + // INFORMACAOADICIONAL1" +
                "NULL," + // INFORMACAOADICIONAL2" +
                "NULL," + // INFORMACAOADICIONAL3" +
                "NULL," + // ORDEMSEPARACAO" +
                "NULL," + // NUMEROPEDIDOSEPARACAO" +
                "NULL," + // DATAPEDIDOSEPARACAO" +
                "NULL," + // CODIGOEMPRESARELAC" +
                "NULL," + // TIPODOCUMENTORELAC" +
                "NULL," + // SERIEDOCUMENTORELAC" +
                "NULL," + // NUMERODOCUMENTORELAC" +
                "NULL," + // LOTERELAC" +
                "NULL," + // LOTE" +
                "NULL," + // FATORBASEICMS" +
                "NULL," + // ALIQUOTAICMSREDUCAO" +
                "NULL," + // FATORBASEIPI" +
                "NULL," + // ALIQUOTAIPIREDUCAO" +
                "NULL," + // VALORUNITARIOFISCAL" +
                "NULL," + // VALORSERVICO" +
                "NULL," + // OBSERVACAO" +
                "NULL," + // NFEINFORMACAOADICIONAL" +
                "NULL," + // PESOLIQUIDO" +
                "NULL," + // PESOBRUTO" +
                "NULL," + // CODIGOTRIBUTACAOMUNICIPAL" +
                "NULL," + // CODIGOLISTASERVICO" +
                "NULL," + // DESCRICAOLISTASERVICO" +
                "NULL," + // VALORICMS" +
                "NULL," + // VALORBASEICMS" +
                "NULL," + // VALORICMSST" +
                "NULL," + // VALORBASEICMSST" +
                "NULL," + // ALIQUOTAISS" +
                "NULL," + // VALORISS" +
                "NULL," + // SITTRIBUTARIAORIGEM" +
                "NULL," + // SITTRIBUTARIAICMS" +
                "NULL," + // VALORIPI" +
                "NULL," + // VALORBASEIPI" +
                "NULL," + // NATUREZAOPERACAOIMPRESSAO" +
                "NULL," + // SEQUENCIA" +
                "NULL," + // SEQUENCIARELAC" +
                "NULL," + // IDCLASSEEMPRESA" +
                "NULL," + // VALORFRETE" +
                "NULL" + // VALORSEGURO" +
                ")";

        return sql;

    }

    public static String getSqlInsertDocumentoEmbalagem(Embalagem embalagem, Integracao integracao, DocumentoDetalhe documentoDetalhe, int count) {

        String sql = "INSERT INTO DOCUMENTOEMBALAGEM\n" +
                "(" +
                "SEQUENCIAINTEGRACAO," +
                "SEQUENCIADOCUMENTO," +
                "SEQUENCIAEMBALAGEM," +
                "CODIGOEMBALAGEMEXPED," +
                "IDEMBALAGEMEXPED," +
                "TIPOEMBALAGEM," +
                "DESCRICAOTIPOEMBALAGEM," +
                "LARGURA," +
                "COMPRIMENTO," +
                "ALTURA," +
                "PESOTARA," +
                "CODIGOPRODUTO," +
                "TIPOUC," +
                "FATORTIPOUC," +
                "QUANTIDADE," +
                "CLASSEPRODUTO," +
                "LOTEFABRICACAO," +
                "DATAFABRICACAO," +
                "DATAVENCIMENTO," +
                "LOTEGERAL" +
                ") VALUES (" +
                "" + integracao.getSequenciaIntegracao() + "," + //-- SEQUENCIAINTEGRACAO
                "1, " + //-- SEQUENCIADOCUMENTO
                ""+count+", " + //-- SEQUENCIAEMBALAGEM
                "" + embalagem.getOb_lpn_nbr().substring(5, embalagem.getOb_lpn_nbr().length()) + "," + //-- CODIGOEMBALAGEMEXPED
                "'" + embalagem.getOb_lpn_nbr() + "'," + //-- IDEMBALAGEMEXPED
                "1," + //-- TIPOEMBALAGEM
                "'" + embalagem.getLpnType() + "'," + //-- DESCRICAOTIPOEMBALAGEM
                "1," + //-- LARGURA
                "1, " + //-- COMPRIMENTO
                "1, " + //-- ALTURA
                "0, " + //-- PESOTARA
                "" + documentoDetalhe.getCodigoProduto() + "," + //-- CODIGOPRODUTO
                "'UN', " + //-- TIPOUC
                "1, " + //-- FATORTIPOUC
                "1," + //-- QUANTIDADE
                "'OK', " + //-- CLASSEPRODUTO
                "NULL, " + //-- LOTEFABRICACAO
                "NULL, " + //-- DATAFABRICACAO
                "NULL," +  //-- DATAVENCIMENTO
                "NULL " + //-- LOTEGERAL
                ")";

        return sql;
    }


    public static String getSqlUpdateIntegracao(Integracao integracao) {

        String sql = ""
                + "UPDATE INTEGRACAO SET ESTADOINTEGRACAO = "
                + integracao.getEstadoIntegracao()
                + " WHERE SEQUENCIAINTEGRACAO = "
                + integracao.getSequenciaIntegracao()
                + "";
        return sql;
    }

    public static String getSqlUpdateIntegracaoLogix(Integracao integracao) {

        String sql = ""
                + "UPDATE INTEGRACAO SET ESTADOINTEGRACAO = "
                + integracao.getEstadoIntegracao()
                + ", REFERENCIA=1"
                + " WHERE SEQUENCIAINTEGRACAO = "
                + integracao.getSequenciaIntegracao()
                + "";
        return sql;
    }

    public static String getSqlInsertDocumentoDetalheSequencia(DocumentoDetalheSequencia documentoDetalheSequencia) {

        StringBuffer sqlInsertDocumentoDetalheSequencia = new StringBuffer();
        sqlInsertDocumentoDetalheSequencia.append("INSERT INTO DOCUMENTODETALHESEQUENCIA ");
        sqlInsertDocumentoDetalheSequencia.append("( ");
        sqlInsertDocumentoDetalheSequencia.append("SEQUENCIAINTEGRACAO, ");
        sqlInsertDocumentoDetalheSequencia.append("SEQUENCIADOCUMENTO, ");
        sqlInsertDocumentoDetalheSequencia.append("SEQUENCIADETALHE, ");
        sqlInsertDocumentoDetalheSequencia.append("TIPOINTEGRACAO, ");
        sqlInsertDocumentoDetalheSequencia.append("CODIGOEMPRESA, ");
        sqlInsertDocumentoDetalheSequencia.append("CODIGOPRODUTO, ");
        sqlInsertDocumentoDetalheSequencia.append("TIPOUC, ");
        sqlInsertDocumentoDetalheSequencia.append("FATORTIPOUC, ");
        sqlInsertDocumentoDetalheSequencia.append("CLASSEPRODUTO, ");
        sqlInsertDocumentoDetalheSequencia.append("CLASSEDESTINO, ");
        sqlInsertDocumentoDetalheSequencia.append("QUANTIDADEMOVIMENTO, ");
        sqlInsertDocumentoDetalheSequencia.append("ALIQUOTAICMS, ");
        sqlInsertDocumentoDetalheSequencia.append("ALIQUOTAICMSSUB, ");
        sqlInsertDocumentoDetalheSequencia.append("ALIQUOTAIPI, ");
        sqlInsertDocumentoDetalheSequencia.append("VALORUNITARIO, ");
        sqlInsertDocumentoDetalheSequencia.append("TIPOLOGISTICO, ");
        sqlInsertDocumentoDetalheSequencia.append("DADOLOGISTICO, ");
        sqlInsertDocumentoDetalheSequencia.append("CLASSIFICACAOFISCAL, ");
        sqlInsertDocumentoDetalheSequencia.append("SITUACAOTRIBUTARIA, ");
        sqlInsertDocumentoDetalheSequencia.append("VALORDESCONTO, ");
        sqlInsertDocumentoDetalheSequencia.append("VALORACRESCIMO, ");
        sqlInsertDocumentoDetalheSequencia.append("NATUREZAOPERACAO, ");
        sqlInsertDocumentoDetalheSequencia.append("DESCRICAONATUREZAOPERACAO, ");
        sqlInsertDocumentoDetalheSequencia.append("CFOP, ");
        sqlInsertDocumentoDetalheSequencia.append("VOLUMECUBICO, ");
        sqlInsertDocumentoDetalheSequencia.append("INFORMACAOADICIONAL1, ");
        sqlInsertDocumentoDetalheSequencia.append("INFORMACAOADICIONAL2, ");
        sqlInsertDocumentoDetalheSequencia.append("INFORMACAOADICIONAL3, ");
        sqlInsertDocumentoDetalheSequencia.append("ORDEMSEPARACAO, ");
        sqlInsertDocumentoDetalheSequencia.append("NUMEROPEDIDOSEPARACAO, ");
        sqlInsertDocumentoDetalheSequencia.append("DATAPEDIDOSEPARACAO, ");
        sqlInsertDocumentoDetalheSequencia.append("CODIGOEMPRESARELAC, ");
        sqlInsertDocumentoDetalheSequencia.append("TIPODOCUMENTORELAC, ");
        sqlInsertDocumentoDetalheSequencia.append("SERIEDOCUMENTORELAC, ");
        sqlInsertDocumentoDetalheSequencia.append("NUMERODOCUMENTORELAC, ");
        sqlInsertDocumentoDetalheSequencia.append("LOTERELAC, ");
        sqlInsertDocumentoDetalheSequencia.append("LOTE, ");
        sqlInsertDocumentoDetalheSequencia.append("FATORBASEICMS, ");
        sqlInsertDocumentoDetalheSequencia.append("ALIQUOTAICMSREDUCAO, ");
        sqlInsertDocumentoDetalheSequencia.append("FATORBASEIPI, ");
        sqlInsertDocumentoDetalheSequencia.append("ALIQUOTAIPIREDUCAO, ");
        sqlInsertDocumentoDetalheSequencia.append("VALORUNITARIOFISCAL, ");
        sqlInsertDocumentoDetalheSequencia.append("VALORSERVICO, ");
        sqlInsertDocumentoDetalheSequencia.append("OBSERVACAO, ");
        sqlInsertDocumentoDetalheSequencia.append("NFEINFORMACAOADICIONAL, ");
        sqlInsertDocumentoDetalheSequencia.append("PESOLIQUIDO, ");
        sqlInsertDocumentoDetalheSequencia.append("PESOBRUTO, ");
        sqlInsertDocumentoDetalheSequencia.append("CODIGOTRIBUTACAOMUNICIPAL, ");
        sqlInsertDocumentoDetalheSequencia.append("CODIGOLISTASERVICO, ");
        sqlInsertDocumentoDetalheSequencia.append("DESCRICAOLISTASERVICO, ");
        sqlInsertDocumentoDetalheSequencia.append("VALORICMS, ");
        sqlInsertDocumentoDetalheSequencia.append("VALORBASEICMS, ");
        sqlInsertDocumentoDetalheSequencia.append("VALORICMSST, ");
        sqlInsertDocumentoDetalheSequencia.append("VALORBASEICMSST, ");
        sqlInsertDocumentoDetalheSequencia.append("ALIQUOTAISS, ");
        sqlInsertDocumentoDetalheSequencia.append("VALORISS, ");
        sqlInsertDocumentoDetalheSequencia.append("SITTRIBUTARIAORIGEM, ");
        sqlInsertDocumentoDetalheSequencia.append("SITTRIBUTARIAICMS, ");
        sqlInsertDocumentoDetalheSequencia.append("VALORIPI, ");
        sqlInsertDocumentoDetalheSequencia.append("VALORBASEIPI, ");
        sqlInsertDocumentoDetalheSequencia.append("NATUREZAOPERACAOIMPRESSAO, ");
        sqlInsertDocumentoDetalheSequencia.append("SEQUENCIA, ");
        sqlInsertDocumentoDetalheSequencia.append("SEQUENCIARELAC, ");
        sqlInsertDocumentoDetalheSequencia.append("IDCLASSEEMPRESA, ");
        sqlInsertDocumentoDetalheSequencia.append("VALORFRETE, ");
        sqlInsertDocumentoDetalheSequencia.append("VALORSEGURO ");
        sqlInsertDocumentoDetalheSequencia.append(") VALUES ( ");
        sqlInsertDocumentoDetalheSequencia.append("SEQ_INTEGRACAO.nextval, "); // SEQUENCIAINTEGRACAO
        sqlInsertDocumentoDetalheSequencia.append(String.format("%s, ", documentoDetalheSequencia.getSequenciaDocumento())); // SEQUENCIADOCUMENTO
        sqlInsertDocumentoDetalheSequencia.append(String.format("%s, ", documentoDetalheSequencia.getSequenciaDetalhe())); //SEQUENCIADETALHE
        sqlInsertDocumentoDetalheSequencia.append("252, "); //TIPOINTEGRACAO
        sqlInsertDocumentoDetalheSequencia.append("'80', "); //CODIGOEMPRESA
        sqlInsertDocumentoDetalheSequencia.append("'147748013872', "); //CODIGOPRODUTO
        sqlInsertDocumentoDetalheSequencia.append("'UN', "); //TIPOUC
        sqlInsertDocumentoDetalheSequencia.append("1, "); //FATORTIPOUC
        sqlInsertDocumentoDetalheSequencia.append("'OK', "); //CLASSEPRODUTO
        sqlInsertDocumentoDetalheSequencia.append("NULL, "); //CLASSEDESTINO
        sqlInsertDocumentoDetalheSequencia.append("3, "); //QUANTIDADEMOVIMENTO
        sqlInsertDocumentoDetalheSequencia.append("0, "); //ALIQUOTAICMS
        sqlInsertDocumentoDetalheSequencia.append("0, "); //ALIQUOTAICMSSUB
        sqlInsertDocumentoDetalheSequencia.append("0, "); //ALIQUOTAIPI
        sqlInsertDocumentoDetalheSequencia.append("148.8, "); //VALORUNITARIO
        sqlInsertDocumentoDetalheSequencia.append("NULL, "); //TIPOLOGISTICO
        sqlInsertDocumentoDetalheSequencia.append("NULL, "); //DADOLOGISTICO
        sqlInsertDocumentoDetalheSequencia.append("NULL, "); //CLASSIFICACAOFISCAL
        sqlInsertDocumentoDetalheSequencia.append("NULL, "); //SITUACAOTRIBUTARIA
        sqlInsertDocumentoDetalheSequencia.append("NULL, "); //VALORDESCONTO
        sqlInsertDocumentoDetalheSequencia.append("0, "); //VALORACRESCIMO
        sqlInsertDocumentoDetalheSequencia.append("'6152', "); //NATUREZAOPERACAO
        sqlInsertDocumentoDetalheSequencia.append("'TRANSFERENCIA MERC. ADQ.TERCEI', "); //DESCRICAONATUREZAOPERACAO
        sqlInsertDocumentoDetalheSequencia.append("NULL, "); //CFOP
        sqlInsertDocumentoDetalheSequencia.append("NULL, "); //VOLUMECUBICO
        sqlInsertDocumentoDetalheSequencia.append("NULL, "); //INFORMACAOADICIONAL1
        sqlInsertDocumentoDetalheSequencia.append("NULL, "); //INFORMACAOADICIONAL2
        sqlInsertDocumentoDetalheSequencia.append("NULL, "); //INFORMACAOADICIONAL3
        sqlInsertDocumentoDetalheSequencia.append("NULL, "); //ORDEMSEPARACAO
        sqlInsertDocumentoDetalheSequencia.append("NULL, "); //NUMEROPEDIDOSEPARACAO
        sqlInsertDocumentoDetalheSequencia.append("NULL, "); //DATAPEDIDOSEPARACAO
        sqlInsertDocumentoDetalheSequencia.append("NULL, "); //CODIGOEMPRESARELAC
        sqlInsertDocumentoDetalheSequencia.append("NULL, "); //TIPODOCUMENTORELAC
        sqlInsertDocumentoDetalheSequencia.append("NULL, "); //SERIEDOCUMENTORELAC
        sqlInsertDocumentoDetalheSequencia.append("NULL, "); //NUMERODOCUMENTORELAC
        sqlInsertDocumentoDetalheSequencia.append("NULL, "); //LOTERELAC
        sqlInsertDocumentoDetalheSequencia.append("4443824, "); //LOTE
        sqlInsertDocumentoDetalheSequencia.append("NULL, "); //FATORBASEICMS
        sqlInsertDocumentoDetalheSequencia.append("NULL, "); //ALIQUOTAICMSREDUCAO
        sqlInsertDocumentoDetalheSequencia.append("NULL, "); //FATORBASEIPI
        sqlInsertDocumentoDetalheSequencia.append("NULL, "); //ALIQUOTAIPIREDUCAO
        sqlInsertDocumentoDetalheSequencia.append("NULL, "); //VALORUNITARIOFISCAL
        sqlInsertDocumentoDetalheSequencia.append("NULL, "); //VALORSERVICO
        sqlInsertDocumentoDetalheSequencia.append("NULL, "); //OBSERVACAO
        sqlInsertDocumentoDetalheSequencia.append("NULL, "); //NFEINFORMACAOADICIONAL
        sqlInsertDocumentoDetalheSequencia.append("NULL, "); //PESOLIQUIDO
        sqlInsertDocumentoDetalheSequencia.append("NULL, "); //PESOBRUTO
        sqlInsertDocumentoDetalheSequencia.append("NULL, "); //CODIGOTRIBUTACAOMUNICIPAL
        sqlInsertDocumentoDetalheSequencia.append("NULL, "); //CODIGOLISTASERVICO
        sqlInsertDocumentoDetalheSequencia.append("NULL, "); //DESCRICAOLISTASERVICO
        sqlInsertDocumentoDetalheSequencia.append("NULL, "); //VALORICMS
        sqlInsertDocumentoDetalheSequencia.append("NULL, "); //VALORBASEICMS
        sqlInsertDocumentoDetalheSequencia.append("NULL, "); //VALORICMSST
        sqlInsertDocumentoDetalheSequencia.append("NULL, "); //VALORBASEICMSST
        sqlInsertDocumentoDetalheSequencia.append("NULL, "); //ALIQUOTAISS
        sqlInsertDocumentoDetalheSequencia.append("NULL, "); //VALORISS
        sqlInsertDocumentoDetalheSequencia.append("NULL, "); //SITTRIBUTARIAORIGEM
        sqlInsertDocumentoDetalheSequencia.append("NULL, "); //SITTRIBUTARIAICMS
        sqlInsertDocumentoDetalheSequencia.append("NULL, "); //VALORIPI
        sqlInsertDocumentoDetalheSequencia.append("NULL, "); //VALORBASEIPI
        sqlInsertDocumentoDetalheSequencia.append("NULL, "); //NATUREZAOPERACAOIMPRESSAO
        sqlInsertDocumentoDetalheSequencia.append("NULL, "); //SEQUENCIA
        sqlInsertDocumentoDetalheSequencia.append("NULL, "); //SEQUENCIARELAC
        sqlInsertDocumentoDetalheSequencia.append("NULL, "); //IDCLASSEEMPRESA
        sqlInsertDocumentoDetalheSequencia.append("NULL, "); // VALORFRETE
        sqlInsertDocumentoDetalheSequencia.append("NULL "); //VALORSEGURO
        sqlInsertDocumentoDetalheSequencia.append(")");

        return sqlInsertDocumentoDetalheSequencia.toString();
    }

    public static String getSqlInsertDocumentoEmbalagem(DocumentoEmbalagem documentoEmbalagem) {

        StringBuffer sqlInsertDocumentoEmbalagem = new StringBuffer();

        sqlInsertDocumentoEmbalagem.append("INSERT INTO DOCUMENTOEMBALAGEM ");
        sqlInsertDocumentoEmbalagem.append("( ");
        sqlInsertDocumentoEmbalagem.append("SEQUENCIAINTEGRACAO, ");
        sqlInsertDocumentoEmbalagem.append("SEQUENCIADOCUMENTO, ");
        sqlInsertDocumentoEmbalagem.append("SEQUENCIAEMBALAGEM, ");
        sqlInsertDocumentoEmbalagem.append("CODIGOEMBALAGEMEXPED, ");
        sqlInsertDocumentoEmbalagem.append("IDEMBALAGEMEXPED, ");
        sqlInsertDocumentoEmbalagem.append("TIPOEMBALAGEM, ");
        sqlInsertDocumentoEmbalagem.append("DESCRICAOTIPOEMBALAGEM, ");
        sqlInsertDocumentoEmbalagem.append("LARGURA, ");
        sqlInsertDocumentoEmbalagem.append("COMPRIMENTO, ");
        sqlInsertDocumentoEmbalagem.append("ALTURA, ");
        sqlInsertDocumentoEmbalagem.append("PESOTARA, ");
        sqlInsertDocumentoEmbalagem.append("CODIGOPRODUTO, ");
        sqlInsertDocumentoEmbalagem.append("TIPOUC, ");
        sqlInsertDocumentoEmbalagem.append("FATORTIPOUC, ");
        sqlInsertDocumentoEmbalagem.append("QUANTIDADE, ");
        sqlInsertDocumentoEmbalagem.append("CLASSEPRODUTO, ");
        sqlInsertDocumentoEmbalagem.append("LOTEFABRICACAO, ");
        sqlInsertDocumentoEmbalagem.append("DATAFABRICACAO, ");
        sqlInsertDocumentoEmbalagem.append("DATAVENCIMENTO, ");
        sqlInsertDocumentoEmbalagem.append("LOTEGERAL ");
        sqlInsertDocumentoEmbalagem.append(") VALUES ( ");
        sqlInsertDocumentoEmbalagem.append("SEQ_INTEGRACAO.nextval, "); //SEQUENCIAINTEGRACAO
        sqlInsertDocumentoEmbalagem.append("1, "); //SEQUENCIADOCUMENTO
        sqlInsertDocumentoEmbalagem.append("1, "); //SEQUENCIAEMBALAGEM
        sqlInsertDocumentoEmbalagem.append("6781596, "); //CODIGOEMBALAGEMEXPED
        sqlInsertDocumentoEmbalagem.append("'EMB0006781441', "); //IDEMBALAGEMEXPED
        sqlInsertDocumentoEmbalagem.append("1, "); //TIPOEMBALAGEM
        sqlInsertDocumentoEmbalagem.append("'PACK LOJAS AVENIDA', "); //DESCRICAOTIPOEMBALAGEM
        sqlInsertDocumentoEmbalagem.append("0.3, "); //LARGURA
        sqlInsertDocumentoEmbalagem.append("0.3, "); //COMPRIMENTO
        sqlInsertDocumentoEmbalagem.append("0.3, "); //ALTURA
        sqlInsertDocumentoEmbalagem.append("0, "); //PESOTARA
        sqlInsertDocumentoEmbalagem.append("'147748013872', "); //CODIGOPRODUTO
        sqlInsertDocumentoEmbalagem.append("'UN', "); //TIPOUC
        sqlInsertDocumentoEmbalagem.append("1, "); //FATORTIPOUC
        sqlInsertDocumentoEmbalagem.append("1, "); //QUANTIDADE
        sqlInsertDocumentoEmbalagem.append("'OK', "); //CLASSEPRODUTO
        sqlInsertDocumentoEmbalagem.append("NULL, "); //LOTEFABRICACAO
        sqlInsertDocumentoEmbalagem.append("NULL, "); //DATAFABRICACAO
        sqlInsertDocumentoEmbalagem.append("NULL, "); //DATAVENCIMENTO
        sqlInsertDocumentoEmbalagem.append("NULL "); //LOTEGERAL
        sqlInsertDocumentoEmbalagem.append(")");

        return sqlInsertDocumentoEmbalagem.toString();

    }

    public static String getSqlInsertIntegracaoHistorico(IntegracaoHistorico integracaoHistorico) {

        StringBuffer sqlInsertIntegracaoHistorico = new StringBuffer();
        sqlInsertIntegracaoHistorico.append("INSERT INTO INTEGRACAOHISTORICO ");
        sqlInsertIntegracaoHistorico.append("( ");
        sqlInsertIntegracaoHistorico.append("SEQUENCIAINTEGRACAO, ");
        sqlInsertIntegracaoHistorico.append("SEQUENCIAHISTORICO, ");
        sqlInsertIntegracaoHistorico.append("ERROINTEGRACAO, ");
        sqlInsertIntegracaoHistorico.append("DATAPROCESSAMENTO, ");
        sqlInsertIntegracaoHistorico.append("DETALHEHISTORICO, ");
        sqlInsertIntegracaoHistorico.append("ERROORACLECODE, ");
        sqlInsertIntegracaoHistorico.append("ERROORACLEMSG, ");
        sqlInsertIntegracaoHistorico.append("USUARIO, ");
        sqlInsertIntegracaoHistorico.append("DATAATUALIZACAO, ");
        sqlInsertIntegracaoHistorico.append("ESTADOINTEGRACAO, ");
        sqlInsertIntegracaoHistorico.append("JUSTIFICATIVA, ");
        sqlInsertIntegracaoHistorico.append("ESTADOANTERIOR ");
        sqlInsertIntegracaoHistorico.append(") VALUES ( ");
        sqlInsertIntegracaoHistorico.append("SEQ_INTEGRACAO.nextval, "); // SEQUENCIAINTEGRACAO
        sqlInsertIntegracaoHistorico.append("1, "); // SEQUENCIAHISTORICO
        sqlInsertIntegracaoHistorico.append("NULL, "); // ERROINTEGRACAO
        sqlInsertIntegracaoHistorico.append("TO_DATE('06/08/2018 10:12:37', 'DD/MM/YYYY HH24:MI:SS'), "); // DATAPROCESSAMENTO
        sqlInsertIntegracaoHistorico.append("'Pendente.', "); // DETALHEHISTORICO
        sqlInsertIntegracaoHistorico.append("NULL, "); // ERROORACLECODE
        sqlInsertIntegracaoHistorico.append("NULL, "); // ERROORACLEMSG
        sqlInsertIntegracaoHistorico.append("'ORAINT', "); // USUARIO
        sqlInsertIntegracaoHistorico.append("TO_DATE('06/08/2018 10:12:37', 'DD/MM/YYYY HH24:MI:SS'), "); // DATAATUALIZACAO
        sqlInsertIntegracaoHistorico.append("1, "); // ESTADOINTEGRACAO
        sqlInsertIntegracaoHistorico.append("NULL, "); // JUSTIFICATIVA
        sqlInsertIntegracaoHistorico.append("NULL "); // ESTADOANTERIOR
        sqlInsertIntegracaoHistorico.append(")");

        return sqlInsertIntegracaoHistorico.toString();

    }

    public static String getSqlInsertRomaneio(Romaneio romaneio) {

        StringBuffer  sqlInsertRomaneio = new StringBuffer();
        sqlInsertRomaneio.append("INSERT INTO ROMANEIO ");
        sqlInsertRomaneio.append("( ");
        sqlInsertRomaneio.append("SEQUENCIAINTEGRACAO, ");
        sqlInsertRomaneio.append("SEQUENCIAROMANEIO, ");
        sqlInsertRomaneio.append("TIPOINTEGRACAO, ");
        sqlInsertRomaneio.append("NUMEROROMANEIO, ");
        sqlInsertRomaneio.append("DOCUMENTOQUALIDADE, ");
        sqlInsertRomaneio.append("ENDERECODOCA, ");
        sqlInsertRomaneio.append("DATAPREVISAOMOVIMENTO, ");
        sqlInsertRomaneio.append("TEMPOPREVISTOOPERACAO, ");
        sqlInsertRomaneio.append("OBSERVACAO, ");
        sqlInsertRomaneio.append("CODIGOPROCEDENCIA, ");
        sqlInsertRomaneio.append("CNPJCPFPROCEDENCIA, ");
        sqlInsertRomaneio.append("NOMEPROCEDENCIA, ");
        sqlInsertRomaneio.append("ENDERECOPROCEDENCIA, ");
        sqlInsertRomaneio.append("BAIRROPROCEDENCIA, ");
        sqlInsertRomaneio.append("MUNICIPIOPROCEDENCIA, ");
        sqlInsertRomaneio.append("UFPROCEDENCIA, ");
        sqlInsertRomaneio.append("CEPPROCEDENCIA, ");
        sqlInsertRomaneio.append("INSCRICAOPROCEDENCIA, ");
        sqlInsertRomaneio.append("TIPOPROCEDENCIA, ");
        sqlInsertRomaneio.append("TIPOPESSOAPROCEDENCIA, ");
        sqlInsertRomaneio.append("CODIGOTRANSPORTADORA, ");
        sqlInsertRomaneio.append("CNPJCPFTRANSPORTADORA, ");
        sqlInsertRomaneio.append("NOMETRANSPORTADORA, ");
        sqlInsertRomaneio.append("ENDERECOTRANSPORTADORA, ");
        sqlInsertRomaneio.append("BAIRROTRANSPORTADORA, ");
        sqlInsertRomaneio.append("MUNICIPIOTRANSPORTADORA, ");
        sqlInsertRomaneio.append("UFTRANSPORTADORA, ");
        sqlInsertRomaneio.append("CEPTRANSPORTADORA, ");
        sqlInsertRomaneio.append("INSCRICAOTRANSPORTADORA, ");
        sqlInsertRomaneio.append("TIPOTRANSPORTADORA, ");
        sqlInsertRomaneio.append("TIPOPESSOATRANSPORTADORA, ");
        sqlInsertRomaneio.append("PLACAVEICULO, ");
        sqlInsertRomaneio.append("DESCRICAOVEICULO, ");
        sqlInsertRomaneio.append("NUMEROCERTIFICADO, ");
        sqlInsertRomaneio.append("NUMEROCHASSI, ");
        sqlInsertRomaneio.append("NUMERORENAVAM, ");
        sqlInsertRomaneio.append("MUNICIPIOVEICULO, ");
        sqlInsertRomaneio.append("UFVEICULO, ");
        sqlInsertRomaneio.append("ANOVEICULO, ");
        sqlInsertRomaneio.append("IDENTIFICADORVEICULO, ");
        sqlInsertRomaneio.append("TIPOVEICULO, ");
        sqlInsertRomaneio.append("DESCRICAOTIPOVEICULO, ");
        sqlInsertRomaneio.append("MARCAMODELOVEICULO, ");
        sqlInsertRomaneio.append("DESCRICAOMARCAMODELOVEICULO, ");
        sqlInsertRomaneio.append("IDMARCAMODELOVEICULO, ");
        sqlInsertRomaneio.append("CORVEICULO, ");
        sqlInsertRomaneio.append("DESCRICAOCORVEICULO, ");
        sqlInsertRomaneio.append("IDCORVEICULO, ");
        sqlInsertRomaneio.append("CODIGOCOMBUSTIVEL, ");
        sqlInsertRomaneio.append("DESCRICAOCOMBUSTIVEL, ");
        sqlInsertRomaneio.append("IDCOMBUSTIVEL, ");
        sqlInsertRomaneio.append("NUMEROCRACHA, ");
        sqlInsertRomaneio.append("CPFPESSOA, ");
        sqlInsertRomaneio.append("NOMEPESSOA, ");
        sqlInsertRomaneio.append("RGPESSOA, ");
        sqlInsertRomaneio.append("MUNICIPIOPESSOA, ");
        sqlInsertRomaneio.append("UFPESSOA, ");
        sqlInsertRomaneio.append("CNHPESSOA, ");
        sqlInsertRomaneio.append("DATAVENCIMENTOCNH, ");
        sqlInsertRomaneio.append("NUMEROMOPP, ");
        sqlInsertRomaneio.append("DATAVENCIMENTOMOPP, ");
        sqlInsertRomaneio.append("CODIGOROMANEIO, ");
        sqlInsertRomaneio.append("CODIGOESTABELECIMENTO, ");
        sqlInsertRomaneio.append("TIPOPICKING ");
        sqlInsertRomaneio.append(") VALUES ( ");
        sqlInsertRomaneio.append("SEQ_INTEGRACAO.nextval, "); // SEQUENCIAINTEGRACAO
        sqlInsertRomaneio.append("1, "); // SEQUENCIAROMANEIO
        sqlInsertRomaneio.append("252, "); // TIPOINTEGRACAO
        sqlInsertRomaneio.append("'059', "); // NUMEROROMANEIO
        sqlInsertRomaneio.append("NULL, "); // DOCUMENTOQUALIDADE
        sqlInsertRomaneio.append("'DC10', "); // ENDERECODOCA
        sqlInsertRomaneio.append("NULL, "); // DATAPREVISAOMOVIMENTO
        sqlInsertRomaneio.append("NULL, "); // TEMPOPREVISTOOPERACAO
        sqlInsertRomaneio.append("NULL, "); // OBSERVACAO
        sqlInsertRomaneio.append("NULL, "); // CODIGOPROCEDENCIA
        sqlInsertRomaneio.append("NULL, "); // CNPJCPFPROCEDENCIA
        sqlInsertRomaneio.append("NULL, "); // NOMEPROCEDENCIA
        sqlInsertRomaneio.append("NULL, "); // ENDERECOPROCEDENCIA
        sqlInsertRomaneio.append("NULL, "); // BAIRROPROCEDENCIA
        sqlInsertRomaneio.append("NULL, "); // MUNICIPIOPROCEDENCIA
        sqlInsertRomaneio.append("NULL, "); // UFPROCEDENCIA
        sqlInsertRomaneio.append("NULL, "); // CEPPROCEDENCIA
        sqlInsertRomaneio.append("NULL, "); // INSCRICAOPROCEDENCIA
        sqlInsertRomaneio.append("NULL, "); // TIPOPROCEDENCIA
        sqlInsertRomaneio.append("NULL, "); // TIPOPESSOAPROCEDENCIA
        sqlInsertRomaneio.append("NULL, "); // CODIGOTRANSPORTADORA
        sqlInsertRomaneio.append("NULL, "); // CNPJCPFTRANSPORTADORA
        sqlInsertRomaneio.append("NULL, "); // NOMETRANSPORTADORA
        sqlInsertRomaneio.append("NULL, "); // ENDERECOTRANSPORTADORA
        sqlInsertRomaneio.append("NULL, "); // BAIRROTRANSPORTADORA
        sqlInsertRomaneio.append("NULL, "); // MUNICIPIOTRANSPORTADORA
        sqlInsertRomaneio.append("NULL, "); // UFTRANSPORTADORA
        sqlInsertRomaneio.append("NULL, "); // CEPTRANSPORTADORA
        sqlInsertRomaneio.append("NULL, "); // INSCRICAOTRANSPORTADORA
        sqlInsertRomaneio.append("NULL, "); // TIPOTRANSPORTADORA
        sqlInsertRomaneio.append("NULL, "); // TIPOPESSOATRANSPORTADORA
        sqlInsertRomaneio.append("NULL, "); // PLACAVEICULO
        sqlInsertRomaneio.append("NULL, "); // DESCRICAOVEICULO
        sqlInsertRomaneio.append("NULL, "); // NUMEROCERTIFICADO
        sqlInsertRomaneio.append("NULL, "); // NUMEROCHASSI
        sqlInsertRomaneio.append("NULL, "); // NUMERORENAVAM
        sqlInsertRomaneio.append("NULL, "); // MUNICIPIOVEICULO
        sqlInsertRomaneio.append("NULL, "); // UFVEICULO
        sqlInsertRomaneio.append("NULL, "); // ANOVEICULO
        sqlInsertRomaneio.append("NULL, "); // IDENTIFICADORVEICULO
        sqlInsertRomaneio.append("NULL, "); // TIPOVEICULO
        sqlInsertRomaneio.append("NULL, "); // DESCRICAOTIPOVEICULO
        sqlInsertRomaneio.append("NULL, "); // MARCAMODELOVEICULO
        sqlInsertRomaneio.append("NULL, "); // DESCRICAOMARCAMODELOVEICULO
        sqlInsertRomaneio.append("NULL, "); // IDMARCAMODELOVEICULO
        sqlInsertRomaneio.append("NULL, "); // CORVEICULO
        sqlInsertRomaneio.append("NULL, "); // DESCRICAOCORVEICULO
        sqlInsertRomaneio.append("NULL, "); // IDCORVEICULO
        sqlInsertRomaneio.append("NULL, "); // CODIGOCOMBUSTIVEL
        sqlInsertRomaneio.append("NULL, "); // DESCRICAOCOMBUSTIVEL
        sqlInsertRomaneio.append("NULL, "); // IDCOMBUSTIVEL
        sqlInsertRomaneio.append("NULL, "); // NUMEROCRACHA
        sqlInsertRomaneio.append("NULL, "); // CPFPESSOA
        sqlInsertRomaneio.append("NULL, "); // NOMEPESSOA
        sqlInsertRomaneio.append("NULL, "); // RGPESSOA
        sqlInsertRomaneio.append("NULL, "); // MUNICIPIOPESSOA
        sqlInsertRomaneio.append("NULL, "); // UFPESSOA
        sqlInsertRomaneio.append("NULL, "); // CNHPESSOA
        sqlInsertRomaneio.append("NULL, "); // DATAVENCIMENTOCNH
        sqlInsertRomaneio.append("NULL, "); // NUMEROMOPP
        sqlInsertRomaneio.append("NULL, "); // DATAVENCIMENTOMOPP
        sqlInsertRomaneio.append("3875018, "); // CODIGOROMANEIO
        sqlInsertRomaneio.append("1, "); // CODIGOESTABELECIMENTO
        sqlInsertRomaneio.append("1 "); // TIPOPICKING
        sqlInsertRomaneio.append(")");

        return sqlInsertRomaneio.toString();

    }

}