package br.com.storeautomacao.sync.util;

public final class Constantes {

    private Constantes() {

    }

    // Coluna da tabela ESTADOINTEGRACAO
    public static final int ESTADOINTEGRACAO_EXPORTANDO = 6;
    public static final int ESTADOINTEGRACAO_REALIZADA_2 = 2;

}
