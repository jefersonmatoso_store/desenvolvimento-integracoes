package br.com.storeautomacao.sync.util;

import br.com.storeautomacao.sync.model.enuns.UnitOfLengthEnum;

/**
 * Created by alexandre on 09/08/17.
 */
public class GeoLocationUtil {

    private static final int EARTH_MIDDLE_RADUIUS_IN_METERS = 6372000;
    private static final int EARTH_MIDDLE_RADUIUS_IN_MILES = 3961;
    private static final int EARTH_MIDDLE_RADUIUS_IN_KILOMETERS = 6372;

    /**
     * Retorna a distancia, utilizando a <i>Lei dos Cossenos</i> para triangulos.
     * <br>
     * @see <a href="http://dan-scientia.blogspot.com.br/2009/05/distancia-entre-coordenadas-geograficas.html">http://dan-scientia.blogspot.com.br/2009/05/distancia-entre-coordenadas-geograficas.html</a>
     *
     * @param latitudeX
     * @param longitudeX
     * @param latitudeY
     * @param longitudeY
     * @param unidadeMedida
     *
     * @return
     */
    public static double distanceBetween(double latitudeX, double longitudeX,
                                         double latitudeY, double longitudeY, UnitOfLengthEnum unidadeMedida) {

        double distance = distanceBetween(latitudeX, longitudeX, latitudeY, longitudeY);

        switch (unidadeMedida) {
            case MILE:
                distance *= EARTH_MIDDLE_RADUIUS_IN_MILES;
                break;
            case KILOMETER:
                distance *= EARTH_MIDDLE_RADUIUS_IN_KILOMETERS;
                break;
            case METER:
            default:
                distance *= EARTH_MIDDLE_RADUIUS_IN_METERS;
                break;
        }

        return distance;
    }

    /**
     * Retorna a distancia, utilizando a <i>Lei dos Cossenos</i> para triangulos.
     * <br>
     * @see <a href="http://dan-scientia.blogspot.com.br/2009/05/distancia-entre-coordenadas-geograficas.html">http://dan-scientia.blogspot.com.br/2009/05/distancia-entre-coordenadas-geograficas.html</a>
     *
     * @param latitudeX
     * @param longitudeX
     * @param latitudeY
     * @param longitudeY
     *
     * @return
     */
    public static double distanceBetween(double latitudeX, double longitudeX,
                                         double latitudeY, double longitudeY) {

        final double radLatitudeX = Math.toRadians(90 - latitudeX);
        final double radLongitudeX = Math.toRadians(360 + longitudeX);
        final double radLatitudeY = Math.toRadians(90 - latitudeY);
        final double radLongitudeY = Math.toRadians(360 + longitudeY);

        double distance = (Math.acos(
                Math.cos(radLatitudeX)
                        * Math.cos(radLatitudeY)
                        + Math.sin(radLatitudeX)
                        * Math.sin(radLatitudeY)
                        * Math.cos(Math.abs(radLongitudeX - radLongitudeY))
        ));

        return distance;
    }

    /**
     * Retorna a distancia em metros, utilizando a <i>Lei dos Cossenos</i> para triangulos.
     * <br>
     * @see <a href="http://dan-scientia.blogspot.com.br/2009/05/distancia-entre-coordenadas-geograficas.html">http://dan-scientia.blogspot.com.br/2009/05/distancia-entre-coordenadas-geograficas.html</a>
     *
     * @param pontoA
     * @param pontoB
     *
     * @return
     */
    public static double distanceBetween(double[] pontoA, double[] pontoB) {
        return distanceBetween(pontoA[0], pontoA[1], pontoB[0], pontoB[1]);
    }

    /**
     * Retorna a distancia em metros, utilizando a <i>Lei dos Cossenos</i> para triangulos.
     * <br>
     * @see <a href="http://dan-scientia.blogspot.com.br/2009/05/distancia-entre-coordenadas-geograficas.html">http://dan-scientia.blogspot.com.br/2009/05/distancia-entre-coordenadas-geograficas.html</a>
     *
     * @param pontoA
     * @param pontoB
     * @param unidadeMedida
     *
     * @return
     */
    public static double distanceBetween(double[] pontoA, double[] pontoB, UnitOfLengthEnum unidadeMedida) {
        return distanceBetween(pontoA[0], pontoA[1], pontoB[0], pontoB[1], unidadeMedida);
    }

}
