package br.com.storeautomacao.sync.util;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.chrono.ChronoLocalDateTime;
import java.util.Calendar;
import java.util.Date;

public final class DateUtil {    


    public static Date getDate(String dateStr) {
        final DateFormat formatter = new SimpleDateFormat("yyyy-M-d HH:mm:ss");
        try {
            formatter.parse(dateStr);
            formatter.getCalendar().getTime();
            formatter.getCalendar().add(Calendar.HOUR, -3);
            return formatter.getCalendar().getTime();
            
        } catch (Exception e) {                
            return null;
        }
    }
    
    public static Date getStartDate(Date date) {
        final DateFormat formatter = new SimpleDateFormat("yyyy-M-d 00:00:00");
        try {
             return  getDate(formatter.format(date));
        } catch (Exception e) {                
            return null;
        }
    }
    
    public static Date getEndDate(Date date) {
        final DateFormat formatter = new SimpleDateFormat("yyyy-M-d 23:59:59");
        try {
        	return getDate(formatter.format(date));
        } catch (Exception e) {                
            return null;
        }
    }

    /**
     *
     *
     */


    private static final String dateTimezone = "yyyy-MM-dd'T'HH:mm:ss";

    /**
     * Formato: dd/MM/yyyy.
     */
    private static final String dd_MM_yyyy = "dd/MM/yyyy";

    /**
     * Formato: dd/MM/yyyy HH:mm:ss.
     */
    private static final String dd_MM_yyyy_HH_mm_ss = dd_MM_yyyy + " HH:mm:ss";

    /**
     * Formato: HH:mm:ss,sss.
     */
    private static final String HH_mm_ss_sss = "hh:mm:ss,SSS";

    /**
     * Formato: yyyyMMdd.
     */
    private static final String yyyyMMdd = "yyyyMMdd";

    /**
     * Formato: yyyyMMddHHmm.
     */
    private static final String yyyyMMddHHmm = "yyyyMMddHHmm";

    /**
     * Formato: yyyyMMddHHmmss.
     */
    private static final String yyyyMMddHHmmss = "yyyyMMddHHmmss";

    /**
     * Formato: dd/MM/yyyy HH:mm
     */
    private static final String ddMMyyyyHHmm = "dd/MM/yyyy HH:mm";

    /**
     * Formato: dd/MM/yyyy HH:mm:ss,sss.
     */
    private static final String dd_MM_yyyy_HH_mm_ss_sss = dd_MM_yyyy + " " + HH_mm_ss_sss;

    /**
     * O formatador com o padrão ptBr.
     */
    private final SimpleDateFormat ptBrFormat = new SimpleDateFormat(dd_MM_yyyy);

    /**
     * O formatador com o padrão ptBr.
     */
    private final SimpleDateFormat ptBrComHoraFormat = new SimpleDateFormat(dd_MM_yyyy_HH_mm_ss);

    /**
     * O formatador com o padrão ptBr.
     */
    private final SimpleDateFormat ptBrComHoraMiliFormat = new SimpleDateFormat(
            dd_MM_yyyy_HH_mm_ss_sss);

    /**
     * O formatador com o padrão ptBr.
     */
    private final SimpleDateFormat timeZonePicked = new SimpleDateFormat(dateTimezone);

    /**
     * Formatador com o padrão de Log.
     */
    private final SimpleDateFormat logFormat = new SimpleDateFormat("HHmmss");

    /**
     * DateUtil.
     */
    private static final DateUtil instance;

    static {
        instance = new DateUtil();
    }

    /**
     * Construtor privado para evitar a instanciação indevida da classe.
     */
    private DateUtil() {}

    /**
     * Transforma um <code>Date</code> em <code>String</code>.
     *
     * @param date o <code>Date</code> a ser transformada em <code>String</code>.
     * @return a <code>String</code> correspondente ao <code>Date</code>.
     */
    public static String dateToPtBrString(final Date date) {
        if (date == null || instance == null) {
            return null;
        }
        return instance.ptBrFormat.format(date);
    }

    /**
     * Transforma um <code>Date</code> em <code>String</code>.
     *
     * @param date o <code>Date</code> a ser transformada em <code>String</code>.
     * @return a <code>String</code> correspondente ao <code>Date</code>.
     */
    public static String dateToPtBrComHoraString(final Date date) {
        if (date == null || instance == null) {
            return null;
        }
        return instance.ptBrComHoraFormat.format(date);
    }

    /**
     * Transforma um <code>Date</code> em <code>String</code>.
     *
     * @param date
     * @return a <code>String</code> correspondente ao <code>Date</code> em formato dd/MM/yyyy
     *         HH:mm:ss,sss.
     */
    public static String dateToPtBrComHoraMiliString(final Date date) {
        if (date == null || instance == null) {
            return null;
        }
        return instance.ptBrComHoraMiliFormat.format(date);
    }

    /**
     * Transforma um <code>Date</code> em <code>String</code>.
     *
     * @param date
     * @return a <code>String</code> correspondente ao <code>Date</code> em formato dd/MM/yyyy
     *         HH:mm:ss,sss.
     */
    public static String dateToTimeZoneStringPicked(final Date date) {
        if (date == null || instance == null) {
            return null;
        }
        return instance.timeZonePicked.format(date);
    }

    /**
     * Converte String com no formato dd/MM/yyyy em Date.
     *
     * @param string
     * @return
     * @throws ParseException
     */
    public static Date stringToPtBrDate(final String string) throws ParseException {
        if (string == null || instance == null) {
            return null;
        }
        return instance.ptBrFormat.parse(string);
    }

    /**
     * Converte String com no formato dd/MM/yyyy HH:mm:ss em Date.
     *
     * @param source
     * @return
     * @throws ParseException
     */
    public static Date stringPtBrComHoraToDate(final String source) throws ParseException {
        if (source == null || instance == null) {
            return null;
        }
        return instance.ptBrComHoraFormat.parse(source);
    }

    /**
     * Transforma um <code>Date</code> em <code>String</code>.
     *
     * @param date
     * @return <code>String</code> no <i>pattern</i> definido para log.
     */
    public static String dateToLogTime(final Date date) {
        if (date == null || instance == null) {
            return null;
        }
        return instance.logFormat.format(date);
    }


    /**
     * Compara o dia, mês e ano de duas datas.
     *
     * @param data1
     * @param data2
     * @return
     */
    public static int compararDatasSemHoras(final Date data1, final Date data2) {
        final SimpleDateFormat format = new SimpleDateFormat();
        format.applyPattern(yyyyMMdd);
        final String dataInicio = format.format(data1);
        final String dataFim = format.format(data2);

        return dataInicio.compareTo(dataFim);
    }

    /**
     * Compara duas datas completas (ano, mês, dia, hora e minuto).
     *
     * @param data1
     * @param data2
     * @return
     */
    public static int compararDatasComHoras(final Date data1, final Date data2) {
        final SimpleDateFormat format = new SimpleDateFormat();
        format.applyPattern(yyyyMMddHHmm);

        final String dataInicio = format.format(data1);
        final String dataFim = format.format(data2);

        return dataInicio.compareTo(dataFim);
    }

    /**
     * Compara duas datas completas (ano, mês, dia, hora, minuto e segundo).
     *
     * @param data1
     * @param data2
     * @return
     */
    public static int compararDatasComSegundos(final Date data1, final Date data2) {
        final SimpleDateFormat format = new SimpleDateFormat();
        format.applyPattern(yyyyMMddHHmmss);

        final String dataInicio = format.format(data1);
        final String dataFim = format.format(data2);

        return dataInicio.compareTo(dataFim);
    }

    /**
     * Formata o intervalo entre dataInicio e dataTermino em String. Exemplos: 01/01/2011 a
     * 02/01/2011 - Saída: 01 a 02/Jan 01/01/2011 a 01/01/2011 - Saída: 01/Jan 01/01/2011 a
     * 01/02/2011 - Saída: 01/Jan a 01/Fev.
     *
     * @param dataInicio
     * @param dataTermino
     * @return
     */
    public static String getDataInicioATermino(final Date dataInicio, final Date dataTermino) {
        String data;
        final SimpleDateFormat formatador = new SimpleDateFormat();
        final String ddMM = "dd/MM";
        final String entre = " a ";

        final Calendar inicio = Calendar.getInstance();
        inicio.setTime(dataInicio);
        final Calendar termino = Calendar.getInstance();
        termino.setTime(dataTermino);

        if (inicio.getTime().compareTo(termino.getTime()) == 0) {
            formatador.applyPattern(ddMM);
            data = formatador.format(dataInicio.getTime());
            return data;
        } else if (inicio.get(Calendar.MONTH) == termino.get(Calendar.MONTH)) {
            formatador.applyPattern("dd");
            data = formatador.format(dataInicio.getTime()) + entre;
            formatador.applyPattern(ddMM);
            data += formatador.format(dataTermino.getTime());
            return data;
        } else {
            formatador.applyPattern(ddMM);
            data =
                    formatador.format(dataInicio.getTime()) + entre
                            + formatador.format(dataTermino.getTime());
            return data;
        }
    }

    public static String getDateAsString(final Date data) {
        final SimpleDateFormat sdf = new SimpleDateFormat(ddMMyyyyHHmm);
        return sdf.format(data);
    }

    public static Date convertToDate(ChronoLocalDateTime temporal) {
        if (temporal == null) {
            return null;
        }

        return Date.from(temporal.atZone(ZoneId.systemDefault()).toInstant());
    }

    public static LocalDate converterToLocalDate(String strYearMonthDay) {
        LocalDate dl = null;
        try {
            dl = LocalDate.parse(strYearMonthDay);
        } catch (Exception e) {
            dl = null;
        }

        return dl;
    }

    public static String converterDate(Timestamp timestamp) {
        return new SimpleDateFormat("dd/MM/yyyy").format(timestamp);
    }

    public static String converterDate(Date date) {
        return new SimpleDateFormat("dd/MM/yyyy").format(date);
    }


}