package br.com.storeautomacao.sync.util;

import org.apache.commons.lang3.StringUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by alexandre on 22/08/17.
 */
public class UserAgentUtil {

    private static final Pattern MOBILE_USER_AGENT_PATTERN =
            Pattern.compile("(mobile|android|iphone|ipad)", Pattern.CASE_INSENSITIVE);

    private static final Pattern WEB_USER_AGENT_DEVELOP_PATTERN =
            Pattern.compile("(postman)", Pattern.CASE_INSENSITIVE);

    private static final Pattern ANDROID_USER_AGENT_PATTERN =
            Pattern.compile("(android)", Pattern.CASE_INSENSITIVE);

    private static final Pattern ANDROID_DEVELOP_USER_AGENT_PATTERN =
            Pattern.compile("(okhttp)", Pattern.CASE_INSENSITIVE);

    private static final Pattern IOS_USER_AGENT_PATTERN =
            Pattern.compile("(iphone|ipad)", Pattern.CASE_INSENSITIVE);

    /**
     * Verifica se o <i>User Agent</i> informado não é de um dispositivo <i>Mobile</i>.
     *
     * @param userAgent
     * @return
     */
    public static boolean isNotMobileDevice(String userAgent) {
        return !isMobileDevice(userAgent);
    }

    /**
     * Verifica se o <i>User Agent</i> informado e' de um dispositivo <i>Mobile</i>.
     *
     * @param userAgent
     * @return
     */
    public static boolean isMobileDevice(String userAgent) {
        boolean isMobile = false;
        if (StringUtils.isNotEmpty(userAgent)) {
            isMobile = isMatch(MOBILE_USER_AGENT_PATTERN, userAgent)
                    || isMatch(ANDROID_DEVELOP_USER_AGENT_PATTERN, userAgent);
        }

        return isMobile;
    }

    /**
     * Verifica se o <i>User Agent</i> informado e' de um dispositivo <i>Android</i>.
     *
     * @param userAgent
     * @return
     */
    public static boolean isAndroidDevice(String userAgent) {
        boolean isMobile = false;
        if (StringUtils.isNotEmpty(userAgent)) {
            isMobile = isMatch(ANDROID_USER_AGENT_PATTERN, userAgent)
                    || isMatch(ANDROID_DEVELOP_USER_AGENT_PATTERN, userAgent);
        }

        return isMobile;
    }

    /**
     * Verifica se o <i>User Agent</i> informado e' de um dispositivo <i>iOS</i>.
     *
     * @param userAgent
     * @return
     */
    public static boolean isIOSDevice(String userAgent) {
        boolean isMobile = false;
        if (StringUtils.isNotEmpty(userAgent)) {
            isMobile = isMatch(IOS_USER_AGENT_PATTERN, userAgent);
        }

        return isMobile;
    }

    private static boolean isMatch(Pattern pattern, String str) {
        Matcher matcher = pattern.matcher(str);
        return matcher.find();
    }
}
