package br.com.storeautomacao.sync.util;

import br.com.storeautomacao.sync.dto.ItemProdutoDto;
import br.com.storeautomacao.sync.dto.SaleDto;
import br.com.storeautomacao.sync.model.entity.*;
import br.com.storeautomacao.sync.model.wms.Appointment.Appointment;
import br.com.storeautomacao.sync.model.wms.Appointment.LgfDataAppointment;
import br.com.storeautomacao.sync.model.wms.Appointment.ListOfAppointments;
import br.com.storeautomacao.sync.model.wms.*;
import br.com.storeautomacao.sync.model.wms.IbShipment.*;
import br.com.storeautomacao.sync.model.wms.Inventory.Inventory;
import br.com.storeautomacao.sync.model.wms.Inventory.LgfDataInventory;
import br.com.storeautomacao.sync.model.wms.Inventory.ListOfInventory;
import br.com.storeautomacao.sync.model.wms.Item;
import br.com.storeautomacao.sync.model.wms.Order;
import br.com.storeautomacao.sync.model.wms.PurchaseOrder.ListOfPurchaseOrders;
import br.com.storeautomacao.sync.model.wms.PurchaseOrder.PurchaseOrder;
import br.com.storeautomacao.sync.model.wms.Sales.LgfDataSales;
import br.com.storeautomacao.sync.model.wms.Sales.ListOfPointOfSales;
import br.com.storeautomacao.sync.model.wms.Sales.PointOfSale;
import br.com.storeautomacao.sync.model.wms.Shipment.*;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.naming.NoNameCoder;
import com.thoughtworks.xstream.io.xml.DomDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class XmlUtil {

    static Logger logger = LoggerFactory.getLogger(XmlUtil.class);

    public static String getXmlBody(String item_alternate_code, String part_a, String description, String barcode, String unit_cost, String unit_length, String unit_width) {

        String xmlString = "<?xml version=\"1.0\" encoding=\"utf-8\"?><LgfData> <Header> <DocumentVersion>9.0.0</DocumentVersion> <OriginSystem>JEF</OriginSystem> <ClientEnvCode>JEF</ClientEnvCode> <ParentCompanyCode>C01</ParentCompanyCode> <Entity>item</Entity> <TimeStamp>2012-12-13T12:12:12</TimeStamp> <MessageId>123</MessageId> </Header> <ListOfItems> <item> <company_code>1</company_code> <item_alternate_code>" + item_alternate_code + "</item_alternate_code> <part_a>" + part_a + "</part_a> <part_b></part_b> <part_c></part_c> <part_d></part_d> <part_e></part_e> <part_f></part_f> <pre_pack_code></pre_pack_code> <action_code>UPDATE</action_code> <description>" + description + "</description> <barcode>" + barcode + "</barcode> <unit_cost>" + unit_cost + "</unit_cost> <unit_length>" + unit_length + "</unit_length> <unit_width>" + unit_width + "</unit_width> <unit_height></unit_height> <unit_weight></unit_weight> <unit_volume></unit_volume> <hazmat></hazmat> <recv_type></recv_type> <ob_lpn_type></ob_lpn_type> <catch_weight_method></catch_weight_method> <order_consolidation_attr></order_consolidation_attr> <season_code></season_code> <brand_code></brand_code> <cust_attr_1></cust_attr_1> <cust_attr_2></cust_attr_2> <retail_price></retail_price> <net_cost></net_cost> <currency_code></currency_code> <std_pack_qty></std_pack_qty> <std_pack_length></std_pack_length> <std_pack_width></std_pack_width> <std_pack_height></std_pack_height> <std_pack_weight></std_pack_weight> <std_pack_volume></std_pack_volume> <std_case_qty></std_case_qty> <max_case_qty></max_case_qty> <std_case_length></std_case_length> <std_case_width></std_case_width> <std_case_height></std_case_height> <std_case_weight></std_case_weight> <std_case_volume></std_case_volume> <dimension1></dimension1> <dimension2></dimension2> <dimension3></dimension3> <hierarchy1_code></hierarchy1_code> <hierarchy1_description></hierarchy1_description> <hierarchy2_code></hierarchy2_code> <hierarchy2_description></hierarchy2_description> <hierarchy3_code></hierarchy3_code> <hierarchy3_description></hierarchy3_description> <hierarchy4_code></hierarchy4_code> <hierarchy4_description></hierarchy4_description> <hierarchy5_code></hierarchy5_code> <hierarchy5_description></hierarchy5_description> <group_code></group_code> <group_description></group_description> <external_style></external_style> <vas_group_code></vas_group_code> <short_descr></short_descr> <putaway_type></putaway_type> <conveyable></conveyable> <stackability_code></stackability_code> <sortable></sortable> <min_dispatch_uom></min_dispatch_uom> <product_life></product_life> <percent_acceptable_product_life></percent_acceptable_product_life> <lpns_per_tier></lpns_per_tier> <tiers_per_pallet></tiers_per_pallet> <velocity_code></velocity_code> <req_batch_nbr_flg></req_batch_nbr_flg> <serial_nbr_tracking></serial_nbr_tracking> <regularity_code></regularity_code> <harmonized_tariff_code></harmonized_tariff_code> <harmonized_tariff_description></harmonized_tariff_description> <full_oblpn_type></full_oblpn_type> <case_oblpn_type></case_oblpn_type> <pack_oblpn_type></pack_oblpn_type> <description_2></description_2> <description_3></description_3> <invn_attr_a_tracking></invn_attr_a_tracking> <invn_attr_a_dflt_value></invn_attr_a_dflt_value> <invn_attr_b_tracking></invn_attr_b_tracking> <invn_attr_b_dflt_value></invn_attr_b_dflt_value> <invn_attr_c_tracking></invn_attr_c_tracking> <invn_attr_c_dflt_value></invn_attr_c_dflt_value> <nmfc_code></nmfc_code> <conversion_factor></conversion_factor> <invn_attr_d_tracking></invn_attr_d_tracking> <invn_attr_e_tracking></invn_attr_e_tracking> <invn_attr_f_tracking></invn_attr_f_tracking> <invn_attr_g_tracking></invn_attr_g_tracking> <host_aware_item_flg>true</host_aware_item_flg> <packing_tolerance_percent></packing_tolerance_percent> <un_number></un_number> <un_class></un_class> <un_description></un_description> <packing_group></packing_group> <proper_shipping_name></proper_shipping_name> <excepted_qty_instr></excepted_qty_instr> <limited_qty_flg></limited_qty_flg> <fulldg_flg></fulldg_flg> <hazard_statement></hazard_statement> <shipping_temperature_instr></shipping_temperature_instr> <carrier_commodity_description></carrier_commodity_description> <hazmat_packaging_description></hazmat_packaging_description> <shipping_conversion_factor></shipping_conversion_factor> <shipping_uom></shipping_uom> <handle_decimal_qty_flg></handle_decimal_qty_flg> </item> </ListOfItems> </LgfData>";

        return xmlString;
    }

    public static String getXmlVendor(Documento documento, Integracao integracao) {

        String dataLog = "2018-10-23T12:12:12";
        String xmlStringVendor = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                "<LgfData>" +
                "<Header>" +
                "<DocumentVersion>18</DocumentVersion>" +
                "<OriginSystem>SYNC</OriginSystem>" +
                "<ClientEnvCode>1</ClientEnvCode>" +
                //"<ParentCompanyCode>" + documento.getCodigoDepositante() + "</ParentCompanyCode>" +
                "<ParentCompanyCode>1</ParentCompanyCode>" +
                "<Entity>vendor</Entity>" +
                "<TimeStamp>" + integracao.getDataLog() + "</TimeStamp>" +
                "<MessageId>" + documento.getSequenciaIntegracao() + "</MessageId>" +
                "</Header>" +
                "<ListOfVendors>" +
                "<vendor>" +
                "<company_code>1</company_code>" +
                "<vendor_code>" + documento.getCodigoEmpresa() + "</vendor_code>" +
                "<name>" + documento.getNomeEmpresa() + "</name>" +
                "<address_1>" + documento.getEnderecoEmpresa() + "</address_1>" +
                "<address_2>" + documento.getBairroEmpresa() + "</address_2>" +
                "<address_3></address_3>" +
                "<locality></locality>" +
                "<city>" + documento.getMunicipioEmpresa() + "</city>" +
                "<state>" + documento.getUfEmpresa() + "</state>" +
                "<zip>" + documento.getCepEmpresa() + "</zip>" +
                "<country></country>" +
                "<phone_nbr></phone_nbr>" +
                "<email></email>" +
                "<contact></contact>" +
                "<action_code>CREATE</action_code>" +
                "<univ_id_1></univ_id_1>" +
                "<cust_field_1>"+documento.getCnpjCpfEmpresa()+"</cust_field_1>" +
                "<cust_field_2></cust_field_2>" +
                "<cust_field_3></cust_field_3>" +
                "<cust_field_4></cust_field_4>" +
                "<cust_field_5></cust_field_5>" +
                "<reception_percent_level></reception_percent_level>" +
                "<create_asn_on_po_intf></create_asn_on_po_intf>" +
                "</vendor>" +
                "</ListOfVendors>" +
                "</LgfData>";

        return xmlStringVendor;
    }

    public static String getXmlPO(Documento documento, List<DocumentoDetalhe> listDocumentoDetalhe, Integracao integracao) {

        String xmlStringVendor = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                "<LgfData>" +
                "<Header>" +
                "<DocumentVersion>18</DocumentVersion>" +
                "<OriginSystem>SYNC</OriginSystem>" +
                "<ClientEnvCode>1</ClientEnvCode>" +
                //"<ParentCompanyCode>" + documento.getCodigoDepositante() + "</ParentCompanyCode>" +
                "<ParentCompanyCode>1</ParentCompanyCode>" +
                "<ParentCompanyCode>1</ParentCompanyCode>" +
                "<Entity>purchase_order</Entity>" +
                "<TimeStamp>" + integracao.getDataLog() + "</TimeStamp>" +
                "<MessageId>" + documento.getSequenciaIntegracao() + "</MessageId>" +
                "</Header>" +
                "<ListOfPurchaseOrders>" +
                "<purchase_order>" +
                "<purchase_order_hdr>" +
                "<po_nbr>" + documento.getNumeroDocumento() + "</po_nbr>" +
                "<facility_code>" + documento.getCodigoDepositante() + "</facility_code>" +
                "<company_code>1</company_code>" +
                "<vendor_code>" + documento.getCodigoEmpresa() + "</vendor_code>" +
                "<action_code>CREATE</action_code>" +
                "<ord_date>" + documento.getDataEmissao() + "</ord_date>" +
                "<ref_nbr></ref_nbr>" +
                "<po_type>" + documento.getTipoDocumento() + "</po_type>" +
                "<delivery_date>" + documento.getDataPrevisaoMovimento()+ "</delivery_date>" +
                "<dept_code></dept_code>" +
                "<ship_date>" + documento.getDataMovimento()+ "</ship_date>" +
                "<cancel_date>" + documento.getDataMovimento()+ "</cancel_date>" +
                "<cust_field_1>" + documento.getSerieDocumento() + "</cust_field_1>" +
                "<cust_field_2>" + documento.getNaturezaOperacao() + "</cust_field_2>" +
                "<cust_field_3>" + documento.getDescricaoNaturezaOperacao() + "</cust_field_3>" +
                "<cust_field_4>" + documento.getCfop() + "</cust_field_4>" +
                "<cust_field_5>" + documento.getAgrupador() + "</cust_field_5>" +
                "<cust_nbr></cust_nbr>" +
                "<cust_name></cust_name>" +
                "<cust_addr></cust_addr>" +
                "<cust_addr2></cust_addr2>" +
                "<cust_addr3></cust_addr3>" +
                "<rma_nbr></rma_nbr>" +
                "<sold_to_legal_name></sold_to_legal_name>" +
                "</purchase_order_hdr>" +
                getListDocumentoDetalheXml(listDocumentoDetalhe) +
                "</purchase_order>" +
                "</ListOfPurchaseOrders>" +
                "</LgfData>";

        return xmlStringVendor;
    }

    private static String getListDocumentoDetalheXml(List<DocumentoDetalhe> listDocumentoDetalhe) {

        StringBuilder stringBuilderDocumentoDetalhe = new StringBuilder();

        for (DocumentoDetalhe documentoDetalhe : listDocumentoDetalhe) {
            stringBuilderDocumentoDetalhe.append(setDocumentoDetalheXml(documentoDetalhe));
        }
        return stringBuilderDocumentoDetalhe.toString();
    }

    private static String setDocumentoDetalheXml(DocumentoDetalhe documentoDetalhe) {

        String xmlDetalhe = "<purchase_order_dtl>" +
                "<seq_nbr>" + documentoDetalhe.getSequenciaDetalhe() + "</seq_nbr>" +
                "<action_code>CREATE</action_code>" +
                "<item_alternate_code>" + documentoDetalhe.getCodigoProduto() + "</item_alternate_code>" +
                "<item_part_a></item_part_a>" +
                "<item_part_b></item_part_b>" +
                "<item_part_c></item_part_c>" +
                "<item_part_d></item_part_d>" +
                "<item_part_e></item_part_e>" +
                "<item_part_f></item_part_f>" +
                "<pre_pack_code></pre_pack_code>" +
                "<pre_pack_ratio></pre_pack_ratio>" +
                "<pre_pack_total_units></pre_pack_total_units>" +
                "<ord_qty>" + documentoDetalhe.getQuantidadeMovimento() + "</ord_qty>" +
                "<unit_cost>" + documentoDetalhe.getValorUnitario() + "</unit_cost>" +
                "<vendor_item_code></vendor_item_code>" +
                "<internal_misc_n1></internal_misc_n1>" +
                "<internal_misc_a1></internal_misc_a1>" +
                "<unit_retail></unit_retail>" +
                "<cust_field_1>" + documentoDetalhe.getClasseProduto() + "</cust_field_1>" +
                "<cust_field_2></cust_field_2>" +
                "<cust_field_3></cust_field_3>" +
                "<cust_field_4></cust_field_4>" +
                "<cust_field_5></cust_field_5>" +
                "<pre_pack_ratio_seq></pre_pack_ratio_seq>" +
                "<item_barcode></item_barcode>" +
                "<uom></uom>" +
                "<line_schedule_nbrs></line_schedule_nbrs>" +
                "<invn_attr_a></invn_attr_a>" +
                "<invn_attr_b></invn_attr_b>" +
                "<invn_attr_c></invn_attr_c>" +
                "<invn_attr_d></invn_attr_d>" +
                "<invn_attr_e></invn_attr_e>" +
                "<invn_attr_f></invn_attr_f>" +
                "<invn_attr_g></invn_attr_g>" +
                "<invn_attr_h></invn_attr_h>" +
                "<invn_attr_i></invn_attr_i>" +
                "<invn_attr_j></invn_attr_j>" +
                "<invn_attr_k></invn_attr_k>" +
                "<invn_attr_l></invn_attr_l>" +
                "<invn_attr_m></invn_attr_m>" +
                "<invn_attr_n></invn_attr_n>" +
                "<invn_attr_o></invn_attr_o>" +
                "</purchase_order_dtl>";

        return xmlDetalhe;

    }


    public static String getXmlOrder(Integracao integracao, Documento documento, List<DocumentoDetalhe> listaDocumentoDetalhe, String stringQuantidadeMovimentoTratado) {

        OrderRelease orderRelease = new OrderRelease();
        LgfDataOrder lgfDataOrder = new LgfDataOrder();
        Header Header = new Header();
        ListOfOrders ListOfOrders = new ListOfOrders();
        Order order = new Order();
        OrderHdr orderHdr = new OrderHdr();

        List<OrderDtl> orderDtls = new ArrayList<>();


        //Header
        Header.setMessageId(String.valueOf(integracao.getSequenciaIntegracao()));
        Header.setTimeStamp(String.valueOf(integracao.getDataLog()));
        Header.setParentCompanyCode("1");
        Header.setEntity("order");
        Header.setClientEnvCode("GAV");
        Header.setOriginSystem("ORAINT");
        Header.setDocumentVersion("18");

        //Order HDR
        orderHdr.setFacility_code(String.valueOf(documento.getCodigoDepositante()));
        //orderHdr.setCompany_code(documento.getCodigoDepositante());
        orderHdr.setCompany_code("1");
        orderHdr.setOrder_nbr(documento.getNumeroDocumento());
        orderHdr.setOrder_type(documento.getTipoDocumento());
        orderHdr.setOrd_date(String.valueOf(documento.getDataEmissao()));
        orderHdr.setReq_ship_date(String.valueOf(documento.getDataPrevisaoMovimento()));
        orderHdr.setDest_facility_code(documento.getCodigoEmpresa());
        orderHdr.setCust_field_1(documento.getSerieDocumento());
        orderHdr.setCust_field_2(documento.getNaturezaOperacao());
        orderHdr.setCust_field_3(documento.getDescricaoNaturezaOperacao());
        orderHdr.setCust_field_4(documento.getCfop());
        orderHdr.setCust_field_5(documento.getAgrupador());
        orderHdr.setAction_code("CREATE");


        int tamanho = documento.getAgrupador().length();
        if(tamanho > 5){
            String agrupadorPart = documento.getAgrupador().substring((tamanho -6), tamanho);
            orderHdr.setCust_short_text_2(agrupadorPart);
        } else {
            orderHdr.setCust_short_text_2("0");
        }

        int setCust_short_text = 1;
        int totalMovimento = 0;
        int count = 1;
        for (DocumentoDetalhe documentoDetalhe : listaDocumentoDetalhe) {
            int qtd = documentoDetalhe.getQuantidadeMovimento().intValue();
            totalMovimento += qtd;
            setCust_short_text = totalMovimento;
            for (int i =0 ; i<qtd ; i++){
                OrderDtl orderDtl = new OrderDtl();
                String seqDetalhe = String.valueOf(documentoDetalhe.getSequenciaDetalhe());
                if(seqDetalhe.length() == 1){
                    seqDetalhe = "0" + seqDetalhe;
                }

                //orderDtl.setCust_field_1(seqDetalhe);
                orderDtl.setSale_price(String.valueOf(documentoDetalhe.getValorUnitario()));
                orderDtl.setOrd_qty(String.valueOf(1));
                orderDtl.setItem_part_a(documentoDetalhe.getCodigoProduto());
                orderDtl.setSeq_nbr(String.valueOf(count));
               // orderDtl.setCust_number_1(stringQuantidadeMovimentoTratado); // VERIFICAR SE ESTA CORRETO
                if(i < 10){
                    orderDtl.setCust_field_1("0"+count);
                }else {
                    orderDtl.setCust_field_1(String.valueOf(count));
                }

                orderDtls.add(orderDtl);

                count++;
            }

        }
        //Order DTL
        if(setCust_short_text < 10){
            orderHdr.setCust_short_text_1("0"+setCust_short_text);
        }else {
            orderHdr.setCust_short_text_1(String.valueOf(setCust_short_text));
        }


        //Order
        order.setOrder_hdr(orderHdr);
        order.setOrder_dtl(orderDtls);


        ListOfOrders.setOrder(order);

        lgfDataOrder.setHeader(Header);
        lgfDataOrder.setListOfOrders(ListOfOrders);

        //orderRelease.setLgfData(lgfDataOrder);

        String xmlOrderRelease = null;
        try {
            XStream xstream = new XStream();
            xstream.processAnnotations(OrderRelease.class);
            xstream.processAnnotations(Header.class);

            xmlOrderRelease = xstream.toXML(lgfDataOrder);

        } catch (Exception e) {
            logger.debug(e.getMessage());
            return xmlOrderRelease;
        }

        return xmlOrderRelease;

    }

    public static String getXmlItem(Integracao integracao, Produto produto, TipoUc tipoUc, String stringQuantidadeComponentes) {

        LgfDataItem LgfData = new LgfDataItem();
        ListOfItems ListOfItems = new ListOfItems();

        //Header
        Header header = new Header();
        header.setDocumentVersion("18");
        header.setOriginSystem("SYNC");
        header.setClientEnvCode("GAV");
        header.setParentCompanyCode("1");
        header.setEntity("item");
        header.setTimeStamp(String.valueOf(integracao.getDataLog()));
        header.setMessageId(String.valueOf(integracao.getSequenciaIntegracao()));

        Item item1 = newItem(produto, tipoUc, stringQuantidadeComponentes);

        ListOfItems.getItens().add(item1);

        LgfData.setHeader(header);
        LgfData.setListOfItems(ListOfItems);

        String xmlItem = null;

        try {
            XStream xstream = new XStream(new DomDriver("UTF_8", new NoNameCoder()));
            xstream.processAnnotations(LgfDataItem.class);
            xstream.processAnnotations(Header.class);

            xmlItem = xstream.toXML(LgfData);

        } catch (Exception e) {
            logger.debug(e.getMessage());
            return xmlItem;
        }

        return xmlItem;

    }

    private static Item newItem(Produto produto, TipoUc tipoUc, String stringQuantidadeComponentes) {

        String shortDescription = produto.getCodigoProduto().substring(0,6);

        Item item = new Item();
        item.setCompany_code("1");
        item.setPart_a(produto.getCodigoProduto());
        item.setAction_code("CREATE");
        item.setDescription(produto.getDescricaoProduto());
        item.setBarcode(produto.getCodigoProduto());

        item.setUnit_length(tipoUc.getComprimentoProduto());
        item.setUnit_width(tipoUc.getLarguraProduto());
        item.setUnit_height(tipoUc.getAlturaProduto());
        item.setUnit_weight(tipoUc.getPesoLiquido());
        item.setHazmat("false");

        item.setGroup_code(produto.getGrupoProduto());
        item.setGroup_description(produto.getDescricaoGrupoProduto());
        item.setConveyable("false");
        item.setSortable("false");
        item.setReq_batch_nbr_flg("false");

        item.setDescription_2(produto.getDescricaoProdutoDet());
        item.setHost_aware_item_flg("true");

        item.setUn_class(produto.getCodigoLinha());
        item.setUn_description(produto.getDescricaoLinha());

        item.setLimited_qty_flg("false");
        item.setFulldg_flg("false");

        item.setHandle_decimal_qty_flg("false");
        item.setDummy_sku_flg("false");
        item.setPack_with_wave_flg("false");
        item.setShort_descr(shortDescription);
        item.setDescription_3(stringQuantidadeComponentes);
        return item;
    }

    public static String getXmlItemPrePack(Integracao integracao, Produto produto, List<ProdutoComponente> produtoComponentes) {

        LgfDataItemPrePack LgfData = new LgfDataItemPrePack();
        ListOfItemPrePacks ListOfItemPrePacks = new ListOfItemPrePacks();

        //Header
        Header header = new Header();
        header.setDocumentVersion("18");
        header.setOriginSystem("SYNC");
        header.setClientEnvCode("1");
        //header.setParentCompanyCode(produto.getCodigoEmpresa());
        header.setParentCompanyCode("1");
        header.setEntity("item_prepack");
        header.setTimeStamp(String.valueOf(integracao.getDataLog()));
        header.setMessageId(String.valueOf(integracao.getSequenciaIntegracao()));

        for (ProdutoComponente produtoComponente : produtoComponentes) {

            ItemPrePack itemPrePack = new ItemPrePack();

            itemPrePack.setCompany_code("1");
            itemPrePack.setPre_pack_code(produtoComponente.getCodigoProduto());
            itemPrePack.setParent_item_code(produtoComponente.getCodigoProduto());
            itemPrePack.setChild_item_code(produtoComponente.getCodigoProdutocomponente());
            itemPrePack.setChild_units(produtoComponente.getQuantidadeComponente());

            itemPrePack.setAction_code("CREATE");
            itemPrePack.setSeq_nbr(produtoComponente.getSequenciaComponente());

            ListOfItemPrePacks.getItemPrePacks().add(itemPrePack);
        }

        LgfData.setHeader(header);
        LgfData.setListOfItemPrePacks(ListOfItemPrePacks);

        String xmlItemPrePack = null;

        try {
            XStream xstream = new XStream(new DomDriver("UTF_8", new NoNameCoder()));
            xstream.processAnnotations(LgfDataItemPrePack.class);
            xstream.processAnnotations(Header.class);

            xmlItemPrePack = xstream.toXML(LgfData);

        } catch (Exception e) {
            logger.debug(e.getMessage());
            return xmlItemPrePack;
        }

        return xmlItemPrePack;

    }

    public static String getXmlIbShipment(List<ListOfPurchaseOrders> listOfPurchaseOrders) {

        LgfDataShipment LgfData = new LgfDataShipment();
        ListOfIbShipments ListOfIbShipments = new ListOfIbShipments();

        //Header
        Header header = new Header();
        header.setDocumentVersion("18");
        header.setOriginSystem("host");
        header.setClientEnvCode("ibshipment");
        header.setParentCompanyCode("C01");
        header.setEntity("ib_shipment");
        header.setTimeStamp(DateUtil.dateToLogTime(new Date()));
        header.setMessageId("str1234");

        for (ListOfPurchaseOrders purchaseOrders : listOfPurchaseOrders) {

            PurchaseOrder purchaseOrder = purchaseOrders.getPurchase_order();

            IbShipment ibShipment = new IbShipment();

            IbShipmentHdr ibShipmentHdr = new IbShipmentHdr();
            ibShipmentHdr.setShipment_nbr(purchaseOrder.getPurchase_order_hdr().getPo_nbr());
            ibShipmentHdr.setFacility_code(purchaseOrder.getPurchase_order_hdr().getFacility_code());
            ibShipmentHdr.setCompany_code("1");
            ibShipmentHdr.setTrailer_nbr("VEI0009");
            ibShipmentHdr.setAction_code("CREATE");
            ibShipmentHdr.setShipment_type("IBFORNECEDOR");
            ibShipmentHdr.setLoad_nbr(purchaseOrder.getPurchase_order_hdr().getPo_nbr());
            ibShipmentHdr.setOrig_shipped_units(purchaseOrder.getPurchase_order_dtl().getOrd_qty());

            IbShipmentDtl ibShipmentDtl = new IbShipmentDtl();
            ibShipmentDtl.setSeq_nbr(purchaseOrder.getPurchase_order_dtl().getSeq_nbr());
            ibShipmentDtl.setAction_code("CREATE");
            ibShipmentDtl.setItem_alternate_code(purchaseOrder.getPurchase_order_dtl().getItem_alternate_code());
            ibShipmentDtl.setShipped_qty(purchaseOrder.getPurchase_order_dtl().getOrd_qty());
            ibShipmentDtl.setPo_nbr(purchaseOrder.getPurchase_order_hdr().getPo_nbr());
            ibShipmentDtl.setUom("UNITS");

            ibShipment.setIb_shipment_hdr(ibShipmentHdr);
            ibShipment.setIb_shipment_dtl(ibShipmentDtl);

            ListOfIbShipments.getIbShipments().add(ibShipment);

        }

        LgfData.setHeader(header);
        LgfData.setListOfIbShipments(ListOfIbShipments);

        String xmlIbShipment = null;

        try {
            XStream xstream = new XStream(new DomDriver("UTF_8", new NoNameCoder()));
            xstream.processAnnotations(LgfDataShipment.class);
            xstream.processAnnotations(Header.class);
            xstream.processAnnotations(ListOfIbShipments.class);

            xmlIbShipment = xstream.toXML(LgfData);

        } catch (Exception e) {
            logger.debug(e.getMessage());
            return xmlIbShipment;
        }

        return xmlIbShipment;

    }

    public static String getXmlAppointment(List<ListOfPurchaseOrders> listOfPurchaseOrders) {

        LgfDataAppointment LgfData = new LgfDataAppointment();
        ListOfAppointments ListOfAppointments = new ListOfAppointments();

        //Header
        Header header = new Header();
        header.setDocumentVersion("18");
        header.setOriginSystem("host");
        header.setClientEnvCode("sched");
        header.setParentCompanyCode("C01");
        header.setEntity("appointment");
        header.setTimeStamp(DateUtil.dateToLogTime(new Date()));
        header.setMessageId("str1234");

        for (ListOfPurchaseOrders purchaseOrders : listOfPurchaseOrders) {

            PurchaseOrder purchaseOrder = purchaseOrders.getPurchase_order();

            Appointment appointment = new Appointment();

            appointment.setFacility_code(purchaseOrder.getPurchase_order_hdr().getFacility_code());
            appointment.setCompany_code("1");
            appointment.setAppt_nbr(purchaseOrder.getPurchase_order_hdr().getPo_nbr());
            appointment.setLoad_nbr(purchaseOrder.getPurchase_order_hdr().getPo_nbr());
            appointment.setDock_type("RECEBER");
            appointment.setAction_code("CREATE");
            appointment.setPlanned_start_ts(purchaseOrder.getPurchase_order_hdr().getShip_date());
            appointment.setDuration("");
            appointment.setEstimated_units(purchaseOrder.getPurchase_order_dtl().getOrd_qty());
            appointment.setTrailer_nbr("VEI0009");

            ListOfAppointments.getAppointments().add(appointment);

        }

        LgfData.setHeader(header);
        LgfData.setListOfAppointments(ListOfAppointments);

        String xmlAppointment = null;

        try {
            XStream xstream = new XStream(new DomDriver("UTF_8", new NoNameCoder()));
            xstream.processAnnotations(LgfDataAppointment.class);
            xstream.processAnnotations(Header.class);

            xmlAppointment = xstream.toXML(LgfData);

        } catch (Exception e) {
            logger.debug(e.getMessage());
            return xmlAppointment;
        }

        return xmlAppointment;

    }

    public static String getXmlSale(SaleDto saleDto) {

        LgfDataSales LgfData = new LgfDataSales();
        ListOfPointOfSales ListOfPointOfSales = new ListOfPointOfSales();

        //Header
        Header header = new Header();
        header.setDocumentVersion("18");
        header.setOriginSystem("ERPCLOUD");
        header.setClientEnvCode("TEST");
        header.setParentCompanyCode("C01");
        header.setEntity("point_of_sale");
        header.setTimeStamp(DateUtil.dateToLogTime(new Date()));
        header.setMessageId("1");

        for (ItemProdutoDto itemProdutoDto : saleDto.getItens()) {
            PointOfSale pointOfSale = new PointOfSale();

            pointOfSale.setFacility_code("LOJA1001");
            pointOfSale.setCompany_code("1");
            pointOfSale.setTransaction_type("CREATE");
            pointOfSale.setTransactionid("1");
            pointOfSale.setSeq_nbr("1");
            pointOfSale.setRef_transactionid("1");
            pointOfSale.setRef_seq_nbr("1");
            pointOfSale.setLocation_barcode("PRAT0012");
            pointOfSale.setItem_alternate_code(itemProdutoDto.getCodigoProduto());
            pointOfSale.setQuantity(itemProdutoDto.getQuantidadeProduto());
            pointOfSale.setPos_user("PDV");

            ListOfPointOfSales.getPointOfSales().add(pointOfSale);
        }

        LgfData.setHeader(header);
        LgfData.setListOfPointOfSales(ListOfPointOfSales);

        String xmlSales = null;

        try {
            XStream xstream = new XStream(new DomDriver("UTF_8", new NoNameCoder()));
            xstream.processAnnotations(LgfDataSales.class);
            xstream.processAnnotations(Header.class);

            xmlSales = xstream.toXML(LgfData);

        } catch (Exception e) {
            logger.debug(e.getMessage());
            return xmlSales;
        }

        return xmlSales;

    }

//    public static String getXmlShipped() {
//
//        LgfDataShipped LgfData = new LgfDataShipped();
//        ListOfShippedLoads ListOfShippedLoads = new ListOfShippedLoads();
//
//        //Header
//        Header header = new Header();
//        header.setDocumentVersion("9.0.1");
//        header.setOriginSystem("LogFire");
//        header.setClientEnvCode("storeautomacao_test");
//        header.setParentCompanyCode("C01");
//        header.setEntity("shipped_load");
//        header.setTimeStamp(DateUtil.dateToLogTime(new Date()));
//        header.setMessageId("SLSHGN59456277000176J3PL0000010522");
//
//        ShippedLoad shippedLoad = new ShippedLoad();
//
//        for (int i= 0; i < 2; i++) {
//
//            Load load = new Load();
//            load.setAction_code("CREATE");
//
//            ObStop obStop = new ObStop();
//            obStop.setOrder_type("");
//
//            shippedLoad.setLoad(load);
//            shippedLoad.setOb_stop(obStop);
//
//            ListOfShippedLoads.getShippedLoads().add(shippedLoad);
//
//        }
//
//        LgfData.setHeader(header);
//        LgfData.setListOfShippedLoads(ListOfShippedLoads);
//
//        String xmlShipped = null;
//
//        try {
//            XStream xstream = new XStream(new DomDriver("UTF_8", new NoNameCoder()));
//            xstream.processAnnotations(LgfDataShipped.class);
//            xstream.processAnnotations(Header.class);
//            xstream.processAnnotations(ListOfShippedLoads.class);
//
//            xmlShipped = xstream.toXML(LgfData);
//
//        } catch (Exception e) {
//            logger.debug(e.getMessage());
//            return xmlShipped;
//        }
//
//        return xmlShipped;
//
//    }

    public static String getXmlInventory() {

        LgfDataInventory LgfData = new LgfDataInventory();
        ListOfInventory ListOfInventory = new ListOfInventory();

        //Header
        Header header = new Header();
        header.setDocumentVersion("9.0.1");
        header.setOriginSystem("LogFire");
        header.setClientEnvCode("storeautomacao_test");
        header.setParentCompanyCode("C01");
        header.setEntity("inventory_summary");
        header.setTimeStamp(DateUtil.dateToLogTime(new Date()));
        header.setMessageId("INSHGN59456277000176J3PL0000020806");

        for (int i= 0; i < 2; i++) {

            Inventory inventory = new Inventory();
            inventory.setFacility_code("J3PL");

            ListOfInventory.getInventories().add(inventory);

        }

        LgfData.setHeader(header);
        LgfData.setListOfInventory(ListOfInventory);

        String xmlInventory = null;

        try {
            XStream xstream = new XStream(new DomDriver("UTF_8", new NoNameCoder()));
            xstream.processAnnotations(LgfDataInventory.class);
            xstream.processAnnotations(Header.class);

            xmlInventory = xstream.toXML(LgfData);

        } catch (Exception e) {
            logger.debug(e.getMessage());
            return xmlInventory;
        }

        return xmlInventory;

    }

    public static String getTransmissionHeader(String xmlTranspotado) {
        String xmlTransmission =
                "<Transmission xmlns:otm=\"http://xmlns.oracle.com/apps/otm/transmission/v6.4\">>" +
                        "<TransmissionHeader>" +
                        "<SenderTransmiossionNo>9981</SenderTransmiossionNo>" +
                        "<AckSpec>" +
                        "<ComMethodGid>" +
                        "<Gid>" +
                        "<Xid>HTTPPOST</Xid>" +
                        "</Gid>" +
                        "</ComMethodGid>" +
                        "<ServerURL>http://localhost</ServerURL>" +
                        "</AckSpec>" +
                        "</TransmissionHeader>" +
                        "<TransmissionBody>" +
                        "<GlogXMLElement>"
                        + xmlTranspotado +
                        "</GlogXMLElement>" +
                        "</TransmissionBody>" +
                        "</Transmission>";

        return xmlTransmission;

    }


    public static String getOrderReleaseOTM() {
        //"<?xml version='1.0'?><otm:Release xmlns:gtm=\"http://xmlns.oracle.com/apps/gtm/transmission/v6.4\" xmlns:otm=\"http://xmlns.oracle.com/apps/otm/transmission/v6.4\">" +
        String xmlOrderReleaseOTM =
                //"<otm:Release>" +
                "<?xml version='1.0'?><otm:Release xmlns:gtm=\"http://xmlns.oracle.com/apps/gtm/transmission/v6.4\" xmlns:otm=\"http://xmlns.oracle.com/apps/otm/transmission/v6.4\">" +
                        "<otm:ReleaseGid><otm:Gid><otm:DomainName>SDISTRIBUIDORA</otm:DomainName><otm:Xid>OR_DIST_01</otm:Xid></otm:Gid></otm:ReleaseGid>" +
                        "<otm:TransactionCode>NP</otm:TransactionCode>" +
                        "<otm:ShipFromLocationRef>" +
                        "<otm:LocationRef>" +
                        "<otm:LocationGid><otm:Gid><otm:DomainName>SDISTRIBUIDORA</otm:DomainName><otm:Xid>61338941000124</otm:Xid></otm:Gid></otm:LocationGid>" +
                        "</otm:LocationRef>" +
                        "</otm:ShipFromLocationRef>" +
                        "<otm:ShipToLocationRef>" +
                        "<otm:LocationRef>" +
                        "<otm:LocationGid><otm:Gid><otm:DomainName>SDISTRIBUIDORA</otm:DomainName><otm:Xid>84361974000190</otm:Xid></otm:Gid></otm:LocationGid>" +
                        "</otm:LocationRef>" +
                        "</otm:ShipToLocationRef>" +
                        "<otm:TimeWindow/>" +
                        "<otm:ReleaseLine> " +
                        "<otm:ReleaseLineGid>" +
                        "<otm:Gid><otm:DomainName>SDISTRIBUIDORA</otm:DomainName><otm:Xid>OR_DISTR_01</otm:Xid></otm:Gid>" +
                        "</otm:ReleaseLineGid>" +
                        "<otm:PackagedItemRef>" +
                        "<otm:PackagedItemGid>" +
                        "<otm:Gid><otm:DomainName>SDISTRIBUIDORA</otm:DomainName><otm:Xid>78223427</otm:Xid></otm:Gid>" +
                        "</otm:PackagedItemGid>" +
                        "</otm:PackagedItemRef><otm:ItemQuantity/>" +
                        "</otm:ReleaseLine>" +
                        "<otm:ShipUnit>" +
                        "<otm:ShipUnitGid>" +
                        "<otm:Gid><otm:DomainName>SDISTRIBUIDORA</otm:DomainName><otm:Xid>OR_DIST_01-001</otm:Xid></otm:Gid>" +
                        "</otm:ShipUnitGid>" +
                        "<otm:WeightVolume>" +
                        "<otm:Weight>" +
                        "<otm:WeightValue>10000.0000</otm:WeightValue><otm:WeightUOMGid><otm:Gid><otm:Xid>KG</otm:Xid></otm:Gid></otm:WeightUOMGid>" +
                        "</otm:Weight><otm:Volume><otm:VolumeValue>0.0000</otm:VolumeValue><otm:VolumeUOMGid><otm:Gid><otm:Xid>CUMTR</otm:Xid></otm:Gid></otm:VolumeUOMGid></otm:Volume>" +
                        "</otm:WeightVolume>" +
                        "<otm:ShipUnitContent>" +
                        "<otm:PackagedItemRef>" +
                        "<otm:PackagedItemGid><otm:Gid><otm:DomainName>SDISTRIBUIDORA</otm:DomainName><otm:Xid>78223427</otm:Xid></otm:Gid></otm:PackagedItemGid>" +
                        "</otm:PackagedItemRef>" +
                        "<otm:LineNumber>1</otm:LineNumber>" +
                        "<otm:ItemQuantity/>" +
                        "<otm:ReleaseGid>" +
                        "<otm:Gid>" +
                        "<otm:DomainName>SDISTRIBUIDORA</otm:DomainName><otm:Xid>OR_DIST_01</otm:Xid>" +
                        "</otm:Gid>" +
                        "</otm:ReleaseGid>" +
                        "<otm:ReleaseLineGid>" +
                        "<otm:Gid>" +
                        "<otm:DomainName>SDISTRIBUIDORA</otm:DomainName><otm:Xid>OR_DISTR_01</otm:Xid>" +
                        "</otm:Gid>" +
                        "</otm:ReleaseLineGid>" +
                        "</otm:ShipUnitContent>" +
                        "<otm:ShipUnitCount>1</otm:ShipUnitCount>" +
                        "</otm:ShipUnit>" +
                        "<otm:ShipUnit>" +
                        "<otm:ShipUnitGid>" +
                        "<otm:Gid><otm:DomainName>SDISTRIBUIDORA</otm:DomainName><otm:Xid>OR_TEST_01-001</otm:Xid></otm:Gid>" +
                        "</otm:ShipUnitGid>" +
                        "<otm:WeightVolume>" +
                        "<otm:Weight>" +
                        "<otm:WeightValue>15000.0000</otm:WeightValue><otm:WeightUOMGid><otm:Gid><otm:Xid>KG</otm:Xid></otm:Gid></otm:WeightUOMGid>" +
                        "</otm:Weight><otm:Volume><otm:VolumeValue>0.0000</otm:VolumeValue><otm:VolumeUOMGid><otm:Gid><otm:Xid>CUMTR</otm:Xid></otm:Gid></otm:VolumeUOMGid></otm:Volume>" +
                        "</otm:WeightVolume>" +
                        "<otm:ShipUnitContent>" +
                        "<otm:PackagedItemRef>" +
                        "<otm:PackagedItemGid><otm:Gid><otm:DomainName>SDISTRIBUIDORA</otm:DomainName><otm:Xid>78223427</otm:Xid></otm:Gid></otm:PackagedItemGid>" +
                        "</otm:PackagedItemRef>" +
                        "<otm:LineNumber>1</otm:LineNumber>" +
                        "<otm:ItemQuantity/>" +
                        "<otm:ReleaseGid>" +
                        "<otm:Gid><otm:DomainName>SDISTRIBUIDORA</otm:DomainName><otm:Xid>OR_TEST</otm:Xid></otm:Gid>" +
                        "</otm:ReleaseGid>" +
                        "<otm:ReleaseLineGid>" +
                        "<otm:Gid><otm:DomainName>SDISTRIBUIDORA</otm:DomainName><otm:Xid>OR_TEST_01</otm:Xid></otm:Gid>" +
                        "</otm:ReleaseLineGid>" +
                        "</otm:ShipUnitContent>" +
                        "<otm:ShipUnitCount>1</otm:ShipUnitCount>" +
                        "</otm:ShipUnit>" +
                        "<otm:TotalWeightVolume>" +
                        "<otm:WeightVolume>" +
                        "<otm:Weight>" +
                        "<otm:WeightValue>10000.0000</otm:WeightValue><otm:WeightUOMGid><otm:Gid><otm:Xid>KG</otm:Xid></otm:Gid></otm:WeightUOMGid>" +
                        "</otm:Weight>" +
                        "<otm:Volume>" +
                        "<otm:VolumeValue>0.0000</otm:VolumeValue>" +
                        "<otm:VolumeUOMGid>" +
                        "<otm:Gid><otm:Xid>CUMTR</otm:Xid></otm:Gid>" +
                        "</otm:VolumeUOMGid>" +
                        "</otm:Volume>" +
                        "</otm:WeightVolume>" +
                        "</otm:TotalWeightVolume>" +
                        "<otm:ReleaseRefnum>" +
                        "<otm:ReleaseRefnumQualifierGid>" +
                        "<otm:Gid><otm:Xid>GLOG</otm:Xid></otm:Gid>" +
                        "</otm:ReleaseRefnumQualifierGid>" +
                        "<otm:ReleaseRefnumValue>SDISTRIBUIDORA.OR_DIST_01</otm:ReleaseRefnumValue>" +
                        "</otm:ReleaseRefnum>" +
                        "<otm:ReleaseStatus>" +
                        "<otm:StatusTypeGid><otm:Gid><otm:DomainName>SDISTRIBUIDORA</otm:DomainName><otm:Xid>BILLED</otm:Xid></otm:Gid></otm:StatusTypeGid>" +
                        "<otm:StatusValueGid><otm:Gid><otm:DomainName>SDISTRIBUIDORA</otm:DomainName><otm:Xid>BILLED_NOT_BILLED</otm:Xid></otm:Gid></otm:StatusValueGid>" +
                        "</otm:ReleaseStatus>" +
                        "<otm:ReleaseStatus>" +
                        "<otm:StatusTypeGid><otm:Gid><otm:DomainName>SDISTRIBUIDORA</otm:DomainName><otm:Xid>QUANTITY VALIDATION SOURCE</otm:Xid></otm:Gid></otm:StatusTypeGid>" +
                        "<otm:StatusValueGid><otm:Gid><otm:DomainName>SDISTRIBUIDORA</otm:DomainName><otm:Xid>QUANTITY VALIDATION SOURCE_NOT STARTED</otm:Xid></otm:Gid></otm:StatusValueGid>" +
                        "</otm:ReleaseStatus>" +
                        "<otm:ReleaseStatus>" +
                        "<otm:StatusTypeGid><otm:Gid><otm:DomainName>SDISTRIBUIDORA</otm:DomainName><otm:Xid>FLEET ASSIGNMENT OR</otm:Xid></otm:Gid></otm:StatusTypeGid>" +
                        "<otm:StatusValueGid><otm:Gid><otm:DomainName>SDISTRIBUIDORA</otm:DomainName><otm:Xid>FLEET ASSIGNMENT OR_NOT ASSIGNED</otm:Xid></otm:Gid></otm:StatusValueGid>" +
                        "</otm:ReleaseStatus>" +
                        "<otm:ReleaseStatus>" +
                        "<otm:StatusTypeGid><otm:Gid><otm:DomainName>SDISTRIBUIDORA</otm:DomainName><otm:Xid>OR_HD_LOAD_STATUS</otm:Xid></otm:Gid></otm:StatusTypeGid>" +
                        "<otm:StatusValueGid><otm:Gid><otm:DomainName>SDISTRIBUIDORA</otm:DomainName><otm:Xid>OR_NOT_READY_TO_LOAD</otm:Xid></otm:Gid></otm:StatusValueGid>" +
                        "</otm:ReleaseStatus>" +
                        "<otm:ReleaseStatus>" +
                        "<otm:StatusTypeGid><otm:Gid><otm:DomainName>SDISTRIBUIDORA</otm:DomainName><otm:Xid>QUANTITY VALIDATION DESTINATION</otm:Xid></otm:Gid></otm:StatusTypeGid>" +
                        "<otm:StatusValueGid><otm:Gid><otm:DomainName>SDISTRIBUIDORA</otm:DomainName><otm:Xid>QUANTITY VALIDATION DESTINATION_NOT STARTED</otm:Xid></otm:Gid></otm:StatusValueGid>" +
                        "</otm:ReleaseStatus>" +
                        "<otm:ReleaseStatus>" +
                        "<otm:StatusTypeGid><otm:Gid><otm:DomainName>SDISTRIBUIDORA</otm:DomainName><otm:Xid>PLANNING</otm:Xid></otm:Gid></otm:StatusTypeGid>" +
                        "<otm:StatusValueGid><otm:Gid><otm:DomainName>SDISTRIBUIDORA</otm:DomainName><otm:Xid>PLANNING_NEW</otm:Xid></otm:Gid></otm:StatusValueGid>" +
                        "</otm:ReleaseStatus>" +
                        "<otm:ReleaseStatus><otm:StatusTypeGid><otm:Gid><otm:DomainName>SDISTRIBUIDORA</otm:DomainName><otm:Xid>ORDER MODIFIED</otm:Xid></otm:Gid></otm:StatusTypeGid>" +
                        "<otm:StatusValueGid><otm:Gid><otm:DomainName>SDISTRIBUIDORA</otm:DomainName><otm:Xid>ORDER MODIFIED_NO MODIFICATION</otm:Xid></otm:Gid></otm:StatusValueGid>" +
                        "</otm:ReleaseStatus>" +
                        "<otm:ReleaseStatus>" +
                        "<otm:StatusTypeGid><otm:Gid><otm:DomainName>SDISTRIBUIDORA</otm:DomainName><otm:Xid>ORDER_RELEASE GUARANTEED DELIVERY</otm:Xid></otm:Gid></otm:StatusTypeGid>" +
                        "<otm:StatusValueGid><otm:Gid><otm:DomainName>SDISTRIBUIDORA</otm:DomainName><otm:Xid>ORDER_RELEASE GUARANTEED DELIVERY_NOT STARTED</otm:Xid></otm:Gid></otm:StatusValueGid>" +
                        "</otm:ReleaseStatus>" +
                        "<otm:ReleaseStatus>" +
                        "<otm:StatusTypeGid><otm:Gid><otm:DomainName>SDISTRIBUIDORA</otm:DomainName><otm:Xid>PLANNING_SELL</otm:Xid></otm:Gid></otm:StatusTypeGid>" +
                        "<otm:StatusValueGid><otm:Gid><otm:DomainName>SDISTRIBUIDORA</otm:DomainName><otm:Xid>PLANNING_SELL_NEW</otm:Xid></otm:Gid></otm:StatusValueGid>" +
                        "</otm:ReleaseStatus>" +
                        "<otm:ReleaseStatus><otm:StatusTypeGid><otm:Gid><otm:DomainName>SDISTRIBUIDORA</otm:DomainName><otm:Xid>ORDER DELIVERED</otm:Xid></otm:Gid></otm:StatusTypeGid>" +
                        "<otm:StatusValueGid><otm:Gid><otm:DomainName>SDISTRIBUIDORA</otm:DomainName><otm:Xid>ORDER DELIVERED_NOT STARTED</otm:Xid></otm:Gid></otm:StatusValueGid>" +
                        "</otm:ReleaseStatus>" +
                        "<otm:ReleaseStatus>" +
                        "<otm:StatusTypeGid><otm:Gid><otm:DomainName>SDISTRIBUIDORA</otm:DomainName><otm:Xid>READY_TO_SHIP</otm:Xid></otm:Gid></otm:StatusTypeGid>" +
                        "<otm:StatusValueGid><otm:Gid><otm:DomainName>SDISTRIBUIDORA</otm:DomainName><otm:Xid>READY_TO_SHIP_NEW</otm:Xid></otm:Gid></otm:StatusValueGid>" +
                        "</otm:ReleaseStatus>" +
                        "<otm:ReleaseStatus>" +
                        "<otm:StatusTypeGid><otm:Gid><otm:DomainName>SDISTRIBUIDORA</otm:DomainName><otm:Xid>CANCELLED</otm:Xid></otm:Gid></otm:StatusTypeGid>" +
                        "<otm:StatusValueGid><otm:Gid><otm:DomainName>SDISTRIBUIDORA</otm:DomainName><otm:Xid>CANCELLED_NOT CANCELLED</otm:Xid></otm:Gid></otm:StatusValueGid>" +
                        "</otm:ReleaseStatus>" +
                        "<otm:ReleaseStatus>" +
                        "<otm:StatusTypeGid><otm:Gid><otm:DomainName>SDISTRIBUIDORA</otm:DomainName><otm:Xid>ORDER RELEASE SKU LINK</otm:Xid></otm:Gid></otm:StatusTypeGid>" +
                        "<otm:StatusValueGid><otm:Gid><otm:DomainName>SDISTRIBUIDORA</otm:DomainName><otm:Xid>ORDER RELEASE SKU LINK_INACTIVE</otm:Xid></otm:Gid></otm:StatusValueGid>" +
                        "</otm:ReleaseStatus>" +
                        "</otm:Release>";

        return xmlOrderReleaseOTM;
    }


    public static String getXmlLocation() {
        //"<?xml version=\"1.0\"?>" +
        //"<otm:Location xmlns:gtm=\"http://xmlns.oracle.com/apps/gtm/transmission/v6.4\" xmlns:otm=\"http://xmlns.oracle.com/apps/otm/transmission/v6.4\">" +
        String xmlLocation =
                "<otm:Location>" +
                        "<otm:SendReason>" +
                        "<otm:Remark>" +
                        "<otm:RemarkSequence>1</otm:RemarkSequence>" +
                        "<otm:RemarkQualifierGid>" +
                        "<otm:Gid>" +
                        "<otm:Xid>QUERY TYPE</otm:Xid>" +
                        "</otm:Gid>" +
                        "</otm:RemarkQualifierGid>" +
                        "<otm:RemarkText>LOCATION</otm:RemarkText>" +
                        "</otm:Remark>" +
                        "<otm:SendReasonGid>" +
                        "<otm:Gid>" +
                        "<otm:Xid>SEND INTEGRATION</otm:Xid>" +
                        "</otm:Gid>" +
                        "</otm:SendReasonGid>" +
                        "<otm:ObjectType>LOCATION</otm:ObjectType>" +
                        "</otm:SendReason>" +
                        "<otm:TransactionCode>NP</otm:TransactionCode>" +
                        "<otm:LocationGid>" +
                        "<otm:Gid>" +
                        "<otm:DomainName>GAVE</otm:DomainName>" +
                        "<otm:Xid>100</otm:Xid>" +
                        "</otm:Gid>" +
                        "</otm:LocationGid>" +
                        "<otm:LocationName>100AVENIDA</otm:LocationName>" +
                        "<otm:IsTemplate>N</otm:IsTemplate>" +
                        "<otm:Address>" +
                        "<otm:City>CAMPO GRANDE</otm:City>" +
                        "<otm:Province>MATO GROSSO DO SUL</otm:Province>" +
                        "<otm:ProvinceCode>MS</otm:ProvinceCode>" +
                        "<otm:PostalCode>79117-010</otm:PostalCode>" +
                        "<otm:CountryCode3Gid>" +
                        "<otm:Gid>" +
                        "<otm:Xid>BR</otm:Xid>" +
                        "</otm:Gid>" +
                        "</otm:CountryCode3Gid>" +
                        "<otm:CountryCode>" +
                        "<otm:CountryCode3Gid>" +
                        "<otm:Gid>" +
                        "<otm:Xid>BR</otm:Xid>" +
                        "</otm:Gid>" +
                        "</otm:CountryCode3Gid>" +
                        "</otm:CountryCode>" +
                        "<otm:TimeZoneGid>" +
                        "<otm:Gid>" +
                        "<otm:Xid>Brazil/East</otm:Xid>" +
                        "</otm:Gid>" +
                        "</otm:TimeZoneGid>" +
                        "<otm:Latitude>-20.43482</otm:Latitude>" +
                        "<otm:Longitude>-54.62852</otm:Longitude>" +
                        "</otm:Address>" +
                        "<otm:IsTemporary>N</otm:IsTemporary>" +
                        "<otm:LocationRole>" +
                        "<otm:LocationRoleGid>" +
                        "<otm:Gid>" +
                        "<otm:Xid>SHIPFROM/SHIPTO</otm:Xid>" +
                        "</otm:Gid>" +
                        "</otm:LocationRoleGid>" +
                        "<otm:XDockIsInboundBias>N</otm:XDockIsInboundBias>" +
                        "<otm:CreatePoolHandlingShipment>N</otm:CreatePoolHandlingShipment>" +
                        "<otm:CreateXDockHandlingShipment>N</otm:CreateXDockHandlingShipment>" +
                        "<otm:IsMixedFreightTHUAllowed>N</otm:IsMixedFreightTHUAllowed>" +
                        "</otm:LocationRole>" +
                        "<otm:IsMakeAppointmentBeforePlan>N</otm:IsMakeAppointmentBeforePlan>" +
                        "<otm:IsShipperKnown>N</otm:IsShipperKnown>" +
                        "<otm:IsAddressValid>U</otm:IsAddressValid>" +
                        "<otm:IsLTLSplitable>Y</otm:IsLTLSplitable>" +
                        "<otm:ApptObjectType>S</otm:ApptObjectType>" +
                        "<otm:ExcludeFromRouteExec>N</otm:ExcludeFromRouteExec>" +
                        "<otm:UseApptPriority>N</otm:UseApptPriority>" +
                        "<otm:SchedLowPriorityAppt>N</otm:SchedLowPriorityAppt>" +
                        "<otm:EnforceTimeWindowAppt>Y</otm:EnforceTimeWindowAppt>" +
                        "<otm:SchedInfeasibleAppt>Y</otm:SchedInfeasibleAppt>" +
                        "<otm:AllowDriverRest>Y</otm:AllowDriverRest>" +
                        "<otm:IsFixedAddress>N</otm:IsFixedAddress>" +
                        "<otm:PrimaryAddressLineSeq>1</otm:PrimaryAddressLineSeq>" +
                        "<otm:FlexFieldStrings/>" +
                        "<otm:FlexFieldNumbers/>" +
                        "<otm:FlexFieldDates/>" +
                        "<otm:IsActive>Y</otm:IsActive>" +
                        "</otm:Location>";

        return xmlLocation;
    }


    public static String getXmlShipment() {
        String xml = "<otm:TenderOffer xmlns:otm=\"http://xmlns.oracle.com/apps/otm/transmission/v6.4\" xmlns:gtm=\"http://xmlns.oracle.com/apps/gtm/transmission/v6.4\">" +
                "<otm:Shipment>" +
                "<otm:ShipmentHeader>" +
                "<otm:ShipmentGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>7003</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:ShipmentGid>" +
                "<otm:ShipmentRefnum>" +
                "<otm:ShipmentRefnumQualifierGid>" +
                "<otm:Gid>" +
                "<otm:Xid>GLOG</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:ShipmentRefnumQualifierGid>" +
                "<otm:ShipmentRefnumValue>GAVE.7003</otm:ShipmentRefnumValue>" +
                "</otm:ShipmentRefnum>" +
                "<otm:TransactionCode>I</otm:TransactionCode>" +
                "<otm:PlannedShipmentInfo/>" +
                "<otm:ServiceProviderGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>SERVPROV</otm:DomainName>" +
                "<otm:Xid>20192551000101</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:ServiceProviderGid>" +
                "<otm:ServiceProviderAlias>" +
                "<otm:ServiceProviderAliasQualifierGid>" +
                "<otm:Gid>" +
                "<otm:Xid>GLOG</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:ServiceProviderAliasQualifierGid>" +
                "<otm:ServiceProviderAliasValue>SERVPROV.20192551000101</otm:ServiceProviderAliasValue>" +
                "</otm:ServiceProviderAlias>" +
                "<otm:ContactGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>SERVPROV</otm:DomainName>" +
                "<otm:Xid>20192551000101</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:ContactGid>" +
                "<otm:RateOfferingGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>RO_RF_TRANSLOG_CGD</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:RateOfferingGid>" +
                "<otm:RateGeoGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>RR_RF_100_NEW</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:RateGeoGid>" +
                "<otm:ExchangeRateInfo>" +
                "<otm:ExchangeRateGid>" +
                "<otm:Gid>" +
                "<otm:Xid>DEFAULT</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:ExchangeRateGid>" +
                "</otm:ExchangeRateInfo>" +
                "<otm:TotalPlannedCost>" +
                "<otm:FinancialAmount>" +
                "<otm:GlobalCurrencyCode>BRL</otm:GlobalCurrencyCode>" +
                "<otm:MonetaryAmount>639.22</otm:MonetaryAmount>" +
                "</otm:FinancialAmount>" +
                "</otm:TotalPlannedCost>" +
                "<otm:TotalActualCost>" +
                "<otm:FinancialAmount>" +
                "<otm:GlobalCurrencyCode>BRL</otm:GlobalCurrencyCode>" +
                "<otm:MonetaryAmount>639.22</otm:MonetaryAmount>" +
                "</otm:FinancialAmount>" +
                "</otm:TotalActualCost>" +
                "<otm:TotalWeightedCost>" +
                "<otm:FinancialAmount>" +
                "<otm:GlobalCurrencyCode>BRL</otm:GlobalCurrencyCode>" +
                "<otm:MonetaryAmount>639.22</otm:MonetaryAmount>" +
                "</otm:FinancialAmount>" +
                "</otm:TotalWeightedCost>" +
                "<otm:ShipmentCost>" +
                "<otm:CostType>B</otm:CostType>" +
                "<otm:Cost>" +
                "<otm:FinancialAmount>" +
                "<otm:GlobalCurrencyCode>BRL</otm:GlobalCurrencyCode>" +
                "<otm:MonetaryAmount>639.2184661464001</otm:MonetaryAmount>" +
                "</otm:FinancialAmount>" +
                "</otm:Cost>" +
                "</otm:ShipmentCost>" +
                "<otm:ITransactionNo>3002</otm:ITransactionNo>" +
                "<otm:IsHazardous>N</otm:IsHazardous>" +
                "<otm:RateServiceGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>RS_GAVE</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:RateServiceGid>" +
                "<otm:TransportModeGid>" +
                "<otm:Gid>" +
                "<otm:Xid>TL</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:TransportModeGid>" +
                "<otm:TotalWeightVolume>" +
                "<otm:WeightVolume>" +
                "<otm:Weight>" +
                "<otm:WeightValue>0.0</otm:WeightValue>" +
                "<otm:WeightUOMGid>" +
                "<otm:Gid>" +
                "<otm:Xid>LB</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:WeightUOMGid>" +
                "</otm:Weight>" +
                "<otm:Volume>" +
                "<otm:VolumeValue>0.0</otm:VolumeValue>" +
                "<otm:VolumeUOMGid>" +
                "<otm:Gid>" +
                "<otm:Xid>CUFT</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:VolumeUOMGid>" +
                "</otm:Volume>" +
                "</otm:WeightVolume>" +
                "</otm:TotalWeightVolume>" +
                "<otm:TotalNetWeightVolume>" +
                "<otm:Weight>" +
                "<otm:WeightValue>0.0</otm:WeightValue>" +
                "<otm:WeightUOMGid>" +
                "<otm:Gid>" +
                "<otm:Xid>LB</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:WeightUOMGid>" +
                "</otm:Weight>" +
                "<otm:Volume>" +
                "<otm:VolumeValue>0.0</otm:VolumeValue>" +
                "<otm:VolumeUOMGid>" +
                "<otm:Gid>" +
                "<otm:Xid>CUFT</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:VolumeUOMGid>" +
                "</otm:Volume>" +
                "</otm:TotalNetWeightVolume>" +
                "<otm:TotalShipUnitCount>4</otm:TotalShipUnitCount>" +
                "<otm:StartDt>" +
                "<otm:GLogDate>20181018211809</otm:GLogDate>" +
                "<otm:TZId>Etc/GMT-2</otm:TZId>" +
                "<otm:TZOffset>+02:00</otm:TZOffset>" +
                "</otm:StartDt>" +
                "<otm:EndDt>" +
                "<otm:GLogDate>20181019012032</otm:GLogDate>" +
                "<otm:TZId>Brazil/West</otm:TZId>" +
                "<otm:TZOffset>-04:00</otm:TZOffset>" +
                "</otm:EndDt>" +
                "<otm:LoadedDistance>" +
                "<otm:Distance>" +
                "<otm:DistanceValue>10.64</otm:DistanceValue>" +
                "<otm:DistanceUOMGid>" +
                "<otm:Gid>" +
                "<otm:Xid>MI</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:DistanceUOMGid>" +
                "</otm:Distance>" +
                "</otm:LoadedDistance>" +
                "<otm:UnloadedDistance>" +
                "<otm:Distance>" +
                "<otm:DistanceValue>0.0</otm:DistanceValue>" +
                "<otm:DistanceUOMGid>" +
                "<otm:Gid>" +
                "<otm:Xid>MI</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:DistanceUOMGid>" +
                "</otm:Distance>" +
                "</otm:UnloadedDistance>" +
                "<otm:StopCount>4</otm:StopCount>" +
                "<otm:IsTemperatureControl>N</otm:IsTemperatureControl>" +
                "<otm:LatestStartDt>" +
                "<otm:GLogDate>20181018211809</otm:GLogDate>" +
                "<otm:TZId>Etc/GMT-2</otm:TZId>" +
                "<otm:TZOffset>+02:00</otm:TZOffset>" +
                "</otm:LatestStartDt>" +
                "<otm:SourceLocationRef>" +
                "<otm:LocationRef>" +
                "<otm:LocationGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>80</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:LocationGid>" +
                "</otm:LocationRef>" +
                "</otm:SourceLocationRef>" +
                "</otm:ShipmentHeader>" +
                "<otm:ShipmentHeader2>" +
                "<otm:Perspective>B</otm:Perspective>" +
                "<otm:ItineraryGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>ROTA_100</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:ItineraryGid>" +
                "<otm:ParentLegGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>1</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:ParentLegGid>" +
                "<otm:ShipmentTypeGid>" +
                "<otm:Gid>" +
                "<otm:Xid>TRANSPORT</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:ShipmentTypeGid>" +
                "<otm:WeightUtil>0.0</otm:WeightUtil>" +
                "<otm:VolumeUtil>0.0</otm:VolumeUtil>" +
                "<otm:EquipRefUnitUtil>0.0</otm:EquipRefUnitUtil>" +
                "<otm:DutyPaid>NA</otm:DutyPaid>" +
                "</otm:ShipmentHeader2>" +
                "<otm:SEquipment>" +
                "<otm:SEquipmentGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>7003</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:SEquipmentGid>" +
                "<otm:EquipmentGroupGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>VAN</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:EquipmentGroupGid>" +
                "<otm:IsFreight>Y</otm:IsFreight>" +
                "</otm:SEquipment>" +
                "<otm:ShipmentStop>" +
                "<otm:StopSequence>1</otm:StopSequence>" +
                "<otm:LocationRef>" +
                "<otm:LocationGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>80</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:LocationGid>" +
                "</otm:LocationRef>" +
                "<otm:ArrivalTime>" +
                "<otm:EventTime>" +
                "<otm:PlannedTime>" +
                "<otm:GLogDate>20181018211809</otm:GLogDate>" +
                "<otm:TZId>Etc/GMT-2</otm:TZId>" +
                "<otm:TZOffset>+02:00</otm:TZOffset>" +
                "</otm:PlannedTime>" +
                "<otm:EstimatedTime>" +
                "<otm:GLogDate>20181018211809</otm:GLogDate>" +
                "<otm:TZId>Etc/GMT-2</otm:TZId>" +
                "<otm:TZOffset>+02:00</otm:TZOffset>" +
                "</otm:EstimatedTime>" +
                "<otm:IsPlannedTimeFixed>N</otm:IsPlannedTimeFixed>" +
                "</otm:EventTime>" +
                "</otm:ArrivalTime>" +
                "<otm:DepartureTime>" +
                "<otm:EventTime>" +
                "<otm:PlannedTime>" +
                "<otm:GLogDate>20181019070000</otm:GLogDate>" +
                "<otm:TZId>Etc/GMT-2</otm:TZId>" +
                "<otm:TZOffset>+02:00</otm:TZOffset>" +
                "</otm:PlannedTime>" +
                "<otm:EstimatedTime>" +
                "<otm:GLogDate>20181019070000</otm:GLogDate>" +
                "<otm:TZId>Etc/GMT-2</otm:TZId>" +
                "<otm:TZOffset>+02:00</otm:TZOffset>" +
                "</otm:EstimatedTime>" +
                "<otm:IsPlannedTimeFixed>N</otm:IsPlannedTimeFixed>" +
                "</otm:EventTime>" +
                "</otm:DepartureTime>" +
                "<otm:ShipmentStopDetail>" +
                "<otm:Activity>P</otm:Activity>" +
                "<otm:ShipUnitGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>8007</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:ShipUnitGid>" +
                "</otm:ShipmentStopDetail>" +
                "<otm:ShipmentStopDetail>" +
                "<otm:Activity>P</otm:Activity>" +
                "<otm:ShipUnitGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>8008</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:ShipUnitGid>" +
                "</otm:ShipmentStopDetail>" +
                "<otm:ShipmentStopDetail>" +
                "<otm:Activity>P</otm:Activity>" +
                "<otm:ShipUnitGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>8009</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:ShipUnitGid>" +
                "</otm:ShipmentStopDetail>" +
                "<otm:ShipmentStopDetail>" +
                "<otm:Activity>P</otm:Activity>" +
                "<otm:ShipUnitGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>8010</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:ShipUnitGid>" +
                "</otm:ShipmentStopDetail>" +
                "</otm:ShipmentStop>" +
                "<otm:ShipmentStop>" +
                "<otm:StopSequence>2</otm:StopSequence>" +
                "<otm:LocationRef>" +
                "<otm:LocationGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>100</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:LocationGid>" +
                "</otm:LocationRef>" +
                "<otm:ArrivalTime>" +
                "<otm:EventTime>" +
                "<otm:PlannedTime>" +
                "<otm:GLogDate>20181019020915</otm:GLogDate>" +
                "<otm:TZId>Brazil/East</otm:TZId>" +
                "<otm:TZOffset>-03:00</otm:TZOffset>" +
                "</otm:PlannedTime>" +
                "<otm:EstimatedTime>" +
                "<otm:GLogDate>20181019020915</otm:GLogDate>" +
                "<otm:TZId>Brazil/East</otm:TZId>" +
                "<otm:TZOffset>-03:00</otm:TZOffset>" +
                "</otm:EstimatedTime>" +
                "<otm:IsPlannedTimeFixed>N</otm:IsPlannedTimeFixed>" +
                "</otm:EventTime>" +
                "</otm:ArrivalTime>" +
                "<otm:DepartureTime>" +
                "<otm:EventTime>" +
                "<otm:PlannedTime>" +
                "<otm:GLogDate>20181019020915</otm:GLogDate>" +
                "<otm:TZId>Brazil/East</otm:TZId>" +
                "<otm:TZOffset>-03:00</otm:TZOffset>" +
                "</otm:PlannedTime>" +
                "<otm:EstimatedTime>" +
                "<otm:GLogDate>20181019020915</otm:GLogDate>" +
                "<otm:TZId>Brazil/East</otm:TZId>" +
                "<otm:TZOffset>-03:00</otm:TZOffset>" +
                "</otm:EstimatedTime>" +
                "<otm:IsPlannedTimeFixed>N</otm:IsPlannedTimeFixed>" +
                "</otm:EventTime>" +
                "</otm:DepartureTime>" +
                "<otm:ShipmentStopDetail>" +
                "<otm:Activity>D</otm:Activity>" +
                "<otm:ShipUnitGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>8007</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:ShipUnitGid>" +
                "</otm:ShipmentStopDetail>" +
                "<otm:ShipmentStopDetail>" +
                "<otm:Activity>D</otm:Activity>" +
                "<otm:ShipUnitGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>8009</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:ShipUnitGid>" +
                "</otm:ShipmentStopDetail>" +
                "</otm:ShipmentStop>" +
                "<otm:ShipmentStop>" +
                "<otm:StopSequence>3</otm:StopSequence>" +
                "<otm:LocationRef>" +
                "<otm:LocationGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>032</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:LocationGid>" +
                "</otm:LocationRef>" +
                "<otm:ArrivalTime>" +
                "<otm:EventTime>" +
                "<otm:PlannedTime>" +
                "<otm:GLogDate>20181019001454</otm:GLogDate>" +
                "<otm:TZId>Brazil/Acre</otm:TZId>" +
                "<otm:TZOffset>-05:00</otm:TZOffset>" +
                "</otm:PlannedTime>" +
                "<otm:EstimatedTime>" +
                "<otm:GLogDate>20181019001454</otm:GLogDate>" +
                "<otm:TZId>Brazil/Acre</otm:TZId>" +
                "<otm:TZOffset>-05:00</otm:TZOffset>" +
                "</otm:EstimatedTime>" +
                "<otm:IsPlannedTimeFixed>N</otm:IsPlannedTimeFixed>" +
                "</otm:EventTime>" +
                "</otm:ArrivalTime>" +
                "<otm:DepartureTime>" +
                "<otm:EventTime>" +
                "<otm:PlannedTime>" +
                "<otm:GLogDate>20181019001454</otm:GLogDate>" +
                "<otm:TZId>Brazil/Acre</otm:TZId>" +
                "<otm:TZOffset>-05:00</otm:TZOffset>" +
                "</otm:PlannedTime>" +
                "<otm:EstimatedTime>" +
                "<otm:GLogDate>20181019001454</otm:GLogDate>" +
                "<otm:TZId>Brazil/Acre</otm:TZId>" +
                "<otm:TZOffset>-05:00</otm:TZOffset>" +
                "</otm:EstimatedTime>" +
                "<otm:IsPlannedTimeFixed>N</otm:IsPlannedTimeFixed>" +
                "</otm:EventTime>" +
                "</otm:DepartureTime>" +
                "<otm:ShipmentStopDetail>" +
                "<otm:Activity>D</otm:Activity>" +
                "<otm:ShipUnitGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>8008</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:ShipUnitGid>" +
                "</otm:ShipmentStopDetail>" +
                "</otm:ShipmentStop>" +
                "<otm:ShipmentStop>" +
                "<otm:StopSequence>4</otm:StopSequence>" +
                "<otm:LocationRef>" +
                "<otm:LocationGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>041</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:LocationGid>" +
                "</otm:LocationRef>" +
                "<otm:ArrivalTime>" +
                "<otm:EventTime>" +
                "<otm:PlannedTime>" +
                "<otm:GLogDate>20181019012032</otm:GLogDate>" +
                "<otm:TZId>Brazil/West</otm:TZId>" +
                "<otm:TZOffset>-04:00</otm:TZOffset>" +
                "</otm:PlannedTime>" +
                "<otm:EstimatedTime>" +
                "<otm:GLogDate>20181019012032</otm:GLogDate>" +
                "<otm:TZId>Brazil/West</otm:TZId>" +
                "<otm:TZOffset>-04:00</otm:TZOffset>" +
                "</otm:EstimatedTime>" +
                "<otm:IsPlannedTimeFixed>N</otm:IsPlannedTimeFixed>" +
                "</otm:EventTime>" +
                "</otm:ArrivalTime>" +
                "<otm:DepartureTime>" +
                "<otm:EventTime>" +
                "<otm:PlannedTime>" +
                "<otm:GLogDate>20181019012032</otm:GLogDate>" +
                "<otm:TZId>Brazil/West</otm:TZId>" +
                "<otm:TZOffset>-04:00</otm:TZOffset>" +
                "</otm:PlannedTime>" +
                "<otm:EstimatedTime>" +
                "<otm:GLogDate>20181019012032</otm:GLogDate>" +
                "<otm:TZId>Brazil/West</otm:TZId>" +
                "<otm:TZOffset>-04:00</otm:TZOffset>" +
                "</otm:EstimatedTime>" +
                "<otm:IsPlannedTimeFixed>N</otm:IsPlannedTimeFixed>" +
                "</otm:EventTime>" +
                "</otm:DepartureTime>" +
                "<otm:ShipmentStopDetail>" +
                "<otm:Activity>D</otm:Activity>" +
                "<otm:ShipUnitGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>8010</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:ShipUnitGid>" +
                "</otm:ShipmentStopDetail>" +
                "</otm:ShipmentStop>" +
                "<otm:Location>" +
                "<otm:TransactionCode>NP</otm:TransactionCode>" +
                "<otm:LocationGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>032</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:LocationGid>" +
                "<otm:LocationName>032AVENIDA</otm:LocationName>" +
                "<otm:Address>" +
                "<otm:AddressLines>" +
                "<otm:SequenceNumber>1</otm:SequenceNumber>" +
                "<otm:AddressLine>RUA DOM AQUINO,1379</otm:AddressLine>" +
                "</otm:AddressLines>" +
                "<otm:City>CAMPO GRANDE</otm:City>" +
                "<otm:Province>MATO GROSSO DO SUL</otm:Province>" +
                "<otm:ProvinceCode>MS</otm:ProvinceCode>" +
                "<otm:PostalCode>79002-181</otm:PostalCode>" +
                "<otm:CountryCode3Gid>" +
                "<otm:Gid>" +
                "<otm:Xid>BR</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:CountryCode3Gid>" +
                "<otm:CountryCode>" +
                "<otm:CountryCode3Gid>" +
                "<otm:Gid>" +
                "<otm:Xid>BR</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:CountryCode3Gid>" +
                "</otm:CountryCode>" +
                "<otm:TimeZoneGid>" +
                "<otm:Gid>" +
                "<otm:Xid>Brazil/Acre</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:TimeZoneGid>" +
                "<otm:Latitude>-20.46129</otm:Latitude>" +
                "<otm:Longitude>-54.61797</otm:Longitude>" +
                "</otm:Address>" +
                "<otm:Contact>" +
                "<otm:ContactGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>032</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:ContactGid>" +
                "</otm:Contact>" +
                "<otm:LocationRole>" +
                "<otm:LocationRoleGid>" +
                "<otm:Gid>" +
                "<otm:Xid>SHIPFROM/SHIPTO</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:LocationRoleGid>" +
                "</otm:LocationRole>" +
                "</otm:Location>" +
                "<otm:Location>" +
                "<otm:TransactionCode>NP</otm:TransactionCode>" +
                "<otm:LocationGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>041</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:LocationGid>" +
                "<otm:LocationName>041AVENIDA</otm:LocationName>" +
                "<otm:Address>" +
                "<otm:AddressLines>" +
                "<otm:SequenceNumber>1</otm:SequenceNumber>" +
                "<otm:AddressLine>RUA BRILHANTE, 2702</otm:AddressLine>" +
                "</otm:AddressLines>" +
                "<otm:City>CAMPO GRANDE</otm:City>" +
                "<otm:Province>MATO GROSSO DO SUL</otm:Province>" +
                "<otm:ProvinceCode>MS</otm:ProvinceCode>" +
                "<otm:PostalCode>79005-250</otm:PostalCode>" +
                "<otm:CountryCode3Gid>" +
                "<otm:Gid>" +
                "<otm:Xid>BR</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:CountryCode3Gid>" +
                "<otm:CountryCode>" +
                "<otm:CountryCode3Gid>" +
                "<otm:Gid>" +
                "<otm:Xid>BR</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:CountryCode3Gid>" +
                "</otm:CountryCode>" +
                "<otm:TimeZoneGid>" +
                "<otm:Gid>" +
                "<otm:Xid>Brazil/West</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:TimeZoneGid>" +
                "<otm:Latitude>-20.47922</otm:Latitude>" +
                "<otm:Longitude>-54.6412</otm:Longitude>" +
                "</otm:Address>" +
                "<otm:Contact>" +
                "<otm:ContactGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>041</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:ContactGid>" +
                "</otm:Contact>" +
                "<otm:LocationRole>" +
                "<otm:LocationRoleGid>" +
                "<otm:Gid>" +
                "<otm:Xid>SHIPFROM/SHIPTO</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:LocationRoleGid>" +
                "</otm:LocationRole>" +
                "</otm:Location>" +
                "<otm:Location>" +
                "<otm:TransactionCode>NP</otm:TransactionCode>" +
                "<otm:LocationGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>100</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:LocationGid>" +
                "<otm:LocationName>100AVENIDA</otm:LocationName>" +
                "<otm:Address>" +
                "<otm:City>CAMPO GRANDE</otm:City>" +
                "<otm:Province>MATO GROSSO DO SUL</otm:Province>" +
                "<otm:ProvinceCode>MS</otm:ProvinceCode>" +
                "<otm:PostalCode>79117-010</otm:PostalCode>" +
                "<otm:CountryCode3Gid>" +
                "<otm:Gid>" +
                "<otm:Xid>BR</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:CountryCode3Gid>" +
                "<otm:CountryCode>" +
                "<otm:CountryCode3Gid>" +
                "<otm:Gid>" +
                "<otm:Xid>BR</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:CountryCode3Gid>" +
                "</otm:CountryCode>" +
                "<otm:TimeZoneGid>" +
                "<otm:Gid>" +
                "<otm:Xid>Brazil/East</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:TimeZoneGid>" +
                "<otm:Latitude>-20.43482</otm:Latitude>" +
                "<otm:Longitude>-54.62852</otm:Longitude>" +
                "</otm:Address>" +
                "<otm:LocationRole>" +
                "<otm:LocationRoleGid>" +
                "<otm:Gid>" +
                "<otm:Xid>SHIPFROM/SHIPTO</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:LocationRoleGid>" +
                "</otm:LocationRole>" +
                "</otm:Location>" +
                "<otm:Location>" +
                "<otm:TransactionCode>NP</otm:TransactionCode>" +
                "<otm:LocationGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>80</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:LocationGid>" +
                "<otm:LocationName>CD CAMPO GRANDE</otm:LocationName>" +
                "<otm:Address>" +
                "<otm:AddressLines>" +
                "<otm:SequenceNumber>1</otm:SequenceNumber>" +
                "<otm:AddressLine>RUA ELIAS NACHIF, 123</otm:AddressLine>" +
                "</otm:AddressLines>" +
                "<otm:City>CAMPO GRANDE</otm:City>" +
                "<otm:ProvinceCode>MS</otm:ProvinceCode>" +
                "<otm:CountryCode3Gid>" +
                "<otm:Gid>" +
                "<otm:Xid>BR</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:CountryCode3Gid>" +
                "<otm:CountryCode>" +
                "<otm:CountryCode3Gid>" +
                "<otm:Gid>" +
                "<otm:Xid>BR</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:CountryCode3Gid>" +
                "</otm:CountryCode>" +
                "<otm:TimeZoneGid>" +
                "<otm:Gid>" +
                "<otm:Xid>Etc/GMT-2</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:TimeZoneGid>" +
                "<otm:Latitude>-20.4233</otm:Latitude>" +
                "<otm:Longitude>-54.58067</otm:Longitude>" +
                "</otm:Address>" +
                "<otm:Contact>" +
                "<otm:ContactGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>80</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:ContactGid>" +
                "</otm:Contact>" +
                "<otm:LocationRole>" +
                "<otm:LocationRoleGid>" +
                "<otm:Gid>" +
                "<otm:Xid>SHIPFROM/SHIPTO</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:LocationRoleGid>" +
                "</otm:LocationRole>" +
                "</otm:Location>" +
                "<otm:Location>" +
                "<otm:TransactionCode>NP</otm:TransactionCode>" +
                "<otm:LocationGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>SERVPROV</otm:DomainName>" +
                "<otm:Xid>20192551000101</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:LocationGid>" +
                "<otm:LocationName>RF TRANSPORTES - CGD</otm:LocationName>" +
                "<otm:Address>" +
                "<otm:AddressLines>" +
                "<otm:SequenceNumber>1</otm:SequenceNumber>" +
                "<otm:AddressLine>RUA SANTA AMELIA 705</otm:AddressLine>" +
                "</otm:AddressLines>" +
                "<otm:AddressLines>" +
                "<otm:SequenceNumber>2</otm:SequenceNumber>" +
                "<otm:AddressLine></otm:AddressLine>" +
                "</otm:AddressLines>" +
                "<otm:City>CAMPO GRANDE</otm:City>" +
                "<otm:Province>MATO GROSSO DO SUL</otm:Province>" +
                "<otm:ProvinceCode>MS</otm:ProvinceCode>" +
                "<otm:PostalCode>79005-240</otm:PostalCode>" +
                "<otm:CountryCode3Gid>" +
                "<otm:Gid>" +
                "<otm:Xid>BR</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:CountryCode3Gid>" +
                "<otm:CountryCode>" +
                "<otm:CountryCode3Gid>" +
                "<otm:Gid>" +
                "<otm:Xid>BR</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:CountryCode3Gid>" +
                "</otm:CountryCode>" +
                "<otm:TimeZoneGid>" +
                "<otm:Gid>" +
                "<otm:Xid>Brazil/West</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:TimeZoneGid>" +
                "<otm:Latitude>-20.47699</otm:Latitude>" +
                "<otm:Longitude>-54.6201</otm:Longitude>" +
                "</otm:Address>" +
                "<otm:Contact>" +
                "<otm:ContactGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>SERVPROV</otm:DomainName>" +
                "<otm:Xid>20192551000101</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:ContactGid>" +
                "<otm:EmailAddress>elaine@cr-ecinfo.com.br</otm:EmailAddress>" +
                "<otm:FirstName>RF_LOGISTICA</otm:FirstName>" +
                "<otm:Phone1>(67)3211-8439</otm:Phone1>" +
                "</otm:Contact>" +
                "<otm:LocationRole>" +
                "<otm:LocationRoleGid>" +
                "<otm:Gid>" +
                "<otm:Xid>CARRIER</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:LocationRoleGid>" +
                "</otm:LocationRole>" +
                "<otm:ServiceProvider>" +
                "<otm:ServiceProviderAlias>" +
                "<otm:ServiceProviderAliasQualifierGid>" +
                "<otm:Gid>" +
                "<otm:Xid>GLOG</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:ServiceProviderAliasQualifierGid>" +
                "<otm:ServiceProviderAliasValue>SERVPROV.20192551000101</otm:ServiceProviderAliasValue>" +
                "</otm:ServiceProviderAlias>" +
                "</otm:ServiceProvider>" +
                "</otm:Location>" +
                "<otm:ShipUnit>" +
                "<otm:ShipUnitGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>8007</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:ShipUnitGid>" +
                "<otm:ShipFromLocationRef>" +
                "<otm:LocationRef>" +
                "<otm:LocationGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>80</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:LocationGid>" +
                "</otm:LocationRef>" +
                "</otm:ShipFromLocationRef>" +
                "<otm:ShipToLocationRef>" +
                "<otm:LocationRef>" +
                "<otm:LocationGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>100</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:LocationGid>" +
                "</otm:LocationRef>" +
                "</otm:ShipToLocationRef>" +
                "<otm:ShipUnitContent>" +
                "<otm:PackagedItemRef>" +
                "<otm:PackagedItemGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>149408018142</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:PackagedItemGid>" +
                "</otm:PackagedItemRef>" +
                "<otm:LineNumber>1</otm:LineNumber>" +
                "<otm:ItemQuantity>" +
                "<otm:WeightVolume>" +
                "<otm:Weight>" +
                "<otm:WeightValue>0.0</otm:WeightValue>" +
                "<otm:WeightUOMGid>" +
                "<otm:Gid>" +
                "<otm:Xid>LB</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:WeightUOMGid>" +
                "</otm:Weight>" +
                "<otm:Volume>" +
                "<otm:VolumeValue>0.0</otm:VolumeValue>" +
                "<otm:VolumeUOMGid>" +
                "<otm:Gid>" +
                "<otm:Xid>CUFT</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:VolumeUOMGid>" +
                "</otm:Volume>" +
                "</otm:WeightVolume>" +
                "<otm:PackagedItemCount>3</otm:PackagedItemCount>" +
                "<otm:DeclaredValue>" +
                "<otm:FinancialAmount>" +
                "<otm:GlobalCurrencyCode>USD</otm:GlobalCurrencyCode>" +
                "<otm:MonetaryAmount>0.0</otm:MonetaryAmount>" +
                "</otm:FinancialAmount>" +
                "</otm:DeclaredValue>" +
                "</otm:ItemQuantity>" +
                "<otm:ReleaseGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>TR_17101803</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:ReleaseGid>" +
                "<otm:ReleaseLineGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>6005</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:ReleaseLineGid>" +
                "<otm:ReleaseShipUnitGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>6007</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:ReleaseShipUnitGid>" +
                "<otm:ReleaseShipUnitLineNumber>1</otm:ReleaseShipUnitLineNumber>" +
                "</otm:ShipUnitContent>" +
                "<otm:ShipUnitCount>1</otm:ShipUnitCount>" +
                "<otm:SEquipmentSShipUnitInfo>" +
                "<otm:SEquipmentGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>7003</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:SEquipmentGid>" +
                "<otm:NumStackingLayers>0</otm:NumStackingLayers>" +
                "<otm:NumLoadingRows>0</otm:NumLoadingRows>" +
                "</otm:SEquipmentSShipUnitInfo>" +
                "</otm:ShipUnit>" +
                "<otm:ShipUnit>" +
                "<otm:ShipUnitGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>8008</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:ShipUnitGid>" +
                "<otm:ShipFromLocationRef>" +
                "<otm:LocationRef>" +
                "<otm:LocationGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>80</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:LocationGid>" +
                "</otm:LocationRef>" +
                "</otm:ShipFromLocationRef>" +
                "<otm:ShipToLocationRef>" +
                "<otm:LocationRef>" +
                "<otm:LocationGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>032</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:LocationGid>" +
                "</otm:LocationRef>" +
                "</otm:ShipToLocationRef>" +
                "<otm:ShipUnitContent>" +
                "<otm:PackagedItemRef>" +
                "<otm:PackagedItemGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>149408018142</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:PackagedItemGid>" +
                "</otm:PackagedItemRef>" +
                "<otm:LineNumber>1</otm:LineNumber>" +
                "<otm:ItemQuantity>" +
                "<otm:WeightVolume>" +
                "<otm:Weight>" +
                "<otm:WeightValue>0.0</otm:WeightValue>" +
                "<otm:WeightUOMGid>" +
                "<otm:Gid>" +
                "<otm:Xid>LB</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:WeightUOMGid>" +
                "</otm:Weight>" +
                "<otm:Volume>" +
                "<otm:VolumeValue>0.0</otm:VolumeValue>" +
                "<otm:VolumeUOMGid>" +
                "<otm:Gid>" +
                "<otm:Xid>CUFT</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:VolumeUOMGid>" +
                "</otm:Volume>" +
                "</otm:WeightVolume>" +
                "<otm:PackagedItemCount>3</otm:PackagedItemCount>" +
                "<otm:DeclaredValue>" +
                "<otm:FinancialAmount>" +
                "<otm:GlobalCurrencyCode>USD</otm:GlobalCurrencyCode>" +
                "<otm:MonetaryAmount>0.0</otm:MonetaryAmount>" +
                "</otm:FinancialAmount>" +
                "</otm:DeclaredValue>" +
                "</otm:ItemQuantity>" +
                "<otm:ReleaseGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>TR_17101801</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:ReleaseGid>" +
                "<otm:ReleaseLineGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>7001</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:ReleaseLineGid>" +
                "<otm:ReleaseShipUnitGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>7001</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:ReleaseShipUnitGid>" +
                "<otm:ReleaseShipUnitLineNumber>1</otm:ReleaseShipUnitLineNumber>" +
                "</otm:ShipUnitContent>" +
                "<otm:ShipUnitCount>1</otm:ShipUnitCount>" +
                "<otm:SEquipmentSShipUnitInfo>" +
                "<otm:SEquipmentGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>7003</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:SEquipmentGid>" +
                "<otm:NumStackingLayers>0</otm:NumStackingLayers>" +
                "<otm:NumLoadingRows>0</otm:NumLoadingRows>" +
                "</otm:SEquipmentSShipUnitInfo>" +
                "</otm:ShipUnit>" +
                "<otm:ShipUnit>" +
                "<otm:ShipUnitGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>8009</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:ShipUnitGid>" +
                "<otm:ShipFromLocationRef>" +
                "<otm:LocationRef>" +
                "<otm:LocationGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>80</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:LocationGid>" +
                "</otm:LocationRef>" +
                "</otm:ShipFromLocationRef>" +
                "<otm:ShipToLocationRef>" +
                "<otm:LocationRef>" +
                "<otm:LocationGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>100</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:LocationGid>" +
                "</otm:LocationRef>" +
                "</otm:ShipToLocationRef>" +
                "<otm:ShipUnitContent>" +
                "<otm:PackagedItemRef>" +
                "<otm:PackagedItemGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>149410016837</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:PackagedItemGid>" +
                "</otm:PackagedItemRef>" +
                "<otm:LineNumber>1</otm:LineNumber>" +
                "<otm:ItemQuantity>" +
                "<otm:WeightVolume>" +
                "<otm:Weight>" +
                "<otm:WeightValue>0.0</otm:WeightValue>" +
                "<otm:WeightUOMGid>" +
                "<otm:Gid>" +
                "<otm:Xid>LB</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:WeightUOMGid>" +
                "</otm:Weight>" +
                "<otm:Volume>" +
                "<otm:VolumeValue>0.0</otm:VolumeValue>" +
                "<otm:VolumeUOMGid>" +
                "<otm:Gid>" +
                "<otm:Xid>CUFT</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:VolumeUOMGid>" +
                "</otm:Volume>" +
                "</otm:WeightVolume>" +
                "<otm:PackagedItemCount>3</otm:PackagedItemCount>" +
                "<otm:DeclaredValue>" +
                "<otm:FinancialAmount>" +
                "<otm:GlobalCurrencyCode>USD</otm:GlobalCurrencyCode>" +
                "<otm:MonetaryAmount>0.0</otm:MonetaryAmount>" +
                "</otm:FinancialAmount>" +
                "</otm:DeclaredValue>" +
                "</otm:ItemQuantity>" +
                "<otm:ReleaseGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>TR_17101804</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:ReleaseGid>" +
                "<otm:ReleaseLineGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>7002</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:ReleaseLineGid>" +
                "<otm:ReleaseShipUnitGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>7002</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:ReleaseShipUnitGid>" +
                "<otm:ReleaseShipUnitLineNumber>1</otm:ReleaseShipUnitLineNumber>" +
                "</otm:ShipUnitContent>" +
                "<otm:ShipUnitCount>1</otm:ShipUnitCount>" +
                "<otm:SEquipmentSShipUnitInfo>" +
                "<otm:SEquipmentGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>7003</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:SEquipmentGid>" +
                "<otm:NumStackingLayers>0</otm:NumStackingLayers>" +
                "<otm:NumLoadingRows>0</otm:NumLoadingRows>" +
                "</otm:SEquipmentSShipUnitInfo>" +
                "</otm:ShipUnit>" +
                "<otm:ShipUnit>" +
                "<otm:ShipUnitGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>8010</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:ShipUnitGid>" +
                "<otm:ShipFromLocationRef>" +
                "<otm:LocationRef>" +
                "<otm:LocationGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>80</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:LocationGid>" +
                "</otm:LocationRef>" +
                "</otm:ShipFromLocationRef>" +
                "<otm:ShipToLocationRef>" +
                "<otm:LocationRef>" +
                "<otm:LocationGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>041</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:LocationGid>" +
                "</otm:LocationRef>" +
                "</otm:ShipToLocationRef>" +
                "<otm:ShipUnitContent>" +
                "<otm:PackagedItemRef>" +
                "<otm:PackagedItemGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>149408018142</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:PackagedItemGid>" +
                "</otm:PackagedItemRef>" +
                "<otm:LineNumber>1</otm:LineNumber>" +
                "<otm:ItemQuantity>" +
                "<otm:WeightVolume>" +
                "<otm:Weight>" +
                "<otm:WeightValue>0.0</otm:WeightValue>" +
                "<otm:WeightUOMGid>" +
                "<otm:Gid>" +
                "<otm:Xid>LB</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:WeightUOMGid>" +
                "</otm:Weight>" +
                "<otm:Volume>" +
                "<otm:VolumeValue>0.0</otm:VolumeValue>" +
                "<otm:VolumeUOMGid>" +
                "<otm:Gid>" +
                "<otm:Xid>CUFT</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:VolumeUOMGid>" +
                "</otm:Volume>" +
                "</otm:WeightVolume>" +
                "<otm:PackagedItemCount>3</otm:PackagedItemCount>" +
                "<otm:DeclaredValue>" +
                "<otm:FinancialAmount>" +
                "<otm:GlobalCurrencyCode>USD</otm:GlobalCurrencyCode>" +
                "<otm:MonetaryAmount>0.0</otm:MonetaryAmount>" +
                "</otm:FinancialAmount>" +
                "</otm:DeclaredValue>" +
                "</otm:ItemQuantity>" +
                "<otm:ReleaseGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>TR_17101802</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:ReleaseGid>" +
                "<otm:ReleaseLineGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>6004</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:ReleaseLineGid>" +
                "<otm:ReleaseShipUnitGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>6006</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:ReleaseShipUnitGid>" +
                "<otm:ReleaseShipUnitLineNumber>1</otm:ReleaseShipUnitLineNumber>" +
                "</otm:ShipUnitContent>" +
                "<otm:ShipUnitCount>1</otm:ShipUnitCount>" +
                "<otm:SEquipmentSShipUnitInfo>" +
                "<otm:SEquipmentGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>7003</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:SEquipmentGid>" +
                "<otm:NumStackingLayers>0</otm:NumStackingLayers>" +
                "<otm:NumLoadingRows>0</otm:NumLoadingRows>" +
                "</otm:SEquipmentSShipUnitInfo>" +
                "</otm:ShipUnit>" +
                "<otm:Release>" +
                "<otm:ReleaseGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>TR_17101801</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:ReleaseGid>" +
                "<otm:TransactionCode>NP</otm:TransactionCode>" +
                "<otm:ShipFromLocationRef>" +
                "<otm:LocationRef>" +
                "<otm:LocationGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>80</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:LocationGid>" +
                "</otm:LocationRef>" +
                "</otm:ShipFromLocationRef>" +
                "<otm:ShipToLocationRef>" +
                "<otm:LocationRef>" +
                "<otm:LocationGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>032</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:LocationGid>" +
                "</otm:LocationRef>" +
                "</otm:ShipToLocationRef>" +
                "<otm:TimeWindow>" +
                "<otm:EarlyPickupDt>" +
                "<otm:GLogDate>20181018100000</otm:GLogDate>" +
                "<otm:TZId>Etc/GMT-2</otm:TZId>" +
                "<otm:TZOffset>+02:00</otm:TZOffset>" +
                "</otm:EarlyPickupDt>" +
                "<otm:PickupIsAppt>N</otm:PickupIsAppt>" +
                "<otm:DeliveryIsAppt>N</otm:DeliveryIsAppt>" +
                "</otm:TimeWindow>" +
                "<otm:ReleaseLine>" +
                "<otm:ReleaseLineGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>7001</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:ReleaseLineGid>" +
                "<otm:PackagedItemRef>" +
                "<otm:PackagedItemGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>149408018142</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:PackagedItemGid>" +
                "</otm:PackagedItemRef>" +
                "<otm:ItemQuantity>" +
                "<otm:WeightVolume>" +
                "<otm:Weight>" +
                "<otm:WeightValue>0.0</otm:WeightValue>" +
                "<otm:WeightUOMGid>" +
                "<otm:Gid>" +
                "<otm:Xid>LB</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:WeightUOMGid>" +
                "</otm:Weight>" +
                "<otm:Volume>" +
                "<otm:VolumeValue>0.0</otm:VolumeValue>" +
                "<otm:VolumeUOMGid>" +
                "<otm:Gid>" +
                "<otm:Xid>CUFT</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:VolumeUOMGid>" +
                "</otm:Volume>" +
                "</otm:WeightVolume>" +
                "<otm:PackagedItemCount>3</otm:PackagedItemCount>" +
                "<otm:DeclaredValue>" +
                "<otm:FinancialAmount>" +
                "<otm:GlobalCurrencyCode>USD</otm:GlobalCurrencyCode>" +
                "<otm:MonetaryAmount>0.0</otm:MonetaryAmount>" +
                "</otm:FinancialAmount>" +
                "</otm:DeclaredValue>" +
                "</otm:ItemQuantity>" +
                "</otm:ReleaseLine>" +
                "<otm:ShipUnit>" +
                "<otm:ShipUnitGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>7001</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:ShipUnitGid>" +
                "<otm:ShipUnitContent>" +
                "<otm:PackagedItemRef>" +
                "<otm:PackagedItemGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>149408018142</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:PackagedItemGid>" +
                "</otm:PackagedItemRef>" +
                "<otm:LineNumber>1</otm:LineNumber>" +
                "<otm:ItemQuantity>" +
                "<otm:IsSplitAllowed>Y</otm:IsSplitAllowed>" +
                "<otm:WeightVolume>" +
                "<otm:Weight>" +
                "<otm:WeightValue>0.0</otm:WeightValue>" +
                "<otm:WeightUOMGid>" +
                "<otm:Gid>" +
                "<otm:Xid>LB</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:WeightUOMGid>" +
                "</otm:Weight>" +
                "<otm:Volume>" +
                "<otm:VolumeValue>0.0</otm:VolumeValue>" +
                "<otm:VolumeUOMGid>" +
                "<otm:Gid>" +
                "<otm:Xid>CUFT</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:VolumeUOMGid>" +
                "</otm:Volume>" +
                "</otm:WeightVolume>" +
                "<otm:PackagedItemCount>3</otm:PackagedItemCount>" +
                "</otm:ItemQuantity>" +
                "<otm:ReleaseGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>TR_17101801</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:ReleaseGid>" +
                "<otm:ReleaseLineGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>7001</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:ReleaseLineGid>" +
                "</otm:ShipUnitContent>" +
                "<otm:ShipUnitCount>1</otm:ShipUnitCount>" +
                "</otm:ShipUnit>" +
                "<otm:TotalWeightVolume>" +
                "<otm:WeightVolume>" +
                "<otm:Weight>" +
                "<otm:WeightValue>0.0</otm:WeightValue>" +
                "<otm:WeightUOMGid>" +
                "<otm:Gid>" +
                "<otm:Xid>LB</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:WeightUOMGid>" +
                "</otm:Weight>" +
                "<otm:Volume>" +
                "<otm:VolumeValue>0.0</otm:VolumeValue>" +
                "<otm:VolumeUOMGid>" +
                "<otm:Gid>" +
                "<otm:Xid>CUFT</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:VolumeUOMGid>" +
                "</otm:Volume>" +
                "</otm:WeightVolume>" +
                "</otm:TotalWeightVolume>" +
                "<otm:ReleaseRefnum>" +
                "<otm:ReleaseRefnumQualifierGid>" +
                "<otm:Gid>" +
                "<otm:Xid>LINE_OF_BUSINESS</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:ReleaseRefnumQualifierGid>" +
                "<otm:ReleaseRefnumValue>6 GIOVANA</otm:ReleaseRefnumValue>" +
                "</otm:ReleaseRefnum>" +
                "<otm:ReleaseRefnum>" +
                "<otm:ReleaseRefnumQualifierGid>" +
                "<otm:Gid>" +
                "<otm:Xid>GLOG</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:ReleaseRefnumQualifierGid>" +
                "<otm:ReleaseRefnumValue>GAVE.TR_17101801</otm:ReleaseRefnumValue>" +
                "</otm:ReleaseRefnum>" +
                "<otm:ReleaseStatus>" +
                "<otm:StatusTypeGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>OR_HD_LOAD_STATUS</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:StatusTypeGid>" +
                "<otm:StatusValueGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>OR_NOT_READY_TO_LOAD</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:StatusValueGid>" +
                "</otm:ReleaseStatus>" +
                "<otm:ReleaseStatus>" +
                "<otm:StatusTypeGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>ORDER_RELEASE GUARANTEED DELIVERY</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:StatusTypeGid>" +
                "<otm:StatusValueGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>ORDER_RELEASE GUARANTEED DELIVERY_NOT STARTED</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:StatusValueGid>" +
                "</otm:ReleaseStatus>" +
                "<otm:ReleaseStatus>" +
                "<otm:StatusTypeGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>PLANNING</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:StatusTypeGid>" +
                "<otm:StatusValueGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>PLANNING_PLANNED - FINAL</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:StatusValueGid>" +
                "</otm:ReleaseStatus>" +
                "<otm:ReleaseStatus>" +
                "<otm:StatusTypeGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>READY_TO_SHIP</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:StatusTypeGid>" +
                "<otm:StatusValueGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>READY_TO_SHIP_NEW</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:StatusValueGid>" +
                "</otm:ReleaseStatus>" +
                "<otm:ReleaseStatus>" +
                "<otm:StatusTypeGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>ORDER RELEASE SKU LINK</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:StatusTypeGid>" +
                "<otm:StatusValueGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>ORDER RELEASE SKU LINK_INACTIVE</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:StatusValueGid>" +
                "</otm:ReleaseStatus>" +
                "<otm:ReleaseStatus>" +
                "<otm:StatusTypeGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>CANCELLED</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:StatusTypeGid>" +
                "<otm:StatusValueGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>CANCELLED_NOT CANCELLED</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:StatusValueGid>" +
                "</otm:ReleaseStatus>" +
                "<otm:ReleaseStatus>" +
                "<otm:StatusTypeGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>QUANTITY VALIDATION SOURCE</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:StatusTypeGid>" +
                "<otm:StatusValueGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>QUANTITY VALIDATION SOURCE_NOT STARTED</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:StatusValueGid>" +
                "</otm:ReleaseStatus>" +
                "<otm:ReleaseStatus>" +
                "<otm:StatusTypeGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>QUANTITY VALIDATION DESTINATION</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:StatusTypeGid>" +
                "<otm:StatusValueGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>QUANTITY VALIDATION DESTINATION_NOT STARTED</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:StatusValueGid>" +
                "</otm:ReleaseStatus>" +
                "<otm:ReleaseStatus>" +
                "<otm:StatusTypeGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>FLEET ASSIGNMENT OR</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:StatusTypeGid>" +
                "<otm:StatusValueGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>FLEET ASSIGNMENT OR_NOT ASSIGNED</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:StatusValueGid>" +
                "</otm:ReleaseStatus>" +
                "<otm:ReleaseStatus>" +
                "<otm:StatusTypeGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>ORDER MODIFIED</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:StatusTypeGid>" +
                "<otm:StatusValueGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>ORDER MODIFIED_NO MODIFICATION</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:StatusValueGid>" +
                "</otm:ReleaseStatus>" +
                "<otm:ReleaseStatus>" +
                "<otm:StatusTypeGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>BILLED</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:StatusTypeGid>" +
                "<otm:StatusValueGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>BILLED_NOT_BILLED</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:StatusValueGid>" +
                "</otm:ReleaseStatus>" +
                "<otm:ReleaseStatus>" +
                "<otm:StatusTypeGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>PLANNING_SELL</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:StatusTypeGid>" +
                "<otm:StatusValueGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>PLANNING_SELL_UNSCHEDULED</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:StatusValueGid>" +
                "</otm:ReleaseStatus>" +
                "<otm:ReleaseStatus>" +
                "<otm:StatusTypeGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>ORDER DELIVERED</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:StatusTypeGid>" +
                "<otm:StatusValueGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>ORDER DELIVERED_NOT STARTED</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:StatusValueGid>" +
                "</otm:ReleaseStatus>" +
                "<otm:OrderMovement>" +
                "<otm:OrderMovementGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>6007</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:OrderMovementGid>" +
                "<otm:TransactionCode>NP</otm:TransactionCode>" +
                "<otm:OrderMovementD>" +
                "<otm:ShipUnit>" +
                "<otm:ShipUnitGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>8008</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:ShipUnitGid>" +
                "<otm:ShipFromLocationRef>" +
                "<otm:LocationRef>" +
                "<otm:LocationGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>80</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:LocationGid>" +
                "</otm:LocationRef>" +
                "</otm:ShipFromLocationRef>" +
                "<otm:ShipToLocationRef>" +
                "<otm:LocationRef>" +
                "<otm:LocationGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>032</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:LocationGid>" +
                "</otm:LocationRef>" +
                "</otm:ShipToLocationRef>" +
                "<otm:ShipUnitCount>1</otm:ShipUnitCount>" +
                "</otm:ShipUnit>" +
                "</otm:OrderMovementD>" +
                "</otm:OrderMovement>" +
                "</otm:Release>" +
                "<otm:Release>" +
                "<otm:ReleaseGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>TR_17101802</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:ReleaseGid>" +
                "<otm:TransactionCode>NP</otm:TransactionCode>" +
                "<otm:ShipFromLocationRef>" +
                "<otm:LocationRef>" +
                "<otm:LocationGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>80</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:LocationGid>" +
                "</otm:LocationRef>" +
                "</otm:ShipFromLocationRef>" +
                "<otm:ShipToLocationRef>" +
                "<otm:LocationRef>" +
                "<otm:LocationGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>041</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:LocationGid>" +
                "</otm:LocationRef>" +
                "</otm:ShipToLocationRef>" +
                "<otm:TimeWindow>" +
                "<otm:EarlyPickupDt>" +
                "<otm:GLogDate>20181018100000</otm:GLogDate>" +
                "<otm:TZId>Etc/GMT-2</otm:TZId>" +
                "<otm:TZOffset>+02:00</otm:TZOffset>" +
                "</otm:EarlyPickupDt>" +
                "<otm:PickupIsAppt>N</otm:PickupIsAppt>" +
                "<otm:DeliveryIsAppt>N</otm:DeliveryIsAppt>" +
                "</otm:TimeWindow>" +
                "<otm:ReleaseLine>" +
                "<otm:ReleaseLineGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>6004</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:ReleaseLineGid>" +
                "<otm:PackagedItemRef>" +
                "<otm:PackagedItemGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>149408018142</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:PackagedItemGid>" +
                "</otm:PackagedItemRef>" +
                "<otm:ItemQuantity>" +
                "<otm:WeightVolume>" +
                "<otm:Weight>" +
                "<otm:WeightValue>0.0</otm:WeightValue>" +
                "<otm:WeightUOMGid>" +
                "<otm:Gid>" +
                "<otm:Xid>LB</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:WeightUOMGid>" +
                "</otm:Weight>" +
                "<otm:Volume>" +
                "<otm:VolumeValue>0.0</otm:VolumeValue>" +
                "<otm:VolumeUOMGid>" +
                "<otm:Gid>" +
                "<otm:Xid>CUFT</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:VolumeUOMGid>" +
                "</otm:Volume>" +
                "</otm:WeightVolume>" +
                "<otm:PackagedItemCount>3</otm:PackagedItemCount>" +
                "<otm:DeclaredValue>" +
                "<otm:FinancialAmount>" +
                "<otm:GlobalCurrencyCode>USD</otm:GlobalCurrencyCode>" +
                "<otm:MonetaryAmount>0.0</otm:MonetaryAmount>" +
                "</otm:FinancialAmount>" +
                "</otm:DeclaredValue>" +
                "</otm:ItemQuantity>" +
                "</otm:ReleaseLine>" +
                "<otm:ShipUnit>" +
                "<otm:ShipUnitGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>6006</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:ShipUnitGid>" +
                "<otm:ShipUnitContent>" +
                "<otm:PackagedItemRef>" +
                "<otm:PackagedItemGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>149408018142</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:PackagedItemGid>" +
                "</otm:PackagedItemRef>" +
                "<otm:LineNumber>1</otm:LineNumber>" +
                "<otm:ItemQuantity>" +
                "<otm:IsSplitAllowed>Y</otm:IsSplitAllowed>" +
                "<otm:WeightVolume>" +
                "<otm:Weight>" +
                "<otm:WeightValue>0.0</otm:WeightValue>" +
                "<otm:WeightUOMGid>" +
                "<otm:Gid>" +
                "<otm:Xid>LB</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:WeightUOMGid>" +
                "</otm:Weight>" +
                "<otm:Volume>" +
                "<otm:VolumeValue>0.0</otm:VolumeValue>" +
                "<otm:VolumeUOMGid>" +
                "<otm:Gid>" +
                "<otm:Xid>CUFT</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:VolumeUOMGid>" +
                "</otm:Volume>" +
                "</otm:WeightVolume>" +
                "<otm:PackagedItemCount>3</otm:PackagedItemCount>" +
                "</otm:ItemQuantity>" +
                "<otm:ReleaseGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>TR_17101802</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:ReleaseGid>" +
                "<otm:ReleaseLineGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>6004</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:ReleaseLineGid>" +
                "</otm:ShipUnitContent>" +
                "<otm:ShipUnitCount>1</otm:ShipUnitCount>" +
                "</otm:ShipUnit>" +
                "<otm:TotalWeightVolume>" +
                "<otm:WeightVolume>" +
                "<otm:Weight>" +
                "<otm:WeightValue>0.0</otm:WeightValue>" +
                "<otm:WeightUOMGid>" +
                "<otm:Gid>" +
                "<otm:Xid>LB</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:WeightUOMGid>" +
                "</otm:Weight>" +
                "<otm:Volume>" +
                "<otm:VolumeValue>0.0</otm:VolumeValue>" +
                "<otm:VolumeUOMGid>" +
                "<otm:Gid>" +
                "<otm:Xid>CUFT</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:VolumeUOMGid>" +
                "</otm:Volume>" +
                "</otm:WeightVolume>" +
                "</otm:TotalWeightVolume>" +
                "<otm:ReleaseRefnum>" +
                "<otm:ReleaseRefnumQualifierGid>" +
                "<otm:Gid>" +
                "<otm:Xid>GLOG</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:ReleaseRefnumQualifierGid>" +
                "<otm:ReleaseRefnumValue>GAVE.TR_17101802</otm:ReleaseRefnumValue>" +
                "</otm:ReleaseRefnum>" +
                "<otm:ReleaseRefnum>" +
                "<otm:ReleaseRefnumQualifierGid>" +
                "<otm:Gid>" +
                "<otm:Xid>LINE_OF_BUSINESS</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:ReleaseRefnumQualifierGid>" +
                "<otm:ReleaseRefnumValue>6 GIOVANA</otm:ReleaseRefnumValue>" +
                "</otm:ReleaseRefnum>" +
                "<otm:ReleaseStatus>" +
                "<otm:StatusTypeGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>QUANTITY VALIDATION SOURCE</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:StatusTypeGid>" +
                "<otm:StatusValueGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>QUANTITY VALIDATION SOURCE_NOT STARTED</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:StatusValueGid>" +
                "</otm:ReleaseStatus>" +
                "<otm:ReleaseStatus>" +
                "<otm:StatusTypeGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>FLEET ASSIGNMENT OR</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:StatusTypeGid>" +
                "<otm:StatusValueGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>FLEET ASSIGNMENT OR_NOT ASSIGNED</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:StatusValueGid>" +
                "</otm:ReleaseStatus>" +
                "<otm:ReleaseStatus>" +
                "<otm:StatusTypeGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>PLANNING</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:StatusTypeGid>" +
                "<otm:StatusValueGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>PLANNING_PLANNED - FINAL</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:StatusValueGid>" +
                "</otm:ReleaseStatus>" +
                "<otm:ReleaseStatus>" +
                "<otm:StatusTypeGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>ORDER RELEASE SKU LINK</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:StatusTypeGid>" +
                "<otm:StatusValueGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>ORDER RELEASE SKU LINK_INACTIVE</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:StatusValueGid>" +
                "</otm:ReleaseStatus>" +
                "<otm:ReleaseStatus>" +
                "<otm:StatusTypeGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>READY_TO_SHIP</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:StatusTypeGid>" +
                "<otm:StatusValueGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>READY_TO_SHIP_NEW</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:StatusValueGid>" +
                "</otm:ReleaseStatus>" +
                "<otm:ReleaseStatus>" +
                "<otm:StatusTypeGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>QUANTITY VALIDATION DESTINATION</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:StatusTypeGid>" +
                "<otm:StatusValueGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>QUANTITY VALIDATION DESTINATION_NOT STARTED</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:StatusValueGid>" +
                "</otm:ReleaseStatus>" +
                "<otm:ReleaseStatus>" +
                "<otm:StatusTypeGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>OR_HD_LOAD_STATUS</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:StatusTypeGid>" +
                "<otm:StatusValueGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>OR_NOT_READY_TO_LOAD</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:StatusValueGid>" +
                "</otm:ReleaseStatus>" +
                "<otm:ReleaseStatus>" +
                "<otm:StatusTypeGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>PLANNING_SELL</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:StatusTypeGid>" +
                "<otm:StatusValueGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>PLANNING_SELL_PLANNED - FINAL</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:StatusValueGid>" +
                "</otm:ReleaseStatus>" +
                "<otm:ReleaseStatus>" +
                "<otm:StatusTypeGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>BILLED</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:StatusTypeGid>" +
                "<otm:StatusValueGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>BILLED_NOT_BILLED</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:StatusValueGid>" +
                "</otm:ReleaseStatus>" +
                "<otm:ReleaseStatus>" +
                "<otm:StatusTypeGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>ORDER_RELEASE GUARANTEED DELIVERY</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:StatusTypeGid>" +
                "<otm:StatusValueGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>ORDER_RELEASE GUARANTEED DELIVERY_NOT STARTED</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:StatusValueGid>" +
                "</otm:ReleaseStatus>" +
                "<otm:ReleaseStatus>" +
                "<otm:StatusTypeGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>ORDER MODIFIED</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:StatusTypeGid>" +
                "<otm:StatusValueGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>ORDER MODIFIED_NO MODIFICATION</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:StatusValueGid>" +
                "</otm:ReleaseStatus>" +
                "<otm:ReleaseStatus>" +
                "<otm:StatusTypeGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>CANCELLED</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:StatusTypeGid>" +
                "<otm:StatusValueGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>CANCELLED_NOT CANCELLED</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:StatusValueGid>" +
                "</otm:ReleaseStatus>" +
                "<otm:ReleaseStatus>" +
                "<otm:StatusTypeGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>ORDER DELIVERED</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:StatusTypeGid>" +
                "<otm:StatusValueGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>ORDER DELIVERED_NOT STARTED</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:StatusValueGid>" +
                "</otm:ReleaseStatus>" +
                "<otm:OrderMovement>" +
                "<otm:OrderMovementGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>6008</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:OrderMovementGid>" +
                "<otm:TransactionCode>NP</otm:TransactionCode>" +
                "<otm:OrderMovementD>" +
                "<otm:ShipUnit>" +
                "<otm:ShipUnitGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>8010</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:ShipUnitGid>" +
                "<otm:ShipFromLocationRef>" +
                "<otm:LocationRef>" +
                "<otm:LocationGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>80</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:LocationGid>" +
                "</otm:LocationRef>" +
                "</otm:ShipFromLocationRef>" +
                "<otm:ShipToLocationRef>" +
                "<otm:LocationRef>" +
                "<otm:LocationGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>041</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:LocationGid>" +
                "</otm:LocationRef>" +
                "</otm:ShipToLocationRef>" +
                "<otm:ShipUnitCount>1</otm:ShipUnitCount>" +
                "</otm:ShipUnit>" +
                "</otm:OrderMovementD>" +
                "</otm:OrderMovement>" +
                "<otm:OrderMovement>" +
                "<otm:OrderMovementGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>6004</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:OrderMovementGid>" +
                "<otm:TransactionCode>NP</otm:TransactionCode>" +
                "<otm:OrderMovementD>" +
                "<otm:ShipUnit>" +
                "<otm:ShipUnitGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>8004</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:ShipUnitGid>" +
                "<otm:ShipFromLocationRef>" +
                "<otm:LocationRef>" +
                "<otm:LocationGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>80</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:LocationGid>" +
                "</otm:LocationRef>" +
                "</otm:ShipFromLocationRef>" +
                "<otm:ShipToLocationRef>" +
                "<otm:LocationRef>" +
                "<otm:LocationGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>041</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:LocationGid>" +
                "</otm:LocationRef>" +
                "</otm:ShipToLocationRef>" +
                "<otm:ShipUnitCount>1</otm:ShipUnitCount>" +
                "</otm:ShipUnit>" +
                "</otm:OrderMovementD>" +
                "</otm:OrderMovement>" +
                "</otm:Release>" +
                "<otm:Release>" +
                "<otm:ReleaseGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>TR_17101803</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:ReleaseGid>" +
                "<otm:TransactionCode>NP</otm:TransactionCode>" +
                "<otm:ShipFromLocationRef>" +
                "<otm:LocationRef>" +
                "<otm:LocationGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>80</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:LocationGid>" +
                "</otm:LocationRef>" +
                "</otm:ShipFromLocationRef>" +
                "<otm:ShipToLocationRef>" +
                "<otm:LocationRef>" +
                "<otm:LocationGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>100</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:LocationGid>" +
                "</otm:LocationRef>" +
                "</otm:ShipToLocationRef>" +
                "<otm:TimeWindow>" +
                "<otm:EarlyPickupDt>" +
                "<otm:GLogDate>20181018100000</otm:GLogDate>" +
                "<otm:TZId>Etc/GMT-2</otm:TZId>" +
                "<otm:TZOffset>+02:00</otm:TZOffset>" +
                "</otm:EarlyPickupDt>" +
                "<otm:PickupIsAppt>N</otm:PickupIsAppt>" +
                "<otm:DeliveryIsAppt>N</otm:DeliveryIsAppt>" +
                "</otm:TimeWindow>" +
                "<otm:ReleaseLine>" +
                "<otm:ReleaseLineGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>6005</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:ReleaseLineGid>" +
                "<otm:PackagedItemRef>" +
                "<otm:PackagedItemGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>149408018142</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:PackagedItemGid>" +
                "</otm:PackagedItemRef>" +
                "<otm:ItemQuantity>" +
                "<otm:WeightVolume>" +
                "<otm:Weight>" +
                "<otm:WeightValue>0.0</otm:WeightValue>" +
                "<otm:WeightUOMGid>" +
                "<otm:Gid>" +
                "<otm:Xid>LB</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:WeightUOMGid>" +
                "</otm:Weight>" +
                "<otm:Volume>" +
                "<otm:VolumeValue>0.0</otm:VolumeValue>" +
                "<otm:VolumeUOMGid>" +
                "<otm:Gid>" +
                "<otm:Xid>CUFT</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:VolumeUOMGid>" +
                "</otm:Volume>" +
                "</otm:WeightVolume>" +
                "<otm:PackagedItemCount>3</otm:PackagedItemCount>" +
                "<otm:DeclaredValue>" +
                "<otm:FinancialAmount>" +
                "<otm:GlobalCurrencyCode>USD</otm:GlobalCurrencyCode>" +
                "<otm:MonetaryAmount>0.0</otm:MonetaryAmount>" +
                "</otm:FinancialAmount>" +
                "</otm:DeclaredValue>" +
                "</otm:ItemQuantity>" +
                "</otm:ReleaseLine>" +
                "<otm:ShipUnit>" +
                "<otm:ShipUnitGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>6007</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:ShipUnitGid>" +
                "<otm:ShipUnitContent>" +
                "<otm:PackagedItemRef>" +
                "<otm:PackagedItemGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>149408018142</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:PackagedItemGid>" +
                "</otm:PackagedItemRef>" +
                "<otm:LineNumber>1</otm:LineNumber>" +
                "<otm:ItemQuantity>" +
                "<otm:IsSplitAllowed>Y</otm:IsSplitAllowed>" +
                "<otm:WeightVolume>" +
                "<otm:Weight>" +
                "<otm:WeightValue>0.0</otm:WeightValue>" +
                "<otm:WeightUOMGid>" +
                "<otm:Gid>" +
                "<otm:Xid>LB</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:WeightUOMGid>" +
                "</otm:Weight>" +
                "<otm:Volume>" +
                "<otm:VolumeValue>0.0</otm:VolumeValue>" +
                "<otm:VolumeUOMGid>" +
                "<otm:Gid>" +
                "<otm:Xid>CUFT</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:VolumeUOMGid>" +
                "</otm:Volume>" +
                "</otm:WeightVolume>" +
                "<otm:PackagedItemCount>3</otm:PackagedItemCount>" +
                "</otm:ItemQuantity>" +
                "<otm:ReleaseGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>TR_17101803</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:ReleaseGid>" +
                "<otm:ReleaseLineGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>6005</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:ReleaseLineGid>" +
                "</otm:ShipUnitContent>" +
                "<otm:ShipUnitCount>1</otm:ShipUnitCount>" +
                "</otm:ShipUnit>" +
                "<otm:TotalWeightVolume>" +
                "<otm:WeightVolume>" +
                "<otm:Weight>" +
                "<otm:WeightValue>0.0</otm:WeightValue>" +
                "<otm:WeightUOMGid>" +
                "<otm:Gid>" +
                "<otm:Xid>LB</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:WeightUOMGid>" +
                "</otm:Weight>" +
                "<otm:Volume>" +
                "<otm:VolumeValue>0.0</otm:VolumeValue>" +
                "<otm:VolumeUOMGid>" +
                "<otm:Gid>" +
                "<otm:Xid>CUFT</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:VolumeUOMGid>" +
                "</otm:Volume>" +
                "</otm:WeightVolume>" +
                "</otm:TotalWeightVolume>" +
                "<otm:ReleaseRefnum>" +
                "<otm:ReleaseRefnumQualifierGid>" +
                "<otm:Gid>" +
                "<otm:Xid>GLOG</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:ReleaseRefnumQualifierGid>" +
                "<otm:ReleaseRefnumValue>GAVE.TR_17101803</otm:ReleaseRefnumValue>" +
                "</otm:ReleaseRefnum>" +
                "<otm:ReleaseRefnum>" +
                "<otm:ReleaseRefnumQualifierGid>" +
                "<otm:Gid>" +
                "<otm:Xid>LINE_OF_BUSINESS</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:ReleaseRefnumQualifierGid>" +
                "<otm:ReleaseRefnumValue>6 GIOVANA</otm:ReleaseRefnumValue>" +
                "</otm:ReleaseRefnum>" +
                "<otm:ReleaseStatus>" +
                "<otm:StatusTypeGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>BILLED</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:StatusTypeGid>" +
                "<otm:StatusValueGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>BILLED_NOT_BILLED</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:StatusValueGid>" +
                "</otm:ReleaseStatus>" +
                "<otm:ReleaseStatus>" +
                "<otm:StatusTypeGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>ORDER DELIVERED</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:StatusTypeGid>" +
                "<otm:StatusValueGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>ORDER DELIVERED_NOT STARTED</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:StatusValueGid>" +
                "</otm:ReleaseStatus>" +
                "<otm:ReleaseStatus>" +
                "<otm:StatusTypeGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>QUANTITY VALIDATION SOURCE</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:StatusTypeGid>" +
                "<otm:StatusValueGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>QUANTITY VALIDATION SOURCE_NOT STARTED</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:StatusValueGid>" +
                "</otm:ReleaseStatus>" +
                "<otm:ReleaseStatus>" +
                "<otm:StatusTypeGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>PLANNING</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:StatusTypeGid>" +
                "<otm:StatusValueGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>PLANNING_PLANNED - FINAL</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:StatusValueGid>" +
                "</otm:ReleaseStatus>" +
                "<otm:ReleaseStatus>" +
                "<otm:StatusTypeGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>ORDER MODIFIED</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:StatusTypeGid>" +
                "<otm:StatusValueGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>ORDER MODIFIED_NO MODIFICATION</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:StatusValueGid>" +
                "</otm:ReleaseStatus>" +
                "<otm:ReleaseStatus>" +
                "<otm:StatusTypeGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>READY_TO_SHIP</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:StatusTypeGid>" +
                "<otm:StatusValueGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>READY_TO_SHIP_NEW</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:StatusValueGid>" +
                "</otm:ReleaseStatus>" +
                "<otm:ReleaseStatus>" +
                "<otm:StatusTypeGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>CANCELLED</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:StatusTypeGid>" +
                "<otm:StatusValueGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>CANCELLED_NOT CANCELLED</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:StatusValueGid>" +
                "</otm:ReleaseStatus>" +
                "<otm:ReleaseStatus>" +
                "<otm:StatusTypeGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>QUANTITY VALIDATION DESTINATION</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:StatusTypeGid>" +
                "<otm:StatusValueGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>QUANTITY VALIDATION DESTINATION_NOT STARTED</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:StatusValueGid>" +
                "</otm:ReleaseStatus>" +
                "<otm:ReleaseStatus>" +
                "<otm:StatusTypeGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>PLANNING_SELL</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:StatusTypeGid>" +
                "<otm:StatusValueGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>PLANNING_SELL_PLANNED - FINAL</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:StatusValueGid>" +
                "</otm:ReleaseStatus>" +
                "<otm:ReleaseStatus>" +
                "<otm:StatusTypeGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>FLEET ASSIGNMENT OR</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:StatusTypeGid>" +
                "<otm:StatusValueGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>FLEET ASSIGNMENT OR_NOT ASSIGNED</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:StatusValueGid>" +
                "</otm:ReleaseStatus>" +
                "<otm:ReleaseStatus>" +
                "<otm:StatusTypeGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>ORDER RELEASE SKU LINK</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:StatusTypeGid>" +
                "<otm:StatusValueGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>ORDER RELEASE SKU LINK_INACTIVE</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:StatusValueGid>" +
                "</otm:ReleaseStatus>" +
                "<otm:ReleaseStatus>" +
                "<otm:StatusTypeGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>ORDER_RELEASE GUARANTEED DELIVERY</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:StatusTypeGid>" +
                "<otm:StatusValueGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>ORDER_RELEASE GUARANTEED DELIVERY_NOT STARTED</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:StatusValueGid>" +
                "</otm:ReleaseStatus>" +
                "<otm:ReleaseStatus>" +
                "<otm:StatusTypeGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>OR_HD_LOAD_STATUS</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:StatusTypeGid>" +
                "<otm:StatusValueGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>OR_NOT_READY_TO_LOAD</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:StatusValueGid>" +
                "</otm:ReleaseStatus>" +
                "<otm:OrderMovement>" +
                "<otm:OrderMovementGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>6009</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:OrderMovementGid>" +
                "<otm:TransactionCode>NP</otm:TransactionCode>" +
                "<otm:OrderMovementD>" +
                "<otm:ShipUnit>" +
                "<otm:ShipUnitGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>8007</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:ShipUnitGid>" +
                "<otm:ShipFromLocationRef>" +
                "<otm:LocationRef>" +
                "<otm:LocationGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>80</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:LocationGid>" +
                "</otm:LocationRef>" +
                "</otm:ShipFromLocationRef>" +
                "<otm:ShipToLocationRef>" +
                "<otm:LocationRef>" +
                "<otm:LocationGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>100</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:LocationGid>" +
                "</otm:LocationRef>" +
                "</otm:ShipToLocationRef>" +
                "<otm:ShipUnitCount>1</otm:ShipUnitCount>" +
                "</otm:ShipUnit>" +
                "</otm:OrderMovementD>" +
                "</otm:OrderMovement>" +
                "<otm:OrderMovement>" +
                "<otm:OrderMovementGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>6005</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:OrderMovementGid>" +
                "<otm:TransactionCode>NP</otm:TransactionCode>" +
                "<otm:OrderMovementD>" +
                "<otm:ShipUnit>" +
                "<otm:ShipUnitGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>8005</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:ShipUnitGid>" +
                "<otm:ShipFromLocationRef>" +
                "<otm:LocationRef>" +
                "<otm:LocationGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>80</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:LocationGid>" +
                "</otm:LocationRef>" +
                "</otm:ShipFromLocationRef>" +
                "<otm:ShipToLocationRef>" +
                "<otm:LocationRef>" +
                "<otm:LocationGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>100</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:LocationGid>" +
                "</otm:LocationRef>" +
                "</otm:ShipToLocationRef>" +
                "<otm:ShipUnitCount>1</otm:ShipUnitCount>" +
                "</otm:ShipUnit>" +
                "</otm:OrderMovementD>" +
                "</otm:OrderMovement>" +
                "</otm:Release>" +
                "<otm:Release>" +
                "<otm:ReleaseGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>TR_17101804</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:ReleaseGid>" +
                "<otm:TransactionCode>NP</otm:TransactionCode>" +
                "<otm:ShipFromLocationRef>" +
                "<otm:LocationRef>" +
                "<otm:LocationGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>80</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:LocationGid>" +
                "</otm:LocationRef>" +
                "</otm:ShipFromLocationRef>" +
                "<otm:ShipToLocationRef>" +
                "<otm:LocationRef>" +
                "<otm:LocationGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>100</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:LocationGid>" +
                "</otm:LocationRef>" +
                "</otm:ShipToLocationRef>" +
                "<otm:TimeWindow>" +
                "<otm:EarlyPickupDt>" +
                "<otm:GLogDate>20181018100000</otm:GLogDate>" +
                "<otm:TZId>Etc/GMT-2</otm:TZId>" +
                "<otm:TZOffset>+02:00</otm:TZOffset>" +
                "</otm:EarlyPickupDt>" +
                "<otm:PickupIsAppt>N</otm:PickupIsAppt>" +
                "<otm:DeliveryIsAppt>N</otm:DeliveryIsAppt>" +
                "</otm:TimeWindow>" +
                "<otm:ReleaseLine>" +
                "<otm:ReleaseLineGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>7002</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:ReleaseLineGid>" +
                "<otm:PackagedItemRef>" +
                "<otm:PackagedItemGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>149410016837</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:PackagedItemGid>" +
                "</otm:PackagedItemRef>" +
                "<otm:ItemQuantity>" +
                "<otm:WeightVolume>" +
                "<otm:Weight>" +
                "<otm:WeightValue>0.0</otm:WeightValue>" +
                "<otm:WeightUOMGid>" +
                "<otm:Gid>" +
                "<otm:Xid>LB</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:WeightUOMGid>" +
                "</otm:Weight>" +
                "<otm:Volume>" +
                "<otm:VolumeValue>0.0</otm:VolumeValue>" +
                "<otm:VolumeUOMGid>" +
                "<otm:Gid>" +
                "<otm:Xid>CUFT</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:VolumeUOMGid>" +
                "</otm:Volume>" +
                "</otm:WeightVolume>" +
                "<otm:PackagedItemCount>3</otm:PackagedItemCount>" +
                "<otm:DeclaredValue>" +
                "<otm:FinancialAmount>" +
                "<otm:GlobalCurrencyCode>USD</otm:GlobalCurrencyCode>" +
                "<otm:MonetaryAmount>0.0</otm:MonetaryAmount>" +
                "</otm:FinancialAmount>" +
                "</otm:DeclaredValue>" +
                "</otm:ItemQuantity>" +
                "</otm:ReleaseLine>" +
                "<otm:ShipUnit>" +
                "<otm:ShipUnitGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>7002</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:ShipUnitGid>" +
                "<otm:ShipUnitContent>" +
                "<otm:PackagedItemRef>" +
                "<otm:PackagedItemGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>149410016837</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:PackagedItemGid>" +
                "</otm:PackagedItemRef>" +
                "<otm:LineNumber>1</otm:LineNumber>" +
                "<otm:ItemQuantity>" +
                "<otm:IsSplitAllowed>Y</otm:IsSplitAllowed>" +
                "<otm:WeightVolume>" +
                "<otm:Weight>" +
                "<otm:WeightValue>0.0</otm:WeightValue>" +
                "<otm:WeightUOMGid>" +
                "<otm:Gid>" +
                "<otm:Xid>LB</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:WeightUOMGid>" +
                "</otm:Weight>" +
                "<otm:Volume>" +
                "<otm:VolumeValue>0.0</otm:VolumeValue>" +
                "<otm:VolumeUOMGid>" +
                "<otm:Gid>" +
                "<otm:Xid>CUFT</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:VolumeUOMGid>" +
                "</otm:Volume>" +
                "</otm:WeightVolume>" +
                "<otm:PackagedItemCount>3</otm:PackagedItemCount>" +
                "</otm:ItemQuantity>" +
                "<otm:ReleaseGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>TR_17101804</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:ReleaseGid>" +
                "<otm:ReleaseLineGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>7002</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:ReleaseLineGid>" +
                "</otm:ShipUnitContent>" +
                "<otm:ShipUnitCount>1</otm:ShipUnitCount>" +
                "</otm:ShipUnit>" +
                "<otm:TotalWeightVolume>" +
                "<otm:WeightVolume>" +
                "<otm:Weight>" +
                "<otm:WeightValue>0.0</otm:WeightValue>" +
                "<otm:WeightUOMGid>" +
                "<otm:Gid>" +
                "<otm:Xid>LB</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:WeightUOMGid>" +
                "</otm:Weight>" +
                "<otm:Volume>" +
                "<otm:VolumeValue>0.0</otm:VolumeValue>" +
                "<otm:VolumeUOMGid>" +
                "<otm:Gid>" +
                "<otm:Xid>CUFT</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:VolumeUOMGid>" +
                "</otm:Volume>" +
                "</otm:WeightVolume>" +
                "</otm:TotalWeightVolume>" +
                "<otm:ReleaseRefnum>" +
                "<otm:ReleaseRefnumQualifierGid>" +
                "<otm:Gid>" +
                "<otm:Xid>LINE_OF_BUSINESS</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:ReleaseRefnumQualifierGid>" +
                "<otm:ReleaseRefnumValue>6 GIOVANA</otm:ReleaseRefnumValue>" +
                "</otm:ReleaseRefnum>" +
                "<otm:ReleaseRefnum>" +
                "<otm:ReleaseRefnumQualifierGid>" +
                "<otm:Gid>" +
                "<otm:Xid>GLOG</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:ReleaseRefnumQualifierGid>" +
                "<otm:ReleaseRefnumValue>GAVE.TR_17101804</otm:ReleaseRefnumValue>" +
                "</otm:ReleaseRefnum>" +
                "<otm:ReleaseStatus>" +
                "<otm:StatusTypeGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>CANCELLED</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:StatusTypeGid>" +
                "<otm:StatusValueGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>CANCELLED_NOT CANCELLED</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:StatusValueGid>" +
                "</otm:ReleaseStatus>" +
                "<otm:ReleaseStatus>" +
                "<otm:StatusTypeGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>READY_TO_SHIP</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:StatusTypeGid>" +
                "<otm:StatusValueGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>READY_TO_SHIP_NEW</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:StatusValueGid>" +
                "</otm:ReleaseStatus>" +
                "<otm:ReleaseStatus>" +
                "<otm:StatusTypeGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>QUANTITY VALIDATION SOURCE</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:StatusTypeGid>" +
                "<otm:StatusValueGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>QUANTITY VALIDATION SOURCE_NOT STARTED</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:StatusValueGid>" +
                "</otm:ReleaseStatus>" +
                "<otm:ReleaseStatus>" +
                "<otm:StatusTypeGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>ORDER DELIVERED</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:StatusTypeGid>" +
                "<otm:StatusValueGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>ORDER DELIVERED_NOT STARTED</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:StatusValueGid>" +
                "</otm:ReleaseStatus>" +
                "<otm:ReleaseStatus>" +
                "<otm:StatusTypeGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>BILLED</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:StatusTypeGid>" +
                "<otm:StatusValueGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>BILLED_NOT_BILLED</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:StatusValueGid>" +
                "</otm:ReleaseStatus>" +
                "<otm:ReleaseStatus>" +
                "<otm:StatusTypeGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>ORDER MODIFIED</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:StatusTypeGid>" +
                "<otm:StatusValueGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>ORDER MODIFIED_NO MODIFICATION</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:StatusValueGid>" +
                "</otm:ReleaseStatus>" +
                "<otm:ReleaseStatus>" +
                "<otm:StatusTypeGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>OR_HD_LOAD_STATUS</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:StatusTypeGid>" +
                "<otm:StatusValueGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>OR_NOT_READY_TO_LOAD</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:StatusValueGid>" +
                "</otm:ReleaseStatus>" +
                "<otm:ReleaseStatus>" +
                "<otm:StatusTypeGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>ORDER_RELEASE GUARANTEED DELIVERY</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:StatusTypeGid>" +
                "<otm:StatusValueGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>ORDER_RELEASE GUARANTEED DELIVERY_NOT STARTED</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:StatusValueGid>" +
                "</otm:ReleaseStatus>" +
                "<otm:ReleaseStatus>" +
                "<otm:StatusTypeGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>PLANNING</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:StatusTypeGid>" +
                "<otm:StatusValueGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>PLANNING_PLANNED - FINAL</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:StatusValueGid>" +
                "</otm:ReleaseStatus>" +
                "<otm:ReleaseStatus>" +
                "<otm:StatusTypeGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>QUANTITY VALIDATION DESTINATION</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:StatusTypeGid>" +
                "<otm:StatusValueGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>QUANTITY VALIDATION DESTINATION_NOT STARTED</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:StatusValueGid>" +
                "</otm:ReleaseStatus>" +
                "<otm:ReleaseStatus>" +
                "<otm:StatusTypeGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>ORDER RELEASE SKU LINK</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:StatusTypeGid>" +
                "<otm:StatusValueGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>ORDER RELEASE SKU LINK_INACTIVE</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:StatusValueGid>" +
                "</otm:ReleaseStatus>" +
                "<otm:ReleaseStatus>" +
                "<otm:StatusTypeGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>PLANNING_SELL</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:StatusTypeGid>" +
                "<otm:StatusValueGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>PLANNING_SELL_PLANNED - FINAL</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:StatusValueGid>" +
                "</otm:ReleaseStatus>" +
                "<otm:ReleaseStatus>" +
                "<otm:StatusTypeGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>FLEET ASSIGNMENT OR</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:StatusTypeGid>" +
                "<otm:StatusValueGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>FLEET ASSIGNMENT OR_NOT ASSIGNED</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:StatusValueGid>" +
                "</otm:ReleaseStatus>" +
                "<otm:OrderMovement>" +
                "<otm:OrderMovementGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>6010</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:OrderMovementGid>" +
                "<otm:TransactionCode>NP</otm:TransactionCode>" +
                "<otm:OrderMovementD>" +
                "<otm:ShipUnit>" +
                "<otm:ShipUnitGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>8009</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:ShipUnitGid>" +
                "<otm:ShipFromLocationRef>" +
                "<otm:LocationRef>" +
                "<otm:LocationGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>80</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:LocationGid>" +
                "</otm:LocationRef>" +
                "</otm:ShipFromLocationRef>" +
                "<otm:ShipToLocationRef>" +
                "<otm:LocationRef>" +
                "<otm:LocationGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>100</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:LocationGid>" +
                "</otm:LocationRef>" +
                "</otm:ShipToLocationRef>" +
                "<otm:ShipUnitCount>1</otm:ShipUnitCount>" +
                "</otm:ShipUnit>" +
                "</otm:OrderMovementD>" +
                "</otm:OrderMovement>" +
                "<otm:OrderMovement>" +
                "<otm:OrderMovementGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>6006</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:OrderMovementGid>" +
                "<otm:TransactionCode>NP</otm:TransactionCode>" +
                "<otm:OrderMovementD>" +
                "<otm:ShipUnit>" +
                "<otm:ShipUnitGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>8006</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:ShipUnitGid>" +
                "<otm:ShipFromLocationRef>" +
                "<otm:LocationRef>" +
                "<otm:LocationGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>80</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:LocationGid>" +
                "</otm:LocationRef>" +
                "</otm:ShipFromLocationRef>" +
                "<otm:ShipToLocationRef>" +
                "<otm:LocationRef>" +
                "<otm:LocationGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>100</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:LocationGid>" +
                "</otm:LocationRef>" +
                "</otm:ShipToLocationRef>" +
                "<otm:ShipUnitCount>1</otm:ShipUnitCount>" +
                "</otm:ShipUnit>" +
                "</otm:OrderMovementD>" +
                "</otm:OrderMovement>" +
                "</otm:Release>" +
                "<otm:PackagedItem>" +
                "<otm:Packaging>" +
                "<otm:PackagedItemGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>149408018142</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:PackagedItemGid>" +
                "<otm:HazmatPackageTypeGid>" +
                "<otm:Gid>" +
                "<otm:Xid>DEFAULT</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:HazmatPackageTypeGid>" +
                "<otm:IsHandlingUnitStackable>Y</otm:IsHandlingUnitStackable>" +
                "<otm:IsDefaultPackaging>Y</otm:IsDefaultPackaging>" +
                "<otm:IsHazardous>N</otm:IsHazardous>" +
                "<otm:FlexFieldStrings/>" +
                "<otm:FlexFieldNumbers/>" +
                "<otm:FlexFieldDates/>" +
                "<otm:IsAllowMixedFreight>Y</otm:IsAllowMixedFreight>" +
                "</otm:Packaging>" +
                "<otm:Item>" +
                "<otm:TransactionCode>NP</otm:TransactionCode>" +
                "<otm:ItemGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>149408018142</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:ItemGid>" +
                "<otm:ItemName>TAM RST GIOVANNA 964290 VZ TERRACOTA WW</otm:ItemName>" +
                "</otm:Item>" +
                "</otm:PackagedItem>" +
                "<otm:PackagedItem>" +
                "<otm:Packaging>" +
                "<otm:PackagedItemGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>149410016837</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:PackagedItemGid>" +
                "<otm:HazmatPackageTypeGid>" +
                "<otm:Gid>" +
                "<otm:Xid>DEFAULT</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:HazmatPackageTypeGid>" +
                "<otm:IsHandlingUnitStackable>Y</otm:IsHandlingUnitStackable>" +
                "<otm:IsDefaultPackaging>Y</otm:IsDefaultPackaging>" +
                "<otm:IsHazardous>N</otm:IsHazardous>" +
                "<otm:FlexFieldStrings/>" +
                "<otm:FlexFieldNumbers/>" +
                "<otm:FlexFieldDates/>" +
                "<otm:IsAllowMixedFreight>Y</otm:IsAllowMixedFreight>" +
                "</otm:Packaging>" +
                "<otm:Item>" +
                "<otm:TransactionCode>NP</otm:TransactionCode>" +
                "<otm:ItemGid>" +
                "<otm:Gid>" +
                "<otm:DomainName>GAVE</otm:DomainName>" +
                "<otm:Xid>149410016837</otm:Xid>" +
                "</otm:Gid>" +
                "</otm:ItemGid>" +
                "<otm:ItemName>TAM RST GIOVANNA 964290 VZ BISTRO WW</otm:ItemName>" +
                "</otm:Item>" +
                "</otm:PackagedItem>" +
                "</otm:Shipment>" +
                "<otm:ExpectedResponseDt>" +
                "<otm:GLogDate>20181019195146</otm:GLogDate>" +
                "<otm:TZId>UTC</otm:TZId>" +
                "<otm:TZOffset>+00:00</otm:TZOffset>" +
                "</otm:ExpectedResponseDt>" +
                "</otm:TenderOffer>";

        return xml;
    }

    public static String getXmlCadastroLoja(Documento documento, Integracao integracao) {

        String dataLog = "2018-10-23T12:12:12";
        String xmlStringLoja = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                "<LgfData>" +
                "<Header>" +
                "<DocumentVersion>18</DocumentVersion>" +
                "<OriginSystem>SYNC</OriginSystem>" +
                "<ClientEnvCode>1</ClientEnvCode>" +
                //"<ParentCompanyCode>" + documento.getCodigoDepositante() + "</ParentCompanyCode>" +
                "<ParentCompanyCode>1</ParentCompanyCode>" +
                "<Entity>vendor</Entity>" +
                "<TimeStamp>" + integracao.getDataLog() + "</TimeStamp>" +
                "<MessageId>" + documento.getSequenciaIntegracao() + "</MessageId>" +
                "</Header>" +
                "<ListOfVendors>" +
                "<vendor>" +
                "<company_code>1</company_code>" +
                "<vendor_code>" + documento.getCodigoEmpresa() + "</vendor_code>" +
                "<name>" + documento.getNomeEmpresa() + "</name>" +
                "<address_1>" + documento.getEnderecoEmpresa() + "</address_1>" +
                "<address_2>" + documento.getBairroEmpresa() + "</address_2>" +
                "<address_3></address_3>" +
                "<locality></locality>" +
                "<city>" + documento.getMunicipioEmpresa() + "</city>" +
                "<state>" + documento.getUfEmpresa() + "</state>" +
                "<zip>" + documento.getCepEmpresa() + "</zip>" +
                "<country></country>" +
                "<phone_nbr></phone_nbr>" +
                "<email></email>" +
                "<contact></contact>" +
                "<action_code>CREATE</action_code>" +
                "<univ_id_1></univ_id_1>" +
                "<cust_field_1>"+documento.getCnpjCpfEmpresa()+"</cust_field_1>" +
                "<cust_field_2></cust_field_2>" +
                "<cust_field_3></cust_field_3>" +
                "<cust_field_4></cust_field_4>" +
                "<cust_field_5></cust_field_5>" +
                "<reception_percent_level></reception_percent_level>" +
                "<create_asn_on_po_intf></create_asn_on_po_intf>" +
                "</vendor>" +
                "</ListOfVendors>" +
                "</LgfData>";

        return xmlStringLoja;
    }

}
