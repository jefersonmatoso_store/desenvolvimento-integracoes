
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * MotorLineItem specifies information for a particular motor invoice line item.
 * 
 * <p>Classe Java de MotorLineItemType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="MotorLineItemType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AssignedNum" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CommonInvoiceLineElements" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}CommonInvoiceLineElementsType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MotorLineItemType", propOrder = {
    "assignedNum",
    "commonInvoiceLineElements"
})
public class MotorLineItemType {

    @XmlElement(name = "AssignedNum", required = true)
    protected String assignedNum;
    @XmlElement(name = "CommonInvoiceLineElements", required = true)
    protected CommonInvoiceLineElementsType commonInvoiceLineElements;

    /**
     * Obtém o valor da propriedade assignedNum.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAssignedNum() {
        return assignedNum;
    }

    /**
     * Define o valor da propriedade assignedNum.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAssignedNum(String value) {
        this.assignedNum = value;
    }

    /**
     * Obtém o valor da propriedade commonInvoiceLineElements.
     * 
     * @return
     *     possible object is
     *     {@link CommonInvoiceLineElementsType }
     *     
     */
    public CommonInvoiceLineElementsType getCommonInvoiceLineElements() {
        return commonInvoiceLineElements;
    }

    /**
     * Define o valor da propriedade commonInvoiceLineElements.
     * 
     * @param value
     *     allowed object is
     *     {@link CommonInvoiceLineElementsType }
     *     
     */
    public void setCommonInvoiceLineElements(CommonInvoiceLineElementsType value) {
        this.commonInvoiceLineElements = value;
    }

}
