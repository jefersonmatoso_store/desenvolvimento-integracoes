
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;


/**
 * 
 *             GtmRegistration interface represents the registration taken from any governmental jurisdiction or regimes.
 *          
 * 
 * <p>Classe Java de GtmRegistrationType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="GtmRegistrationType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OTMTransactionInOut">
 *       &lt;sequence>
 *         &lt;element name="GtmRegistrationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="TransactionCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransactionCodeType" minOccurs="0"/>
 *         &lt;element name="GtmJurisdictionGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="GtmRegistrationCategoryGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="GtmRegistrationTypeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="GtmRegistrationCodeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="RegisteredName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RegistrationNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RegisteredAddress" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EffectiveDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="ExpirationDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="RequestedDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="ApprovalDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="RenewalDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="GtmProdClassTypeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="ClassificationCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Indicator" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="UserDefIconInfo" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}UserDefIconInfoType" minOccurs="0"/>
 *         &lt;element name="IsActive" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsTemplate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="GtmRegistrationDate" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}GtmRegistrationDateType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="GtmRegRelatedItem" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}GtmRegRelatedItemType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="GtmRegRelatedCommodity" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}GtmRegRelatedCommodityType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="GtmRegRelatedUDCommodity" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}GtmRegRelatedUDCommodityType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="GtmRegRelatedLicense" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}GtmRegRelatedLicenseType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="GtmRegRelatedRegistration" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}GtmRegRelatedRegistrationType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Quantity" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}QuantityType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Currency" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}CurrencyType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Status" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}StatusType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Remark" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RemarkType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="InvolvedParty" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}InvolvedPartyType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Refnum" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RefnumType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="RegimeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GtmRegistrationType", namespace = "http://xmlns.oracle.com/apps/gtm/transmission/v6.4", propOrder = {
    "gtmRegistrationGid",
    "transactionCode",
    "gtmJurisdictionGid",
    "gtmRegistrationCategoryGid",
    "gtmRegistrationTypeGid",
    "gtmRegistrationCodeGid",
    "registeredName",
    "registrationNumber",
    "registeredAddress",
    "effectiveDate",
    "expirationDate",
    "requestedDate",
    "approvalDate",
    "renewalDate",
    "gtmProdClassTypeGid",
    "classificationCode",
    "indicator",
    "userDefIconInfo",
    "isActive",
    "isTemplate",
    "gtmRegistrationDate",
    "gtmRegRelatedItem",
    "gtmRegRelatedCommodity",
    "gtmRegRelatedUDCommodity",
    "gtmRegRelatedLicense",
    "gtmRegRelatedRegistration",
    "quantity",
    "currency",
    "status",
    "remark",
    "involvedParty",
    "refnum",
    "regimeGid"
})
public class GtmRegistrationType
    extends OTMTransactionInOut
{

    @XmlElement(name = "GtmRegistrationGid", required = true)
    protected GLogXMLGidType gtmRegistrationGid;
    @XmlElement(name = "TransactionCode")
    @XmlSchemaType(name = "string")
    protected TransactionCodeType transactionCode;
    @XmlElement(name = "GtmJurisdictionGid")
    protected GLogXMLGidType gtmJurisdictionGid;
    @XmlElement(name = "GtmRegistrationCategoryGid")
    protected GLogXMLGidType gtmRegistrationCategoryGid;
    @XmlElement(name = "GtmRegistrationTypeGid")
    protected GLogXMLGidType gtmRegistrationTypeGid;
    @XmlElement(name = "GtmRegistrationCodeGid")
    protected GLogXMLGidType gtmRegistrationCodeGid;
    @XmlElement(name = "RegisteredName")
    protected String registeredName;
    @XmlElement(name = "RegistrationNumber")
    protected String registrationNumber;
    @XmlElement(name = "RegisteredAddress")
    protected String registeredAddress;
    @XmlElement(name = "EffectiveDate")
    protected GLogDateTimeType effectiveDate;
    @XmlElement(name = "ExpirationDate")
    protected GLogDateTimeType expirationDate;
    @XmlElement(name = "RequestedDate")
    protected GLogDateTimeType requestedDate;
    @XmlElement(name = "ApprovalDate")
    protected GLogDateTimeType approvalDate;
    @XmlElement(name = "RenewalDate")
    protected GLogDateTimeType renewalDate;
    @XmlElement(name = "GtmProdClassTypeGid")
    protected GLogXMLGidType gtmProdClassTypeGid;
    @XmlElement(name = "ClassificationCode")
    protected String classificationCode;
    @XmlElement(name = "Indicator")
    protected String indicator;
    @XmlElement(name = "UserDefIconInfo")
    protected UserDefIconInfoType userDefIconInfo;
    @XmlElement(name = "IsActive")
    protected String isActive;
    @XmlElement(name = "IsTemplate")
    protected String isTemplate;
    @XmlElement(name = "GtmRegistrationDate")
    protected List<GtmRegistrationDateType> gtmRegistrationDate;
    @XmlElement(name = "GtmRegRelatedItem")
    protected List<GtmRegRelatedItemType> gtmRegRelatedItem;
    @XmlElement(name = "GtmRegRelatedCommodity")
    protected List<GtmRegRelatedCommodityType> gtmRegRelatedCommodity;
    @XmlElement(name = "GtmRegRelatedUDCommodity")
    protected List<GtmRegRelatedUDCommodityType> gtmRegRelatedUDCommodity;
    @XmlElement(name = "GtmRegRelatedLicense")
    protected List<GtmRegRelatedLicenseType> gtmRegRelatedLicense;
    @XmlElement(name = "GtmRegRelatedRegistration")
    protected List<GtmRegRelatedRegistrationType> gtmRegRelatedRegistration;
    @XmlElement(name = "Quantity")
    protected List<QuantityType> quantity;
    @XmlElement(name = "Currency")
    protected List<CurrencyType> currency;
    @XmlElement(name = "Status")
    protected List<StatusType> status;
    @XmlElement(name = "Remark")
    protected List<RemarkType> remark;
    @XmlElement(name = "InvolvedParty")
    protected List<InvolvedPartyType> involvedParty;
    @XmlElement(name = "Refnum")
    protected List<RefnumType> refnum;
    @XmlElement(name = "RegimeGid")
    protected GLogXMLGidType regimeGid;

    /**
     * Obtém o valor da propriedade gtmRegistrationGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getGtmRegistrationGid() {
        return gtmRegistrationGid;
    }

    /**
     * Define o valor da propriedade gtmRegistrationGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setGtmRegistrationGid(GLogXMLGidType value) {
        this.gtmRegistrationGid = value;
    }

    /**
     * Obtém o valor da propriedade transactionCode.
     * 
     * @return
     *     possible object is
     *     {@link TransactionCodeType }
     *     
     */
    public TransactionCodeType getTransactionCode() {
        return transactionCode;
    }

    /**
     * Define o valor da propriedade transactionCode.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionCodeType }
     *     
     */
    public void setTransactionCode(TransactionCodeType value) {
        this.transactionCode = value;
    }

    /**
     * Obtém o valor da propriedade gtmJurisdictionGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getGtmJurisdictionGid() {
        return gtmJurisdictionGid;
    }

    /**
     * Define o valor da propriedade gtmJurisdictionGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setGtmJurisdictionGid(GLogXMLGidType value) {
        this.gtmJurisdictionGid = value;
    }

    /**
     * Obtém o valor da propriedade gtmRegistrationCategoryGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getGtmRegistrationCategoryGid() {
        return gtmRegistrationCategoryGid;
    }

    /**
     * Define o valor da propriedade gtmRegistrationCategoryGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setGtmRegistrationCategoryGid(GLogXMLGidType value) {
        this.gtmRegistrationCategoryGid = value;
    }

    /**
     * Obtém o valor da propriedade gtmRegistrationTypeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getGtmRegistrationTypeGid() {
        return gtmRegistrationTypeGid;
    }

    /**
     * Define o valor da propriedade gtmRegistrationTypeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setGtmRegistrationTypeGid(GLogXMLGidType value) {
        this.gtmRegistrationTypeGid = value;
    }

    /**
     * Obtém o valor da propriedade gtmRegistrationCodeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getGtmRegistrationCodeGid() {
        return gtmRegistrationCodeGid;
    }

    /**
     * Define o valor da propriedade gtmRegistrationCodeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setGtmRegistrationCodeGid(GLogXMLGidType value) {
        this.gtmRegistrationCodeGid = value;
    }

    /**
     * Obtém o valor da propriedade registeredName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRegisteredName() {
        return registeredName;
    }

    /**
     * Define o valor da propriedade registeredName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegisteredName(String value) {
        this.registeredName = value;
    }

    /**
     * Obtém o valor da propriedade registrationNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRegistrationNumber() {
        return registrationNumber;
    }

    /**
     * Define o valor da propriedade registrationNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegistrationNumber(String value) {
        this.registrationNumber = value;
    }

    /**
     * Obtém o valor da propriedade registeredAddress.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRegisteredAddress() {
        return registeredAddress;
    }

    /**
     * Define o valor da propriedade registeredAddress.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegisteredAddress(String value) {
        this.registeredAddress = value;
    }

    /**
     * Obtém o valor da propriedade effectiveDate.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getEffectiveDate() {
        return effectiveDate;
    }

    /**
     * Define o valor da propriedade effectiveDate.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setEffectiveDate(GLogDateTimeType value) {
        this.effectiveDate = value;
    }

    /**
     * Obtém o valor da propriedade expirationDate.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getExpirationDate() {
        return expirationDate;
    }

    /**
     * Define o valor da propriedade expirationDate.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setExpirationDate(GLogDateTimeType value) {
        this.expirationDate = value;
    }

    /**
     * Obtém o valor da propriedade requestedDate.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getRequestedDate() {
        return requestedDate;
    }

    /**
     * Define o valor da propriedade requestedDate.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setRequestedDate(GLogDateTimeType value) {
        this.requestedDate = value;
    }

    /**
     * Obtém o valor da propriedade approvalDate.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getApprovalDate() {
        return approvalDate;
    }

    /**
     * Define o valor da propriedade approvalDate.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setApprovalDate(GLogDateTimeType value) {
        this.approvalDate = value;
    }

    /**
     * Obtém o valor da propriedade renewalDate.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getRenewalDate() {
        return renewalDate;
    }

    /**
     * Define o valor da propriedade renewalDate.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setRenewalDate(GLogDateTimeType value) {
        this.renewalDate = value;
    }

    /**
     * Obtém o valor da propriedade gtmProdClassTypeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getGtmProdClassTypeGid() {
        return gtmProdClassTypeGid;
    }

    /**
     * Define o valor da propriedade gtmProdClassTypeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setGtmProdClassTypeGid(GLogXMLGidType value) {
        this.gtmProdClassTypeGid = value;
    }

    /**
     * Obtém o valor da propriedade classificationCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClassificationCode() {
        return classificationCode;
    }

    /**
     * Define o valor da propriedade classificationCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClassificationCode(String value) {
        this.classificationCode = value;
    }

    /**
     * Obtém o valor da propriedade indicator.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIndicator() {
        return indicator;
    }

    /**
     * Define o valor da propriedade indicator.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIndicator(String value) {
        this.indicator = value;
    }

    /**
     * Obtém o valor da propriedade userDefIconInfo.
     * 
     * @return
     *     possible object is
     *     {@link UserDefIconInfoType }
     *     
     */
    public UserDefIconInfoType getUserDefIconInfo() {
        return userDefIconInfo;
    }

    /**
     * Define o valor da propriedade userDefIconInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link UserDefIconInfoType }
     *     
     */
    public void setUserDefIconInfo(UserDefIconInfoType value) {
        this.userDefIconInfo = value;
    }

    /**
     * Obtém o valor da propriedade isActive.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsActive() {
        return isActive;
    }

    /**
     * Define o valor da propriedade isActive.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsActive(String value) {
        this.isActive = value;
    }

    /**
     * Obtém o valor da propriedade isTemplate.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsTemplate() {
        return isTemplate;
    }

    /**
     * Define o valor da propriedade isTemplate.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsTemplate(String value) {
        this.isTemplate = value;
    }

    /**
     * Gets the value of the gtmRegistrationDate property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the gtmRegistrationDate property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGtmRegistrationDate().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GtmRegistrationDateType }
     * 
     * 
     */
    public List<GtmRegistrationDateType> getGtmRegistrationDate() {
        if (gtmRegistrationDate == null) {
            gtmRegistrationDate = new ArrayList<GtmRegistrationDateType>();
        }
        return this.gtmRegistrationDate;
    }

    /**
     * Gets the value of the gtmRegRelatedItem property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the gtmRegRelatedItem property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGtmRegRelatedItem().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GtmRegRelatedItemType }
     * 
     * 
     */
    public List<GtmRegRelatedItemType> getGtmRegRelatedItem() {
        if (gtmRegRelatedItem == null) {
            gtmRegRelatedItem = new ArrayList<GtmRegRelatedItemType>();
        }
        return this.gtmRegRelatedItem;
    }

    /**
     * Gets the value of the gtmRegRelatedCommodity property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the gtmRegRelatedCommodity property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGtmRegRelatedCommodity().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GtmRegRelatedCommodityType }
     * 
     * 
     */
    public List<GtmRegRelatedCommodityType> getGtmRegRelatedCommodity() {
        if (gtmRegRelatedCommodity == null) {
            gtmRegRelatedCommodity = new ArrayList<GtmRegRelatedCommodityType>();
        }
        return this.gtmRegRelatedCommodity;
    }

    /**
     * Gets the value of the gtmRegRelatedUDCommodity property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the gtmRegRelatedUDCommodity property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGtmRegRelatedUDCommodity().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GtmRegRelatedUDCommodityType }
     * 
     * 
     */
    public List<GtmRegRelatedUDCommodityType> getGtmRegRelatedUDCommodity() {
        if (gtmRegRelatedUDCommodity == null) {
            gtmRegRelatedUDCommodity = new ArrayList<GtmRegRelatedUDCommodityType>();
        }
        return this.gtmRegRelatedUDCommodity;
    }

    /**
     * Gets the value of the gtmRegRelatedLicense property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the gtmRegRelatedLicense property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGtmRegRelatedLicense().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GtmRegRelatedLicenseType }
     * 
     * 
     */
    public List<GtmRegRelatedLicenseType> getGtmRegRelatedLicense() {
        if (gtmRegRelatedLicense == null) {
            gtmRegRelatedLicense = new ArrayList<GtmRegRelatedLicenseType>();
        }
        return this.gtmRegRelatedLicense;
    }

    /**
     * Gets the value of the gtmRegRelatedRegistration property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the gtmRegRelatedRegistration property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGtmRegRelatedRegistration().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GtmRegRelatedRegistrationType }
     * 
     * 
     */
    public List<GtmRegRelatedRegistrationType> getGtmRegRelatedRegistration() {
        if (gtmRegRelatedRegistration == null) {
            gtmRegRelatedRegistration = new ArrayList<GtmRegRelatedRegistrationType>();
        }
        return this.gtmRegRelatedRegistration;
    }

    /**
     * Gets the value of the quantity property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the quantity property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getQuantity().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link QuantityType }
     * 
     * 
     */
    public List<QuantityType> getQuantity() {
        if (quantity == null) {
            quantity = new ArrayList<QuantityType>();
        }
        return this.quantity;
    }

    /**
     * Gets the value of the currency property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the currency property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCurrency().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CurrencyType }
     * 
     * 
     */
    public List<CurrencyType> getCurrency() {
        if (currency == null) {
            currency = new ArrayList<CurrencyType>();
        }
        return this.currency;
    }

    /**
     * Gets the value of the status property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the status property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getStatus().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link StatusType }
     * 
     * 
     */
    public List<StatusType> getStatus() {
        if (status == null) {
            status = new ArrayList<StatusType>();
        }
        return this.status;
    }

    /**
     * Gets the value of the remark property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the remark property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRemark().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RemarkType }
     * 
     * 
     */
    public List<RemarkType> getRemark() {
        if (remark == null) {
            remark = new ArrayList<RemarkType>();
        }
        return this.remark;
    }

    /**
     * Gets the value of the involvedParty property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the involvedParty property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInvolvedParty().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link InvolvedPartyType }
     * 
     * 
     */
    public List<InvolvedPartyType> getInvolvedParty() {
        if (involvedParty == null) {
            involvedParty = new ArrayList<InvolvedPartyType>();
        }
        return this.involvedParty;
    }

    /**
     * Gets the value of the refnum property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the refnum property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRefnum().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RefnumType }
     * 
     * 
     */
    public List<RefnumType> getRefnum() {
        if (refnum == null) {
            refnum = new ArrayList<RefnumType>();
        }
        return this.refnum;
    }

    /**
     * Obtém o valor da propriedade regimeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRegimeGid() {
        return regimeGid;
    }

    /**
     * Define o valor da propriedade regimeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRegimeGid(GLogXMLGidType value) {
        this.regimeGid = value;
    }

}
