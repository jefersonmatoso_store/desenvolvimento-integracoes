
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Specifies the association between the SEquipment and the Shipment ShipUnit.
 * 
 * <p>Classe Java de SEquipmentSShipUnitInfoType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="SEquipmentSShipUnitInfoType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;choice>
 *           &lt;element name="SEquipmentGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *           &lt;element name="SEquipmentGidQuery" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}SEquipmentGidQueryType"/>
 *         &lt;/choice>
 *         &lt;element name="CompartmentNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LoadingSequence" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LoadingPatternGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="NumStackingLayers" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="NumLoadingRows" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SEquipmentSShipUnitInfoType", propOrder = {
    "sEquipmentGid",
    "sEquipmentGidQuery",
    "compartmentNum",
    "loadingSequence",
    "loadingPatternGid",
    "numStackingLayers",
    "numLoadingRows"
})
public class SEquipmentSShipUnitInfoType {

    @XmlElement(name = "SEquipmentGid")
    protected GLogXMLGidType sEquipmentGid;
    @XmlElement(name = "SEquipmentGidQuery")
    protected SEquipmentGidQueryType sEquipmentGidQuery;
    @XmlElement(name = "CompartmentNum")
    protected String compartmentNum;
    @XmlElement(name = "LoadingSequence")
    protected String loadingSequence;
    @XmlElement(name = "LoadingPatternGid")
    protected GLogXMLGidType loadingPatternGid;
    @XmlElement(name = "NumStackingLayers", required = true)
    protected String numStackingLayers;
    @XmlElement(name = "NumLoadingRows", required = true)
    protected String numLoadingRows;

    /**
     * Obtém o valor da propriedade sEquipmentGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getSEquipmentGid() {
        return sEquipmentGid;
    }

    /**
     * Define o valor da propriedade sEquipmentGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setSEquipmentGid(GLogXMLGidType value) {
        this.sEquipmentGid = value;
    }

    /**
     * Obtém o valor da propriedade sEquipmentGidQuery.
     * 
     * @return
     *     possible object is
     *     {@link SEquipmentGidQueryType }
     *     
     */
    public SEquipmentGidQueryType getSEquipmentGidQuery() {
        return sEquipmentGidQuery;
    }

    /**
     * Define o valor da propriedade sEquipmentGidQuery.
     * 
     * @param value
     *     allowed object is
     *     {@link SEquipmentGidQueryType }
     *     
     */
    public void setSEquipmentGidQuery(SEquipmentGidQueryType value) {
        this.sEquipmentGidQuery = value;
    }

    /**
     * Obtém o valor da propriedade compartmentNum.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCompartmentNum() {
        return compartmentNum;
    }

    /**
     * Define o valor da propriedade compartmentNum.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCompartmentNum(String value) {
        this.compartmentNum = value;
    }

    /**
     * Obtém o valor da propriedade loadingSequence.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLoadingSequence() {
        return loadingSequence;
    }

    /**
     * Define o valor da propriedade loadingSequence.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLoadingSequence(String value) {
        this.loadingSequence = value;
    }

    /**
     * Obtém o valor da propriedade loadingPatternGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getLoadingPatternGid() {
        return loadingPatternGid;
    }

    /**
     * Define o valor da propriedade loadingPatternGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setLoadingPatternGid(GLogXMLGidType value) {
        this.loadingPatternGid = value;
    }

    /**
     * Obtém o valor da propriedade numStackingLayers.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumStackingLayers() {
        return numStackingLayers;
    }

    /**
     * Define o valor da propriedade numStackingLayers.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumStackingLayers(String value) {
        this.numStackingLayers = value;
    }

    /**
     * Obtém o valor da propriedade numLoadingRows.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumLoadingRows() {
        return numLoadingRows;
    }

    /**
     * Define o valor da propriedade numLoadingRows.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumLoadingRows(String value) {
        this.numLoadingRows = value;
    }

}
