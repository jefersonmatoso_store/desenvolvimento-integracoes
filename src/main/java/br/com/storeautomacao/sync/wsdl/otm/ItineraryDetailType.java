
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Itinerary details. Contains references to path from source to destination.
 * 
 * <p>Classe Java de ItineraryDetailType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="ItineraryDetailType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SequenceNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="LegRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LegRefType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ItineraryDetailType", propOrder = {
    "sequenceNumber",
    "legRef"
})
public class ItineraryDetailType {

    @XmlElement(name = "SequenceNumber", required = true)
    protected String sequenceNumber;
    @XmlElement(name = "LegRef", required = true)
    protected LegRefType legRef;

    /**
     * Obtém o valor da propriedade sequenceNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSequenceNumber() {
        return sequenceNumber;
    }

    /**
     * Define o valor da propriedade sequenceNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSequenceNumber(String value) {
        this.sequenceNumber = value;
    }

    /**
     * Obtém o valor da propriedade legRef.
     * 
     * @return
     *     possible object is
     *     {@link LegRefType }
     *     
     */
    public LegRefType getLegRef() {
        return legRef;
    }

    /**
     * Define o valor da propriedade legRef.
     * 
     * @param value
     *     allowed object is
     *     {@link LegRefType }
     *     
     */
    public void setLegRef(LegRefType value) {
        this.legRef = value;
    }

}
