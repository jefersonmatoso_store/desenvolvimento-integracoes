
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;


/**
 * The ShipmentHeader information includes the shipment global identifier, transaction code,
 *             and others elements.
 * 
 *             The RateServiceGid element is outbound only.
 * 
 *             The SourceLocationRef element and DestinationLocationRef element are outbound only.
 * 
 *             Note: As of 6.0, the Shipment.SpecialService element has been replaced by the ShipmentSpecialService element.
 *          
 * 
 * <p>Classe Java de ShipmentHeaderType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="ShipmentHeaderType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SendReason" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}SendReasonType" minOccurs="0"/>
 *         &lt;element name="IntSavedQuery" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}IntSavedQueryType" minOccurs="0"/>
 *         &lt;element name="ShipmentGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="ShipmentName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ShipmentRefnum" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ShipmentRefnumType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="TransactionCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransactionCodeType"/>
 *         &lt;element name="ReplaceChildren" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ReplaceChildrenType" minOccurs="0"/>
 *         &lt;element name="IntCommand" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}IntCommandType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="ShipmentModViaOrderLine" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ShipmentModViaOrderLineType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="ShipmentModViaOrderSU" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ShipmentModViaOrderSUType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="ShipRouteExecutionInfo" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ShipRouteExecutionInfoType" minOccurs="0"/>
 *         &lt;element name="ProcessingCodeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="InternalShipmentStatus" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}StatusType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="PlannedShipmentInfo" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}PlannedShipmentInfoType" minOccurs="0"/>
 *         &lt;element name="ServiceProviderGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="ServiceProviderAlias" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ServiceProviderAliasType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="IsServiceProviderFixed" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ContactGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="IsTenderContactFixed" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RateOfferingGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="IsRateOfferingFixed" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RateGeoGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="IsRateGeoFixed" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="GlobalCurrencyCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ExchangeRateInfo" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ExchangeRateInfoType" minOccurs="0"/>
 *         &lt;element name="TotalPlannedCost" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/>
 *         &lt;element name="TotalActualCost" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/>
 *         &lt;element name="TotalWeightedCost" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/>
 *         &lt;element name="ShipmentCost" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ShipmentCostType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="ShipmentInfCost" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ShipmentInfCostType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="ShipmentInvoiceCostInfo" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ShipmentInvoiceCostInfoType" minOccurs="0"/>
 *         &lt;element name="Accessorial" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}AccessorialType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="IsCostFixed" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsSpotCosted" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsServiceTimeFixed" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ITransactionNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsHazardous" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RateServiceGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="RateServiceProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="TransportModeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="AirRailRouteCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsRecalcTotals" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TotalWeightVolume" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLWeightVolumeType" minOccurs="0"/>
 *         &lt;element name="TotalNetWeightVolume" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}WeightVolumeType" minOccurs="0"/>
 *         &lt;element name="TotalShipUnitCount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TotalPackagedItemSpecCount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TotalPackagedItemCount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="StartDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="EndDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="CommercialTerms" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}CommercialTermsType" minOccurs="0"/>
 *         &lt;element name="InsuranceInfo" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}InsuranceInfoType" minOccurs="0"/>
 *         &lt;element name="InvolvedParty" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}InvolvedPartyType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="AccessorialCodeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="ShipmentSpecialService" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ShipmentSpecialServiceType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Remark" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RemarkType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Tariff" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TariffType" minOccurs="0"/>
 *         &lt;element name="IsFixedDistance" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LoadedDistance" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDistanceType" minOccurs="0"/>
 *         &lt;element name="UnloadedDistance" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDistanceType" minOccurs="0"/>
 *         &lt;element name="StopCount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NumOrderReleases" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TotalNumReferenceUnits" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EquipmentRefUnitGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="IsTemperatureControl" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EarliestStartDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="LatestStartDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="VoyageGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="VesselGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="VesselName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SourceLocationRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocRefType" minOccurs="0"/>
 *         &lt;element name="SourceLocOverrideRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocOverrideRefType" minOccurs="0"/>
 *         &lt;element name="DestinationLocationRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocRefType" minOccurs="0"/>
 *         &lt;element name="DestLocOverrideRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocOverrideRefType" minOccurs="0"/>
 *         &lt;element name="PortOfLoadLocationRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocRefType" minOccurs="0"/>
 *         &lt;element name="PortOfLoadLocOverrideRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocOverrideRefType" minOccurs="0"/>
 *         &lt;element name="PortOfDisLocationRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocRefType" minOccurs="0"/>
 *         &lt;element name="PortOfDisLocOverrideRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocOverrideRefType" minOccurs="0"/>
 *         &lt;element name="SourcePierLocationRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocRefType" minOccurs="0"/>
 *         &lt;element name="DestPierLocationRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocRefType" minOccurs="0"/>
 *         &lt;element name="BookingInfo" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}BookingInfoType" minOccurs="0"/>
 *         &lt;element name="DelivServprovGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="OrigServprovGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="RouteCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RouteCodeType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="EquipMarks" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}EquipMarksType" minOccurs="0"/>
 *         &lt;element name="Text" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TextType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="DriverGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="SecondaryDriverGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="PowerUnitGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="ShipmentInfeasibility" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="FeasibilityCodeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="PlanningParamSetGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="EmergPhoneNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsFixedVoyageFlag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FlexFieldStrings" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldStringType" minOccurs="0"/>
 *         &lt;element name="FlexFieldNumbers" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldNumberType" minOccurs="0"/>
 *         &lt;element name="FlexFieldDates" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldDateType" minOccurs="0"/>
 *         &lt;element name="FlexFieldCurrencies" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldCurrencyType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ShipmentHeaderType", propOrder = {
    "sendReason",
    "intSavedQuery",
    "shipmentGid",
    "shipmentName",
    "shipmentRefnum",
    "transactionCode",
    "replaceChildren",
    "intCommand",
    "shipmentModViaOrderLine",
    "shipmentModViaOrderSU",
    "shipRouteExecutionInfo",
    "processingCodeGid",
    "internalShipmentStatus",
    "plannedShipmentInfo",
    "serviceProviderGid",
    "serviceProviderAlias",
    "isServiceProviderFixed",
    "contactGid",
    "isTenderContactFixed",
    "rateOfferingGid",
    "isRateOfferingFixed",
    "rateGeoGid",
    "isRateGeoFixed",
    "globalCurrencyCode",
    "exchangeRateInfo",
    "totalPlannedCost",
    "totalActualCost",
    "totalWeightedCost",
    "shipmentCost",
    "shipmentInfCost",
    "shipmentInvoiceCostInfo",
    "accessorial",
    "isCostFixed",
    "isSpotCosted",
    "isServiceTimeFixed",
    "iTransactionNo",
    "isHazardous",
    "rateServiceGid",
    "rateServiceProfileGid",
    "transportModeGid",
    "airRailRouteCode",
    "isRecalcTotals",
    "totalWeightVolume",
    "totalNetWeightVolume",
    "totalShipUnitCount",
    "totalPackagedItemSpecCount",
    "totalPackagedItemCount",
    "startDt",
    "endDt",
    "commercialTerms",
    "insuranceInfo",
    "involvedParty",
    "accessorialCodeGid",
    "shipmentSpecialService",
    "remark",
    "tariff",
    "isFixedDistance",
    "loadedDistance",
    "unloadedDistance",
    "stopCount",
    "numOrderReleases",
    "totalNumReferenceUnits",
    "equipmentRefUnitGid",
    "isTemperatureControl",
    "earliestStartDt",
    "latestStartDt",
    "voyageGid",
    "vesselGid",
    "vesselName",
    "sourceLocationRef",
    "sourceLocOverrideRef",
    "destinationLocationRef",
    "destLocOverrideRef",
    "portOfLoadLocationRef",
    "portOfLoadLocOverrideRef",
    "portOfDisLocationRef",
    "portOfDisLocOverrideRef",
    "sourcePierLocationRef",
    "destPierLocationRef",
    "bookingInfo",
    "delivServprovGid",
    "origServprovGid",
    "routeCode",
    "equipMarks",
    "text",
    "driverGid",
    "secondaryDriverGid",
    "powerUnitGid",
    "shipmentInfeasibility",
    "planningParamSetGid",
    "emergPhoneNum",
    "isFixedVoyageFlag",
    "flexFieldStrings",
    "flexFieldNumbers",
    "flexFieldDates",
    "flexFieldCurrencies"
})
public class ShipmentHeaderType {

    @XmlElement(name = "SendReason")
    protected SendReasonType sendReason;
    @XmlElement(name = "IntSavedQuery")
    protected IntSavedQueryType intSavedQuery;
    @XmlElement(name = "ShipmentGid")
    protected GLogXMLGidType shipmentGid;
    @XmlElement(name = "ShipmentName")
    protected String shipmentName;
    @XmlElement(name = "ShipmentRefnum")
    protected List<ShipmentRefnumType> shipmentRefnum;
    @XmlElement(name = "TransactionCode", required = true)
    @XmlSchemaType(name = "string")
    protected TransactionCodeType transactionCode;
    @XmlElement(name = "ReplaceChildren")
    protected ReplaceChildrenType replaceChildren;
    @XmlElement(name = "IntCommand")
    protected List<IntCommandType> intCommand;
    @XmlElement(name = "ShipmentModViaOrderLine")
    protected List<ShipmentModViaOrderLineType> shipmentModViaOrderLine;
    @XmlElement(name = "ShipmentModViaOrderSU")
    protected List<ShipmentModViaOrderSUType> shipmentModViaOrderSU;
    @XmlElement(name = "ShipRouteExecutionInfo")
    protected ShipRouteExecutionInfoType shipRouteExecutionInfo;
    @XmlElement(name = "ProcessingCodeGid")
    protected GLogXMLGidType processingCodeGid;
    @XmlElement(name = "InternalShipmentStatus")
    protected List<StatusType> internalShipmentStatus;
    @XmlElement(name = "PlannedShipmentInfo")
    protected PlannedShipmentInfoType plannedShipmentInfo;
    @XmlElement(name = "ServiceProviderGid")
    protected GLogXMLGidType serviceProviderGid;
    @XmlElement(name = "ServiceProviderAlias")
    protected List<ServiceProviderAliasType> serviceProviderAlias;
    @XmlElement(name = "IsServiceProviderFixed")
    protected String isServiceProviderFixed;
    @XmlElement(name = "ContactGid")
    protected GLogXMLGidType contactGid;
    @XmlElement(name = "IsTenderContactFixed")
    protected String isTenderContactFixed;
    @XmlElement(name = "RateOfferingGid")
    protected GLogXMLGidType rateOfferingGid;
    @XmlElement(name = "IsRateOfferingFixed")
    protected String isRateOfferingFixed;
    @XmlElement(name = "RateGeoGid")
    protected GLogXMLGidType rateGeoGid;
    @XmlElement(name = "IsRateGeoFixed")
    protected String isRateGeoFixed;
    @XmlElement(name = "GlobalCurrencyCode")
    protected String globalCurrencyCode;
    @XmlElement(name = "ExchangeRateInfo")
    protected ExchangeRateInfoType exchangeRateInfo;
    @XmlElement(name = "TotalPlannedCost")
    protected GLogXMLFinancialAmountType totalPlannedCost;
    @XmlElement(name = "TotalActualCost")
    protected GLogXMLFinancialAmountType totalActualCost;
    @XmlElement(name = "TotalWeightedCost")
    protected GLogXMLFinancialAmountType totalWeightedCost;
    @XmlElement(name = "ShipmentCost")
    protected List<ShipmentCostType> shipmentCost;
    @XmlElement(name = "ShipmentInfCost")
    protected List<ShipmentInfCostType> shipmentInfCost;
    @XmlElement(name = "ShipmentInvoiceCostInfo")
    protected ShipmentInvoiceCostInfoType shipmentInvoiceCostInfo;
    @XmlElement(name = "Accessorial")
    protected List<AccessorialType> accessorial;
    @XmlElement(name = "IsCostFixed")
    protected String isCostFixed;
    @XmlElement(name = "IsSpotCosted")
    protected String isSpotCosted;
    @XmlElement(name = "IsServiceTimeFixed")
    protected String isServiceTimeFixed;
    @XmlElement(name = "ITransactionNo")
    protected String iTransactionNo;
    @XmlElement(name = "IsHazardous")
    protected String isHazardous;
    @XmlElement(name = "RateServiceGid")
    protected GLogXMLGidType rateServiceGid;
    @XmlElement(name = "RateServiceProfileGid")
    protected GLogXMLGidType rateServiceProfileGid;
    @XmlElement(name = "TransportModeGid")
    protected GLogXMLGidType transportModeGid;
    @XmlElement(name = "AirRailRouteCode")
    protected String airRailRouteCode;
    @XmlElement(name = "IsRecalcTotals")
    protected String isRecalcTotals;
    @XmlElement(name = "TotalWeightVolume")
    protected GLogXMLWeightVolumeType totalWeightVolume;
    @XmlElement(name = "TotalNetWeightVolume")
    protected WeightVolumeType totalNetWeightVolume;
    @XmlElement(name = "TotalShipUnitCount")
    protected String totalShipUnitCount;
    @XmlElement(name = "TotalPackagedItemSpecCount")
    protected String totalPackagedItemSpecCount;
    @XmlElement(name = "TotalPackagedItemCount")
    protected String totalPackagedItemCount;
    @XmlElement(name = "StartDt")
    protected GLogDateTimeType startDt;
    @XmlElement(name = "EndDt")
    protected GLogDateTimeType endDt;
    @XmlElement(name = "CommercialTerms")
    protected CommercialTermsType commercialTerms;
    @XmlElement(name = "InsuranceInfo")
    protected InsuranceInfoType insuranceInfo;
    @XmlElement(name = "InvolvedParty")
    protected List<InvolvedPartyType> involvedParty;
    @XmlElement(name = "AccessorialCodeGid")
    protected List<GLogXMLGidType> accessorialCodeGid;
    @XmlElement(name = "ShipmentSpecialService")
    protected List<ShipmentSpecialServiceType> shipmentSpecialService;
    @XmlElement(name = "Remark")
    protected List<RemarkType> remark;
    @XmlElement(name = "Tariff")
    protected TariffType tariff;
    @XmlElement(name = "IsFixedDistance")
    protected String isFixedDistance;
    @XmlElement(name = "LoadedDistance")
    protected GLogXMLDistanceType loadedDistance;
    @XmlElement(name = "UnloadedDistance")
    protected GLogXMLDistanceType unloadedDistance;
    @XmlElement(name = "StopCount")
    protected String stopCount;
    @XmlElement(name = "NumOrderReleases")
    protected String numOrderReleases;
    @XmlElement(name = "TotalNumReferenceUnits")
    protected String totalNumReferenceUnits;
    @XmlElement(name = "EquipmentRefUnitGid")
    protected GLogXMLGidType equipmentRefUnitGid;
    @XmlElement(name = "IsTemperatureControl")
    protected String isTemperatureControl;
    @XmlElement(name = "EarliestStartDt")
    protected GLogDateTimeType earliestStartDt;
    @XmlElement(name = "LatestStartDt")
    protected GLogDateTimeType latestStartDt;
    @XmlElement(name = "VoyageGid")
    protected GLogXMLGidType voyageGid;
    @XmlElement(name = "VesselGid")
    protected GLogXMLGidType vesselGid;
    @XmlElement(name = "VesselName")
    protected String vesselName;
    @XmlElement(name = "SourceLocationRef")
    protected GLogXMLLocRefType sourceLocationRef;
    @XmlElement(name = "SourceLocOverrideRef")
    protected GLogXMLLocOverrideRefType sourceLocOverrideRef;
    @XmlElement(name = "DestinationLocationRef")
    protected GLogXMLLocRefType destinationLocationRef;
    @XmlElement(name = "DestLocOverrideRef")
    protected GLogXMLLocOverrideRefType destLocOverrideRef;
    @XmlElement(name = "PortOfLoadLocationRef")
    protected GLogXMLLocRefType portOfLoadLocationRef;
    @XmlElement(name = "PortOfLoadLocOverrideRef")
    protected GLogXMLLocOverrideRefType portOfLoadLocOverrideRef;
    @XmlElement(name = "PortOfDisLocationRef")
    protected GLogXMLLocRefType portOfDisLocationRef;
    @XmlElement(name = "PortOfDisLocOverrideRef")
    protected GLogXMLLocOverrideRefType portOfDisLocOverrideRef;
    @XmlElement(name = "SourcePierLocationRef")
    protected GLogXMLLocRefType sourcePierLocationRef;
    @XmlElement(name = "DestPierLocationRef")
    protected GLogXMLLocRefType destPierLocationRef;
    @XmlElement(name = "BookingInfo")
    protected BookingInfoType bookingInfo;
    @XmlElement(name = "DelivServprovGid")
    protected GLogXMLGidType delivServprovGid;
    @XmlElement(name = "OrigServprovGid")
    protected GLogXMLGidType origServprovGid;
    @XmlElement(name = "RouteCode")
    protected List<RouteCodeType> routeCode;
    @XmlElement(name = "EquipMarks")
    protected EquipMarksType equipMarks;
    @XmlElement(name = "Text")
    protected List<TextType> text;
    @XmlElement(name = "DriverGid")
    protected GLogXMLGidType driverGid;
    @XmlElement(name = "SecondaryDriverGid")
    protected GLogXMLGidType secondaryDriverGid;
    @XmlElement(name = "PowerUnitGid")
    protected GLogXMLGidType powerUnitGid;
    @XmlElement(name = "ShipmentInfeasibility")
    protected List<ShipmentInfeasibility> shipmentInfeasibility;
    @XmlElement(name = "PlanningParamSetGid")
    protected GLogXMLGidType planningParamSetGid;
    @XmlElement(name = "EmergPhoneNum")
    protected String emergPhoneNum;
    @XmlElement(name = "IsFixedVoyageFlag")
    protected String isFixedVoyageFlag;
    @XmlElement(name = "FlexFieldStrings")
    protected FlexFieldStringType flexFieldStrings;
    @XmlElement(name = "FlexFieldNumbers")
    protected FlexFieldNumberType flexFieldNumbers;
    @XmlElement(name = "FlexFieldDates")
    protected FlexFieldDateType flexFieldDates;
    @XmlElement(name = "FlexFieldCurrencies")
    protected FlexFieldCurrencyType flexFieldCurrencies;

    /**
     * Obtém o valor da propriedade sendReason.
     * 
     * @return
     *     possible object is
     *     {@link SendReasonType }
     *     
     */
    public SendReasonType getSendReason() {
        return sendReason;
    }

    /**
     * Define o valor da propriedade sendReason.
     * 
     * @param value
     *     allowed object is
     *     {@link SendReasonType }
     *     
     */
    public void setSendReason(SendReasonType value) {
        this.sendReason = value;
    }

    /**
     * Obtém o valor da propriedade intSavedQuery.
     * 
     * @return
     *     possible object is
     *     {@link IntSavedQueryType }
     *     
     */
    public IntSavedQueryType getIntSavedQuery() {
        return intSavedQuery;
    }

    /**
     * Define o valor da propriedade intSavedQuery.
     * 
     * @param value
     *     allowed object is
     *     {@link IntSavedQueryType }
     *     
     */
    public void setIntSavedQuery(IntSavedQueryType value) {
        this.intSavedQuery = value;
    }

    /**
     * Obtém o valor da propriedade shipmentGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getShipmentGid() {
        return shipmentGid;
    }

    /**
     * Define o valor da propriedade shipmentGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setShipmentGid(GLogXMLGidType value) {
        this.shipmentGid = value;
    }

    /**
     * Obtém o valor da propriedade shipmentName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipmentName() {
        return shipmentName;
    }

    /**
     * Define o valor da propriedade shipmentName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipmentName(String value) {
        this.shipmentName = value;
    }

    /**
     * Gets the value of the shipmentRefnum property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the shipmentRefnum property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getShipmentRefnum().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ShipmentRefnumType }
     * 
     * 
     */
    public List<ShipmentRefnumType> getShipmentRefnum() {
        if (shipmentRefnum == null) {
            shipmentRefnum = new ArrayList<ShipmentRefnumType>();
        }
        return this.shipmentRefnum;
    }

    /**
     * Obtém o valor da propriedade transactionCode.
     * 
     * @return
     *     possible object is
     *     {@link TransactionCodeType }
     *     
     */
    public TransactionCodeType getTransactionCode() {
        return transactionCode;
    }

    /**
     * Define o valor da propriedade transactionCode.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionCodeType }
     *     
     */
    public void setTransactionCode(TransactionCodeType value) {
        this.transactionCode = value;
    }

    /**
     * Obtém o valor da propriedade replaceChildren.
     * 
     * @return
     *     possible object is
     *     {@link ReplaceChildrenType }
     *     
     */
    public ReplaceChildrenType getReplaceChildren() {
        return replaceChildren;
    }

    /**
     * Define o valor da propriedade replaceChildren.
     * 
     * @param value
     *     allowed object is
     *     {@link ReplaceChildrenType }
     *     
     */
    public void setReplaceChildren(ReplaceChildrenType value) {
        this.replaceChildren = value;
    }

    /**
     * Gets the value of the intCommand property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the intCommand property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getIntCommand().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link IntCommandType }
     * 
     * 
     */
    public List<IntCommandType> getIntCommand() {
        if (intCommand == null) {
            intCommand = new ArrayList<IntCommandType>();
        }
        return this.intCommand;
    }

    /**
     * Gets the value of the shipmentModViaOrderLine property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the shipmentModViaOrderLine property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getShipmentModViaOrderLine().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ShipmentModViaOrderLineType }
     * 
     * 
     */
    public List<ShipmentModViaOrderLineType> getShipmentModViaOrderLine() {
        if (shipmentModViaOrderLine == null) {
            shipmentModViaOrderLine = new ArrayList<ShipmentModViaOrderLineType>();
        }
        return this.shipmentModViaOrderLine;
    }

    /**
     * Gets the value of the shipmentModViaOrderSU property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the shipmentModViaOrderSU property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getShipmentModViaOrderSU().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ShipmentModViaOrderSUType }
     * 
     * 
     */
    public List<ShipmentModViaOrderSUType> getShipmentModViaOrderSU() {
        if (shipmentModViaOrderSU == null) {
            shipmentModViaOrderSU = new ArrayList<ShipmentModViaOrderSUType>();
        }
        return this.shipmentModViaOrderSU;
    }

    /**
     * Obtém o valor da propriedade shipRouteExecutionInfo.
     * 
     * @return
     *     possible object is
     *     {@link ShipRouteExecutionInfoType }
     *     
     */
    public ShipRouteExecutionInfoType getShipRouteExecutionInfo() {
        return shipRouteExecutionInfo;
    }

    /**
     * Define o valor da propriedade shipRouteExecutionInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link ShipRouteExecutionInfoType }
     *     
     */
    public void setShipRouteExecutionInfo(ShipRouteExecutionInfoType value) {
        this.shipRouteExecutionInfo = value;
    }

    /**
     * Obtém o valor da propriedade processingCodeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getProcessingCodeGid() {
        return processingCodeGid;
    }

    /**
     * Define o valor da propriedade processingCodeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setProcessingCodeGid(GLogXMLGidType value) {
        this.processingCodeGid = value;
    }

    /**
     * Gets the value of the internalShipmentStatus property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the internalShipmentStatus property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInternalShipmentStatus().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link StatusType }
     * 
     * 
     */
    public List<StatusType> getInternalShipmentStatus() {
        if (internalShipmentStatus == null) {
            internalShipmentStatus = new ArrayList<StatusType>();
        }
        return this.internalShipmentStatus;
    }

    /**
     * Obtém o valor da propriedade plannedShipmentInfo.
     * 
     * @return
     *     possible object is
     *     {@link PlannedShipmentInfoType }
     *     
     */
    public PlannedShipmentInfoType getPlannedShipmentInfo() {
        return plannedShipmentInfo;
    }

    /**
     * Define o valor da propriedade plannedShipmentInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link PlannedShipmentInfoType }
     *     
     */
    public void setPlannedShipmentInfo(PlannedShipmentInfoType value) {
        this.plannedShipmentInfo = value;
    }

    /**
     * Obtém o valor da propriedade serviceProviderGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getServiceProviderGid() {
        return serviceProviderGid;
    }

    /**
     * Define o valor da propriedade serviceProviderGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setServiceProviderGid(GLogXMLGidType value) {
        this.serviceProviderGid = value;
    }

    /**
     * Gets the value of the serviceProviderAlias property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the serviceProviderAlias property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getServiceProviderAlias().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ServiceProviderAliasType }
     * 
     * 
     */
    public List<ServiceProviderAliasType> getServiceProviderAlias() {
        if (serviceProviderAlias == null) {
            serviceProviderAlias = new ArrayList<ServiceProviderAliasType>();
        }
        return this.serviceProviderAlias;
    }

    /**
     * Obtém o valor da propriedade isServiceProviderFixed.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsServiceProviderFixed() {
        return isServiceProviderFixed;
    }

    /**
     * Define o valor da propriedade isServiceProviderFixed.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsServiceProviderFixed(String value) {
        this.isServiceProviderFixed = value;
    }

    /**
     * Obtém o valor da propriedade contactGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getContactGid() {
        return contactGid;
    }

    /**
     * Define o valor da propriedade contactGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setContactGid(GLogXMLGidType value) {
        this.contactGid = value;
    }

    /**
     * Obtém o valor da propriedade isTenderContactFixed.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsTenderContactFixed() {
        return isTenderContactFixed;
    }

    /**
     * Define o valor da propriedade isTenderContactFixed.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsTenderContactFixed(String value) {
        this.isTenderContactFixed = value;
    }

    /**
     * Obtém o valor da propriedade rateOfferingGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRateOfferingGid() {
        return rateOfferingGid;
    }

    /**
     * Define o valor da propriedade rateOfferingGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRateOfferingGid(GLogXMLGidType value) {
        this.rateOfferingGid = value;
    }

    /**
     * Obtém o valor da propriedade isRateOfferingFixed.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsRateOfferingFixed() {
        return isRateOfferingFixed;
    }

    /**
     * Define o valor da propriedade isRateOfferingFixed.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsRateOfferingFixed(String value) {
        this.isRateOfferingFixed = value;
    }

    /**
     * Obtém o valor da propriedade rateGeoGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRateGeoGid() {
        return rateGeoGid;
    }

    /**
     * Define o valor da propriedade rateGeoGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRateGeoGid(GLogXMLGidType value) {
        this.rateGeoGid = value;
    }

    /**
     * Obtém o valor da propriedade isRateGeoFixed.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsRateGeoFixed() {
        return isRateGeoFixed;
    }

    /**
     * Define o valor da propriedade isRateGeoFixed.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsRateGeoFixed(String value) {
        this.isRateGeoFixed = value;
    }

    /**
     * Obtém o valor da propriedade globalCurrencyCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGlobalCurrencyCode() {
        return globalCurrencyCode;
    }

    /**
     * Define o valor da propriedade globalCurrencyCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGlobalCurrencyCode(String value) {
        this.globalCurrencyCode = value;
    }

    /**
     * Obtém o valor da propriedade exchangeRateInfo.
     * 
     * @return
     *     possible object is
     *     {@link ExchangeRateInfoType }
     *     
     */
    public ExchangeRateInfoType getExchangeRateInfo() {
        return exchangeRateInfo;
    }

    /**
     * Define o valor da propriedade exchangeRateInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link ExchangeRateInfoType }
     *     
     */
    public void setExchangeRateInfo(ExchangeRateInfoType value) {
        this.exchangeRateInfo = value;
    }

    /**
     * Obtém o valor da propriedade totalPlannedCost.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getTotalPlannedCost() {
        return totalPlannedCost;
    }

    /**
     * Define o valor da propriedade totalPlannedCost.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setTotalPlannedCost(GLogXMLFinancialAmountType value) {
        this.totalPlannedCost = value;
    }

    /**
     * Obtém o valor da propriedade totalActualCost.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getTotalActualCost() {
        return totalActualCost;
    }

    /**
     * Define o valor da propriedade totalActualCost.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setTotalActualCost(GLogXMLFinancialAmountType value) {
        this.totalActualCost = value;
    }

    /**
     * Obtém o valor da propriedade totalWeightedCost.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getTotalWeightedCost() {
        return totalWeightedCost;
    }

    /**
     * Define o valor da propriedade totalWeightedCost.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setTotalWeightedCost(GLogXMLFinancialAmountType value) {
        this.totalWeightedCost = value;
    }

    /**
     * Gets the value of the shipmentCost property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the shipmentCost property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getShipmentCost().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ShipmentCostType }
     * 
     * 
     */
    public List<ShipmentCostType> getShipmentCost() {
        if (shipmentCost == null) {
            shipmentCost = new ArrayList<ShipmentCostType>();
        }
        return this.shipmentCost;
    }

    /**
     * Gets the value of the shipmentInfCost property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the shipmentInfCost property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getShipmentInfCost().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ShipmentInfCostType }
     * 
     * 
     */
    public List<ShipmentInfCostType> getShipmentInfCost() {
        if (shipmentInfCost == null) {
            shipmentInfCost = new ArrayList<ShipmentInfCostType>();
        }
        return this.shipmentInfCost;
    }

    /**
     * Obtém o valor da propriedade shipmentInvoiceCostInfo.
     * 
     * @return
     *     possible object is
     *     {@link ShipmentInvoiceCostInfoType }
     *     
     */
    public ShipmentInvoiceCostInfoType getShipmentInvoiceCostInfo() {
        return shipmentInvoiceCostInfo;
    }

    /**
     * Define o valor da propriedade shipmentInvoiceCostInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link ShipmentInvoiceCostInfoType }
     *     
     */
    public void setShipmentInvoiceCostInfo(ShipmentInvoiceCostInfoType value) {
        this.shipmentInvoiceCostInfo = value;
    }

    /**
     * Gets the value of the accessorial property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the accessorial property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAccessorial().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AccessorialType }
     * 
     * 
     */
    public List<AccessorialType> getAccessorial() {
        if (accessorial == null) {
            accessorial = new ArrayList<AccessorialType>();
        }
        return this.accessorial;
    }

    /**
     * Obtém o valor da propriedade isCostFixed.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsCostFixed() {
        return isCostFixed;
    }

    /**
     * Define o valor da propriedade isCostFixed.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsCostFixed(String value) {
        this.isCostFixed = value;
    }

    /**
     * Obtém o valor da propriedade isSpotCosted.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsSpotCosted() {
        return isSpotCosted;
    }

    /**
     * Define o valor da propriedade isSpotCosted.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsSpotCosted(String value) {
        this.isSpotCosted = value;
    }

    /**
     * Obtém o valor da propriedade isServiceTimeFixed.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsServiceTimeFixed() {
        return isServiceTimeFixed;
    }

    /**
     * Define o valor da propriedade isServiceTimeFixed.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsServiceTimeFixed(String value) {
        this.isServiceTimeFixed = value;
    }

    /**
     * Obtém o valor da propriedade iTransactionNo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getITransactionNo() {
        return iTransactionNo;
    }

    /**
     * Define o valor da propriedade iTransactionNo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setITransactionNo(String value) {
        this.iTransactionNo = value;
    }

    /**
     * Obtém o valor da propriedade isHazardous.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsHazardous() {
        return isHazardous;
    }

    /**
     * Define o valor da propriedade isHazardous.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsHazardous(String value) {
        this.isHazardous = value;
    }

    /**
     * Obtém o valor da propriedade rateServiceGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRateServiceGid() {
        return rateServiceGid;
    }

    /**
     * Define o valor da propriedade rateServiceGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRateServiceGid(GLogXMLGidType value) {
        this.rateServiceGid = value;
    }

    /**
     * Obtém o valor da propriedade rateServiceProfileGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRateServiceProfileGid() {
        return rateServiceProfileGid;
    }

    /**
     * Define o valor da propriedade rateServiceProfileGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRateServiceProfileGid(GLogXMLGidType value) {
        this.rateServiceProfileGid = value;
    }

    /**
     * Obtém o valor da propriedade transportModeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getTransportModeGid() {
        return transportModeGid;
    }

    /**
     * Define o valor da propriedade transportModeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setTransportModeGid(GLogXMLGidType value) {
        this.transportModeGid = value;
    }

    /**
     * Obtém o valor da propriedade airRailRouteCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAirRailRouteCode() {
        return airRailRouteCode;
    }

    /**
     * Define o valor da propriedade airRailRouteCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAirRailRouteCode(String value) {
        this.airRailRouteCode = value;
    }

    /**
     * Obtém o valor da propriedade isRecalcTotals.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsRecalcTotals() {
        return isRecalcTotals;
    }

    /**
     * Define o valor da propriedade isRecalcTotals.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsRecalcTotals(String value) {
        this.isRecalcTotals = value;
    }

    /**
     * Obtém o valor da propriedade totalWeightVolume.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLWeightVolumeType }
     *     
     */
    public GLogXMLWeightVolumeType getTotalWeightVolume() {
        return totalWeightVolume;
    }

    /**
     * Define o valor da propriedade totalWeightVolume.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLWeightVolumeType }
     *     
     */
    public void setTotalWeightVolume(GLogXMLWeightVolumeType value) {
        this.totalWeightVolume = value;
    }

    /**
     * Obtém o valor da propriedade totalNetWeightVolume.
     * 
     * @return
     *     possible object is
     *     {@link WeightVolumeType }
     *     
     */
    public WeightVolumeType getTotalNetWeightVolume() {
        return totalNetWeightVolume;
    }

    /**
     * Define o valor da propriedade totalNetWeightVolume.
     * 
     * @param value
     *     allowed object is
     *     {@link WeightVolumeType }
     *     
     */
    public void setTotalNetWeightVolume(WeightVolumeType value) {
        this.totalNetWeightVolume = value;
    }

    /**
     * Obtém o valor da propriedade totalShipUnitCount.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTotalShipUnitCount() {
        return totalShipUnitCount;
    }

    /**
     * Define o valor da propriedade totalShipUnitCount.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTotalShipUnitCount(String value) {
        this.totalShipUnitCount = value;
    }

    /**
     * Obtém o valor da propriedade totalPackagedItemSpecCount.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTotalPackagedItemSpecCount() {
        return totalPackagedItemSpecCount;
    }

    /**
     * Define o valor da propriedade totalPackagedItemSpecCount.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTotalPackagedItemSpecCount(String value) {
        this.totalPackagedItemSpecCount = value;
    }

    /**
     * Obtém o valor da propriedade totalPackagedItemCount.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTotalPackagedItemCount() {
        return totalPackagedItemCount;
    }

    /**
     * Define o valor da propriedade totalPackagedItemCount.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTotalPackagedItemCount(String value) {
        this.totalPackagedItemCount = value;
    }

    /**
     * Obtém o valor da propriedade startDt.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getStartDt() {
        return startDt;
    }

    /**
     * Define o valor da propriedade startDt.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setStartDt(GLogDateTimeType value) {
        this.startDt = value;
    }

    /**
     * Obtém o valor da propriedade endDt.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getEndDt() {
        return endDt;
    }

    /**
     * Define o valor da propriedade endDt.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setEndDt(GLogDateTimeType value) {
        this.endDt = value;
    }

    /**
     * Obtém o valor da propriedade commercialTerms.
     * 
     * @return
     *     possible object is
     *     {@link CommercialTermsType }
     *     
     */
    public CommercialTermsType getCommercialTerms() {
        return commercialTerms;
    }

    /**
     * Define o valor da propriedade commercialTerms.
     * 
     * @param value
     *     allowed object is
     *     {@link CommercialTermsType }
     *     
     */
    public void setCommercialTerms(CommercialTermsType value) {
        this.commercialTerms = value;
    }

    /**
     * Obtém o valor da propriedade insuranceInfo.
     * 
     * @return
     *     possible object is
     *     {@link InsuranceInfoType }
     *     
     */
    public InsuranceInfoType getInsuranceInfo() {
        return insuranceInfo;
    }

    /**
     * Define o valor da propriedade insuranceInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link InsuranceInfoType }
     *     
     */
    public void setInsuranceInfo(InsuranceInfoType value) {
        this.insuranceInfo = value;
    }

    /**
     * Gets the value of the involvedParty property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the involvedParty property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInvolvedParty().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link InvolvedPartyType }
     * 
     * 
     */
    public List<InvolvedPartyType> getInvolvedParty() {
        if (involvedParty == null) {
            involvedParty = new ArrayList<InvolvedPartyType>();
        }
        return this.involvedParty;
    }

    /**
     * Gets the value of the accessorialCodeGid property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the accessorialCodeGid property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAccessorialCodeGid().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GLogXMLGidType }
     * 
     * 
     */
    public List<GLogXMLGidType> getAccessorialCodeGid() {
        if (accessorialCodeGid == null) {
            accessorialCodeGid = new ArrayList<GLogXMLGidType>();
        }
        return this.accessorialCodeGid;
    }

    /**
     * Gets the value of the shipmentSpecialService property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the shipmentSpecialService property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getShipmentSpecialService().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ShipmentSpecialServiceType }
     * 
     * 
     */
    public List<ShipmentSpecialServiceType> getShipmentSpecialService() {
        if (shipmentSpecialService == null) {
            shipmentSpecialService = new ArrayList<ShipmentSpecialServiceType>();
        }
        return this.shipmentSpecialService;
    }

    /**
     * Gets the value of the remark property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the remark property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRemark().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RemarkType }
     * 
     * 
     */
    public List<RemarkType> getRemark() {
        if (remark == null) {
            remark = new ArrayList<RemarkType>();
        }
        return this.remark;
    }

    /**
     * Obtém o valor da propriedade tariff.
     * 
     * @return
     *     possible object is
     *     {@link TariffType }
     *     
     */
    public TariffType getTariff() {
        return tariff;
    }

    /**
     * Define o valor da propriedade tariff.
     * 
     * @param value
     *     allowed object is
     *     {@link TariffType }
     *     
     */
    public void setTariff(TariffType value) {
        this.tariff = value;
    }

    /**
     * Obtém o valor da propriedade isFixedDistance.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsFixedDistance() {
        return isFixedDistance;
    }

    /**
     * Define o valor da propriedade isFixedDistance.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsFixedDistance(String value) {
        this.isFixedDistance = value;
    }

    /**
     * Obtém o valor da propriedade loadedDistance.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDistanceType }
     *     
     */
    public GLogXMLDistanceType getLoadedDistance() {
        return loadedDistance;
    }

    /**
     * Define o valor da propriedade loadedDistance.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDistanceType }
     *     
     */
    public void setLoadedDistance(GLogXMLDistanceType value) {
        this.loadedDistance = value;
    }

    /**
     * Obtém o valor da propriedade unloadedDistance.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDistanceType }
     *     
     */
    public GLogXMLDistanceType getUnloadedDistance() {
        return unloadedDistance;
    }

    /**
     * Define o valor da propriedade unloadedDistance.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDistanceType }
     *     
     */
    public void setUnloadedDistance(GLogXMLDistanceType value) {
        this.unloadedDistance = value;
    }

    /**
     * Obtém o valor da propriedade stopCount.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStopCount() {
        return stopCount;
    }

    /**
     * Define o valor da propriedade stopCount.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStopCount(String value) {
        this.stopCount = value;
    }

    /**
     * Obtém o valor da propriedade numOrderReleases.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumOrderReleases() {
        return numOrderReleases;
    }

    /**
     * Define o valor da propriedade numOrderReleases.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumOrderReleases(String value) {
        this.numOrderReleases = value;
    }

    /**
     * Obtém o valor da propriedade totalNumReferenceUnits.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTotalNumReferenceUnits() {
        return totalNumReferenceUnits;
    }

    /**
     * Define o valor da propriedade totalNumReferenceUnits.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTotalNumReferenceUnits(String value) {
        this.totalNumReferenceUnits = value;
    }

    /**
     * Obtém o valor da propriedade equipmentRefUnitGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getEquipmentRefUnitGid() {
        return equipmentRefUnitGid;
    }

    /**
     * Define o valor da propriedade equipmentRefUnitGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setEquipmentRefUnitGid(GLogXMLGidType value) {
        this.equipmentRefUnitGid = value;
    }

    /**
     * Obtém o valor da propriedade isTemperatureControl.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsTemperatureControl() {
        return isTemperatureControl;
    }

    /**
     * Define o valor da propriedade isTemperatureControl.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsTemperatureControl(String value) {
        this.isTemperatureControl = value;
    }

    /**
     * Obtém o valor da propriedade earliestStartDt.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getEarliestStartDt() {
        return earliestStartDt;
    }

    /**
     * Define o valor da propriedade earliestStartDt.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setEarliestStartDt(GLogDateTimeType value) {
        this.earliestStartDt = value;
    }

    /**
     * Obtém o valor da propriedade latestStartDt.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getLatestStartDt() {
        return latestStartDt;
    }

    /**
     * Define o valor da propriedade latestStartDt.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setLatestStartDt(GLogDateTimeType value) {
        this.latestStartDt = value;
    }

    /**
     * Obtém o valor da propriedade voyageGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getVoyageGid() {
        return voyageGid;
    }

    /**
     * Define o valor da propriedade voyageGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setVoyageGid(GLogXMLGidType value) {
        this.voyageGid = value;
    }

    /**
     * Obtém o valor da propriedade vesselGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getVesselGid() {
        return vesselGid;
    }

    /**
     * Define o valor da propriedade vesselGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setVesselGid(GLogXMLGidType value) {
        this.vesselGid = value;
    }

    /**
     * Obtém o valor da propriedade vesselName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVesselName() {
        return vesselName;
    }

    /**
     * Define o valor da propriedade vesselName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVesselName(String value) {
        this.vesselName = value;
    }

    /**
     * Obtém o valor da propriedade sourceLocationRef.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public GLogXMLLocRefType getSourceLocationRef() {
        return sourceLocationRef;
    }

    /**
     * Define o valor da propriedade sourceLocationRef.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public void setSourceLocationRef(GLogXMLLocRefType value) {
        this.sourceLocationRef = value;
    }

    /**
     * Obtém o valor da propriedade sourceLocOverrideRef.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocOverrideRefType }
     *     
     */
    public GLogXMLLocOverrideRefType getSourceLocOverrideRef() {
        return sourceLocOverrideRef;
    }

    /**
     * Define o valor da propriedade sourceLocOverrideRef.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocOverrideRefType }
     *     
     */
    public void setSourceLocOverrideRef(GLogXMLLocOverrideRefType value) {
        this.sourceLocOverrideRef = value;
    }

    /**
     * Obtém o valor da propriedade destinationLocationRef.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public GLogXMLLocRefType getDestinationLocationRef() {
        return destinationLocationRef;
    }

    /**
     * Define o valor da propriedade destinationLocationRef.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public void setDestinationLocationRef(GLogXMLLocRefType value) {
        this.destinationLocationRef = value;
    }

    /**
     * Obtém o valor da propriedade destLocOverrideRef.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocOverrideRefType }
     *     
     */
    public GLogXMLLocOverrideRefType getDestLocOverrideRef() {
        return destLocOverrideRef;
    }

    /**
     * Define o valor da propriedade destLocOverrideRef.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocOverrideRefType }
     *     
     */
    public void setDestLocOverrideRef(GLogXMLLocOverrideRefType value) {
        this.destLocOverrideRef = value;
    }

    /**
     * Obtém o valor da propriedade portOfLoadLocationRef.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public GLogXMLLocRefType getPortOfLoadLocationRef() {
        return portOfLoadLocationRef;
    }

    /**
     * Define o valor da propriedade portOfLoadLocationRef.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public void setPortOfLoadLocationRef(GLogXMLLocRefType value) {
        this.portOfLoadLocationRef = value;
    }

    /**
     * Obtém o valor da propriedade portOfLoadLocOverrideRef.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocOverrideRefType }
     *     
     */
    public GLogXMLLocOverrideRefType getPortOfLoadLocOverrideRef() {
        return portOfLoadLocOverrideRef;
    }

    /**
     * Define o valor da propriedade portOfLoadLocOverrideRef.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocOverrideRefType }
     *     
     */
    public void setPortOfLoadLocOverrideRef(GLogXMLLocOverrideRefType value) {
        this.portOfLoadLocOverrideRef = value;
    }

    /**
     * Obtém o valor da propriedade portOfDisLocationRef.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public GLogXMLLocRefType getPortOfDisLocationRef() {
        return portOfDisLocationRef;
    }

    /**
     * Define o valor da propriedade portOfDisLocationRef.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public void setPortOfDisLocationRef(GLogXMLLocRefType value) {
        this.portOfDisLocationRef = value;
    }

    /**
     * Obtém o valor da propriedade portOfDisLocOverrideRef.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocOverrideRefType }
     *     
     */
    public GLogXMLLocOverrideRefType getPortOfDisLocOverrideRef() {
        return portOfDisLocOverrideRef;
    }

    /**
     * Define o valor da propriedade portOfDisLocOverrideRef.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocOverrideRefType }
     *     
     */
    public void setPortOfDisLocOverrideRef(GLogXMLLocOverrideRefType value) {
        this.portOfDisLocOverrideRef = value;
    }

    /**
     * Obtém o valor da propriedade sourcePierLocationRef.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public GLogXMLLocRefType getSourcePierLocationRef() {
        return sourcePierLocationRef;
    }

    /**
     * Define o valor da propriedade sourcePierLocationRef.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public void setSourcePierLocationRef(GLogXMLLocRefType value) {
        this.sourcePierLocationRef = value;
    }

    /**
     * Obtém o valor da propriedade destPierLocationRef.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public GLogXMLLocRefType getDestPierLocationRef() {
        return destPierLocationRef;
    }

    /**
     * Define o valor da propriedade destPierLocationRef.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public void setDestPierLocationRef(GLogXMLLocRefType value) {
        this.destPierLocationRef = value;
    }

    /**
     * Obtém o valor da propriedade bookingInfo.
     * 
     * @return
     *     possible object is
     *     {@link BookingInfoType }
     *     
     */
    public BookingInfoType getBookingInfo() {
        return bookingInfo;
    }

    /**
     * Define o valor da propriedade bookingInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link BookingInfoType }
     *     
     */
    public void setBookingInfo(BookingInfoType value) {
        this.bookingInfo = value;
    }

    /**
     * Obtém o valor da propriedade delivServprovGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getDelivServprovGid() {
        return delivServprovGid;
    }

    /**
     * Define o valor da propriedade delivServprovGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setDelivServprovGid(GLogXMLGidType value) {
        this.delivServprovGid = value;
    }

    /**
     * Obtém o valor da propriedade origServprovGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getOrigServprovGid() {
        return origServprovGid;
    }

    /**
     * Define o valor da propriedade origServprovGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setOrigServprovGid(GLogXMLGidType value) {
        this.origServprovGid = value;
    }

    /**
     * Gets the value of the routeCode property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the routeCode property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRouteCode().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RouteCodeType }
     * 
     * 
     */
    public List<RouteCodeType> getRouteCode() {
        if (routeCode == null) {
            routeCode = new ArrayList<RouteCodeType>();
        }
        return this.routeCode;
    }

    /**
     * Obtém o valor da propriedade equipMarks.
     * 
     * @return
     *     possible object is
     *     {@link EquipMarksType }
     *     
     */
    public EquipMarksType getEquipMarks() {
        return equipMarks;
    }

    /**
     * Define o valor da propriedade equipMarks.
     * 
     * @param value
     *     allowed object is
     *     {@link EquipMarksType }
     *     
     */
    public void setEquipMarks(EquipMarksType value) {
        this.equipMarks = value;
    }

    /**
     * Gets the value of the text property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the text property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getText().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TextType }
     * 
     * 
     */
    public List<TextType> getText() {
        if (text == null) {
            text = new ArrayList<TextType>();
        }
        return this.text;
    }

    /**
     * Obtém o valor da propriedade driverGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getDriverGid() {
        return driverGid;
    }

    /**
     * Define o valor da propriedade driverGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setDriverGid(GLogXMLGidType value) {
        this.driverGid = value;
    }

    /**
     * Obtém o valor da propriedade secondaryDriverGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getSecondaryDriverGid() {
        return secondaryDriverGid;
    }

    /**
     * Define o valor da propriedade secondaryDriverGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setSecondaryDriverGid(GLogXMLGidType value) {
        this.secondaryDriverGid = value;
    }

    /**
     * Obtém o valor da propriedade powerUnitGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getPowerUnitGid() {
        return powerUnitGid;
    }

    /**
     * Define o valor da propriedade powerUnitGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setPowerUnitGid(GLogXMLGidType value) {
        this.powerUnitGid = value;
    }

    /**
     * Gets the value of the shipmentInfeasibility property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the shipmentInfeasibility property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getShipmentInfeasibility().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ShipmentInfeasibility }
     * 
     * 
     */
    public List<ShipmentInfeasibility> getShipmentInfeasibility() {
        if (shipmentInfeasibility == null) {
            shipmentInfeasibility = new ArrayList<ShipmentInfeasibility>();
        }
        return this.shipmentInfeasibility;
    }

    /**
     * Obtém o valor da propriedade planningParamSetGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getPlanningParamSetGid() {
        return planningParamSetGid;
    }

    /**
     * Define o valor da propriedade planningParamSetGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setPlanningParamSetGid(GLogXMLGidType value) {
        this.planningParamSetGid = value;
    }

    /**
     * Obtém o valor da propriedade emergPhoneNum.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmergPhoneNum() {
        return emergPhoneNum;
    }

    /**
     * Define o valor da propriedade emergPhoneNum.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmergPhoneNum(String value) {
        this.emergPhoneNum = value;
    }

    /**
     * Obtém o valor da propriedade isFixedVoyageFlag.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsFixedVoyageFlag() {
        return isFixedVoyageFlag;
    }

    /**
     * Define o valor da propriedade isFixedVoyageFlag.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsFixedVoyageFlag(String value) {
        this.isFixedVoyageFlag = value;
    }

    /**
     * Obtém o valor da propriedade flexFieldStrings.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldStringType }
     *     
     */
    public FlexFieldStringType getFlexFieldStrings() {
        return flexFieldStrings;
    }

    /**
     * Define o valor da propriedade flexFieldStrings.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldStringType }
     *     
     */
    public void setFlexFieldStrings(FlexFieldStringType value) {
        this.flexFieldStrings = value;
    }

    /**
     * Obtém o valor da propriedade flexFieldNumbers.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldNumberType }
     *     
     */
    public FlexFieldNumberType getFlexFieldNumbers() {
        return flexFieldNumbers;
    }

    /**
     * Define o valor da propriedade flexFieldNumbers.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldNumberType }
     *     
     */
    public void setFlexFieldNumbers(FlexFieldNumberType value) {
        this.flexFieldNumbers = value;
    }

    /**
     * Obtém o valor da propriedade flexFieldDates.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldDateType }
     *     
     */
    public FlexFieldDateType getFlexFieldDates() {
        return flexFieldDates;
    }

    /**
     * Define o valor da propriedade flexFieldDates.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldDateType }
     *     
     */
    public void setFlexFieldDates(FlexFieldDateType value) {
        this.flexFieldDates = value;
    }

    /**
     * Obtém o valor da propriedade flexFieldCurrencies.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldCurrencyType }
     *     
     */
    public FlexFieldCurrencyType getFlexFieldCurrencies() {
        return flexFieldCurrencies;
    }

    /**
     * Define o valor da propriedade flexFieldCurrencies.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldCurrencyType }
     *     
     */
    public void setFlexFieldCurrencies(FlexFieldCurrencyType value) {
        this.flexFieldCurrencies = value;
    }


    /**
     * <p>Classe Java de anonymous complex type.
     * 
     * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="FeasibilityCodeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "feasibilityCodeGid"
    })
    public static class ShipmentInfeasibility {

        @XmlElement(name = "FeasibilityCodeGid", required = true)
        protected GLogXMLGidType feasibilityCodeGid;

        /**
         * Obtém o valor da propriedade feasibilityCodeGid.
         * 
         * @return
         *     possible object is
         *     {@link GLogXMLGidType }
         *     
         */
        public GLogXMLGidType getFeasibilityCodeGid() {
            return feasibilityCodeGid;
        }

        /**
         * Define o valor da propriedade feasibilityCodeGid.
         * 
         * @param value
         *     allowed object is
         *     {@link GLogXMLGidType }
         *     
         */
        public void setFeasibilityCodeGid(GLogXMLGidType value) {
            this.feasibilityCodeGid = value;
        }

    }

}
