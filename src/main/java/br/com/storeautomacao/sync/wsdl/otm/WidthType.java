
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Width contains sub-elements for width unit of measture and value.
 * 
 * <p>Classe Java de WidthType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="WidthType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="WidthValue" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="WidthUOMGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WidthType", propOrder = {
    "widthValue",
    "widthUOMGid"
})
public class WidthType {

    @XmlElement(name = "WidthValue", required = true)
    protected String widthValue;
    @XmlElement(name = "WidthUOMGid", required = true)
    protected GLogXMLGidType widthUOMGid;

    /**
     * Obtém o valor da propriedade widthValue.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWidthValue() {
        return widthValue;
    }

    /**
     * Define o valor da propriedade widthValue.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWidthValue(String value) {
        this.widthValue = value;
    }

    /**
     * Obtém o valor da propriedade widthUOMGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getWidthUOMGid() {
        return widthUOMGid;
    }

    /**
     * Define o valor da propriedade widthUOMGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setWidthUOMGid(GLogXMLGidType value) {
        this.widthUOMGid = value;
    }

}
