
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Marks is a repeating structure containing MarksNumberQualifierGid and MarksNumber elements
 * 
 * <p>Classe Java de MarksType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="MarksType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="MarksNumberQualifier" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="MarksNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MarksType", propOrder = {
    "marksNumberQualifier",
    "marksNumber"
})
public class MarksType {

    @XmlElement(name = "MarksNumberQualifier", required = true)
    protected String marksNumberQualifier;
    @XmlElement(name = "MarksNumber", required = true)
    protected String marksNumber;

    /**
     * Obtém o valor da propriedade marksNumberQualifier.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMarksNumberQualifier() {
        return marksNumberQualifier;
    }

    /**
     * Define o valor da propriedade marksNumberQualifier.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMarksNumberQualifier(String value) {
        this.marksNumberQualifier = value;
    }

    /**
     * Obtém o valor da propriedade marksNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMarksNumber() {
        return marksNumber;
    }

    /**
     * Define o valor da propriedade marksNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMarksNumber(String value) {
        this.marksNumber = value;
    }

}
