
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 *             Specifies the content of the document.
 *          
 * 
 * <p>Classe Java de DocumentContentType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="DocumentContentType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DocMimeType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;choice minOccurs="0">
 *           &lt;element name="DocumentURL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *           &lt;element name="DocumentSecureURL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *           &lt;element name="DocContentText" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *           &lt;element name="DocContentBinary" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;/choice>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DocumentContentType", propOrder = {
    "docMimeType",
    "documentURL",
    "documentSecureURL",
    "docContentText",
    "docContentBinary"
})
public class DocumentContentType {

    @XmlElement(name = "DocMimeType")
    protected String docMimeType;
    @XmlElement(name = "DocumentURL")
    protected String documentURL;
    @XmlElement(name = "DocumentSecureURL")
    protected String documentSecureURL;
    @XmlElement(name = "DocContentText")
    protected String docContentText;
    @XmlElement(name = "DocContentBinary")
    protected String docContentBinary;

    /**
     * Obtém o valor da propriedade docMimeType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDocMimeType() {
        return docMimeType;
    }

    /**
     * Define o valor da propriedade docMimeType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDocMimeType(String value) {
        this.docMimeType = value;
    }

    /**
     * Obtém o valor da propriedade documentURL.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDocumentURL() {
        return documentURL;
    }

    /**
     * Define o valor da propriedade documentURL.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDocumentURL(String value) {
        this.documentURL = value;
    }

    /**
     * Obtém o valor da propriedade documentSecureURL.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDocumentSecureURL() {
        return documentSecureURL;
    }

    /**
     * Define o valor da propriedade documentSecureURL.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDocumentSecureURL(String value) {
        this.documentSecureURL = value;
    }

    /**
     * Obtém o valor da propriedade docContentText.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDocContentText() {
        return docContentText;
    }

    /**
     * Define o valor da propriedade docContentText.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDocContentText(String value) {
        this.docContentText = value;
    }

    /**
     * Obtém o valor da propriedade docContentBinary.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDocContentBinary() {
        return docContentBinary;
    }

    /**
     * Define o valor da propriedade docContentBinary.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDocContentBinary(String value) {
        this.docContentBinary = value;
    }

}
