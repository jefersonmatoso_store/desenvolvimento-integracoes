
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.*;


/**
 * <p>Classe Java de EquipTypeSpecialServiceType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="EquipTypeSpecialServiceType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;choice>
 *           &lt;element name="SpclService" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}SpclServiceType"/>
 *           &lt;element name="SpecialServiceGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;/choice>
 *         &lt;element name="TransactionCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransactionCodeType" minOccurs="0"/>
 *         &lt;element name="EffectiveDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="ExpirationDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EquipTypeSpecialServiceType", propOrder = {
    "spclService",
    "specialServiceGid",
    "transactionCode",
    "effectiveDate",
    "expirationDate"
})
public class EquipTypeSpecialServiceType {

    @XmlElement(name = "SpclService")
    protected SpclServiceType spclService;
    @XmlElement(name = "SpecialServiceGid")
    protected GLogXMLGidType specialServiceGid;
    @XmlElement(name = "TransactionCode")
    @XmlSchemaType(name = "string")
    protected TransactionCodeType transactionCode;
    @XmlElement(name = "EffectiveDate")
    protected GLogDateTimeType effectiveDate;
    @XmlElement(name = "ExpirationDate")
    protected GLogDateTimeType expirationDate;

    /**
     * Obtém o valor da propriedade spclService.
     * 
     * @return
     *     possible object is
     *     {@link SpclServiceType }
     *     
     */
    public SpclServiceType getSpclService() {
        return spclService;
    }

    /**
     * Define o valor da propriedade spclService.
     * 
     * @param value
     *     allowed object is
     *     {@link SpclServiceType }
     *     
     */
    public void setSpclService(SpclServiceType value) {
        this.spclService = value;
    }

    /**
     * Obtém o valor da propriedade specialServiceGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getSpecialServiceGid() {
        return specialServiceGid;
    }

    /**
     * Define o valor da propriedade specialServiceGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setSpecialServiceGid(GLogXMLGidType value) {
        this.specialServiceGid = value;
    }

    /**
     * Obtém o valor da propriedade transactionCode.
     * 
     * @return
     *     possible object is
     *     {@link TransactionCodeType }
     *     
     */
    public TransactionCodeType getTransactionCode() {
        return transactionCode;
    }

    /**
     * Define o valor da propriedade transactionCode.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionCodeType }
     *     
     */
    public void setTransactionCode(TransactionCodeType value) {
        this.transactionCode = value;
    }

    /**
     * Obtém o valor da propriedade effectiveDate.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getEffectiveDate() {
        return effectiveDate;
    }

    /**
     * Define o valor da propriedade effectiveDate.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setEffectiveDate(GLogDateTimeType value) {
        this.effectiveDate = value;
    }

    /**
     * Obtém o valor da propriedade expirationDate.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getExpirationDate() {
        return expirationDate;
    }

    /**
     * Define o valor da propriedade expirationDate.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setExpirationDate(GLogDateTimeType value) {
        this.expirationDate = value;
    }

}
