
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * (Outbound) LicenseDeterminationResponse is response for the license screening triggered via ServiceRequest integration interface.
 *          
 * 
 * <p>Classe Java de LicenseDeterminationResponseType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="LicenseDeterminationResponseType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="LicenseGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="LicenseNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="LicenseLineGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="ControlTypeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="ControlCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ExpirationDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType"/>
 *         &lt;element name="QuantityTypeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="BalanceQuantityStr" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CurrencyTypeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="BalanceValueStr" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LicenseDeterminationResponseType", namespace = "http://xmlns.oracle.com/apps/gtm/transmission/v6.4", propOrder = {
    "licenseGid",
    "licenseNumber",
    "licenseLineGid",
    "controlTypeGid",
    "controlCode",
    "expirationDate",
    "quantityTypeGid",
    "balanceQuantityStr",
    "currencyTypeGid",
    "balanceValueStr"
})
public class LicenseDeterminationResponseType {

    @XmlElement(name = "LicenseGid", required = true)
    protected GLogXMLGidType licenseGid;
    @XmlElement(name = "LicenseNumber", required = true)
    protected String licenseNumber;
    @XmlElement(name = "LicenseLineGid", required = true)
    protected GLogXMLGidType licenseLineGid;
    @XmlElement(name = "ControlTypeGid", required = true)
    protected GLogXMLGidType controlTypeGid;
    @XmlElement(name = "ControlCode", required = true)
    protected String controlCode;
    @XmlElement(name = "ExpirationDate", required = true)
    protected GLogDateTimeType expirationDate;
    @XmlElement(name = "QuantityTypeGid", required = true)
    protected GLogXMLGidType quantityTypeGid;
    @XmlElement(name = "BalanceQuantityStr", required = true)
    protected String balanceQuantityStr;
    @XmlElement(name = "CurrencyTypeGid", required = true)
    protected GLogXMLGidType currencyTypeGid;
    @XmlElement(name = "BalanceValueStr", required = true)
    protected String balanceValueStr;

    /**
     * Obtém o valor da propriedade licenseGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getLicenseGid() {
        return licenseGid;
    }

    /**
     * Define o valor da propriedade licenseGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setLicenseGid(GLogXMLGidType value) {
        this.licenseGid = value;
    }

    /**
     * Obtém o valor da propriedade licenseNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLicenseNumber() {
        return licenseNumber;
    }

    /**
     * Define o valor da propriedade licenseNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLicenseNumber(String value) {
        this.licenseNumber = value;
    }

    /**
     * Obtém o valor da propriedade licenseLineGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getLicenseLineGid() {
        return licenseLineGid;
    }

    /**
     * Define o valor da propriedade licenseLineGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setLicenseLineGid(GLogXMLGidType value) {
        this.licenseLineGid = value;
    }

    /**
     * Obtém o valor da propriedade controlTypeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getControlTypeGid() {
        return controlTypeGid;
    }

    /**
     * Define o valor da propriedade controlTypeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setControlTypeGid(GLogXMLGidType value) {
        this.controlTypeGid = value;
    }

    /**
     * Obtém o valor da propriedade controlCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getControlCode() {
        return controlCode;
    }

    /**
     * Define o valor da propriedade controlCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setControlCode(String value) {
        this.controlCode = value;
    }

    /**
     * Obtém o valor da propriedade expirationDate.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getExpirationDate() {
        return expirationDate;
    }

    /**
     * Define o valor da propriedade expirationDate.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setExpirationDate(GLogDateTimeType value) {
        this.expirationDate = value;
    }

    /**
     * Obtém o valor da propriedade quantityTypeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getQuantityTypeGid() {
        return quantityTypeGid;
    }

    /**
     * Define o valor da propriedade quantityTypeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setQuantityTypeGid(GLogXMLGidType value) {
        this.quantityTypeGid = value;
    }

    /**
     * Obtém o valor da propriedade balanceQuantityStr.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBalanceQuantityStr() {
        return balanceQuantityStr;
    }

    /**
     * Define o valor da propriedade balanceQuantityStr.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBalanceQuantityStr(String value) {
        this.balanceQuantityStr = value;
    }

    /**
     * Obtém o valor da propriedade currencyTypeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getCurrencyTypeGid() {
        return currencyTypeGid;
    }

    /**
     * Define o valor da propriedade currencyTypeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setCurrencyTypeGid(GLogXMLGidType value) {
        this.currencyTypeGid = value;
    }

    /**
     * Obtém o valor da propriedade balanceValueStr.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBalanceValueStr() {
        return balanceValueStr;
    }

    /**
     * Define o valor da propriedade balanceValueStr.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBalanceValueStr(String value) {
        this.balanceValueStr = value;
    }

}
