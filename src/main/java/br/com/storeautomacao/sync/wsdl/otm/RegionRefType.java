
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * RegionRef is a reference to a region - either a region gid or a region definition.
 *             It may be used in the context of a Location to specify a region for the location.
 *             It may also be used in the context of an XLaneNode to reference an existing region, or define a new region on-the-fly.
 *          
 * 
 * <p>Classe Java de RegionRefType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="RegionRefType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element name="RegionGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="RegionHeader">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="RegionGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *                   &lt;element name="RegionDesc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="RegionComponent" maxOccurs="unbounded">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="MileageAddress" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}MileageAddressType"/>
 *                             &lt;element name="PostalCodeRange" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}PostalCodeRangeType" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RegionRefType", propOrder = {
    "regionGid",
    "regionHeader"
})
public class RegionRefType {

    @XmlElement(name = "RegionGid")
    protected GLogXMLGidType regionGid;
    @XmlElement(name = "RegionHeader")
    protected RegionHeader regionHeader;

    /**
     * Obtém o valor da propriedade regionGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRegionGid() {
        return regionGid;
    }

    /**
     * Define o valor da propriedade regionGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRegionGid(GLogXMLGidType value) {
        this.regionGid = value;
    }

    /**
     * Obtém o valor da propriedade regionHeader.
     * 
     * @return
     *     possible object is
     *     {@link RegionHeader }
     *     
     */
    public RegionHeader getRegionHeader() {
        return regionHeader;
    }

    /**
     * Define o valor da propriedade regionHeader.
     * 
     * @param value
     *     allowed object is
     *     {@link RegionHeader }
     *     
     */
    public void setRegionHeader(RegionHeader value) {
        this.regionHeader = value;
    }


    /**
     * <p>Classe Java de anonymous complex type.
     * 
     * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="RegionGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
     *         &lt;element name="RegionDesc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="RegionComponent" maxOccurs="unbounded">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="MileageAddress" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}MileageAddressType"/>
     *                   &lt;element name="PostalCodeRange" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}PostalCodeRangeType" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "regionGid",
        "regionDesc",
        "regionComponent"
    })
    public static class RegionHeader {

        @XmlElement(name = "RegionGid", required = true)
        protected GLogXMLGidType regionGid;
        @XmlElement(name = "RegionDesc")
        protected String regionDesc;
        @XmlElement(name = "RegionComponent", required = true)
        protected List<RegionComponent> regionComponent;

        /**
         * Obtém o valor da propriedade regionGid.
         * 
         * @return
         *     possible object is
         *     {@link GLogXMLGidType }
         *     
         */
        public GLogXMLGidType getRegionGid() {
            return regionGid;
        }

        /**
         * Define o valor da propriedade regionGid.
         * 
         * @param value
         *     allowed object is
         *     {@link GLogXMLGidType }
         *     
         */
        public void setRegionGid(GLogXMLGidType value) {
            this.regionGid = value;
        }

        /**
         * Obtém o valor da propriedade regionDesc.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRegionDesc() {
            return regionDesc;
        }

        /**
         * Define o valor da propriedade regionDesc.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRegionDesc(String value) {
            this.regionDesc = value;
        }

        /**
         * Gets the value of the regionComponent property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the regionComponent property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getRegionComponent().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link RegionComponent }
         * 
         * 
         */
        public List<RegionComponent> getRegionComponent() {
            if (regionComponent == null) {
                regionComponent = new ArrayList<RegionComponent>();
            }
            return this.regionComponent;
        }


        /**
         * <p>Classe Java de anonymous complex type.
         * 
         * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="MileageAddress" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}MileageAddressType"/>
         *         &lt;element name="PostalCodeRange" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}PostalCodeRangeType" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "mileageAddress",
            "postalCodeRange"
        })
        public static class RegionComponent {

            @XmlElement(name = "MileageAddress", required = true)
            protected MileageAddressType mileageAddress;
            @XmlElement(name = "PostalCodeRange")
            protected PostalCodeRangeType postalCodeRange;

            /**
             * Obtém o valor da propriedade mileageAddress.
             * 
             * @return
             *     possible object is
             *     {@link MileageAddressType }
             *     
             */
            public MileageAddressType getMileageAddress() {
                return mileageAddress;
            }

            /**
             * Define o valor da propriedade mileageAddress.
             * 
             * @param value
             *     allowed object is
             *     {@link MileageAddressType }
             *     
             */
            public void setMileageAddress(MileageAddressType value) {
                this.mileageAddress = value;
            }

            /**
             * Obtém o valor da propriedade postalCodeRange.
             * 
             * @return
             *     possible object is
             *     {@link PostalCodeRangeType }
             *     
             */
            public PostalCodeRangeType getPostalCodeRange() {
                return postalCodeRange;
            }

            /**
             * Define o valor da propriedade postalCodeRange.
             * 
             * @param value
             *     allowed object is
             *     {@link PostalCodeRangeType }
             *     
             */
            public void setPostalCodeRange(PostalCodeRangeType value) {
                this.postalCodeRange = value;
            }

        }

    }

}
