
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 *             Specifies the currency conversion between two currencies.
 *          
 * 
 * <p>Classe Java de CurrencyExchangeRateType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="CurrencyExchangeRateType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CurrencyFrom" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}CurrencyCodeType"/>
 *         &lt;element name="CurrencyTo" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}CurrencyCodeType"/>
 *         &lt;element name="TriangulationCurrency" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}CurrencyCodeType" minOccurs="0"/>
 *         &lt;element name="MaximumPrecision" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FractionalDigits" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ExchangeRateValue" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CurrencyExchangeRateType", propOrder = {
    "currencyFrom",
    "currencyTo",
    "triangulationCurrency",
    "maximumPrecision",
    "fractionalDigits",
    "exchangeRateValue"
})
public class CurrencyExchangeRateType {

    @XmlElement(name = "CurrencyFrom", required = true)
    protected CurrencyCodeType currencyFrom;
    @XmlElement(name = "CurrencyTo", required = true)
    protected CurrencyCodeType currencyTo;
    @XmlElement(name = "TriangulationCurrency")
    protected CurrencyCodeType triangulationCurrency;
    @XmlElement(name = "MaximumPrecision")
    protected String maximumPrecision;
    @XmlElement(name = "FractionalDigits")
    protected String fractionalDigits;
    @XmlElement(name = "ExchangeRateValue", required = true)
    protected String exchangeRateValue;

    /**
     * Obtém o valor da propriedade currencyFrom.
     * 
     * @return
     *     possible object is
     *     {@link CurrencyCodeType }
     *     
     */
    public CurrencyCodeType getCurrencyFrom() {
        return currencyFrom;
    }

    /**
     * Define o valor da propriedade currencyFrom.
     * 
     * @param value
     *     allowed object is
     *     {@link CurrencyCodeType }
     *     
     */
    public void setCurrencyFrom(CurrencyCodeType value) {
        this.currencyFrom = value;
    }

    /**
     * Obtém o valor da propriedade currencyTo.
     * 
     * @return
     *     possible object is
     *     {@link CurrencyCodeType }
     *     
     */
    public CurrencyCodeType getCurrencyTo() {
        return currencyTo;
    }

    /**
     * Define o valor da propriedade currencyTo.
     * 
     * @param value
     *     allowed object is
     *     {@link CurrencyCodeType }
     *     
     */
    public void setCurrencyTo(CurrencyCodeType value) {
        this.currencyTo = value;
    }

    /**
     * Obtém o valor da propriedade triangulationCurrency.
     * 
     * @return
     *     possible object is
     *     {@link CurrencyCodeType }
     *     
     */
    public CurrencyCodeType getTriangulationCurrency() {
        return triangulationCurrency;
    }

    /**
     * Define o valor da propriedade triangulationCurrency.
     * 
     * @param value
     *     allowed object is
     *     {@link CurrencyCodeType }
     *     
     */
    public void setTriangulationCurrency(CurrencyCodeType value) {
        this.triangulationCurrency = value;
    }

    /**
     * Obtém o valor da propriedade maximumPrecision.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMaximumPrecision() {
        return maximumPrecision;
    }

    /**
     * Define o valor da propriedade maximumPrecision.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMaximumPrecision(String value) {
        this.maximumPrecision = value;
    }

    /**
     * Obtém o valor da propriedade fractionalDigits.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFractionalDigits() {
        return fractionalDigits;
    }

    /**
     * Define o valor da propriedade fractionalDigits.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFractionalDigits(String value) {
        this.fractionalDigits = value;
    }

    /**
     * Obtém o valor da propriedade exchangeRateValue.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExchangeRateValue() {
        return exchangeRateValue;
    }

    /**
     * Define o valor da propriedade exchangeRateValue.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExchangeRateValue(String value) {
        this.exchangeRateValue = value;
    }

}
