
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Common type for tables which contain Flex Field columns for CURRENCY data type
 * 
 * <p>Classe Java de FlexFieldCurrencyType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="FlexFieldCurrencyType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AttributeCurrency1" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/>
 *         &lt;element name="AttributeCurrency2" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/>
 *         &lt;element name="AttributeCurrency3" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FlexFieldCurrencyType", propOrder = {
    "attributeCurrency1",
    "attributeCurrency2",
    "attributeCurrency3"
})
public class FlexFieldCurrencyType {

    @XmlElement(name = "AttributeCurrency1")
    protected GLogXMLFinancialAmountType attributeCurrency1;
    @XmlElement(name = "AttributeCurrency2")
    protected GLogXMLFinancialAmountType attributeCurrency2;
    @XmlElement(name = "AttributeCurrency3")
    protected GLogXMLFinancialAmountType attributeCurrency3;

    /**
     * Obtém o valor da propriedade attributeCurrency1.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getAttributeCurrency1() {
        return attributeCurrency1;
    }

    /**
     * Define o valor da propriedade attributeCurrency1.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setAttributeCurrency1(GLogXMLFinancialAmountType value) {
        this.attributeCurrency1 = value;
    }

    /**
     * Obtém o valor da propriedade attributeCurrency2.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getAttributeCurrency2() {
        return attributeCurrency2;
    }

    /**
     * Define o valor da propriedade attributeCurrency2.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setAttributeCurrency2(GLogXMLFinancialAmountType value) {
        this.attributeCurrency2 = value;
    }

    /**
     * Obtém o valor da propriedade attributeCurrency3.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getAttributeCurrency3() {
        return attributeCurrency3;
    }

    /**
     * Define o valor da propriedade attributeCurrency3.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setAttributeCurrency3(GLogXMLFinancialAmountType value) {
        this.attributeCurrency3 = value;
    }

}
