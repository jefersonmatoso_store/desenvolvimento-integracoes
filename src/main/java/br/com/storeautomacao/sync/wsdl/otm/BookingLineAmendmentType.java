
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * 
 *             Will contain either the BookLineAmendViaRelease or the BookLineAmendViaServiceProvider but not both.
 *             The selection is based on the agent used to send the notification for the message.
 *          
 * 
 * <p>Classe Java de BookingLineAmendmentType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="BookingLineAmendmentType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OTMTransactionOut">
 *       &lt;sequence>
 *         &lt;element name="BookLineAmendViaRelease" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}BookLineAmendViaReleaseType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="BookLineAmendViaServiceProvider" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}BookLineAmendViaServiceProviderType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="CharterVoyage" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}CharterVoyageType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Release" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ReleaseType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BookingLineAmendmentType", propOrder = {
    "bookLineAmendViaRelease",
    "bookLineAmendViaServiceProvider",
    "charterVoyage",
    "release"
})
public class BookingLineAmendmentType
    extends OTMTransactionOut
{

    @XmlElement(name = "BookLineAmendViaRelease")
    protected List<BookLineAmendViaReleaseType> bookLineAmendViaRelease;
    @XmlElement(name = "BookLineAmendViaServiceProvider")
    protected List<BookLineAmendViaServiceProviderType> bookLineAmendViaServiceProvider;
    @XmlElement(name = "CharterVoyage")
    protected List<CharterVoyageType> charterVoyage;
    @XmlElement(name = "Release")
    protected List<ReleaseType> release;

    /**
     * Gets the value of the bookLineAmendViaRelease property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the bookLineAmendViaRelease property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBookLineAmendViaRelease().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BookLineAmendViaReleaseType }
     * 
     * 
     */
    public List<BookLineAmendViaReleaseType> getBookLineAmendViaRelease() {
        if (bookLineAmendViaRelease == null) {
            bookLineAmendViaRelease = new ArrayList<BookLineAmendViaReleaseType>();
        }
        return this.bookLineAmendViaRelease;
    }

    /**
     * Gets the value of the bookLineAmendViaServiceProvider property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the bookLineAmendViaServiceProvider property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBookLineAmendViaServiceProvider().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BookLineAmendViaServiceProviderType }
     * 
     * 
     */
    public List<BookLineAmendViaServiceProviderType> getBookLineAmendViaServiceProvider() {
        if (bookLineAmendViaServiceProvider == null) {
            bookLineAmendViaServiceProvider = new ArrayList<BookLineAmendViaServiceProviderType>();
        }
        return this.bookLineAmendViaServiceProvider;
    }

    /**
     * Gets the value of the charterVoyage property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the charterVoyage property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCharterVoyage().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CharterVoyageType }
     * 
     * 
     */
    public List<CharterVoyageType> getCharterVoyage() {
        if (charterVoyage == null) {
            charterVoyage = new ArrayList<CharterVoyageType>();
        }
        return this.charterVoyage;
    }

    /**
     * Gets the value of the release property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the release property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRelease().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ReleaseType }
     * 
     * 
     */
    public List<ReleaseType> getRelease() {
        if (release == null) {
            release = new ArrayList<ReleaseType>();
        }
        return this.release;
    }

}
