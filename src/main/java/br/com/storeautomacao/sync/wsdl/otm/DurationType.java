
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Duration is the amount of time spent doing an activity, such as a stop.
 *             Includes a duration value and unit of measure.
 *             Example: To specify 30 seconds you would specify DurationValue=30 and DurationUOMGid=S.
 *          
 * 
 * <p>Classe Java de DurationType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="DurationType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DurationValue" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="DurationUOMGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DurationType", propOrder = {
    "durationValue",
    "durationUOMGid"
})
public class DurationType {

    @XmlElement(name = "DurationValue", required = true)
    protected String durationValue;
    @XmlElement(name = "DurationUOMGid", required = true)
    protected GLogXMLGidType durationUOMGid;

    /**
     * Obtém o valor da propriedade durationValue.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDurationValue() {
        return durationValue;
    }

    /**
     * Define o valor da propriedade durationValue.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDurationValue(String value) {
        this.durationValue = value;
    }

    /**
     * Obtém o valor da propriedade durationUOMGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getDurationUOMGid() {
        return durationUOMGid;
    }

    /**
     * Define o valor da propriedade durationUOMGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setDurationUOMGid(GLogXMLGidType value) {
        this.durationUOMGid = value;
    }

}
