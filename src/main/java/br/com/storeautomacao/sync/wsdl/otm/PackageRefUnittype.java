
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Used to specify Packaging Reference Unit information.
 * 
 * <p>Classe Java de PackageRefUnittype complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="PackageRefUnittype">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PackageRefUnitGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="NumReferenceUnits" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="TotalNumReferenceUnits" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PackageRefUnittype", propOrder = {
    "packageRefUnitGid",
    "numReferenceUnits",
    "totalNumReferenceUnits"
})
public class PackageRefUnittype {

    @XmlElement(name = "PackageRefUnitGid", required = true)
    protected GLogXMLGidType packageRefUnitGid;
    @XmlElement(name = "NumReferenceUnits", required = true)
    protected String numReferenceUnits;
    @XmlElement(name = "TotalNumReferenceUnits", required = true)
    protected String totalNumReferenceUnits;

    /**
     * Obtém o valor da propriedade packageRefUnitGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getPackageRefUnitGid() {
        return packageRefUnitGid;
    }

    /**
     * Define o valor da propriedade packageRefUnitGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setPackageRefUnitGid(GLogXMLGidType value) {
        this.packageRefUnitGid = value;
    }

    /**
     * Obtém o valor da propriedade numReferenceUnits.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumReferenceUnits() {
        return numReferenceUnits;
    }

    /**
     * Define o valor da propriedade numReferenceUnits.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumReferenceUnits(String value) {
        this.numReferenceUnits = value;
    }

    /**
     * Obtém o valor da propriedade totalNumReferenceUnits.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTotalNumReferenceUnits() {
        return totalNumReferenceUnits;
    }

    /**
     * Define o valor da propriedade totalNumReferenceUnits.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTotalNumReferenceUnits(String value) {
        this.totalNumReferenceUnits = value;
    }

}
