
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * Equipment type. Characterizes the equipment.
 * 
 * <p>Classe Java de EquipmentDescriptionType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="EquipmentDescriptionType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="EquipmentTypeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="EquipTypeName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EquipDesc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EquipDescCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EquipDescCodeDef" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ISOEquipCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ISOEquipCodeDef" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MaxWeight" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLWeightType" minOccurs="0"/>
 *         &lt;element name="TareWeight" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLWeightType" minOccurs="0"/>
 *         &lt;element name="NominalWeight" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLWeightType" minOccurs="0"/>
 *         &lt;element name="InteriorVolume" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLVolumeType" minOccurs="0"/>
 *         &lt;element name="Metric1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Metric2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InteriorLWH" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LengthWidthHeightType" minOccurs="0"/>
 *         &lt;element name="ExteriorLWH" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LengthWidthHeightType" minOccurs="0"/>
 *         &lt;element name="IsTemperatureControl" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TemperatureControlGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="TagInfo" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TagInfoType" minOccurs="0"/>
 *         &lt;element name="Remark" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RemarkType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="EquipTypeSpecialService" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}EquipTypeSpecialServiceType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EquipmentDescriptionType", propOrder = {
    "equipmentTypeGid",
    "equipTypeName",
    "equipDesc",
    "equipDescCode",
    "equipDescCodeDef",
    "isoEquipCode",
    "isoEquipCodeDef",
    "maxWeight",
    "tareWeight",
    "nominalWeight",
    "interiorVolume",
    "metric1",
    "metric2",
    "interiorLWH",
    "exteriorLWH",
    "isTemperatureControl",
    "temperatureControlGid",
    "tagInfo",
    "remark",
    "equipTypeSpecialService"
})
public class EquipmentDescriptionType {

    @XmlElement(name = "EquipmentTypeGid", required = true)
    protected GLogXMLGidType equipmentTypeGid;
    @XmlElement(name = "EquipTypeName")
    protected String equipTypeName;
    @XmlElement(name = "EquipDesc")
    protected String equipDesc;
    @XmlElement(name = "EquipDescCode")
    protected String equipDescCode;
    @XmlElement(name = "EquipDescCodeDef")
    protected String equipDescCodeDef;
    @XmlElement(name = "ISOEquipCode")
    protected String isoEquipCode;
    @XmlElement(name = "ISOEquipCodeDef")
    protected String isoEquipCodeDef;
    @XmlElement(name = "MaxWeight")
    protected GLogXMLWeightType maxWeight;
    @XmlElement(name = "TareWeight")
    protected GLogXMLWeightType tareWeight;
    @XmlElement(name = "NominalWeight")
    protected GLogXMLWeightType nominalWeight;
    @XmlElement(name = "InteriorVolume")
    protected GLogXMLVolumeType interiorVolume;
    @XmlElement(name = "Metric1")
    protected String metric1;
    @XmlElement(name = "Metric2")
    protected String metric2;
    @XmlElement(name = "InteriorLWH")
    protected LengthWidthHeightType interiorLWH;
    @XmlElement(name = "ExteriorLWH")
    protected LengthWidthHeightType exteriorLWH;
    @XmlElement(name = "IsTemperatureControl")
    protected String isTemperatureControl;
    @XmlElement(name = "TemperatureControlGid")
    protected GLogXMLGidType temperatureControlGid;
    @XmlElement(name = "TagInfo")
    protected TagInfoType tagInfo;
    @XmlElement(name = "Remark")
    protected List<RemarkType> remark;
    @XmlElement(name = "EquipTypeSpecialService")
    protected List<EquipTypeSpecialServiceType> equipTypeSpecialService;

    /**
     * Obtém o valor da propriedade equipmentTypeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getEquipmentTypeGid() {
        return equipmentTypeGid;
    }

    /**
     * Define o valor da propriedade equipmentTypeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setEquipmentTypeGid(GLogXMLGidType value) {
        this.equipmentTypeGid = value;
    }

    /**
     * Obtém o valor da propriedade equipTypeName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEquipTypeName() {
        return equipTypeName;
    }

    /**
     * Define o valor da propriedade equipTypeName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEquipTypeName(String value) {
        this.equipTypeName = value;
    }

    /**
     * Obtém o valor da propriedade equipDesc.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEquipDesc() {
        return equipDesc;
    }

    /**
     * Define o valor da propriedade equipDesc.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEquipDesc(String value) {
        this.equipDesc = value;
    }

    /**
     * Obtém o valor da propriedade equipDescCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEquipDescCode() {
        return equipDescCode;
    }

    /**
     * Define o valor da propriedade equipDescCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEquipDescCode(String value) {
        this.equipDescCode = value;
    }

    /**
     * Obtém o valor da propriedade equipDescCodeDef.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEquipDescCodeDef() {
        return equipDescCodeDef;
    }

    /**
     * Define o valor da propriedade equipDescCodeDef.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEquipDescCodeDef(String value) {
        this.equipDescCodeDef = value;
    }

    /**
     * Obtém o valor da propriedade isoEquipCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getISOEquipCode() {
        return isoEquipCode;
    }

    /**
     * Define o valor da propriedade isoEquipCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setISOEquipCode(String value) {
        this.isoEquipCode = value;
    }

    /**
     * Obtém o valor da propriedade isoEquipCodeDef.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getISOEquipCodeDef() {
        return isoEquipCodeDef;
    }

    /**
     * Define o valor da propriedade isoEquipCodeDef.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setISOEquipCodeDef(String value) {
        this.isoEquipCodeDef = value;
    }

    /**
     * Obtém o valor da propriedade maxWeight.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLWeightType }
     *     
     */
    public GLogXMLWeightType getMaxWeight() {
        return maxWeight;
    }

    /**
     * Define o valor da propriedade maxWeight.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLWeightType }
     *     
     */
    public void setMaxWeight(GLogXMLWeightType value) {
        this.maxWeight = value;
    }

    /**
     * Obtém o valor da propriedade tareWeight.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLWeightType }
     *     
     */
    public GLogXMLWeightType getTareWeight() {
        return tareWeight;
    }

    /**
     * Define o valor da propriedade tareWeight.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLWeightType }
     *     
     */
    public void setTareWeight(GLogXMLWeightType value) {
        this.tareWeight = value;
    }

    /**
     * Obtém o valor da propriedade nominalWeight.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLWeightType }
     *     
     */
    public GLogXMLWeightType getNominalWeight() {
        return nominalWeight;
    }

    /**
     * Define o valor da propriedade nominalWeight.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLWeightType }
     *     
     */
    public void setNominalWeight(GLogXMLWeightType value) {
        this.nominalWeight = value;
    }

    /**
     * Obtém o valor da propriedade interiorVolume.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLVolumeType }
     *     
     */
    public GLogXMLVolumeType getInteriorVolume() {
        return interiorVolume;
    }

    /**
     * Define o valor da propriedade interiorVolume.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLVolumeType }
     *     
     */
    public void setInteriorVolume(GLogXMLVolumeType value) {
        this.interiorVolume = value;
    }

    /**
     * Obtém o valor da propriedade metric1.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMetric1() {
        return metric1;
    }

    /**
     * Define o valor da propriedade metric1.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMetric1(String value) {
        this.metric1 = value;
    }

    /**
     * Obtém o valor da propriedade metric2.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMetric2() {
        return metric2;
    }

    /**
     * Define o valor da propriedade metric2.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMetric2(String value) {
        this.metric2 = value;
    }

    /**
     * Obtém o valor da propriedade interiorLWH.
     * 
     * @return
     *     possible object is
     *     {@link LengthWidthHeightType }
     *     
     */
    public LengthWidthHeightType getInteriorLWH() {
        return interiorLWH;
    }

    /**
     * Define o valor da propriedade interiorLWH.
     * 
     * @param value
     *     allowed object is
     *     {@link LengthWidthHeightType }
     *     
     */
    public void setInteriorLWH(LengthWidthHeightType value) {
        this.interiorLWH = value;
    }

    /**
     * Obtém o valor da propriedade exteriorLWH.
     * 
     * @return
     *     possible object is
     *     {@link LengthWidthHeightType }
     *     
     */
    public LengthWidthHeightType getExteriorLWH() {
        return exteriorLWH;
    }

    /**
     * Define o valor da propriedade exteriorLWH.
     * 
     * @param value
     *     allowed object is
     *     {@link LengthWidthHeightType }
     *     
     */
    public void setExteriorLWH(LengthWidthHeightType value) {
        this.exteriorLWH = value;
    }

    /**
     * Obtém o valor da propriedade isTemperatureControl.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsTemperatureControl() {
        return isTemperatureControl;
    }

    /**
     * Define o valor da propriedade isTemperatureControl.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsTemperatureControl(String value) {
        this.isTemperatureControl = value;
    }

    /**
     * Obtém o valor da propriedade temperatureControlGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getTemperatureControlGid() {
        return temperatureControlGid;
    }

    /**
     * Define o valor da propriedade temperatureControlGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setTemperatureControlGid(GLogXMLGidType value) {
        this.temperatureControlGid = value;
    }

    /**
     * Obtém o valor da propriedade tagInfo.
     * 
     * @return
     *     possible object is
     *     {@link TagInfoType }
     *     
     */
    public TagInfoType getTagInfo() {
        return tagInfo;
    }

    /**
     * Define o valor da propriedade tagInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link TagInfoType }
     *     
     */
    public void setTagInfo(TagInfoType value) {
        this.tagInfo = value;
    }

    /**
     * Gets the value of the remark property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the remark property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRemark().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RemarkType }
     * 
     * 
     */
    public List<RemarkType> getRemark() {
        if (remark == null) {
            remark = new ArrayList<RemarkType>();
        }
        return this.remark;
    }

    /**
     * Gets the value of the equipTypeSpecialService property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the equipTypeSpecialService property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEquipTypeSpecialService().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EquipTypeSpecialServiceType }
     * 
     * 
     */
    public List<EquipTypeSpecialServiceType> getEquipTypeSpecialService() {
        if (equipTypeSpecialService == null) {
            equipTypeSpecialService = new ArrayList<EquipTypeSpecialServiceType>();
        }
        return this.equipTypeSpecialService;
    }

}
