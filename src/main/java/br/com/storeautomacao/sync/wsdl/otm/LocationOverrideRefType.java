
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.*;


/**
 * Location override reference.
 * 
 * <p>Classe Java de LocationOverrideRefType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="LocationOverrideRefType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element name="LocationOverrideGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="LocationOverride">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="LocationOverrideGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *                   &lt;element name="TransactionCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransactionCodeType"/>
 *                   &lt;element name="LocationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *                   &lt;element name="LocationOverrideInfo" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LocationOverrideInfoType" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LocationOverrideRefType", propOrder = {
    "locationOverrideGid",
    "locationOverride"
})
public class LocationOverrideRefType {

    @XmlElement(name = "LocationOverrideGid")
    protected GLogXMLGidType locationOverrideGid;
    @XmlElement(name = "LocationOverride")
    protected LocationOverride locationOverride;

    /**
     * Obtém o valor da propriedade locationOverrideGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getLocationOverrideGid() {
        return locationOverrideGid;
    }

    /**
     * Define o valor da propriedade locationOverrideGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setLocationOverrideGid(GLogXMLGidType value) {
        this.locationOverrideGid = value;
    }

    /**
     * Obtém o valor da propriedade locationOverride.
     * 
     * @return
     *     possible object is
     *     {@link LocationOverride }
     *     
     */
    public LocationOverride getLocationOverride() {
        return locationOverride;
    }

    /**
     * Define o valor da propriedade locationOverride.
     * 
     * @param value
     *     allowed object is
     *     {@link LocationOverride }
     *     
     */
    public void setLocationOverride(LocationOverride value) {
        this.locationOverride = value;
    }


    /**
     * <p>Classe Java de anonymous complex type.
     * 
     * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="LocationOverrideGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
     *         &lt;element name="TransactionCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransactionCodeType"/>
     *         &lt;element name="LocationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
     *         &lt;element name="LocationOverrideInfo" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LocationOverrideInfoType" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "locationOverrideGid",
        "transactionCode",
        "locationGid",
        "locationOverrideInfo"
    })
    public static class LocationOverride {

        @XmlElement(name = "LocationOverrideGid", required = true)
        protected GLogXMLGidType locationOverrideGid;
        @XmlElement(name = "TransactionCode", required = true)
        @XmlSchemaType(name = "string")
        protected TransactionCodeType transactionCode;
        @XmlElement(name = "LocationGid", required = true)
        protected GLogXMLGidType locationGid;
        @XmlElement(name = "LocationOverrideInfo")
        protected LocationOverrideInfoType locationOverrideInfo;

        /**
         * Obtém o valor da propriedade locationOverrideGid.
         * 
         * @return
         *     possible object is
         *     {@link GLogXMLGidType }
         *     
         */
        public GLogXMLGidType getLocationOverrideGid() {
            return locationOverrideGid;
        }

        /**
         * Define o valor da propriedade locationOverrideGid.
         * 
         * @param value
         *     allowed object is
         *     {@link GLogXMLGidType }
         *     
         */
        public void setLocationOverrideGid(GLogXMLGidType value) {
            this.locationOverrideGid = value;
        }

        /**
         * Obtém o valor da propriedade transactionCode.
         * 
         * @return
         *     possible object is
         *     {@link TransactionCodeType }
         *     
         */
        public TransactionCodeType getTransactionCode() {
            return transactionCode;
        }

        /**
         * Define o valor da propriedade transactionCode.
         * 
         * @param value
         *     allowed object is
         *     {@link TransactionCodeType }
         *     
         */
        public void setTransactionCode(TransactionCodeType value) {
            this.transactionCode = value;
        }

        /**
         * Obtém o valor da propriedade locationGid.
         * 
         * @return
         *     possible object is
         *     {@link GLogXMLGidType }
         *     
         */
        public GLogXMLGidType getLocationGid() {
            return locationGid;
        }

        /**
         * Define o valor da propriedade locationGid.
         * 
         * @param value
         *     allowed object is
         *     {@link GLogXMLGidType }
         *     
         */
        public void setLocationGid(GLogXMLGidType value) {
            this.locationGid = value;
        }

        /**
         * Obtém o valor da propriedade locationOverrideInfo.
         * 
         * @return
         *     possible object is
         *     {@link LocationOverrideInfoType }
         *     
         */
        public LocationOverrideInfoType getLocationOverrideInfo() {
            return locationOverrideInfo;
        }

        /**
         * Define o valor da propriedade locationOverrideInfo.
         * 
         * @param value
         *     allowed object is
         *     {@link LocationOverrideInfoType }
         *     
         */
        public void setLocationOverrideInfo(LocationOverrideInfoType value) {
            this.locationOverrideInfo = value;
        }

    }

}
