
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * All secondary charge shipments of RIQ routing option.
 * 
 * <p>Classe Java de RIQSecondaryChargesType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="RIQSecondaryChargesType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RIQRouteShipment" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RIQRouteShipmentType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RIQSecondaryChargesType", propOrder = {
    "riqRouteShipment"
})
public class RIQSecondaryChargesType {

    @XmlElement(name = "RIQRouteShipment")
    protected List<RIQRouteShipmentType> riqRouteShipment;

    /**
     * Gets the value of the riqRouteShipment property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the riqRouteShipment property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRIQRouteShipment().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RIQRouteShipmentType }
     * 
     * 
     */
    public List<RIQRouteShipmentType> getRIQRouteShipment() {
        if (riqRouteShipment == null) {
            riqRouteShipment = new ArrayList<RIQRouteShipmentType>();
        }
        return this.riqRouteShipment;
    }

}
