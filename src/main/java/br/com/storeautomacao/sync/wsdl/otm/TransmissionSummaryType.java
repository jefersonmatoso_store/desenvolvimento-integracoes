
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * TransmissionSummary is the summary component of a TransmissionReport.
 * 
 * <p>Classe Java de TransmissionSummaryType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="TransmissionSummaryType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TransmissionNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FirstTransactionNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LastTransactionNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ApplicationReport" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="ApplicationName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="InsertCount" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="UpdateCount" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="InsertUpdateCount" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="DeleteCount" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="ErrorCount" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TransmissionSummaryType", propOrder = {
    "transmissionNo",
    "firstTransactionNo",
    "lastTransactionNo",
    "applicationReport"
})
public class TransmissionSummaryType {

    @XmlElement(name = "TransmissionNo")
    protected String transmissionNo;
    @XmlElement(name = "FirstTransactionNo")
    protected String firstTransactionNo;
    @XmlElement(name = "LastTransactionNo")
    protected String lastTransactionNo;
    @XmlElement(name = "ApplicationReport")
    protected List<ApplicationReport> applicationReport;

    /**
     * Obtém o valor da propriedade transmissionNo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransmissionNo() {
        return transmissionNo;
    }

    /**
     * Define o valor da propriedade transmissionNo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransmissionNo(String value) {
        this.transmissionNo = value;
    }

    /**
     * Obtém o valor da propriedade firstTransactionNo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFirstTransactionNo() {
        return firstTransactionNo;
    }

    /**
     * Define o valor da propriedade firstTransactionNo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFirstTransactionNo(String value) {
        this.firstTransactionNo = value;
    }

    /**
     * Obtém o valor da propriedade lastTransactionNo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLastTransactionNo() {
        return lastTransactionNo;
    }

    /**
     * Define o valor da propriedade lastTransactionNo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLastTransactionNo(String value) {
        this.lastTransactionNo = value;
    }

    /**
     * Gets the value of the applicationReport property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the applicationReport property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getApplicationReport().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ApplicationReport }
     * 
     * 
     */
    public List<ApplicationReport> getApplicationReport() {
        if (applicationReport == null) {
            applicationReport = new ArrayList<ApplicationReport>();
        }
        return this.applicationReport;
    }


    /**
     * <p>Classe Java de anonymous complex type.
     * 
     * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="ApplicationName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="InsertCount" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="UpdateCount" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="InsertUpdateCount" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="DeleteCount" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="ErrorCount" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "applicationName",
        "insertCount",
        "updateCount",
        "insertUpdateCount",
        "deleteCount",
        "errorCount"
    })
    public static class ApplicationReport {

        @XmlElement(name = "ApplicationName", required = true)
        protected String applicationName;
        @XmlElement(name = "InsertCount", required = true)
        protected String insertCount;
        @XmlElement(name = "UpdateCount", required = true)
        protected String updateCount;
        @XmlElement(name = "InsertUpdateCount", required = true)
        protected String insertUpdateCount;
        @XmlElement(name = "DeleteCount", required = true)
        protected String deleteCount;
        @XmlElement(name = "ErrorCount", required = true)
        protected String errorCount;

        /**
         * Obtém o valor da propriedade applicationName.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getApplicationName() {
            return applicationName;
        }

        /**
         * Define o valor da propriedade applicationName.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setApplicationName(String value) {
            this.applicationName = value;
        }

        /**
         * Obtém o valor da propriedade insertCount.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getInsertCount() {
            return insertCount;
        }

        /**
         * Define o valor da propriedade insertCount.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setInsertCount(String value) {
            this.insertCount = value;
        }

        /**
         * Obtém o valor da propriedade updateCount.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getUpdateCount() {
            return updateCount;
        }

        /**
         * Define o valor da propriedade updateCount.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setUpdateCount(String value) {
            this.updateCount = value;
        }

        /**
         * Obtém o valor da propriedade insertUpdateCount.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getInsertUpdateCount() {
            return insertUpdateCount;
        }

        /**
         * Define o valor da propriedade insertUpdateCount.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setInsertUpdateCount(String value) {
            this.insertUpdateCount = value;
        }

        /**
         * Obtém o valor da propriedade deleteCount.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDeleteCount() {
            return deleteCount;
        }

        /**
         * Define o valor da propriedade deleteCount.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDeleteCount(String value) {
            this.deleteCount = value;
        }

        /**
         * Obtém o valor da propriedade errorCount.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getErrorCount() {
            return errorCount;
        }

        /**
         * Define o valor da propriedade errorCount.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setErrorCount(String value) {
            this.errorCount = value;
        }

    }

}
