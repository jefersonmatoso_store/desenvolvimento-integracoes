
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the br.com.storeautomacao.sync.wsdl package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ServiceResponse_QNAME = new QName("http://xmlns.oracle.com/apps/gtm/transmission/v6.4", "ServiceResponse");
    private final static QName _ItemMaster_QNAME = new QName("http://xmlns.oracle.com/apps/otm/transmission/v6.4", "ItemMaster");
    private final static QName _RATEGEO_QNAME = new QName("http://xmlns.oracle.com/apps/otm/transmission/v6.4", "RATE_GEO");
    private final static QName _TransOrder_QNAME = new QName("http://xmlns.oracle.com/apps/otm/transmission/v6.4", "TransOrder");
    private final static QName _BulkTrailerBuild_QNAME = new QName("http://xmlns.oracle.com/apps/otm/transmission/v6.4", "BulkTrailerBuild");
    private final static QName _FleetBulkPlan_QNAME = new QName("http://xmlns.oracle.com/apps/otm/transmission/v6.4", "FleetBulkPlan");
    private final static QName _DriverCalendarEvent_QNAME = new QName("http://xmlns.oracle.com/apps/otm/transmission/v6.4", "DriverCalendarEvent");
    private final static QName _TransmissionReport_QNAME = new QName("http://xmlns.oracle.com/apps/otm/transmission/v6.4", "TransmissionReport");
    private final static QName _GtmDeclaration_QNAME = new QName("http://xmlns.oracle.com/apps/gtm/transmission/v6.4", "GtmDeclaration");
    private final static QName _RouteTemplate_QNAME = new QName("http://xmlns.oracle.com/apps/otm/transmission/v6.4", "RouteTemplate");
    private final static QName _GtmDeclarationMessage_QNAME = new QName("http://xmlns.oracle.com/apps/gtm/transmission/v6.4", "GtmDeclarationMessage");
    private final static QName _RATEOFFERING_QNAME = new QName("http://xmlns.oracle.com/apps/otm/transmission/v6.4", "RATE_OFFERING");
    private final static QName _Device_QNAME = new QName("http://xmlns.oracle.com/apps/otm/transmission/v6.4", "Device");
    private final static QName _Mileage_QNAME = new QName("http://xmlns.oracle.com/apps/otm/transmission/v6.4", "Mileage");
    private final static QName _Voyage_QNAME = new QName("http://xmlns.oracle.com/apps/otm/transmission/v6.4", "Voyage");
    private final static QName _DemurrageTransaction_QNAME = new QName("http://xmlns.oracle.com/apps/otm/transmission/v6.4", "DemurrageTransaction");
    private final static QName _ActualShipment_QNAME = new QName("http://xmlns.oracle.com/apps/otm/transmission/v6.4", "ActualShipment");
    private final static QName _ShipmentGroupTenderOffer_QNAME = new QName("http://xmlns.oracle.com/apps/otm/transmission/v6.4", "ShipmentGroupTenderOffer");
    private final static QName _CharterVoyage_QNAME = new QName("http://xmlns.oracle.com/apps/otm/transmission/v6.4", "CharterVoyage");
    private final static QName _BulkContMove_QNAME = new QName("http://xmlns.oracle.com/apps/otm/transmission/v6.4", "BulkContMove");
    private final static QName _ReleaseInstruction_QNAME = new QName("http://xmlns.oracle.com/apps/otm/transmission/v6.4", "ReleaseInstruction");
    private final static QName _Item_QNAME = new QName("http://xmlns.oracle.com/apps/otm/transmission/v6.4", "Item");
    private final static QName _CSVDataLoad_QNAME = new QName("http://xmlns.oracle.com/apps/otm/transmission/v6.4", "CSVDataLoad");
    private final static QName _Itinerary_QNAME = new QName("http://xmlns.oracle.com/apps/otm/transmission/v6.4", "Itinerary");
    private final static QName _XLane_QNAME = new QName("http://xmlns.oracle.com/apps/otm/transmission/v6.4", "XLane");
    private final static QName _Job_QNAME = new QName("http://xmlns.oracle.com/apps/otm/transmission/v6.4", "Job");
    private final static QName _OrderMovement_QNAME = new QName("http://xmlns.oracle.com/apps/otm/transmission/v6.4", "OrderMovement");
    private final static QName _Accrual_QNAME = new QName("http://xmlns.oracle.com/apps/otm/transmission/v6.4", "Accrual");
    private final static QName _Invoice_QNAME = new QName("http://xmlns.oracle.com/apps/otm/transmission/v6.4", "Invoice");
    private final static QName _TenderOffer_QNAME = new QName("http://xmlns.oracle.com/apps/otm/transmission/v6.4", "TenderOffer");
    private final static QName _OBShipUnit_QNAME = new QName("http://xmlns.oracle.com/apps/otm/transmission/v6.4", "OBShipUnit");
    private final static QName _BulkRating_QNAME = new QName("http://xmlns.oracle.com/apps/otm/transmission/v6.4", "BulkRating");
    private final static QName _HazmatItem_QNAME = new QName("http://xmlns.oracle.com/apps/otm/transmission/v6.4", "HazmatItem");
    private final static QName _DataQuerySummary_QNAME = new QName("http://xmlns.oracle.com/apps/otm/transmission/v6.4", "DataQuerySummary");
    private final static QName _WorkInvoice_QNAME = new QName("http://xmlns.oracle.com/apps/otm/transmission/v6.4", "WorkInvoice");
    private final static QName _TransOrderLink_QNAME = new QName("http://xmlns.oracle.com/apps/otm/transmission/v6.4", "TransOrderLink");
    private final static QName _GtmStructure_QNAME = new QName("http://xmlns.oracle.com/apps/gtm/transmission/v6.4", "GtmStructure");
    private final static QName _Document_QNAME = new QName("http://xmlns.oracle.com/apps/otm/transmission/v6.4", "Document");
    private final static QName _OBLine_QNAME = new QName("http://xmlns.oracle.com/apps/otm/transmission/v6.4", "OBLine");
    private final static QName _TenderResponse_QNAME = new QName("http://xmlns.oracle.com/apps/otm/transmission/v6.4", "TenderResponse");
    private final static QName _BookingLineAmendment_QNAME = new QName("http://xmlns.oracle.com/apps/otm/transmission/v6.4", "BookingLineAmendment");
    private final static QName _TransOrderStatus_QNAME = new QName("http://xmlns.oracle.com/apps/otm/transmission/v6.4", "TransOrderStatus");
    private final static QName _Quote_QNAME = new QName("http://xmlns.oracle.com/apps/otm/transmission/v6.4", "Quote");
    private final static QName _Corporation_QNAME = new QName("http://xmlns.oracle.com/apps/otm/transmission/v6.4", "Corporation");
    private final static QName _ShipStop_QNAME = new QName("http://xmlns.oracle.com/apps/otm/transmission/v6.4", "ShipStop");
    private final static QName _Billing_QNAME = new QName("http://xmlns.oracle.com/apps/otm/transmission/v6.4", "Billing");
    private final static QName _PlannedShipment_QNAME = new QName("http://xmlns.oracle.com/apps/otm/transmission/v6.4", "PlannedShipment");
    private final static QName _Equipment_QNAME = new QName("http://xmlns.oracle.com/apps/otm/transmission/v6.4", "Equipment");
    private final static QName _ServiceTime_QNAME = new QName("http://xmlns.oracle.com/apps/otm/transmission/v6.4", "ServiceTime");
    private final static QName _SkuEvent_QNAME = new QName("http://xmlns.oracle.com/apps/otm/transmission/v6.4", "SkuEvent");
    private final static QName _Claim_QNAME = new QName("http://xmlns.oracle.com/apps/otm/transmission/v6.4", "Claim");
    private final static QName _Location_QNAME = new QName("http://xmlns.oracle.com/apps/otm/transmission/v6.4", "Location");
    private final static QName _GLogXMLTransaction_QNAME = new QName("http://xmlns.oracle.com/apps/otm/transmission/v6.4", "GLogXMLTransaction");
    private final static QName _TransmissionHeader_QNAME = new QName("http://xmlns.oracle.com/apps/otm/transmission/v6.4", "TransmissionHeader");
    private final static QName _ContactGroup_QNAME = new QName("http://xmlns.oracle.com/apps/otm/transmission/v6.4", "ContactGroup");
    private final static QName _Sku_QNAME = new QName("http://xmlns.oracle.com/apps/otm/transmission/v6.4", "Sku");
    private final static QName _BulkPlan_QNAME = new QName("http://xmlns.oracle.com/apps/otm/transmission/v6.4", "BulkPlan");
    private final static QName _AllocationBase_QNAME = new QName("http://xmlns.oracle.com/apps/otm/transmission/v6.4", "AllocationBase");
    private final static QName _SShipUnit_QNAME = new QName("http://xmlns.oracle.com/apps/otm/transmission/v6.4", "SShipUnit");
    private final static QName _ShipmentGroup_QNAME = new QName("http://xmlns.oracle.com/apps/otm/transmission/v6.4", "ShipmentGroup");
    private final static QName _TransactionAck_QNAME = new QName("http://xmlns.oracle.com/apps/otm/transmission/v6.4", "TransactionAck");
    private final static QName _Contact_QNAME = new QName("http://xmlns.oracle.com/apps/otm/transmission/v6.4", "Contact");
    private final static QName _Voucher_QNAME = new QName("http://xmlns.oracle.com/apps/otm/transmission/v6.4", "Voucher");
    private final static QName _GtmBond_QNAME = new QName("http://xmlns.oracle.com/apps/gtm/transmission/v6.4", "GtmBond");
    private final static QName _GtmTransactionLine_QNAME = new QName("http://xmlns.oracle.com/apps/gtm/transmission/v6.4", "GtmTransactionLine");
    private final static QName _GtmContact_QNAME = new QName("http://xmlns.oracle.com/apps/gtm/transmission/v6.4", "GtmContact");
    private final static QName _ActivityTimeDef_QNAME = new QName("http://xmlns.oracle.com/apps/otm/transmission/v6.4", "ActivityTimeDef");
    private final static QName _GtmTransaction_QNAME = new QName("http://xmlns.oracle.com/apps/gtm/transmission/v6.4", "GtmTransaction");
    private final static QName _ShipmentLink_QNAME = new QName("http://xmlns.oracle.com/apps/otm/transmission/v6.4", "ShipmentLink");
    private final static QName _FinancialSystemFeed_QNAME = new QName("http://xmlns.oracle.com/apps/otm/transmission/v6.4", "FinancialSystemFeed");
    private final static QName _ShipmentStatus_QNAME = new QName("http://xmlns.oracle.com/apps/otm/transmission/v6.4", "ShipmentStatus");
    private final static QName _PowerUnit_QNAME = new QName("http://xmlns.oracle.com/apps/otm/transmission/v6.4", "PowerUnit");
    private final static QName _RemoteQuery_QNAME = new QName("http://xmlns.oracle.com/apps/otm/transmission/v6.4", "RemoteQuery");
    private final static QName _RemoteQueryReply_QNAME = new QName("http://xmlns.oracle.com/apps/otm/transmission/v6.4", "RemoteQueryReply");
    private final static QName _Release_QNAME = new QName("http://xmlns.oracle.com/apps/otm/transmission/v6.4", "Release");
    private final static QName _Topic_QNAME = new QName("http://xmlns.oracle.com/apps/otm/transmission/v6.4", "Topic");
    private final static QName _ExchangeRate_QNAME = new QName("http://xmlns.oracle.com/apps/otm/transmission/v6.4", "ExchangeRate");
    private final static QName _User_QNAME = new QName("http://xmlns.oracle.com/apps/otm/transmission/v6.4", "User");
    private final static QName _SkuTransaction_QNAME = new QName("http://xmlns.oracle.com/apps/otm/transmission/v6.4", "SkuTransaction");
    private final static QName _Consol_QNAME = new QName("http://xmlns.oracle.com/apps/otm/transmission/v6.4", "Consol");
    private final static QName _HazmatGeneric_QNAME = new QName("http://xmlns.oracle.com/apps/otm/transmission/v6.4", "HazmatGeneric");
    private final static QName _GtmRegistration_QNAME = new QName("http://xmlns.oracle.com/apps/gtm/transmission/v6.4", "GtmRegistration");
    private final static QName _Driver_QNAME = new QName("http://xmlns.oracle.com/apps/otm/transmission/v6.4", "Driver");
    private final static QName _GenericStatusUpdate_QNAME = new QName("http://xmlns.oracle.com/apps/otm/transmission/v6.4", "GenericStatusUpdate");
    private final static QName _ServiceRequest_QNAME = new QName("http://xmlns.oracle.com/apps/gtm/transmission/v6.4", "ServiceRequest");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: br.com.storeautomacao.sync.wsdl
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Transmission }
     * 
     */
    public Transmission createTransmission() {
        return new Transmission();
    }

    /**
     * Create an instance of {@link TransmissionAck }
     * 
     */
    public TransmissionAck createTransmissionAck() {
        return new TransmissionAck();
    }

    /**
     * Create an instance of {@link XLANE }
     * 
     */
    public XLANE createXLANE() {
        return new XLANE();
    }

    /**
     * Create an instance of {@link RIQQueryInfoType }
     * 
     */
    public RIQQueryInfoType createRIQQueryInfoType() {
        return new RIQQueryInfoType();
    }

    /**
     * Create an instance of {@link DriverHeaderType }
     * 
     */
    public DriverHeaderType createDriverHeaderType() {
        return new DriverHeaderType();
    }

    /**
     * Create an instance of {@link QuoteCostOptionType }
     * 
     */
    public QuoteCostOptionType createQuoteCostOptionType() {
        return new QuoteCostOptionType();
    }

    /**
     * Create an instance of {@link TransmissionSummaryType }
     * 
     */
    public TransmissionSummaryType createTransmissionSummaryType() {
        return new TransmissionSummaryType();
    }

    /**
     * Create an instance of {@link RIQQueryType }
     * 
     */
    public RIQQueryType createRIQQueryType() {
        return new RIQQueryType();
    }

    /**
     * Create an instance of {@link RATEGEOCOSTWEIGHTBREAKTYPE }
     * 
     */
    public RATEGEOCOSTWEIGHTBREAKTYPE createRATEGEOCOSTWEIGHTBREAKTYPE() {
        return new RATEGEOCOSTWEIGHTBREAKTYPE();
    }

    /**
     * Create an instance of {@link ServiceProviderType }
     * 
     */
    public ServiceProviderType createServiceProviderType() {
        return new ServiceProviderType();
    }

    /**
     * Create an instance of {@link TransactionHeaderType }
     * 
     */
    public TransactionHeaderType createTransactionHeaderType() {
        return new TransactionHeaderType();
    }

    /**
     * Create an instance of {@link IntSavedQueryType }
     * 
     */
    public IntSavedQueryType createIntSavedQueryType() {
        return new IntSavedQueryType();
    }

    /**
     * Create an instance of {@link RATEGEOCOSTTYPE }
     * 
     */
    public RATEGEOCOSTTYPE createRATEGEOCOSTTYPE() {
        return new RATEGEOCOSTTYPE();
    }

    /**
     * Create an instance of {@link LocationOverrideRefType }
     * 
     */
    public LocationOverrideRefType createLocationOverrideRefType() {
        return new LocationOverrideRefType();
    }

    /**
     * Create an instance of {@link RegionRefType }
     * 
     */
    public RegionRefType createRegionRefType() {
        return new RegionRefType();
    }

    /**
     * Create an instance of {@link RegionRefType.RegionHeader }
     * 
     */
    public RegionRefType.RegionHeader createRegionRefTypeRegionHeader() {
        return new RegionRefType.RegionHeader();
    }

    /**
     * Create an instance of {@link RouteTemplateLegType }
     * 
     */
    public RouteTemplateLegType createRouteTemplateLegType() {
        return new RouteTemplateLegType();
    }

    /**
     * Create an instance of {@link RouteTemplateLegType.RouteTempLegLocIncompat }
     * 
     */
    public RouteTemplateLegType.RouteTempLegLocIncompat createRouteTemplateLegTypeRouteTempLegLocIncompat() {
        return new RouteTemplateLegType.RouteTempLegLocIncompat();
    }

    /**
     * Create an instance of {@link ACCESSORIALCOSTTYPE }
     * 
     */
    public ACCESSORIALCOSTTYPE createACCESSORIALCOSTTYPE() {
        return new ACCESSORIALCOSTTYPE();
    }

    /**
     * Create an instance of {@link ACCESSORIALCOSTTYPE.ACCESSORIALCOSTROW }
     * 
     */
    public ACCESSORIALCOSTTYPE.ACCESSORIALCOSTROW createACCESSORIALCOSTTYPEACCESSORIALCOSTROW() {
        return new ACCESSORIALCOSTTYPE.ACCESSORIALCOSTROW();
    }

    /**
     * Create an instance of {@link ACCESSORIALCOSTTYPE.ACCESSORIALCOSTROW.ACCESSORIALCOSTUNITBREAK }
     * 
     */
    public ACCESSORIALCOSTTYPE.ACCESSORIALCOSTROW.ACCESSORIALCOSTUNITBREAK createACCESSORIALCOSTTYPEACCESSORIALCOSTROWACCESSORIALCOSTUNITBREAK() {
        return new ACCESSORIALCOSTTYPE.ACCESSORIALCOSTROW.ACCESSORIALCOSTUNITBREAK();
    }

    /**
     * Create an instance of {@link CostOptionShipType }
     * 
     */
    public CostOptionShipType createCostOptionShipType() {
        return new CostOptionShipType();
    }

    /**
     * Create an instance of {@link LocationServProvPrefType }
     * 
     */
    public LocationServProvPrefType createLocationServProvPrefType() {
        return new LocationServProvPrefType();
    }

    /**
     * Create an instance of {@link LegType }
     * 
     */
    public LegType createLegType() {
        return new LegType();
    }

    /**
     * Create an instance of {@link ShipUnitType }
     * 
     */
    public ShipUnitType createShipUnitType() {
        return new ShipUnitType();
    }

    /**
     * Create an instance of {@link RATEGEOCOSTUNITBREAKTYPE }
     * 
     */
    public RATEGEOCOSTUNITBREAKTYPE createRATEGEOCOSTUNITBREAKTYPE() {
        return new RATEGEOCOSTUNITBREAKTYPE();
    }

    /**
     * Create an instance of {@link IntCommandType }
     * 
     */
    public IntCommandType createIntCommandType() {
        return new IntCommandType();
    }

    /**
     * Create an instance of {@link ReleaseLineType }
     * 
     */
    public ReleaseLineType createReleaseLineType() {
        return new ReleaseLineType();
    }

    /**
     * Create an instance of {@link ShipmentHeaderType }
     * 
     */
    public ShipmentHeaderType createShipmentHeaderType() {
        return new ShipmentHeaderType();
    }

    /**
     * Create an instance of {@link LocationType }
     * 
     */
    public LocationType createLocationType() {
        return new LocationType();
    }

    /**
     * Create an instance of {@link ContactGroupType }
     * 
     */
    public ContactGroupType createContactGroupType() {
        return new ContactGroupType();
    }

    /**
     * Create an instance of {@link TransOrderStatusType }
     * 
     */
    public TransOrderStatusType createTransOrderStatusType() {
        return new TransOrderStatusType();
    }

    /**
     * Create an instance of {@link ContactType }
     * 
     */
    public ContactType createContactType() {
        return new ContactType();
    }

    /**
     * Create an instance of {@link TransactionAckType }
     * 
     */
    public TransactionAckType createTransactionAckType() {
        return new TransactionAckType();
    }

    /**
     * Create an instance of {@link AllocationBaseType }
     * 
     */
    public AllocationBaseType createAllocationBaseType() {
        return new AllocationBaseType();
    }

    /**
     * Create an instance of {@link ReleaseType }
     * 
     */
    public ReleaseType createReleaseType() {
        return new ReleaseType();
    }

    /**
     * Create an instance of {@link TopicType }
     * 
     */
    public TopicType createTopicType() {
        return new TopicType();
    }

    /**
     * Create an instance of {@link ShipmentLinkType }
     * 
     */
    public ShipmentLinkType createShipmentLinkType() {
        return new ShipmentLinkType();
    }

    /**
     * Create an instance of {@link RateGeoType }
     * 
     */
    public RateGeoType createRateGeoType() {
        return new RateGeoType();
    }

    /**
     * Create an instance of {@link RateGeoType.RATEGEOROW }
     * 
     */
    public RateGeoType.RATEGEOROW createRateGeoTypeRATEGEOROW() {
        return new RateGeoType.RATEGEOROW();
    }

    /**
     * Create an instance of {@link RateGeoType.RATEGEOROW.RATEGEOREMARKS }
     * 
     */
    public RateGeoType.RATEGEOROW.RATEGEOREMARKS createRateGeoTypeRATEGEOROWRATEGEOREMARKS() {
        return new RateGeoType.RATEGEOROW.RATEGEOREMARKS();
    }

    /**
     * Create an instance of {@link RateGeoType.RATEGEOROW.RGSPECIALSERVICEACCESSORIAL }
     * 
     */
    public RateGeoType.RATEGEOROW.RGSPECIALSERVICEACCESSORIAL createRateGeoTypeRATEGEOROWRGSPECIALSERVICEACCESSORIAL() {
        return new RateGeoType.RATEGEOROW.RGSPECIALSERVICEACCESSORIAL();
    }

    /**
     * Create an instance of {@link RateGeoType.RATEGEOROW.RATEGEOCOSTGROUP }
     * 
     */
    public RateGeoType.RATEGEOROW.RATEGEOCOSTGROUP createRateGeoTypeRATEGEOROWRATEGEOCOSTGROUP() {
        return new RateGeoType.RATEGEOROW.RATEGEOCOSTGROUP();
    }

    /**
     * Create an instance of {@link RateGeoType.RATEGEOROW.RATEGEOACCESSORIAL }
     * 
     */
    public RateGeoType.RATEGEOROW.RATEGEOACCESSORIAL createRateGeoTypeRATEGEOROWRATEGEOACCESSORIAL() {
        return new RateGeoType.RATEGEOROW.RATEGEOACCESSORIAL();
    }

    /**
     * Create an instance of {@link RateGeoType.RATEGEOROW.RATEGEOSTOPS }
     * 
     */
    public RateGeoType.RATEGEOROW.RATEGEOSTOPS createRateGeoTypeRATEGEOROWRATEGEOSTOPS() {
        return new RateGeoType.RATEGEOROW.RATEGEOSTOPS();
    }

    /**
     * Create an instance of {@link RateOfferingType }
     * 
     */
    public RateOfferingType createRateOfferingType() {
        return new RateOfferingType();
    }

    /**
     * Create an instance of {@link RateOfferingType.RATEOFFERINGROW }
     * 
     */
    public RateOfferingType.RATEOFFERINGROW createRateOfferingTypeRATEOFFERINGROW() {
        return new RateOfferingType.RATEOFFERINGROW();
    }

    /**
     * Create an instance of {@link RateOfferingType.RATEOFFERINGROW.RATEOFFERINGINVPARTY }
     * 
     */
    public RateOfferingType.RATEOFFERINGROW.RATEOFFERINGINVPARTY createRateOfferingTypeRATEOFFERINGROWRATEOFFERINGINVPARTY() {
        return new RateOfferingType.RATEOFFERINGROW.RATEOFFERINGINVPARTY();
    }

    /**
     * Create an instance of {@link RateOfferingType.RATEOFFERINGROW.ROSPECIALSERVICEACCESSORIAL }
     * 
     */
    public RateOfferingType.RATEOFFERINGROW.ROSPECIALSERVICEACCESSORIAL createRateOfferingTypeRATEOFFERINGROWROSPECIALSERVICEACCESSORIAL() {
        return new RateOfferingType.RATEOFFERINGROW.ROSPECIALSERVICEACCESSORIAL();
    }

    /**
     * Create an instance of {@link RateOfferingType.RATEOFFERINGROW.RATERULESANDTERMS }
     * 
     */
    public RateOfferingType.RATEOFFERINGROW.RATERULESANDTERMS createRateOfferingTypeRATEOFFERINGROWRATERULESANDTERMS() {
        return new RateOfferingType.RATEOFFERINGROW.RATERULESANDTERMS();
    }

    /**
     * Create an instance of {@link RateOfferingType.RATEOFFERINGROW.RATEOFFERINGCOMMENT }
     * 
     */
    public RateOfferingType.RATEOFFERINGROW.RATEOFFERINGCOMMENT createRateOfferingTypeRATEOFFERINGROWRATEOFFERINGCOMMENT() {
        return new RateOfferingType.RATEOFFERINGROW.RATEOFFERINGCOMMENT();
    }

    /**
     * Create an instance of {@link RateOfferingType.RATEOFFERINGROW.RATEOFFERINGACCESSORIAL }
     * 
     */
    public RateOfferingType.RATEOFFERINGROW.RATEOFFERINGACCESSORIAL createRateOfferingTypeRATEOFFERINGROWRATEOFFERINGACCESSORIAL() {
        return new RateOfferingType.RATEOFFERINGROW.RATEOFFERINGACCESSORIAL();
    }

    /**
     * Create an instance of {@link RateOfferingType.RATEOFFERINGROW.RATEOFFERINGSTOPS }
     * 
     */
    public RateOfferingType.RATEOFFERINGROW.RATEOFFERINGSTOPS createRateOfferingTypeRATEOFFERINGROWRATEOFFERINGSTOPS() {
        return new RateOfferingType.RATEOFFERINGROW.RATEOFFERINGSTOPS();
    }

    /**
     * Create an instance of {@link JobType }
     * 
     */
    public JobType createJobType() {
        return new JobType();
    }

    /**
     * Create an instance of {@link ReleaseInstructionType }
     * 
     */
    public ReleaseInstructionType createReleaseInstructionType() {
        return new ReleaseInstructionType();
    }

    /**
     * Create an instance of {@link WorkInvoiceType }
     * 
     */
    public WorkInvoiceType createWorkInvoiceType() {
        return new WorkInvoiceType();
    }

    /**
     * Create an instance of {@link DataQuerySummaryType }
     * 
     */
    public DataQuerySummaryType createDataQuerySummaryType() {
        return new DataQuerySummaryType();
    }

    /**
     * Create an instance of {@link HazmatItemType }
     * 
     */
    public HazmatItemType createHazmatItemType() {
        return new HazmatItemType();
    }

    /**
     * Create an instance of {@link Publish }
     * 
     */
    public Publish createPublish() {
        return new Publish();
    }

    /**
     * Create an instance of {@link TransmissionHeaderType }
     * 
     */
    public TransmissionHeaderType createTransmissionHeaderType() {
        return new TransmissionHeaderType();
    }

    /**
     * Create an instance of {@link Transmission.TransmissionBody }
     * 
     */
    public Transmission.TransmissionBody createTransmissionTransmissionBody() {
        return new Transmission.TransmissionBody();
    }

    /**
     * Create an instance of {@link ExecuteResponse }
     * 
     */
    public ExecuteResponse createExecuteResponse() {
        return new ExecuteResponse();
    }

    /**
     * Create an instance of {@link Execute }
     * 
     */
    public Execute createExecute() {
        return new Execute();
    }

    /**
     * Create an instance of {@link PublishResponse }
     * 
     */
    public PublishResponse createPublishResponse() {
        return new PublishResponse();
    }

    /**
     * Create an instance of {@link TransmissionAck.EchoedTransmissionHeader }
     * 
     */
    public TransmissionAck.EchoedTransmissionHeader createTransmissionAckEchoedTransmissionHeader() {
        return new TransmissionAck.EchoedTransmissionHeader();
    }

    /**
     * Create an instance of {@link RefnumType }
     * 
     */
    public RefnumType createRefnumType() {
        return new RefnumType();
    }

    /**
     * Create an instance of {@link TransmissionAck.QueryResultInfo }
     * 
     */
    public TransmissionAck.QueryResultInfo createTransmissionAckQueryResultInfo() {
        return new TransmissionAck.QueryResultInfo();
    }

    /**
     * Create an instance of {@link TenderOfferType }
     * 
     */
    public TenderOfferType createTenderOfferType() {
        return new TenderOfferType();
    }

    /**
     * Create an instance of {@link OrderMovementType }
     * 
     */
    public OrderMovementType createOrderMovementType() {
        return new OrderMovementType();
    }

    /**
     * Create an instance of {@link AccrualType }
     * 
     */
    public AccrualType createAccrualType() {
        return new AccrualType();
    }

    /**
     * Create an instance of {@link InvoiceType }
     * 
     */
    public InvoiceType createInvoiceType() {
        return new InvoiceType();
    }

    /**
     * Create an instance of {@link OBShipUnitType }
     * 
     */
    public OBShipUnitType createOBShipUnitType() {
        return new OBShipUnitType();
    }

    /**
     * Create an instance of {@link BulkRatingType }
     * 
     */
    public BulkRatingType createBulkRatingType() {
        return new BulkRatingType();
    }

    /**
     * Create an instance of {@link TransOrderLinkType }
     * 
     */
    public TransOrderLinkType createTransOrderLinkType() {
        return new TransOrderLinkType();
    }

    /**
     * Create an instance of {@link DocumentType }
     * 
     */
    public DocumentType createDocumentType() {
        return new DocumentType();
    }

    /**
     * Create an instance of {@link ItemType }
     * 
     */
    public ItemType createItemType() {
        return new ItemType();
    }

    /**
     * Create an instance of {@link BulkContMoveType }
     * 
     */
    public BulkContMoveType createBulkContMoveType() {
        return new BulkContMoveType();
    }

    /**
     * Create an instance of {@link CSVDataLoadType }
     * 
     */
    public CSVDataLoadType createCSVDataLoadType() {
        return new CSVDataLoadType();
    }

    /**
     * Create an instance of {@link ItineraryType }
     * 
     */
    public ItineraryType createItineraryType() {
        return new ItineraryType();
    }

    /**
     * Create an instance of {@link XLaneType }
     * 
     */
    public XLaneType createXLaneType() {
        return new XLaneType();
    }

    /**
     * Create an instance of {@link RouteTemplateType }
     * 
     */
    public RouteTemplateType createRouteTemplateType() {
        return new RouteTemplateType();
    }

    /**
     * Create an instance of {@link DeviceType }
     * 
     */
    public DeviceType createDeviceType() {
        return new DeviceType();
    }

    /**
     * Create an instance of {@link MileageType }
     * 
     */
    public MileageType createMileageType() {
        return new MileageType();
    }

    /**
     * Create an instance of {@link DemurrageTransactionType }
     * 
     */
    public DemurrageTransactionType createDemurrageTransactionType() {
        return new DemurrageTransactionType();
    }

    /**
     * Create an instance of {@link VoyageType }
     * 
     */
    public VoyageType createVoyageType() {
        return new VoyageType();
    }

    /**
     * Create an instance of {@link CharterVoyageType }
     * 
     */
    public CharterVoyageType createCharterVoyageType() {
        return new CharterVoyageType();
    }

    /**
     * Create an instance of {@link ActualShipmentType }
     * 
     */
    public ActualShipmentType createActualShipmentType() {
        return new ActualShipmentType();
    }

    /**
     * Create an instance of {@link ShipmentGroupTenderOfferType }
     * 
     */
    public ShipmentGroupTenderOfferType createShipmentGroupTenderOfferType() {
        return new ShipmentGroupTenderOfferType();
    }

    /**
     * Create an instance of {@link ItemMasterType }
     * 
     */
    public ItemMasterType createItemMasterType() {
        return new ItemMasterType();
    }

    /**
     * Create an instance of {@link BulkTrailerBuildType }
     * 
     */
    public BulkTrailerBuildType createBulkTrailerBuildType() {
        return new BulkTrailerBuildType();
    }

    /**
     * Create an instance of {@link TransOrderType }
     * 
     */
    public TransOrderType createTransOrderType() {
        return new TransOrderType();
    }

    /**
     * Create an instance of {@link TransmissionReportType }
     * 
     */
    public TransmissionReportType createTransmissionReportType() {
        return new TransmissionReportType();
    }

    /**
     * Create an instance of {@link FleetBulkPlanType }
     * 
     */
    public FleetBulkPlanType createFleetBulkPlanType() {
        return new FleetBulkPlanType();
    }

    /**
     * Create an instance of {@link DriverCalendarEventType }
     * 
     */
    public DriverCalendarEventType createDriverCalendarEventType() {
        return new DriverCalendarEventType();
    }

    /**
     * Create an instance of {@link ExchangeRateType }
     * 
     */
    public ExchangeRateType createExchangeRateType() {
        return new ExchangeRateType();
    }

    /**
     * Create an instance of {@link UserType }
     * 
     */
    public UserType createUserType() {
        return new UserType();
    }

    /**
     * Create an instance of {@link SkuTransactionType }
     * 
     */
    public SkuTransactionType createSkuTransactionType() {
        return new SkuTransactionType();
    }

    /**
     * Create an instance of {@link DriverType }
     * 
     */
    public DriverType createDriverType() {
        return new DriverType();
    }

    /**
     * Create an instance of {@link ConsolType }
     * 
     */
    public ConsolType createConsolType() {
        return new ConsolType();
    }

    /**
     * Create an instance of {@link HazmatGenericType }
     * 
     */
    public HazmatGenericType createHazmatGenericType() {
        return new HazmatGenericType();
    }

    /**
     * Create an instance of {@link GenericStatusUpdateType }
     * 
     */
    public GenericStatusUpdateType createGenericStatusUpdateType() {
        return new GenericStatusUpdateType();
    }

    /**
     * Create an instance of {@link ActivityTimeDefType }
     * 
     */
    public ActivityTimeDefType createActivityTimeDefType() {
        return new ActivityTimeDefType();
    }

    /**
     * Create an instance of {@link ShipmentStatusType }
     * 
     */
    public ShipmentStatusType createShipmentStatusType() {
        return new ShipmentStatusType();
    }

    /**
     * Create an instance of {@link FinancialSystemFeedType }
     * 
     */
    public FinancialSystemFeedType createFinancialSystemFeedType() {
        return new FinancialSystemFeedType();
    }

    /**
     * Create an instance of {@link RemoteQueryReplyType }
     * 
     */
    public RemoteQueryReplyType createRemoteQueryReplyType() {
        return new RemoteQueryReplyType();
    }

    /**
     * Create an instance of {@link PowerUnitType }
     * 
     */
    public PowerUnitType createPowerUnitType() {
        return new PowerUnitType();
    }

    /**
     * Create an instance of {@link RemoteQueryType }
     * 
     */
    public RemoteQueryType createRemoteQueryType() {
        return new RemoteQueryType();
    }

    /**
     * Create an instance of {@link BulkPlanType }
     * 
     */
    public BulkPlanType createBulkPlanType() {
        return new BulkPlanType();
    }

    /**
     * Create an instance of {@link ShipmentGroupType }
     * 
     */
    public ShipmentGroupType createShipmentGroupType() {
        return new ShipmentGroupType();
    }

    /**
     * Create an instance of {@link SShipUnitType }
     * 
     */
    public SShipUnitType createSShipUnitType() {
        return new SShipUnitType();
    }

    /**
     * Create an instance of {@link VoucherType }
     * 
     */
    public VoucherType createVoucherType() {
        return new VoucherType();
    }

    /**
     * Create an instance of {@link QuoteType }
     * 
     */
    public QuoteType createQuoteType() {
        return new QuoteType();
    }

    /**
     * Create an instance of {@link CorporationType }
     * 
     */
    public CorporationType createCorporationType() {
        return new CorporationType();
    }

    /**
     * Create an instance of {@link OBLineType }
     * 
     */
    public OBLineType createOBLineType() {
        return new OBLineType();
    }

    /**
     * Create an instance of {@link TenderResponseType }
     * 
     */
    public TenderResponseType createTenderResponseType() {
        return new TenderResponseType();
    }

    /**
     * Create an instance of {@link BookingLineAmendmentType }
     * 
     */
    public BookingLineAmendmentType createBookingLineAmendmentType() {
        return new BookingLineAmendmentType();
    }

    /**
     * Create an instance of {@link PlannedShipmentType }
     * 
     */
    public PlannedShipmentType createPlannedShipmentType() {
        return new PlannedShipmentType();
    }

    /**
     * Create an instance of {@link EquipmentType }
     * 
     */
    public EquipmentType createEquipmentType() {
        return new EquipmentType();
    }

    /**
     * Create an instance of {@link ShipStopType }
     * 
     */
    public ShipStopType createShipStopType() {
        return new ShipStopType();
    }

    /**
     * Create an instance of {@link BillingType }
     * 
     */
    public BillingType createBillingType() {
        return new BillingType();
    }

    /**
     * Create an instance of {@link ServiceTimeType }
     * 
     */
    public ServiceTimeType createServiceTimeType() {
        return new ServiceTimeType();
    }

    /**
     * Create an instance of {@link SkuEventType }
     * 
     */
    public SkuEventType createSkuEventType() {
        return new SkuEventType();
    }

    /**
     * Create an instance of {@link XLANE.XLANEROW }
     * 
     */
    public XLANE.XLANEROW createXLANEXLANEROW() {
        return new XLANE.XLANEROW();
    }

    /**
     * Create an instance of {@link SkuType }
     * 
     */
    public SkuType createSkuType() {
        return new SkuType();
    }

    /**
     * Create an instance of {@link ClaimType }
     * 
     */
    public ClaimType createClaimType() {
        return new ClaimType();
    }

    /**
     * Create an instance of {@link AvailableByType }
     * 
     */
    public AvailableByType createAvailableByType() {
        return new AvailableByType();
    }

    /**
     * Create an instance of {@link SpecialServiceRefType }
     * 
     */
    public SpecialServiceRefType createSpecialServiceRefType() {
        return new SpecialServiceRefType();
    }

    /**
     * Create an instance of {@link SkuDescriptorType }
     * 
     */
    public SkuDescriptorType createSkuDescriptorType() {
        return new SkuDescriptorType();
    }

    /**
     * Create an instance of {@link GtmItemUomConversionType }
     * 
     */
    public GtmItemUomConversionType createGtmItemUomConversionType() {
        return new GtmItemUomConversionType();
    }

    /**
     * Create an instance of {@link RIQQuerySpecialServiceType }
     * 
     */
    public RIQQuerySpecialServiceType createRIQQuerySpecialServiceType() {
        return new RIQQuerySpecialServiceType();
    }

    /**
     * Create an instance of {@link ShipmentStopType }
     * 
     */
    public ShipmentStopType createShipmentStopType() {
        return new ShipmentStopType();
    }

    /**
     * Create an instance of {@link TermsType }
     * 
     */
    public TermsType createTermsType() {
        return new TermsType();
    }

    /**
     * Create an instance of {@link ShippingAgentContactProfitType }
     * 
     */
    public ShippingAgentContactProfitType createShippingAgentContactProfitType() {
        return new ShippingAgentContactProfitType();
    }

    /**
     * Create an instance of {@link TariffType }
     * 
     */
    public TariffType createTariffType() {
        return new TariffType();
    }

    /**
     * Create an instance of {@link OrderRoutingRuleReplyType }
     * 
     */
    public OrderRoutingRuleReplyType createOrderRoutingRuleReplyType() {
        return new OrderRoutingRuleReplyType();
    }

    /**
     * Create an instance of {@link GLogXMLRegionGidType }
     * 
     */
    public GLogXMLRegionGidType createGLogXMLRegionGidType() {
        return new GLogXMLRegionGidType();
    }

    /**
     * Create an instance of {@link BulkPlanByModeType }
     * 
     */
    public BulkPlanByModeType createBulkPlanByModeType() {
        return new BulkPlanByModeType();
    }

    /**
     * Create an instance of {@link SSContactType }
     * 
     */
    public SSContactType createSSContactType() {
        return new SSContactType();
    }

    /**
     * Create an instance of {@link SkuLevelType }
     * 
     */
    public SkuLevelType createSkuLevelType() {
        return new SkuLevelType();
    }

    /**
     * Create an instance of {@link GLogXMLLocOverrideRefType }
     * 
     */
    public GLogXMLLocOverrideRefType createGLogXMLLocOverrideRefType() {
        return new GLogXMLLocOverrideRefType();
    }

    /**
     * Create an instance of {@link EventGroupType }
     * 
     */
    public EventGroupType createEventGroupType() {
        return new EventGroupType();
    }

    /**
     * Create an instance of {@link ShipmentHeader2Type }
     * 
     */
    public ShipmentHeader2Type createShipmentHeader2Type() {
        return new ShipmentHeader2Type();
    }

    /**
     * Create an instance of {@link PortIdentifierType }
     * 
     */
    public PortIdentifierType createPortIdentifierType() {
        return new PortIdentifierType();
    }

    /**
     * Create an instance of {@link RouteCodeDetailType }
     * 
     */
    public RouteCodeDetailType createRouteCodeDetailType() {
        return new RouteCodeDetailType();
    }

    /**
     * Create an instance of {@link EquipmentRefnumType }
     * 
     */
    public EquipmentRefnumType createEquipmentRefnumType() {
        return new EquipmentRefnumType();
    }

    /**
     * Create an instance of {@link CommodityType }
     * 
     */
    public CommodityType createCommodityType() {
        return new CommodityType();
    }

    /**
     * Create an instance of {@link ShipmentInfCostDetailType }
     * 
     */
    public ShipmentInfCostDetailType createShipmentInfCostDetailType() {
        return new ShipmentInfCostDetailType();
    }

    /**
     * Create an instance of {@link RIQResultInfoType }
     * 
     */
    public RIQResultInfoType createRIQResultInfoType() {
        return new RIQResultInfoType();
    }

    /**
     * Create an instance of {@link RouteTempStrategicInfoType }
     * 
     */
    public RouteTempStrategicInfoType createRouteTempStrategicInfoType() {
        return new RouteTempStrategicInfoType();
    }

    /**
     * Create an instance of {@link AllocReleaseDetailType }
     * 
     */
    public AllocReleaseDetailType createAllocReleaseDetailType() {
        return new AllocReleaseDetailType();
    }

    /**
     * Create an instance of {@link GLogXMLHeightType }
     * 
     */
    public GLogXMLHeightType createGLogXMLHeightType() {
        return new GLogXMLHeightType();
    }

    /**
     * Create an instance of {@link GLogXMLDiameterType }
     * 
     */
    public GLogXMLDiameterType createGLogXMLDiameterType() {
        return new GLogXMLDiameterType();
    }

    /**
     * Create an instance of {@link GLogXMLEventTimeType }
     * 
     */
    public GLogXMLEventTimeType createGLogXMLEventTimeType() {
        return new GLogXMLEventTimeType();
    }

    /**
     * Create an instance of {@link GLogXMLDurationType }
     * 
     */
    public GLogXMLDurationType createGLogXMLDurationType() {
        return new GLogXMLDurationType();
    }

    /**
     * Create an instance of {@link AppointmentMissingType }
     * 
     */
    public AppointmentMissingType createAppointmentMissingType() {
        return new AppointmentMissingType();
    }

    /**
     * Create an instance of {@link InsuranceInfoType }
     * 
     */
    public InsuranceInfoType createInsuranceInfoType() {
        return new InsuranceInfoType();
    }

    /**
     * Create an instance of {@link UserDefIconInfoType }
     * 
     */
    public UserDefIconInfoType createUserDefIconInfoType() {
        return new UserDefIconInfoType();
    }

    /**
     * Create an instance of {@link PaymentHeaderType }
     * 
     */
    public PaymentHeaderType createPaymentHeaderType() {
        return new PaymentHeaderType();
    }

    /**
     * Create an instance of {@link ClaimLineItemType }
     * 
     */
    public ClaimLineItemType createClaimLineItemType() {
        return new ClaimLineItemType();
    }

    /**
     * Create an instance of {@link PortType }
     * 
     */
    public PortType createPortType() {
        return new PortType();
    }

    /**
     * Create an instance of {@link OperLocDetailType }
     * 
     */
    public OperLocDetailType createOperLocDetailType() {
        return new OperLocDetailType();
    }

    /**
     * Create an instance of {@link PackagingInnerPackType }
     * 
     */
    public PackagingInnerPackType createPackagingInnerPackType() {
        return new PackagingInnerPackType();
    }

    /**
     * Create an instance of {@link DemurrageTransactionLineitemType }
     * 
     */
    public DemurrageTransactionLineitemType createDemurrageTransactionLineitemType() {
        return new DemurrageTransactionLineitemType();
    }

    /**
     * Create an instance of {@link BilledRatedType }
     * 
     */
    public BilledRatedType createBilledRatedType() {
        return new BilledRatedType();
    }

    /**
     * Create an instance of {@link OrderMovementDType }
     * 
     */
    public OrderMovementDType createOrderMovementDType() {
        return new OrderMovementDType();
    }

    /**
     * Create an instance of {@link FlightInstanceRowType }
     * 
     */
    public FlightInstanceRowType createFlightInstanceRowType() {
        return new FlightInstanceRowType();
    }

    /**
     * Create an instance of {@link ContactRefType }
     * 
     */
    public ContactRefType createContactRefType() {
        return new ContactRefType();
    }

    /**
     * Create an instance of {@link ConditionalBookingType }
     * 
     */
    public ConditionalBookingType createConditionalBookingType() {
        return new ConditionalBookingType();
    }

    /**
     * Create an instance of {@link EquipTypeSpecialServiceType }
     * 
     */
    public EquipTypeSpecialServiceType createEquipTypeSpecialServiceType() {
        return new EquipTypeSpecialServiceType();
    }

    /**
     * Create an instance of {@link EventReasonType }
     * 
     */
    public EventReasonType createEventReasonType() {
        return new EventReasonType();
    }

    /**
     * Create an instance of {@link GLogXMLGlPassword }
     * 
     */
    public GLogXMLGlPassword createGLogXMLGlPassword() {
        return new GLogXMLGlPassword();
    }

    /**
     * Create an instance of {@link PriLegServProvType }
     * 
     */
    public PriLegServProvType createPriLegServProvType() {
        return new PriLegServProvType();
    }

    /**
     * Create an instance of {@link PriShipRefObjectType }
     * 
     */
    public PriShipRefObjectType createPriShipRefObjectType() {
        return new PriShipRefObjectType();
    }

    /**
     * Create an instance of {@link FlightRowType }
     * 
     */
    public FlightRowType createFlightRowType() {
        return new FlightRowType();
    }

    /**
     * Create an instance of {@link RIQSecondaryChargesType }
     * 
     */
    public RIQSecondaryChargesType createRIQSecondaryChargesType() {
        return new RIQSecondaryChargesType();
    }

    /**
     * Create an instance of {@link GenericQueryReplyType }
     * 
     */
    public GenericQueryReplyType createGenericQueryReplyType() {
        return new GenericQueryReplyType();
    }

    /**
     * Create an instance of {@link UOMContextType }
     * 
     */
    public UOMContextType createUOMContextType() {
        return new UOMContextType();
    }

    /**
     * Create an instance of {@link StatusCodeType }
     * 
     */
    public StatusCodeType createStatusCodeType() {
        return new StatusCodeType();
    }

    /**
     * Create an instance of {@link OceanEquipmentType }
     * 
     */
    public OceanEquipmentType createOceanEquipmentType() {
        return new OceanEquipmentType();
    }

    /**
     * Create an instance of {@link StagingInfoType }
     * 
     */
    public StagingInfoType createStagingInfoType() {
        return new StagingInfoType();
    }

    /**
     * Create an instance of {@link EquipmentSpecialServiceType }
     * 
     */
    public EquipmentSpecialServiceType createEquipmentSpecialServiceType() {
        return new EquipmentSpecialServiceType();
    }

    /**
     * Create an instance of {@link OceanDetailType }
     * 
     */
    public OceanDetailType createOceanDetailType() {
        return new OceanDetailType();
    }

    /**
     * Create an instance of {@link CostOptionEquipType }
     * 
     */
    public CostOptionEquipType createCostOptionEquipType() {
        return new CostOptionEquipType();
    }

    /**
     * Create an instance of {@link VatAnalysisType }
     * 
     */
    public VatAnalysisType createVatAnalysisType() {
        return new VatAnalysisType();
    }

    /**
     * Create an instance of {@link ReasonCodeType }
     * 
     */
    public ReasonCodeType createReasonCodeType() {
        return new ReasonCodeType();
    }

    /**
     * Create an instance of {@link PaymentSummaryDetailType }
     * 
     */
    public PaymentSummaryDetailType createPaymentSummaryDetailType() {
        return new PaymentSummaryDetailType();
    }

    /**
     * Create an instance of {@link ShipUnitViewInfoType }
     * 
     */
    public ShipUnitViewInfoType createShipUnitViewInfoType() {
        return new ShipUnitViewInfoType();
    }

    /**
     * Create an instance of {@link BookLineAmendViaReleaseType }
     * 
     */
    public BookLineAmendViaReleaseType createBookLineAmendViaReleaseType() {
        return new BookLineAmendViaReleaseType();
    }

    /**
     * Create an instance of {@link MileageCorporationType }
     * 
     */
    public MileageCorporationType createMileageCorporationType() {
        return new MileageCorporationType();
    }

    /**
     * Create an instance of {@link RouteType }
     * 
     */
    public RouteType createRouteType() {
        return new RouteType();
    }

    /**
     * Create an instance of {@link EquipmentDescriptionType }
     * 
     */
    public EquipmentDescriptionType createEquipmentDescriptionType() {
        return new EquipmentDescriptionType();
    }

    /**
     * Create an instance of {@link ShipmentGroupRefnumType }
     * 
     */
    public ShipmentGroupRefnumType createShipmentGroupRefnumType() {
        return new ShipmentGroupRefnumType();
    }

    /**
     * Create an instance of {@link AddressType }
     * 
     */
    public AddressType createAddressType() {
        return new AddressType();
    }

    /**
     * Create an instance of {@link AppointmentQueryType }
     * 
     */
    public AppointmentQueryType createAppointmentQueryType() {
        return new AppointmentQueryType();
    }

    /**
     * Create an instance of {@link ShipUnitRefnumType }
     * 
     */
    public ShipUnitRefnumType createShipUnitRefnumType() {
        return new ShipUnitRefnumType();
    }

    /**
     * Create an instance of {@link PowerUnitHeaderType }
     * 
     */
    public PowerUnitHeaderType createPowerUnitHeaderType() {
        return new PowerUnitHeaderType();
    }

    /**
     * Create an instance of {@link Rule11SecondShipmentType }
     * 
     */
    public Rule11SecondShipmentType createRule11SecondShipmentType() {
        return new Rule11SecondShipmentType();
    }

    /**
     * Create an instance of {@link TransactionReportType }
     * 
     */
    public TransactionReportType createTransactionReportType() {
        return new TransactionReportType();
    }

    /**
     * Create an instance of {@link FinalCommercialTermsType }
     * 
     */
    public FinalCommercialTermsType createFinalCommercialTermsType() {
        return new FinalCommercialTermsType();
    }

    /**
     * Create an instance of {@link TimeWithTimezoneType }
     * 
     */
    public TimeWithTimezoneType createTimeWithTimezoneType() {
        return new TimeWithTimezoneType();
    }

    /**
     * Create an instance of {@link InRouteCarriersType }
     * 
     */
    public InRouteCarriersType createInRouteCarriersType() {
        return new InRouteCarriersType();
    }

    /**
     * Create an instance of {@link ReleaseLineIntlClassificationType }
     * 
     */
    public ReleaseLineIntlClassificationType createReleaseLineIntlClassificationType() {
        return new ReleaseLineIntlClassificationType();
    }

    /**
     * Create an instance of {@link SStatusShipUnitType }
     * 
     */
    public SStatusShipUnitType createSStatusShipUnitType() {
        return new SStatusShipUnitType();
    }

    /**
     * Create an instance of {@link ReleaseRefnumType }
     * 
     */
    public ReleaseRefnumType createReleaseRefnumType() {
        return new ReleaseRefnumType();
    }

    /**
     * Create an instance of {@link LineGidType }
     * 
     */
    public LineGidType createLineGidType() {
        return new LineGidType();
    }

    /**
     * Create an instance of {@link SpecialChargeType }
     * 
     */
    public SpecialChargeType createSpecialChargeType() {
        return new SpecialChargeType();
    }

    /**
     * Create an instance of {@link PostalCodeRangeType }
     * 
     */
    public PostalCodeRangeType createPostalCodeRangeType() {
        return new PostalCodeRangeType();
    }

    /**
     * Create an instance of {@link VoucherInvoiceLineItemType }
     * 
     */
    public VoucherInvoiceLineItemType createVoucherInvoiceLineItemType() {
        return new VoucherInvoiceLineItemType();
    }

    /**
     * Create an instance of {@link MotorLineItemType }
     * 
     */
    public MotorLineItemType createMotorLineItemType() {
        return new MotorLineItemType();
    }

    /**
     * Create an instance of {@link PackageDimensionsType }
     * 
     */
    public PackageDimensionsType createPackageDimensionsType() {
        return new PackageDimensionsType();
    }

    /**
     * Create an instance of {@link LetterOfCreditType }
     * 
     */
    public LetterOfCreditType createLetterOfCreditType() {
        return new LetterOfCreditType();
    }

    /**
     * Create an instance of {@link ShipUnitViewType }
     * 
     */
    public ShipUnitViewType createShipUnitViewType() {
        return new ShipUnitViewType();
    }

    /**
     * Create an instance of {@link LineItemCostRefType }
     * 
     */
    public LineItemCostRefType createLineItemCostRefType() {
        return new LineItemCostRefType();
    }

    /**
     * Create an instance of {@link FlexFieldStringType }
     * 
     */
    public FlexFieldStringType createFlexFieldStringType() {
        return new FlexFieldStringType();
    }

    /**
     * Create an instance of {@link TransOrderLineType }
     * 
     */
    public TransOrderLineType createTransOrderLineType() {
        return new TransOrderLineType();
    }

    /**
     * Create an instance of {@link RailType }
     * 
     */
    public RailType createRailType() {
        return new RailType();
    }

    /**
     * Create an instance of {@link THUCapacityEquipRefUnitType }
     * 
     */
    public THUCapacityEquipRefUnitType createTHUCapacityEquipRefUnitType() {
        return new THUCapacityEquipRefUnitType();
    }

    /**
     * Create an instance of {@link THUCapacityPackageRefUnitType }
     * 
     */
    public THUCapacityPackageRefUnitType createTHUCapacityPackageRefUnitType() {
        return new THUCapacityPackageRefUnitType();
    }

    /**
     * Create an instance of {@link PackageRefUnittype }
     * 
     */
    public PackageRefUnittype createPackageRefUnittype() {
        return new PackageRefUnittype();
    }

    /**
     * Create an instance of {@link ShippingAgentContactType }
     * 
     */
    public ShippingAgentContactType createShippingAgentContactType() {
        return new ShippingAgentContactType();
    }

    /**
     * Create an instance of {@link StopOffType }
     * 
     */
    public StopOffType createStopOffType() {
        return new StopOffType();
    }

    /**
     * Create an instance of {@link GenericEquipmentType }
     * 
     */
    public GenericEquipmentType createGenericEquipmentType() {
        return new GenericEquipmentType();
    }

    /**
     * Create an instance of {@link ShipRouteExecutionInfoType }
     * 
     */
    public ShipRouteExecutionInfoType createShipRouteExecutionInfoType() {
        return new ShipRouteExecutionInfoType();
    }

    /**
     * Create an instance of {@link ShipmentInfCostType }
     * 
     */
    public ShipmentInfCostType createShipmentInfCostType() {
        return new ShipmentInfCostType();
    }

    /**
     * Create an instance of {@link CountryCodeType }
     * 
     */
    public CountryCodeType createCountryCodeType() {
        return new CountryCodeType();
    }

    /**
     * Create an instance of {@link ShipmentCostRefType }
     * 
     */
    public ShipmentCostRefType createShipmentCostRefType() {
        return new ShipmentCostRefType();
    }

    /**
     * Create an instance of {@link InvoiceRefnumType }
     * 
     */
    public InvoiceRefnumType createInvoiceRefnumType() {
        return new InvoiceRefnumType();
    }

    /**
     * Create an instance of {@link SEquipmentSShipUnitInfoType }
     * 
     */
    public SEquipmentSShipUnitInfoType createSEquipmentSShipUnitInfoType() {
        return new SEquipmentSShipUnitInfoType();
    }

    /**
     * Create an instance of {@link SSStopType }
     * 
     */
    public SSStopType createSSStopType() {
        return new SSStopType();
    }

    /**
     * Create an instance of {@link GLogXMLWeightType }
     * 
     */
    public GLogXMLWeightType createGLogXMLWeightType() {
        return new GLogXMLWeightType();
    }

    /**
     * Create an instance of {@link TextDocumentDefinitionType }
     * 
     */
    public TextDocumentDefinitionType createTextDocumentDefinitionType() {
        return new TextDocumentDefinitionType();
    }

    /**
     * Create an instance of {@link QuoteLocInfoType }
     * 
     */
    public QuoteLocInfoType createQuoteLocInfoType() {
        return new QuoteLocInfoType();
    }

    /**
     * Create an instance of {@link ImportLicenseType }
     * 
     */
    public ImportLicenseType createImportLicenseType() {
        return new ImportLicenseType();
    }

    /**
     * Create an instance of {@link VatProvincialRegistrationType }
     * 
     */
    public VatProvincialRegistrationType createVatProvincialRegistrationType() {
        return new VatProvincialRegistrationType();
    }

    /**
     * Create an instance of {@link AccessorialRefType }
     * 
     */
    public AccessorialRefType createAccessorialRefType() {
        return new AccessorialRefType();
    }

    /**
     * Create an instance of {@link ShippingAgentType }
     * 
     */
    public ShippingAgentType createShippingAgentType() {
        return new ShippingAgentType();
    }

    /**
     * Create an instance of {@link LegRefType }
     * 
     */
    public LegRefType createLegRefType() {
        return new LegRefType();
    }

    /**
     * Create an instance of {@link EquipmentRefUnitType }
     * 
     */
    public EquipmentRefUnitType createEquipmentRefUnitType() {
        return new EquipmentRefUnitType();
    }

    /**
     * Create an instance of {@link CommercialInvoiceDataType }
     * 
     */
    public CommercialInvoiceDataType createCommercialInvoiceDataType() {
        return new CommercialInvoiceDataType();
    }

    /**
     * Create an instance of {@link FlightInstanceType }
     * 
     */
    public FlightInstanceType createFlightInstanceType() {
        return new FlightInstanceType();
    }

    /**
     * Create an instance of {@link GLogXMLStationType }
     * 
     */
    public GLogXMLStationType createGLogXMLStationType() {
        return new GLogXMLStationType();
    }

    /**
     * Create an instance of {@link BookLineAmendDtlSEquipmentType }
     * 
     */
    public BookLineAmendDtlSEquipmentType createBookLineAmendDtlSEquipmentType() {
        return new BookLineAmendDtlSEquipmentType();
    }

    /**
     * Create an instance of {@link ShipmentOrderReleaseType }
     * 
     */
    public ShipmentOrderReleaseType createShipmentOrderReleaseType() {
        return new ShipmentOrderReleaseType();
    }

    /**
     * Create an instance of {@link RouteCodeType }
     * 
     */
    public RouteCodeType createRouteCodeType() {
        return new RouteCodeType();
    }

    /**
     * Create an instance of {@link ShipStopDebriefType }
     * 
     */
    public ShipStopDebriefType createShipStopDebriefType() {
        return new ShipStopDebriefType();
    }

    /**
     * Create an instance of {@link GLogXMLElementType }
     * 
     */
    public GLogXMLElementType createGLogXMLElementType() {
        return new GLogXMLElementType();
    }

    /**
     * Create an instance of {@link RouteTemplateLegHdrType }
     * 
     */
    public RouteTemplateLegHdrType createRouteTemplateLegHdrType() {
        return new RouteTemplateLegHdrType();
    }

    /**
     * Create an instance of {@link HazmatCommonInfoType }
     * 
     */
    public HazmatCommonInfoType createHazmatCommonInfoType() {
        return new HazmatCommonInfoType();
    }

    /**
     * Create an instance of {@link InspectionAndSurveyInfoType }
     * 
     */
    public InspectionAndSurveyInfoType createInspectionAndSurveyInfoType() {
        return new InspectionAndSurveyInfoType();
    }

    /**
     * Create an instance of {@link VesselType }
     * 
     */
    public VesselType createVesselType() {
        return new VesselType();
    }

    /**
     * Create an instance of {@link QuoteLocationType }
     * 
     */
    public QuoteLocationType createQuoteLocationType() {
        return new QuoteLocationType();
    }

    /**
     * Create an instance of {@link ShipUnitViewByReleaseLineType }
     * 
     */
    public ShipUnitViewByReleaseLineType createShipUnitViewByReleaseLineType() {
        return new ShipUnitViewByReleaseLineType();
    }

    /**
     * Create an instance of {@link IntegrationLogDetailType }
     * 
     */
    public IntegrationLogDetailType createIntegrationLogDetailType() {
        return new IntegrationLogDetailType();
    }

    /**
     * Create an instance of {@link CommercialInvoiceChargeType }
     * 
     */
    public CommercialInvoiceChargeType createCommercialInvoiceChargeType() {
        return new CommercialInvoiceChargeType();
    }

    /**
     * Create an instance of {@link ReleaseLegConstraintType }
     * 
     */
    public ReleaseLegConstraintType createReleaseLegConstraintType() {
        return new ReleaseLegConstraintType();
    }

    /**
     * Create an instance of {@link DocumentOwnerType }
     * 
     */
    public DocumentOwnerType createDocumentOwnerType() {
        return new DocumentOwnerType();
    }

    /**
     * Create an instance of {@link AppointmentQueryReplyType }
     * 
     */
    public AppointmentQueryReplyType createAppointmentQueryReplyType() {
        return new AppointmentQueryReplyType();
    }

    /**
     * Create an instance of {@link EquipMarkListType }
     * 
     */
    public EquipMarkListType createEquipMarkListType() {
        return new EquipMarkListType();
    }

    /**
     * Create an instance of {@link RIQResultType }
     * 
     */
    public RIQResultType createRIQResultType() {
        return new RIQResultType();
    }

    /**
     * Create an instance of {@link OrderRefnumType }
     * 
     */
    public OrderRefnumType createOrderRefnumType() {
        return new OrderRefnumType();
    }

    /**
     * Create an instance of {@link GLogXMLFlexQuantityType }
     * 
     */
    public GLogXMLFlexQuantityType createGLogXMLFlexQuantityType() {
        return new GLogXMLFlexQuantityType();
    }

    /**
     * Create an instance of {@link TimeWindowType }
     * 
     */
    public TimeWindowType createTimeWindowType() {
        return new TimeWindowType();
    }

    /**
     * Create an instance of {@link RIQResultRouteNRType }
     * 
     */
    public RIQResultRouteNRType createRIQResultRouteNRType() {
        return new RIQResultRouteNRType();
    }

    /**
     * Create an instance of {@link CostDetailInfoType }
     * 
     */
    public CostDetailInfoType createCostDetailInfoType() {
        return new CostDetailInfoType();
    }

    /**
     * Create an instance of {@link GenericQueryType }
     * 
     */
    public GenericQueryType createGenericQueryType() {
        return new GenericQueryType();
    }

    /**
     * Create an instance of {@link PackagingShipUnitType }
     * 
     */
    public PackagingShipUnitType createPackagingShipUnitType() {
        return new PackagingShipUnitType();
    }

    /**
     * Create an instance of {@link Rule11SecShipRateOfferingType }
     * 
     */
    public Rule11SecShipRateOfferingType createRule11SecShipRateOfferingType() {
        return new Rule11SecShipRateOfferingType();
    }

    /**
     * Create an instance of {@link StatusGroupType }
     * 
     */
    public StatusGroupType createStatusGroupType() {
        return new StatusGroupType();
    }

    /**
     * Create an instance of {@link FinancialsType }
     * 
     */
    public FinancialsType createFinancialsType() {
        return new FinancialsType();
    }

    /**
     * Create an instance of {@link ExchangeRateInfoType }
     * 
     */
    public ExchangeRateInfoType createExchangeRateInfoType() {
        return new ExchangeRateInfoType();
    }

    /**
     * Create an instance of {@link ShipmentQueryReplyType }
     * 
     */
    public ShipmentQueryReplyType createShipmentQueryReplyType() {
        return new ShipmentQueryReplyType();
    }

    /**
     * Create an instance of {@link OrderRoutingRuleQueryType }
     * 
     */
    public OrderRoutingRuleQueryType createOrderRoutingRuleQueryType() {
        return new OrderRoutingRuleQueryType();
    }

    /**
     * Create an instance of {@link LocationTHUCapacityType }
     * 
     */
    public LocationTHUCapacityType createLocationTHUCapacityType() {
        return new LocationTHUCapacityType();
    }

    /**
     * Create an instance of {@link ItemAttributeType }
     * 
     */
    public ItemAttributeType createItemAttributeType() {
        return new ItemAttributeType();
    }

    /**
     * Create an instance of {@link DriverCdlType }
     * 
     */
    public DriverCdlType createDriverCdlType() {
        return new DriverCdlType();
    }

    /**
     * Create an instance of {@link GidType }
     * 
     */
    public GidType createGidType() {
        return new GidType();
    }

    /**
     * Create an instance of {@link AllocReleaseLineDetailType }
     * 
     */
    public AllocReleaseLineDetailType createAllocReleaseLineDetailType() {
        return new AllocReleaseLineDetailType();
    }

    /**
     * Create an instance of {@link NotifyInfoType }
     * 
     */
    public NotifyInfoType createNotifyInfoType() {
        return new NotifyInfoType();
    }

    /**
     * Create an instance of {@link CharterVoyageStowageType }
     * 
     */
    public CharterVoyageStowageType createCharterVoyageStowageType() {
        return new CharterVoyageStowageType();
    }

    /**
     * Create an instance of {@link GLogXMLLWHType }
     * 
     */
    public GLogXMLLWHType createGLogXMLLWHType() {
        return new GLogXMLLWHType();
    }

    /**
     * Create an instance of {@link InterimFlightInstanceType }
     * 
     */
    public InterimFlightInstanceType createInterimFlightInstanceType() {
        return new InterimFlightInstanceType();
    }

    /**
     * Create an instance of {@link ShipmentAllocationType }
     * 
     */
    public ShipmentAllocationType createShipmentAllocationType() {
        return new ShipmentAllocationType();
    }

    /**
     * Create an instance of {@link BookLineAmendViaServiceProviderType }
     * 
     */
    public BookLineAmendViaServiceProviderType createBookLineAmendViaServiceProviderType() {
        return new BookLineAmendViaServiceProviderType();
    }

    /**
     * Create an instance of {@link CostDetailsType }
     * 
     */
    public CostDetailsType createCostDetailsType() {
        return new CostDetailsType();
    }

    /**
     * Create an instance of {@link CommercialInvoiceTermsType }
     * 
     */
    public CommercialInvoiceTermsType createCommercialInvoiceTermsType() {
        return new CommercialInvoiceTermsType();
    }

    /**
     * Create an instance of {@link DurationType }
     * 
     */
    public DurationType createDurationType() {
        return new DurationType();
    }

    /**
     * Create an instance of {@link TechnicalNameGidType }
     * 
     */
    public TechnicalNameGidType createTechnicalNameGidType() {
        return new TechnicalNameGidType();
    }

    /**
     * Create an instance of {@link StopSealType }
     * 
     */
    public StopSealType createStopSealType() {
        return new StopSealType();
    }

    /**
     * Create an instance of {@link ShippingAgentContactNoteType }
     * 
     */
    public ShippingAgentContactNoteType createShippingAgentContactNoteType() {
        return new ShippingAgentContactNoteType();
    }

    /**
     * Create an instance of {@link DiameterType }
     * 
     */
    public DiameterType createDiameterType() {
        return new DiameterType();
    }

    /**
     * Create an instance of {@link BookLineAmendDetailType }
     * 
     */
    public BookLineAmendDetailType createBookLineAmendDetailType() {
        return new BookLineAmendDetailType();
    }

    /**
     * Create an instance of {@link CommonInvoiceLineElementsType }
     * 
     */
    public CommonInvoiceLineElementsType createCommonInvoiceLineElementsType() {
        return new CommonInvoiceLineElementsType();
    }

    /**
     * Create an instance of {@link VoucherRefnumType }
     * 
     */
    public VoucherRefnumType createVoucherRefnumType() {
        return new VoucherRefnumType();
    }

    /**
     * Create an instance of {@link QuoteHdrType }
     * 
     */
    public QuoteHdrType createQuoteHdrType() {
        return new QuoteHdrType();
    }

    /**
     * Create an instance of {@link SkuEventQuantityType }
     * 
     */
    public SkuEventQuantityType createSkuEventQuantityType() {
        return new SkuEventQuantityType();
    }

    /**
     * Create an instance of {@link PrimaryLegRouteOptionType }
     * 
     */
    public PrimaryLegRouteOptionType createPrimaryLegRouteOptionType() {
        return new PrimaryLegRouteOptionType();
    }

    /**
     * Create an instance of {@link HazmatItemRefType }
     * 
     */
    public HazmatItemRefType createHazmatItemRefType() {
        return new HazmatItemRefType();
    }

    /**
     * Create an instance of {@link UserPasswordType }
     * 
     */
    public UserPasswordType createUserPasswordType() {
        return new UserPasswordType();
    }

    /**
     * Create an instance of {@link GLogXMLIntSavedQueryType }
     * 
     */
    public GLogXMLIntSavedQueryType createGLogXMLIntSavedQueryType() {
        return new GLogXMLIntSavedQueryType();
    }

    /**
     * Create an instance of {@link CommercialInvoiceType }
     * 
     */
    public CommercialInvoiceType createCommercialInvoiceType() {
        return new CommercialInvoiceType();
    }

    /**
     * Create an instance of {@link GlPasswordType }
     * 
     */
    public GlPasswordType createGlPasswordType() {
        return new GlPasswordType();
    }

    /**
     * Create an instance of {@link ShipUnitSpecType }
     * 
     */
    public ShipUnitSpecType createShipUnitSpecType() {
        return new ShipUnitSpecType();
    }

    /**
     * Create an instance of {@link ProtectiveSvcType }
     * 
     */
    public ProtectiveSvcType createProtectiveSvcType() {
        return new ProtectiveSvcType();
    }

    /**
     * Create an instance of {@link SEquipmentGidQueryType }
     * 
     */
    public SEquipmentGidQueryType createSEquipmentGidQueryType() {
        return new SEquipmentGidQueryType();
    }

    /**
     * Create an instance of {@link AccrualShipmentInfoType }
     * 
     */
    public AccrualShipmentInfoType createAccrualShipmentInfoType() {
        return new AccrualShipmentInfoType();
    }

    /**
     * Create an instance of {@link PowerUnitSpecialServiceType }
     * 
     */
    public PowerUnitSpecialServiceType createPowerUnitSpecialServiceType() {
        return new PowerUnitSpecialServiceType();
    }

    /**
     * Create an instance of {@link PickupByType }
     * 
     */
    public PickupByType createPickupByType() {
        return new PickupByType();
    }

    /**
     * Create an instance of {@link FlexFieldNumberType }
     * 
     */
    public FlexFieldNumberType createFlexFieldNumberType() {
        return new FlexFieldNumberType();
    }

    /**
     * Create an instance of {@link LocationRefType }
     * 
     */
    public LocationRefType createLocationRefType() {
        return new LocationRefType();
    }

    /**
     * Create an instance of {@link DemurrageTransactionEventType }
     * 
     */
    public DemurrageTransactionEventType createDemurrageTransactionEventType() {
        return new DemurrageTransactionEventType();
    }

    /**
     * Create an instance of {@link GLogXMLLocRefType }
     * 
     */
    public GLogXMLLocRefType createGLogXMLLocRefType() {
        return new GLogXMLLocRefType();
    }

    /**
     * Create an instance of {@link TariffItemNumType }
     * 
     */
    public TariffItemNumType createTariffItemNumType() {
        return new TariffItemNumType();
    }

    /**
     * Create an instance of {@link ServiceProviderAliasType }
     * 
     */
    public ServiceProviderAliasType createServiceProviderAliasType() {
        return new ServiceProviderAliasType();
    }

    /**
     * Create an instance of {@link PlannedShipmentInfoType }
     * 
     */
    public PlannedShipmentInfoType createPlannedShipmentInfoType() {
        return new PlannedShipmentInfoType();
    }

    /**
     * Create an instance of {@link FleetBulkPlanCostType }
     * 
     */
    public FleetBulkPlanCostType createFleetBulkPlanCostType() {
        return new FleetBulkPlanCostType();
    }

    /**
     * Create an instance of {@link BulkPlanPartitionByModeType }
     * 
     */
    public BulkPlanPartitionByModeType createBulkPlanPartitionByModeType() {
        return new BulkPlanPartitionByModeType();
    }

    /**
     * Create an instance of {@link LocationRefnumType }
     * 
     */
    public LocationRefnumType createLocationRefnumType() {
        return new LocationRefnumType();
    }

    /**
     * Create an instance of {@link RailStopOffType }
     * 
     */
    public RailStopOffType createRailStopOffType() {
        return new RailStopOffType();
    }

    /**
     * Create an instance of {@link AssetParentLocationType }
     * 
     */
    public AssetParentLocationType createAssetParentLocationType() {
        return new AssetParentLocationType();
    }

    /**
     * Create an instance of {@link DemurrageInvolvedPartyType }
     * 
     */
    public DemurrageInvolvedPartyType createDemurrageInvolvedPartyType() {
        return new DemurrageInvolvedPartyType();
    }

    /**
     * Create an instance of {@link AccrualTransOrderInfoType }
     * 
     */
    public AccrualTransOrderInfoType createAccrualTransOrderInfoType() {
        return new AccrualTransOrderInfoType();
    }

    /**
     * Create an instance of {@link GLogXMLGidType }
     * 
     */
    public GLogXMLGidType createGLogXMLGidType() {
        return new GLogXMLGidType();
    }

    /**
     * Create an instance of {@link GtmItemCountryOfOriginType }
     * 
     */
    public GtmItemCountryOfOriginType createGtmItemCountryOfOriginType() {
        return new GtmItemCountryOfOriginType();
    }

    /**
     * Create an instance of {@link ClaimNoteType }
     * 
     */
    public ClaimNoteType createClaimNoteType() {
        return new ClaimNoteType();
    }

    /**
     * Create an instance of {@link ShipmentSEquipmentInfoType }
     * 
     */
    public ShipmentSEquipmentInfoType createShipmentSEquipmentInfoType() {
        return new ShipmentSEquipmentInfoType();
    }

    /**
     * Create an instance of {@link LocationOverrideInfoType }
     * 
     */
    public LocationOverrideInfoType createLocationOverrideInfoType() {
        return new LocationOverrideInfoType();
    }

    /**
     * Create an instance of {@link CondBookingFieldInfoType }
     * 
     */
    public CondBookingFieldInfoType createCondBookingFieldInfoType() {
        return new CondBookingFieldInfoType();
    }

    /**
     * Create an instance of {@link ShipmentInvoiceCostInfoType }
     * 
     */
    public ShipmentInvoiceCostInfoType createShipmentInvoiceCostInfoType() {
        return new ShipmentInvoiceCostInfoType();
    }

    /**
     * Create an instance of {@link GLogXMLLocGidType }
     * 
     */
    public GLogXMLLocGidType createGLogXMLLocGidType() {
        return new GLogXMLLocGidType();
    }

    /**
     * Create an instance of {@link PaymentSummaryType }
     * 
     */
    public PaymentSummaryType createPaymentSummaryType() {
        return new PaymentSummaryType();
    }

    /**
     * Create an instance of {@link RailEquipmentType }
     * 
     */
    public RailEquipmentType createRailEquipmentType() {
        return new RailEquipmentType();
    }

    /**
     * Create an instance of {@link GtmItemDescriptionType }
     * 
     */
    public GtmItemDescriptionType createGtmItemDescriptionType() {
        return new GtmItemDescriptionType();
    }

    /**
     * Create an instance of {@link ShipUnitDetailType }
     * 
     */
    public ShipUnitDetailType createShipUnitDetailType() {
        return new ShipUnitDetailType();
    }

    /**
     * Create an instance of {@link RailInfoType }
     * 
     */
    public RailInfoType createRailInfoType() {
        return new RailInfoType();
    }

    /**
     * Create an instance of {@link DeliveryByType }
     * 
     */
    public DeliveryByType createDeliveryByType() {
        return new DeliveryByType();
    }

    /**
     * Create an instance of {@link OperationalLocationType }
     * 
     */
    public OperationalLocationType createOperationalLocationType() {
        return new OperationalLocationType();
    }

    /**
     * Create an instance of {@link PaymentType }
     * 
     */
    public PaymentType createPaymentType() {
        return new PaymentType();
    }

    /**
     * Create an instance of {@link InvolvedPartyType }
     * 
     */
    public InvolvedPartyType createInvolvedPartyType() {
        return new InvolvedPartyType();
    }

    /**
     * Create an instance of {@link CostOptionType }
     * 
     */
    public CostOptionType createCostOptionType() {
        return new CostOptionType();
    }

    /**
     * Create an instance of {@link SkuQuantityType }
     * 
     */
    public SkuQuantityType createSkuQuantityType() {
        return new SkuQuantityType();
    }

    /**
     * Create an instance of {@link RouteTemplateHdrType }
     * 
     */
    public RouteTemplateHdrType createRouteTemplateHdrType() {
        return new RouteTemplateHdrType();
    }

    /**
     * Create an instance of {@link FlexFieldCurrencyType }
     * 
     */
    public FlexFieldCurrencyType createFlexFieldCurrencyType() {
        return new FlexFieldCurrencyType();
    }

    /**
     * Create an instance of {@link VoyageLocType }
     * 
     */
    public VoyageLocType createVoyageLocType() {
        return new VoyageLocType();
    }

    /**
     * Create an instance of {@link PkgItemPackageRefUnitType }
     * 
     */
    public PkgItemPackageRefUnitType createPkgItemPackageRefUnitType() {
        return new PkgItemPackageRefUnitType();
    }

    /**
     * Create an instance of {@link LineItemRefNumType }
     * 
     */
    public LineItemRefNumType createLineItemRefNumType() {
        return new LineItemRefNumType();
    }

    /**
     * Create an instance of {@link LocationResourceType }
     * 
     */
    public LocationResourceType createLocationResourceType() {
        return new LocationResourceType();
    }

    /**
     * Create an instance of {@link DriverCalendarType }
     * 
     */
    public DriverCalendarType createDriverCalendarType() {
        return new DriverCalendarType();
    }

    /**
     * Create an instance of {@link DriverTeamType }
     * 
     */
    public DriverTeamType createDriverTeamType() {
        return new DriverTeamType();
    }

    /**
     * Create an instance of {@link ShipStatusSpclServiceType }
     * 
     */
    public ShipStatusSpclServiceType createShipStatusSpclServiceType() {
        return new ShipStatusSpclServiceType();
    }

    /**
     * Create an instance of {@link ReleaseAllocType }
     * 
     */
    public ReleaseAllocType createReleaseAllocType() {
        return new ReleaseAllocType();
    }

    /**
     * Create an instance of {@link GLogXMLVolumeType }
     * 
     */
    public GLogXMLVolumeType createGLogXMLVolumeType() {
        return new GLogXMLVolumeType();
    }

    /**
     * Create an instance of {@link QuoteShipUnitType }
     * 
     */
    public QuoteShipUnitType createQuoteShipUnitType() {
        return new QuoteShipUnitType();
    }

    /**
     * Create an instance of {@link SkuTransactionDescriptorType }
     * 
     */
    public SkuTransactionDescriptorType createSkuTransactionDescriptorType() {
        return new SkuTransactionDescriptorType();
    }

    /**
     * Create an instance of {@link WeightType }
     * 
     */
    public WeightType createWeightType() {
        return new WeightType();
    }

    /**
     * Create an instance of {@link DistanceType }
     * 
     */
    public DistanceType createDistanceType() {
        return new DistanceType();
    }

    /**
     * Create an instance of {@link GLogXMLFinancialAmountType }
     * 
     */
    public GLogXMLFinancialAmountType createGLogXMLFinancialAmountType() {
        return new GLogXMLFinancialAmountType();
    }

    /**
     * Create an instance of {@link ShipmentSpecialServiceType }
     * 
     */
    public ShipmentSpecialServiceType createShipmentSpecialServiceType() {
        return new ShipmentSpecialServiceType();
    }

    /**
     * Create an instance of {@link GLogXMLLengthType }
     * 
     */
    public GLogXMLLengthType createGLogXMLLengthType() {
        return new GLogXMLLengthType();
    }

    /**
     * Create an instance of {@link GLogXMLCostPerUOM }
     * 
     */
    public GLogXMLCostPerUOM createGLogXMLCostPerUOM() {
        return new GLogXMLCostPerUOM();
    }

    /**
     * Create an instance of {@link DemurrageTransactionRemarkType }
     * 
     */
    public DemurrageTransactionRemarkType createDemurrageTransactionRemarkType() {
        return new DemurrageTransactionRemarkType();
    }

    /**
     * Create an instance of {@link TransmissionReportQueryType }
     * 
     */
    public TransmissionReportQueryType createTransmissionReportQueryType() {
        return new TransmissionReportQueryType();
    }

    /**
     * Create an instance of {@link ExportImportLicenseType }
     * 
     */
    public ExportImportLicenseType createExportImportLicenseType() {
        return new ExportImportLicenseType();
    }

    /**
     * Create an instance of {@link SkuQuantityAssetType }
     * 
     */
    public SkuQuantityAssetType createSkuQuantityAssetType() {
        return new SkuQuantityAssetType();
    }

    /**
     * Create an instance of {@link PackagedItemRefType }
     * 
     */
    public PackagedItemRefType createPackagedItemRefType() {
        return new PackagedItemRefType();
    }

    /**
     * Create an instance of {@link AmountWithBaseType }
     * 
     */
    public AmountWithBaseType createAmountWithBaseType() {
        return new AmountWithBaseType();
    }

    /**
     * Create an instance of {@link GLogXMLWeightVolumeType }
     * 
     */
    public GLogXMLWeightVolumeType createGLogXMLWeightVolumeType() {
        return new GLogXMLWeightVolumeType();
    }

    /**
     * Create an instance of {@link TextType }
     * 
     */
    public TextType createTextType() {
        return new TextType();
    }

    /**
     * Create an instance of {@link ShipmentCostType }
     * 
     */
    public ShipmentCostType createShipmentCostType() {
        return new ShipmentCostType();
    }

    /**
     * Create an instance of {@link AppointmentType }
     * 
     */
    public AppointmentType createAppointmentType() {
        return new AppointmentType();
    }

    /**
     * Create an instance of {@link ItineraryDetailType }
     * 
     */
    public ItineraryDetailType createItineraryDetailType() {
        return new ItineraryDetailType();
    }

    /**
     * Create an instance of {@link ProcessInfoType }
     * 
     */
    public ProcessInfoType createProcessInfoType() {
        return new ProcessInfoType();
    }

    /**
     * Create an instance of {@link GLogXMLTemperatureType }
     * 
     */
    public GLogXMLTemperatureType createGLogXMLTemperatureType() {
        return new GLogXMLTemperatureType();
    }

    /**
     * Create an instance of {@link SpecialServiceType }
     * 
     */
    public SpecialServiceType createSpecialServiceType() {
        return new SpecialServiceType();
    }

    /**
     * Create an instance of {@link ShipUnitCriteriaType }
     * 
     */
    public ShipUnitCriteriaType createShipUnitCriteriaType() {
        return new ShipUnitCriteriaType();
    }

    /**
     * Create an instance of {@link RIQRouteShipmentType }
     * 
     */
    public RIQRouteShipmentType createRIQRouteShipmentType() {
        return new RIQRouteShipmentType();
    }

    /**
     * Create an instance of {@link MatchObjInvolvedPartyType }
     * 
     */
    public MatchObjInvolvedPartyType createMatchObjInvolvedPartyType() {
        return new MatchObjInvolvedPartyType();
    }

    /**
     * Create an instance of {@link EventTimeType }
     * 
     */
    public EventTimeType createEventTimeType() {
        return new EventTimeType();
    }

    /**
     * Create an instance of {@link TendRespSEquipmentType }
     * 
     */
    public TendRespSEquipmentType createTendRespSEquipmentType() {
        return new TendRespSEquipmentType();
    }

    /**
     * Create an instance of {@link OrStopType }
     * 
     */
    public OrStopType createOrStopType() {
        return new OrStopType();
    }

    /**
     * Create an instance of {@link EquipMarksType }
     * 
     */
    public EquipMarksType createEquipMarksType() {
        return new EquipMarksType();
    }

    /**
     * Create an instance of {@link FreightRateType }
     * 
     */
    public FreightRateType createFreightRateType() {
        return new FreightRateType();
    }

    /**
     * Create an instance of {@link PkgItemEquipRefUnitType }
     * 
     */
    public PkgItemEquipRefUnitType createPkgItemEquipRefUnitType() {
        return new PkgItemEquipRefUnitType();
    }

    /**
     * Create an instance of {@link HazardousClassType }
     * 
     */
    public HazardousClassType createHazardousClassType() {
        return new HazardousClassType();
    }

    /**
     * Create an instance of {@link BulkPlanPartitionType }
     * 
     */
    public BulkPlanPartitionType createBulkPlanPartitionType() {
        return new BulkPlanPartitionType();
    }

    /**
     * Create an instance of {@link FlexFieldDateType }
     * 
     */
    public FlexFieldDateType createFlexFieldDateType() {
        return new FlexFieldDateType();
    }

    /**
     * Create an instance of {@link GLogXMLContactRefType }
     * 
     */
    public GLogXMLContactRefType createGLogXMLContactRefType() {
        return new GLogXMLContactRefType();
    }

    /**
     * Create an instance of {@link SStatusSEquipmentType }
     * 
     */
    public SStatusSEquipmentType createSStatusSEquipmentType() {
        return new SStatusSEquipmentType();
    }

    /**
     * Create an instance of {@link HOSRuleState }
     * 
     */
    public HOSRuleState createHOSRuleState() {
        return new HOSRuleState();
    }

    /**
     * Create an instance of {@link VatRegistrationType }
     * 
     */
    public VatRegistrationType createVatRegistrationType() {
        return new VatRegistrationType();
    }

    /**
     * Create an instance of {@link MotorEquipmentType }
     * 
     */
    public MotorEquipmentType createMotorEquipmentType() {
        return new MotorEquipmentType();
    }

    /**
     * Create an instance of {@link WidthType }
     * 
     */
    public WidthType createWidthType() {
        return new WidthType();
    }

    /**
     * Create an instance of {@link JobInvolvedPartyType }
     * 
     */
    public JobInvolvedPartyType createJobInvolvedPartyType() {
        return new JobInvolvedPartyType();
    }

    /**
     * Create an instance of {@link IeDocumentResponseType }
     * 
     */
    public IeDocumentResponseType createIeDocumentResponseType() {
        return new IeDocumentResponseType();
    }

    /**
     * Create an instance of {@link PricePerUnitType }
     * 
     */
    public PricePerUnitType createPricePerUnitType() {
        return new PricePerUnitType();
    }

    /**
     * Create an instance of {@link GenericDetailType }
     * 
     */
    public GenericDetailType createGenericDetailType() {
        return new GenericDetailType();
    }

    /**
     * Create an instance of {@link RailDetailType }
     * 
     */
    public RailDetailType createRailDetailType() {
        return new RailDetailType();
    }

    /**
     * Create an instance of {@link SkuCostType }
     * 
     */
    public SkuCostType createSkuCostType() {
        return new SkuCostType();
    }

    /**
     * Create an instance of {@link PreferenceType }
     * 
     */
    public PreferenceType createPreferenceType() {
        return new PreferenceType();
    }

    /**
     * Create an instance of {@link LoadConfigSetupOrientationType }
     * 
     */
    public LoadConfigSetupOrientationType createLoadConfigSetupOrientationType() {
        return new LoadConfigSetupOrientationType();
    }

    /**
     * Create an instance of {@link FPTariffType }
     * 
     */
    public FPTariffType createFPTariffType() {
        return new FPTariffType();
    }

    /**
     * Create an instance of {@link ReleaseAllocShipmentType }
     * 
     */
    public ReleaseAllocShipmentType createReleaseAllocShipmentType() {
        return new ReleaseAllocShipmentType();
    }

    /**
     * Create an instance of {@link MasterBLConsigneeInfoType }
     * 
     */
    public MasterBLConsigneeInfoType createMasterBLConsigneeInfoType() {
        return new MasterBLConsigneeInfoType();
    }

    /**
     * Create an instance of {@link InnerPackInfoType }
     * 
     */
    public InnerPackInfoType createInnerPackInfoType() {
        return new InnerPackInfoType();
    }

    /**
     * Create an instance of {@link ShipmentRefnumType }
     * 
     */
    public ShipmentRefnumType createShipmentRefnumType() {
        return new ShipmentRefnumType();
    }

    /**
     * Create an instance of {@link RIQResultRouteType }
     * 
     */
    public RIQResultRouteType createRIQResultRouteType() {
        return new RIQResultRouteType();
    }

    /**
     * Create an instance of {@link SEquipmentType }
     * 
     */
    public SEquipmentType createSEquipmentType() {
        return new SEquipmentType();
    }

    /**
     * Create an instance of {@link TagInfoType }
     * 
     */
    public TagInfoType createTagInfoType() {
        return new TagInfoType();
    }

    /**
     * Create an instance of {@link HeightType }
     * 
     */
    public HeightType createHeightType() {
        return new HeightType();
    }

    /**
     * Create an instance of {@link XLaneRefType }
     * 
     */
    public XLaneRefType createXLaneRefType() {
        return new XLaneRefType();
    }

    /**
     * Create an instance of {@link GenericLineItemType }
     * 
     */
    public GenericLineItemType createGenericLineItemType() {
        return new GenericLineItemType();
    }

    /**
     * Create an instance of {@link GLogXMLLocRoleType }
     * 
     */
    public GLogXMLLocRoleType createGLogXMLLocRoleType() {
        return new GLogXMLLocRoleType();
    }

    /**
     * Create an instance of {@link TransOrderLineDetailType }
     * 
     */
    public TransOrderLineDetailType createTransOrderLineDetailType() {
        return new TransOrderLineDetailType();
    }

    /**
     * Create an instance of {@link CostPerWeightVolumeUOMType }
     * 
     */
    public CostPerWeightVolumeUOMType createCostPerWeightVolumeUOMType() {
        return new CostPerWeightVolumeUOMType();
    }

    /**
     * Create an instance of {@link MileageAddressType }
     * 
     */
    public MileageAddressType createMileageAddressType() {
        return new MileageAddressType();
    }

    /**
     * Create an instance of {@link ReleaseLineHazmatInfoType }
     * 
     */
    public ReleaseLineHazmatInfoType createReleaseLineHazmatInfoType() {
        return new ReleaseLineHazmatInfoType();
    }

    /**
     * Create an instance of {@link GLogXMLDistanceType }
     * 
     */
    public GLogXMLDistanceType createGLogXMLDistanceType() {
        return new GLogXMLDistanceType();
    }

    /**
     * Create an instance of {@link LocationRoleType }
     * 
     */
    public LocationRoleType createLocationRoleType() {
        return new LocationRoleType();
    }

    /**
     * Create an instance of {@link TendRespAcceptedSUType }
     * 
     */
    public TendRespAcceptedSUType createTendRespAcceptedSUType() {
        return new TendRespAcceptedSUType();
    }

    /**
     * Create an instance of {@link SEquipmentSealType }
     * 
     */
    public SEquipmentSealType createSEquipmentSealType() {
        return new SEquipmentSealType();
    }

    /**
     * Create an instance of {@link RemoteQueryStatusType }
     * 
     */
    public RemoteQueryStatusType createRemoteQueryStatusType() {
        return new RemoteQueryStatusType();
    }

    /**
     * Create an instance of {@link PaymentModeDetailType }
     * 
     */
    public PaymentModeDetailType createPaymentModeDetailType() {
        return new PaymentModeDetailType();
    }

    /**
     * Create an instance of {@link TransmissionReportQueryReplyType }
     * 
     */
    public TransmissionReportQueryReplyType createTransmissionReportQueryReplyType() {
        return new TransmissionReportQueryReplyType();
    }

    /**
     * Create an instance of {@link TextTemplateType }
     * 
     */
    public TextTemplateType createTextTemplateType() {
        return new TextTemplateType();
    }

    /**
     * Create an instance of {@link TextItemType }
     * 
     */
    public TextItemType createTextItemType() {
        return new TextItemType();
    }

    /**
     * Create an instance of {@link LineItemVatCostRefType }
     * 
     */
    public LineItemVatCostRefType createLineItemVatCostRefType() {
        return new LineItemVatCostRefType();
    }

    /**
     * Create an instance of {@link ShipUnitViewByReleaseType }
     * 
     */
    public ShipUnitViewByReleaseType createShipUnitViewByReleaseType() {
        return new ShipUnitViewByReleaseType();
    }

    /**
     * Create an instance of {@link ProvinceVatRegType }
     * 
     */
    public ProvinceVatRegType createProvinceVatRegType() {
        return new ProvinceVatRegType();
    }

    /**
     * Create an instance of {@link ShipUnitPieceType }
     * 
     */
    public ShipUnitPieceType createShipUnitPieceType() {
        return new ShipUnitPieceType();
    }

    /**
     * Create an instance of {@link ShipmentGroupHeaderType }
     * 
     */
    public ShipmentGroupHeaderType createShipmentGroupHeaderType() {
        return new ShipmentGroupHeaderType();
    }

    /**
     * Create an instance of {@link ORLineRefnumType }
     * 
     */
    public ORLineRefnumType createORLineRefnumType() {
        return new ORLineRefnumType();
    }

    /**
     * Create an instance of {@link DeviceObjectType }
     * 
     */
    public DeviceObjectType createDeviceObjectType() {
        return new DeviceObjectType();
    }

    /**
     * Create an instance of {@link DiscountType }
     * 
     */
    public DiscountType createDiscountType() {
        return new DiscountType();
    }

    /**
     * Create an instance of {@link ParentInvoiceType }
     * 
     */
    public ParentInvoiceType createParentInvoiceType() {
        return new ParentInvoiceType();
    }

    /**
     * Create an instance of {@link SSLocationType }
     * 
     */
    public SSLocationType createSSLocationType() {
        return new SSLocationType();
    }

    /**
     * Create an instance of {@link ShipmentModViaOrderLineType }
     * 
     */
    public ShipmentModViaOrderLineType createShipmentModViaOrderLineType() {
        return new ShipmentModViaOrderLineType();
    }

    /**
     * Create an instance of {@link ProcessGroupingType }
     * 
     */
    public ProcessGroupingType createProcessGroupingType() {
        return new ProcessGroupingType();
    }

    /**
     * Create an instance of {@link WeightVolumeType }
     * 
     */
    public WeightVolumeType createWeightVolumeType() {
        return new WeightVolumeType();
    }

    /**
     * Create an instance of {@link SecondaryChargeReferenceType }
     * 
     */
    public SecondaryChargeReferenceType createSecondaryChargeReferenceType() {
        return new SecondaryChargeReferenceType();
    }

    /**
     * Create an instance of {@link ShipmentCostDetailType }
     * 
     */
    public ShipmentCostDetailType createShipmentCostDetailType() {
        return new ShipmentCostDetailType();
    }

    /**
     * Create an instance of {@link DocumentContentType }
     * 
     */
    public DocumentContentType createDocumentContentType() {
        return new DocumentContentType();
    }

    /**
     * Create an instance of {@link AckSpecType }
     * 
     */
    public AckSpecType createAckSpecType() {
        return new AckSpecType();
    }

    /**
     * Create an instance of {@link CostOptionShipCostType }
     * 
     */
    public CostOptionShipCostType createCostOptionShipCostType() {
        return new CostOptionShipCostType();
    }

    /**
     * Create an instance of {@link PackagingType }
     * 
     */
    public PackagingType createPackagingType() {
        return new PackagingType();
    }

    /**
     * Create an instance of {@link DriverSpecialServiceType }
     * 
     */
    public DriverSpecialServiceType createDriverSpecialServiceType() {
        return new DriverSpecialServiceType();
    }

    /**
     * Create an instance of {@link PartialShipmentType }
     * 
     */
    public PartialShipmentType createPartialShipmentType() {
        return new PartialShipmentType();
    }

    /**
     * Create an instance of {@link REquipmentType }
     * 
     */
    public REquipmentType createREquipmentType() {
        return new REquipmentType();
    }

    /**
     * Create an instance of {@link ShipUnitRelInstInfoType }
     * 
     */
    public ShipUnitRelInstInfoType createShipUnitRelInstInfoType() {
        return new ShipUnitRelInstInfoType();
    }

    /**
     * Create an instance of {@link DriverRefnumType }
     * 
     */
    public DriverRefnumType createDriverRefnumType() {
        return new DriverRefnumType();
    }

    /**
     * Create an instance of {@link TextTemplateDocumentDefType }
     * 
     */
    public TextTemplateDocumentDefType createTextTemplateDocumentDefType() {
        return new TextTemplateDocumentDefType();
    }

    /**
     * Create an instance of {@link RailLineItemType }
     * 
     */
    public RailLineItemType createRailLineItemType() {
        return new RailLineItemType();
    }

    /**
     * Create an instance of {@link TemperatureType }
     * 
     */
    public TemperatureType createTemperatureType() {
        return new TemperatureType();
    }

    /**
     * Create an instance of {@link ShipSpclServiceRefType }
     * 
     */
    public ShipSpclServiceRefType createShipSpclServiceRefType() {
        return new ShipSpclServiceRefType();
    }

    /**
     * Create an instance of {@link StopRefnumType }
     * 
     */
    public StopRefnumType createStopRefnumType() {
        return new StopRefnumType();
    }

    /**
     * Create an instance of {@link LineItemVatType }
     * 
     */
    public LineItemVatType createLineItemVatType() {
        return new LineItemVatType();
    }

    /**
     * Create an instance of {@link DriverAssignmentType }
     * 
     */
    public DriverAssignmentType createDriverAssignmentType() {
        return new DriverAssignmentType();
    }

    /**
     * Create an instance of {@link MarksType }
     * 
     */
    public MarksType createMarksType() {
        return new MarksType();
    }

    /**
     * Create an instance of {@link CommercialTermsType }
     * 
     */
    public CommercialTermsType createCommercialTermsType() {
        return new CommercialTermsType();
    }

    /**
     * Create an instance of {@link DriverPowerUnitType }
     * 
     */
    public DriverPowerUnitType createDriverPowerUnitType() {
        return new DriverPowerUnitType();
    }

    /**
     * Create an instance of {@link GLogXMLRouteCodeType }
     * 
     */
    public GLogXMLRouteCodeType createGLogXMLRouteCodeType() {
        return new GLogXMLRouteCodeType();
    }

    /**
     * Create an instance of {@link EquipmentAttributeType }
     * 
     */
    public EquipmentAttributeType createEquipmentAttributeType() {
        return new EquipmentAttributeType();
    }

    /**
     * Create an instance of {@link PasswordType }
     * 
     */
    public PasswordType createPasswordType() {
        return new PasswordType();
    }

    /**
     * Create an instance of {@link ShipmentQueryType }
     * 
     */
    public ShipmentQueryType createShipmentQueryType() {
        return new ShipmentQueryType();
    }

    /**
     * Create an instance of {@link GLogXMLXLaneNodeType }
     * 
     */
    public GLogXMLXLaneNodeType createGLogXMLXLaneNodeType() {
        return new GLogXMLXLaneNodeType();
    }

    /**
     * Create an instance of {@link MotorDetailType }
     * 
     */
    public MotorDetailType createMotorDetailType() {
        return new MotorDetailType();
    }

    /**
     * Create an instance of {@link ReleaseAllocShipmentDetailType }
     * 
     */
    public ReleaseAllocShipmentDetailType createReleaseAllocShipmentDetailType() {
        return new ReleaseAllocShipmentDetailType();
    }

    /**
     * Create an instance of {@link MasterBLNotifyInfoType }
     * 
     */
    public MasterBLNotifyInfoType createMasterBLNotifyInfoType() {
        return new MasterBLNotifyInfoType();
    }

    /**
     * Create an instance of {@link CommunicationMethodType }
     * 
     */
    public CommunicationMethodType createCommunicationMethodType() {
        return new CommunicationMethodType();
    }

    /**
     * Create an instance of {@link ShipUnitLineRefnumType }
     * 
     */
    public ShipUnitLineRefnumType createShipUnitLineRefnumType() {
        return new ShipUnitLineRefnumType();
    }

    /**
     * Create an instance of {@link FlightType }
     * 
     */
    public FlightType createFlightType() {
        return new FlightType();
    }

    /**
     * Create an instance of {@link InterimFlightInstanceRowType }
     * 
     */
    public InterimFlightInstanceRowType createInterimFlightInstanceRowType() {
        return new InterimFlightInstanceRowType();
    }

    /**
     * Create an instance of {@link CurrencyExchangeRateType }
     * 
     */
    public CurrencyExchangeRateType createCurrencyExchangeRateType() {
        return new CurrencyExchangeRateType();
    }

    /**
     * Create an instance of {@link StationType }
     * 
     */
    public StationType createStationType() {
        return new StationType();
    }

    /**
     * Create an instance of {@link OceanLineItemType }
     * 
     */
    public OceanLineItemType createOceanLineItemType() {
        return new OceanLineItemType();
    }

    /**
     * Create an instance of {@link ShipModViaOrderLineMatchType }
     * 
     */
    public ShipModViaOrderLineMatchType createShipModViaOrderLineMatchType() {
        return new ShipModViaOrderLineMatchType();
    }

    /**
     * Create an instance of {@link XLaneNodeType }
     * 
     */
    public XLaneNodeType createXLaneNodeType() {
        return new XLaneNodeType();
    }

    /**
     * Create an instance of {@link ShipmentStopDetailType }
     * 
     */
    public ShipmentStopDetailType createShipmentStopDetailType() {
        return new ShipmentStopDetailType();
    }

    /**
     * Create an instance of {@link ApptRuleSetType }
     * 
     */
    public ApptRuleSetType createApptRuleSetType() {
        return new ApptRuleSetType();
    }

    /**
     * Create an instance of {@link ContainerGroupDetailType }
     * 
     */
    public ContainerGroupDetailType createContainerGroupDetailType() {
        return new ContainerGroupDetailType();
    }

    /**
     * Create an instance of {@link TariffRefnumType }
     * 
     */
    public TariffRefnumType createTariffRefnumType() {
        return new TariffRefnumType();
    }

    /**
     * Create an instance of {@link GLogDateTimeType }
     * 
     */
    public GLogDateTimeType createGLogDateTimeType() {
        return new GLogDateTimeType();
    }

    /**
     * Create an instance of {@link SSEquipmentType }
     * 
     */
    public SSEquipmentType createSSEquipmentType() {
        return new SSEquipmentType();
    }

    /**
     * Create an instance of {@link ImportLicenseInfoType }
     * 
     */
    public ImportLicenseInfoType createImportLicenseInfoType() {
        return new ImportLicenseInfoType();
    }

    /**
     * Create an instance of {@link Rule11SecShipRateGeoType }
     * 
     */
    public Rule11SecShipRateGeoType createRule11SecShipRateGeoType() {
        return new Rule11SecShipRateGeoType();
    }

    /**
     * Create an instance of {@link ShipmentModViaOrderSUType }
     * 
     */
    public ShipmentModViaOrderSUType createShipmentModViaOrderSUType() {
        return new ShipmentModViaOrderSUType();
    }

    /**
     * Create an instance of {@link ItemQuantityType }
     * 
     */
    public ItemQuantityType createItemQuantityType() {
        return new ItemQuantityType();
    }

    /**
     * Create an instance of {@link BookingInfoType }
     * 
     */
    public BookingInfoType createBookingInfoType() {
        return new BookingInfoType();
    }

    /**
     * Create an instance of {@link ItemFeatureType }
     * 
     */
    public ItemFeatureType createItemFeatureType() {
        return new ItemFeatureType();
    }

    /**
     * Create an instance of {@link SpclServiceType }
     * 
     */
    public SpclServiceType createSpclServiceType() {
        return new SpclServiceType();
    }

    /**
     * Create an instance of {@link SendReasonType }
     * 
     */
    public SendReasonType createSendReasonType() {
        return new SendReasonType();
    }

    /**
     * Create an instance of {@link RIQQueryReplyType }
     * 
     */
    public RIQQueryReplyType createRIQQueryReplyType() {
        return new RIQQueryReplyType();
    }

    /**
     * Create an instance of {@link RailReturnRouteGidType }
     * 
     */
    public RailReturnRouteGidType createRailReturnRouteGidType() {
        return new RailReturnRouteGidType();
    }

    /**
     * Create an instance of {@link ClaimCostType }
     * 
     */
    public ClaimCostType createClaimCostType() {
        return new ClaimCostType();
    }

    /**
     * Create an instance of {@link ReasonGroupType }
     * 
     */
    public ReasonGroupType createReasonGroupType() {
        return new ReasonGroupType();
    }

    /**
     * Create an instance of {@link PaymentShipmentType }
     * 
     */
    public PaymentShipmentType createPaymentShipmentType() {
        return new PaymentShipmentType();
    }

    /**
     * Create an instance of {@link PackagedItemType }
     * 
     */
    public PackagedItemType createPackagedItemType() {
        return new PackagedItemType();
    }

    /**
     * Create an instance of {@link LengthType }
     * 
     */
    public LengthType createLengthType() {
        return new LengthType();
    }

    /**
     * Create an instance of {@link GLogXMLWidthType }
     * 
     */
    public GLogXMLWidthType createGLogXMLWidthType() {
        return new GLogXMLWidthType();
    }

    /**
     * Create an instance of {@link OrStopSpecialServiceType }
     * 
     */
    public OrStopSpecialServiceType createOrStopSpecialServiceType() {
        return new OrStopSpecialServiceType();
    }

    /**
     * Create an instance of {@link ReleaseServiceType }
     * 
     */
    public ReleaseServiceType createReleaseServiceType() {
        return new ReleaseServiceType();
    }

    /**
     * Create an instance of {@link OrderLegConstraintType }
     * 
     */
    public OrderLegConstraintType createOrderLegConstraintType() {
        return new OrderLegConstraintType();
    }

    /**
     * Create an instance of {@link VesselInfoType }
     * 
     */
    public VesselInfoType createVesselInfoType() {
        return new VesselInfoType();
    }

    /**
     * Create an instance of {@link IntegrationLogMessageType }
     * 
     */
    public IntegrationLogMessageType createIntegrationLogMessageType() {
        return new IntegrationLogMessageType();
    }

    /**
     * Create an instance of {@link LocationResourceTypeType }
     * 
     */
    public LocationResourceTypeType createLocationResourceTypeType() {
        return new LocationResourceTypeType();
    }

    /**
     * Create an instance of {@link RegionalHandlingTimeType }
     * 
     */
    public RegionalHandlingTimeType createRegionalHandlingTimeType() {
        return new RegionalHandlingTimeType();
    }

    /**
     * Create an instance of {@link CommodityProtectiveServiceType }
     * 
     */
    public CommodityProtectiveServiceType createCommodityProtectiveServiceType() {
        return new CommodityProtectiveServiceType();
    }

    /**
     * Create an instance of {@link WorkInvoiceActivityType }
     * 
     */
    public WorkInvoiceActivityType createWorkInvoiceActivityType() {
        return new WorkInvoiceActivityType();
    }

    /**
     * Create an instance of {@link PrimaryShipmentRefInfoTYpe }
     * 
     */
    public PrimaryShipmentRefInfoTYpe createPrimaryShipmentRefInfoTYpe() {
        return new PrimaryShipmentRefInfoTYpe();
    }

    /**
     * Create an instance of {@link DeclaredValueType }
     * 
     */
    public DeclaredValueType createDeclaredValueType() {
        return new DeclaredValueType();
    }

    /**
     * Create an instance of {@link ItemRefType }
     * 
     */
    public ItemRefType createItemRefType() {
        return new ItemRefType();
    }

    /**
     * Create an instance of {@link SStatusShipUnitContentType }
     * 
     */
    public SStatusShipUnitContentType createSStatusShipUnitContentType() {
        return new SStatusShipUnitContentType();
    }

    /**
     * Create an instance of {@link PaymentDetailType }
     * 
     */
    public PaymentDetailType createPaymentDetailType() {
        return new PaymentDetailType();
    }

    /**
     * Create an instance of {@link ShipUnitSpecRefType }
     * 
     */
    public ShipUnitSpecRefType createShipUnitSpecRefType() {
        return new ShipUnitSpecRefType();
    }

    /**
     * Create an instance of {@link OrderLineRefnumType }
     * 
     */
    public OrderLineRefnumType createOrderLineRefnumType() {
        return new OrderLineRefnumType();
    }

    /**
     * Create an instance of {@link ReplaceChildrenType }
     * 
     */
    public ReplaceChildrenType createReplaceChildrenType() {
        return new ReplaceChildrenType();
    }

    /**
     * Create an instance of {@link ChildPaymentsType }
     * 
     */
    public ChildPaymentsType createChildPaymentsType() {
        return new ChildPaymentsType();
    }

    /**
     * Create an instance of {@link AccessorialType }
     * 
     */
    public AccessorialType createAccessorialType() {
        return new AccessorialType();
    }

    /**
     * Create an instance of {@link FlexQuantityType }
     * 
     */
    public FlexQuantityType createFlexQuantityType() {
        return new FlexQuantityType();
    }

    /**
     * Create an instance of {@link BookLineAmendDtlContGroupType }
     * 
     */
    public BookLineAmendDtlContGroupType createBookLineAmendDtlContGroupType() {
        return new BookLineAmendDtlContGroupType();
    }

    /**
     * Create an instance of {@link ShipmentType }
     * 
     */
    public ShipmentType createShipmentType() {
        return new ShipmentType();
    }

    /**
     * Create an instance of {@link AppointmentInfoType }
     * 
     */
    public AppointmentInfoType createAppointmentInfoType() {
        return new AppointmentInfoType();
    }

    /**
     * Create an instance of {@link DocumentContentParamType }
     * 
     */
    public DocumentContentParamType createDocumentContentParamType() {
        return new DocumentContentParamType();
    }

    /**
     * Create an instance of {@link LetterOfCreditInfoType }
     * 
     */
    public LetterOfCreditInfoType createLetterOfCreditInfoType() {
        return new LetterOfCreditInfoType();
    }

    /**
     * Create an instance of {@link AddressLinesType }
     * 
     */
    public AddressLinesType createAddressLinesType() {
        return new AddressLinesType();
    }

    /**
     * Create an instance of {@link TransOrderHeaderType }
     * 
     */
    public TransOrderHeaderType createTransOrderHeaderType() {
        return new TransOrderHeaderType();
    }

    /**
     * Create an instance of {@link GLogXMLShipUnitSpecRefType }
     * 
     */
    public GLogXMLShipUnitSpecRefType createGLogXMLShipUnitSpecRefType() {
        return new GLogXMLShipUnitSpecRefType();
    }

    /**
     * Create an instance of {@link ShipUnitLoadingSplitType }
     * 
     */
    public ShipUnitLoadingSplitType createShipUnitLoadingSplitType() {
        return new ShipUnitLoadingSplitType();
    }

    /**
     * Create an instance of {@link ShipUnitContentType }
     * 
     */
    public ShipUnitContentType createShipUnitContentType() {
        return new ShipUnitContentType();
    }

    /**
     * Create an instance of {@link LengthWidthHeightType }
     * 
     */
    public LengthWidthHeightType createLengthWidthHeightType() {
        return new LengthWidthHeightType();
    }

    /**
     * Create an instance of {@link LegInterimPointType }
     * 
     */
    public LegInterimPointType createLegInterimPointType() {
        return new LegInterimPointType();
    }

    /**
     * Create an instance of {@link LocationActivityTimeDefType }
     * 
     */
    public LocationActivityTimeDefType createLocationActivityTimeDefType() {
        return new LocationActivityTimeDefType();
    }

    /**
     * Create an instance of {@link QuoteEquipmentGroupType }
     * 
     */
    public QuoteEquipmentGroupType createQuoteEquipmentGroupType() {
        return new QuoteEquipmentGroupType();
    }

    /**
     * Create an instance of {@link AccrualReleaseInfoType }
     * 
     */
    public AccrualReleaseInfoType createAccrualReleaseInfoType() {
        return new AccrualReleaseInfoType();
    }

    /**
     * Create an instance of {@link EquipmentSealType }
     * 
     */
    public EquipmentSealType createEquipmentSealType() {
        return new EquipmentSealType();
    }

    /**
     * Create an instance of {@link LoadConfigSetupType }
     * 
     */
    public LoadConfigSetupType createLoadConfigSetupType() {
        return new LoadConfigSetupType();
    }

    /**
     * Create an instance of {@link ReleaseHeaderType }
     * 
     */
    public ReleaseHeaderType createReleaseHeaderType() {
        return new ReleaseHeaderType();
    }

    /**
     * Create an instance of {@link FullShipmentType }
     * 
     */
    public FullShipmentType createFullShipmentType() {
        return new FullShipmentType();
    }

    /**
     * Create an instance of {@link RemarkType }
     * 
     */
    public RemarkType createRemarkType() {
        return new RemarkType();
    }

    /**
     * Create an instance of {@link ShipModViaOrderSUMatchType }
     * 
     */
    public ShipModViaOrderSUMatchType createShipModViaOrderSUMatchType() {
        return new ShipModViaOrderSUMatchType();
    }

    /**
     * Create an instance of {@link DemurrageTransactionRefnumType }
     * 
     */
    public DemurrageTransactionRefnumType createDemurrageTransactionRefnumType() {
        return new DemurrageTransactionRefnumType();
    }

    /**
     * Create an instance of {@link GtmItemClassificationType }
     * 
     */
    public GtmItemClassificationType createGtmItemClassificationType() {
        return new GtmItemClassificationType();
    }

    /**
     * Create an instance of {@link GLogXMLLocProfileType }
     * 
     */
    public GLogXMLLocProfileType createGLogXMLLocProfileType() {
        return new GLogXMLLocProfileType();
    }

    /**
     * Create an instance of {@link CurrencyCodeType }
     * 
     */
    public CurrencyCodeType createCurrencyCodeType() {
        return new CurrencyCodeType();
    }

    /**
     * Create an instance of {@link VolumeType }
     * 
     */
    public VolumeType createVolumeType() {
        return new VolumeType();
    }

    /**
     * Create an instance of {@link FinancialAmountType }
     * 
     */
    public FinancialAmountType createFinancialAmountType() {
        return new FinancialAmountType();
    }

    /**
     * Create an instance of {@link DemurrageTransactionNoteType }
     * 
     */
    public DemurrageTransactionNoteType createDemurrageTransactionNoteType() {
        return new DemurrageTransactionNoteType();
    }

    /**
     * Create an instance of {@link TimeWindowSecType }
     * 
     */
    public TimeWindowSecType createTimeWindowSecType() {
        return new TimeWindowSecType();
    }

    /**
     * Create an instance of {@link ReleaseSpecialServiceType }
     * 
     */
    public ReleaseSpecialServiceType createReleaseSpecialServiceType() {
        return new ReleaseSpecialServiceType();
    }

    /**
     * Create an instance of {@link ServprovEquipmentType }
     * 
     */
    public ServprovEquipmentType createServprovEquipmentType() {
        return new ServprovEquipmentType();
    }

    /**
     * Create an instance of {@link LocationServProvPrefDetailType }
     * 
     */
    public LocationServProvPrefDetailType createLocationServProvPrefDetailType() {
        return new LocationServProvPrefDetailType();
    }

    /**
     * Create an instance of {@link StatusType }
     * 
     */
    public StatusType createStatusType() {
        return new StatusType();
    }

    /**
     * Create an instance of {@link PowerUnitRefnumType }
     * 
     */
    public PowerUnitRefnumType createPowerUnitRefnumType() {
        return new PowerUnitRefnumType();
    }

    /**
     * Create an instance of {@link ExportLicenseType }
     * 
     */
    public ExportLicenseType createExportLicenseType() {
        return new ExportLicenseType();
    }

    /**
     * Create an instance of {@link SecondaryChargesType }
     * 
     */
    public SecondaryChargesType createSecondaryChargesType() {
        return new SecondaryChargesType();
    }

    /**
     * Create an instance of {@link ContainerGroupType }
     * 
     */
    public ContainerGroupType createContainerGroupType() {
        return new ContainerGroupType();
    }

    /**
     * Create an instance of {@link GtmContactType }
     * 
     */
    public GtmContactType createGtmContactType() {
        return new GtmContactType();
    }

    /**
     * Create an instance of {@link GtmTransactionType }
     * 
     */
    public GtmTransactionType createGtmTransactionType() {
        return new GtmTransactionType();
    }

    /**
     * Create an instance of {@link GtmTransactionLineType }
     * 
     */
    public GtmTransactionLineType createGtmTransactionLineType() {
        return new GtmTransactionLineType();
    }

    /**
     * Create an instance of {@link ServiceResponseType }
     * 
     */
    public ServiceResponseType createServiceResponseType() {
        return new ServiceResponseType();
    }

    /**
     * Create an instance of {@link GtmDeclarationMessageType }
     * 
     */
    public GtmDeclarationMessageType createGtmDeclarationMessageType() {
        return new GtmDeclarationMessageType();
    }

    /**
     * Create an instance of {@link GtmStructureType }
     * 
     */
    public GtmStructureType createGtmStructureType() {
        return new GtmStructureType();
    }

    /**
     * Create an instance of {@link GtmRegistrationType }
     * 
     */
    public GtmRegistrationType createGtmRegistrationType() {
        return new GtmRegistrationType();
    }

    /**
     * Create an instance of {@link GtmDeclarationType }
     * 
     */
    public GtmDeclarationType createGtmDeclarationType() {
        return new GtmDeclarationType();
    }

    /**
     * Create an instance of {@link ServiceRequestType }
     * 
     */
    public ServiceRequestType createServiceRequestType() {
        return new ServiceRequestType();
    }

    /**
     * Create an instance of {@link GtmBondType }
     * 
     */
    public GtmBondType createGtmBondType() {
        return new GtmBondType();
    }

    /**
     * Create an instance of {@link ProcedureDetailType }
     * 
     */
    public ProcedureDetailType createProcedureDetailType() {
        return new ProcedureDetailType();
    }

    /**
     * Create an instance of {@link ProcedureType }
     * 
     */
    public ProcedureType createProcedureType() {
        return new ProcedureType();
    }

    /**
     * Create an instance of {@link ClassCodeType }
     * 
     */
    public ClassCodeType createClassCodeType() {
        return new ClassCodeType();
    }

    /**
     * Create an instance of {@link UserDefinedCodeType }
     * 
     */
    public UserDefinedCodeType createUserDefinedCodeType() {
        return new UserDefinedCodeType();
    }

    /**
     * Create an instance of {@link GtmRegistrationDateType }
     * 
     */
    public GtmRegistrationDateType createGtmRegistrationDateType() {
        return new GtmRegistrationDateType();
    }

    /**
     * Create an instance of {@link RegionInfoType }
     * 
     */
    public RegionInfoType createRegionInfoType() {
        return new RegionInfoType();
    }

    /**
     * Create an instance of {@link ReportingQuantityType }
     * 
     */
    public ReportingQuantityType createReportingQuantityType() {
        return new ReportingQuantityType();
    }

    /**
     * Create an instance of {@link ProducedDocumentType }
     * 
     */
    public ProducedDocumentType createProducedDocumentType() {
        return new ProducedDocumentType();
    }

    /**
     * Create an instance of {@link TerritoryInfoType }
     * 
     */
    public TerritoryInfoType createTerritoryInfoType() {
        return new TerritoryInfoType();
    }

    /**
     * Create an instance of {@link RegionCountryInfoType }
     * 
     */
    public RegionCountryInfoType createRegionCountryInfoType() {
        return new RegionCountryInfoType();
    }

    /**
     * Create an instance of {@link PortInfoType }
     * 
     */
    public PortInfoType createPortInfoType() {
        return new PortInfoType();
    }

    /**
     * Create an instance of {@link UserDefinedClassificationType }
     * 
     */
    public UserDefinedClassificationType createUserDefinedClassificationType() {
        return new UserDefinedClassificationType();
    }

    /**
     * Create an instance of {@link AttributeType }
     * 
     */
    public AttributeType createAttributeType() {
        return new AttributeType();
    }

    /**
     * Create an instance of {@link RestrictedPartyScreeningResultType }
     * 
     */
    public RestrictedPartyScreeningResultType createRestrictedPartyScreeningResultType() {
        return new RestrictedPartyScreeningResultType();
    }

    /**
     * Create an instance of {@link ProductInfoType }
     * 
     */
    public ProductInfoType createProductInfoType() {
        return new ProductInfoType();
    }

    /**
     * Create an instance of {@link ReportingResultType }
     * 
     */
    public ReportingResultType createReportingResultType() {
        return new ReportingResultType();
    }

    /**
     * Create an instance of {@link LocationInfoType }
     * 
     */
    public LocationInfoType createLocationInfoType() {
        return new LocationInfoType();
    }

    /**
     * Create an instance of {@link UserDefinedTypeType }
     * 
     */
    public UserDefinedTypeType createUserDefinedTypeType() {
        return new UserDefinedTypeType();
    }

    /**
     * Create an instance of {@link GtmRegRelatedCommodityType }
     * 
     */
    public GtmRegRelatedCommodityType createGtmRegRelatedCommodityType() {
        return new GtmRegRelatedCommodityType();
    }

    /**
     * Create an instance of {@link DeclarationTypeType }
     * 
     */
    public DeclarationTypeType createDeclarationTypeType() {
        return new DeclarationTypeType();
    }

    /**
     * Create an instance of {@link TariffPreferenceTypeType }
     * 
     */
    public TariffPreferenceTypeType createTariffPreferenceTypeType() {
        return new TariffPreferenceTypeType();
    }

    /**
     * Create an instance of {@link ComplianceRuleType }
     * 
     */
    public ComplianceRuleType createComplianceRuleType() {
        return new ComplianceRuleType();
    }

    /**
     * Create an instance of {@link RestrictedPartyResponseType }
     * 
     */
    public RestrictedPartyResponseType createRestrictedPartyResponseType() {
        return new RestrictedPartyResponseType();
    }

    /**
     * Create an instance of {@link ProductClassificationType }
     * 
     */
    public ProductClassificationType createProductClassificationType() {
        return new ProductClassificationType();
    }

    /**
     * Create an instance of {@link GtmStructureComponentType }
     * 
     */
    public GtmStructureComponentType createGtmStructureComponentType() {
        return new GtmStructureComponentType();
    }

    /**
     * Create an instance of {@link GtmInvolvedPartyType }
     * 
     */
    public GtmInvolvedPartyType createGtmInvolvedPartyType() {
        return new GtmInvolvedPartyType();
    }

    /**
     * Create an instance of {@link LicenseDeterminationResponseType }
     * 
     */
    public LicenseDeterminationResponseType createLicenseDeterminationResponseType() {
        return new LicenseDeterminationResponseType();
    }

    /**
     * Create an instance of {@link CommentsType }
     * 
     */
    public CommentsType createCommentsType() {
        return new CommentsType();
    }

    /**
     * Create an instance of {@link TransactionPolicyType }
     * 
     */
    public TransactionPolicyType createTransactionPolicyType() {
        return new TransactionPolicyType();
    }

    /**
     * Create an instance of {@link GtmTransLineLicenseType }
     * 
     */
    public GtmTransLineLicenseType createGtmTransLineLicenseType() {
        return new GtmTransLineLicenseType();
    }

    /**
     * Create an instance of {@link GtmRegRelatedUDCommodityType }
     * 
     */
    public GtmRegRelatedUDCommodityType createGtmRegRelatedUDCommodityType() {
        return new GtmRegRelatedUDCommodityType();
    }

    /**
     * Create an instance of {@link ProcedureCategoryType }
     * 
     */
    public ProcedureCategoryType createProcedureCategoryType() {
        return new ProcedureCategoryType();
    }

    /**
     * Create an instance of {@link InvolvedPartyRemarkDetailType }
     * 
     */
    public InvolvedPartyRemarkDetailType createInvolvedPartyRemarkDetailType() {
        return new InvolvedPartyRemarkDetailType();
    }

    /**
     * Create an instance of {@link InvolvedPartyDetailType }
     * 
     */
    public InvolvedPartyDetailType createInvolvedPartyDetailType() {
        return new InvolvedPartyDetailType();
    }

    /**
     * Create an instance of {@link QuantityType }
     * 
     */
    public QuantityType createQuantityType() {
        return new QuantityType();
    }

    /**
     * Create an instance of {@link SanctionedTerritoryResponseType }
     * 
     */
    public SanctionedTerritoryResponseType createSanctionedTerritoryResponseType() {
        return new SanctionedTerritoryResponseType();
    }

    /**
     * Create an instance of {@link ComplianceRuleResponseType }
     * 
     */
    public ComplianceRuleResponseType createComplianceRuleResponseType() {
        return new ComplianceRuleResponseType();
    }

    /**
     * Create an instance of {@link ControlScreeningResultType }
     * 
     */
    public ControlScreeningResultType createControlScreeningResultType() {
        return new ControlScreeningResultType();
    }

    /**
     * Create an instance of {@link DeclarationSubTypeType }
     * 
     */
    public DeclarationSubTypeType createDeclarationSubTypeType() {
        return new DeclarationSubTypeType();
    }

    /**
     * Create an instance of {@link PercentageType }
     * 
     */
    public PercentageType createPercentageType() {
        return new PercentageType();
    }

    /**
     * Create an instance of {@link ConditionType }
     * 
     */
    public ConditionType createConditionType() {
        return new ConditionType();
    }

    /**
     * Create an instance of {@link CurrencyType }
     * 
     */
    public CurrencyType createCurrencyType() {
        return new CurrencyType();
    }

    /**
     * Create an instance of {@link ValuationMethodType }
     * 
     */
    public ValuationMethodType createValuationMethodType() {
        return new ValuationMethodType();
    }

    /**
     * Create an instance of {@link TransactionRequiredDocumentType }
     * 
     */
    public TransactionRequiredDocumentType createTransactionRequiredDocumentType() {
        return new TransactionRequiredDocumentType();
    }

    /**
     * Create an instance of {@link ControlTypeCodeType }
     * 
     */
    public ControlTypeCodeType createControlTypeCodeType() {
        return new ControlTypeCodeType();
    }

    /**
     * Create an instance of {@link PartyScreeningResultType }
     * 
     */
    public PartyScreeningResultType createPartyScreeningResultType() {
        return new PartyScreeningResultType();
    }

    /**
     * Create an instance of {@link TransactionInvLocationType }
     * 
     */
    public TransactionInvLocationType createTransactionInvLocationType() {
        return new TransactionInvLocationType();
    }

    /**
     * Create an instance of {@link RestrictedPartyType }
     * 
     */
    public RestrictedPartyType createRestrictedPartyType() {
        return new RestrictedPartyType();
    }

    /**
     * Create an instance of {@link ControlScreeningResultGenericType }
     * 
     */
    public ControlScreeningResultGenericType createControlScreeningResultGenericType() {
        return new ControlScreeningResultGenericType();
    }

    /**
     * Create an instance of {@link ContactRegistrationType }
     * 
     */
    public ContactRegistrationType createContactRegistrationType() {
        return new ContactRegistrationType();
    }

    /**
     * Create an instance of {@link RuleControlType }
     * 
     */
    public RuleControlType createRuleControlType() {
        return new RuleControlType();
    }

    /**
     * Create an instance of {@link ProdClassCodeUOMType }
     * 
     */
    public ProdClassCodeUOMType createProdClassCodeUOMType() {
        return new ProdClassCodeUOMType();
    }

    /**
     * Create an instance of {@link GtmRegRelatedItemType }
     * 
     */
    public GtmRegRelatedItemType createGtmRegRelatedItemType() {
        return new GtmRegRelatedItemType();
    }

    /**
     * Create an instance of {@link AdditionalInfoType }
     * 
     */
    public AdditionalInfoType createAdditionalInfoType() {
        return new AdditionalInfoType();
    }

    /**
     * Create an instance of {@link ConveyanceType }
     * 
     */
    public ConveyanceType createConveyanceType() {
        return new ConveyanceType();
    }

    /**
     * Create an instance of {@link GtmRemarksType }
     * 
     */
    public GtmRemarksType createGtmRemarksType() {
        return new GtmRemarksType();
    }

    /**
     * Create an instance of {@link RequiredTextType }
     * 
     */
    public RequiredTextType createRequiredTextType() {
        return new RequiredTextType();
    }

    /**
     * Create an instance of {@link LicenseDeterminationType }
     * 
     */
    public LicenseDeterminationType createLicenseDeterminationType() {
        return new LicenseDeterminationType();
    }

    /**
     * Create an instance of {@link CalculatedValueResultType }
     * 
     */
    public CalculatedValueResultType createCalculatedValueResultType() {
        return new CalculatedValueResultType();
    }

    /**
     * Create an instance of {@link SanctionControlScreeningResultType }
     * 
     */
    public SanctionControlScreeningResultType createSanctionControlScreeningResultType() {
        return new SanctionControlScreeningResultType();
    }

    /**
     * Create an instance of {@link ClassificationType }
     * 
     */
    public ClassificationType createClassificationType() {
        return new ClassificationType();
    }

    /**
     * Create an instance of {@link GtmDeclarationLineType }
     * 
     */
    public GtmDeclarationLineType createGtmDeclarationLineType() {
        return new GtmDeclarationLineType();
    }

    /**
     * Create an instance of {@link CategoryTypeCodeType }
     * 
     */
    public CategoryTypeCodeType createCategoryTypeCodeType() {
        return new CategoryTypeCodeType();
    }

    /**
     * Create an instance of {@link ProdClassCodeNoteType }
     * 
     */
    public ProdClassCodeNoteType createProdClassCodeNoteType() {
        return new ProdClassCodeNoteType();
    }

    /**
     * Create an instance of {@link TransDateType }
     * 
     */
    public TransDateType createTransDateType() {
        return new TransDateType();
    }

    /**
     * Create an instance of {@link InvolvedPartyRefnumDetailType }
     * 
     */
    public InvolvedPartyRefnumDetailType createInvolvedPartyRefnumDetailType() {
        return new InvolvedPartyRefnumDetailType();
    }

    /**
     * Create an instance of {@link ResponseInfoType }
     * 
     */
    public ResponseInfoType createResponseInfoType() {
        return new ResponseInfoType();
    }

    /**
     * Create an instance of {@link ClassificationResponseType }
     * 
     */
    public ClassificationResponseType createClassificationResponseType() {
        return new ClassificationResponseType();
    }

    /**
     * Create an instance of {@link ProdClassCodeDescType }
     * 
     */
    public ProdClassCodeDescType createProdClassCodeDescType() {
        return new ProdClassCodeDescType();
    }

    /**
     * Create an instance of {@link SanctionedTerritoryType }
     * 
     */
    public SanctionedTerritoryType createSanctionedTerritoryType() {
        return new SanctionedTerritoryType();
    }

    /**
     * Create an instance of {@link GtmRegRelatedLicenseType }
     * 
     */
    public GtmRegRelatedLicenseType createGtmRegRelatedLicenseType() {
        return new GtmRegRelatedLicenseType();
    }

    /**
     * Create an instance of {@link GtmTransactionLineRequiredDocumentType }
     * 
     */
    public GtmTransactionLineRequiredDocumentType createGtmTransactionLineRequiredDocumentType() {
        return new GtmTransactionLineRequiredDocumentType();
    }

    /**
     * Create an instance of {@link GtmRegRelatedRegistrationType }
     * 
     */
    public GtmRegRelatedRegistrationType createGtmRegRelatedRegistrationType() {
        return new GtmRegRelatedRegistrationType();
    }

    /**
     * Create an instance of {@link RIQQueryInfoType.SourceLocRef }
     * 
     */
    public RIQQueryInfoType.SourceLocRef createRIQQueryInfoTypeSourceLocRef() {
        return new RIQQueryInfoType.SourceLocRef();
    }

    /**
     * Create an instance of {@link RIQQueryInfoType.DestLocRef }
     * 
     */
    public RIQQueryInfoType.DestLocRef createRIQQueryInfoTypeDestLocRef() {
        return new RIQQueryInfoType.DestLocRef();
    }

    /**
     * Create an instance of {@link DriverHeaderType.DefHomeLocation }
     * 
     */
    public DriverHeaderType.DefHomeLocation createDriverHeaderTypeDefHomeLocation() {
        return new DriverHeaderType.DefHomeLocation();
    }

    /**
     * Create an instance of {@link QuoteCostOptionType.SellCostOption }
     * 
     */
    public QuoteCostOptionType.SellCostOption createQuoteCostOptionTypeSellCostOption() {
        return new QuoteCostOptionType.SellCostOption();
    }

    /**
     * Create an instance of {@link QuoteCostOptionType.BuyCostOption }
     * 
     */
    public QuoteCostOptionType.BuyCostOption createQuoteCostOptionTypeBuyCostOption() {
        return new QuoteCostOptionType.BuyCostOption();
    }

    /**
     * Create an instance of {@link TransmissionSummaryType.ApplicationReport }
     * 
     */
    public TransmissionSummaryType.ApplicationReport createTransmissionSummaryTypeApplicationReport() {
        return new TransmissionSummaryType.ApplicationReport();
    }

    /**
     * Create an instance of {@link RIQQueryType.StopLocation }
     * 
     */
    public RIQQueryType.StopLocation createRIQQueryTypeStopLocation() {
        return new RIQQueryType.StopLocation();
    }

    /**
     * Create an instance of {@link RIQQueryType.RoutingConstraints }
     * 
     */
    public RIQQueryType.RoutingConstraints createRIQQueryTypeRoutingConstraints() {
        return new RIQQueryType.RoutingConstraints();
    }

    /**
     * Create an instance of {@link RATEGEOCOSTWEIGHTBREAKTYPE.RATEGEOCOSTWEIGHTBREAKROW }
     * 
     */
    public RATEGEOCOSTWEIGHTBREAKTYPE.RATEGEOCOSTWEIGHTBREAKROW createRATEGEOCOSTWEIGHTBREAKTYPERATEGEOCOSTWEIGHTBREAKROW() {
        return new RATEGEOCOSTWEIGHTBREAKTYPE.RATEGEOCOSTWEIGHTBREAKROW();
    }

    /**
     * Create an instance of {@link ServiceProviderType.ConditionalBookingProfile }
     * 
     */
    public ServiceProviderType.ConditionalBookingProfile createServiceProviderTypeConditionalBookingProfile() {
        return new ServiceProviderType.ConditionalBookingProfile();
    }

    /**
     * Create an instance of {@link ServiceProviderType.ServprovService }
     * 
     */
    public ServiceProviderType.ServprovService createServiceProviderTypeServprovService() {
        return new ServiceProviderType.ServprovService();
    }

    /**
     * Create an instance of {@link ServiceProviderType.ServprovModeInfo }
     * 
     */
    public ServiceProviderType.ServprovModeInfo createServiceProviderTypeServprovModeInfo() {
        return new ServiceProviderType.ServprovModeInfo();
    }

    /**
     * Create an instance of {@link ServiceProviderType.ServprovPolicy }
     * 
     */
    public ServiceProviderType.ServprovPolicy createServiceProviderTypeServprovPolicy() {
        return new ServiceProviderType.ServprovPolicy();
    }

    /**
     * Create an instance of {@link TransactionHeaderType.ObjectModInfo }
     * 
     */
    public TransactionHeaderType.ObjectModInfo createTransactionHeaderTypeObjectModInfo() {
        return new TransactionHeaderType.ObjectModInfo();
    }

    /**
     * Create an instance of {@link IntSavedQueryType.IntSavedQueryArg }
     * 
     */
    public IntSavedQueryType.IntSavedQueryArg createIntSavedQueryTypeIntSavedQueryArg() {
        return new IntSavedQueryType.IntSavedQueryArg();
    }

    /**
     * Create an instance of {@link RATEGEOCOSTTYPE.RATEGEOCOSTROW }
     * 
     */
    public RATEGEOCOSTTYPE.RATEGEOCOSTROW createRATEGEOCOSTTYPERATEGEOCOSTROW() {
        return new RATEGEOCOSTTYPE.RATEGEOCOSTROW();
    }

    /**
     * Create an instance of {@link LocationOverrideRefType.LocationOverride }
     * 
     */
    public LocationOverrideRefType.LocationOverride createLocationOverrideRefTypeLocationOverride() {
        return new LocationOverrideRefType.LocationOverride();
    }

    /**
     * Create an instance of {@link RegionRefType.RegionHeader.RegionComponent }
     * 
     */
    public RegionRefType.RegionHeader.RegionComponent createRegionRefTypeRegionHeaderRegionComponent() {
        return new RegionRefType.RegionHeader.RegionComponent();
    }

    /**
     * Create an instance of {@link RouteTemplateLegType.RouteTempLegOrderCompat }
     * 
     */
    public RouteTemplateLegType.RouteTempLegOrderCompat createRouteTemplateLegTypeRouteTempLegOrderCompat() {
        return new RouteTemplateLegType.RouteTempLegOrderCompat();
    }

    /**
     * Create an instance of {@link RouteTemplateLegType.RouteTempLegShipCompat }
     * 
     */
    public RouteTemplateLegType.RouteTempLegShipCompat createRouteTemplateLegTypeRouteTempLegShipCompat() {
        return new RouteTemplateLegType.RouteTempLegShipCompat();
    }

    /**
     * Create an instance of {@link RouteTemplateLegType.RouteTempLegLocIncompat.LocCompat }
     * 
     */
    public RouteTemplateLegType.RouteTempLegLocIncompat.LocCompat createRouteTemplateLegTypeRouteTempLegLocIncompatLocCompat() {
        return new RouteTemplateLegType.RouteTempLegLocIncompat.LocCompat();
    }

    /**
     * Create an instance of {@link ACCESSORIALCOSTTYPE.ACCESSORIALCOSTROW.ACCESSORIALCOSTUNITBREAK.ACCESSORIALCOSTUNITBREAKROW }
     * 
     */
    public ACCESSORIALCOSTTYPE.ACCESSORIALCOSTROW.ACCESSORIALCOSTUNITBREAK.ACCESSORIALCOSTUNITBREAKROW createACCESSORIALCOSTTYPEACCESSORIALCOSTROWACCESSORIALCOSTUNITBREAKACCESSORIALCOSTUNITBREAKROW() {
        return new ACCESSORIALCOSTTYPE.ACCESSORIALCOSTROW.ACCESSORIALCOSTUNITBREAK.ACCESSORIALCOSTUNITBREAKROW();
    }

    /**
     * Create an instance of {@link CostOptionShipType.SrcLoc }
     * 
     */
    public CostOptionShipType.SrcLoc createCostOptionShipTypeSrcLoc() {
        return new CostOptionShipType.SrcLoc();
    }

    /**
     * Create an instance of {@link CostOptionShipType.DestLoc }
     * 
     */
    public CostOptionShipType.DestLoc createCostOptionShipTypeDestLoc() {
        return new CostOptionShipType.DestLoc();
    }

    /**
     * Create an instance of {@link LocationServProvPrefType.ServiceLocationGid }
     * 
     */
    public LocationServProvPrefType.ServiceLocationGid createLocationServProvPrefTypeServiceLocationGid() {
        return new LocationServProvPrefType.ServiceLocationGid();
    }

    /**
     * Create an instance of {@link LegType.LegSchedule }
     * 
     */
    public LegType.LegSchedule createLegTypeLegSchedule() {
        return new LegType.LegSchedule();
    }

    /**
     * Create an instance of {@link ShipUnitType.ShipUnitSeal }
     * 
     */
    public ShipUnitType.ShipUnitSeal createShipUnitTypeShipUnitSeal() {
        return new ShipUnitType.ShipUnitSeal();
    }

    /**
     * Create an instance of {@link ShipUnitType.LoadConfigSetupRef }
     * 
     */
    public ShipUnitType.LoadConfigSetupRef createShipUnitTypeLoadConfigSetupRef() {
        return new ShipUnitType.LoadConfigSetupRef();
    }

    /**
     * Create an instance of {@link RATEGEOCOSTUNITBREAKTYPE.RATEGEOCOSTUNITBREAKROW }
     * 
     */
    public RATEGEOCOSTUNITBREAKTYPE.RATEGEOCOSTUNITBREAKROW createRATEGEOCOSTUNITBREAKTYPERATEGEOCOSTUNITBREAKROW() {
        return new RATEGEOCOSTUNITBREAKTYPE.RATEGEOCOSTUNITBREAKROW();
    }

    /**
     * Create an instance of {@link IntCommandType.IntArg }
     * 
     */
    public IntCommandType.IntArg createIntCommandTypeIntArg() {
        return new IntCommandType.IntArg();
    }

    /**
     * Create an instance of {@link ReleaseLineType.ReleaseLineAllocationInfo }
     * 
     */
    public ReleaseLineType.ReleaseLineAllocationInfo createReleaseLineTypeReleaseLineAllocationInfo() {
        return new ReleaseLineType.ReleaseLineAllocationInfo();
    }

    /**
     * Create an instance of {@link ShipmentHeaderType.ShipmentInfeasibility }
     * 
     */
    public ShipmentHeaderType.ShipmentInfeasibility createShipmentHeaderTypeShipmentInfeasibility() {
        return new ShipmentHeaderType.ShipmentInfeasibility();
    }

    /**
     * Create an instance of {@link LocationType.ChildOperationalLocations }
     * 
     */
    public LocationType.ChildOperationalLocations createLocationTypeChildOperationalLocations() {
        return new LocationType.ChildOperationalLocations();
    }

    /**
     * Create an instance of {@link LocationType.LoadUnloadPoint }
     * 
     */
    public LocationType.LoadUnloadPoint createLocationTypeLoadUnloadPoint() {
        return new LocationType.LoadUnloadPoint();
    }

    /**
     * Create an instance of {@link LocationType.LocationCapacityGroupInfo }
     * 
     */
    public LocationType.LocationCapacityGroupInfo createLocationTypeLocationCapacityGroupInfo() {
        return new LocationType.LocationCapacityGroupInfo();
    }

    /**
     * Create an instance of {@link ContactGroupType.IncludeContact }
     * 
     */
    public ContactGroupType.IncludeContact createContactGroupTypeIncludeContact() {
        return new ContactGroupType.IncludeContact();
    }

    /**
     * Create an instance of {@link TransOrderStatusType.SLine }
     * 
     */
    public TransOrderStatusType.SLine createTransOrderStatusTypeSLine() {
        return new TransOrderStatusType.SLine();
    }

    /**
     * Create an instance of {@link ContactType.ExternalSystem }
     * 
     */
    public ContactType.ExternalSystem createContactTypeExternalSystem() {
        return new ContactType.ExternalSystem();
    }

    /**
     * Create an instance of {@link ContactType.ConsolidationProfile }
     * 
     */
    public ContactType.ConsolidationProfile createContactTypeConsolidationProfile() {
        return new ContactType.ConsolidationProfile();
    }

    /**
     * Create an instance of {@link ContactType.ContactCorporation }
     * 
     */
    public ContactType.ContactCorporation createContactTypeContactCorporation() {
        return new ContactType.ContactCorporation();
    }

    /**
     * Create an instance of {@link TransactionAckType.ReferencedSendReason }
     * 
     */
    public TransactionAckType.ReferencedSendReason createTransactionAckTypeReferencedSendReason() {
        return new TransactionAckType.ReferencedSendReason();
    }

    /**
     * Create an instance of {@link AllocationBaseType.ChildAllocationBases }
     * 
     */
    public AllocationBaseType.ChildAllocationBases createAllocationBaseTypeChildAllocationBases() {
        return new AllocationBaseType.ChildAllocationBases();
    }

    /**
     * Create an instance of {@link ReleaseType.ReleaseShipmentInfo }
     * 
     */
    public ReleaseType.ReleaseShipmentInfo createReleaseTypeReleaseShipmentInfo() {
        return new ReleaseType.ReleaseShipmentInfo();
    }

    /**
     * Create an instance of {@link ReleaseType.ReleaseAllocationInfo }
     * 
     */
    public ReleaseType.ReleaseAllocationInfo createReleaseTypeReleaseAllocationInfo() {
        return new ReleaseType.ReleaseAllocationInfo();
    }

    /**
     * Create an instance of {@link TopicType.TopicArg }
     * 
     */
    public TopicType.TopicArg createTopicTypeTopicArg() {
        return new TopicType.TopicArg();
    }

    /**
     * Create an instance of {@link ShipmentLinkType.PrevShipmentStopNum }
     * 
     */
    public ShipmentLinkType.PrevShipmentStopNum createShipmentLinkTypePrevShipmentStopNum() {
        return new ShipmentLinkType.PrevShipmentStopNum();
    }

    /**
     * Create an instance of {@link ShipmentLinkType.NextShipmentStopNum }
     * 
     */
    public ShipmentLinkType.NextShipmentStopNum createShipmentLinkTypeNextShipmentStopNum() {
        return new ShipmentLinkType.NextShipmentStopNum();
    }

    /**
     * Create an instance of {@link RateGeoType.RATEGEOROW.RATEGEOREMARKS.RATEGEOREMARKSROW }
     * 
     */
    public RateGeoType.RATEGEOROW.RATEGEOREMARKS.RATEGEOREMARKSROW createRateGeoTypeRATEGEOROWRATEGEOREMARKSRATEGEOREMARKSROW() {
        return new RateGeoType.RATEGEOROW.RATEGEOREMARKS.RATEGEOREMARKSROW();
    }

    /**
     * Create an instance of {@link RateGeoType.RATEGEOROW.RGSPECIALSERVICEACCESSORIAL.RGSPECIALSERVICEACCESSORIALROW }
     * 
     */
    public RateGeoType.RATEGEOROW.RGSPECIALSERVICEACCESSORIAL.RGSPECIALSERVICEACCESSORIALROW createRateGeoTypeRATEGEOROWRGSPECIALSERVICEACCESSORIALRGSPECIALSERVICEACCESSORIALROW() {
        return new RateGeoType.RATEGEOROW.RGSPECIALSERVICEACCESSORIAL.RGSPECIALSERVICEACCESSORIALROW();
    }

    /**
     * Create an instance of {@link RateGeoType.RATEGEOROW.RATEGEOCOSTGROUP.RATEGEOCOSTGROUPROW }
     * 
     */
    public RateGeoType.RATEGEOROW.RATEGEOCOSTGROUP.RATEGEOCOSTGROUPROW createRateGeoTypeRATEGEOROWRATEGEOCOSTGROUPRATEGEOCOSTGROUPROW() {
        return new RateGeoType.RATEGEOROW.RATEGEOCOSTGROUP.RATEGEOCOSTGROUPROW();
    }

    /**
     * Create an instance of {@link RateGeoType.RATEGEOROW.RATEGEOACCESSORIAL.RATEGEOACCESSORIALROW }
     * 
     */
    public RateGeoType.RATEGEOROW.RATEGEOACCESSORIAL.RATEGEOACCESSORIALROW createRateGeoTypeRATEGEOROWRATEGEOACCESSORIALRATEGEOACCESSORIALROW() {
        return new RateGeoType.RATEGEOROW.RATEGEOACCESSORIAL.RATEGEOACCESSORIALROW();
    }

    /**
     * Create an instance of {@link RateGeoType.RATEGEOROW.RATEGEOSTOPS.RATEGEOSTOPSROW }
     * 
     */
    public RateGeoType.RATEGEOROW.RATEGEOSTOPS.RATEGEOSTOPSROW createRateGeoTypeRATEGEOROWRATEGEOSTOPSRATEGEOSTOPSROW() {
        return new RateGeoType.RATEGEOROW.RATEGEOSTOPS.RATEGEOSTOPSROW();
    }

    /**
     * Create an instance of {@link RateOfferingType.RATEOFFERINGROW.RATEOFFERINGINVPARTY.RATEOFFERINGINVPARTYROW }
     * 
     */
    public RateOfferingType.RATEOFFERINGROW.RATEOFFERINGINVPARTY.RATEOFFERINGINVPARTYROW createRateOfferingTypeRATEOFFERINGROWRATEOFFERINGINVPARTYRATEOFFERINGINVPARTYROW() {
        return new RateOfferingType.RATEOFFERINGROW.RATEOFFERINGINVPARTY.RATEOFFERINGINVPARTYROW();
    }

    /**
     * Create an instance of {@link RateOfferingType.RATEOFFERINGROW.ROSPECIALSERVICEACCESSORIAL.ROSPECIALSERVICEACCESSORIALROW }
     * 
     */
    public RateOfferingType.RATEOFFERINGROW.ROSPECIALSERVICEACCESSORIAL.ROSPECIALSERVICEACCESSORIALROW createRateOfferingTypeRATEOFFERINGROWROSPECIALSERVICEACCESSORIALROSPECIALSERVICEACCESSORIALROW() {
        return new RateOfferingType.RATEOFFERINGROW.ROSPECIALSERVICEACCESSORIAL.ROSPECIALSERVICEACCESSORIALROW();
    }

    /**
     * Create an instance of {@link RateOfferingType.RATEOFFERINGROW.RATERULESANDTERMS.RATERULESANDTERMSROW }
     * 
     */
    public RateOfferingType.RATEOFFERINGROW.RATERULESANDTERMS.RATERULESANDTERMSROW createRateOfferingTypeRATEOFFERINGROWRATERULESANDTERMSRATERULESANDTERMSROW() {
        return new RateOfferingType.RATEOFFERINGROW.RATERULESANDTERMS.RATERULESANDTERMSROW();
    }

    /**
     * Create an instance of {@link RateOfferingType.RATEOFFERINGROW.RATEOFFERINGCOMMENT.RATEOFFERINGCOMMENTROW }
     * 
     */
    public RateOfferingType.RATEOFFERINGROW.RATEOFFERINGCOMMENT.RATEOFFERINGCOMMENTROW createRateOfferingTypeRATEOFFERINGROWRATEOFFERINGCOMMENTRATEOFFERINGCOMMENTROW() {
        return new RateOfferingType.RATEOFFERINGROW.RATEOFFERINGCOMMENT.RATEOFFERINGCOMMENTROW();
    }

    /**
     * Create an instance of {@link RateOfferingType.RATEOFFERINGROW.RATEOFFERINGACCESSORIAL.RATEOFFERINGACCESSORIALROW }
     * 
     */
    public RateOfferingType.RATEOFFERINGROW.RATEOFFERINGACCESSORIAL.RATEOFFERINGACCESSORIALROW createRateOfferingTypeRATEOFFERINGROWRATEOFFERINGACCESSORIALRATEOFFERINGACCESSORIALROW() {
        return new RateOfferingType.RATEOFFERINGROW.RATEOFFERINGACCESSORIAL.RATEOFFERINGACCESSORIALROW();
    }

    /**
     * Create an instance of {@link RateOfferingType.RATEOFFERINGROW.RATEOFFERINGSTOPS.RATEOFFERINGSTOPSROW }
     * 
     */
    public RateOfferingType.RATEOFFERINGROW.RATEOFFERINGSTOPS.RATEOFFERINGSTOPSROW createRateOfferingTypeRATEOFFERINGROWRATEOFFERINGSTOPSRATEOFFERINGSTOPSROW() {
        return new RateOfferingType.RATEOFFERINGROW.RATEOFFERINGSTOPS.RATEOFFERINGSTOPSROW();
    }

    /**
     * Create an instance of {@link JobType.HouseJobs }
     * 
     */
    public JobType.HouseJobs createJobTypeHouseJobs() {
        return new JobType.HouseJobs();
    }

    /**
     * Create an instance of {@link ReleaseInstructionType.QuantityToRelease }
     * 
     */
    public ReleaseInstructionType.QuantityToRelease createReleaseInstructionTypeQuantityToRelease() {
        return new ReleaseInstructionType.QuantityToRelease();
    }

    /**
     * Create an instance of {@link ReleaseInstructionType.LineRelInstInfo }
     * 
     */
    public ReleaseInstructionType.LineRelInstInfo createReleaseInstructionTypeLineRelInstInfo() {
        return new ReleaseInstructionType.LineRelInstInfo();
    }

    /**
     * Create an instance of {@link WorkInvoiceType.SecondaryDriver }
     * 
     */
    public WorkInvoiceType.SecondaryDriver createWorkInvoiceTypeSecondaryDriver() {
        return new WorkInvoiceType.SecondaryDriver();
    }

    /**
     * Create an instance of {@link DataQuerySummaryType.DataList }
     * 
     */
    public DataQuerySummaryType.DataList createDataQuerySummaryTypeDataList() {
        return new DataQuerySummaryType.DataList();
    }

    /**
     * Create an instance of {@link HazmatItemType.DotExemptionGid }
     * 
     */
    public HazmatItemType.DotExemptionGid createHazmatItemTypeDotExemptionGid() {
        return new HazmatItemType.DotExemptionGid();
    }

    /**
     * Create an instance of {@link HazmatItemType.AdditionalTransportMsg1Gid }
     * 
     */
    public HazmatItemType.AdditionalTransportMsg1Gid createHazmatItemTypeAdditionalTransportMsg1Gid() {
        return new HazmatItemType.AdditionalTransportMsg1Gid();
    }

    /**
     * Create an instance of {@link HazmatItemType.AdditionalTransportMsg2Gid }
     * 
     */
    public HazmatItemType.AdditionalTransportMsg2Gid createHazmatItemTypeAdditionalTransportMsg2Gid() {
        return new HazmatItemType.AdditionalTransportMsg2Gid();
    }

    /**
     * Create an instance of {@link HazmatItemType.Stcc49CodeGid }
     * 
     */
    public HazmatItemType.Stcc49CodeGid createHazmatItemTypeStcc49CodeGid() {
        return new HazmatItemType.Stcc49CodeGid();
    }

    /**
     * Create an instance of {@link HazmatItemType.CompetentAuthorityCodeGid }
     * 
     */
    public HazmatItemType.CompetentAuthorityCodeGid createHazmatItemTypeCompetentAuthorityCodeGid() {
        return new HazmatItemType.CompetentAuthorityCodeGid();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ServiceResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/gtm/transmission/v6.4", name = "ServiceResponse", substitutionHeadNamespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", substitutionHeadName = "GLogXMLTransaction")
    public JAXBElement<ServiceResponseType> createServiceResponse(ServiceResponseType value) {
        return new JAXBElement<ServiceResponseType>(_ServiceResponse_QNAME, ServiceResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ItemMasterType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", name = "ItemMaster", substitutionHeadNamespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", substitutionHeadName = "GLogXMLTransaction")
    public JAXBElement<ItemMasterType> createItemMaster(ItemMasterType value) {
        return new JAXBElement<ItemMasterType>(_ItemMaster_QNAME, ItemMasterType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RateGeoType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", name = "RATE_GEO", substitutionHeadNamespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", substitutionHeadName = "GLogXMLTransaction")
    public JAXBElement<RateGeoType> createRATEGEO(RateGeoType value) {
        return new JAXBElement<RateGeoType>(_RATEGEO_QNAME, RateGeoType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransOrderType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", name = "TransOrder", substitutionHeadNamespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", substitutionHeadName = "GLogXMLTransaction")
    public JAXBElement<TransOrderType> createTransOrder(TransOrderType value) {
        return new JAXBElement<TransOrderType>(_TransOrder_QNAME, TransOrderType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BulkTrailerBuildType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", name = "BulkTrailerBuild", substitutionHeadNamespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", substitutionHeadName = "GLogXMLTransaction")
    public JAXBElement<BulkTrailerBuildType> createBulkTrailerBuild(BulkTrailerBuildType value) {
        return new JAXBElement<BulkTrailerBuildType>(_BulkTrailerBuild_QNAME, BulkTrailerBuildType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FleetBulkPlanType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", name = "FleetBulkPlan", substitutionHeadNamespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", substitutionHeadName = "GLogXMLTransaction")
    public JAXBElement<FleetBulkPlanType> createFleetBulkPlan(FleetBulkPlanType value) {
        return new JAXBElement<FleetBulkPlanType>(_FleetBulkPlan_QNAME, FleetBulkPlanType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DriverCalendarEventType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", name = "DriverCalendarEvent", substitutionHeadNamespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", substitutionHeadName = "GLogXMLTransaction")
    public JAXBElement<DriverCalendarEventType> createDriverCalendarEvent(DriverCalendarEventType value) {
        return new JAXBElement<DriverCalendarEventType>(_DriverCalendarEvent_QNAME, DriverCalendarEventType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransmissionReportType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", name = "TransmissionReport")
    public JAXBElement<TransmissionReportType> createTransmissionReport(TransmissionReportType value) {
        return new JAXBElement<TransmissionReportType>(_TransmissionReport_QNAME, TransmissionReportType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GtmDeclarationType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/gtm/transmission/v6.4", name = "GtmDeclaration", substitutionHeadNamespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", substitutionHeadName = "GLogXMLTransaction")
    public JAXBElement<GtmDeclarationType> createGtmDeclaration(GtmDeclarationType value) {
        return new JAXBElement<GtmDeclarationType>(_GtmDeclaration_QNAME, GtmDeclarationType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RouteTemplateType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", name = "RouteTemplate", substitutionHeadNamespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", substitutionHeadName = "GLogXMLTransaction")
    public JAXBElement<RouteTemplateType> createRouteTemplate(RouteTemplateType value) {
        return new JAXBElement<RouteTemplateType>(_RouteTemplate_QNAME, RouteTemplateType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GtmDeclarationMessageType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/gtm/transmission/v6.4", name = "GtmDeclarationMessage", substitutionHeadNamespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", substitutionHeadName = "GLogXMLTransaction")
    public JAXBElement<GtmDeclarationMessageType> createGtmDeclarationMessage(GtmDeclarationMessageType value) {
        return new JAXBElement<GtmDeclarationMessageType>(_GtmDeclarationMessage_QNAME, GtmDeclarationMessageType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RateOfferingType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", name = "RATE_OFFERING", substitutionHeadNamespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", substitutionHeadName = "GLogXMLTransaction")
    public JAXBElement<RateOfferingType> createRATEOFFERING(RateOfferingType value) {
        return new JAXBElement<RateOfferingType>(_RATEOFFERING_QNAME, RateOfferingType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeviceType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", name = "Device", substitutionHeadNamespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", substitutionHeadName = "GLogXMLTransaction")
    public JAXBElement<DeviceType> createDevice(DeviceType value) {
        return new JAXBElement<DeviceType>(_Device_QNAME, DeviceType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MileageType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", name = "Mileage", substitutionHeadNamespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", substitutionHeadName = "GLogXMLTransaction")
    public JAXBElement<MileageType> createMileage(MileageType value) {
        return new JAXBElement<MileageType>(_Mileage_QNAME, MileageType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VoyageType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", name = "Voyage", substitutionHeadNamespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", substitutionHeadName = "GLogXMLTransaction")
    public JAXBElement<VoyageType> createVoyage(VoyageType value) {
        return new JAXBElement<VoyageType>(_Voyage_QNAME, VoyageType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DemurrageTransactionType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", name = "DemurrageTransaction", substitutionHeadNamespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", substitutionHeadName = "GLogXMLTransaction")
    public JAXBElement<DemurrageTransactionType> createDemurrageTransaction(DemurrageTransactionType value) {
        return new JAXBElement<DemurrageTransactionType>(_DemurrageTransaction_QNAME, DemurrageTransactionType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ActualShipmentType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", name = "ActualShipment", substitutionHeadNamespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", substitutionHeadName = "GLogXMLTransaction")
    public JAXBElement<ActualShipmentType> createActualShipment(ActualShipmentType value) {
        return new JAXBElement<ActualShipmentType>(_ActualShipment_QNAME, ActualShipmentType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ShipmentGroupTenderOfferType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", name = "ShipmentGroupTenderOffer", substitutionHeadNamespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", substitutionHeadName = "GLogXMLTransaction")
    public JAXBElement<ShipmentGroupTenderOfferType> createShipmentGroupTenderOffer(ShipmentGroupTenderOfferType value) {
        return new JAXBElement<ShipmentGroupTenderOfferType>(_ShipmentGroupTenderOffer_QNAME, ShipmentGroupTenderOfferType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CharterVoyageType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", name = "CharterVoyage", substitutionHeadNamespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", substitutionHeadName = "GLogXMLTransaction")
    public JAXBElement<CharterVoyageType> createCharterVoyage(CharterVoyageType value) {
        return new JAXBElement<CharterVoyageType>(_CharterVoyage_QNAME, CharterVoyageType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BulkContMoveType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", name = "BulkContMove", substitutionHeadNamespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", substitutionHeadName = "GLogXMLTransaction")
    public JAXBElement<BulkContMoveType> createBulkContMove(BulkContMoveType value) {
        return new JAXBElement<BulkContMoveType>(_BulkContMove_QNAME, BulkContMoveType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReleaseInstructionType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", name = "ReleaseInstruction", substitutionHeadNamespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", substitutionHeadName = "GLogXMLTransaction")
    public JAXBElement<ReleaseInstructionType> createReleaseInstruction(ReleaseInstructionType value) {
        return new JAXBElement<ReleaseInstructionType>(_ReleaseInstruction_QNAME, ReleaseInstructionType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ItemType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", name = "Item", substitutionHeadNamespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", substitutionHeadName = "GLogXMLTransaction")
    public JAXBElement<ItemType> createItem(ItemType value) {
        return new JAXBElement<ItemType>(_Item_QNAME, ItemType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CSVDataLoadType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", name = "CSVDataLoad", substitutionHeadNamespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", substitutionHeadName = "GLogXMLTransaction")
    public JAXBElement<CSVDataLoadType> createCSVDataLoad(CSVDataLoadType value) {
        return new JAXBElement<CSVDataLoadType>(_CSVDataLoad_QNAME, CSVDataLoadType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ItineraryType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", name = "Itinerary", substitutionHeadNamespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", substitutionHeadName = "GLogXMLTransaction")
    public JAXBElement<ItineraryType> createItinerary(ItineraryType value) {
        return new JAXBElement<ItineraryType>(_Itinerary_QNAME, ItineraryType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XLaneType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", name = "XLane", substitutionHeadNamespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", substitutionHeadName = "GLogXMLTransaction")
    public JAXBElement<XLaneType> createXLane(XLaneType value) {
        return new JAXBElement<XLaneType>(_XLane_QNAME, XLaneType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link JobType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", name = "Job", substitutionHeadNamespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", substitutionHeadName = "GLogXMLTransaction")
    public JAXBElement<JobType> createJob(JobType value) {
        return new JAXBElement<JobType>(_Job_QNAME, JobType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OrderMovementType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", name = "OrderMovement", substitutionHeadNamespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", substitutionHeadName = "GLogXMLTransaction")
    public JAXBElement<OrderMovementType> createOrderMovement(OrderMovementType value) {
        return new JAXBElement<OrderMovementType>(_OrderMovement_QNAME, OrderMovementType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AccrualType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", name = "Accrual", substitutionHeadNamespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", substitutionHeadName = "GLogXMLTransaction")
    public JAXBElement<AccrualType> createAccrual(AccrualType value) {
        return new JAXBElement<AccrualType>(_Accrual_QNAME, AccrualType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InvoiceType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", name = "Invoice", substitutionHeadNamespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", substitutionHeadName = "GLogXMLTransaction")
    public JAXBElement<InvoiceType> createInvoice(InvoiceType value) {
        return new JAXBElement<InvoiceType>(_Invoice_QNAME, InvoiceType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TenderOfferType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", name = "TenderOffer", substitutionHeadNamespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", substitutionHeadName = "GLogXMLTransaction")
    public JAXBElement<TenderOfferType> createTenderOffer(TenderOfferType value) {
        return new JAXBElement<TenderOfferType>(_TenderOffer_QNAME, TenderOfferType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OBShipUnitType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", name = "OBShipUnit", substitutionHeadNamespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", substitutionHeadName = "GLogXMLTransaction")
    public JAXBElement<OBShipUnitType> createOBShipUnit(OBShipUnitType value) {
        return new JAXBElement<OBShipUnitType>(_OBShipUnit_QNAME, OBShipUnitType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BulkRatingType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", name = "BulkRating", substitutionHeadNamespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", substitutionHeadName = "GLogXMLTransaction")
    public JAXBElement<BulkRatingType> createBulkRating(BulkRatingType value) {
        return new JAXBElement<BulkRatingType>(_BulkRating_QNAME, BulkRatingType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link HazmatItemType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", name = "HazmatItem", substitutionHeadNamespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", substitutionHeadName = "GLogXMLTransaction")
    public JAXBElement<HazmatItemType> createHazmatItem(HazmatItemType value) {
        return new JAXBElement<HazmatItemType>(_HazmatItem_QNAME, HazmatItemType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataQuerySummaryType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", name = "DataQuerySummary", substitutionHeadNamespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", substitutionHeadName = "GLogXMLTransaction")
    public JAXBElement<DataQuerySummaryType> createDataQuerySummary(DataQuerySummaryType value) {
        return new JAXBElement<DataQuerySummaryType>(_DataQuerySummary_QNAME, DataQuerySummaryType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WorkInvoiceType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", name = "WorkInvoice", substitutionHeadNamespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", substitutionHeadName = "GLogXMLTransaction")
    public JAXBElement<WorkInvoiceType> createWorkInvoice(WorkInvoiceType value) {
        return new JAXBElement<WorkInvoiceType>(_WorkInvoice_QNAME, WorkInvoiceType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransOrderLinkType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", name = "TransOrderLink", substitutionHeadNamespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", substitutionHeadName = "GLogXMLTransaction")
    public JAXBElement<TransOrderLinkType> createTransOrderLink(TransOrderLinkType value) {
        return new JAXBElement<TransOrderLinkType>(_TransOrderLink_QNAME, TransOrderLinkType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GtmStructureType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/gtm/transmission/v6.4", name = "GtmStructure", substitutionHeadNamespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", substitutionHeadName = "GLogXMLTransaction")
    public JAXBElement<GtmStructureType> createGtmStructure(GtmStructureType value) {
        return new JAXBElement<GtmStructureType>(_GtmStructure_QNAME, GtmStructureType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DocumentType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", name = "Document", substitutionHeadNamespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", substitutionHeadName = "GLogXMLTransaction")
    public JAXBElement<DocumentType> createDocument(DocumentType value) {
        return new JAXBElement<DocumentType>(_Document_QNAME, DocumentType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OBLineType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", name = "OBLine", substitutionHeadNamespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", substitutionHeadName = "GLogXMLTransaction")
    public JAXBElement<OBLineType> createOBLine(OBLineType value) {
        return new JAXBElement<OBLineType>(_OBLine_QNAME, OBLineType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TenderResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", name = "TenderResponse", substitutionHeadNamespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", substitutionHeadName = "GLogXMLTransaction")
    public JAXBElement<TenderResponseType> createTenderResponse(TenderResponseType value) {
        return new JAXBElement<TenderResponseType>(_TenderResponse_QNAME, TenderResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BookingLineAmendmentType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", name = "BookingLineAmendment", substitutionHeadNamespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", substitutionHeadName = "GLogXMLTransaction")
    public JAXBElement<BookingLineAmendmentType> createBookingLineAmendment(BookingLineAmendmentType value) {
        return new JAXBElement<BookingLineAmendmentType>(_BookingLineAmendment_QNAME, BookingLineAmendmentType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransOrderStatusType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", name = "TransOrderStatus", substitutionHeadNamespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", substitutionHeadName = "GLogXMLTransaction")
    public JAXBElement<TransOrderStatusType> createTransOrderStatus(TransOrderStatusType value) {
        return new JAXBElement<TransOrderStatusType>(_TransOrderStatus_QNAME, TransOrderStatusType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link QuoteType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", name = "Quote", substitutionHeadNamespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", substitutionHeadName = "GLogXMLTransaction")
    public JAXBElement<QuoteType> createQuote(QuoteType value) {
        return new JAXBElement<QuoteType>(_Quote_QNAME, QuoteType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CorporationType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", name = "Corporation", substitutionHeadNamespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", substitutionHeadName = "GLogXMLTransaction")
    public JAXBElement<CorporationType> createCorporation(CorporationType value) {
        return new JAXBElement<CorporationType>(_Corporation_QNAME, CorporationType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ShipStopType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", name = "ShipStop", substitutionHeadNamespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", substitutionHeadName = "GLogXMLTransaction")
    public JAXBElement<ShipStopType> createShipStop(ShipStopType value) {
        return new JAXBElement<ShipStopType>(_ShipStop_QNAME, ShipStopType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BillingType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", name = "Billing", substitutionHeadNamespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", substitutionHeadName = "GLogXMLTransaction")
    public JAXBElement<BillingType> createBilling(BillingType value) {
        return new JAXBElement<BillingType>(_Billing_QNAME, BillingType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PlannedShipmentType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", name = "PlannedShipment", substitutionHeadNamespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", substitutionHeadName = "GLogXMLTransaction")
    public JAXBElement<PlannedShipmentType> createPlannedShipment(PlannedShipmentType value) {
        return new JAXBElement<PlannedShipmentType>(_PlannedShipment_QNAME, PlannedShipmentType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EquipmentType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", name = "Equipment", substitutionHeadNamespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", substitutionHeadName = "GLogXMLTransaction")
    public JAXBElement<EquipmentType> createEquipment(EquipmentType value) {
        return new JAXBElement<EquipmentType>(_Equipment_QNAME, EquipmentType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ServiceTimeType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", name = "ServiceTime", substitutionHeadNamespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", substitutionHeadName = "GLogXMLTransaction")
    public JAXBElement<ServiceTimeType> createServiceTime(ServiceTimeType value) {
        return new JAXBElement<ServiceTimeType>(_ServiceTime_QNAME, ServiceTimeType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SkuEventType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", name = "SkuEvent", substitutionHeadNamespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", substitutionHeadName = "GLogXMLTransaction")
    public JAXBElement<SkuEventType> createSkuEvent(SkuEventType value) {
        return new JAXBElement<SkuEventType>(_SkuEvent_QNAME, SkuEventType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ClaimType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", name = "Claim", substitutionHeadNamespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", substitutionHeadName = "GLogXMLTransaction")
    public JAXBElement<ClaimType> createClaim(ClaimType value) {
        return new JAXBElement<ClaimType>(_Claim_QNAME, ClaimType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LocationType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", name = "Location", substitutionHeadNamespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", substitutionHeadName = "GLogXMLTransaction")
    public JAXBElement<LocationType> createLocation(LocationType value) {
        return new JAXBElement<LocationType>(_Location_QNAME, LocationType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GLogXMLTransactionType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", name = "GLogXMLTransaction")
    public JAXBElement<GLogXMLTransactionType> createGLogXMLTransaction(GLogXMLTransactionType value) {
        return new JAXBElement<GLogXMLTransactionType>(_GLogXMLTransaction_QNAME, GLogXMLTransactionType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransmissionHeaderType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", name = "TransmissionHeader")
    public JAXBElement<TransmissionHeaderType> createTransmissionHeader(TransmissionHeaderType value) {
        return new JAXBElement<TransmissionHeaderType>(_TransmissionHeader_QNAME, TransmissionHeaderType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ContactGroupType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", name = "ContactGroup", substitutionHeadNamespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", substitutionHeadName = "GLogXMLTransaction")
    public JAXBElement<ContactGroupType> createContactGroup(ContactGroupType value) {
        return new JAXBElement<ContactGroupType>(_ContactGroup_QNAME, ContactGroupType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SkuType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", name = "Sku", substitutionHeadNamespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", substitutionHeadName = "GLogXMLTransaction")
    public JAXBElement<SkuType> createSku(SkuType value) {
        return new JAXBElement<SkuType>(_Sku_QNAME, SkuType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BulkPlanType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", name = "BulkPlan", substitutionHeadNamespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", substitutionHeadName = "GLogXMLTransaction")
    public JAXBElement<BulkPlanType> createBulkPlan(BulkPlanType value) {
        return new JAXBElement<BulkPlanType>(_BulkPlan_QNAME, BulkPlanType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AllocationBaseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", name = "AllocationBase", substitutionHeadNamespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", substitutionHeadName = "GLogXMLTransaction")
    public JAXBElement<AllocationBaseType> createAllocationBase(AllocationBaseType value) {
        return new JAXBElement<AllocationBaseType>(_AllocationBase_QNAME, AllocationBaseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SShipUnitType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", name = "SShipUnit", substitutionHeadNamespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", substitutionHeadName = "GLogXMLTransaction")
    public JAXBElement<SShipUnitType> createSShipUnit(SShipUnitType value) {
        return new JAXBElement<SShipUnitType>(_SShipUnit_QNAME, SShipUnitType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ShipmentGroupType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", name = "ShipmentGroup", substitutionHeadNamespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", substitutionHeadName = "GLogXMLTransaction")
    public JAXBElement<ShipmentGroupType> createShipmentGroup(ShipmentGroupType value) {
        return new JAXBElement<ShipmentGroupType>(_ShipmentGroup_QNAME, ShipmentGroupType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransactionAckType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", name = "TransactionAck", substitutionHeadNamespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", substitutionHeadName = "GLogXMLTransaction")
    public JAXBElement<TransactionAckType> createTransactionAck(TransactionAckType value) {
        return new JAXBElement<TransactionAckType>(_TransactionAck_QNAME, TransactionAckType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ContactType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", name = "Contact", substitutionHeadNamespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", substitutionHeadName = "GLogXMLTransaction")
    public JAXBElement<ContactType> createContact(ContactType value) {
        return new JAXBElement<ContactType>(_Contact_QNAME, ContactType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VoucherType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", name = "Voucher", substitutionHeadNamespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", substitutionHeadName = "GLogXMLTransaction")
    public JAXBElement<VoucherType> createVoucher(VoucherType value) {
        return new JAXBElement<VoucherType>(_Voucher_QNAME, VoucherType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GtmBondType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/gtm/transmission/v6.4", name = "GtmBond", substitutionHeadNamespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", substitutionHeadName = "GLogXMLTransaction")
    public JAXBElement<GtmBondType> createGtmBond(GtmBondType value) {
        return new JAXBElement<GtmBondType>(_GtmBond_QNAME, GtmBondType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GtmTransactionLineType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/gtm/transmission/v6.4", name = "GtmTransactionLine", substitutionHeadNamespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", substitutionHeadName = "GLogXMLTransaction")
    public JAXBElement<GtmTransactionLineType> createGtmTransactionLine(GtmTransactionLineType value) {
        return new JAXBElement<GtmTransactionLineType>(_GtmTransactionLine_QNAME, GtmTransactionLineType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GtmContactType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/gtm/transmission/v6.4", name = "GtmContact", substitutionHeadNamespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", substitutionHeadName = "GLogXMLTransaction")
    public JAXBElement<GtmContactType> createGtmContact(GtmContactType value) {
        return new JAXBElement<GtmContactType>(_GtmContact_QNAME, GtmContactType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ActivityTimeDefType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", name = "ActivityTimeDef", substitutionHeadNamespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", substitutionHeadName = "GLogXMLTransaction")
    public JAXBElement<ActivityTimeDefType> createActivityTimeDef(ActivityTimeDefType value) {
        return new JAXBElement<ActivityTimeDefType>(_ActivityTimeDef_QNAME, ActivityTimeDefType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GtmTransactionType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/gtm/transmission/v6.4", name = "GtmTransaction", substitutionHeadNamespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", substitutionHeadName = "GLogXMLTransaction")
    public JAXBElement<GtmTransactionType> createGtmTransaction(GtmTransactionType value) {
        return new JAXBElement<GtmTransactionType>(_GtmTransaction_QNAME, GtmTransactionType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ShipmentLinkType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", name = "ShipmentLink", substitutionHeadNamespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", substitutionHeadName = "GLogXMLTransaction")
    public JAXBElement<ShipmentLinkType> createShipmentLink(ShipmentLinkType value) {
        return new JAXBElement<ShipmentLinkType>(_ShipmentLink_QNAME, ShipmentLinkType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FinancialSystemFeedType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", name = "FinancialSystemFeed", substitutionHeadNamespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", substitutionHeadName = "GLogXMLTransaction")
    public JAXBElement<FinancialSystemFeedType> createFinancialSystemFeed(FinancialSystemFeedType value) {
        return new JAXBElement<FinancialSystemFeedType>(_FinancialSystemFeed_QNAME, FinancialSystemFeedType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ShipmentStatusType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", name = "ShipmentStatus", substitutionHeadNamespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", substitutionHeadName = "GLogXMLTransaction")
    public JAXBElement<ShipmentStatusType> createShipmentStatus(ShipmentStatusType value) {
        return new JAXBElement<ShipmentStatusType>(_ShipmentStatus_QNAME, ShipmentStatusType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PowerUnitType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", name = "PowerUnit", substitutionHeadNamespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", substitutionHeadName = "GLogXMLTransaction")
    public JAXBElement<PowerUnitType> createPowerUnit(PowerUnitType value) {
        return new JAXBElement<PowerUnitType>(_PowerUnit_QNAME, PowerUnitType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoteQueryType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", name = "RemoteQuery", substitutionHeadNamespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", substitutionHeadName = "GLogXMLTransaction")
    public JAXBElement<RemoteQueryType> createRemoteQuery(RemoteQueryType value) {
        return new JAXBElement<RemoteQueryType>(_RemoteQuery_QNAME, RemoteQueryType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoteQueryReplyType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", name = "RemoteQueryReply", substitutionHeadNamespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", substitutionHeadName = "GLogXMLTransaction")
    public JAXBElement<RemoteQueryReplyType> createRemoteQueryReply(RemoteQueryReplyType value) {
        return new JAXBElement<RemoteQueryReplyType>(_RemoteQueryReply_QNAME, RemoteQueryReplyType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReleaseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", name = "Release", substitutionHeadNamespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", substitutionHeadName = "GLogXMLTransaction")
    public JAXBElement<ReleaseType> createRelease(ReleaseType value) {
        return new JAXBElement<ReleaseType>(_Release_QNAME, ReleaseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TopicType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", name = "Topic", substitutionHeadNamespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", substitutionHeadName = "GLogXMLTransaction")
    public JAXBElement<TopicType> createTopic(TopicType value) {
        return new JAXBElement<TopicType>(_Topic_QNAME, TopicType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ExchangeRateType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", name = "ExchangeRate", substitutionHeadNamespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", substitutionHeadName = "GLogXMLTransaction")
    public JAXBElement<ExchangeRateType> createExchangeRate(ExchangeRateType value) {
        return new JAXBElement<ExchangeRateType>(_ExchangeRate_QNAME, ExchangeRateType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UserType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", name = "User", substitutionHeadNamespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", substitutionHeadName = "GLogXMLTransaction")
    public JAXBElement<UserType> createUser(UserType value) {
        return new JAXBElement<UserType>(_User_QNAME, UserType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SkuTransactionType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", name = "SkuTransaction", substitutionHeadNamespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", substitutionHeadName = "GLogXMLTransaction")
    public JAXBElement<SkuTransactionType> createSkuTransaction(SkuTransactionType value) {
        return new JAXBElement<SkuTransactionType>(_SkuTransaction_QNAME, SkuTransactionType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsolType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", name = "Consol", substitutionHeadNamespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", substitutionHeadName = "GLogXMLTransaction")
    public JAXBElement<ConsolType> createConsol(ConsolType value) {
        return new JAXBElement<ConsolType>(_Consol_QNAME, ConsolType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link HazmatGenericType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", name = "HazmatGeneric", substitutionHeadNamespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", substitutionHeadName = "GLogXMLTransaction")
    public JAXBElement<HazmatGenericType> createHazmatGeneric(HazmatGenericType value) {
        return new JAXBElement<HazmatGenericType>(_HazmatGeneric_QNAME, HazmatGenericType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GtmRegistrationType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/gtm/transmission/v6.4", name = "GtmRegistration", substitutionHeadNamespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", substitutionHeadName = "GLogXMLTransaction")
    public JAXBElement<GtmRegistrationType> createGtmRegistration(GtmRegistrationType value) {
        return new JAXBElement<GtmRegistrationType>(_GtmRegistration_QNAME, GtmRegistrationType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DriverType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", name = "Driver", substitutionHeadNamespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", substitutionHeadName = "GLogXMLTransaction")
    public JAXBElement<DriverType> createDriver(DriverType value) {
        return new JAXBElement<DriverType>(_Driver_QNAME, DriverType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GenericStatusUpdateType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", name = "GenericStatusUpdate", substitutionHeadNamespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", substitutionHeadName = "GLogXMLTransaction")
    public JAXBElement<GenericStatusUpdateType> createGenericStatusUpdate(GenericStatusUpdateType value) {
        return new JAXBElement<GenericStatusUpdateType>(_GenericStatusUpdate_QNAME, GenericStatusUpdateType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ServiceRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/gtm/transmission/v6.4", name = "ServiceRequest", substitutionHeadNamespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", substitutionHeadName = "GLogXMLTransaction")
    public JAXBElement<ServiceRequestType> createServiceRequest(ServiceRequestType value) {
        return new JAXBElement<ServiceRequestType>(_ServiceRequest_QNAME, ServiceRequestType.class, null, value);
    }

}
