
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.*;


/**
 * Provides an alternative for updating actual quantites for a Shipment by referencing the order line.
 *             The ShipModViaOrderLineMatch element provides the fields that are used to match the affected ship units.
 *             The quantities on the ship units that are matched are updated according to the new value. Note that the action may
 *             delete existing ship units or add new ship units as needed to apply the change.
 *             Warning: Using this feature may impact or overwrite any changes made via the Shipment.ShipUnit element.
 * 
 *             The TransactionCode is used to specify if this is a modification, or the order line or ship unit should be added or removed.
 *          
 * 
 * <p>Classe Java de ShipmentModViaOrderLineType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="ShipmentModViaOrderLineType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ShipModViaOrderLineMatch" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ShipModViaOrderLineMatchType"/>
 *         &lt;element name="TransactionCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransactionCodeType" minOccurs="0"/>
 *         &lt;element name="AffectsCurrentLegOnly" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="WeightVolume" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}WeightVolumeType" minOccurs="0"/>
 *         &lt;element name="PackagedItemCount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ShipmentModViaOrderLineType", propOrder = {
    "shipModViaOrderLineMatch",
    "transactionCode",
    "affectsCurrentLegOnly",
    "weightVolume",
    "packagedItemCount"
})
public class ShipmentModViaOrderLineType {

    @XmlElement(name = "ShipModViaOrderLineMatch", required = true)
    protected ShipModViaOrderLineMatchType shipModViaOrderLineMatch;
    @XmlElement(name = "TransactionCode")
    @XmlSchemaType(name = "string")
    protected TransactionCodeType transactionCode;
    @XmlElement(name = "AffectsCurrentLegOnly")
    protected String affectsCurrentLegOnly;
    @XmlElement(name = "WeightVolume")
    protected WeightVolumeType weightVolume;
    @XmlElement(name = "PackagedItemCount")
    protected String packagedItemCount;

    /**
     * Obtém o valor da propriedade shipModViaOrderLineMatch.
     * 
     * @return
     *     possible object is
     *     {@link ShipModViaOrderLineMatchType }
     *     
     */
    public ShipModViaOrderLineMatchType getShipModViaOrderLineMatch() {
        return shipModViaOrderLineMatch;
    }

    /**
     * Define o valor da propriedade shipModViaOrderLineMatch.
     * 
     * @param value
     *     allowed object is
     *     {@link ShipModViaOrderLineMatchType }
     *     
     */
    public void setShipModViaOrderLineMatch(ShipModViaOrderLineMatchType value) {
        this.shipModViaOrderLineMatch = value;
    }

    /**
     * Obtém o valor da propriedade transactionCode.
     * 
     * @return
     *     possible object is
     *     {@link TransactionCodeType }
     *     
     */
    public TransactionCodeType getTransactionCode() {
        return transactionCode;
    }

    /**
     * Define o valor da propriedade transactionCode.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionCodeType }
     *     
     */
    public void setTransactionCode(TransactionCodeType value) {
        this.transactionCode = value;
    }

    /**
     * Obtém o valor da propriedade affectsCurrentLegOnly.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAffectsCurrentLegOnly() {
        return affectsCurrentLegOnly;
    }

    /**
     * Define o valor da propriedade affectsCurrentLegOnly.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAffectsCurrentLegOnly(String value) {
        this.affectsCurrentLegOnly = value;
    }

    /**
     * Obtém o valor da propriedade weightVolume.
     * 
     * @return
     *     possible object is
     *     {@link WeightVolumeType }
     *     
     */
    public WeightVolumeType getWeightVolume() {
        return weightVolume;
    }

    /**
     * Define o valor da propriedade weightVolume.
     * 
     * @param value
     *     allowed object is
     *     {@link WeightVolumeType }
     *     
     */
    public void setWeightVolume(WeightVolumeType value) {
        this.weightVolume = value;
    }

    /**
     * Obtém o valor da propriedade packagedItemCount.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPackagedItemCount() {
        return packagedItemCount;
    }

    /**
     * Define o valor da propriedade packagedItemCount.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPackagedItemCount(String value) {
        this.packagedItemCount = value;
    }

}
