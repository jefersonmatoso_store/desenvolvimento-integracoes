
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * ServiceProvider is an optional structure specified only if a location represents a service provider.
 * 
 * <p>Classe Java de ServiceProviderType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="ServiceProviderType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ScacGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="ServiceProviderAlias" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ServiceProviderAliasType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="ConditionalBookingProfile" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="TransportModeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *                   &lt;element name="ConditionalBookingProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="ModeProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="TenderResponseTime" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDurationType" minOccurs="0"/>
 *         &lt;element name="MatchRuleProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="MatchValidRuleProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="AutoApproveRuleProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="AutoPaymentFlag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AllocationRuleProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="IsAllowTender" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsDispatchByRegion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RateServiceGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="OutXmlProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="IsAcceptSpotBids" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsAcceptBroadcastTenders" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsAcceptCondBooking" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsLocalizeBroadcastSpotContact" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SpotRateAdjustmentFactor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InvoicingProcess" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsCopyInvDeltaToShipment" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsAcceptByShipUnit" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsInternalNVOCC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NVOCCDomainName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LineApproveTolProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="IsMinority" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Incumbent" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Tier" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ScorecardValue" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EpaIndexValue" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BroadcastSpotContact" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLContactRefType" minOccurs="0"/>
 *         &lt;element name="EquipMarkList" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}EquipMarkListType" minOccurs="0"/>
 *         &lt;element name="ServprovService" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="LocationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *                   &lt;element name="RateServiceGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="ServprovModeInfo" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="TransportModeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *                   &lt;element name="EquipmentGroupGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="ServprovPolicy" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="SequenceNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="PolicyType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="EffectiveDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType"/>
 *                   &lt;element name="ExpirationDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType"/>
 *                   &lt;element name="Issuer" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="PolicyNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="CoverageAmount" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="IsFleet" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AllowSpotRating" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceProviderType", propOrder = {
    "scacGid",
    "serviceProviderAlias",
    "conditionalBookingProfile",
    "modeProfileGid",
    "tenderResponseTime",
    "matchRuleProfileGid",
    "matchValidRuleProfileGid",
    "autoApproveRuleProfileGid",
    "autoPaymentFlag",
    "allocationRuleProfileGid",
    "isAllowTender",
    "isDispatchByRegion",
    "rateServiceGid",
    "outXmlProfileGid",
    "isAcceptSpotBids",
    "isAcceptBroadcastTenders",
    "isAcceptCondBooking",
    "isLocalizeBroadcastSpotContact",
    "spotRateAdjustmentFactor",
    "invoicingProcess",
    "isCopyInvDeltaToShipment",
    "isAcceptByShipUnit",
    "isInternalNVOCC",
    "nvoccDomainName",
    "lineApproveTolProfileGid",
    "isMinority",
    "incumbent",
    "tier",
    "scorecardValue",
    "epaIndexValue",
    "broadcastSpotContact",
    "equipMarkList",
    "servprovService",
    "servprovModeInfo",
    "servprovPolicy",
    "isFleet",
    "allowSpotRating"
})
public class ServiceProviderType {

    @XmlElement(name = "ScacGid")
    protected GLogXMLGidType scacGid;
    @XmlElement(name = "ServiceProviderAlias")
    protected List<ServiceProviderAliasType> serviceProviderAlias;
    @XmlElement(name = "ConditionalBookingProfile")
    protected List<ConditionalBookingProfile> conditionalBookingProfile;
    @XmlElement(name = "ModeProfileGid")
    protected GLogXMLGidType modeProfileGid;
    @XmlElement(name = "TenderResponseTime")
    protected GLogXMLDurationType tenderResponseTime;
    @XmlElement(name = "MatchRuleProfileGid")
    protected GLogXMLGidType matchRuleProfileGid;
    @XmlElement(name = "MatchValidRuleProfileGid")
    protected GLogXMLGidType matchValidRuleProfileGid;
    @XmlElement(name = "AutoApproveRuleProfileGid")
    protected GLogXMLGidType autoApproveRuleProfileGid;
    @XmlElement(name = "AutoPaymentFlag")
    protected String autoPaymentFlag;
    @XmlElement(name = "AllocationRuleProfileGid")
    protected GLogXMLGidType allocationRuleProfileGid;
    @XmlElement(name = "IsAllowTender")
    protected String isAllowTender;
    @XmlElement(name = "IsDispatchByRegion")
    protected String isDispatchByRegion;
    @XmlElement(name = "RateServiceGid")
    protected GLogXMLGidType rateServiceGid;
    @XmlElement(name = "OutXmlProfileGid")
    protected GLogXMLGidType outXmlProfileGid;
    @XmlElement(name = "IsAcceptSpotBids")
    protected String isAcceptSpotBids;
    @XmlElement(name = "IsAcceptBroadcastTenders")
    protected String isAcceptBroadcastTenders;
    @XmlElement(name = "IsAcceptCondBooking")
    protected String isAcceptCondBooking;
    @XmlElement(name = "IsLocalizeBroadcastSpotContact")
    protected String isLocalizeBroadcastSpotContact;
    @XmlElement(name = "SpotRateAdjustmentFactor")
    protected String spotRateAdjustmentFactor;
    @XmlElement(name = "InvoicingProcess")
    protected String invoicingProcess;
    @XmlElement(name = "IsCopyInvDeltaToShipment")
    protected String isCopyInvDeltaToShipment;
    @XmlElement(name = "IsAcceptByShipUnit")
    protected String isAcceptByShipUnit;
    @XmlElement(name = "IsInternalNVOCC")
    protected String isInternalNVOCC;
    @XmlElement(name = "NVOCCDomainName")
    protected String nvoccDomainName;
    @XmlElement(name = "LineApproveTolProfileGid")
    protected GLogXMLGidType lineApproveTolProfileGid;
    @XmlElement(name = "IsMinority")
    protected String isMinority;
    @XmlElement(name = "Incumbent")
    protected String incumbent;
    @XmlElement(name = "Tier")
    protected String tier;
    @XmlElement(name = "ScorecardValue")
    protected String scorecardValue;
    @XmlElement(name = "EpaIndexValue")
    protected String epaIndexValue;
    @XmlElement(name = "BroadcastSpotContact")
    protected GLogXMLContactRefType broadcastSpotContact;
    @XmlElement(name = "EquipMarkList")
    protected EquipMarkListType equipMarkList;
    @XmlElement(name = "ServprovService")
    protected List<ServprovService> servprovService;
    @XmlElement(name = "ServprovModeInfo")
    protected List<ServprovModeInfo> servprovModeInfo;
    @XmlElement(name = "ServprovPolicy")
    protected List<ServprovPolicy> servprovPolicy;
    @XmlElement(name = "IsFleet")
    protected String isFleet;
    @XmlElement(name = "AllowSpotRating")
    protected String allowSpotRating;

    /**
     * Obtém o valor da propriedade scacGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getScacGid() {
        return scacGid;
    }

    /**
     * Define o valor da propriedade scacGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setScacGid(GLogXMLGidType value) {
        this.scacGid = value;
    }

    /**
     * Gets the value of the serviceProviderAlias property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the serviceProviderAlias property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getServiceProviderAlias().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ServiceProviderAliasType }
     * 
     * 
     */
    public List<ServiceProviderAliasType> getServiceProviderAlias() {
        if (serviceProviderAlias == null) {
            serviceProviderAlias = new ArrayList<ServiceProviderAliasType>();
        }
        return this.serviceProviderAlias;
    }

    /**
     * Gets the value of the conditionalBookingProfile property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the conditionalBookingProfile property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getConditionalBookingProfile().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ConditionalBookingProfile }
     * 
     * 
     */
    public List<ConditionalBookingProfile> getConditionalBookingProfile() {
        if (conditionalBookingProfile == null) {
            conditionalBookingProfile = new ArrayList<ConditionalBookingProfile>();
        }
        return this.conditionalBookingProfile;
    }

    /**
     * Obtém o valor da propriedade modeProfileGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getModeProfileGid() {
        return modeProfileGid;
    }

    /**
     * Define o valor da propriedade modeProfileGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setModeProfileGid(GLogXMLGidType value) {
        this.modeProfileGid = value;
    }

    /**
     * Obtém o valor da propriedade tenderResponseTime.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public GLogXMLDurationType getTenderResponseTime() {
        return tenderResponseTime;
    }

    /**
     * Define o valor da propriedade tenderResponseTime.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public void setTenderResponseTime(GLogXMLDurationType value) {
        this.tenderResponseTime = value;
    }

    /**
     * Obtém o valor da propriedade matchRuleProfileGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getMatchRuleProfileGid() {
        return matchRuleProfileGid;
    }

    /**
     * Define o valor da propriedade matchRuleProfileGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setMatchRuleProfileGid(GLogXMLGidType value) {
        this.matchRuleProfileGid = value;
    }

    /**
     * Obtém o valor da propriedade matchValidRuleProfileGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getMatchValidRuleProfileGid() {
        return matchValidRuleProfileGid;
    }

    /**
     * Define o valor da propriedade matchValidRuleProfileGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setMatchValidRuleProfileGid(GLogXMLGidType value) {
        this.matchValidRuleProfileGid = value;
    }

    /**
     * Obtém o valor da propriedade autoApproveRuleProfileGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getAutoApproveRuleProfileGid() {
        return autoApproveRuleProfileGid;
    }

    /**
     * Define o valor da propriedade autoApproveRuleProfileGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setAutoApproveRuleProfileGid(GLogXMLGidType value) {
        this.autoApproveRuleProfileGid = value;
    }

    /**
     * Obtém o valor da propriedade autoPaymentFlag.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAutoPaymentFlag() {
        return autoPaymentFlag;
    }

    /**
     * Define o valor da propriedade autoPaymentFlag.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAutoPaymentFlag(String value) {
        this.autoPaymentFlag = value;
    }

    /**
     * Obtém o valor da propriedade allocationRuleProfileGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getAllocationRuleProfileGid() {
        return allocationRuleProfileGid;
    }

    /**
     * Define o valor da propriedade allocationRuleProfileGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setAllocationRuleProfileGid(GLogXMLGidType value) {
        this.allocationRuleProfileGid = value;
    }

    /**
     * Obtém o valor da propriedade isAllowTender.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsAllowTender() {
        return isAllowTender;
    }

    /**
     * Define o valor da propriedade isAllowTender.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsAllowTender(String value) {
        this.isAllowTender = value;
    }

    /**
     * Obtém o valor da propriedade isDispatchByRegion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsDispatchByRegion() {
        return isDispatchByRegion;
    }

    /**
     * Define o valor da propriedade isDispatchByRegion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsDispatchByRegion(String value) {
        this.isDispatchByRegion = value;
    }

    /**
     * Obtém o valor da propriedade rateServiceGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRateServiceGid() {
        return rateServiceGid;
    }

    /**
     * Define o valor da propriedade rateServiceGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRateServiceGid(GLogXMLGidType value) {
        this.rateServiceGid = value;
    }

    /**
     * Obtém o valor da propriedade outXmlProfileGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getOutXmlProfileGid() {
        return outXmlProfileGid;
    }

    /**
     * Define o valor da propriedade outXmlProfileGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setOutXmlProfileGid(GLogXMLGidType value) {
        this.outXmlProfileGid = value;
    }

    /**
     * Obtém o valor da propriedade isAcceptSpotBids.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsAcceptSpotBids() {
        return isAcceptSpotBids;
    }

    /**
     * Define o valor da propriedade isAcceptSpotBids.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsAcceptSpotBids(String value) {
        this.isAcceptSpotBids = value;
    }

    /**
     * Obtém o valor da propriedade isAcceptBroadcastTenders.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsAcceptBroadcastTenders() {
        return isAcceptBroadcastTenders;
    }

    /**
     * Define o valor da propriedade isAcceptBroadcastTenders.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsAcceptBroadcastTenders(String value) {
        this.isAcceptBroadcastTenders = value;
    }

    /**
     * Obtém o valor da propriedade isAcceptCondBooking.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsAcceptCondBooking() {
        return isAcceptCondBooking;
    }

    /**
     * Define o valor da propriedade isAcceptCondBooking.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsAcceptCondBooking(String value) {
        this.isAcceptCondBooking = value;
    }

    /**
     * Obtém o valor da propriedade isLocalizeBroadcastSpotContact.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsLocalizeBroadcastSpotContact() {
        return isLocalizeBroadcastSpotContact;
    }

    /**
     * Define o valor da propriedade isLocalizeBroadcastSpotContact.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsLocalizeBroadcastSpotContact(String value) {
        this.isLocalizeBroadcastSpotContact = value;
    }

    /**
     * Obtém o valor da propriedade spotRateAdjustmentFactor.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSpotRateAdjustmentFactor() {
        return spotRateAdjustmentFactor;
    }

    /**
     * Define o valor da propriedade spotRateAdjustmentFactor.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSpotRateAdjustmentFactor(String value) {
        this.spotRateAdjustmentFactor = value;
    }

    /**
     * Obtém o valor da propriedade invoicingProcess.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInvoicingProcess() {
        return invoicingProcess;
    }

    /**
     * Define o valor da propriedade invoicingProcess.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInvoicingProcess(String value) {
        this.invoicingProcess = value;
    }

    /**
     * Obtém o valor da propriedade isCopyInvDeltaToShipment.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsCopyInvDeltaToShipment() {
        return isCopyInvDeltaToShipment;
    }

    /**
     * Define o valor da propriedade isCopyInvDeltaToShipment.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsCopyInvDeltaToShipment(String value) {
        this.isCopyInvDeltaToShipment = value;
    }

    /**
     * Obtém o valor da propriedade isAcceptByShipUnit.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsAcceptByShipUnit() {
        return isAcceptByShipUnit;
    }

    /**
     * Define o valor da propriedade isAcceptByShipUnit.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsAcceptByShipUnit(String value) {
        this.isAcceptByShipUnit = value;
    }

    /**
     * Obtém o valor da propriedade isInternalNVOCC.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsInternalNVOCC() {
        return isInternalNVOCC;
    }

    /**
     * Define o valor da propriedade isInternalNVOCC.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsInternalNVOCC(String value) {
        this.isInternalNVOCC = value;
    }

    /**
     * Obtém o valor da propriedade nvoccDomainName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNVOCCDomainName() {
        return nvoccDomainName;
    }

    /**
     * Define o valor da propriedade nvoccDomainName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNVOCCDomainName(String value) {
        this.nvoccDomainName = value;
    }

    /**
     * Obtém o valor da propriedade lineApproveTolProfileGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getLineApproveTolProfileGid() {
        return lineApproveTolProfileGid;
    }

    /**
     * Define o valor da propriedade lineApproveTolProfileGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setLineApproveTolProfileGid(GLogXMLGidType value) {
        this.lineApproveTolProfileGid = value;
    }

    /**
     * Obtém o valor da propriedade isMinority.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsMinority() {
        return isMinority;
    }

    /**
     * Define o valor da propriedade isMinority.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsMinority(String value) {
        this.isMinority = value;
    }

    /**
     * Obtém o valor da propriedade incumbent.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIncumbent() {
        return incumbent;
    }

    /**
     * Define o valor da propriedade incumbent.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIncumbent(String value) {
        this.incumbent = value;
    }

    /**
     * Obtém o valor da propriedade tier.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTier() {
        return tier;
    }

    /**
     * Define o valor da propriedade tier.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTier(String value) {
        this.tier = value;
    }

    /**
     * Obtém o valor da propriedade scorecardValue.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getScorecardValue() {
        return scorecardValue;
    }

    /**
     * Define o valor da propriedade scorecardValue.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setScorecardValue(String value) {
        this.scorecardValue = value;
    }

    /**
     * Obtém o valor da propriedade epaIndexValue.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEpaIndexValue() {
        return epaIndexValue;
    }

    /**
     * Define o valor da propriedade epaIndexValue.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEpaIndexValue(String value) {
        this.epaIndexValue = value;
    }

    /**
     * Obtém o valor da propriedade broadcastSpotContact.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLContactRefType }
     *     
     */
    public GLogXMLContactRefType getBroadcastSpotContact() {
        return broadcastSpotContact;
    }

    /**
     * Define o valor da propriedade broadcastSpotContact.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLContactRefType }
     *     
     */
    public void setBroadcastSpotContact(GLogXMLContactRefType value) {
        this.broadcastSpotContact = value;
    }

    /**
     * Obtém o valor da propriedade equipMarkList.
     * 
     * @return
     *     possible object is
     *     {@link EquipMarkListType }
     *     
     */
    public EquipMarkListType getEquipMarkList() {
        return equipMarkList;
    }

    /**
     * Define o valor da propriedade equipMarkList.
     * 
     * @param value
     *     allowed object is
     *     {@link EquipMarkListType }
     *     
     */
    public void setEquipMarkList(EquipMarkListType value) {
        this.equipMarkList = value;
    }

    /**
     * Gets the value of the servprovService property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the servprovService property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getServprovService().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ServprovService }
     * 
     * 
     */
    public List<ServprovService> getServprovService() {
        if (servprovService == null) {
            servprovService = new ArrayList<ServprovService>();
        }
        return this.servprovService;
    }

    /**
     * Gets the value of the servprovModeInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the servprovModeInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getServprovModeInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ServprovModeInfo }
     * 
     * 
     */
    public List<ServprovModeInfo> getServprovModeInfo() {
        if (servprovModeInfo == null) {
            servprovModeInfo = new ArrayList<ServprovModeInfo>();
        }
        return this.servprovModeInfo;
    }

    /**
     * Gets the value of the servprovPolicy property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the servprovPolicy property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getServprovPolicy().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ServprovPolicy }
     * 
     * 
     */
    public List<ServprovPolicy> getServprovPolicy() {
        if (servprovPolicy == null) {
            servprovPolicy = new ArrayList<ServprovPolicy>();
        }
        return this.servprovPolicy;
    }

    /**
     * Obtém o valor da propriedade isFleet.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsFleet() {
        return isFleet;
    }

    /**
     * Define o valor da propriedade isFleet.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsFleet(String value) {
        this.isFleet = value;
    }

    /**
     * Obtém o valor da propriedade allowSpotRating.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAllowSpotRating() {
        return allowSpotRating;
    }

    /**
     * Define o valor da propriedade allowSpotRating.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAllowSpotRating(String value) {
        this.allowSpotRating = value;
    }


    /**
     * <p>Classe Java de anonymous complex type.
     * 
     * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="TransportModeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
     *         &lt;element name="ConditionalBookingProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "transportModeGid",
        "conditionalBookingProfileGid"
    })
    public static class ConditionalBookingProfile {

        @XmlElement(name = "TransportModeGid")
        protected GLogXMLGidType transportModeGid;
        @XmlElement(name = "ConditionalBookingProfileGid")
        protected GLogXMLGidType conditionalBookingProfileGid;

        /**
         * Obtém o valor da propriedade transportModeGid.
         * 
         * @return
         *     possible object is
         *     {@link GLogXMLGidType }
         *     
         */
        public GLogXMLGidType getTransportModeGid() {
            return transportModeGid;
        }

        /**
         * Define o valor da propriedade transportModeGid.
         * 
         * @param value
         *     allowed object is
         *     {@link GLogXMLGidType }
         *     
         */
        public void setTransportModeGid(GLogXMLGidType value) {
            this.transportModeGid = value;
        }

        /**
         * Obtém o valor da propriedade conditionalBookingProfileGid.
         * 
         * @return
         *     possible object is
         *     {@link GLogXMLGidType }
         *     
         */
        public GLogXMLGidType getConditionalBookingProfileGid() {
            return conditionalBookingProfileGid;
        }

        /**
         * Define o valor da propriedade conditionalBookingProfileGid.
         * 
         * @param value
         *     allowed object is
         *     {@link GLogXMLGidType }
         *     
         */
        public void setConditionalBookingProfileGid(GLogXMLGidType value) {
            this.conditionalBookingProfileGid = value;
        }

    }


    /**
     * <p>Classe Java de anonymous complex type.
     * 
     * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="TransportModeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
     *         &lt;element name="EquipmentGroupGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "transportModeGid",
        "equipmentGroupGid"
    })
    public static class ServprovModeInfo {

        @XmlElement(name = "TransportModeGid", required = true)
        protected GLogXMLGidType transportModeGid;
        @XmlElement(name = "EquipmentGroupGid")
        protected List<GLogXMLGidType> equipmentGroupGid;

        /**
         * Obtém o valor da propriedade transportModeGid.
         * 
         * @return
         *     possible object is
         *     {@link GLogXMLGidType }
         *     
         */
        public GLogXMLGidType getTransportModeGid() {
            return transportModeGid;
        }

        /**
         * Define o valor da propriedade transportModeGid.
         * 
         * @param value
         *     allowed object is
         *     {@link GLogXMLGidType }
         *     
         */
        public void setTransportModeGid(GLogXMLGidType value) {
            this.transportModeGid = value;
        }

        /**
         * Gets the value of the equipmentGroupGid property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the equipmentGroupGid property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getEquipmentGroupGid().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link GLogXMLGidType }
         * 
         * 
         */
        public List<GLogXMLGidType> getEquipmentGroupGid() {
            if (equipmentGroupGid == null) {
                equipmentGroupGid = new ArrayList<GLogXMLGidType>();
            }
            return this.equipmentGroupGid;
        }

    }


    /**
     * <p>Classe Java de anonymous complex type.
     * 
     * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="SequenceNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="PolicyType" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="EffectiveDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType"/>
     *         &lt;element name="ExpirationDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType"/>
     *         &lt;element name="Issuer" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="PolicyNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="CoverageAmount" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "sequenceNumber",
        "policyType",
        "effectiveDate",
        "expirationDate",
        "issuer",
        "policyNumber",
        "coverageAmount"
    })
    public static class ServprovPolicy {

        @XmlElement(name = "SequenceNumber")
        protected String sequenceNumber;
        @XmlElement(name = "PolicyType", required = true)
        protected String policyType;
        @XmlElement(name = "EffectiveDate", required = true)
        protected GLogDateTimeType effectiveDate;
        @XmlElement(name = "ExpirationDate", required = true)
        protected GLogDateTimeType expirationDate;
        @XmlElement(name = "Issuer")
        protected String issuer;
        @XmlElement(name = "PolicyNumber")
        protected String policyNumber;
        @XmlElement(name = "CoverageAmount")
        protected GLogXMLFinancialAmountType coverageAmount;

        /**
         * Obtém o valor da propriedade sequenceNumber.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSequenceNumber() {
            return sequenceNumber;
        }

        /**
         * Define o valor da propriedade sequenceNumber.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSequenceNumber(String value) {
            this.sequenceNumber = value;
        }

        /**
         * Obtém o valor da propriedade policyType.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPolicyType() {
            return policyType;
        }

        /**
         * Define o valor da propriedade policyType.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPolicyType(String value) {
            this.policyType = value;
        }

        /**
         * Obtém o valor da propriedade effectiveDate.
         * 
         * @return
         *     possible object is
         *     {@link GLogDateTimeType }
         *     
         */
        public GLogDateTimeType getEffectiveDate() {
            return effectiveDate;
        }

        /**
         * Define o valor da propriedade effectiveDate.
         * 
         * @param value
         *     allowed object is
         *     {@link GLogDateTimeType }
         *     
         */
        public void setEffectiveDate(GLogDateTimeType value) {
            this.effectiveDate = value;
        }

        /**
         * Obtém o valor da propriedade expirationDate.
         * 
         * @return
         *     possible object is
         *     {@link GLogDateTimeType }
         *     
         */
        public GLogDateTimeType getExpirationDate() {
            return expirationDate;
        }

        /**
         * Define o valor da propriedade expirationDate.
         * 
         * @param value
         *     allowed object is
         *     {@link GLogDateTimeType }
         *     
         */
        public void setExpirationDate(GLogDateTimeType value) {
            this.expirationDate = value;
        }

        /**
         * Obtém o valor da propriedade issuer.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getIssuer() {
            return issuer;
        }

        /**
         * Define o valor da propriedade issuer.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setIssuer(String value) {
            this.issuer = value;
        }

        /**
         * Obtém o valor da propriedade policyNumber.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPolicyNumber() {
            return policyNumber;
        }

        /**
         * Define o valor da propriedade policyNumber.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPolicyNumber(String value) {
            this.policyNumber = value;
        }

        /**
         * Obtém o valor da propriedade coverageAmount.
         * 
         * @return
         *     possible object is
         *     {@link GLogXMLFinancialAmountType }
         *     
         */
        public GLogXMLFinancialAmountType getCoverageAmount() {
            return coverageAmount;
        }

        /**
         * Define o valor da propriedade coverageAmount.
         * 
         * @param value
         *     allowed object is
         *     {@link GLogXMLFinancialAmountType }
         *     
         */
        public void setCoverageAmount(GLogXMLFinancialAmountType value) {
            this.coverageAmount = value;
        }

    }


    /**
     * <p>Classe Java de anonymous complex type.
     * 
     * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="LocationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
     *         &lt;element name="RateServiceGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "locationGid",
        "rateServiceGid"
    })
    public static class ServprovService {

        @XmlElement(name = "LocationGid", required = true)
        protected GLogXMLGidType locationGid;
        @XmlElement(name = "RateServiceGid", required = true)
        protected GLogXMLGidType rateServiceGid;

        /**
         * Obtém o valor da propriedade locationGid.
         * 
         * @return
         *     possible object is
         *     {@link GLogXMLGidType }
         *     
         */
        public GLogXMLGidType getLocationGid() {
            return locationGid;
        }

        /**
         * Define o valor da propriedade locationGid.
         * 
         * @param value
         *     allowed object is
         *     {@link GLogXMLGidType }
         *     
         */
        public void setLocationGid(GLogXMLGidType value) {
            this.locationGid = value;
        }

        /**
         * Obtém o valor da propriedade rateServiceGid.
         * 
         * @return
         *     possible object is
         *     {@link GLogXMLGidType }
         *     
         */
        public GLogXMLGidType getRateServiceGid() {
            return rateServiceGid;
        }

        /**
         * Define o valor da propriedade rateServiceGid.
         * 
         * @param value
         *     allowed object is
         *     {@link GLogXMLGidType }
         *     
         */
        public void setRateServiceGid(GLogXMLGidType value) {
            this.rateServiceGid = value;
        }

    }

}
