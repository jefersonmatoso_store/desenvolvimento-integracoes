
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.*;


/**
 * <p>Classe Java de DriverAssignmentType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="DriverAssignmentType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DriverAssignmtSeqNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="TransactionCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransactionCodeType"/>
 *         &lt;element name="IntSavedQuery" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}IntSavedQueryType" minOccurs="0"/>
 *         &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ShipmentGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="NextAvailLocGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="NextAvailTime" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType"/>
 *         &lt;element name="PrevDriverAssignmtSeqNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="IsSystemGenerated" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DriverAssignmentType", propOrder = {
    "driverAssignmtSeqNo",
    "transactionCode",
    "intSavedQuery",
    "description",
    "shipmentGid",
    "nextAvailLocGid",
    "nextAvailTime",
    "prevDriverAssignmtSeqNo",
    "isSystemGenerated"
})
public class DriverAssignmentType {

    @XmlElement(name = "DriverAssignmtSeqNo", required = true)
    protected String driverAssignmtSeqNo;
    @XmlElement(name = "TransactionCode", required = true)
    @XmlSchemaType(name = "string")
    protected TransactionCodeType transactionCode;
    @XmlElement(name = "IntSavedQuery")
    protected IntSavedQueryType intSavedQuery;
    @XmlElement(name = "Description", required = true)
    protected String description;
    @XmlElement(name = "ShipmentGid", required = true)
    protected GLogXMLGidType shipmentGid;
    @XmlElement(name = "NextAvailLocGid", required = true)
    protected GLogXMLGidType nextAvailLocGid;
    @XmlElement(name = "NextAvailTime", required = true)
    protected GLogDateTimeType nextAvailTime;
    @XmlElement(name = "PrevDriverAssignmtSeqNo", required = true)
    protected String prevDriverAssignmtSeqNo;
    @XmlElement(name = "IsSystemGenerated", required = true)
    protected String isSystemGenerated;

    /**
     * Obtém o valor da propriedade driverAssignmtSeqNo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDriverAssignmtSeqNo() {
        return driverAssignmtSeqNo;
    }

    /**
     * Define o valor da propriedade driverAssignmtSeqNo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDriverAssignmtSeqNo(String value) {
        this.driverAssignmtSeqNo = value;
    }

    /**
     * Obtém o valor da propriedade transactionCode.
     * 
     * @return
     *     possible object is
     *     {@link TransactionCodeType }
     *     
     */
    public TransactionCodeType getTransactionCode() {
        return transactionCode;
    }

    /**
     * Define o valor da propriedade transactionCode.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionCodeType }
     *     
     */
    public void setTransactionCode(TransactionCodeType value) {
        this.transactionCode = value;
    }

    /**
     * Obtém o valor da propriedade intSavedQuery.
     * 
     * @return
     *     possible object is
     *     {@link IntSavedQueryType }
     *     
     */
    public IntSavedQueryType getIntSavedQuery() {
        return intSavedQuery;
    }

    /**
     * Define o valor da propriedade intSavedQuery.
     * 
     * @param value
     *     allowed object is
     *     {@link IntSavedQueryType }
     *     
     */
    public void setIntSavedQuery(IntSavedQueryType value) {
        this.intSavedQuery = value;
    }

    /**
     * Obtém o valor da propriedade description.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Define o valor da propriedade description.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Obtém o valor da propriedade shipmentGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getShipmentGid() {
        return shipmentGid;
    }

    /**
     * Define o valor da propriedade shipmentGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setShipmentGid(GLogXMLGidType value) {
        this.shipmentGid = value;
    }

    /**
     * Obtém o valor da propriedade nextAvailLocGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getNextAvailLocGid() {
        return nextAvailLocGid;
    }

    /**
     * Define o valor da propriedade nextAvailLocGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setNextAvailLocGid(GLogXMLGidType value) {
        this.nextAvailLocGid = value;
    }

    /**
     * Obtém o valor da propriedade nextAvailTime.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getNextAvailTime() {
        return nextAvailTime;
    }

    /**
     * Define o valor da propriedade nextAvailTime.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setNextAvailTime(GLogDateTimeType value) {
        this.nextAvailTime = value;
    }

    /**
     * Obtém o valor da propriedade prevDriverAssignmtSeqNo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrevDriverAssignmtSeqNo() {
        return prevDriverAssignmtSeqNo;
    }

    /**
     * Define o valor da propriedade prevDriverAssignmtSeqNo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrevDriverAssignmtSeqNo(String value) {
        this.prevDriverAssignmtSeqNo = value;
    }

    /**
     * Obtém o valor da propriedade isSystemGenerated.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsSystemGenerated() {
        return isSystemGenerated;
    }

    /**
     * Define o valor da propriedade isSystemGenerated.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsSystemGenerated(String value) {
        this.isSystemGenerated = value;
    }

}
