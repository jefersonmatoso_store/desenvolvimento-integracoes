
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 *             DriverRefnum is a qualifier/value pair used to refer to a driver.
 *          
 * 
 * <p>Classe Java de DriverRefnumType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="DriverRefnumType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DriverRefnumQualGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="DriverRefnumValue" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DriverRefnumType", propOrder = {
    "driverRefnumQualGid",
    "driverRefnumValue"
})
public class DriverRefnumType {

    @XmlElement(name = "DriverRefnumQualGid", required = true)
    protected GLogXMLGidType driverRefnumQualGid;
    @XmlElement(name = "DriverRefnumValue", required = true)
    protected String driverRefnumValue;

    /**
     * Obtém o valor da propriedade driverRefnumQualGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getDriverRefnumQualGid() {
        return driverRefnumQualGid;
    }

    /**
     * Define o valor da propriedade driverRefnumQualGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setDriverRefnumQualGid(GLogXMLGidType value) {
        this.driverRefnumQualGid = value;
    }

    /**
     * Obtém o valor da propriedade driverRefnumValue.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDriverRefnumValue() {
        return driverRefnumValue;
    }

    /**
     * Define o valor da propriedade driverRefnumValue.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDriverRefnumValue(String value) {
        this.driverRefnumValue = value;
    }

}
