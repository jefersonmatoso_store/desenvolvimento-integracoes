
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Start Time and Time Zone.
 * 
 * <p>Classe Java de TimeWithTimezoneType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="TimeWithTimezoneType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GLogDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TimeZoneGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TimeWithTimezoneType", propOrder = {
    "gLogDate",
    "timeZoneGid"
})
public class TimeWithTimezoneType {

    @XmlElement(name = "GLogDate")
    protected String gLogDate;
    @XmlElement(name = "TimeZoneGid")
    protected GLogXMLGidType timeZoneGid;

    /**
     * Obtém o valor da propriedade gLogDate.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGLogDate() {
        return gLogDate;
    }

    /**
     * Define o valor da propriedade gLogDate.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGLogDate(String value) {
        this.gLogDate = value;
    }

    /**
     * Obtém o valor da propriedade timeZoneGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getTimeZoneGid() {
        return timeZoneGid;
    }

    /**
     * Define o valor da propriedade timeZoneGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setTimeZoneGid(GLogXMLGidType value) {
        this.timeZoneGid = value;
    }

}
