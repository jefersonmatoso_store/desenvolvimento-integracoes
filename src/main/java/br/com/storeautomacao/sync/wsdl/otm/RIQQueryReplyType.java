
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * RIQQueryReply: Rate Inquiry Reply.
 * 
 * <p>Classe Java de RIQQueryReplyType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="RIQQueryReplyType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RIQQuery" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RIQQueryType"/>
 *         &lt;element name="RIQQueryInfo" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RIQQueryInfoType" minOccurs="0"/>
 *         &lt;choice minOccurs="0">
 *           &lt;element name="RIQResult" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RIQResultType" maxOccurs="unbounded" minOccurs="0"/>
 *           &lt;element name="RIQResultRoute" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RIQResultRouteType" maxOccurs="unbounded" minOccurs="0"/>
 *           &lt;element name="RIQResultRouteNR" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RIQResultRouteNRType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;/choice>
 *         &lt;element name="RemoteQueryStatus" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RemoteQueryStatusType" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RIQQueryReplyType", propOrder = {
    "riqQuery",
    "riqQueryInfo",
    "riqResult",
    "riqResultRoute",
    "riqResultRouteNR",
    "remoteQueryStatus"
})
public class RIQQueryReplyType {

    @XmlElement(name = "RIQQuery", required = true)
    protected RIQQueryType riqQuery;
    @XmlElement(name = "RIQQueryInfo")
    protected RIQQueryInfoType riqQueryInfo;
    @XmlElement(name = "RIQResult")
    protected List<RIQResultType> riqResult;
    @XmlElement(name = "RIQResultRoute")
    protected List<RIQResultRouteType> riqResultRoute;
    @XmlElement(name = "RIQResultRouteNR")
    protected List<RIQResultRouteNRType> riqResultRouteNR;
    @XmlElement(name = "RemoteQueryStatus", required = true)
    protected List<RemoteQueryStatusType> remoteQueryStatus;

    /**
     * Obtém o valor da propriedade riqQuery.
     * 
     * @return
     *     possible object is
     *     {@link RIQQueryType }
     *     
     */
    public RIQQueryType getRIQQuery() {
        return riqQuery;
    }

    /**
     * Define o valor da propriedade riqQuery.
     * 
     * @param value
     *     allowed object is
     *     {@link RIQQueryType }
     *     
     */
    public void setRIQQuery(RIQQueryType value) {
        this.riqQuery = value;
    }

    /**
     * Obtém o valor da propriedade riqQueryInfo.
     * 
     * @return
     *     possible object is
     *     {@link RIQQueryInfoType }
     *     
     */
    public RIQQueryInfoType getRIQQueryInfo() {
        return riqQueryInfo;
    }

    /**
     * Define o valor da propriedade riqQueryInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link RIQQueryInfoType }
     *     
     */
    public void setRIQQueryInfo(RIQQueryInfoType value) {
        this.riqQueryInfo = value;
    }

    /**
     * Gets the value of the riqResult property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the riqResult property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRIQResult().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RIQResultType }
     * 
     * 
     */
    public List<RIQResultType> getRIQResult() {
        if (riqResult == null) {
            riqResult = new ArrayList<RIQResultType>();
        }
        return this.riqResult;
    }

    /**
     * Gets the value of the riqResultRoute property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the riqResultRoute property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRIQResultRoute().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RIQResultRouteType }
     * 
     * 
     */
    public List<RIQResultRouteType> getRIQResultRoute() {
        if (riqResultRoute == null) {
            riqResultRoute = new ArrayList<RIQResultRouteType>();
        }
        return this.riqResultRoute;
    }

    /**
     * Gets the value of the riqResultRouteNR property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the riqResultRouteNR property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRIQResultRouteNR().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RIQResultRouteNRType }
     * 
     * 
     */
    public List<RIQResultRouteNRType> getRIQResultRouteNR() {
        if (riqResultRouteNR == null) {
            riqResultRouteNR = new ArrayList<RIQResultRouteNRType>();
        }
        return this.riqResultRouteNR;
    }

    /**
     * Gets the value of the remoteQueryStatus property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the remoteQueryStatus property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRemoteQueryStatus().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RemoteQueryStatusType }
     * 
     * 
     */
    public List<RemoteQueryStatusType> getRemoteQueryStatus() {
        if (remoteQueryStatus == null) {
            remoteQueryStatus = new ArrayList<RemoteQueryStatusType>();
        }
        return this.remoteQueryStatus;
    }

}
