
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 *             FP_Tarriff is the tariff data from the contract (rate offering) used to rate the shipment associated with this invoice or bill.
 *          
 * 
 * <p>Classe Java de FPTariffType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="FPTariffType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TariffAgencyCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="IssuingCarrierID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TariffRefnum" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TariffRefnumType"/>
 *         &lt;element name="PrimaryPubAuthority" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RegulatoryAgencyCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TariffItemNum" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TariffItemNumType" minOccurs="0"/>
 *         &lt;element name="TariffItemPart" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FreightClassCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TariffSuppID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TariffSection" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TariffEffectiveDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FPTariffType", propOrder = {
    "tariffAgencyCode",
    "issuingCarrierID",
    "tariffRefnum",
    "primaryPubAuthority",
    "regulatoryAgencyCode",
    "tariffItemNum",
    "tariffItemPart",
    "freightClassCode",
    "tariffSuppID",
    "tariffSection",
    "tariffEffectiveDate"
})
public class FPTariffType {

    @XmlElement(name = "TariffAgencyCode", required = true)
    protected String tariffAgencyCode;
    @XmlElement(name = "IssuingCarrierID")
    protected String issuingCarrierID;
    @XmlElement(name = "TariffRefnum", required = true)
    protected TariffRefnumType tariffRefnum;
    @XmlElement(name = "PrimaryPubAuthority")
    protected String primaryPubAuthority;
    @XmlElement(name = "RegulatoryAgencyCode")
    protected String regulatoryAgencyCode;
    @XmlElement(name = "TariffItemNum")
    protected TariffItemNumType tariffItemNum;
    @XmlElement(name = "TariffItemPart")
    protected String tariffItemPart;
    @XmlElement(name = "FreightClassCode")
    protected String freightClassCode;
    @XmlElement(name = "TariffSuppID")
    protected String tariffSuppID;
    @XmlElement(name = "TariffSection")
    protected String tariffSection;
    @XmlElement(name = "TariffEffectiveDate")
    protected GLogDateTimeType tariffEffectiveDate;

    /**
     * Obtém o valor da propriedade tariffAgencyCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTariffAgencyCode() {
        return tariffAgencyCode;
    }

    /**
     * Define o valor da propriedade tariffAgencyCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTariffAgencyCode(String value) {
        this.tariffAgencyCode = value;
    }

    /**
     * Obtém o valor da propriedade issuingCarrierID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIssuingCarrierID() {
        return issuingCarrierID;
    }

    /**
     * Define o valor da propriedade issuingCarrierID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIssuingCarrierID(String value) {
        this.issuingCarrierID = value;
    }

    /**
     * Obtém o valor da propriedade tariffRefnum.
     * 
     * @return
     *     possible object is
     *     {@link TariffRefnumType }
     *     
     */
    public TariffRefnumType getTariffRefnum() {
        return tariffRefnum;
    }

    /**
     * Define o valor da propriedade tariffRefnum.
     * 
     * @param value
     *     allowed object is
     *     {@link TariffRefnumType }
     *     
     */
    public void setTariffRefnum(TariffRefnumType value) {
        this.tariffRefnum = value;
    }

    /**
     * Obtém o valor da propriedade primaryPubAuthority.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrimaryPubAuthority() {
        return primaryPubAuthority;
    }

    /**
     * Define o valor da propriedade primaryPubAuthority.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrimaryPubAuthority(String value) {
        this.primaryPubAuthority = value;
    }

    /**
     * Obtém o valor da propriedade regulatoryAgencyCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRegulatoryAgencyCode() {
        return regulatoryAgencyCode;
    }

    /**
     * Define o valor da propriedade regulatoryAgencyCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegulatoryAgencyCode(String value) {
        this.regulatoryAgencyCode = value;
    }

    /**
     * Obtém o valor da propriedade tariffItemNum.
     * 
     * @return
     *     possible object is
     *     {@link TariffItemNumType }
     *     
     */
    public TariffItemNumType getTariffItemNum() {
        return tariffItemNum;
    }

    /**
     * Define o valor da propriedade tariffItemNum.
     * 
     * @param value
     *     allowed object is
     *     {@link TariffItemNumType }
     *     
     */
    public void setTariffItemNum(TariffItemNumType value) {
        this.tariffItemNum = value;
    }

    /**
     * Obtém o valor da propriedade tariffItemPart.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTariffItemPart() {
        return tariffItemPart;
    }

    /**
     * Define o valor da propriedade tariffItemPart.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTariffItemPart(String value) {
        this.tariffItemPart = value;
    }

    /**
     * Obtém o valor da propriedade freightClassCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFreightClassCode() {
        return freightClassCode;
    }

    /**
     * Define o valor da propriedade freightClassCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFreightClassCode(String value) {
        this.freightClassCode = value;
    }

    /**
     * Obtém o valor da propriedade tariffSuppID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTariffSuppID() {
        return tariffSuppID;
    }

    /**
     * Define o valor da propriedade tariffSuppID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTariffSuppID(String value) {
        this.tariffSuppID = value;
    }

    /**
     * Obtém o valor da propriedade tariffSection.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTariffSection() {
        return tariffSection;
    }

    /**
     * Define o valor da propriedade tariffSection.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTariffSection(String value) {
        this.tariffSection = value;
    }

    /**
     * Obtém o valor da propriedade tariffEffectiveDate.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getTariffEffectiveDate() {
        return tariffEffectiveDate;
    }

    /**
     * Define o valor da propriedade tariffEffectiveDate.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setTariffEffectiveDate(GLogDateTimeType value) {
        this.tariffEffectiveDate = value;
    }

}
