
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.*;


/**
 * Provides an alternative for updating actual quantites for a Shipment by referencing the order ship unit.
 *             The ShipModViaOrderSUMatch element provides the fields that are used to match the affected ship units.
 *             The quantities on the ship units that are matched are updated according to the new value. Note that the action may
 *             delete existing ship units or add new ship units as needed to apply the change.
 *             Warning: Using this feature may impact or overwrite any changes made via the Shipment.ShipUnit element.
 * 
 *             The TransactionCode is used to specify if this is a modification, or the order line or ship unit should be added or removed.
 *          
 * 
 * <p>Classe Java de ShipmentModViaOrderSUType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="ShipmentModViaOrderSUType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ShipModViaOrderSUMatch" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ShipModViaOrderSUMatchType"/>
 *         &lt;element name="TransactionCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransactionCodeType" minOccurs="0"/>
 *         &lt;element name="AffectsCurrentLegOnly" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ShipUnitCount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LengthWidthHeight" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LengthWidthHeightType" minOccurs="0"/>
 *         &lt;element name="UnitNetWeightVolume" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}WeightVolumeType" minOccurs="0"/>
 *         &lt;element name="UnitWeightVolume" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}WeightVolumeType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ShipmentModViaOrderSUType", propOrder = {
    "shipModViaOrderSUMatch",
    "transactionCode",
    "affectsCurrentLegOnly",
    "shipUnitCount",
    "lengthWidthHeight",
    "unitNetWeightVolume",
    "unitWeightVolume"
})
public class ShipmentModViaOrderSUType {

    @XmlElement(name = "ShipModViaOrderSUMatch", required = true)
    protected ShipModViaOrderSUMatchType shipModViaOrderSUMatch;
    @XmlElement(name = "TransactionCode")
    @XmlSchemaType(name = "string")
    protected TransactionCodeType transactionCode;
    @XmlElement(name = "AffectsCurrentLegOnly")
    protected String affectsCurrentLegOnly;
    @XmlElement(name = "ShipUnitCount")
    protected String shipUnitCount;
    @XmlElement(name = "LengthWidthHeight")
    protected LengthWidthHeightType lengthWidthHeight;
    @XmlElement(name = "UnitNetWeightVolume")
    protected WeightVolumeType unitNetWeightVolume;
    @XmlElement(name = "UnitWeightVolume")
    protected WeightVolumeType unitWeightVolume;

    /**
     * Obtém o valor da propriedade shipModViaOrderSUMatch.
     * 
     * @return
     *     possible object is
     *     {@link ShipModViaOrderSUMatchType }
     *     
     */
    public ShipModViaOrderSUMatchType getShipModViaOrderSUMatch() {
        return shipModViaOrderSUMatch;
    }

    /**
     * Define o valor da propriedade shipModViaOrderSUMatch.
     * 
     * @param value
     *     allowed object is
     *     {@link ShipModViaOrderSUMatchType }
     *     
     */
    public void setShipModViaOrderSUMatch(ShipModViaOrderSUMatchType value) {
        this.shipModViaOrderSUMatch = value;
    }

    /**
     * Obtém o valor da propriedade transactionCode.
     * 
     * @return
     *     possible object is
     *     {@link TransactionCodeType }
     *     
     */
    public TransactionCodeType getTransactionCode() {
        return transactionCode;
    }

    /**
     * Define o valor da propriedade transactionCode.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionCodeType }
     *     
     */
    public void setTransactionCode(TransactionCodeType value) {
        this.transactionCode = value;
    }

    /**
     * Obtém o valor da propriedade affectsCurrentLegOnly.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAffectsCurrentLegOnly() {
        return affectsCurrentLegOnly;
    }

    /**
     * Define o valor da propriedade affectsCurrentLegOnly.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAffectsCurrentLegOnly(String value) {
        this.affectsCurrentLegOnly = value;
    }

    /**
     * Obtém o valor da propriedade shipUnitCount.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipUnitCount() {
        return shipUnitCount;
    }

    /**
     * Define o valor da propriedade shipUnitCount.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipUnitCount(String value) {
        this.shipUnitCount = value;
    }

    /**
     * Obtém o valor da propriedade lengthWidthHeight.
     * 
     * @return
     *     possible object is
     *     {@link LengthWidthHeightType }
     *     
     */
    public LengthWidthHeightType getLengthWidthHeight() {
        return lengthWidthHeight;
    }

    /**
     * Define o valor da propriedade lengthWidthHeight.
     * 
     * @param value
     *     allowed object is
     *     {@link LengthWidthHeightType }
     *     
     */
    public void setLengthWidthHeight(LengthWidthHeightType value) {
        this.lengthWidthHeight = value;
    }

    /**
     * Obtém o valor da propriedade unitNetWeightVolume.
     * 
     * @return
     *     possible object is
     *     {@link WeightVolumeType }
     *     
     */
    public WeightVolumeType getUnitNetWeightVolume() {
        return unitNetWeightVolume;
    }

    /**
     * Define o valor da propriedade unitNetWeightVolume.
     * 
     * @param value
     *     allowed object is
     *     {@link WeightVolumeType }
     *     
     */
    public void setUnitNetWeightVolume(WeightVolumeType value) {
        this.unitNetWeightVolume = value;
    }

    /**
     * Obtém o valor da propriedade unitWeightVolume.
     * 
     * @return
     *     possible object is
     *     {@link WeightVolumeType }
     *     
     */
    public WeightVolumeType getUnitWeightVolume() {
        return unitWeightVolume;
    }

    /**
     * Define o valor da propriedade unitWeightVolume.
     * 
     * @param value
     *     allowed object is
     *     {@link WeightVolumeType }
     *     
     */
    public void setUnitWeightVolume(WeightVolumeType value) {
        this.unitWeightVolume = value;
    }

}
