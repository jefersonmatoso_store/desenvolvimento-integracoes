
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Price per unit.
 *             Note: The UOM is only valid when the PricePerUnit is in the ReleaseLine element.
 *          
 * 
 * <p>Classe Java de PricePerUnitType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="PricePerUnitType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FinancialAmount" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FinancialAmountType"/>
 *         &lt;element name="UOM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PricePerUnitType", propOrder = {
    "financialAmount",
    "uom"
})
public class PricePerUnitType {

    @XmlElement(name = "FinancialAmount", required = true)
    protected FinancialAmountType financialAmount;
    @XmlElement(name = "UOM")
    protected String uom;

    /**
     * Obtém o valor da propriedade financialAmount.
     * 
     * @return
     *     possible object is
     *     {@link FinancialAmountType }
     *     
     */
    public FinancialAmountType getFinancialAmount() {
        return financialAmount;
    }

    /**
     * Define o valor da propriedade financialAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link FinancialAmountType }
     *     
     */
    public void setFinancialAmount(FinancialAmountType value) {
        this.financialAmount = value;
    }

    /**
     * Obtém o valor da propriedade uom.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUOM() {
        return uom;
    }

    /**
     * Define o valor da propriedade uom.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUOM(String value) {
        this.uom = value;
    }

}
