
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * Integration Command.
 *             When the IntCommandName = "PropagateShipUnitChanges", IntArg would support ArgName field "Type" and ArgValue with the following values:
 *             None, Upstream, Downstream, UpAndDownstream
 *             For backward compatibility, the default value for Type will be Downstream.
 *          
 * 
 * <p>Classe Java de IntCommandType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="IntCommandType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IntCommandName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="IntArg" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="ArgName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="ArgValue" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IntCommandType", propOrder = {
    "intCommandName",
    "intArg"
})
public class IntCommandType {

    @XmlElement(name = "IntCommandName", required = true)
    protected String intCommandName;
    @XmlElement(name = "IntArg")
    protected List<IntArg> intArg;

    /**
     * Obtém o valor da propriedade intCommandName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIntCommandName() {
        return intCommandName;
    }

    /**
     * Define o valor da propriedade intCommandName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIntCommandName(String value) {
        this.intCommandName = value;
    }

    /**
     * Gets the value of the intArg property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the intArg property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getIntArg().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link IntArg }
     * 
     * 
     */
    public List<IntArg> getIntArg() {
        if (intArg == null) {
            intArg = new ArrayList<IntArg>();
        }
        return this.intArg;
    }


    /**
     * <p>Classe Java de anonymous complex type.
     * 
     * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="ArgName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="ArgValue" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "argName",
        "argValue"
    })
    public static class IntArg {

        @XmlElement(name = "ArgName", required = true)
        protected String argName;
        @XmlElement(name = "ArgValue", required = true)
        protected String argValue;

        /**
         * Obtém o valor da propriedade argName.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getArgName() {
            return argName;
        }

        /**
         * Define o valor da propriedade argName.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setArgName(String value) {
            this.argName = value;
        }

        /**
         * Obtém o valor da propriedade argValue.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getArgValue() {
            return argValue;
        }

        /**
         * Define o valor da propriedade argValue.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setArgValue(String value) {
            this.argValue = value;
        }

    }

}
