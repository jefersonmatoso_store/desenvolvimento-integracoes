
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 *             (Both) General User Defined Code pertaining to business objects, including but not limited to: end use code,
 *             end user code, product type code, transaction code, etc.
 *          
 * 
 * <p>Classe Java de UserDefinedClassificationType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="UserDefinedClassificationType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="UserDefinedCategoryGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="UserDefinedType" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}UserDefinedTypeType"/>
 *         &lt;element name="UserDefinedCode" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}UserDefinedCodeType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserDefinedClassificationType", namespace = "http://xmlns.oracle.com/apps/gtm/transmission/v6.4", propOrder = {
    "userDefinedCategoryGid",
    "userDefinedType",
    "userDefinedCode"
})
public class UserDefinedClassificationType {

    @XmlElement(name = "UserDefinedCategoryGid")
    protected GLogXMLGidType userDefinedCategoryGid;
    @XmlElement(name = "UserDefinedType", required = true)
    protected UserDefinedTypeType userDefinedType;
    @XmlElement(name = "UserDefinedCode", required = true)
    protected UserDefinedCodeType userDefinedCode;

    /**
     * Obtém o valor da propriedade userDefinedCategoryGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getUserDefinedCategoryGid() {
        return userDefinedCategoryGid;
    }

    /**
     * Define o valor da propriedade userDefinedCategoryGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setUserDefinedCategoryGid(GLogXMLGidType value) {
        this.userDefinedCategoryGid = value;
    }

    /**
     * Obtém o valor da propriedade userDefinedType.
     * 
     * @return
     *     possible object is
     *     {@link UserDefinedTypeType }
     *     
     */
    public UserDefinedTypeType getUserDefinedType() {
        return userDefinedType;
    }

    /**
     * Define o valor da propriedade userDefinedType.
     * 
     * @param value
     *     allowed object is
     *     {@link UserDefinedTypeType }
     *     
     */
    public void setUserDefinedType(UserDefinedTypeType value) {
        this.userDefinedType = value;
    }

    /**
     * Obtém o valor da propriedade userDefinedCode.
     * 
     * @return
     *     possible object is
     *     {@link UserDefinedCodeType }
     *     
     */
    public UserDefinedCodeType getUserDefinedCode() {
        return userDefinedCode;
    }

    /**
     * Define o valor da propriedade userDefinedCode.
     * 
     * @param value
     *     allowed object is
     *     {@link UserDefinedCodeType }
     *     
     */
    public void setUserDefinedCode(UserDefinedCodeType value) {
        this.userDefinedCode = value;
    }

}
