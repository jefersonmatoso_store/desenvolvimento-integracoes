
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Gid stands for global identifier.
 *             Components of a gid include the DomainName, Xid, and XidQualifier.
 *             The DomainName and Xid are combined to form a globally unique identifier.
 *             Examples: Given a domain name of ROSENBLOOMS.COM, and an xid of MYORDER,
 *             the gid would be formed as ROSENBLOOMS.COM.MYORDER.
 *             Given a domain name of PUBLIC and an xid of PORT, the gid would be just PORT.
 *             Thus, when referring to PUBLIC data, the domain name may be omitted.
 *          
 * 
 * <p>Classe Java de GidType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="GidType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DomainName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Xid" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GidType", propOrder = {
    "domainName",
    "xid"
})
public class GidType {

    @XmlElement(name = "DomainName")
    protected String domainName;
    @XmlElement(name = "Xid", required = true)
    protected String xid;

    /**
     * Obtém o valor da propriedade domainName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDomainName() {
        return domainName;
    }

    /**
     * Define o valor da propriedade domainName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDomainName(String value) {
        this.domainName = value;
    }

    /**
     * Obtém o valor da propriedade xid.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXid() {
        return xid;
    }

    /**
     * Define o valor da propriedade xid.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXid(String value) {
        this.xid = value;
    }

}
