
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * Specifies the resource type defined for the location.
 * 
 * <p>Classe Java de LocationResourceTypeType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="LocationResourceTypeType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ResourceTypeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="CalendarGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="AutoScheduleAppt" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ConstraintAppt" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LocationResource" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LocationResourceType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LocationResourceTypeType", propOrder = {
    "resourceTypeGid",
    "calendarGid",
    "autoScheduleAppt",
    "constraintAppt",
    "description",
    "locationResource"
})
public class LocationResourceTypeType {

    @XmlElement(name = "ResourceTypeGid", required = true)
    protected GLogXMLGidType resourceTypeGid;
    @XmlElement(name = "CalendarGid")
    protected GLogXMLGidType calendarGid;
    @XmlElement(name = "AutoScheduleAppt")
    protected String autoScheduleAppt;
    @XmlElement(name = "ConstraintAppt")
    protected String constraintAppt;
    @XmlElement(name = "Description")
    protected String description;
    @XmlElement(name = "LocationResource")
    protected List<LocationResourceType> locationResource;

    /**
     * Obtém o valor da propriedade resourceTypeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getResourceTypeGid() {
        return resourceTypeGid;
    }

    /**
     * Define o valor da propriedade resourceTypeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setResourceTypeGid(GLogXMLGidType value) {
        this.resourceTypeGid = value;
    }

    /**
     * Obtém o valor da propriedade calendarGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getCalendarGid() {
        return calendarGid;
    }

    /**
     * Define o valor da propriedade calendarGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setCalendarGid(GLogXMLGidType value) {
        this.calendarGid = value;
    }

    /**
     * Obtém o valor da propriedade autoScheduleAppt.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAutoScheduleAppt() {
        return autoScheduleAppt;
    }

    /**
     * Define o valor da propriedade autoScheduleAppt.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAutoScheduleAppt(String value) {
        this.autoScheduleAppt = value;
    }

    /**
     * Obtém o valor da propriedade constraintAppt.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConstraintAppt() {
        return constraintAppt;
    }

    /**
     * Define o valor da propriedade constraintAppt.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConstraintAppt(String value) {
        this.constraintAppt = value;
    }

    /**
     * Obtém o valor da propriedade description.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Define o valor da propriedade description.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the locationResource property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the locationResource property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLocationResource().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LocationResourceType }
     * 
     * 
     */
    public List<LocationResourceType> getLocationResource() {
        if (locationResource == null) {
            locationResource = new ArrayList<LocationResourceType>();
        }
        return this.locationResource;
    }

}
