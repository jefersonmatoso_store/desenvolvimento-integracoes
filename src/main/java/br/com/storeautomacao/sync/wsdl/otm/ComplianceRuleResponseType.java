
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * (Outbound) ComplianceRuleResponse is response for the compliance screening triggered via ServiceRequest integration interface.
 *          
 * 
 * <p>Classe Java de ComplianceRuleResponseType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="ComplianceRuleResponseType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RuleControl" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}RuleControlType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ComplianceRuleResponseType", namespace = "http://xmlns.oracle.com/apps/gtm/transmission/v6.4", propOrder = {
    "ruleControl"
})
public class ComplianceRuleResponseType {

    @XmlElement(name = "RuleControl", required = true)
    protected RuleControlType ruleControl;

    /**
     * Obtém o valor da propriedade ruleControl.
     * 
     * @return
     *     possible object is
     *     {@link RuleControlType }
     *     
     */
    public RuleControlType getRuleControl() {
        return ruleControl;
    }

    /**
     * Define o valor da propriedade ruleControl.
     * 
     * @param value
     *     allowed object is
     *     {@link RuleControlType }
     *     
     */
    public void setRuleControl(RuleControlType value) {
        this.ruleControl = value;
    }

}
