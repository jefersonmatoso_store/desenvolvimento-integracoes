
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;


/**
 * 
 *             Contains all header level information e.g. constraints, reference numbers, which apply
 *             to the entire Order Base.
 *          
 * 
 * <p>Classe Java de TransOrderHeaderType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="TransOrderHeaderType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SendReason" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}SendReasonType" minOccurs="0"/>
 *         &lt;element name="IntSavedQuery" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}IntSavedQueryType" minOccurs="0"/>
 *         &lt;element name="TransOrderGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="ExternalSystemId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TransactionCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransactionCodeType"/>
 *         &lt;element name="ReplaceChildren" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ReplaceChildrenType" minOccurs="0"/>
 *         &lt;element name="IsUpdateShipmentOnly" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ProcessingCodeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="ReleaseMethodGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="InvolvedParty" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}InvolvedPartyType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="CommercialTerms" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}CommercialTermsType" minOccurs="0"/>
 *         &lt;element name="PlanningGroupGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="FixedItineraryGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="OrderTypeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="TimeWindowEmphasisGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="IsAppointmentRequired" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsMakeAppointmentBeforePlan" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;choice minOccurs="0">
 *           &lt;element name="RateServiceGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *           &lt;element name="RateServiceProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;/choice>
 *         &lt;choice minOccurs="0">
 *           &lt;element name="ServiceProviderGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *           &lt;element name="ServiceProviderProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;/choice>
 *         &lt;choice minOccurs="0">
 *           &lt;element name="SellServiceProviderGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *           &lt;element name="SellServiceProviderProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;/choice>
 *         &lt;choice minOccurs="0">
 *           &lt;element name="TransportModeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *           &lt;element name="ModeProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;/choice>
 *         &lt;choice minOccurs="0">
 *           &lt;element name="EquipmentGroupGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *           &lt;element name="EquipmentGroupProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;/choice>
 *         &lt;element name="OrderRefnum" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OrderRefnumType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="ShipWithGroup" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Remark" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RemarkType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="OBSpecialService" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}SpecialServiceType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="AccessorialCodeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="IsFullyReleased" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EffectiveDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="ExpirationDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="IsTemplate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RateOfferingGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="RateGeoGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="SellRateOfferingGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="SellRateGeoGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="FixedSellItineraryGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="MustShipDirect" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MustShipThruXDock" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MustShipThruPool" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BundlingType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsShipperKnown" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TotalNetWeightVolume" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}WeightVolumeType" minOccurs="0"/>
 *         &lt;element name="DimRateFactorGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="PickupRoutingSeqGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="DropoffRoutingSeqGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="StuffLocation" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocRefType" minOccurs="0"/>
 *         &lt;element name="DestuffLocation" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocRefType" minOccurs="0"/>
 *         &lt;element name="StowageModeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="UnitizationConditionName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CustomerUnitizationRequest" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsIgnoreLocationCalendar" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ReleaseInstruction" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ReleaseInstructionType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Text" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TextType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="OrderLegConstraint" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OrderLegConstraintType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="OrderPriority" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FlexFieldStrings" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldStringType" minOccurs="0"/>
 *         &lt;element name="FlexFieldNumbers" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldNumberType" minOccurs="0"/>
 *         &lt;element name="FlexFieldDates" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldDateType" minOccurs="0"/>
 *         &lt;element name="FlexFieldCurrencies" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldCurrencyType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TransOrderHeaderType", propOrder = {
    "sendReason",
    "intSavedQuery",
    "transOrderGid",
    "externalSystemId",
    "transactionCode",
    "replaceChildren",
    "isUpdateShipmentOnly",
    "processingCodeGid",
    "releaseMethodGid",
    "involvedParty",
    "commercialTerms",
    "planningGroupGid",
    "fixedItineraryGid",
    "orderTypeGid",
    "timeWindowEmphasisGid",
    "isAppointmentRequired",
    "isMakeAppointmentBeforePlan",
    "rateServiceGid",
    "rateServiceProfileGid",
    "serviceProviderGid",
    "serviceProviderProfileGid",
    "sellServiceProviderGid",
    "sellServiceProviderProfileGid",
    "transportModeGid",
    "modeProfileGid",
    "equipmentGroupGid",
    "equipmentGroupProfileGid",
    "orderRefnum",
    "shipWithGroup",
    "remark",
    "obSpecialService",
    "accessorialCodeGid",
    "isFullyReleased",
    "effectiveDate",
    "expirationDate",
    "isTemplate",
    "rateOfferingGid",
    "rateGeoGid",
    "sellRateOfferingGid",
    "sellRateGeoGid",
    "fixedSellItineraryGid",
    "mustShipDirect",
    "mustShipThruXDock",
    "mustShipThruPool",
    "bundlingType",
    "isShipperKnown",
    "totalNetWeightVolume",
    "dimRateFactorGid",
    "pickupRoutingSeqGid",
    "dropoffRoutingSeqGid",
    "stuffLocation",
    "destuffLocation",
    "stowageModeGid",
    "unitizationConditionName",
    "customerUnitizationRequest",
    "isIgnoreLocationCalendar",
    "releaseInstruction",
    "text",
    "orderLegConstraint",
    "orderPriority",
    "flexFieldStrings",
    "flexFieldNumbers",
    "flexFieldDates",
    "flexFieldCurrencies"
})
public class TransOrderHeaderType {

    @XmlElement(name = "SendReason")
    protected SendReasonType sendReason;
    @XmlElement(name = "IntSavedQuery")
    protected IntSavedQueryType intSavedQuery;
    @XmlElement(name = "TransOrderGid", required = true)
    protected GLogXMLGidType transOrderGid;
    @XmlElement(name = "ExternalSystemId")
    protected String externalSystemId;
    @XmlElement(name = "TransactionCode", required = true)
    @XmlSchemaType(name = "string")
    protected TransactionCodeType transactionCode;
    @XmlElement(name = "ReplaceChildren")
    protected ReplaceChildrenType replaceChildren;
    @XmlElement(name = "IsUpdateShipmentOnly")
    protected String isUpdateShipmentOnly;
    @XmlElement(name = "ProcessingCodeGid")
    protected GLogXMLGidType processingCodeGid;
    @XmlElement(name = "ReleaseMethodGid")
    protected GLogXMLGidType releaseMethodGid;
    @XmlElement(name = "InvolvedParty")
    protected List<InvolvedPartyType> involvedParty;
    @XmlElement(name = "CommercialTerms")
    protected CommercialTermsType commercialTerms;
    @XmlElement(name = "PlanningGroupGid")
    protected GLogXMLGidType planningGroupGid;
    @XmlElement(name = "FixedItineraryGid")
    protected GLogXMLGidType fixedItineraryGid;
    @XmlElement(name = "OrderTypeGid")
    protected GLogXMLGidType orderTypeGid;
    @XmlElement(name = "TimeWindowEmphasisGid")
    protected GLogXMLGidType timeWindowEmphasisGid;
    @XmlElement(name = "IsAppointmentRequired")
    protected String isAppointmentRequired;
    @XmlElement(name = "IsMakeAppointmentBeforePlan")
    protected String isMakeAppointmentBeforePlan;
    @XmlElement(name = "RateServiceGid")
    protected GLogXMLGidType rateServiceGid;
    @XmlElement(name = "RateServiceProfileGid")
    protected GLogXMLGidType rateServiceProfileGid;
    @XmlElement(name = "ServiceProviderGid")
    protected GLogXMLGidType serviceProviderGid;
    @XmlElement(name = "ServiceProviderProfileGid")
    protected GLogXMLGidType serviceProviderProfileGid;
    @XmlElement(name = "SellServiceProviderGid")
    protected GLogXMLGidType sellServiceProviderGid;
    @XmlElement(name = "SellServiceProviderProfileGid")
    protected GLogXMLGidType sellServiceProviderProfileGid;
    @XmlElement(name = "TransportModeGid")
    protected GLogXMLGidType transportModeGid;
    @XmlElement(name = "ModeProfileGid")
    protected GLogXMLGidType modeProfileGid;
    @XmlElement(name = "EquipmentGroupGid")
    protected GLogXMLGidType equipmentGroupGid;
    @XmlElement(name = "EquipmentGroupProfileGid")
    protected GLogXMLGidType equipmentGroupProfileGid;
    @XmlElement(name = "OrderRefnum")
    protected List<OrderRefnumType> orderRefnum;
    @XmlElement(name = "ShipWithGroup")
    protected String shipWithGroup;
    @XmlElement(name = "Remark")
    protected List<RemarkType> remark;
    @XmlElement(name = "OBSpecialService")
    protected List<SpecialServiceType> obSpecialService;
    @XmlElement(name = "AccessorialCodeGid")
    protected List<GLogXMLGidType> accessorialCodeGid;
    @XmlElement(name = "IsFullyReleased")
    protected String isFullyReleased;
    @XmlElement(name = "EffectiveDate")
    protected GLogDateTimeType effectiveDate;
    @XmlElement(name = "ExpirationDate")
    protected GLogDateTimeType expirationDate;
    @XmlElement(name = "IsTemplate")
    protected String isTemplate;
    @XmlElement(name = "RateOfferingGid")
    protected GLogXMLGidType rateOfferingGid;
    @XmlElement(name = "RateGeoGid")
    protected GLogXMLGidType rateGeoGid;
    @XmlElement(name = "SellRateOfferingGid")
    protected GLogXMLGidType sellRateOfferingGid;
    @XmlElement(name = "SellRateGeoGid")
    protected GLogXMLGidType sellRateGeoGid;
    @XmlElement(name = "FixedSellItineraryGid")
    protected GLogXMLGidType fixedSellItineraryGid;
    @XmlElement(name = "MustShipDirect")
    protected String mustShipDirect;
    @XmlElement(name = "MustShipThruXDock")
    protected String mustShipThruXDock;
    @XmlElement(name = "MustShipThruPool")
    protected String mustShipThruPool;
    @XmlElement(name = "BundlingType")
    protected String bundlingType;
    @XmlElement(name = "IsShipperKnown")
    protected String isShipperKnown;
    @XmlElement(name = "TotalNetWeightVolume")
    protected WeightVolumeType totalNetWeightVolume;
    @XmlElement(name = "DimRateFactorGid")
    protected GLogXMLGidType dimRateFactorGid;
    @XmlElement(name = "PickupRoutingSeqGid")
    protected GLogXMLGidType pickupRoutingSeqGid;
    @XmlElement(name = "DropoffRoutingSeqGid")
    protected GLogXMLGidType dropoffRoutingSeqGid;
    @XmlElement(name = "StuffLocation")
    protected GLogXMLLocRefType stuffLocation;
    @XmlElement(name = "DestuffLocation")
    protected GLogXMLLocRefType destuffLocation;
    @XmlElement(name = "StowageModeGid")
    protected GLogXMLGidType stowageModeGid;
    @XmlElement(name = "UnitizationConditionName")
    protected String unitizationConditionName;
    @XmlElement(name = "CustomerUnitizationRequest")
    protected String customerUnitizationRequest;
    @XmlElement(name = "IsIgnoreLocationCalendar")
    protected String isIgnoreLocationCalendar;
    @XmlElement(name = "ReleaseInstruction")
    protected List<ReleaseInstructionType> releaseInstruction;
    @XmlElement(name = "Text")
    protected List<TextType> text;
    @XmlElement(name = "OrderLegConstraint")
    protected List<OrderLegConstraintType> orderLegConstraint;
    @XmlElement(name = "OrderPriority")
    protected String orderPriority;
    @XmlElement(name = "FlexFieldStrings")
    protected FlexFieldStringType flexFieldStrings;
    @XmlElement(name = "FlexFieldNumbers")
    protected FlexFieldNumberType flexFieldNumbers;
    @XmlElement(name = "FlexFieldDates")
    protected FlexFieldDateType flexFieldDates;
    @XmlElement(name = "FlexFieldCurrencies")
    protected FlexFieldCurrencyType flexFieldCurrencies;

    /**
     * Obtém o valor da propriedade sendReason.
     * 
     * @return
     *     possible object is
     *     {@link SendReasonType }
     *     
     */
    public SendReasonType getSendReason() {
        return sendReason;
    }

    /**
     * Define o valor da propriedade sendReason.
     * 
     * @param value
     *     allowed object is
     *     {@link SendReasonType }
     *     
     */
    public void setSendReason(SendReasonType value) {
        this.sendReason = value;
    }

    /**
     * Obtém o valor da propriedade intSavedQuery.
     * 
     * @return
     *     possible object is
     *     {@link IntSavedQueryType }
     *     
     */
    public IntSavedQueryType getIntSavedQuery() {
        return intSavedQuery;
    }

    /**
     * Define o valor da propriedade intSavedQuery.
     * 
     * @param value
     *     allowed object is
     *     {@link IntSavedQueryType }
     *     
     */
    public void setIntSavedQuery(IntSavedQueryType value) {
        this.intSavedQuery = value;
    }

    /**
     * Obtém o valor da propriedade transOrderGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getTransOrderGid() {
        return transOrderGid;
    }

    /**
     * Define o valor da propriedade transOrderGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setTransOrderGid(GLogXMLGidType value) {
        this.transOrderGid = value;
    }

    /**
     * Obtém o valor da propriedade externalSystemId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExternalSystemId() {
        return externalSystemId;
    }

    /**
     * Define o valor da propriedade externalSystemId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExternalSystemId(String value) {
        this.externalSystemId = value;
    }

    /**
     * Obtém o valor da propriedade transactionCode.
     * 
     * @return
     *     possible object is
     *     {@link TransactionCodeType }
     *     
     */
    public TransactionCodeType getTransactionCode() {
        return transactionCode;
    }

    /**
     * Define o valor da propriedade transactionCode.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionCodeType }
     *     
     */
    public void setTransactionCode(TransactionCodeType value) {
        this.transactionCode = value;
    }

    /**
     * Obtém o valor da propriedade replaceChildren.
     * 
     * @return
     *     possible object is
     *     {@link ReplaceChildrenType }
     *     
     */
    public ReplaceChildrenType getReplaceChildren() {
        return replaceChildren;
    }

    /**
     * Define o valor da propriedade replaceChildren.
     * 
     * @param value
     *     allowed object is
     *     {@link ReplaceChildrenType }
     *     
     */
    public void setReplaceChildren(ReplaceChildrenType value) {
        this.replaceChildren = value;
    }

    /**
     * Obtém o valor da propriedade isUpdateShipmentOnly.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsUpdateShipmentOnly() {
        return isUpdateShipmentOnly;
    }

    /**
     * Define o valor da propriedade isUpdateShipmentOnly.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsUpdateShipmentOnly(String value) {
        this.isUpdateShipmentOnly = value;
    }

    /**
     * Obtém o valor da propriedade processingCodeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getProcessingCodeGid() {
        return processingCodeGid;
    }

    /**
     * Define o valor da propriedade processingCodeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setProcessingCodeGid(GLogXMLGidType value) {
        this.processingCodeGid = value;
    }

    /**
     * Obtém o valor da propriedade releaseMethodGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getReleaseMethodGid() {
        return releaseMethodGid;
    }

    /**
     * Define o valor da propriedade releaseMethodGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setReleaseMethodGid(GLogXMLGidType value) {
        this.releaseMethodGid = value;
    }

    /**
     * Gets the value of the involvedParty property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the involvedParty property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInvolvedParty().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link InvolvedPartyType }
     * 
     * 
     */
    public List<InvolvedPartyType> getInvolvedParty() {
        if (involvedParty == null) {
            involvedParty = new ArrayList<InvolvedPartyType>();
        }
        return this.involvedParty;
    }

    /**
     * Obtém o valor da propriedade commercialTerms.
     * 
     * @return
     *     possible object is
     *     {@link CommercialTermsType }
     *     
     */
    public CommercialTermsType getCommercialTerms() {
        return commercialTerms;
    }

    /**
     * Define o valor da propriedade commercialTerms.
     * 
     * @param value
     *     allowed object is
     *     {@link CommercialTermsType }
     *     
     */
    public void setCommercialTerms(CommercialTermsType value) {
        this.commercialTerms = value;
    }

    /**
     * Obtém o valor da propriedade planningGroupGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getPlanningGroupGid() {
        return planningGroupGid;
    }

    /**
     * Define o valor da propriedade planningGroupGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setPlanningGroupGid(GLogXMLGidType value) {
        this.planningGroupGid = value;
    }

    /**
     * Obtém o valor da propriedade fixedItineraryGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getFixedItineraryGid() {
        return fixedItineraryGid;
    }

    /**
     * Define o valor da propriedade fixedItineraryGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setFixedItineraryGid(GLogXMLGidType value) {
        this.fixedItineraryGid = value;
    }

    /**
     * Obtém o valor da propriedade orderTypeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getOrderTypeGid() {
        return orderTypeGid;
    }

    /**
     * Define o valor da propriedade orderTypeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setOrderTypeGid(GLogXMLGidType value) {
        this.orderTypeGid = value;
    }

    /**
     * Obtém o valor da propriedade timeWindowEmphasisGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getTimeWindowEmphasisGid() {
        return timeWindowEmphasisGid;
    }

    /**
     * Define o valor da propriedade timeWindowEmphasisGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setTimeWindowEmphasisGid(GLogXMLGidType value) {
        this.timeWindowEmphasisGid = value;
    }

    /**
     * Obtém o valor da propriedade isAppointmentRequired.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsAppointmentRequired() {
        return isAppointmentRequired;
    }

    /**
     * Define o valor da propriedade isAppointmentRequired.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsAppointmentRequired(String value) {
        this.isAppointmentRequired = value;
    }

    /**
     * Obtém o valor da propriedade isMakeAppointmentBeforePlan.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsMakeAppointmentBeforePlan() {
        return isMakeAppointmentBeforePlan;
    }

    /**
     * Define o valor da propriedade isMakeAppointmentBeforePlan.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsMakeAppointmentBeforePlan(String value) {
        this.isMakeAppointmentBeforePlan = value;
    }

    /**
     * Obtém o valor da propriedade rateServiceGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRateServiceGid() {
        return rateServiceGid;
    }

    /**
     * Define o valor da propriedade rateServiceGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRateServiceGid(GLogXMLGidType value) {
        this.rateServiceGid = value;
    }

    /**
     * Obtém o valor da propriedade rateServiceProfileGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRateServiceProfileGid() {
        return rateServiceProfileGid;
    }

    /**
     * Define o valor da propriedade rateServiceProfileGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRateServiceProfileGid(GLogXMLGidType value) {
        this.rateServiceProfileGid = value;
    }

    /**
     * Obtém o valor da propriedade serviceProviderGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getServiceProviderGid() {
        return serviceProviderGid;
    }

    /**
     * Define o valor da propriedade serviceProviderGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setServiceProviderGid(GLogXMLGidType value) {
        this.serviceProviderGid = value;
    }

    /**
     * Obtém o valor da propriedade serviceProviderProfileGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getServiceProviderProfileGid() {
        return serviceProviderProfileGid;
    }

    /**
     * Define o valor da propriedade serviceProviderProfileGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setServiceProviderProfileGid(GLogXMLGidType value) {
        this.serviceProviderProfileGid = value;
    }

    /**
     * Obtém o valor da propriedade sellServiceProviderGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getSellServiceProviderGid() {
        return sellServiceProviderGid;
    }

    /**
     * Define o valor da propriedade sellServiceProviderGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setSellServiceProviderGid(GLogXMLGidType value) {
        this.sellServiceProviderGid = value;
    }

    /**
     * Obtém o valor da propriedade sellServiceProviderProfileGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getSellServiceProviderProfileGid() {
        return sellServiceProviderProfileGid;
    }

    /**
     * Define o valor da propriedade sellServiceProviderProfileGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setSellServiceProviderProfileGid(GLogXMLGidType value) {
        this.sellServiceProviderProfileGid = value;
    }

    /**
     * Obtém o valor da propriedade transportModeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getTransportModeGid() {
        return transportModeGid;
    }

    /**
     * Define o valor da propriedade transportModeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setTransportModeGid(GLogXMLGidType value) {
        this.transportModeGid = value;
    }

    /**
     * Obtém o valor da propriedade modeProfileGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getModeProfileGid() {
        return modeProfileGid;
    }

    /**
     * Define o valor da propriedade modeProfileGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setModeProfileGid(GLogXMLGidType value) {
        this.modeProfileGid = value;
    }

    /**
     * Obtém o valor da propriedade equipmentGroupGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getEquipmentGroupGid() {
        return equipmentGroupGid;
    }

    /**
     * Define o valor da propriedade equipmentGroupGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setEquipmentGroupGid(GLogXMLGidType value) {
        this.equipmentGroupGid = value;
    }

    /**
     * Obtém o valor da propriedade equipmentGroupProfileGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getEquipmentGroupProfileGid() {
        return equipmentGroupProfileGid;
    }

    /**
     * Define o valor da propriedade equipmentGroupProfileGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setEquipmentGroupProfileGid(GLogXMLGidType value) {
        this.equipmentGroupProfileGid = value;
    }

    /**
     * Gets the value of the orderRefnum property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the orderRefnum property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOrderRefnum().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OrderRefnumType }
     * 
     * 
     */
    public List<OrderRefnumType> getOrderRefnum() {
        if (orderRefnum == null) {
            orderRefnum = new ArrayList<OrderRefnumType>();
        }
        return this.orderRefnum;
    }

    /**
     * Obtém o valor da propriedade shipWithGroup.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipWithGroup() {
        return shipWithGroup;
    }

    /**
     * Define o valor da propriedade shipWithGroup.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipWithGroup(String value) {
        this.shipWithGroup = value;
    }

    /**
     * Gets the value of the remark property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the remark property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRemark().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RemarkType }
     * 
     * 
     */
    public List<RemarkType> getRemark() {
        if (remark == null) {
            remark = new ArrayList<RemarkType>();
        }
        return this.remark;
    }

    /**
     * Gets the value of the obSpecialService property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the obSpecialService property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOBSpecialService().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SpecialServiceType }
     * 
     * 
     */
    public List<SpecialServiceType> getOBSpecialService() {
        if (obSpecialService == null) {
            obSpecialService = new ArrayList<SpecialServiceType>();
        }
        return this.obSpecialService;
    }

    /**
     * Gets the value of the accessorialCodeGid property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the accessorialCodeGid property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAccessorialCodeGid().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GLogXMLGidType }
     * 
     * 
     */
    public List<GLogXMLGidType> getAccessorialCodeGid() {
        if (accessorialCodeGid == null) {
            accessorialCodeGid = new ArrayList<GLogXMLGidType>();
        }
        return this.accessorialCodeGid;
    }

    /**
     * Obtém o valor da propriedade isFullyReleased.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsFullyReleased() {
        return isFullyReleased;
    }

    /**
     * Define o valor da propriedade isFullyReleased.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsFullyReleased(String value) {
        this.isFullyReleased = value;
    }

    /**
     * Obtém o valor da propriedade effectiveDate.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getEffectiveDate() {
        return effectiveDate;
    }

    /**
     * Define o valor da propriedade effectiveDate.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setEffectiveDate(GLogDateTimeType value) {
        this.effectiveDate = value;
    }

    /**
     * Obtém o valor da propriedade expirationDate.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getExpirationDate() {
        return expirationDate;
    }

    /**
     * Define o valor da propriedade expirationDate.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setExpirationDate(GLogDateTimeType value) {
        this.expirationDate = value;
    }

    /**
     * Obtém o valor da propriedade isTemplate.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsTemplate() {
        return isTemplate;
    }

    /**
     * Define o valor da propriedade isTemplate.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsTemplate(String value) {
        this.isTemplate = value;
    }

    /**
     * Obtém o valor da propriedade rateOfferingGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRateOfferingGid() {
        return rateOfferingGid;
    }

    /**
     * Define o valor da propriedade rateOfferingGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRateOfferingGid(GLogXMLGidType value) {
        this.rateOfferingGid = value;
    }

    /**
     * Obtém o valor da propriedade rateGeoGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRateGeoGid() {
        return rateGeoGid;
    }

    /**
     * Define o valor da propriedade rateGeoGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRateGeoGid(GLogXMLGidType value) {
        this.rateGeoGid = value;
    }

    /**
     * Obtém o valor da propriedade sellRateOfferingGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getSellRateOfferingGid() {
        return sellRateOfferingGid;
    }

    /**
     * Define o valor da propriedade sellRateOfferingGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setSellRateOfferingGid(GLogXMLGidType value) {
        this.sellRateOfferingGid = value;
    }

    /**
     * Obtém o valor da propriedade sellRateGeoGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getSellRateGeoGid() {
        return sellRateGeoGid;
    }

    /**
     * Define o valor da propriedade sellRateGeoGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setSellRateGeoGid(GLogXMLGidType value) {
        this.sellRateGeoGid = value;
    }

    /**
     * Obtém o valor da propriedade fixedSellItineraryGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getFixedSellItineraryGid() {
        return fixedSellItineraryGid;
    }

    /**
     * Define o valor da propriedade fixedSellItineraryGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setFixedSellItineraryGid(GLogXMLGidType value) {
        this.fixedSellItineraryGid = value;
    }

    /**
     * Obtém o valor da propriedade mustShipDirect.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMustShipDirect() {
        return mustShipDirect;
    }

    /**
     * Define o valor da propriedade mustShipDirect.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMustShipDirect(String value) {
        this.mustShipDirect = value;
    }

    /**
     * Obtém o valor da propriedade mustShipThruXDock.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMustShipThruXDock() {
        return mustShipThruXDock;
    }

    /**
     * Define o valor da propriedade mustShipThruXDock.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMustShipThruXDock(String value) {
        this.mustShipThruXDock = value;
    }

    /**
     * Obtém o valor da propriedade mustShipThruPool.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMustShipThruPool() {
        return mustShipThruPool;
    }

    /**
     * Define o valor da propriedade mustShipThruPool.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMustShipThruPool(String value) {
        this.mustShipThruPool = value;
    }

    /**
     * Obtém o valor da propriedade bundlingType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBundlingType() {
        return bundlingType;
    }

    /**
     * Define o valor da propriedade bundlingType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBundlingType(String value) {
        this.bundlingType = value;
    }

    /**
     * Obtém o valor da propriedade isShipperKnown.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsShipperKnown() {
        return isShipperKnown;
    }

    /**
     * Define o valor da propriedade isShipperKnown.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsShipperKnown(String value) {
        this.isShipperKnown = value;
    }

    /**
     * Obtém o valor da propriedade totalNetWeightVolume.
     * 
     * @return
     *     possible object is
     *     {@link WeightVolumeType }
     *     
     */
    public WeightVolumeType getTotalNetWeightVolume() {
        return totalNetWeightVolume;
    }

    /**
     * Define o valor da propriedade totalNetWeightVolume.
     * 
     * @param value
     *     allowed object is
     *     {@link WeightVolumeType }
     *     
     */
    public void setTotalNetWeightVolume(WeightVolumeType value) {
        this.totalNetWeightVolume = value;
    }

    /**
     * Obtém o valor da propriedade dimRateFactorGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getDimRateFactorGid() {
        return dimRateFactorGid;
    }

    /**
     * Define o valor da propriedade dimRateFactorGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setDimRateFactorGid(GLogXMLGidType value) {
        this.dimRateFactorGid = value;
    }

    /**
     * Obtém o valor da propriedade pickupRoutingSeqGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getPickupRoutingSeqGid() {
        return pickupRoutingSeqGid;
    }

    /**
     * Define o valor da propriedade pickupRoutingSeqGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setPickupRoutingSeqGid(GLogXMLGidType value) {
        this.pickupRoutingSeqGid = value;
    }

    /**
     * Obtém o valor da propriedade dropoffRoutingSeqGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getDropoffRoutingSeqGid() {
        return dropoffRoutingSeqGid;
    }

    /**
     * Define o valor da propriedade dropoffRoutingSeqGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setDropoffRoutingSeqGid(GLogXMLGidType value) {
        this.dropoffRoutingSeqGid = value;
    }

    /**
     * Obtém o valor da propriedade stuffLocation.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public GLogXMLLocRefType getStuffLocation() {
        return stuffLocation;
    }

    /**
     * Define o valor da propriedade stuffLocation.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public void setStuffLocation(GLogXMLLocRefType value) {
        this.stuffLocation = value;
    }

    /**
     * Obtém o valor da propriedade destuffLocation.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public GLogXMLLocRefType getDestuffLocation() {
        return destuffLocation;
    }

    /**
     * Define o valor da propriedade destuffLocation.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public void setDestuffLocation(GLogXMLLocRefType value) {
        this.destuffLocation = value;
    }

    /**
     * Obtém o valor da propriedade stowageModeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getStowageModeGid() {
        return stowageModeGid;
    }

    /**
     * Define o valor da propriedade stowageModeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setStowageModeGid(GLogXMLGidType value) {
        this.stowageModeGid = value;
    }

    /**
     * Obtém o valor da propriedade unitizationConditionName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUnitizationConditionName() {
        return unitizationConditionName;
    }

    /**
     * Define o valor da propriedade unitizationConditionName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUnitizationConditionName(String value) {
        this.unitizationConditionName = value;
    }

    /**
     * Obtém o valor da propriedade customerUnitizationRequest.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomerUnitizationRequest() {
        return customerUnitizationRequest;
    }

    /**
     * Define o valor da propriedade customerUnitizationRequest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomerUnitizationRequest(String value) {
        this.customerUnitizationRequest = value;
    }

    /**
     * Obtém o valor da propriedade isIgnoreLocationCalendar.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsIgnoreLocationCalendar() {
        return isIgnoreLocationCalendar;
    }

    /**
     * Define o valor da propriedade isIgnoreLocationCalendar.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsIgnoreLocationCalendar(String value) {
        this.isIgnoreLocationCalendar = value;
    }

    /**
     * Gets the value of the releaseInstruction property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the releaseInstruction property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReleaseInstruction().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ReleaseInstructionType }
     * 
     * 
     */
    public List<ReleaseInstructionType> getReleaseInstruction() {
        if (releaseInstruction == null) {
            releaseInstruction = new ArrayList<ReleaseInstructionType>();
        }
        return this.releaseInstruction;
    }

    /**
     * Gets the value of the text property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the text property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getText().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TextType }
     * 
     * 
     */
    public List<TextType> getText() {
        if (text == null) {
            text = new ArrayList<TextType>();
        }
        return this.text;
    }

    /**
     * Gets the value of the orderLegConstraint property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the orderLegConstraint property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOrderLegConstraint().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OrderLegConstraintType }
     * 
     * 
     */
    public List<OrderLegConstraintType> getOrderLegConstraint() {
        if (orderLegConstraint == null) {
            orderLegConstraint = new ArrayList<OrderLegConstraintType>();
        }
        return this.orderLegConstraint;
    }

    /**
     * Obtém o valor da propriedade orderPriority.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrderPriority() {
        return orderPriority;
    }

    /**
     * Define o valor da propriedade orderPriority.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrderPriority(String value) {
        this.orderPriority = value;
    }

    /**
     * Obtém o valor da propriedade flexFieldStrings.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldStringType }
     *     
     */
    public FlexFieldStringType getFlexFieldStrings() {
        return flexFieldStrings;
    }

    /**
     * Define o valor da propriedade flexFieldStrings.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldStringType }
     *     
     */
    public void setFlexFieldStrings(FlexFieldStringType value) {
        this.flexFieldStrings = value;
    }

    /**
     * Obtém o valor da propriedade flexFieldNumbers.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldNumberType }
     *     
     */
    public FlexFieldNumberType getFlexFieldNumbers() {
        return flexFieldNumbers;
    }

    /**
     * Define o valor da propriedade flexFieldNumbers.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldNumberType }
     *     
     */
    public void setFlexFieldNumbers(FlexFieldNumberType value) {
        this.flexFieldNumbers = value;
    }

    /**
     * Obtém o valor da propriedade flexFieldDates.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldDateType }
     *     
     */
    public FlexFieldDateType getFlexFieldDates() {
        return flexFieldDates;
    }

    /**
     * Define o valor da propriedade flexFieldDates.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldDateType }
     *     
     */
    public void setFlexFieldDates(FlexFieldDateType value) {
        this.flexFieldDates = value;
    }

    /**
     * Obtém o valor da propriedade flexFieldCurrencies.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldCurrencyType }
     *     
     */
    public FlexFieldCurrencyType getFlexFieldCurrencies() {
        return flexFieldCurrencies;
    }

    /**
     * Define o valor da propriedade flexFieldCurrencies.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldCurrencyType }
     *     
     */
    public void setFlexFieldCurrencies(FlexFieldCurrencyType value) {
        this.flexFieldCurrencies = value;
    }

}
