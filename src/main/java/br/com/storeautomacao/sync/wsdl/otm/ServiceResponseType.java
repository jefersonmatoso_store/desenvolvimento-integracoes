
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * 
 *             Request/Reply style interface reply to the ServiceRequest.
 *          
 * 
 * <p>Classe Java de ServiceResponseType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="ServiceResponseType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OTMTransactionOut">
 *       &lt;sequence>
 *         &lt;element name="ServiceRequest" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}ServiceRequestType" minOccurs="0"/>
 *         &lt;choice>
 *           &lt;element name="RestrictedPartyResponse" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}RestrictedPartyResponseType" maxOccurs="unbounded" minOccurs="0"/>
 *           &lt;element name="SanctionedTerritoryResponse" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}SanctionedTerritoryResponseType" maxOccurs="unbounded" minOccurs="0"/>
 *           &lt;element name="ClassificationResponse" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}ClassificationResponseType" maxOccurs="unbounded" minOccurs="0"/>
 *           &lt;element name="ComplianceRuleResponse" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}ComplianceRuleResponseType" maxOccurs="unbounded" minOccurs="0"/>
 *           &lt;element name="LicenseDeterminationResponse" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}LicenseDeterminationResponseType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;/choice>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceResponseType", namespace = "http://xmlns.oracle.com/apps/gtm/transmission/v6.4", propOrder = {
    "serviceRequest",
    "restrictedPartyResponse",
    "sanctionedTerritoryResponse",
    "classificationResponse",
    "complianceRuleResponse",
    "licenseDeterminationResponse"
})
public class ServiceResponseType
    extends OTMTransactionOut
{

    @XmlElement(name = "ServiceRequest")
    protected ServiceRequestType serviceRequest;
    @XmlElement(name = "RestrictedPartyResponse")
    protected List<RestrictedPartyResponseType> restrictedPartyResponse;
    @XmlElement(name = "SanctionedTerritoryResponse")
    protected List<SanctionedTerritoryResponseType> sanctionedTerritoryResponse;
    @XmlElement(name = "ClassificationResponse")
    protected List<ClassificationResponseType> classificationResponse;
    @XmlElement(name = "ComplianceRuleResponse")
    protected List<ComplianceRuleResponseType> complianceRuleResponse;
    @XmlElement(name = "LicenseDeterminationResponse")
    protected List<LicenseDeterminationResponseType> licenseDeterminationResponse;

    /**
     * Obtém o valor da propriedade serviceRequest.
     * 
     * @return
     *     possible object is
     *     {@link ServiceRequestType }
     *     
     */
    public ServiceRequestType getServiceRequest() {
        return serviceRequest;
    }

    /**
     * Define o valor da propriedade serviceRequest.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceRequestType }
     *     
     */
    public void setServiceRequest(ServiceRequestType value) {
        this.serviceRequest = value;
    }

    /**
     * Gets the value of the restrictedPartyResponse property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the restrictedPartyResponse property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRestrictedPartyResponse().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RestrictedPartyResponseType }
     * 
     * 
     */
    public List<RestrictedPartyResponseType> getRestrictedPartyResponse() {
        if (restrictedPartyResponse == null) {
            restrictedPartyResponse = new ArrayList<RestrictedPartyResponseType>();
        }
        return this.restrictedPartyResponse;
    }

    /**
     * Gets the value of the sanctionedTerritoryResponse property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the sanctionedTerritoryResponse property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSanctionedTerritoryResponse().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SanctionedTerritoryResponseType }
     * 
     * 
     */
    public List<SanctionedTerritoryResponseType> getSanctionedTerritoryResponse() {
        if (sanctionedTerritoryResponse == null) {
            sanctionedTerritoryResponse = new ArrayList<SanctionedTerritoryResponseType>();
        }
        return this.sanctionedTerritoryResponse;
    }

    /**
     * Gets the value of the classificationResponse property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the classificationResponse property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getClassificationResponse().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ClassificationResponseType }
     * 
     * 
     */
    public List<ClassificationResponseType> getClassificationResponse() {
        if (classificationResponse == null) {
            classificationResponse = new ArrayList<ClassificationResponseType>();
        }
        return this.classificationResponse;
    }

    /**
     * Gets the value of the complianceRuleResponse property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the complianceRuleResponse property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getComplianceRuleResponse().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ComplianceRuleResponseType }
     * 
     * 
     */
    public List<ComplianceRuleResponseType> getComplianceRuleResponse() {
        if (complianceRuleResponse == null) {
            complianceRuleResponse = new ArrayList<ComplianceRuleResponseType>();
        }
        return this.complianceRuleResponse;
    }

    /**
     * Gets the value of the licenseDeterminationResponse property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the licenseDeterminationResponse property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLicenseDeterminationResponse().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LicenseDeterminationResponseType }
     * 
     * 
     */
    public List<LicenseDeterminationResponseType> getLicenseDeterminationResponse() {
        if (licenseDeterminationResponse == null) {
            licenseDeterminationResponse = new ArrayList<LicenseDeterminationResponseType>();
        }
        return this.licenseDeterminationResponse;
    }

}
