
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * (Both) Date on the business transaction.
 *          
 * 
 * <p>Classe Java de TransDateType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="TransDateType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DateQualifierGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="TransactionDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TransDateType", namespace = "http://xmlns.oracle.com/apps/gtm/transmission/v6.4", propOrder = {
    "dateQualifierGid",
    "transactionDate"
})
public class TransDateType {

    @XmlElement(name = "DateQualifierGid", required = true)
    protected GLogXMLGidType dateQualifierGid;
    @XmlElement(name = "TransactionDate", required = true)
    protected GLogDateTimeType transactionDate;

    /**
     * Obtém o valor da propriedade dateQualifierGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getDateQualifierGid() {
        return dateQualifierGid;
    }

    /**
     * Define o valor da propriedade dateQualifierGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setDateQualifierGid(GLogXMLGidType value) {
        this.dateQualifierGid = value;
    }

    /**
     * Obtém o valor da propriedade transactionDate.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getTransactionDate() {
        return transactionDate;
    }

    /**
     * Define o valor da propriedade transactionDate.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setTransactionDate(GLogDateTimeType value) {
        this.transactionDate = value;
    }

}
