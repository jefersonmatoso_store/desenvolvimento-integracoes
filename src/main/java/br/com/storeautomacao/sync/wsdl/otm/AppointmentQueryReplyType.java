
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * Specifies the reply for an appointment query.
 * 
 * <p>Classe Java de AppointmentQueryReplyType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="AppointmentQueryReplyType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AppointmentQuery" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}AppointmentQueryType"/>
 *         &lt;element name="AppointmentInfo" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}AppointmentInfoType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="AppointmentMissing" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}AppointmentMissingType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AppointmentQueryReplyType", propOrder = {
    "appointmentQuery",
    "appointmentInfo",
    "appointmentMissing"
})
public class AppointmentQueryReplyType {

    @XmlElement(name = "AppointmentQuery", required = true)
    protected AppointmentQueryType appointmentQuery;
    @XmlElement(name = "AppointmentInfo")
    protected List<AppointmentInfoType> appointmentInfo;
    @XmlElement(name = "AppointmentMissing")
    protected List<AppointmentMissingType> appointmentMissing;

    /**
     * Obtém o valor da propriedade appointmentQuery.
     * 
     * @return
     *     possible object is
     *     {@link AppointmentQueryType }
     *     
     */
    public AppointmentQueryType getAppointmentQuery() {
        return appointmentQuery;
    }

    /**
     * Define o valor da propriedade appointmentQuery.
     * 
     * @param value
     *     allowed object is
     *     {@link AppointmentQueryType }
     *     
     */
    public void setAppointmentQuery(AppointmentQueryType value) {
        this.appointmentQuery = value;
    }

    /**
     * Gets the value of the appointmentInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the appointmentInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAppointmentInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AppointmentInfoType }
     * 
     * 
     */
    public List<AppointmentInfoType> getAppointmentInfo() {
        if (appointmentInfo == null) {
            appointmentInfo = new ArrayList<AppointmentInfoType>();
        }
        return this.appointmentInfo;
    }

    /**
     * Gets the value of the appointmentMissing property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the appointmentMissing property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAppointmentMissing().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AppointmentMissingType }
     * 
     * 
     */
    public List<AppointmentMissingType> getAppointmentMissing() {
        if (appointmentMissing == null) {
            appointmentMissing = new ArrayList<AppointmentMissingType>();
        }
        return this.appointmentMissing;
    }

}
