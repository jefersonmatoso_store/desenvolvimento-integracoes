
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de QuoteLocationType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="QuoteLocationType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element name="LocationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;sequence minOccurs="0">
 *           &lt;element name="Type" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *           &lt;element name="QuoteLocInfo" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}QuoteLocInfoType"/>
 *           &lt;element name="ViaLocation" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocGidType" minOccurs="0"/>
 *         &lt;/sequence>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QuoteLocationType", propOrder = {
    "locationGid",
    "type",
    "quoteLocInfo",
    "viaLocation"
})
public class QuoteLocationType {

    @XmlElement(name = "LocationGid")
    protected GLogXMLGidType locationGid;
    @XmlElement(name = "Type")
    protected String type;
    @XmlElement(name = "QuoteLocInfo")
    protected QuoteLocInfoType quoteLocInfo;
    @XmlElement(name = "ViaLocation")
    protected GLogXMLLocGidType viaLocation;

    /**
     * Obtém o valor da propriedade locationGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getLocationGid() {
        return locationGid;
    }

    /**
     * Define o valor da propriedade locationGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setLocationGid(GLogXMLGidType value) {
        this.locationGid = value;
    }

    /**
     * Obtém o valor da propriedade type.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getType() {
        return type;
    }

    /**
     * Define o valor da propriedade type.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setType(String value) {
        this.type = value;
    }

    /**
     * Obtém o valor da propriedade quoteLocInfo.
     * 
     * @return
     *     possible object is
     *     {@link QuoteLocInfoType }
     *     
     */
    public QuoteLocInfoType getQuoteLocInfo() {
        return quoteLocInfo;
    }

    /**
     * Define o valor da propriedade quoteLocInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link QuoteLocInfoType }
     *     
     */
    public void setQuoteLocInfo(QuoteLocInfoType value) {
        this.quoteLocInfo = value;
    }

    /**
     * Obtém o valor da propriedade viaLocation.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocGidType }
     *     
     */
    public GLogXMLLocGidType getViaLocation() {
        return viaLocation;
    }

    /**
     * Define o valor da propriedade viaLocation.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocGidType }
     *     
     */
    public void setViaLocation(GLogXMLLocGidType value) {
        this.viaLocation = value;
    }

}
