
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * 
 *             TransmissionReport consists of a TransmissionNo followed by 0 or more IntegrationLogMessage elements,
 *             followed by a TransmissionSummary element.
 *          
 * 
 * <p>Classe Java de TransmissionReportType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="TransmissionReportType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransmissionDocumentType">
 *       &lt;sequence>
 *         &lt;element name="UserName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Password" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}PasswordType" minOccurs="0"/>
 *         &lt;element name="SendReason" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}SendReasonType" minOccurs="0"/>
 *         &lt;element name="TransmissionNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SenderTransmissionNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ReportStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Refnum" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RefnumType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="IntegrationLogMessage" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}IntegrationLogMessageType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="TransmissionSummary" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransmissionSummaryType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TransmissionReportType", propOrder = {
    "userName",
    "password",
    "sendReason",
    "transmissionNo",
    "senderTransmissionNo",
    "reportStatus",
    "refnum",
    "integrationLogMessage",
    "transmissionSummary"
})
public class TransmissionReportType
    extends TransmissionDocumentType
{

    @XmlElement(name = "UserName")
    protected String userName;
    @XmlElement(name = "Password")
    protected PasswordType password;
    @XmlElement(name = "SendReason")
    protected SendReasonType sendReason;
    @XmlElement(name = "TransmissionNo", required = true)
    protected String transmissionNo;
    @XmlElement(name = "SenderTransmissionNo")
    protected String senderTransmissionNo;
    @XmlElement(name = "ReportStatus")
    protected String reportStatus;
    @XmlElement(name = "Refnum")
    protected List<RefnumType> refnum;
    @XmlElement(name = "IntegrationLogMessage")
    protected List<IntegrationLogMessageType> integrationLogMessage;
    @XmlElement(name = "TransmissionSummary")
    protected TransmissionSummaryType transmissionSummary;

    /**
     * Obtém o valor da propriedade userName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserName() {
        return userName;
    }

    /**
     * Define o valor da propriedade userName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserName(String value) {
        this.userName = value;
    }

    /**
     * Obtém o valor da propriedade password.
     * 
     * @return
     *     possible object is
     *     {@link PasswordType }
     *     
     */
    public PasswordType getPassword() {
        return password;
    }

    /**
     * Define o valor da propriedade password.
     * 
     * @param value
     *     allowed object is
     *     {@link PasswordType }
     *     
     */
    public void setPassword(PasswordType value) {
        this.password = value;
    }

    /**
     * Obtém o valor da propriedade sendReason.
     * 
     * @return
     *     possible object is
     *     {@link SendReasonType }
     *     
     */
    public SendReasonType getSendReason() {
        return sendReason;
    }

    /**
     * Define o valor da propriedade sendReason.
     * 
     * @param value
     *     allowed object is
     *     {@link SendReasonType }
     *     
     */
    public void setSendReason(SendReasonType value) {
        this.sendReason = value;
    }

    /**
     * Obtém o valor da propriedade transmissionNo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransmissionNo() {
        return transmissionNo;
    }

    /**
     * Define o valor da propriedade transmissionNo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransmissionNo(String value) {
        this.transmissionNo = value;
    }

    /**
     * Obtém o valor da propriedade senderTransmissionNo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSenderTransmissionNo() {
        return senderTransmissionNo;
    }

    /**
     * Define o valor da propriedade senderTransmissionNo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSenderTransmissionNo(String value) {
        this.senderTransmissionNo = value;
    }

    /**
     * Obtém o valor da propriedade reportStatus.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReportStatus() {
        return reportStatus;
    }

    /**
     * Define o valor da propriedade reportStatus.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReportStatus(String value) {
        this.reportStatus = value;
    }

    /**
     * Gets the value of the refnum property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the refnum property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRefnum().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RefnumType }
     * 
     * 
     */
    public List<RefnumType> getRefnum() {
        if (refnum == null) {
            refnum = new ArrayList<RefnumType>();
        }
        return this.refnum;
    }

    /**
     * Gets the value of the integrationLogMessage property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the integrationLogMessage property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getIntegrationLogMessage().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link IntegrationLogMessageType }
     * 
     * 
     */
    public List<IntegrationLogMessageType> getIntegrationLogMessage() {
        if (integrationLogMessage == null) {
            integrationLogMessage = new ArrayList<IntegrationLogMessageType>();
        }
        return this.integrationLogMessage;
    }

    /**
     * Obtém o valor da propriedade transmissionSummary.
     * 
     * @return
     *     possible object is
     *     {@link TransmissionSummaryType }
     *     
     */
    public TransmissionSummaryType getTransmissionSummary() {
        return transmissionSummary;
    }

    /**
     * Define o valor da propriedade transmissionSummary.
     * 
     * @param value
     *     allowed object is
     *     {@link TransmissionSummaryType }
     *     
     */
    public void setTransmissionSummary(TransmissionSummaryType value) {
        this.transmissionSummary = value;
    }

}
