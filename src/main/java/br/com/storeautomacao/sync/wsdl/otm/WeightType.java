
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Weight is a group of fields for specifying weight.
 *             It includes sub-elements for weight value and unit of measure.
 *          
 * 
 * <p>Classe Java de WeightType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="WeightType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="WeightValue" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="WeightUOMGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WeightType", propOrder = {
    "weightValue",
    "weightUOMGid"
})
public class WeightType {

    @XmlElement(name = "WeightValue", required = true)
    protected String weightValue;
    @XmlElement(name = "WeightUOMGid", required = true)
    protected GLogXMLGidType weightUOMGid;

    /**
     * Obtém o valor da propriedade weightValue.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWeightValue() {
        return weightValue;
    }

    /**
     * Define o valor da propriedade weightValue.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWeightValue(String value) {
        this.weightValue = value;
    }

    /**
     * Obtém o valor da propriedade weightUOMGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getWeightUOMGid() {
        return weightUOMGid;
    }

    /**
     * Define o valor da propriedade weightUOMGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setWeightUOMGid(GLogXMLGidType value) {
        this.weightUOMGid = value;
    }

}
