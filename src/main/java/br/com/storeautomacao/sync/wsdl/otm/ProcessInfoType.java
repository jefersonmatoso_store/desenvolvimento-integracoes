
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 *             ProcessInfo specifies information pertaining to the processing of GLogXMLElements.
 *             Refer to the comments for the individual elements for more information.
 *          
 * 
 * <p>Classe Java de ProcessInfoType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="ProcessInfoType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ProcessGroup" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ProcessSequence" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProcessInfoType", propOrder = {
    "processGroup",
    "processSequence"
})
public class ProcessInfoType {

    @XmlElement(name = "ProcessGroup")
    protected String processGroup;
    @XmlElement(name = "ProcessSequence")
    protected String processSequence;

    /**
     * Obtém o valor da propriedade processGroup.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProcessGroup() {
        return processGroup;
    }

    /**
     * Define o valor da propriedade processGroup.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProcessGroup(String value) {
        this.processGroup = value;
    }

    /**
     * Obtém o valor da propriedade processSequence.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProcessSequence() {
        return processSequence;
    }

    /**
     * Define o valor da propriedade processSequence.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProcessSequence(String value) {
        this.processSequence = value;
    }

}
