
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Common type for tables which contain Flex Field columns for VARCHAR2 data type
 * 
 * <p>Classe Java de FlexFieldStringType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="FlexFieldStringType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Attribute1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Attribute2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Attribute3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Attribute4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Attribute5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Attribute6" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Attribute7" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Attribute8" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Attribute9" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Attribute10" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Attribute11" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Attribute12" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Attribute13" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Attribute14" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Attribute15" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Attribute16" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Attribute17" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Attribute18" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Attribute19" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Attribute20" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FlexFieldStringType", propOrder = {
    "attribute1",
    "attribute2",
    "attribute3",
    "attribute4",
    "attribute5",
    "attribute6",
    "attribute7",
    "attribute8",
    "attribute9",
    "attribute10",
    "attribute11",
    "attribute12",
    "attribute13",
    "attribute14",
    "attribute15",
    "attribute16",
    "attribute17",
    "attribute18",
    "attribute19",
    "attribute20"
})
public class FlexFieldStringType {

    @XmlElement(name = "Attribute1")
    protected String attribute1;
    @XmlElement(name = "Attribute2")
    protected String attribute2;
    @XmlElement(name = "Attribute3")
    protected String attribute3;
    @XmlElement(name = "Attribute4")
    protected String attribute4;
    @XmlElement(name = "Attribute5")
    protected String attribute5;
    @XmlElement(name = "Attribute6")
    protected String attribute6;
    @XmlElement(name = "Attribute7")
    protected String attribute7;
    @XmlElement(name = "Attribute8")
    protected String attribute8;
    @XmlElement(name = "Attribute9")
    protected String attribute9;
    @XmlElement(name = "Attribute10")
    protected String attribute10;
    @XmlElement(name = "Attribute11")
    protected String attribute11;
    @XmlElement(name = "Attribute12")
    protected String attribute12;
    @XmlElement(name = "Attribute13")
    protected String attribute13;
    @XmlElement(name = "Attribute14")
    protected String attribute14;
    @XmlElement(name = "Attribute15")
    protected String attribute15;
    @XmlElement(name = "Attribute16")
    protected String attribute16;
    @XmlElement(name = "Attribute17")
    protected String attribute17;
    @XmlElement(name = "Attribute18")
    protected String attribute18;
    @XmlElement(name = "Attribute19")
    protected String attribute19;
    @XmlElement(name = "Attribute20")
    protected String attribute20;

    /**
     * Obtém o valor da propriedade attribute1.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAttribute1() {
        return attribute1;
    }

    /**
     * Define o valor da propriedade attribute1.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAttribute1(String value) {
        this.attribute1 = value;
    }

    /**
     * Obtém o valor da propriedade attribute2.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAttribute2() {
        return attribute2;
    }

    /**
     * Define o valor da propriedade attribute2.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAttribute2(String value) {
        this.attribute2 = value;
    }

    /**
     * Obtém o valor da propriedade attribute3.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAttribute3() {
        return attribute3;
    }

    /**
     * Define o valor da propriedade attribute3.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAttribute3(String value) {
        this.attribute3 = value;
    }

    /**
     * Obtém o valor da propriedade attribute4.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAttribute4() {
        return attribute4;
    }

    /**
     * Define o valor da propriedade attribute4.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAttribute4(String value) {
        this.attribute4 = value;
    }

    /**
     * Obtém o valor da propriedade attribute5.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAttribute5() {
        return attribute5;
    }

    /**
     * Define o valor da propriedade attribute5.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAttribute5(String value) {
        this.attribute5 = value;
    }

    /**
     * Obtém o valor da propriedade attribute6.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAttribute6() {
        return attribute6;
    }

    /**
     * Define o valor da propriedade attribute6.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAttribute6(String value) {
        this.attribute6 = value;
    }

    /**
     * Obtém o valor da propriedade attribute7.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAttribute7() {
        return attribute7;
    }

    /**
     * Define o valor da propriedade attribute7.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAttribute7(String value) {
        this.attribute7 = value;
    }

    /**
     * Obtém o valor da propriedade attribute8.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAttribute8() {
        return attribute8;
    }

    /**
     * Define o valor da propriedade attribute8.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAttribute8(String value) {
        this.attribute8 = value;
    }

    /**
     * Obtém o valor da propriedade attribute9.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAttribute9() {
        return attribute9;
    }

    /**
     * Define o valor da propriedade attribute9.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAttribute9(String value) {
        this.attribute9 = value;
    }

    /**
     * Obtém o valor da propriedade attribute10.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAttribute10() {
        return attribute10;
    }

    /**
     * Define o valor da propriedade attribute10.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAttribute10(String value) {
        this.attribute10 = value;
    }

    /**
     * Obtém o valor da propriedade attribute11.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAttribute11() {
        return attribute11;
    }

    /**
     * Define o valor da propriedade attribute11.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAttribute11(String value) {
        this.attribute11 = value;
    }

    /**
     * Obtém o valor da propriedade attribute12.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAttribute12() {
        return attribute12;
    }

    /**
     * Define o valor da propriedade attribute12.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAttribute12(String value) {
        this.attribute12 = value;
    }

    /**
     * Obtém o valor da propriedade attribute13.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAttribute13() {
        return attribute13;
    }

    /**
     * Define o valor da propriedade attribute13.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAttribute13(String value) {
        this.attribute13 = value;
    }

    /**
     * Obtém o valor da propriedade attribute14.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAttribute14() {
        return attribute14;
    }

    /**
     * Define o valor da propriedade attribute14.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAttribute14(String value) {
        this.attribute14 = value;
    }

    /**
     * Obtém o valor da propriedade attribute15.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAttribute15() {
        return attribute15;
    }

    /**
     * Define o valor da propriedade attribute15.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAttribute15(String value) {
        this.attribute15 = value;
    }

    /**
     * Obtém o valor da propriedade attribute16.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAttribute16() {
        return attribute16;
    }

    /**
     * Define o valor da propriedade attribute16.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAttribute16(String value) {
        this.attribute16 = value;
    }

    /**
     * Obtém o valor da propriedade attribute17.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAttribute17() {
        return attribute17;
    }

    /**
     * Define o valor da propriedade attribute17.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAttribute17(String value) {
        this.attribute17 = value;
    }

    /**
     * Obtém o valor da propriedade attribute18.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAttribute18() {
        return attribute18;
    }

    /**
     * Define o valor da propriedade attribute18.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAttribute18(String value) {
        this.attribute18 = value;
    }

    /**
     * Obtém o valor da propriedade attribute19.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAttribute19() {
        return attribute19;
    }

    /**
     * Define o valor da propriedade attribute19.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAttribute19(String value) {
        this.attribute19 = value;
    }

    /**
     * Obtém o valor da propriedade attribute20.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAttribute20() {
        return attribute20;
    }

    /**
     * Define o valor da propriedade attribute20.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAttribute20(String value) {
        this.attribute20 = value;
    }

}
