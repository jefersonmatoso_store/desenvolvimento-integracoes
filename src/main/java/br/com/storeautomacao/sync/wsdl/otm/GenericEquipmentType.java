
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * 
 *             GenericEquipment is used to convey equipment information pertaining to motor, ocean or rail carriers.
 *          
 * 
 * <p>Classe Java de GenericEquipmentType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="GenericEquipmentType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SequenceNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EquipmentPrefix" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EquipmentNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EquipmentInitialNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SEquipmentGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="EquipmentSeal" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}EquipmentSealType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="EquipmentDescriptionCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ISOEquipmentTypeCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EquipmentOwner" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MinTemperature" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLTemperatureType" minOccurs="0"/>
 *         &lt;element name="MaxTemperature" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLTemperatureType" minOccurs="0"/>
 *         &lt;element name="PercentHumidity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="VentSetting" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="WeightVolume" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}WeightVolumeType" minOccurs="0"/>
 *         &lt;element name="LengthWidthHeight" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LengthWidthHeightType" minOccurs="0"/>
 *         &lt;element name="TareWeight" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLWeightType" minOccurs="0"/>
 *         &lt;element name="Dunnage" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLWeightType" minOccurs="0"/>
 *         &lt;element name="UnitCount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OwnershipCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GenericEquipmentType", propOrder = {
    "sequenceNumber",
    "equipmentPrefix",
    "equipmentNumber",
    "equipmentInitialNumber",
    "sEquipmentGid",
    "equipmentSeal",
    "equipmentDescriptionCode",
    "isoEquipmentTypeCode",
    "equipmentOwner",
    "minTemperature",
    "maxTemperature",
    "percentHumidity",
    "ventSetting",
    "weightVolume",
    "lengthWidthHeight",
    "tareWeight",
    "dunnage",
    "unitCount",
    "ownershipCode"
})
public class GenericEquipmentType {

    @XmlElement(name = "SequenceNumber")
    protected String sequenceNumber;
    @XmlElement(name = "EquipmentPrefix")
    protected String equipmentPrefix;
    @XmlElement(name = "EquipmentNumber")
    protected String equipmentNumber;
    @XmlElement(name = "EquipmentInitialNumber")
    protected String equipmentInitialNumber;
    @XmlElement(name = "SEquipmentGid")
    protected GLogXMLGidType sEquipmentGid;
    @XmlElement(name = "EquipmentSeal")
    protected List<EquipmentSealType> equipmentSeal;
    @XmlElement(name = "EquipmentDescriptionCode")
    protected String equipmentDescriptionCode;
    @XmlElement(name = "ISOEquipmentTypeCode")
    protected String isoEquipmentTypeCode;
    @XmlElement(name = "EquipmentOwner")
    protected String equipmentOwner;
    @XmlElement(name = "MinTemperature")
    protected GLogXMLTemperatureType minTemperature;
    @XmlElement(name = "MaxTemperature")
    protected GLogXMLTemperatureType maxTemperature;
    @XmlElement(name = "PercentHumidity")
    protected String percentHumidity;
    @XmlElement(name = "VentSetting")
    protected String ventSetting;
    @XmlElement(name = "WeightVolume")
    protected WeightVolumeType weightVolume;
    @XmlElement(name = "LengthWidthHeight")
    protected LengthWidthHeightType lengthWidthHeight;
    @XmlElement(name = "TareWeight")
    protected GLogXMLWeightType tareWeight;
    @XmlElement(name = "Dunnage")
    protected GLogXMLWeightType dunnage;
    @XmlElement(name = "UnitCount")
    protected String unitCount;
    @XmlElement(name = "OwnershipCode")
    protected String ownershipCode;

    /**
     * Obtém o valor da propriedade sequenceNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSequenceNumber() {
        return sequenceNumber;
    }

    /**
     * Define o valor da propriedade sequenceNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSequenceNumber(String value) {
        this.sequenceNumber = value;
    }

    /**
     * Obtém o valor da propriedade equipmentPrefix.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEquipmentPrefix() {
        return equipmentPrefix;
    }

    /**
     * Define o valor da propriedade equipmentPrefix.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEquipmentPrefix(String value) {
        this.equipmentPrefix = value;
    }

    /**
     * Obtém o valor da propriedade equipmentNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEquipmentNumber() {
        return equipmentNumber;
    }

    /**
     * Define o valor da propriedade equipmentNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEquipmentNumber(String value) {
        this.equipmentNumber = value;
    }

    /**
     * Obtém o valor da propriedade equipmentInitialNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEquipmentInitialNumber() {
        return equipmentInitialNumber;
    }

    /**
     * Define o valor da propriedade equipmentInitialNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEquipmentInitialNumber(String value) {
        this.equipmentInitialNumber = value;
    }

    /**
     * Obtém o valor da propriedade sEquipmentGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getSEquipmentGid() {
        return sEquipmentGid;
    }

    /**
     * Define o valor da propriedade sEquipmentGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setSEquipmentGid(GLogXMLGidType value) {
        this.sEquipmentGid = value;
    }

    /**
     * Gets the value of the equipmentSeal property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the equipmentSeal property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEquipmentSeal().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EquipmentSealType }
     * 
     * 
     */
    public List<EquipmentSealType> getEquipmentSeal() {
        if (equipmentSeal == null) {
            equipmentSeal = new ArrayList<EquipmentSealType>();
        }
        return this.equipmentSeal;
    }

    /**
     * Obtém o valor da propriedade equipmentDescriptionCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEquipmentDescriptionCode() {
        return equipmentDescriptionCode;
    }

    /**
     * Define o valor da propriedade equipmentDescriptionCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEquipmentDescriptionCode(String value) {
        this.equipmentDescriptionCode = value;
    }

    /**
     * Obtém o valor da propriedade isoEquipmentTypeCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getISOEquipmentTypeCode() {
        return isoEquipmentTypeCode;
    }

    /**
     * Define o valor da propriedade isoEquipmentTypeCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setISOEquipmentTypeCode(String value) {
        this.isoEquipmentTypeCode = value;
    }

    /**
     * Obtém o valor da propriedade equipmentOwner.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEquipmentOwner() {
        return equipmentOwner;
    }

    /**
     * Define o valor da propriedade equipmentOwner.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEquipmentOwner(String value) {
        this.equipmentOwner = value;
    }

    /**
     * Obtém o valor da propriedade minTemperature.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLTemperatureType }
     *     
     */
    public GLogXMLTemperatureType getMinTemperature() {
        return minTemperature;
    }

    /**
     * Define o valor da propriedade minTemperature.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLTemperatureType }
     *     
     */
    public void setMinTemperature(GLogXMLTemperatureType value) {
        this.minTemperature = value;
    }

    /**
     * Obtém o valor da propriedade maxTemperature.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLTemperatureType }
     *     
     */
    public GLogXMLTemperatureType getMaxTemperature() {
        return maxTemperature;
    }

    /**
     * Define o valor da propriedade maxTemperature.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLTemperatureType }
     *     
     */
    public void setMaxTemperature(GLogXMLTemperatureType value) {
        this.maxTemperature = value;
    }

    /**
     * Obtém o valor da propriedade percentHumidity.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPercentHumidity() {
        return percentHumidity;
    }

    /**
     * Define o valor da propriedade percentHumidity.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPercentHumidity(String value) {
        this.percentHumidity = value;
    }

    /**
     * Obtém o valor da propriedade ventSetting.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVentSetting() {
        return ventSetting;
    }

    /**
     * Define o valor da propriedade ventSetting.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVentSetting(String value) {
        this.ventSetting = value;
    }

    /**
     * Obtém o valor da propriedade weightVolume.
     * 
     * @return
     *     possible object is
     *     {@link WeightVolumeType }
     *     
     */
    public WeightVolumeType getWeightVolume() {
        return weightVolume;
    }

    /**
     * Define o valor da propriedade weightVolume.
     * 
     * @param value
     *     allowed object is
     *     {@link WeightVolumeType }
     *     
     */
    public void setWeightVolume(WeightVolumeType value) {
        this.weightVolume = value;
    }

    /**
     * Obtém o valor da propriedade lengthWidthHeight.
     * 
     * @return
     *     possible object is
     *     {@link LengthWidthHeightType }
     *     
     */
    public LengthWidthHeightType getLengthWidthHeight() {
        return lengthWidthHeight;
    }

    /**
     * Define o valor da propriedade lengthWidthHeight.
     * 
     * @param value
     *     allowed object is
     *     {@link LengthWidthHeightType }
     *     
     */
    public void setLengthWidthHeight(LengthWidthHeightType value) {
        this.lengthWidthHeight = value;
    }

    /**
     * Obtém o valor da propriedade tareWeight.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLWeightType }
     *     
     */
    public GLogXMLWeightType getTareWeight() {
        return tareWeight;
    }

    /**
     * Define o valor da propriedade tareWeight.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLWeightType }
     *     
     */
    public void setTareWeight(GLogXMLWeightType value) {
        this.tareWeight = value;
    }

    /**
     * Obtém o valor da propriedade dunnage.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLWeightType }
     *     
     */
    public GLogXMLWeightType getDunnage() {
        return dunnage;
    }

    /**
     * Define o valor da propriedade dunnage.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLWeightType }
     *     
     */
    public void setDunnage(GLogXMLWeightType value) {
        this.dunnage = value;
    }

    /**
     * Obtém o valor da propriedade unitCount.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUnitCount() {
        return unitCount;
    }

    /**
     * Define o valor da propriedade unitCount.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUnitCount(String value) {
        this.unitCount = value;
    }

    /**
     * Obtém o valor da propriedade ownershipCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOwnershipCode() {
        return ownershipCode;
    }

    /**
     * Define o valor da propriedade ownershipCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOwnershipCode(String value) {
        this.ownershipCode = value;
    }

}
