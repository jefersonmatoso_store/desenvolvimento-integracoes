
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 *             InRouteCarriers identifies a particular carrier that is involved in the shipment/activity
 *          
 * 
 * <p>Classe Java de InRouteCarriersType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="InRouteCarriersType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ServiceProviderAlias" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ServiceProviderAliasType" minOccurs="0"/>
 *         &lt;element name="InRouteCarrierSequence" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InRouteTransportationMethod" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InRouteCarriersType", propOrder = {
    "serviceProviderAlias",
    "inRouteCarrierSequence",
    "inRouteTransportationMethod"
})
public class InRouteCarriersType {

    @XmlElement(name = "ServiceProviderAlias")
    protected ServiceProviderAliasType serviceProviderAlias;
    @XmlElement(name = "InRouteCarrierSequence")
    protected String inRouteCarrierSequence;
    @XmlElement(name = "InRouteTransportationMethod")
    protected String inRouteTransportationMethod;

    /**
     * Obtém o valor da propriedade serviceProviderAlias.
     * 
     * @return
     *     possible object is
     *     {@link ServiceProviderAliasType }
     *     
     */
    public ServiceProviderAliasType getServiceProviderAlias() {
        return serviceProviderAlias;
    }

    /**
     * Define o valor da propriedade serviceProviderAlias.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceProviderAliasType }
     *     
     */
    public void setServiceProviderAlias(ServiceProviderAliasType value) {
        this.serviceProviderAlias = value;
    }

    /**
     * Obtém o valor da propriedade inRouteCarrierSequence.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInRouteCarrierSequence() {
        return inRouteCarrierSequence;
    }

    /**
     * Define o valor da propriedade inRouteCarrierSequence.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInRouteCarrierSequence(String value) {
        this.inRouteCarrierSequence = value;
    }

    /**
     * Obtém o valor da propriedade inRouteTransportationMethod.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInRouteTransportationMethod() {
        return inRouteTransportationMethod;
    }

    /**
     * Define o valor da propriedade inRouteTransportationMethod.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInRouteTransportationMethod(String value) {
        this.inRouteTransportationMethod = value;
    }

}
