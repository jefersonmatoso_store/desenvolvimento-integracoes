
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * Contains shipment detail information resulting from the RIQ Query using the RIQ Route
 *             functionality.
 *          
 * 
 * <p>Classe Java de RIQRouteShipmentType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="RIQRouteShipmentType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ServiceProviderGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="RateOfferingGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="RateGeoGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="RateServiceGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="TransportModeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="EquipmentGroupGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="SourceLocationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="DestLocationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="FlightGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="StartTimestamp" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TimeWithTimezoneType" minOccurs="0"/>
 *         &lt;element name="EndTimestamp" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TimeWithTimezoneType" minOccurs="0"/>
 *         &lt;element name="Distance" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}DistanceType" minOccurs="0"/>
 *         &lt;element name="TotalActualCost" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/>
 *         &lt;element name="TotalWeightedCost" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/>
 *         &lt;element name="TotalActualCostPerUOM" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLCostPerUOM" minOccurs="0"/>
 *         &lt;element name="TotalWeightedCostPerUOM" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLCostPerUOM" minOccurs="0"/>
 *         &lt;element name="Indicator" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OrigCarrierGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="DelivCarrierGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="RouteCodeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="RouteCodeCombinationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="CostDetails" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}CostDetailsType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="RIQResultInfo" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RIQResultInfoType" minOccurs="0"/>
 *         &lt;element name="IsPrimary" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EstArrivalTimeSrc" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="EstDeptTimeSrc" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="EstArrivalTimeDest" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="EstDeptTimeDest" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RIQRouteShipmentType", propOrder = {
    "serviceProviderGid",
    "rateOfferingGid",
    "rateGeoGid",
    "rateServiceGid",
    "transportModeGid",
    "equipmentGroupGid",
    "sourceLocationGid",
    "destLocationGid",
    "flightGid",
    "startTimestamp",
    "endTimestamp",
    "distance",
    "totalActualCost",
    "totalWeightedCost",
    "totalActualCostPerUOM",
    "totalWeightedCostPerUOM",
    "indicator",
    "origCarrierGid",
    "delivCarrierGid",
    "routeCodeGid",
    "routeCodeCombinationGid",
    "costDetails",
    "riqResultInfo",
    "isPrimary",
    "estArrivalTimeSrc",
    "estDeptTimeSrc",
    "estArrivalTimeDest",
    "estDeptTimeDest"
})
public class RIQRouteShipmentType {

    @XmlElement(name = "ServiceProviderGid", required = true)
    protected GLogXMLGidType serviceProviderGid;
    @XmlElement(name = "RateOfferingGid", required = true)
    protected GLogXMLGidType rateOfferingGid;
    @XmlElement(name = "RateGeoGid", required = true)
    protected GLogXMLGidType rateGeoGid;
    @XmlElement(name = "RateServiceGid")
    protected GLogXMLGidType rateServiceGid;
    @XmlElement(name = "TransportModeGid", required = true)
    protected GLogXMLGidType transportModeGid;
    @XmlElement(name = "EquipmentGroupGid")
    protected GLogXMLGidType equipmentGroupGid;
    @XmlElement(name = "SourceLocationGid", required = true)
    protected GLogXMLGidType sourceLocationGid;
    @XmlElement(name = "DestLocationGid", required = true)
    protected GLogXMLGidType destLocationGid;
    @XmlElement(name = "FlightGid")
    protected GLogXMLGidType flightGid;
    @XmlElement(name = "StartTimestamp")
    protected TimeWithTimezoneType startTimestamp;
    @XmlElement(name = "EndTimestamp")
    protected TimeWithTimezoneType endTimestamp;
    @XmlElement(name = "Distance")
    protected DistanceType distance;
    @XmlElement(name = "TotalActualCost")
    protected GLogXMLFinancialAmountType totalActualCost;
    @XmlElement(name = "TotalWeightedCost")
    protected GLogXMLFinancialAmountType totalWeightedCost;
    @XmlElement(name = "TotalActualCostPerUOM")
    protected GLogXMLCostPerUOM totalActualCostPerUOM;
    @XmlElement(name = "TotalWeightedCostPerUOM")
    protected GLogXMLCostPerUOM totalWeightedCostPerUOM;
    @XmlElement(name = "Indicator")
    protected String indicator;
    @XmlElement(name = "OrigCarrierGid")
    protected GLogXMLGidType origCarrierGid;
    @XmlElement(name = "DelivCarrierGid")
    protected GLogXMLGidType delivCarrierGid;
    @XmlElement(name = "RouteCodeGid")
    protected GLogXMLGidType routeCodeGid;
    @XmlElement(name = "RouteCodeCombinationGid")
    protected GLogXMLGidType routeCodeCombinationGid;
    @XmlElement(name = "CostDetails")
    protected List<CostDetailsType> costDetails;
    @XmlElement(name = "RIQResultInfo")
    protected RIQResultInfoType riqResultInfo;
    @XmlElement(name = "IsPrimary")
    protected String isPrimary;
    @XmlElement(name = "EstArrivalTimeSrc")
    protected GLogDateTimeType estArrivalTimeSrc;
    @XmlElement(name = "EstDeptTimeSrc")
    protected GLogDateTimeType estDeptTimeSrc;
    @XmlElement(name = "EstArrivalTimeDest")
    protected GLogDateTimeType estArrivalTimeDest;
    @XmlElement(name = "EstDeptTimeDest")
    protected GLogDateTimeType estDeptTimeDest;

    /**
     * Obtém o valor da propriedade serviceProviderGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getServiceProviderGid() {
        return serviceProviderGid;
    }

    /**
     * Define o valor da propriedade serviceProviderGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setServiceProviderGid(GLogXMLGidType value) {
        this.serviceProviderGid = value;
    }

    /**
     * Obtém o valor da propriedade rateOfferingGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRateOfferingGid() {
        return rateOfferingGid;
    }

    /**
     * Define o valor da propriedade rateOfferingGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRateOfferingGid(GLogXMLGidType value) {
        this.rateOfferingGid = value;
    }

    /**
     * Obtém o valor da propriedade rateGeoGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRateGeoGid() {
        return rateGeoGid;
    }

    /**
     * Define o valor da propriedade rateGeoGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRateGeoGid(GLogXMLGidType value) {
        this.rateGeoGid = value;
    }

    /**
     * Obtém o valor da propriedade rateServiceGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRateServiceGid() {
        return rateServiceGid;
    }

    /**
     * Define o valor da propriedade rateServiceGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRateServiceGid(GLogXMLGidType value) {
        this.rateServiceGid = value;
    }

    /**
     * Obtém o valor da propriedade transportModeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getTransportModeGid() {
        return transportModeGid;
    }

    /**
     * Define o valor da propriedade transportModeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setTransportModeGid(GLogXMLGidType value) {
        this.transportModeGid = value;
    }

    /**
     * Obtém o valor da propriedade equipmentGroupGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getEquipmentGroupGid() {
        return equipmentGroupGid;
    }

    /**
     * Define o valor da propriedade equipmentGroupGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setEquipmentGroupGid(GLogXMLGidType value) {
        this.equipmentGroupGid = value;
    }

    /**
     * Obtém o valor da propriedade sourceLocationGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getSourceLocationGid() {
        return sourceLocationGid;
    }

    /**
     * Define o valor da propriedade sourceLocationGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setSourceLocationGid(GLogXMLGidType value) {
        this.sourceLocationGid = value;
    }

    /**
     * Obtém o valor da propriedade destLocationGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getDestLocationGid() {
        return destLocationGid;
    }

    /**
     * Define o valor da propriedade destLocationGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setDestLocationGid(GLogXMLGidType value) {
        this.destLocationGid = value;
    }

    /**
     * Obtém o valor da propriedade flightGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getFlightGid() {
        return flightGid;
    }

    /**
     * Define o valor da propriedade flightGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setFlightGid(GLogXMLGidType value) {
        this.flightGid = value;
    }

    /**
     * Obtém o valor da propriedade startTimestamp.
     * 
     * @return
     *     possible object is
     *     {@link TimeWithTimezoneType }
     *     
     */
    public TimeWithTimezoneType getStartTimestamp() {
        return startTimestamp;
    }

    /**
     * Define o valor da propriedade startTimestamp.
     * 
     * @param value
     *     allowed object is
     *     {@link TimeWithTimezoneType }
     *     
     */
    public void setStartTimestamp(TimeWithTimezoneType value) {
        this.startTimestamp = value;
    }

    /**
     * Obtém o valor da propriedade endTimestamp.
     * 
     * @return
     *     possible object is
     *     {@link TimeWithTimezoneType }
     *     
     */
    public TimeWithTimezoneType getEndTimestamp() {
        return endTimestamp;
    }

    /**
     * Define o valor da propriedade endTimestamp.
     * 
     * @param value
     *     allowed object is
     *     {@link TimeWithTimezoneType }
     *     
     */
    public void setEndTimestamp(TimeWithTimezoneType value) {
        this.endTimestamp = value;
    }

    /**
     * Obtém o valor da propriedade distance.
     * 
     * @return
     *     possible object is
     *     {@link DistanceType }
     *     
     */
    public DistanceType getDistance() {
        return distance;
    }

    /**
     * Define o valor da propriedade distance.
     * 
     * @param value
     *     allowed object is
     *     {@link DistanceType }
     *     
     */
    public void setDistance(DistanceType value) {
        this.distance = value;
    }

    /**
     * Obtém o valor da propriedade totalActualCost.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getTotalActualCost() {
        return totalActualCost;
    }

    /**
     * Define o valor da propriedade totalActualCost.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setTotalActualCost(GLogXMLFinancialAmountType value) {
        this.totalActualCost = value;
    }

    /**
     * Obtém o valor da propriedade totalWeightedCost.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getTotalWeightedCost() {
        return totalWeightedCost;
    }

    /**
     * Define o valor da propriedade totalWeightedCost.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setTotalWeightedCost(GLogXMLFinancialAmountType value) {
        this.totalWeightedCost = value;
    }

    /**
     * Obtém o valor da propriedade totalActualCostPerUOM.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLCostPerUOM }
     *     
     */
    public GLogXMLCostPerUOM getTotalActualCostPerUOM() {
        return totalActualCostPerUOM;
    }

    /**
     * Define o valor da propriedade totalActualCostPerUOM.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLCostPerUOM }
     *     
     */
    public void setTotalActualCostPerUOM(GLogXMLCostPerUOM value) {
        this.totalActualCostPerUOM = value;
    }

    /**
     * Obtém o valor da propriedade totalWeightedCostPerUOM.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLCostPerUOM }
     *     
     */
    public GLogXMLCostPerUOM getTotalWeightedCostPerUOM() {
        return totalWeightedCostPerUOM;
    }

    /**
     * Define o valor da propriedade totalWeightedCostPerUOM.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLCostPerUOM }
     *     
     */
    public void setTotalWeightedCostPerUOM(GLogXMLCostPerUOM value) {
        this.totalWeightedCostPerUOM = value;
    }

    /**
     * Obtém o valor da propriedade indicator.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIndicator() {
        return indicator;
    }

    /**
     * Define o valor da propriedade indicator.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIndicator(String value) {
        this.indicator = value;
    }

    /**
     * Obtém o valor da propriedade origCarrierGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getOrigCarrierGid() {
        return origCarrierGid;
    }

    /**
     * Define o valor da propriedade origCarrierGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setOrigCarrierGid(GLogXMLGidType value) {
        this.origCarrierGid = value;
    }

    /**
     * Obtém o valor da propriedade delivCarrierGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getDelivCarrierGid() {
        return delivCarrierGid;
    }

    /**
     * Define o valor da propriedade delivCarrierGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setDelivCarrierGid(GLogXMLGidType value) {
        this.delivCarrierGid = value;
    }

    /**
     * Obtém o valor da propriedade routeCodeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRouteCodeGid() {
        return routeCodeGid;
    }

    /**
     * Define o valor da propriedade routeCodeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRouteCodeGid(GLogXMLGidType value) {
        this.routeCodeGid = value;
    }

    /**
     * Obtém o valor da propriedade routeCodeCombinationGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRouteCodeCombinationGid() {
        return routeCodeCombinationGid;
    }

    /**
     * Define o valor da propriedade routeCodeCombinationGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRouteCodeCombinationGid(GLogXMLGidType value) {
        this.routeCodeCombinationGid = value;
    }

    /**
     * Gets the value of the costDetails property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the costDetails property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCostDetails().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CostDetailsType }
     * 
     * 
     */
    public List<CostDetailsType> getCostDetails() {
        if (costDetails == null) {
            costDetails = new ArrayList<CostDetailsType>();
        }
        return this.costDetails;
    }

    /**
     * Obtém o valor da propriedade riqResultInfo.
     * 
     * @return
     *     possible object is
     *     {@link RIQResultInfoType }
     *     
     */
    public RIQResultInfoType getRIQResultInfo() {
        return riqResultInfo;
    }

    /**
     * Define o valor da propriedade riqResultInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link RIQResultInfoType }
     *     
     */
    public void setRIQResultInfo(RIQResultInfoType value) {
        this.riqResultInfo = value;
    }

    /**
     * Obtém o valor da propriedade isPrimary.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsPrimary() {
        return isPrimary;
    }

    /**
     * Define o valor da propriedade isPrimary.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsPrimary(String value) {
        this.isPrimary = value;
    }

    /**
     * Obtém o valor da propriedade estArrivalTimeSrc.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getEstArrivalTimeSrc() {
        return estArrivalTimeSrc;
    }

    /**
     * Define o valor da propriedade estArrivalTimeSrc.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setEstArrivalTimeSrc(GLogDateTimeType value) {
        this.estArrivalTimeSrc = value;
    }

    /**
     * Obtém o valor da propriedade estDeptTimeSrc.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getEstDeptTimeSrc() {
        return estDeptTimeSrc;
    }

    /**
     * Define o valor da propriedade estDeptTimeSrc.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setEstDeptTimeSrc(GLogDateTimeType value) {
        this.estDeptTimeSrc = value;
    }

    /**
     * Obtém o valor da propriedade estArrivalTimeDest.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getEstArrivalTimeDest() {
        return estArrivalTimeDest;
    }

    /**
     * Define o valor da propriedade estArrivalTimeDest.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setEstArrivalTimeDest(GLogDateTimeType value) {
        this.estArrivalTimeDest = value;
    }

    /**
     * Obtém o valor da propriedade estDeptTimeDest.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getEstDeptTimeDest() {
        return estDeptTimeDest;
    }

    /**
     * Define o valor da propriedade estDeptTimeDest.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setEstDeptTimeDest(GLogDateTimeType value) {
        this.estDeptTimeDest = value;
    }

}
