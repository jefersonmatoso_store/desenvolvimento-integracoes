
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * 
 *             This is used to provide statistics about the fleet resource assignments that were created to the planned shipments during a
 *             given run of fleet bulk plan.
 *          
 * 
 * <p>Classe Java de FleetBulkPlanType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="FleetBulkPlanType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OTMTransactionOut">
 *       &lt;sequence>
 *         &lt;element name="SendReason" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}SendReasonType" minOccurs="0"/>
 *         &lt;element name="FleetBulkPlanGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="ShipmentQueryName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ResourceQueryName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="StartTime" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="EndTime" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="NumOfShipmentsSelected" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NumOfResourcesSelected" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ResourceType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NumOfShipmentsAssignable" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NumOfResourcesAssignable" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NumOfResourcesWithMultiAssign" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NumOfShipmentsAssigned" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NumOfResourcesAssigned" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PreShipCost" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/>
 *         &lt;element name="AfterShipCost" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/>
 *         &lt;element name="BobTailDist" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDistanceType" minOccurs="0"/>
 *         &lt;element name="LoadedDist" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDistanceType" minOccurs="0"/>
 *         &lt;element name="DeadHeadDist" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDistanceType" minOccurs="0"/>
 *         &lt;element name="PlanningParamSetGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="FleetBulkPlanCost" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FleetBulkPlanCostType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FleetBulkPlanType", propOrder = {
    "sendReason",
    "fleetBulkPlanGid",
    "shipmentQueryName",
    "resourceQueryName",
    "startTime",
    "endTime",
    "numOfShipmentsSelected",
    "numOfResourcesSelected",
    "resourceType",
    "numOfShipmentsAssignable",
    "numOfResourcesAssignable",
    "numOfResourcesWithMultiAssign",
    "numOfShipmentsAssigned",
    "numOfResourcesAssigned",
    "preShipCost",
    "afterShipCost",
    "bobTailDist",
    "loadedDist",
    "deadHeadDist",
    "planningParamSetGid",
    "fleetBulkPlanCost"
})
public class FleetBulkPlanType
    extends OTMTransactionOut
{

    @XmlElement(name = "SendReason")
    protected SendReasonType sendReason;
    @XmlElement(name = "FleetBulkPlanGid", required = true)
    protected GLogXMLGidType fleetBulkPlanGid;
    @XmlElement(name = "ShipmentQueryName")
    protected String shipmentQueryName;
    @XmlElement(name = "ResourceQueryName")
    protected String resourceQueryName;
    @XmlElement(name = "StartTime")
    protected GLogDateTimeType startTime;
    @XmlElement(name = "EndTime")
    protected GLogDateTimeType endTime;
    @XmlElement(name = "NumOfShipmentsSelected")
    protected String numOfShipmentsSelected;
    @XmlElement(name = "NumOfResourcesSelected")
    protected String numOfResourcesSelected;
    @XmlElement(name = "ResourceType")
    protected String resourceType;
    @XmlElement(name = "NumOfShipmentsAssignable")
    protected String numOfShipmentsAssignable;
    @XmlElement(name = "NumOfResourcesAssignable")
    protected String numOfResourcesAssignable;
    @XmlElement(name = "NumOfResourcesWithMultiAssign")
    protected String numOfResourcesWithMultiAssign;
    @XmlElement(name = "NumOfShipmentsAssigned")
    protected String numOfShipmentsAssigned;
    @XmlElement(name = "NumOfResourcesAssigned")
    protected String numOfResourcesAssigned;
    @XmlElement(name = "PreShipCost")
    protected GLogXMLFinancialAmountType preShipCost;
    @XmlElement(name = "AfterShipCost")
    protected GLogXMLFinancialAmountType afterShipCost;
    @XmlElement(name = "BobTailDist")
    protected GLogXMLDistanceType bobTailDist;
    @XmlElement(name = "LoadedDist")
    protected GLogXMLDistanceType loadedDist;
    @XmlElement(name = "DeadHeadDist")
    protected GLogXMLDistanceType deadHeadDist;
    @XmlElement(name = "PlanningParamSetGid")
    protected GLogXMLGidType planningParamSetGid;
    @XmlElement(name = "FleetBulkPlanCost")
    protected List<FleetBulkPlanCostType> fleetBulkPlanCost;

    /**
     * Obtém o valor da propriedade sendReason.
     * 
     * @return
     *     possible object is
     *     {@link SendReasonType }
     *     
     */
    public SendReasonType getSendReason() {
        return sendReason;
    }

    /**
     * Define o valor da propriedade sendReason.
     * 
     * @param value
     *     allowed object is
     *     {@link SendReasonType }
     *     
     */
    public void setSendReason(SendReasonType value) {
        this.sendReason = value;
    }

    /**
     * Obtém o valor da propriedade fleetBulkPlanGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getFleetBulkPlanGid() {
        return fleetBulkPlanGid;
    }

    /**
     * Define o valor da propriedade fleetBulkPlanGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setFleetBulkPlanGid(GLogXMLGidType value) {
        this.fleetBulkPlanGid = value;
    }

    /**
     * Obtém o valor da propriedade shipmentQueryName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipmentQueryName() {
        return shipmentQueryName;
    }

    /**
     * Define o valor da propriedade shipmentQueryName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipmentQueryName(String value) {
        this.shipmentQueryName = value;
    }

    /**
     * Obtém o valor da propriedade resourceQueryName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResourceQueryName() {
        return resourceQueryName;
    }

    /**
     * Define o valor da propriedade resourceQueryName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResourceQueryName(String value) {
        this.resourceQueryName = value;
    }

    /**
     * Obtém o valor da propriedade startTime.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getStartTime() {
        return startTime;
    }

    /**
     * Define o valor da propriedade startTime.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setStartTime(GLogDateTimeType value) {
        this.startTime = value;
    }

    /**
     * Obtém o valor da propriedade endTime.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getEndTime() {
        return endTime;
    }

    /**
     * Define o valor da propriedade endTime.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setEndTime(GLogDateTimeType value) {
        this.endTime = value;
    }

    /**
     * Obtém o valor da propriedade numOfShipmentsSelected.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumOfShipmentsSelected() {
        return numOfShipmentsSelected;
    }

    /**
     * Define o valor da propriedade numOfShipmentsSelected.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumOfShipmentsSelected(String value) {
        this.numOfShipmentsSelected = value;
    }

    /**
     * Obtém o valor da propriedade numOfResourcesSelected.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumOfResourcesSelected() {
        return numOfResourcesSelected;
    }

    /**
     * Define o valor da propriedade numOfResourcesSelected.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumOfResourcesSelected(String value) {
        this.numOfResourcesSelected = value;
    }

    /**
     * Obtém o valor da propriedade resourceType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResourceType() {
        return resourceType;
    }

    /**
     * Define o valor da propriedade resourceType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResourceType(String value) {
        this.resourceType = value;
    }

    /**
     * Obtém o valor da propriedade numOfShipmentsAssignable.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumOfShipmentsAssignable() {
        return numOfShipmentsAssignable;
    }

    /**
     * Define o valor da propriedade numOfShipmentsAssignable.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumOfShipmentsAssignable(String value) {
        this.numOfShipmentsAssignable = value;
    }

    /**
     * Obtém o valor da propriedade numOfResourcesAssignable.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumOfResourcesAssignable() {
        return numOfResourcesAssignable;
    }

    /**
     * Define o valor da propriedade numOfResourcesAssignable.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumOfResourcesAssignable(String value) {
        this.numOfResourcesAssignable = value;
    }

    /**
     * Obtém o valor da propriedade numOfResourcesWithMultiAssign.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumOfResourcesWithMultiAssign() {
        return numOfResourcesWithMultiAssign;
    }

    /**
     * Define o valor da propriedade numOfResourcesWithMultiAssign.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumOfResourcesWithMultiAssign(String value) {
        this.numOfResourcesWithMultiAssign = value;
    }

    /**
     * Obtém o valor da propriedade numOfShipmentsAssigned.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumOfShipmentsAssigned() {
        return numOfShipmentsAssigned;
    }

    /**
     * Define o valor da propriedade numOfShipmentsAssigned.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumOfShipmentsAssigned(String value) {
        this.numOfShipmentsAssigned = value;
    }

    /**
     * Obtém o valor da propriedade numOfResourcesAssigned.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumOfResourcesAssigned() {
        return numOfResourcesAssigned;
    }

    /**
     * Define o valor da propriedade numOfResourcesAssigned.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumOfResourcesAssigned(String value) {
        this.numOfResourcesAssigned = value;
    }

    /**
     * Obtém o valor da propriedade preShipCost.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getPreShipCost() {
        return preShipCost;
    }

    /**
     * Define o valor da propriedade preShipCost.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setPreShipCost(GLogXMLFinancialAmountType value) {
        this.preShipCost = value;
    }

    /**
     * Obtém o valor da propriedade afterShipCost.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getAfterShipCost() {
        return afterShipCost;
    }

    /**
     * Define o valor da propriedade afterShipCost.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setAfterShipCost(GLogXMLFinancialAmountType value) {
        this.afterShipCost = value;
    }

    /**
     * Obtém o valor da propriedade bobTailDist.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDistanceType }
     *     
     */
    public GLogXMLDistanceType getBobTailDist() {
        return bobTailDist;
    }

    /**
     * Define o valor da propriedade bobTailDist.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDistanceType }
     *     
     */
    public void setBobTailDist(GLogXMLDistanceType value) {
        this.bobTailDist = value;
    }

    /**
     * Obtém o valor da propriedade loadedDist.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDistanceType }
     *     
     */
    public GLogXMLDistanceType getLoadedDist() {
        return loadedDist;
    }

    /**
     * Define o valor da propriedade loadedDist.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDistanceType }
     *     
     */
    public void setLoadedDist(GLogXMLDistanceType value) {
        this.loadedDist = value;
    }

    /**
     * Obtém o valor da propriedade deadHeadDist.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDistanceType }
     *     
     */
    public GLogXMLDistanceType getDeadHeadDist() {
        return deadHeadDist;
    }

    /**
     * Define o valor da propriedade deadHeadDist.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDistanceType }
     *     
     */
    public void setDeadHeadDist(GLogXMLDistanceType value) {
        this.deadHeadDist = value;
    }

    /**
     * Obtém o valor da propriedade planningParamSetGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getPlanningParamSetGid() {
        return planningParamSetGid;
    }

    /**
     * Define o valor da propriedade planningParamSetGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setPlanningParamSetGid(GLogXMLGidType value) {
        this.planningParamSetGid = value;
    }

    /**
     * Gets the value of the fleetBulkPlanCost property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the fleetBulkPlanCost property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFleetBulkPlanCost().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FleetBulkPlanCostType }
     * 
     * 
     */
    public List<FleetBulkPlanCostType> getFleetBulkPlanCost() {
        if (fleetBulkPlanCost == null) {
            fleetBulkPlanCost = new ArrayList<FleetBulkPlanCostType>();
        }
        return this.fleetBulkPlanCost;
    }

}
