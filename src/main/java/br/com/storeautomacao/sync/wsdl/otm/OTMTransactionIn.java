
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de OTMTransactionIn complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="OTMTransactionIn">
 *   &lt;complexContent>
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLTransactionType">
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OTMTransactionIn")
@XmlSeeAlso({
    TransOrderLinkType.class,
    ReleaseInstructionType.class,
    ItineraryType.class,
    RouteTemplateType.class,
    MileageType.class,
    ActualShipmentType.class,
    DriverCalendarEventType.class,
    UserType.class,
    GenericStatusUpdateType.class,
    ShipmentLinkType.class,
    RemoteQueryType.class,
    TopicType.class,
    TransactionAckType.class,
    SShipUnitType.class,
    TransOrderStatusType.class,
    TenderResponseType.class,
    ShipStopType.class,
    ServiceTimeType.class,
    ContactGroupType.class,
    GtmStructureType.class,
    ServiceRequestType.class
})
public abstract class OTMTransactionIn
    extends GLogXMLTransactionType
{


}
