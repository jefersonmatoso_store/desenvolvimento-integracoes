
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * The TagInfo element is used to specify tagging information for tracing purposes. When an TransOrder is created with the
 *             tag information, the fields will be copied when the release is created. Similarly if the fields are on the Release,
 *             they will be copied to the Shipment when it is created.
 *          
 * 
 * <p>Classe Java de TagInfoType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="TagInfoType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ItemTag1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ItemTag2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ItemTag3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ItemTag4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TagInfoType", propOrder = {
    "itemTag1",
    "itemTag2",
    "itemTag3",
    "itemTag4"
})
public class TagInfoType {

    @XmlElement(name = "ItemTag1")
    protected String itemTag1;
    @XmlElement(name = "ItemTag2")
    protected String itemTag2;
    @XmlElement(name = "ItemTag3")
    protected String itemTag3;
    @XmlElement(name = "ItemTag4")
    protected String itemTag4;

    /**
     * Obtém o valor da propriedade itemTag1.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItemTag1() {
        return itemTag1;
    }

    /**
     * Define o valor da propriedade itemTag1.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItemTag1(String value) {
        this.itemTag1 = value;
    }

    /**
     * Obtém o valor da propriedade itemTag2.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItemTag2() {
        return itemTag2;
    }

    /**
     * Define o valor da propriedade itemTag2.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItemTag2(String value) {
        this.itemTag2 = value;
    }

    /**
     * Obtém o valor da propriedade itemTag3.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItemTag3() {
        return itemTag3;
    }

    /**
     * Define o valor da propriedade itemTag3.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItemTag3(String value) {
        this.itemTag3 = value;
    }

    /**
     * Obtém o valor da propriedade itemTag4.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItemTag4() {
        return itemTag4;
    }

    /**
     * Define o valor da propriedade itemTag4.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItemTag4(String value) {
        this.itemTag4 = value;
    }

}
