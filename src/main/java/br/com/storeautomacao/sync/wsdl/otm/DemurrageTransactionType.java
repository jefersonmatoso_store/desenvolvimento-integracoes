
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * 
 *             The demurrage trasaction is built based on events (like CLM, EDI322 etc) and help manage resouces (equipments) that
 *             are incuring costs. It includes charges (that are time based) called demurrage, detention and storage with no clear
 *             definition of terms.
 *          
 * 
 * <p>Classe Java de DemurrageTransactionType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="DemurrageTransactionType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OTMTransactionOut">
 *       &lt;sequence>
 *         &lt;element name="ReportingScac" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DmLocationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="DmTimeZoneGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="DmCity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DmState" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DmCountry" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DmSplc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CpLocationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="CpTimeZoneGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="CpCity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CpState" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CpCountry" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CpSplc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EquipmentInitial" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EquipmentNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EquipmentInitialNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EquipmentGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="EquipmentTypeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="EquipmentRefnumQualifierGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="EquipmentRefnumValue" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AARCarType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ChassisInitial" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ChassisNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ChassisInitialNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="StartEventStatusCodeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="StartLocationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="StartScacGid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="StartTimeZoneGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="StartCity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="StartState" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="StartCountry" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="StartSplc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="StartIsCarLoaded" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EndEventStatusCodeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="EndLocationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="EndScacGid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EndTimeZoneGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="EndCity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EndState" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EndCountry" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EndSplc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EndIsCarLoaded" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InShipmentGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="InSEquipmentGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="InSTCCGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="InUserDefinedCommodityGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="InSourceLocationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="InDestLocationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="InShipUnitCount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InWeight" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InWeightUomCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InWeightBase" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InPackagedItemGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="InItemGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="InTHUGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="OutShipmentGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="OutSEquipmentGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="OutSTCCGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="OutUserDefinedCommodityGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="OutSourceLocationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="OutDestLocationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="OutShipUnitCount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OutWeight" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OutWeightUomCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OutWeightBase" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OutPackagedItemGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="OutItemGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="OutTHUGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="DmEventDate1" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="DmEventDate2" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="DmEventDate3" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="DmEventDate4" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="DmEventDate5" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="DmEventDate6" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="DmEventDate7" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="DmEventDate8" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="DmEventDate9" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="DmEventDate10" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="BsStatusCode1Gid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="BsStatusCode2Gid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="BsStatusCode3Gid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="BsStatusCode4Gid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="BsStatusCode5Gid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="BsStatusCode6Gid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="BsStatusCode7Gid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="BsStatusCode8Gid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="BsStatusCode9Gid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="BsStatusCode10Gid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="IsSystemGenerated" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MarkedForPurge" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ActivityTypeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="ModeType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EndDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="DemurrageTransactionEvent" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}DemurrageTransactionEventType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="DemurrageTransactionInvolvedParty" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}DemurrageInvolvedPartyType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="DemurrageTransactionLineitem" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}DemurrageTransactionLineitemType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="DemurrageTransactionNote" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}DemurrageTransactionNoteType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="DemurrageTransactionRefnum" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}DemurrageTransactionRefnumType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="DemurrageTransactionRemark" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}DemurrageTransactionRemarkType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="DemurrageTransactionStatus" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}StatusType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DemurrageTransactionType", propOrder = {
    "reportingScac",
    "dmLocationGid",
    "dmTimeZoneGid",
    "dmCity",
    "dmState",
    "dmCountry",
    "dmSplc",
    "cpLocationGid",
    "cpTimeZoneGid",
    "cpCity",
    "cpState",
    "cpCountry",
    "cpSplc",
    "equipmentInitial",
    "equipmentNumber",
    "equipmentInitialNumber",
    "equipmentGid",
    "equipmentTypeGid",
    "equipmentRefnumQualifierGid",
    "equipmentRefnumValue",
    "aarCarType",
    "chassisInitial",
    "chassisNumber",
    "chassisInitialNumber",
    "startEventStatusCodeGid",
    "startLocationGid",
    "startScacGid",
    "startTimeZoneGid",
    "startCity",
    "startState",
    "startCountry",
    "startSplc",
    "startIsCarLoaded",
    "endEventStatusCodeGid",
    "endLocationGid",
    "endScacGid",
    "endTimeZoneGid",
    "endCity",
    "endState",
    "endCountry",
    "endSplc",
    "endIsCarLoaded",
    "inShipmentGid",
    "inSEquipmentGid",
    "inSTCCGid",
    "inUserDefinedCommodityGid",
    "inSourceLocationGid",
    "inDestLocationGid",
    "inShipUnitCount",
    "inWeight",
    "inWeightUomCode",
    "inWeightBase",
    "inPackagedItemGid",
    "inItemGid",
    "inTHUGid",
    "outShipmentGid",
    "outSEquipmentGid",
    "outSTCCGid",
    "outUserDefinedCommodityGid",
    "outSourceLocationGid",
    "outDestLocationGid",
    "outShipUnitCount",
    "outWeight",
    "outWeightUomCode",
    "outWeightBase",
    "outPackagedItemGid",
    "outItemGid",
    "outTHUGid",
    "dmEventDate1",
    "dmEventDate2",
    "dmEventDate3",
    "dmEventDate4",
    "dmEventDate5",
    "dmEventDate6",
    "dmEventDate7",
    "dmEventDate8",
    "dmEventDate9",
    "dmEventDate10",
    "bsStatusCode1Gid",
    "bsStatusCode2Gid",
    "bsStatusCode3Gid",
    "bsStatusCode4Gid",
    "bsStatusCode5Gid",
    "bsStatusCode6Gid",
    "bsStatusCode7Gid",
    "bsStatusCode8Gid",
    "bsStatusCode9Gid",
    "bsStatusCode10Gid",
    "isSystemGenerated",
    "markedForPurge",
    "activityTypeGid",
    "modeType",
    "endDate",
    "demurrageTransactionEvent",
    "demurrageTransactionInvolvedParty",
    "demurrageTransactionLineitem",
    "demurrageTransactionNote",
    "demurrageTransactionRefnum",
    "demurrageTransactionRemark",
    "demurrageTransactionStatus"
})
public class DemurrageTransactionType
    extends OTMTransactionOut
{

    @XmlElement(name = "ReportingScac")
    protected String reportingScac;
    @XmlElement(name = "DmLocationGid")
    protected GLogXMLGidType dmLocationGid;
    @XmlElement(name = "DmTimeZoneGid")
    protected GLogXMLGidType dmTimeZoneGid;
    @XmlElement(name = "DmCity")
    protected String dmCity;
    @XmlElement(name = "DmState")
    protected String dmState;
    @XmlElement(name = "DmCountry")
    protected String dmCountry;
    @XmlElement(name = "DmSplc")
    protected String dmSplc;
    @XmlElement(name = "CpLocationGid")
    protected GLogXMLGidType cpLocationGid;
    @XmlElement(name = "CpTimeZoneGid")
    protected GLogXMLGidType cpTimeZoneGid;
    @XmlElement(name = "CpCity")
    protected String cpCity;
    @XmlElement(name = "CpState")
    protected String cpState;
    @XmlElement(name = "CpCountry")
    protected String cpCountry;
    @XmlElement(name = "CpSplc")
    protected String cpSplc;
    @XmlElement(name = "EquipmentInitial")
    protected String equipmentInitial;
    @XmlElement(name = "EquipmentNumber")
    protected String equipmentNumber;
    @XmlElement(name = "EquipmentInitialNumber")
    protected String equipmentInitialNumber;
    @XmlElement(name = "EquipmentGid")
    protected GLogXMLGidType equipmentGid;
    @XmlElement(name = "EquipmentTypeGid")
    protected GLogXMLGidType equipmentTypeGid;
    @XmlElement(name = "EquipmentRefnumQualifierGid")
    protected GLogXMLGidType equipmentRefnumQualifierGid;
    @XmlElement(name = "EquipmentRefnumValue")
    protected String equipmentRefnumValue;
    @XmlElement(name = "AARCarType")
    protected String aarCarType;
    @XmlElement(name = "ChassisInitial")
    protected String chassisInitial;
    @XmlElement(name = "ChassisNumber")
    protected String chassisNumber;
    @XmlElement(name = "ChassisInitialNumber")
    protected String chassisInitialNumber;
    @XmlElement(name = "StartEventStatusCodeGid")
    protected GLogXMLGidType startEventStatusCodeGid;
    @XmlElement(name = "StartLocationGid")
    protected GLogXMLGidType startLocationGid;
    @XmlElement(name = "StartScacGid")
    protected String startScacGid;
    @XmlElement(name = "StartTimeZoneGid")
    protected GLogXMLGidType startTimeZoneGid;
    @XmlElement(name = "StartCity")
    protected String startCity;
    @XmlElement(name = "StartState")
    protected String startState;
    @XmlElement(name = "StartCountry")
    protected String startCountry;
    @XmlElement(name = "StartSplc")
    protected String startSplc;
    @XmlElement(name = "StartIsCarLoaded")
    protected String startIsCarLoaded;
    @XmlElement(name = "EndEventStatusCodeGid")
    protected GLogXMLGidType endEventStatusCodeGid;
    @XmlElement(name = "EndLocationGid")
    protected GLogXMLGidType endLocationGid;
    @XmlElement(name = "EndScacGid")
    protected String endScacGid;
    @XmlElement(name = "EndTimeZoneGid")
    protected GLogXMLGidType endTimeZoneGid;
    @XmlElement(name = "EndCity")
    protected String endCity;
    @XmlElement(name = "EndState")
    protected String endState;
    @XmlElement(name = "EndCountry")
    protected String endCountry;
    @XmlElement(name = "EndSplc")
    protected String endSplc;
    @XmlElement(name = "EndIsCarLoaded")
    protected String endIsCarLoaded;
    @XmlElement(name = "InShipmentGid")
    protected GLogXMLGidType inShipmentGid;
    @XmlElement(name = "InSEquipmentGid")
    protected GLogXMLGidType inSEquipmentGid;
    @XmlElement(name = "InSTCCGid")
    protected GLogXMLGidType inSTCCGid;
    @XmlElement(name = "InUserDefinedCommodityGid")
    protected GLogXMLGidType inUserDefinedCommodityGid;
    @XmlElement(name = "InSourceLocationGid")
    protected GLogXMLGidType inSourceLocationGid;
    @XmlElement(name = "InDestLocationGid")
    protected GLogXMLGidType inDestLocationGid;
    @XmlElement(name = "InShipUnitCount")
    protected String inShipUnitCount;
    @XmlElement(name = "InWeight")
    protected String inWeight;
    @XmlElement(name = "InWeightUomCode")
    protected String inWeightUomCode;
    @XmlElement(name = "InWeightBase")
    protected String inWeightBase;
    @XmlElement(name = "InPackagedItemGid")
    protected GLogXMLGidType inPackagedItemGid;
    @XmlElement(name = "InItemGid")
    protected GLogXMLGidType inItemGid;
    @XmlElement(name = "InTHUGid")
    protected GLogXMLGidType inTHUGid;
    @XmlElement(name = "OutShipmentGid")
    protected GLogXMLGidType outShipmentGid;
    @XmlElement(name = "OutSEquipmentGid")
    protected GLogXMLGidType outSEquipmentGid;
    @XmlElement(name = "OutSTCCGid")
    protected GLogXMLGidType outSTCCGid;
    @XmlElement(name = "OutUserDefinedCommodityGid")
    protected GLogXMLGidType outUserDefinedCommodityGid;
    @XmlElement(name = "OutSourceLocationGid")
    protected GLogXMLGidType outSourceLocationGid;
    @XmlElement(name = "OutDestLocationGid")
    protected GLogXMLGidType outDestLocationGid;
    @XmlElement(name = "OutShipUnitCount")
    protected String outShipUnitCount;
    @XmlElement(name = "OutWeight")
    protected String outWeight;
    @XmlElement(name = "OutWeightUomCode")
    protected String outWeightUomCode;
    @XmlElement(name = "OutWeightBase")
    protected String outWeightBase;
    @XmlElement(name = "OutPackagedItemGid")
    protected GLogXMLGidType outPackagedItemGid;
    @XmlElement(name = "OutItemGid")
    protected GLogXMLGidType outItemGid;
    @XmlElement(name = "OutTHUGid")
    protected GLogXMLGidType outTHUGid;
    @XmlElement(name = "DmEventDate1")
    protected GLogDateTimeType dmEventDate1;
    @XmlElement(name = "DmEventDate2")
    protected GLogDateTimeType dmEventDate2;
    @XmlElement(name = "DmEventDate3")
    protected GLogDateTimeType dmEventDate3;
    @XmlElement(name = "DmEventDate4")
    protected GLogDateTimeType dmEventDate4;
    @XmlElement(name = "DmEventDate5")
    protected GLogDateTimeType dmEventDate5;
    @XmlElement(name = "DmEventDate6")
    protected GLogDateTimeType dmEventDate6;
    @XmlElement(name = "DmEventDate7")
    protected GLogDateTimeType dmEventDate7;
    @XmlElement(name = "DmEventDate8")
    protected GLogDateTimeType dmEventDate8;
    @XmlElement(name = "DmEventDate9")
    protected GLogDateTimeType dmEventDate9;
    @XmlElement(name = "DmEventDate10")
    protected GLogDateTimeType dmEventDate10;
    @XmlElement(name = "BsStatusCode1Gid")
    protected GLogXMLGidType bsStatusCode1Gid;
    @XmlElement(name = "BsStatusCode2Gid")
    protected GLogXMLGidType bsStatusCode2Gid;
    @XmlElement(name = "BsStatusCode3Gid")
    protected GLogXMLGidType bsStatusCode3Gid;
    @XmlElement(name = "BsStatusCode4Gid")
    protected GLogXMLGidType bsStatusCode4Gid;
    @XmlElement(name = "BsStatusCode5Gid")
    protected GLogXMLGidType bsStatusCode5Gid;
    @XmlElement(name = "BsStatusCode6Gid")
    protected GLogXMLGidType bsStatusCode6Gid;
    @XmlElement(name = "BsStatusCode7Gid")
    protected GLogXMLGidType bsStatusCode7Gid;
    @XmlElement(name = "BsStatusCode8Gid")
    protected GLogXMLGidType bsStatusCode8Gid;
    @XmlElement(name = "BsStatusCode9Gid")
    protected GLogXMLGidType bsStatusCode9Gid;
    @XmlElement(name = "BsStatusCode10Gid")
    protected GLogXMLGidType bsStatusCode10Gid;
    @XmlElement(name = "IsSystemGenerated")
    protected String isSystemGenerated;
    @XmlElement(name = "MarkedForPurge")
    protected String markedForPurge;
    @XmlElement(name = "ActivityTypeGid")
    protected GLogXMLGidType activityTypeGid;
    @XmlElement(name = "ModeType")
    protected String modeType;
    @XmlElement(name = "EndDate")
    protected GLogDateTimeType endDate;
    @XmlElement(name = "DemurrageTransactionEvent")
    protected List<DemurrageTransactionEventType> demurrageTransactionEvent;
    @XmlElement(name = "DemurrageTransactionInvolvedParty")
    protected List<DemurrageInvolvedPartyType> demurrageTransactionInvolvedParty;
    @XmlElement(name = "DemurrageTransactionLineitem")
    protected List<DemurrageTransactionLineitemType> demurrageTransactionLineitem;
    @XmlElement(name = "DemurrageTransactionNote")
    protected List<DemurrageTransactionNoteType> demurrageTransactionNote;
    @XmlElement(name = "DemurrageTransactionRefnum")
    protected List<DemurrageTransactionRefnumType> demurrageTransactionRefnum;
    @XmlElement(name = "DemurrageTransactionRemark")
    protected List<DemurrageTransactionRemarkType> demurrageTransactionRemark;
    @XmlElement(name = "DemurrageTransactionStatus")
    protected List<StatusType> demurrageTransactionStatus;

    /**
     * Obtém o valor da propriedade reportingScac.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReportingScac() {
        return reportingScac;
    }

    /**
     * Define o valor da propriedade reportingScac.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReportingScac(String value) {
        this.reportingScac = value;
    }

    /**
     * Obtém o valor da propriedade dmLocationGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getDmLocationGid() {
        return dmLocationGid;
    }

    /**
     * Define o valor da propriedade dmLocationGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setDmLocationGid(GLogXMLGidType value) {
        this.dmLocationGid = value;
    }

    /**
     * Obtém o valor da propriedade dmTimeZoneGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getDmTimeZoneGid() {
        return dmTimeZoneGid;
    }

    /**
     * Define o valor da propriedade dmTimeZoneGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setDmTimeZoneGid(GLogXMLGidType value) {
        this.dmTimeZoneGid = value;
    }

    /**
     * Obtém o valor da propriedade dmCity.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDmCity() {
        return dmCity;
    }

    /**
     * Define o valor da propriedade dmCity.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDmCity(String value) {
        this.dmCity = value;
    }

    /**
     * Obtém o valor da propriedade dmState.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDmState() {
        return dmState;
    }

    /**
     * Define o valor da propriedade dmState.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDmState(String value) {
        this.dmState = value;
    }

    /**
     * Obtém o valor da propriedade dmCountry.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDmCountry() {
        return dmCountry;
    }

    /**
     * Define o valor da propriedade dmCountry.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDmCountry(String value) {
        this.dmCountry = value;
    }

    /**
     * Obtém o valor da propriedade dmSplc.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDmSplc() {
        return dmSplc;
    }

    /**
     * Define o valor da propriedade dmSplc.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDmSplc(String value) {
        this.dmSplc = value;
    }

    /**
     * Obtém o valor da propriedade cpLocationGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getCpLocationGid() {
        return cpLocationGid;
    }

    /**
     * Define o valor da propriedade cpLocationGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setCpLocationGid(GLogXMLGidType value) {
        this.cpLocationGid = value;
    }

    /**
     * Obtém o valor da propriedade cpTimeZoneGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getCpTimeZoneGid() {
        return cpTimeZoneGid;
    }

    /**
     * Define o valor da propriedade cpTimeZoneGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setCpTimeZoneGid(GLogXMLGidType value) {
        this.cpTimeZoneGid = value;
    }

    /**
     * Obtém o valor da propriedade cpCity.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCpCity() {
        return cpCity;
    }

    /**
     * Define o valor da propriedade cpCity.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCpCity(String value) {
        this.cpCity = value;
    }

    /**
     * Obtém o valor da propriedade cpState.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCpState() {
        return cpState;
    }

    /**
     * Define o valor da propriedade cpState.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCpState(String value) {
        this.cpState = value;
    }

    /**
     * Obtém o valor da propriedade cpCountry.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCpCountry() {
        return cpCountry;
    }

    /**
     * Define o valor da propriedade cpCountry.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCpCountry(String value) {
        this.cpCountry = value;
    }

    /**
     * Obtém o valor da propriedade cpSplc.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCpSplc() {
        return cpSplc;
    }

    /**
     * Define o valor da propriedade cpSplc.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCpSplc(String value) {
        this.cpSplc = value;
    }

    /**
     * Obtém o valor da propriedade equipmentInitial.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEquipmentInitial() {
        return equipmentInitial;
    }

    /**
     * Define o valor da propriedade equipmentInitial.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEquipmentInitial(String value) {
        this.equipmentInitial = value;
    }

    /**
     * Obtém o valor da propriedade equipmentNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEquipmentNumber() {
        return equipmentNumber;
    }

    /**
     * Define o valor da propriedade equipmentNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEquipmentNumber(String value) {
        this.equipmentNumber = value;
    }

    /**
     * Obtém o valor da propriedade equipmentInitialNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEquipmentInitialNumber() {
        return equipmentInitialNumber;
    }

    /**
     * Define o valor da propriedade equipmentInitialNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEquipmentInitialNumber(String value) {
        this.equipmentInitialNumber = value;
    }

    /**
     * Obtém o valor da propriedade equipmentGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getEquipmentGid() {
        return equipmentGid;
    }

    /**
     * Define o valor da propriedade equipmentGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setEquipmentGid(GLogXMLGidType value) {
        this.equipmentGid = value;
    }

    /**
     * Obtém o valor da propriedade equipmentTypeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getEquipmentTypeGid() {
        return equipmentTypeGid;
    }

    /**
     * Define o valor da propriedade equipmentTypeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setEquipmentTypeGid(GLogXMLGidType value) {
        this.equipmentTypeGid = value;
    }

    /**
     * Obtém o valor da propriedade equipmentRefnumQualifierGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getEquipmentRefnumQualifierGid() {
        return equipmentRefnumQualifierGid;
    }

    /**
     * Define o valor da propriedade equipmentRefnumQualifierGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setEquipmentRefnumQualifierGid(GLogXMLGidType value) {
        this.equipmentRefnumQualifierGid = value;
    }

    /**
     * Obtém o valor da propriedade equipmentRefnumValue.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEquipmentRefnumValue() {
        return equipmentRefnumValue;
    }

    /**
     * Define o valor da propriedade equipmentRefnumValue.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEquipmentRefnumValue(String value) {
        this.equipmentRefnumValue = value;
    }

    /**
     * Obtém o valor da propriedade aarCarType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAARCarType() {
        return aarCarType;
    }

    /**
     * Define o valor da propriedade aarCarType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAARCarType(String value) {
        this.aarCarType = value;
    }

    /**
     * Obtém o valor da propriedade chassisInitial.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChassisInitial() {
        return chassisInitial;
    }

    /**
     * Define o valor da propriedade chassisInitial.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChassisInitial(String value) {
        this.chassisInitial = value;
    }

    /**
     * Obtém o valor da propriedade chassisNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChassisNumber() {
        return chassisNumber;
    }

    /**
     * Define o valor da propriedade chassisNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChassisNumber(String value) {
        this.chassisNumber = value;
    }

    /**
     * Obtém o valor da propriedade chassisInitialNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChassisInitialNumber() {
        return chassisInitialNumber;
    }

    /**
     * Define o valor da propriedade chassisInitialNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChassisInitialNumber(String value) {
        this.chassisInitialNumber = value;
    }

    /**
     * Obtém o valor da propriedade startEventStatusCodeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getStartEventStatusCodeGid() {
        return startEventStatusCodeGid;
    }

    /**
     * Define o valor da propriedade startEventStatusCodeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setStartEventStatusCodeGid(GLogXMLGidType value) {
        this.startEventStatusCodeGid = value;
    }

    /**
     * Obtém o valor da propriedade startLocationGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getStartLocationGid() {
        return startLocationGid;
    }

    /**
     * Define o valor da propriedade startLocationGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setStartLocationGid(GLogXMLGidType value) {
        this.startLocationGid = value;
    }

    /**
     * Obtém o valor da propriedade startScacGid.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStartScacGid() {
        return startScacGid;
    }

    /**
     * Define o valor da propriedade startScacGid.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStartScacGid(String value) {
        this.startScacGid = value;
    }

    /**
     * Obtém o valor da propriedade startTimeZoneGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getStartTimeZoneGid() {
        return startTimeZoneGid;
    }

    /**
     * Define o valor da propriedade startTimeZoneGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setStartTimeZoneGid(GLogXMLGidType value) {
        this.startTimeZoneGid = value;
    }

    /**
     * Obtém o valor da propriedade startCity.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStartCity() {
        return startCity;
    }

    /**
     * Define o valor da propriedade startCity.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStartCity(String value) {
        this.startCity = value;
    }

    /**
     * Obtém o valor da propriedade startState.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStartState() {
        return startState;
    }

    /**
     * Define o valor da propriedade startState.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStartState(String value) {
        this.startState = value;
    }

    /**
     * Obtém o valor da propriedade startCountry.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStartCountry() {
        return startCountry;
    }

    /**
     * Define o valor da propriedade startCountry.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStartCountry(String value) {
        this.startCountry = value;
    }

    /**
     * Obtém o valor da propriedade startSplc.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStartSplc() {
        return startSplc;
    }

    /**
     * Define o valor da propriedade startSplc.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStartSplc(String value) {
        this.startSplc = value;
    }

    /**
     * Obtém o valor da propriedade startIsCarLoaded.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStartIsCarLoaded() {
        return startIsCarLoaded;
    }

    /**
     * Define o valor da propriedade startIsCarLoaded.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStartIsCarLoaded(String value) {
        this.startIsCarLoaded = value;
    }

    /**
     * Obtém o valor da propriedade endEventStatusCodeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getEndEventStatusCodeGid() {
        return endEventStatusCodeGid;
    }

    /**
     * Define o valor da propriedade endEventStatusCodeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setEndEventStatusCodeGid(GLogXMLGidType value) {
        this.endEventStatusCodeGid = value;
    }

    /**
     * Obtém o valor da propriedade endLocationGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getEndLocationGid() {
        return endLocationGid;
    }

    /**
     * Define o valor da propriedade endLocationGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setEndLocationGid(GLogXMLGidType value) {
        this.endLocationGid = value;
    }

    /**
     * Obtém o valor da propriedade endScacGid.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEndScacGid() {
        return endScacGid;
    }

    /**
     * Define o valor da propriedade endScacGid.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEndScacGid(String value) {
        this.endScacGid = value;
    }

    /**
     * Obtém o valor da propriedade endTimeZoneGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getEndTimeZoneGid() {
        return endTimeZoneGid;
    }

    /**
     * Define o valor da propriedade endTimeZoneGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setEndTimeZoneGid(GLogXMLGidType value) {
        this.endTimeZoneGid = value;
    }

    /**
     * Obtém o valor da propriedade endCity.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEndCity() {
        return endCity;
    }

    /**
     * Define o valor da propriedade endCity.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEndCity(String value) {
        this.endCity = value;
    }

    /**
     * Obtém o valor da propriedade endState.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEndState() {
        return endState;
    }

    /**
     * Define o valor da propriedade endState.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEndState(String value) {
        this.endState = value;
    }

    /**
     * Obtém o valor da propriedade endCountry.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEndCountry() {
        return endCountry;
    }

    /**
     * Define o valor da propriedade endCountry.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEndCountry(String value) {
        this.endCountry = value;
    }

    /**
     * Obtém o valor da propriedade endSplc.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEndSplc() {
        return endSplc;
    }

    /**
     * Define o valor da propriedade endSplc.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEndSplc(String value) {
        this.endSplc = value;
    }

    /**
     * Obtém o valor da propriedade endIsCarLoaded.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEndIsCarLoaded() {
        return endIsCarLoaded;
    }

    /**
     * Define o valor da propriedade endIsCarLoaded.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEndIsCarLoaded(String value) {
        this.endIsCarLoaded = value;
    }

    /**
     * Obtém o valor da propriedade inShipmentGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getInShipmentGid() {
        return inShipmentGid;
    }

    /**
     * Define o valor da propriedade inShipmentGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setInShipmentGid(GLogXMLGidType value) {
        this.inShipmentGid = value;
    }

    /**
     * Obtém o valor da propriedade inSEquipmentGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getInSEquipmentGid() {
        return inSEquipmentGid;
    }

    /**
     * Define o valor da propriedade inSEquipmentGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setInSEquipmentGid(GLogXMLGidType value) {
        this.inSEquipmentGid = value;
    }

    /**
     * Obtém o valor da propriedade inSTCCGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getInSTCCGid() {
        return inSTCCGid;
    }

    /**
     * Define o valor da propriedade inSTCCGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setInSTCCGid(GLogXMLGidType value) {
        this.inSTCCGid = value;
    }

    /**
     * Obtém o valor da propriedade inUserDefinedCommodityGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getInUserDefinedCommodityGid() {
        return inUserDefinedCommodityGid;
    }

    /**
     * Define o valor da propriedade inUserDefinedCommodityGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setInUserDefinedCommodityGid(GLogXMLGidType value) {
        this.inUserDefinedCommodityGid = value;
    }

    /**
     * Obtém o valor da propriedade inSourceLocationGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getInSourceLocationGid() {
        return inSourceLocationGid;
    }

    /**
     * Define o valor da propriedade inSourceLocationGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setInSourceLocationGid(GLogXMLGidType value) {
        this.inSourceLocationGid = value;
    }

    /**
     * Obtém o valor da propriedade inDestLocationGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getInDestLocationGid() {
        return inDestLocationGid;
    }

    /**
     * Define o valor da propriedade inDestLocationGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setInDestLocationGid(GLogXMLGidType value) {
        this.inDestLocationGid = value;
    }

    /**
     * Obtém o valor da propriedade inShipUnitCount.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInShipUnitCount() {
        return inShipUnitCount;
    }

    /**
     * Define o valor da propriedade inShipUnitCount.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInShipUnitCount(String value) {
        this.inShipUnitCount = value;
    }

    /**
     * Obtém o valor da propriedade inWeight.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInWeight() {
        return inWeight;
    }

    /**
     * Define o valor da propriedade inWeight.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInWeight(String value) {
        this.inWeight = value;
    }

    /**
     * Obtém o valor da propriedade inWeightUomCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInWeightUomCode() {
        return inWeightUomCode;
    }

    /**
     * Define o valor da propriedade inWeightUomCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInWeightUomCode(String value) {
        this.inWeightUomCode = value;
    }

    /**
     * Obtém o valor da propriedade inWeightBase.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInWeightBase() {
        return inWeightBase;
    }

    /**
     * Define o valor da propriedade inWeightBase.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInWeightBase(String value) {
        this.inWeightBase = value;
    }

    /**
     * Obtém o valor da propriedade inPackagedItemGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getInPackagedItemGid() {
        return inPackagedItemGid;
    }

    /**
     * Define o valor da propriedade inPackagedItemGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setInPackagedItemGid(GLogXMLGidType value) {
        this.inPackagedItemGid = value;
    }

    /**
     * Obtém o valor da propriedade inItemGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getInItemGid() {
        return inItemGid;
    }

    /**
     * Define o valor da propriedade inItemGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setInItemGid(GLogXMLGidType value) {
        this.inItemGid = value;
    }

    /**
     * Obtém o valor da propriedade inTHUGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getInTHUGid() {
        return inTHUGid;
    }

    /**
     * Define o valor da propriedade inTHUGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setInTHUGid(GLogXMLGidType value) {
        this.inTHUGid = value;
    }

    /**
     * Obtém o valor da propriedade outShipmentGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getOutShipmentGid() {
        return outShipmentGid;
    }

    /**
     * Define o valor da propriedade outShipmentGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setOutShipmentGid(GLogXMLGidType value) {
        this.outShipmentGid = value;
    }

    /**
     * Obtém o valor da propriedade outSEquipmentGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getOutSEquipmentGid() {
        return outSEquipmentGid;
    }

    /**
     * Define o valor da propriedade outSEquipmentGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setOutSEquipmentGid(GLogXMLGidType value) {
        this.outSEquipmentGid = value;
    }

    /**
     * Obtém o valor da propriedade outSTCCGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getOutSTCCGid() {
        return outSTCCGid;
    }

    /**
     * Define o valor da propriedade outSTCCGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setOutSTCCGid(GLogXMLGidType value) {
        this.outSTCCGid = value;
    }

    /**
     * Obtém o valor da propriedade outUserDefinedCommodityGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getOutUserDefinedCommodityGid() {
        return outUserDefinedCommodityGid;
    }

    /**
     * Define o valor da propriedade outUserDefinedCommodityGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setOutUserDefinedCommodityGid(GLogXMLGidType value) {
        this.outUserDefinedCommodityGid = value;
    }

    /**
     * Obtém o valor da propriedade outSourceLocationGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getOutSourceLocationGid() {
        return outSourceLocationGid;
    }

    /**
     * Define o valor da propriedade outSourceLocationGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setOutSourceLocationGid(GLogXMLGidType value) {
        this.outSourceLocationGid = value;
    }

    /**
     * Obtém o valor da propriedade outDestLocationGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getOutDestLocationGid() {
        return outDestLocationGid;
    }

    /**
     * Define o valor da propriedade outDestLocationGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setOutDestLocationGid(GLogXMLGidType value) {
        this.outDestLocationGid = value;
    }

    /**
     * Obtém o valor da propriedade outShipUnitCount.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOutShipUnitCount() {
        return outShipUnitCount;
    }

    /**
     * Define o valor da propriedade outShipUnitCount.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOutShipUnitCount(String value) {
        this.outShipUnitCount = value;
    }

    /**
     * Obtém o valor da propriedade outWeight.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOutWeight() {
        return outWeight;
    }

    /**
     * Define o valor da propriedade outWeight.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOutWeight(String value) {
        this.outWeight = value;
    }

    /**
     * Obtém o valor da propriedade outWeightUomCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOutWeightUomCode() {
        return outWeightUomCode;
    }

    /**
     * Define o valor da propriedade outWeightUomCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOutWeightUomCode(String value) {
        this.outWeightUomCode = value;
    }

    /**
     * Obtém o valor da propriedade outWeightBase.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOutWeightBase() {
        return outWeightBase;
    }

    /**
     * Define o valor da propriedade outWeightBase.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOutWeightBase(String value) {
        this.outWeightBase = value;
    }

    /**
     * Obtém o valor da propriedade outPackagedItemGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getOutPackagedItemGid() {
        return outPackagedItemGid;
    }

    /**
     * Define o valor da propriedade outPackagedItemGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setOutPackagedItemGid(GLogXMLGidType value) {
        this.outPackagedItemGid = value;
    }

    /**
     * Obtém o valor da propriedade outItemGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getOutItemGid() {
        return outItemGid;
    }

    /**
     * Define o valor da propriedade outItemGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setOutItemGid(GLogXMLGidType value) {
        this.outItemGid = value;
    }

    /**
     * Obtém o valor da propriedade outTHUGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getOutTHUGid() {
        return outTHUGid;
    }

    /**
     * Define o valor da propriedade outTHUGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setOutTHUGid(GLogXMLGidType value) {
        this.outTHUGid = value;
    }

    /**
     * Obtém o valor da propriedade dmEventDate1.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getDmEventDate1() {
        return dmEventDate1;
    }

    /**
     * Define o valor da propriedade dmEventDate1.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setDmEventDate1(GLogDateTimeType value) {
        this.dmEventDate1 = value;
    }

    /**
     * Obtém o valor da propriedade dmEventDate2.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getDmEventDate2() {
        return dmEventDate2;
    }

    /**
     * Define o valor da propriedade dmEventDate2.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setDmEventDate2(GLogDateTimeType value) {
        this.dmEventDate2 = value;
    }

    /**
     * Obtém o valor da propriedade dmEventDate3.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getDmEventDate3() {
        return dmEventDate3;
    }

    /**
     * Define o valor da propriedade dmEventDate3.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setDmEventDate3(GLogDateTimeType value) {
        this.dmEventDate3 = value;
    }

    /**
     * Obtém o valor da propriedade dmEventDate4.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getDmEventDate4() {
        return dmEventDate4;
    }

    /**
     * Define o valor da propriedade dmEventDate4.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setDmEventDate4(GLogDateTimeType value) {
        this.dmEventDate4 = value;
    }

    /**
     * Obtém o valor da propriedade dmEventDate5.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getDmEventDate5() {
        return dmEventDate5;
    }

    /**
     * Define o valor da propriedade dmEventDate5.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setDmEventDate5(GLogDateTimeType value) {
        this.dmEventDate5 = value;
    }

    /**
     * Obtém o valor da propriedade dmEventDate6.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getDmEventDate6() {
        return dmEventDate6;
    }

    /**
     * Define o valor da propriedade dmEventDate6.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setDmEventDate6(GLogDateTimeType value) {
        this.dmEventDate6 = value;
    }

    /**
     * Obtém o valor da propriedade dmEventDate7.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getDmEventDate7() {
        return dmEventDate7;
    }

    /**
     * Define o valor da propriedade dmEventDate7.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setDmEventDate7(GLogDateTimeType value) {
        this.dmEventDate7 = value;
    }

    /**
     * Obtém o valor da propriedade dmEventDate8.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getDmEventDate8() {
        return dmEventDate8;
    }

    /**
     * Define o valor da propriedade dmEventDate8.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setDmEventDate8(GLogDateTimeType value) {
        this.dmEventDate8 = value;
    }

    /**
     * Obtém o valor da propriedade dmEventDate9.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getDmEventDate9() {
        return dmEventDate9;
    }

    /**
     * Define o valor da propriedade dmEventDate9.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setDmEventDate9(GLogDateTimeType value) {
        this.dmEventDate9 = value;
    }

    /**
     * Obtém o valor da propriedade dmEventDate10.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getDmEventDate10() {
        return dmEventDate10;
    }

    /**
     * Define o valor da propriedade dmEventDate10.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setDmEventDate10(GLogDateTimeType value) {
        this.dmEventDate10 = value;
    }

    /**
     * Obtém o valor da propriedade bsStatusCode1Gid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getBsStatusCode1Gid() {
        return bsStatusCode1Gid;
    }

    /**
     * Define o valor da propriedade bsStatusCode1Gid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setBsStatusCode1Gid(GLogXMLGidType value) {
        this.bsStatusCode1Gid = value;
    }

    /**
     * Obtém o valor da propriedade bsStatusCode2Gid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getBsStatusCode2Gid() {
        return bsStatusCode2Gid;
    }

    /**
     * Define o valor da propriedade bsStatusCode2Gid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setBsStatusCode2Gid(GLogXMLGidType value) {
        this.bsStatusCode2Gid = value;
    }

    /**
     * Obtém o valor da propriedade bsStatusCode3Gid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getBsStatusCode3Gid() {
        return bsStatusCode3Gid;
    }

    /**
     * Define o valor da propriedade bsStatusCode3Gid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setBsStatusCode3Gid(GLogXMLGidType value) {
        this.bsStatusCode3Gid = value;
    }

    /**
     * Obtém o valor da propriedade bsStatusCode4Gid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getBsStatusCode4Gid() {
        return bsStatusCode4Gid;
    }

    /**
     * Define o valor da propriedade bsStatusCode4Gid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setBsStatusCode4Gid(GLogXMLGidType value) {
        this.bsStatusCode4Gid = value;
    }

    /**
     * Obtém o valor da propriedade bsStatusCode5Gid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getBsStatusCode5Gid() {
        return bsStatusCode5Gid;
    }

    /**
     * Define o valor da propriedade bsStatusCode5Gid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setBsStatusCode5Gid(GLogXMLGidType value) {
        this.bsStatusCode5Gid = value;
    }

    /**
     * Obtém o valor da propriedade bsStatusCode6Gid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getBsStatusCode6Gid() {
        return bsStatusCode6Gid;
    }

    /**
     * Define o valor da propriedade bsStatusCode6Gid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setBsStatusCode6Gid(GLogXMLGidType value) {
        this.bsStatusCode6Gid = value;
    }

    /**
     * Obtém o valor da propriedade bsStatusCode7Gid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getBsStatusCode7Gid() {
        return bsStatusCode7Gid;
    }

    /**
     * Define o valor da propriedade bsStatusCode7Gid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setBsStatusCode7Gid(GLogXMLGidType value) {
        this.bsStatusCode7Gid = value;
    }

    /**
     * Obtém o valor da propriedade bsStatusCode8Gid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getBsStatusCode8Gid() {
        return bsStatusCode8Gid;
    }

    /**
     * Define o valor da propriedade bsStatusCode8Gid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setBsStatusCode8Gid(GLogXMLGidType value) {
        this.bsStatusCode8Gid = value;
    }

    /**
     * Obtém o valor da propriedade bsStatusCode9Gid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getBsStatusCode9Gid() {
        return bsStatusCode9Gid;
    }

    /**
     * Define o valor da propriedade bsStatusCode9Gid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setBsStatusCode9Gid(GLogXMLGidType value) {
        this.bsStatusCode9Gid = value;
    }

    /**
     * Obtém o valor da propriedade bsStatusCode10Gid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getBsStatusCode10Gid() {
        return bsStatusCode10Gid;
    }

    /**
     * Define o valor da propriedade bsStatusCode10Gid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setBsStatusCode10Gid(GLogXMLGidType value) {
        this.bsStatusCode10Gid = value;
    }

    /**
     * Obtém o valor da propriedade isSystemGenerated.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsSystemGenerated() {
        return isSystemGenerated;
    }

    /**
     * Define o valor da propriedade isSystemGenerated.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsSystemGenerated(String value) {
        this.isSystemGenerated = value;
    }

    /**
     * Obtém o valor da propriedade markedForPurge.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMarkedForPurge() {
        return markedForPurge;
    }

    /**
     * Define o valor da propriedade markedForPurge.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMarkedForPurge(String value) {
        this.markedForPurge = value;
    }

    /**
     * Obtém o valor da propriedade activityTypeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getActivityTypeGid() {
        return activityTypeGid;
    }

    /**
     * Define o valor da propriedade activityTypeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setActivityTypeGid(GLogXMLGidType value) {
        this.activityTypeGid = value;
    }

    /**
     * Obtém o valor da propriedade modeType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getModeType() {
        return modeType;
    }

    /**
     * Define o valor da propriedade modeType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setModeType(String value) {
        this.modeType = value;
    }

    /**
     * Obtém o valor da propriedade endDate.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getEndDate() {
        return endDate;
    }

    /**
     * Define o valor da propriedade endDate.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setEndDate(GLogDateTimeType value) {
        this.endDate = value;
    }

    /**
     * Gets the value of the demurrageTransactionEvent property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the demurrageTransactionEvent property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDemurrageTransactionEvent().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DemurrageTransactionEventType }
     * 
     * 
     */
    public List<DemurrageTransactionEventType> getDemurrageTransactionEvent() {
        if (demurrageTransactionEvent == null) {
            demurrageTransactionEvent = new ArrayList<DemurrageTransactionEventType>();
        }
        return this.demurrageTransactionEvent;
    }

    /**
     * Gets the value of the demurrageTransactionInvolvedParty property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the demurrageTransactionInvolvedParty property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDemurrageTransactionInvolvedParty().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DemurrageInvolvedPartyType }
     * 
     * 
     */
    public List<DemurrageInvolvedPartyType> getDemurrageTransactionInvolvedParty() {
        if (demurrageTransactionInvolvedParty == null) {
            demurrageTransactionInvolvedParty = new ArrayList<DemurrageInvolvedPartyType>();
        }
        return this.demurrageTransactionInvolvedParty;
    }

    /**
     * Gets the value of the demurrageTransactionLineitem property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the demurrageTransactionLineitem property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDemurrageTransactionLineitem().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DemurrageTransactionLineitemType }
     * 
     * 
     */
    public List<DemurrageTransactionLineitemType> getDemurrageTransactionLineitem() {
        if (demurrageTransactionLineitem == null) {
            demurrageTransactionLineitem = new ArrayList<DemurrageTransactionLineitemType>();
        }
        return this.demurrageTransactionLineitem;
    }

    /**
     * Gets the value of the demurrageTransactionNote property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the demurrageTransactionNote property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDemurrageTransactionNote().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DemurrageTransactionNoteType }
     * 
     * 
     */
    public List<DemurrageTransactionNoteType> getDemurrageTransactionNote() {
        if (demurrageTransactionNote == null) {
            demurrageTransactionNote = new ArrayList<DemurrageTransactionNoteType>();
        }
        return this.demurrageTransactionNote;
    }

    /**
     * Gets the value of the demurrageTransactionRefnum property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the demurrageTransactionRefnum property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDemurrageTransactionRefnum().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DemurrageTransactionRefnumType }
     * 
     * 
     */
    public List<DemurrageTransactionRefnumType> getDemurrageTransactionRefnum() {
        if (demurrageTransactionRefnum == null) {
            demurrageTransactionRefnum = new ArrayList<DemurrageTransactionRefnumType>();
        }
        return this.demurrageTransactionRefnum;
    }

    /**
     * Gets the value of the demurrageTransactionRemark property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the demurrageTransactionRemark property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDemurrageTransactionRemark().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DemurrageTransactionRemarkType }
     * 
     * 
     */
    public List<DemurrageTransactionRemarkType> getDemurrageTransactionRemark() {
        if (demurrageTransactionRemark == null) {
            demurrageTransactionRemark = new ArrayList<DemurrageTransactionRemarkType>();
        }
        return this.demurrageTransactionRemark;
    }

    /**
     * Gets the value of the demurrageTransactionStatus property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the demurrageTransactionStatus property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDemurrageTransactionStatus().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link StatusType }
     * 
     * 
     */
    public List<StatusType> getDemurrageTransactionStatus() {
        if (demurrageTransactionStatus == null) {
            demurrageTransactionStatus = new ArrayList<StatusType>();
        }
        return this.demurrageTransactionStatus;
    }

}
