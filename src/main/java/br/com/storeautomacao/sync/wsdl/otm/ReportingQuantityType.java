
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * (Both) Reporting Quantity represents the quantity in external unit of measure as needed by different regimes.
 *          
 * 
 * <p>Classe Java de ReportingQuantityType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="ReportingQuantityType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SequenceNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="GtmProdClassTypeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="QuantityTypeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="QuantityValue" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AttrName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="UOM" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReportingQuantityType", namespace = "http://xmlns.oracle.com/apps/gtm/transmission/v6.4", propOrder = {
    "sequenceNumber",
    "gtmProdClassTypeGid",
    "quantityTypeGid",
    "quantityValue",
    "attrName",
    "uom"
})
public class ReportingQuantityType {

    @XmlElement(name = "SequenceNumber", required = true)
    protected String sequenceNumber;
    @XmlElement(name = "GtmProdClassTypeGid", required = true)
    protected GLogXMLGidType gtmProdClassTypeGid;
    @XmlElement(name = "QuantityTypeGid", required = true)
    protected GLogXMLGidType quantityTypeGid;
    @XmlElement(name = "QuantityValue", required = true)
    protected String quantityValue;
    @XmlElement(name = "AttrName", required = true)
    protected String attrName;
    @XmlElement(name = "UOM", required = true)
    protected String uom;

    /**
     * Obtém o valor da propriedade sequenceNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSequenceNumber() {
        return sequenceNumber;
    }

    /**
     * Define o valor da propriedade sequenceNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSequenceNumber(String value) {
        this.sequenceNumber = value;
    }

    /**
     * Obtém o valor da propriedade gtmProdClassTypeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getGtmProdClassTypeGid() {
        return gtmProdClassTypeGid;
    }

    /**
     * Define o valor da propriedade gtmProdClassTypeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setGtmProdClassTypeGid(GLogXMLGidType value) {
        this.gtmProdClassTypeGid = value;
    }

    /**
     * Obtém o valor da propriedade quantityTypeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getQuantityTypeGid() {
        return quantityTypeGid;
    }

    /**
     * Define o valor da propriedade quantityTypeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setQuantityTypeGid(GLogXMLGidType value) {
        this.quantityTypeGid = value;
    }

    /**
     * Obtém o valor da propriedade quantityValue.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQuantityValue() {
        return quantityValue;
    }

    /**
     * Define o valor da propriedade quantityValue.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQuantityValue(String value) {
        this.quantityValue = value;
    }

    /**
     * Obtém o valor da propriedade attrName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAttrName() {
        return attrName;
    }

    /**
     * Define o valor da propriedade attrName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAttrName(String value) {
        this.attrName = value;
    }

    /**
     * Obtém o valor da propriedade uom.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUOM() {
        return uom;
    }

    /**
     * Define o valor da propriedade uom.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUOM(String value) {
        this.uom = value;
    }

}
