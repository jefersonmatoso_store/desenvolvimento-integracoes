
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Classe Java de ACCESSORIAL_COST_TYPE complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="ACCESSORIAL_COST_TYPE">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ACCESSORIAL_COST_ROW" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="ACCESSORIAL_COST_GID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="ACCESSORIAL_COST_XID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="DESCRIPTION" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="LEFT_OPERAND1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="OPER1_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="LOW_VALUE1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="HIGH_VALUE1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="AND_OR1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="LEFT_OPERAND2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="OPER2_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="LOW_VALUE2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="HIGH_VALUE2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="AND_OR2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="LEFT_OPERAND3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="OPER3_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="LOW_VALUE3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="HIGH_VALUE3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="AND_OR3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="LEFT_OPERAND4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="OPER4_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="LOW_VALUE4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="HIGH_VALUE4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="CHARGE_MULTIPLIER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="CHARGE_AMOUNT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="CHARGE_AMOUNT_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="CHARGE_AMOUNT_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="CHARGE_UNIT_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="CHARGE_UNIT_COUNT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="CHARGE_MULTIPLIER_SCALAR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="CHARGE_BREAK_COMPARATOR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="CHARGE_ACTION" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="CHARGE_TYPE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="CHARGE_MULTIPLIER2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="CHARGE_UNIT_UOM_CODE2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="CHARGE_UNIT_COUNT2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="USE_DEFAULTS" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="CHARGE_SEQUENCE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="CHARGE_GROUP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="CHARGE_UNIT_BREAK_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="CHARGE_MULTIPLIER_OPTION" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="RATE_GEO_COST_OPERAND_SEQ" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="DIM_RATE_FACTOR_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="USES_UNIT_BREAKS" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="EFFECTIVE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="EXPIRATION_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="CALENDAR_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="NOTES" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ROUNDING_TYPE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ROUNDING_INTERVAL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ROUNDING_FIELDS_LEVEL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ROUNDING_APPLICATION" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="MIN_COST" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="MIN_COST_CURRENCY_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="MIN_COST_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="MAX_COST" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="MAX_COST_CURRENCY_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="MAX_COST_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="PAYMENT_METHOD_CODE_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="IS_FILED_AS_TARIFF" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="COST_TYPE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="CALENDAR_ACTIVITY_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="RATE_UNIT_BREAK_PROFILE_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="RATE_UNIT_BREAK_PROFILE2_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="CHARGE_BREAK_COMPARATOR2_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="EXTERNAL_RATING_ENGINE_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="EXT_RE_FIELDSET_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="COST_CATEGORY_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="COST_CODE_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ACCESSORIAL_COST_UNIT_BREAK" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="ACCESSORIAL_COST_UNIT_BREAK_ROW" maxOccurs="unbounded" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="ACCESSORIAL_COST_GID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="RATE_UNIT_BREAK_GID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="CHARGE_AMOUNT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                       &lt;element name="CHARGE_AMOUNT_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                       &lt;element name="CHARGE_AMOUNT_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                       &lt;element name="CHARGE_DISCOUNT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                       &lt;element name="MIN_COST" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                       &lt;element name="MIN_COST_CURRENCY_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                       &lt;element name="MIN_COST_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                       &lt;element name="MAX_COST" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                       &lt;element name="MAX_COST_CURRENCY_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                       &lt;element name="MAX_COST_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                       &lt;element name="RATE_UNIT_BREAK2_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                       &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                       &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                     &lt;/sequence>
 *                                     &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="ATTRIBUTE1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ATTRIBUTE2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ATTRIBUTE3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ATTRIBUTE4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ATTRIBUTE5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ATTRIBUTE6" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ATTRIBUTE7" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ATTRIBUTE8" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ATTRIBUTE9" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ATTRIBUTE10" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ATTRIBUTE11" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ATTRIBUTE12" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ATTRIBUTE13" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ATTRIBUTE14" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ATTRIBUTE15" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ATTRIBUTE16" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ATTRIBUTE17" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ATTRIBUTE18" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ATTRIBUTE19" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ATTRIBUTE20" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ATTRIBUTE_NUMBER1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ATTRIBUTE_NUMBER2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ATTRIBUTE_NUMBER3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ATTRIBUTE_NUMBER4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ATTRIBUTE_NUMBER5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ATTRIBUTE_NUMBER6" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ATTRIBUTE_NUMBER7" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ATTRIBUTE_NUMBER8" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ATTRIBUTE_NUMBER9" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ATTRIBUTE_NUMBER10" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ATTRIBUTE_DATE1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ATTRIBUTE_DATE2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ATTRIBUTE_DATE3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ATTRIBUTE_DATE4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ATTRIBUTE_DATE5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ATTRIBUTE_DATE6" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ATTRIBUTE_DATE7" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ATTRIBUTE_DATE8" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ATTRIBUTE_DATE9" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ATTRIBUTE_DATE10" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                 &lt;/sequence>
 *                 &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" />
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ACCESSORIAL_COST_TYPE", propOrder = {
    "accessorialcostrow"
})
public class ACCESSORIALCOSTTYPE {

    @XmlElement(name = "ACCESSORIAL_COST_ROW")
    protected List<ACCESSORIALCOSTROW> accessorialcostrow;

    /**
     * Gets the value of the accessorialcostrow property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the accessorialcostrow property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getACCESSORIALCOSTROW().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ACCESSORIALCOSTROW }
     * 
     * 
     */
    public List<ACCESSORIALCOSTROW> getACCESSORIALCOSTROW() {
        if (accessorialcostrow == null) {
            accessorialcostrow = new ArrayList<ACCESSORIALCOSTROW>();
        }
        return this.accessorialcostrow;
    }


    /**
     * <p>Classe Java de anonymous complex type.
     * 
     * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="ACCESSORIAL_COST_GID" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="ACCESSORIAL_COST_XID" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="DESCRIPTION" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="LEFT_OPERAND1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="OPER1_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="LOW_VALUE1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="HIGH_VALUE1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="AND_OR1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="LEFT_OPERAND2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="OPER2_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="LOW_VALUE2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="HIGH_VALUE2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="AND_OR2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="LEFT_OPERAND3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="OPER3_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="LOW_VALUE3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="HIGH_VALUE3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="AND_OR3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="LEFT_OPERAND4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="OPER4_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="LOW_VALUE4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="HIGH_VALUE4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="CHARGE_MULTIPLIER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="CHARGE_AMOUNT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="CHARGE_AMOUNT_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="CHARGE_AMOUNT_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="CHARGE_UNIT_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="CHARGE_UNIT_COUNT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="CHARGE_MULTIPLIER_SCALAR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="CHARGE_BREAK_COMPARATOR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="CHARGE_ACTION" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="CHARGE_TYPE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="CHARGE_MULTIPLIER2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="CHARGE_UNIT_UOM_CODE2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="CHARGE_UNIT_COUNT2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="USE_DEFAULTS" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="CHARGE_SEQUENCE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="CHARGE_GROUP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="CHARGE_UNIT_BREAK_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="CHARGE_MULTIPLIER_OPTION" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="RATE_GEO_COST_OPERAND_SEQ" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="DIM_RATE_FACTOR_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="USES_UNIT_BREAKS" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="EFFECTIVE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="EXPIRATION_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="CALENDAR_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="NOTES" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ROUNDING_TYPE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ROUNDING_INTERVAL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ROUNDING_FIELDS_LEVEL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ROUNDING_APPLICATION" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="MIN_COST" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="MIN_COST_CURRENCY_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="MIN_COST_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="MAX_COST" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="MAX_COST_CURRENCY_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="MAX_COST_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="PAYMENT_METHOD_CODE_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="IS_FILED_AS_TARIFF" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="COST_TYPE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="CALENDAR_ACTIVITY_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="RATE_UNIT_BREAK_PROFILE_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="RATE_UNIT_BREAK_PROFILE2_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="CHARGE_BREAK_COMPARATOR2_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="EXTERNAL_RATING_ENGINE_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="EXT_RE_FIELDSET_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="COST_CATEGORY_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="COST_CODE_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ACCESSORIAL_COST_UNIT_BREAK" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="ACCESSORIAL_COST_UNIT_BREAK_ROW" maxOccurs="unbounded" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="ACCESSORIAL_COST_GID" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="RATE_UNIT_BREAK_GID" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="CHARGE_AMOUNT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                             &lt;element name="CHARGE_AMOUNT_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                             &lt;element name="CHARGE_AMOUNT_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                             &lt;element name="CHARGE_DISCOUNT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                             &lt;element name="MIN_COST" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                             &lt;element name="MIN_COST_CURRENCY_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                             &lt;element name="MIN_COST_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                             &lt;element name="MAX_COST" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                             &lt;element name="MAX_COST_CURRENCY_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                             &lt;element name="MAX_COST_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                             &lt;element name="RATE_UNIT_BREAK2_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                             &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                             &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                           &lt;/sequence>
     *                           &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="ATTRIBUTE1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE6" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE7" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE8" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE9" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE10" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE11" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE12" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE13" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE14" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE15" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE16" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE17" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE18" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE19" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE20" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE_NUMBER1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE_NUMBER2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE_NUMBER3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE_NUMBER4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE_NUMBER5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE_NUMBER6" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE_NUMBER7" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE_NUMBER8" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE_NUMBER9" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE_NUMBER10" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE_DATE1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE_DATE2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE_DATE3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE_DATE4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE_DATE5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE_DATE6" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE_DATE7" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE_DATE8" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE_DATE9" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE_DATE10" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *       &lt;/sequence>
     *       &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" />
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "accessorialcostgid",
        "accessorialcostxid",
        "description",
        "leftoperand1",
        "oper1GID",
        "lowvalue1",
        "highvalue1",
        "andor1",
        "leftoperand2",
        "oper2GID",
        "lowvalue2",
        "highvalue2",
        "andor2",
        "leftoperand3",
        "oper3GID",
        "lowvalue3",
        "highvalue3",
        "andor3",
        "leftoperand4",
        "oper4GID",
        "lowvalue4",
        "highvalue4",
        "chargemultiplier",
        "chargeamount",
        "chargeamountgid",
        "chargeamountbase",
        "chargeunituomcode",
        "chargeunitcount",
        "chargemultiplierscalar",
        "chargebreakcomparator",
        "chargeaction",
        "chargetype",
        "chargemultiplier2",
        "chargeunituomcode2",
        "chargeunitcount2",
        "usedefaults",
        "chargesequence",
        "chargegroup",
        "chargeunitbreakgid",
        "chargemultiplieroption",
        "rategeocostoperandseq",
        "dimratefactorgid",
        "usesunitbreaks",
        "effectivedate",
        "expirationdate",
        "calendargid",
        "notes",
        "roundingtype",
        "roundinginterval",
        "roundingfieldslevel",
        "roundingapplication",
        "mincost",
        "mincostcurrencygid",
        "mincostbase",
        "maxcost",
        "maxcostcurrencygid",
        "maxcostbase",
        "paymentmethodcodegid",
        "isfiledastariff",
        "costtype",
        "calendaractivitygid",
        "rateunitbreakprofilegid",
        "rateunitbreakprofile2GID",
        "chargebreakcomparator2GID",
        "externalratingenginegid",
        "extrefieldsetgid",
        "costcategorygid",
        "costcodegid",
        "domainname",
        "insertuser",
        "insertdate",
        "updateuser",
        "updatedate",
        "accessorialcostunitbreak",
        "attribute1",
        "attribute2",
        "attribute3",
        "attribute4",
        "attribute5",
        "attribute6",
        "attribute7",
        "attribute8",
        "attribute9",
        "attribute10",
        "attribute11",
        "attribute12",
        "attribute13",
        "attribute14",
        "attribute15",
        "attribute16",
        "attribute17",
        "attribute18",
        "attribute19",
        "attribute20",
        "attributenumber1",
        "attributenumber2",
        "attributenumber3",
        "attributenumber4",
        "attributenumber5",
        "attributenumber6",
        "attributenumber7",
        "attributenumber8",
        "attributenumber9",
        "attributenumber10",
        "attributedate1",
        "attributedate2",
        "attributedate3",
        "attributedate4",
        "attributedate5",
        "attributedate6",
        "attributedate7",
        "attributedate8",
        "attributedate9",
        "attributedate10"
    })
    public static class ACCESSORIALCOSTROW {

        @XmlElement(name = "ACCESSORIAL_COST_GID", required = true)
        protected String accessorialcostgid;
        @XmlElement(name = "ACCESSORIAL_COST_XID", required = true)
        protected String accessorialcostxid;
        @XmlElement(name = "DESCRIPTION")
        protected String description;
        @XmlElement(name = "LEFT_OPERAND1")
        protected String leftoperand1;
        @XmlElement(name = "OPER1_GID")
        protected String oper1GID;
        @XmlElement(name = "LOW_VALUE1")
        protected String lowvalue1;
        @XmlElement(name = "HIGH_VALUE1")
        protected String highvalue1;
        @XmlElement(name = "AND_OR1")
        protected String andor1;
        @XmlElement(name = "LEFT_OPERAND2")
        protected String leftoperand2;
        @XmlElement(name = "OPER2_GID")
        protected String oper2GID;
        @XmlElement(name = "LOW_VALUE2")
        protected String lowvalue2;
        @XmlElement(name = "HIGH_VALUE2")
        protected String highvalue2;
        @XmlElement(name = "AND_OR2")
        protected String andor2;
        @XmlElement(name = "LEFT_OPERAND3")
        protected String leftoperand3;
        @XmlElement(name = "OPER3_GID")
        protected String oper3GID;
        @XmlElement(name = "LOW_VALUE3")
        protected String lowvalue3;
        @XmlElement(name = "HIGH_VALUE3")
        protected String highvalue3;
        @XmlElement(name = "AND_OR3")
        protected String andor3;
        @XmlElement(name = "LEFT_OPERAND4")
        protected String leftoperand4;
        @XmlElement(name = "OPER4_GID")
        protected String oper4GID;
        @XmlElement(name = "LOW_VALUE4")
        protected String lowvalue4;
        @XmlElement(name = "HIGH_VALUE4")
        protected String highvalue4;
        @XmlElement(name = "CHARGE_MULTIPLIER")
        protected String chargemultiplier;
        @XmlElement(name = "CHARGE_AMOUNT")
        protected String chargeamount;
        @XmlElement(name = "CHARGE_AMOUNT_GID")
        protected String chargeamountgid;
        @XmlElement(name = "CHARGE_AMOUNT_BASE")
        protected String chargeamountbase;
        @XmlElement(name = "CHARGE_UNIT_UOM_CODE")
        protected String chargeunituomcode;
        @XmlElement(name = "CHARGE_UNIT_COUNT")
        protected String chargeunitcount;
        @XmlElement(name = "CHARGE_MULTIPLIER_SCALAR")
        protected String chargemultiplierscalar;
        @XmlElement(name = "CHARGE_BREAK_COMPARATOR")
        protected String chargebreakcomparator;
        @XmlElement(name = "CHARGE_ACTION")
        protected String chargeaction;
        @XmlElement(name = "CHARGE_TYPE")
        protected String chargetype;
        @XmlElement(name = "CHARGE_MULTIPLIER2")
        protected String chargemultiplier2;
        @XmlElement(name = "CHARGE_UNIT_UOM_CODE2")
        protected String chargeunituomcode2;
        @XmlElement(name = "CHARGE_UNIT_COUNT2")
        protected String chargeunitcount2;
        @XmlElement(name = "USE_DEFAULTS")
        protected String usedefaults;
        @XmlElement(name = "CHARGE_SEQUENCE")
        protected String chargesequence;
        @XmlElement(name = "CHARGE_GROUP")
        protected String chargegroup;
        @XmlElement(name = "CHARGE_UNIT_BREAK_GID")
        protected String chargeunitbreakgid;
        @XmlElement(name = "CHARGE_MULTIPLIER_OPTION")
        protected String chargemultiplieroption;
        @XmlElement(name = "RATE_GEO_COST_OPERAND_SEQ")
        protected String rategeocostoperandseq;
        @XmlElement(name = "DIM_RATE_FACTOR_GID")
        protected String dimratefactorgid;
        @XmlElement(name = "USES_UNIT_BREAKS")
        protected String usesunitbreaks;
        @XmlElement(name = "EFFECTIVE_DATE")
        protected String effectivedate;
        @XmlElement(name = "EXPIRATION_DATE")
        protected String expirationdate;
        @XmlElement(name = "CALENDAR_GID")
        protected String calendargid;
        @XmlElement(name = "NOTES")
        protected String notes;
        @XmlElement(name = "ROUNDING_TYPE")
        protected String roundingtype;
        @XmlElement(name = "ROUNDING_INTERVAL")
        protected String roundinginterval;
        @XmlElement(name = "ROUNDING_FIELDS_LEVEL")
        protected String roundingfieldslevel;
        @XmlElement(name = "ROUNDING_APPLICATION")
        protected String roundingapplication;
        @XmlElement(name = "MIN_COST")
        protected String mincost;
        @XmlElement(name = "MIN_COST_CURRENCY_GID")
        protected String mincostcurrencygid;
        @XmlElement(name = "MIN_COST_BASE")
        protected String mincostbase;
        @XmlElement(name = "MAX_COST")
        protected String maxcost;
        @XmlElement(name = "MAX_COST_CURRENCY_GID")
        protected String maxcostcurrencygid;
        @XmlElement(name = "MAX_COST_BASE")
        protected String maxcostbase;
        @XmlElement(name = "PAYMENT_METHOD_CODE_GID")
        protected String paymentmethodcodegid;
        @XmlElement(name = "IS_FILED_AS_TARIFF")
        protected String isfiledastariff;
        @XmlElement(name = "COST_TYPE")
        protected String costtype;
        @XmlElement(name = "CALENDAR_ACTIVITY_GID")
        protected String calendaractivitygid;
        @XmlElement(name = "RATE_UNIT_BREAK_PROFILE_GID")
        protected String rateunitbreakprofilegid;
        @XmlElement(name = "RATE_UNIT_BREAK_PROFILE2_GID")
        protected String rateunitbreakprofile2GID;
        @XmlElement(name = "CHARGE_BREAK_COMPARATOR2_GID")
        protected String chargebreakcomparator2GID;
        @XmlElement(name = "EXTERNAL_RATING_ENGINE_GID")
        protected String externalratingenginegid;
        @XmlElement(name = "EXT_RE_FIELDSET_GID")
        protected String extrefieldsetgid;
        @XmlElement(name = "COST_CATEGORY_GID")
        protected String costcategorygid;
        @XmlElement(name = "COST_CODE_GID")
        protected String costcodegid;
        @XmlElement(name = "DOMAIN_NAME", required = true)
        protected String domainname;
        @XmlElement(name = "INSERT_USER", required = true)
        protected String insertuser;
        @XmlElement(name = "INSERT_DATE", required = true)
        protected String insertdate;
        @XmlElement(name = "UPDATE_USER")
        protected String updateuser;
        @XmlElement(name = "UPDATE_DATE")
        protected String updatedate;
        @XmlElement(name = "ACCESSORIAL_COST_UNIT_BREAK")
        protected ACCESSORIALCOSTUNITBREAK accessorialcostunitbreak;
        @XmlElement(name = "ATTRIBUTE1")
        protected String attribute1;
        @XmlElement(name = "ATTRIBUTE2")
        protected String attribute2;
        @XmlElement(name = "ATTRIBUTE3")
        protected String attribute3;
        @XmlElement(name = "ATTRIBUTE4")
        protected String attribute4;
        @XmlElement(name = "ATTRIBUTE5")
        protected String attribute5;
        @XmlElement(name = "ATTRIBUTE6")
        protected String attribute6;
        @XmlElement(name = "ATTRIBUTE7")
        protected String attribute7;
        @XmlElement(name = "ATTRIBUTE8")
        protected String attribute8;
        @XmlElement(name = "ATTRIBUTE9")
        protected String attribute9;
        @XmlElement(name = "ATTRIBUTE10")
        protected String attribute10;
        @XmlElement(name = "ATTRIBUTE11")
        protected String attribute11;
        @XmlElement(name = "ATTRIBUTE12")
        protected String attribute12;
        @XmlElement(name = "ATTRIBUTE13")
        protected String attribute13;
        @XmlElement(name = "ATTRIBUTE14")
        protected String attribute14;
        @XmlElement(name = "ATTRIBUTE15")
        protected String attribute15;
        @XmlElement(name = "ATTRIBUTE16")
        protected String attribute16;
        @XmlElement(name = "ATTRIBUTE17")
        protected String attribute17;
        @XmlElement(name = "ATTRIBUTE18")
        protected String attribute18;
        @XmlElement(name = "ATTRIBUTE19")
        protected String attribute19;
        @XmlElement(name = "ATTRIBUTE20")
        protected String attribute20;
        @XmlElement(name = "ATTRIBUTE_NUMBER1")
        protected String attributenumber1;
        @XmlElement(name = "ATTRIBUTE_NUMBER2")
        protected String attributenumber2;
        @XmlElement(name = "ATTRIBUTE_NUMBER3")
        protected String attributenumber3;
        @XmlElement(name = "ATTRIBUTE_NUMBER4")
        protected String attributenumber4;
        @XmlElement(name = "ATTRIBUTE_NUMBER5")
        protected String attributenumber5;
        @XmlElement(name = "ATTRIBUTE_NUMBER6")
        protected String attributenumber6;
        @XmlElement(name = "ATTRIBUTE_NUMBER7")
        protected String attributenumber7;
        @XmlElement(name = "ATTRIBUTE_NUMBER8")
        protected String attributenumber8;
        @XmlElement(name = "ATTRIBUTE_NUMBER9")
        protected String attributenumber9;
        @XmlElement(name = "ATTRIBUTE_NUMBER10")
        protected String attributenumber10;
        @XmlElement(name = "ATTRIBUTE_DATE1")
        protected String attributedate1;
        @XmlElement(name = "ATTRIBUTE_DATE2")
        protected String attributedate2;
        @XmlElement(name = "ATTRIBUTE_DATE3")
        protected String attributedate3;
        @XmlElement(name = "ATTRIBUTE_DATE4")
        protected String attributedate4;
        @XmlElement(name = "ATTRIBUTE_DATE5")
        protected String attributedate5;
        @XmlElement(name = "ATTRIBUTE_DATE6")
        protected String attributedate6;
        @XmlElement(name = "ATTRIBUTE_DATE7")
        protected String attributedate7;
        @XmlElement(name = "ATTRIBUTE_DATE8")
        protected String attributedate8;
        @XmlElement(name = "ATTRIBUTE_DATE9")
        protected String attributedate9;
        @XmlElement(name = "ATTRIBUTE_DATE10")
        protected String attributedate10;
        @XmlAttribute(name = "num")
        protected String num;

        /**
         * Obtém o valor da propriedade accessorialcostgid.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getACCESSORIALCOSTGID() {
            return accessorialcostgid;
        }

        /**
         * Define o valor da propriedade accessorialcostgid.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setACCESSORIALCOSTGID(String value) {
            this.accessorialcostgid = value;
        }

        /**
         * Obtém o valor da propriedade accessorialcostxid.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getACCESSORIALCOSTXID() {
            return accessorialcostxid;
        }

        /**
         * Define o valor da propriedade accessorialcostxid.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setACCESSORIALCOSTXID(String value) {
            this.accessorialcostxid = value;
        }

        /**
         * Obtém o valor da propriedade description.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDESCRIPTION() {
            return description;
        }

        /**
         * Define o valor da propriedade description.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDESCRIPTION(String value) {
            this.description = value;
        }

        /**
         * Obtém o valor da propriedade leftoperand1.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLEFTOPERAND1() {
            return leftoperand1;
        }

        /**
         * Define o valor da propriedade leftoperand1.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLEFTOPERAND1(String value) {
            this.leftoperand1 = value;
        }

        /**
         * Obtém o valor da propriedade oper1GID.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getOPER1GID() {
            return oper1GID;
        }

        /**
         * Define o valor da propriedade oper1GID.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setOPER1GID(String value) {
            this.oper1GID = value;
        }

        /**
         * Obtém o valor da propriedade lowvalue1.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLOWVALUE1() {
            return lowvalue1;
        }

        /**
         * Define o valor da propriedade lowvalue1.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLOWVALUE1(String value) {
            this.lowvalue1 = value;
        }

        /**
         * Obtém o valor da propriedade highvalue1.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getHIGHVALUE1() {
            return highvalue1;
        }

        /**
         * Define o valor da propriedade highvalue1.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setHIGHVALUE1(String value) {
            this.highvalue1 = value;
        }

        /**
         * Obtém o valor da propriedade andor1.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getANDOR1() {
            return andor1;
        }

        /**
         * Define o valor da propriedade andor1.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setANDOR1(String value) {
            this.andor1 = value;
        }

        /**
         * Obtém o valor da propriedade leftoperand2.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLEFTOPERAND2() {
            return leftoperand2;
        }

        /**
         * Define o valor da propriedade leftoperand2.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLEFTOPERAND2(String value) {
            this.leftoperand2 = value;
        }

        /**
         * Obtém o valor da propriedade oper2GID.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getOPER2GID() {
            return oper2GID;
        }

        /**
         * Define o valor da propriedade oper2GID.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setOPER2GID(String value) {
            this.oper2GID = value;
        }

        /**
         * Obtém o valor da propriedade lowvalue2.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLOWVALUE2() {
            return lowvalue2;
        }

        /**
         * Define o valor da propriedade lowvalue2.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLOWVALUE2(String value) {
            this.lowvalue2 = value;
        }

        /**
         * Obtém o valor da propriedade highvalue2.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getHIGHVALUE2() {
            return highvalue2;
        }

        /**
         * Define o valor da propriedade highvalue2.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setHIGHVALUE2(String value) {
            this.highvalue2 = value;
        }

        /**
         * Obtém o valor da propriedade andor2.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getANDOR2() {
            return andor2;
        }

        /**
         * Define o valor da propriedade andor2.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setANDOR2(String value) {
            this.andor2 = value;
        }

        /**
         * Obtém o valor da propriedade leftoperand3.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLEFTOPERAND3() {
            return leftoperand3;
        }

        /**
         * Define o valor da propriedade leftoperand3.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLEFTOPERAND3(String value) {
            this.leftoperand3 = value;
        }

        /**
         * Obtém o valor da propriedade oper3GID.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getOPER3GID() {
            return oper3GID;
        }

        /**
         * Define o valor da propriedade oper3GID.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setOPER3GID(String value) {
            this.oper3GID = value;
        }

        /**
         * Obtém o valor da propriedade lowvalue3.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLOWVALUE3() {
            return lowvalue3;
        }

        /**
         * Define o valor da propriedade lowvalue3.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLOWVALUE3(String value) {
            this.lowvalue3 = value;
        }

        /**
         * Obtém o valor da propriedade highvalue3.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getHIGHVALUE3() {
            return highvalue3;
        }

        /**
         * Define o valor da propriedade highvalue3.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setHIGHVALUE3(String value) {
            this.highvalue3 = value;
        }

        /**
         * Obtém o valor da propriedade andor3.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getANDOR3() {
            return andor3;
        }

        /**
         * Define o valor da propriedade andor3.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setANDOR3(String value) {
            this.andor3 = value;
        }

        /**
         * Obtém o valor da propriedade leftoperand4.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLEFTOPERAND4() {
            return leftoperand4;
        }

        /**
         * Define o valor da propriedade leftoperand4.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLEFTOPERAND4(String value) {
            this.leftoperand4 = value;
        }

        /**
         * Obtém o valor da propriedade oper4GID.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getOPER4GID() {
            return oper4GID;
        }

        /**
         * Define o valor da propriedade oper4GID.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setOPER4GID(String value) {
            this.oper4GID = value;
        }

        /**
         * Obtém o valor da propriedade lowvalue4.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLOWVALUE4() {
            return lowvalue4;
        }

        /**
         * Define o valor da propriedade lowvalue4.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLOWVALUE4(String value) {
            this.lowvalue4 = value;
        }

        /**
         * Obtém o valor da propriedade highvalue4.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getHIGHVALUE4() {
            return highvalue4;
        }

        /**
         * Define o valor da propriedade highvalue4.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setHIGHVALUE4(String value) {
            this.highvalue4 = value;
        }

        /**
         * Obtém o valor da propriedade chargemultiplier.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCHARGEMULTIPLIER() {
            return chargemultiplier;
        }

        /**
         * Define o valor da propriedade chargemultiplier.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCHARGEMULTIPLIER(String value) {
            this.chargemultiplier = value;
        }

        /**
         * Obtém o valor da propriedade chargeamount.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCHARGEAMOUNT() {
            return chargeamount;
        }

        /**
         * Define o valor da propriedade chargeamount.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCHARGEAMOUNT(String value) {
            this.chargeamount = value;
        }

        /**
         * Obtém o valor da propriedade chargeamountgid.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCHARGEAMOUNTGID() {
            return chargeamountgid;
        }

        /**
         * Define o valor da propriedade chargeamountgid.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCHARGEAMOUNTGID(String value) {
            this.chargeamountgid = value;
        }

        /**
         * Obtém o valor da propriedade chargeamountbase.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCHARGEAMOUNTBASE() {
            return chargeamountbase;
        }

        /**
         * Define o valor da propriedade chargeamountbase.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCHARGEAMOUNTBASE(String value) {
            this.chargeamountbase = value;
        }

        /**
         * Obtém o valor da propriedade chargeunituomcode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCHARGEUNITUOMCODE() {
            return chargeunituomcode;
        }

        /**
         * Define o valor da propriedade chargeunituomcode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCHARGEUNITUOMCODE(String value) {
            this.chargeunituomcode = value;
        }

        /**
         * Obtém o valor da propriedade chargeunitcount.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCHARGEUNITCOUNT() {
            return chargeunitcount;
        }

        /**
         * Define o valor da propriedade chargeunitcount.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCHARGEUNITCOUNT(String value) {
            this.chargeunitcount = value;
        }

        /**
         * Obtém o valor da propriedade chargemultiplierscalar.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCHARGEMULTIPLIERSCALAR() {
            return chargemultiplierscalar;
        }

        /**
         * Define o valor da propriedade chargemultiplierscalar.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCHARGEMULTIPLIERSCALAR(String value) {
            this.chargemultiplierscalar = value;
        }

        /**
         * Obtém o valor da propriedade chargebreakcomparator.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCHARGEBREAKCOMPARATOR() {
            return chargebreakcomparator;
        }

        /**
         * Define o valor da propriedade chargebreakcomparator.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCHARGEBREAKCOMPARATOR(String value) {
            this.chargebreakcomparator = value;
        }

        /**
         * Obtém o valor da propriedade chargeaction.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCHARGEACTION() {
            return chargeaction;
        }

        /**
         * Define o valor da propriedade chargeaction.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCHARGEACTION(String value) {
            this.chargeaction = value;
        }

        /**
         * Obtém o valor da propriedade chargetype.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCHARGETYPE() {
            return chargetype;
        }

        /**
         * Define o valor da propriedade chargetype.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCHARGETYPE(String value) {
            this.chargetype = value;
        }

        /**
         * Obtém o valor da propriedade chargemultiplier2.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCHARGEMULTIPLIER2() {
            return chargemultiplier2;
        }

        /**
         * Define o valor da propriedade chargemultiplier2.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCHARGEMULTIPLIER2(String value) {
            this.chargemultiplier2 = value;
        }

        /**
         * Obtém o valor da propriedade chargeunituomcode2.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCHARGEUNITUOMCODE2() {
            return chargeunituomcode2;
        }

        /**
         * Define o valor da propriedade chargeunituomcode2.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCHARGEUNITUOMCODE2(String value) {
            this.chargeunituomcode2 = value;
        }

        /**
         * Obtém o valor da propriedade chargeunitcount2.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCHARGEUNITCOUNT2() {
            return chargeunitcount2;
        }

        /**
         * Define o valor da propriedade chargeunitcount2.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCHARGEUNITCOUNT2(String value) {
            this.chargeunitcount2 = value;
        }

        /**
         * Obtém o valor da propriedade usedefaults.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getUSEDEFAULTS() {
            return usedefaults;
        }

        /**
         * Define o valor da propriedade usedefaults.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setUSEDEFAULTS(String value) {
            this.usedefaults = value;
        }

        /**
         * Obtém o valor da propriedade chargesequence.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCHARGESEQUENCE() {
            return chargesequence;
        }

        /**
         * Define o valor da propriedade chargesequence.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCHARGESEQUENCE(String value) {
            this.chargesequence = value;
        }

        /**
         * Obtém o valor da propriedade chargegroup.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCHARGEGROUP() {
            return chargegroup;
        }

        /**
         * Define o valor da propriedade chargegroup.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCHARGEGROUP(String value) {
            this.chargegroup = value;
        }

        /**
         * Obtém o valor da propriedade chargeunitbreakgid.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCHARGEUNITBREAKGID() {
            return chargeunitbreakgid;
        }

        /**
         * Define o valor da propriedade chargeunitbreakgid.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCHARGEUNITBREAKGID(String value) {
            this.chargeunitbreakgid = value;
        }

        /**
         * Obtém o valor da propriedade chargemultiplieroption.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCHARGEMULTIPLIEROPTION() {
            return chargemultiplieroption;
        }

        /**
         * Define o valor da propriedade chargemultiplieroption.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCHARGEMULTIPLIEROPTION(String value) {
            this.chargemultiplieroption = value;
        }

        /**
         * Obtém o valor da propriedade rategeocostoperandseq.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRATEGEOCOSTOPERANDSEQ() {
            return rategeocostoperandseq;
        }

        /**
         * Define o valor da propriedade rategeocostoperandseq.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRATEGEOCOSTOPERANDSEQ(String value) {
            this.rategeocostoperandseq = value;
        }

        /**
         * Obtém o valor da propriedade dimratefactorgid.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDIMRATEFACTORGID() {
            return dimratefactorgid;
        }

        /**
         * Define o valor da propriedade dimratefactorgid.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDIMRATEFACTORGID(String value) {
            this.dimratefactorgid = value;
        }

        /**
         * Obtém o valor da propriedade usesunitbreaks.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getUSESUNITBREAKS() {
            return usesunitbreaks;
        }

        /**
         * Define o valor da propriedade usesunitbreaks.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setUSESUNITBREAKS(String value) {
            this.usesunitbreaks = value;
        }

        /**
         * Obtém o valor da propriedade effectivedate.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEFFECTIVEDATE() {
            return effectivedate;
        }

        /**
         * Define o valor da propriedade effectivedate.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEFFECTIVEDATE(String value) {
            this.effectivedate = value;
        }

        /**
         * Obtém o valor da propriedade expirationdate.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEXPIRATIONDATE() {
            return expirationdate;
        }

        /**
         * Define o valor da propriedade expirationdate.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEXPIRATIONDATE(String value) {
            this.expirationdate = value;
        }

        /**
         * Obtém o valor da propriedade calendargid.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCALENDARGID() {
            return calendargid;
        }

        /**
         * Define o valor da propriedade calendargid.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCALENDARGID(String value) {
            this.calendargid = value;
        }

        /**
         * Obtém o valor da propriedade notes.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNOTES() {
            return notes;
        }

        /**
         * Define o valor da propriedade notes.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNOTES(String value) {
            this.notes = value;
        }

        /**
         * Obtém o valor da propriedade roundingtype.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getROUNDINGTYPE() {
            return roundingtype;
        }

        /**
         * Define o valor da propriedade roundingtype.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setROUNDINGTYPE(String value) {
            this.roundingtype = value;
        }

        /**
         * Obtém o valor da propriedade roundinginterval.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getROUNDINGINTERVAL() {
            return roundinginterval;
        }

        /**
         * Define o valor da propriedade roundinginterval.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setROUNDINGINTERVAL(String value) {
            this.roundinginterval = value;
        }

        /**
         * Obtém o valor da propriedade roundingfieldslevel.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getROUNDINGFIELDSLEVEL() {
            return roundingfieldslevel;
        }

        /**
         * Define o valor da propriedade roundingfieldslevel.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setROUNDINGFIELDSLEVEL(String value) {
            this.roundingfieldslevel = value;
        }

        /**
         * Obtém o valor da propriedade roundingapplication.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getROUNDINGAPPLICATION() {
            return roundingapplication;
        }

        /**
         * Define o valor da propriedade roundingapplication.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setROUNDINGAPPLICATION(String value) {
            this.roundingapplication = value;
        }

        /**
         * Obtém o valor da propriedade mincost.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMINCOST() {
            return mincost;
        }

        /**
         * Define o valor da propriedade mincost.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMINCOST(String value) {
            this.mincost = value;
        }

        /**
         * Obtém o valor da propriedade mincostcurrencygid.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMINCOSTCURRENCYGID() {
            return mincostcurrencygid;
        }

        /**
         * Define o valor da propriedade mincostcurrencygid.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMINCOSTCURRENCYGID(String value) {
            this.mincostcurrencygid = value;
        }

        /**
         * Obtém o valor da propriedade mincostbase.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMINCOSTBASE() {
            return mincostbase;
        }

        /**
         * Define o valor da propriedade mincostbase.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMINCOSTBASE(String value) {
            this.mincostbase = value;
        }

        /**
         * Obtém o valor da propriedade maxcost.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMAXCOST() {
            return maxcost;
        }

        /**
         * Define o valor da propriedade maxcost.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMAXCOST(String value) {
            this.maxcost = value;
        }

        /**
         * Obtém o valor da propriedade maxcostcurrencygid.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMAXCOSTCURRENCYGID() {
            return maxcostcurrencygid;
        }

        /**
         * Define o valor da propriedade maxcostcurrencygid.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMAXCOSTCURRENCYGID(String value) {
            this.maxcostcurrencygid = value;
        }

        /**
         * Obtém o valor da propriedade maxcostbase.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMAXCOSTBASE() {
            return maxcostbase;
        }

        /**
         * Define o valor da propriedade maxcostbase.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMAXCOSTBASE(String value) {
            this.maxcostbase = value;
        }

        /**
         * Obtém o valor da propriedade paymentmethodcodegid.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPAYMENTMETHODCODEGID() {
            return paymentmethodcodegid;
        }

        /**
         * Define o valor da propriedade paymentmethodcodegid.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPAYMENTMETHODCODEGID(String value) {
            this.paymentmethodcodegid = value;
        }

        /**
         * Obtém o valor da propriedade isfiledastariff.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getISFILEDASTARIFF() {
            return isfiledastariff;
        }

        /**
         * Define o valor da propriedade isfiledastariff.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setISFILEDASTARIFF(String value) {
            this.isfiledastariff = value;
        }

        /**
         * Obtém o valor da propriedade costtype.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCOSTTYPE() {
            return costtype;
        }

        /**
         * Define o valor da propriedade costtype.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCOSTTYPE(String value) {
            this.costtype = value;
        }

        /**
         * Obtém o valor da propriedade calendaractivitygid.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCALENDARACTIVITYGID() {
            return calendaractivitygid;
        }

        /**
         * Define o valor da propriedade calendaractivitygid.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCALENDARACTIVITYGID(String value) {
            this.calendaractivitygid = value;
        }

        /**
         * Obtém o valor da propriedade rateunitbreakprofilegid.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRATEUNITBREAKPROFILEGID() {
            return rateunitbreakprofilegid;
        }

        /**
         * Define o valor da propriedade rateunitbreakprofilegid.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRATEUNITBREAKPROFILEGID(String value) {
            this.rateunitbreakprofilegid = value;
        }

        /**
         * Obtém o valor da propriedade rateunitbreakprofile2GID.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRATEUNITBREAKPROFILE2GID() {
            return rateunitbreakprofile2GID;
        }

        /**
         * Define o valor da propriedade rateunitbreakprofile2GID.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRATEUNITBREAKPROFILE2GID(String value) {
            this.rateunitbreakprofile2GID = value;
        }

        /**
         * Obtém o valor da propriedade chargebreakcomparator2GID.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCHARGEBREAKCOMPARATOR2GID() {
            return chargebreakcomparator2GID;
        }

        /**
         * Define o valor da propriedade chargebreakcomparator2GID.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCHARGEBREAKCOMPARATOR2GID(String value) {
            this.chargebreakcomparator2GID = value;
        }

        /**
         * Obtém o valor da propriedade externalratingenginegid.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEXTERNALRATINGENGINEGID() {
            return externalratingenginegid;
        }

        /**
         * Define o valor da propriedade externalratingenginegid.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEXTERNALRATINGENGINEGID(String value) {
            this.externalratingenginegid = value;
        }

        /**
         * Obtém o valor da propriedade extrefieldsetgid.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEXTREFIELDSETGID() {
            return extrefieldsetgid;
        }

        /**
         * Define o valor da propriedade extrefieldsetgid.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEXTREFIELDSETGID(String value) {
            this.extrefieldsetgid = value;
        }

        /**
         * Obtém o valor da propriedade costcategorygid.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCOSTCATEGORYGID() {
            return costcategorygid;
        }

        /**
         * Define o valor da propriedade costcategorygid.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCOSTCATEGORYGID(String value) {
            this.costcategorygid = value;
        }

        /**
         * Obtém o valor da propriedade costcodegid.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCOSTCODEGID() {
            return costcodegid;
        }

        /**
         * Define o valor da propriedade costcodegid.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCOSTCODEGID(String value) {
            this.costcodegid = value;
        }

        /**
         * Obtém o valor da propriedade domainname.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDOMAINNAME() {
            return domainname;
        }

        /**
         * Define o valor da propriedade domainname.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDOMAINNAME(String value) {
            this.domainname = value;
        }

        /**
         * Obtém o valor da propriedade insertuser.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getINSERTUSER() {
            return insertuser;
        }

        /**
         * Define o valor da propriedade insertuser.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setINSERTUSER(String value) {
            this.insertuser = value;
        }

        /**
         * Obtém o valor da propriedade insertdate.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getINSERTDATE() {
            return insertdate;
        }

        /**
         * Define o valor da propriedade insertdate.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setINSERTDATE(String value) {
            this.insertdate = value;
        }

        /**
         * Obtém o valor da propriedade updateuser.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getUPDATEUSER() {
            return updateuser;
        }

        /**
         * Define o valor da propriedade updateuser.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setUPDATEUSER(String value) {
            this.updateuser = value;
        }

        /**
         * Obtém o valor da propriedade updatedate.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getUPDATEDATE() {
            return updatedate;
        }

        /**
         * Define o valor da propriedade updatedate.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setUPDATEDATE(String value) {
            this.updatedate = value;
        }

        /**
         * Obtém o valor da propriedade accessorialcostunitbreak.
         * 
         * @return
         *     possible object is
         *     {@link ACCESSORIALCOSTUNITBREAK }
         *     
         */
        public ACCESSORIALCOSTUNITBREAK getACCESSORIALCOSTUNITBREAK() {
            return accessorialcostunitbreak;
        }

        /**
         * Define o valor da propriedade accessorialcostunitbreak.
         * 
         * @param value
         *     allowed object is
         *     {@link ACCESSORIALCOSTUNITBREAK }
         *     
         */
        public void setACCESSORIALCOSTUNITBREAK(ACCESSORIALCOSTUNITBREAK value) {
            this.accessorialcostunitbreak = value;
        }

        /**
         * Obtém o valor da propriedade attribute1.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE1() {
            return attribute1;
        }

        /**
         * Define o valor da propriedade attribute1.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE1(String value) {
            this.attribute1 = value;
        }

        /**
         * Obtém o valor da propriedade attribute2.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE2() {
            return attribute2;
        }

        /**
         * Define o valor da propriedade attribute2.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE2(String value) {
            this.attribute2 = value;
        }

        /**
         * Obtém o valor da propriedade attribute3.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE3() {
            return attribute3;
        }

        /**
         * Define o valor da propriedade attribute3.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE3(String value) {
            this.attribute3 = value;
        }

        /**
         * Obtém o valor da propriedade attribute4.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE4() {
            return attribute4;
        }

        /**
         * Define o valor da propriedade attribute4.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE4(String value) {
            this.attribute4 = value;
        }

        /**
         * Obtém o valor da propriedade attribute5.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE5() {
            return attribute5;
        }

        /**
         * Define o valor da propriedade attribute5.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE5(String value) {
            this.attribute5 = value;
        }

        /**
         * Obtém o valor da propriedade attribute6.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE6() {
            return attribute6;
        }

        /**
         * Define o valor da propriedade attribute6.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE6(String value) {
            this.attribute6 = value;
        }

        /**
         * Obtém o valor da propriedade attribute7.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE7() {
            return attribute7;
        }

        /**
         * Define o valor da propriedade attribute7.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE7(String value) {
            this.attribute7 = value;
        }

        /**
         * Obtém o valor da propriedade attribute8.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE8() {
            return attribute8;
        }

        /**
         * Define o valor da propriedade attribute8.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE8(String value) {
            this.attribute8 = value;
        }

        /**
         * Obtém o valor da propriedade attribute9.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE9() {
            return attribute9;
        }

        /**
         * Define o valor da propriedade attribute9.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE9(String value) {
            this.attribute9 = value;
        }

        /**
         * Obtém o valor da propriedade attribute10.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE10() {
            return attribute10;
        }

        /**
         * Define o valor da propriedade attribute10.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE10(String value) {
            this.attribute10 = value;
        }

        /**
         * Obtém o valor da propriedade attribute11.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE11() {
            return attribute11;
        }

        /**
         * Define o valor da propriedade attribute11.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE11(String value) {
            this.attribute11 = value;
        }

        /**
         * Obtém o valor da propriedade attribute12.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE12() {
            return attribute12;
        }

        /**
         * Define o valor da propriedade attribute12.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE12(String value) {
            this.attribute12 = value;
        }

        /**
         * Obtém o valor da propriedade attribute13.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE13() {
            return attribute13;
        }

        /**
         * Define o valor da propriedade attribute13.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE13(String value) {
            this.attribute13 = value;
        }

        /**
         * Obtém o valor da propriedade attribute14.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE14() {
            return attribute14;
        }

        /**
         * Define o valor da propriedade attribute14.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE14(String value) {
            this.attribute14 = value;
        }

        /**
         * Obtém o valor da propriedade attribute15.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE15() {
            return attribute15;
        }

        /**
         * Define o valor da propriedade attribute15.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE15(String value) {
            this.attribute15 = value;
        }

        /**
         * Obtém o valor da propriedade attribute16.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE16() {
            return attribute16;
        }

        /**
         * Define o valor da propriedade attribute16.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE16(String value) {
            this.attribute16 = value;
        }

        /**
         * Obtém o valor da propriedade attribute17.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE17() {
            return attribute17;
        }

        /**
         * Define o valor da propriedade attribute17.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE17(String value) {
            this.attribute17 = value;
        }

        /**
         * Obtém o valor da propriedade attribute18.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE18() {
            return attribute18;
        }

        /**
         * Define o valor da propriedade attribute18.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE18(String value) {
            this.attribute18 = value;
        }

        /**
         * Obtém o valor da propriedade attribute19.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE19() {
            return attribute19;
        }

        /**
         * Define o valor da propriedade attribute19.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE19(String value) {
            this.attribute19 = value;
        }

        /**
         * Obtém o valor da propriedade attribute20.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE20() {
            return attribute20;
        }

        /**
         * Define o valor da propriedade attribute20.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE20(String value) {
            this.attribute20 = value;
        }

        /**
         * Obtém o valor da propriedade attributenumber1.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTENUMBER1() {
            return attributenumber1;
        }

        /**
         * Define o valor da propriedade attributenumber1.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTENUMBER1(String value) {
            this.attributenumber1 = value;
        }

        /**
         * Obtém o valor da propriedade attributenumber2.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTENUMBER2() {
            return attributenumber2;
        }

        /**
         * Define o valor da propriedade attributenumber2.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTENUMBER2(String value) {
            this.attributenumber2 = value;
        }

        /**
         * Obtém o valor da propriedade attributenumber3.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTENUMBER3() {
            return attributenumber3;
        }

        /**
         * Define o valor da propriedade attributenumber3.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTENUMBER3(String value) {
            this.attributenumber3 = value;
        }

        /**
         * Obtém o valor da propriedade attributenumber4.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTENUMBER4() {
            return attributenumber4;
        }

        /**
         * Define o valor da propriedade attributenumber4.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTENUMBER4(String value) {
            this.attributenumber4 = value;
        }

        /**
         * Obtém o valor da propriedade attributenumber5.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTENUMBER5() {
            return attributenumber5;
        }

        /**
         * Define o valor da propriedade attributenumber5.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTENUMBER5(String value) {
            this.attributenumber5 = value;
        }

        /**
         * Obtém o valor da propriedade attributenumber6.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTENUMBER6() {
            return attributenumber6;
        }

        /**
         * Define o valor da propriedade attributenumber6.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTENUMBER6(String value) {
            this.attributenumber6 = value;
        }

        /**
         * Obtém o valor da propriedade attributenumber7.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTENUMBER7() {
            return attributenumber7;
        }

        /**
         * Define o valor da propriedade attributenumber7.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTENUMBER7(String value) {
            this.attributenumber7 = value;
        }

        /**
         * Obtém o valor da propriedade attributenumber8.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTENUMBER8() {
            return attributenumber8;
        }

        /**
         * Define o valor da propriedade attributenumber8.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTENUMBER8(String value) {
            this.attributenumber8 = value;
        }

        /**
         * Obtém o valor da propriedade attributenumber9.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTENUMBER9() {
            return attributenumber9;
        }

        /**
         * Define o valor da propriedade attributenumber9.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTENUMBER9(String value) {
            this.attributenumber9 = value;
        }

        /**
         * Obtém o valor da propriedade attributenumber10.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTENUMBER10() {
            return attributenumber10;
        }

        /**
         * Define o valor da propriedade attributenumber10.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTENUMBER10(String value) {
            this.attributenumber10 = value;
        }

        /**
         * Obtém o valor da propriedade attributedate1.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTEDATE1() {
            return attributedate1;
        }

        /**
         * Define o valor da propriedade attributedate1.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTEDATE1(String value) {
            this.attributedate1 = value;
        }

        /**
         * Obtém o valor da propriedade attributedate2.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTEDATE2() {
            return attributedate2;
        }

        /**
         * Define o valor da propriedade attributedate2.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTEDATE2(String value) {
            this.attributedate2 = value;
        }

        /**
         * Obtém o valor da propriedade attributedate3.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTEDATE3() {
            return attributedate3;
        }

        /**
         * Define o valor da propriedade attributedate3.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTEDATE3(String value) {
            this.attributedate3 = value;
        }

        /**
         * Obtém o valor da propriedade attributedate4.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTEDATE4() {
            return attributedate4;
        }

        /**
         * Define o valor da propriedade attributedate4.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTEDATE4(String value) {
            this.attributedate4 = value;
        }

        /**
         * Obtém o valor da propriedade attributedate5.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTEDATE5() {
            return attributedate5;
        }

        /**
         * Define o valor da propriedade attributedate5.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTEDATE5(String value) {
            this.attributedate5 = value;
        }

        /**
         * Obtém o valor da propriedade attributedate6.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTEDATE6() {
            return attributedate6;
        }

        /**
         * Define o valor da propriedade attributedate6.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTEDATE6(String value) {
            this.attributedate6 = value;
        }

        /**
         * Obtém o valor da propriedade attributedate7.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTEDATE7() {
            return attributedate7;
        }

        /**
         * Define o valor da propriedade attributedate7.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTEDATE7(String value) {
            this.attributedate7 = value;
        }

        /**
         * Obtém o valor da propriedade attributedate8.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTEDATE8() {
            return attributedate8;
        }

        /**
         * Define o valor da propriedade attributedate8.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTEDATE8(String value) {
            this.attributedate8 = value;
        }

        /**
         * Obtém o valor da propriedade attributedate9.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTEDATE9() {
            return attributedate9;
        }

        /**
         * Define o valor da propriedade attributedate9.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTEDATE9(String value) {
            this.attributedate9 = value;
        }

        /**
         * Obtém o valor da propriedade attributedate10.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTEDATE10() {
            return attributedate10;
        }

        /**
         * Define o valor da propriedade attributedate10.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTEDATE10(String value) {
            this.attributedate10 = value;
        }

        /**
         * Obtém o valor da propriedade num.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNum() {
            return num;
        }

        /**
         * Define o valor da propriedade num.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNum(String value) {
            this.num = value;
        }


        /**
         * <p>Classe Java de anonymous complex type.
         * 
         * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="ACCESSORIAL_COST_UNIT_BREAK_ROW" maxOccurs="unbounded" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="ACCESSORIAL_COST_GID" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="RATE_UNIT_BREAK_GID" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="CHARGE_AMOUNT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                   &lt;element name="CHARGE_AMOUNT_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                   &lt;element name="CHARGE_AMOUNT_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                   &lt;element name="CHARGE_DISCOUNT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                   &lt;element name="MIN_COST" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                   &lt;element name="MIN_COST_CURRENCY_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                   &lt;element name="MIN_COST_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                   &lt;element name="MAX_COST" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                   &lt;element name="MAX_COST_CURRENCY_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                   &lt;element name="MAX_COST_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                   &lt;element name="RATE_UNIT_BREAK2_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                   &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                   &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                 &lt;/sequence>
         *                 &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" />
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "accessorialcostunitbreakrow"
        })
        public static class ACCESSORIALCOSTUNITBREAK {

            @XmlElement(name = "ACCESSORIAL_COST_UNIT_BREAK_ROW")
            protected List<ACCESSORIALCOSTUNITBREAKROW> accessorialcostunitbreakrow;

            /**
             * Gets the value of the accessorialcostunitbreakrow property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the accessorialcostunitbreakrow property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getACCESSORIALCOSTUNITBREAKROW().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link ACCESSORIALCOSTUNITBREAKROW }
             * 
             * 
             */
            public List<ACCESSORIALCOSTUNITBREAKROW> getACCESSORIALCOSTUNITBREAKROW() {
                if (accessorialcostunitbreakrow == null) {
                    accessorialcostunitbreakrow = new ArrayList<ACCESSORIALCOSTUNITBREAKROW>();
                }
                return this.accessorialcostunitbreakrow;
            }


            /**
             * <p>Classe Java de anonymous complex type.
             * 
             * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="ACCESSORIAL_COST_GID" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="RATE_UNIT_BREAK_GID" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="CHARGE_AMOUNT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *         &lt;element name="CHARGE_AMOUNT_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *         &lt;element name="CHARGE_AMOUNT_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *         &lt;element name="CHARGE_DISCOUNT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *         &lt;element name="MIN_COST" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *         &lt;element name="MIN_COST_CURRENCY_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *         &lt;element name="MIN_COST_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *         &lt;element name="MAX_COST" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *         &lt;element name="MAX_COST_CURRENCY_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *         &lt;element name="MAX_COST_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *         &lt;element name="RATE_UNIT_BREAK2_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *         &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *         &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *       &lt;/sequence>
             *       &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" />
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "accessorialcostgid",
                "rateunitbreakgid",
                "chargeamount",
                "chargeamountgid",
                "chargeamountbase",
                "chargediscount",
                "mincost",
                "mincostcurrencygid",
                "mincostbase",
                "maxcost",
                "maxcostcurrencygid",
                "maxcostbase",
                "rateunitbreak2GID",
                "domainname",
                "insertuser",
                "insertdate",
                "updateuser",
                "updatedate"
            })
            public static class ACCESSORIALCOSTUNITBREAKROW {

                @XmlElement(name = "ACCESSORIAL_COST_GID", required = true)
                protected String accessorialcostgid;
                @XmlElement(name = "RATE_UNIT_BREAK_GID", required = true)
                protected String rateunitbreakgid;
                @XmlElement(name = "CHARGE_AMOUNT")
                protected String chargeamount;
                @XmlElement(name = "CHARGE_AMOUNT_GID")
                protected String chargeamountgid;
                @XmlElement(name = "CHARGE_AMOUNT_BASE")
                protected String chargeamountbase;
                @XmlElement(name = "CHARGE_DISCOUNT")
                protected String chargediscount;
                @XmlElement(name = "MIN_COST")
                protected String mincost;
                @XmlElement(name = "MIN_COST_CURRENCY_GID")
                protected String mincostcurrencygid;
                @XmlElement(name = "MIN_COST_BASE")
                protected String mincostbase;
                @XmlElement(name = "MAX_COST")
                protected String maxcost;
                @XmlElement(name = "MAX_COST_CURRENCY_GID")
                protected String maxcostcurrencygid;
                @XmlElement(name = "MAX_COST_BASE")
                protected String maxcostbase;
                @XmlElement(name = "RATE_UNIT_BREAK2_GID")
                protected String rateunitbreak2GID;
                @XmlElement(name = "DOMAIN_NAME", required = true)
                protected String domainname;
                @XmlElement(name = "INSERT_USER", required = true)
                protected String insertuser;
                @XmlElement(name = "INSERT_DATE", required = true)
                protected String insertdate;
                @XmlElement(name = "UPDATE_USER")
                protected String updateuser;
                @XmlElement(name = "UPDATE_DATE")
                protected String updatedate;
                @XmlAttribute(name = "num")
                protected String num;

                /**
                 * Obtém o valor da propriedade accessorialcostgid.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getACCESSORIALCOSTGID() {
                    return accessorialcostgid;
                }

                /**
                 * Define o valor da propriedade accessorialcostgid.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setACCESSORIALCOSTGID(String value) {
                    this.accessorialcostgid = value;
                }

                /**
                 * Obtém o valor da propriedade rateunitbreakgid.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getRATEUNITBREAKGID() {
                    return rateunitbreakgid;
                }

                /**
                 * Define o valor da propriedade rateunitbreakgid.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setRATEUNITBREAKGID(String value) {
                    this.rateunitbreakgid = value;
                }

                /**
                 * Obtém o valor da propriedade chargeamount.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCHARGEAMOUNT() {
                    return chargeamount;
                }

                /**
                 * Define o valor da propriedade chargeamount.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCHARGEAMOUNT(String value) {
                    this.chargeamount = value;
                }

                /**
                 * Obtém o valor da propriedade chargeamountgid.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCHARGEAMOUNTGID() {
                    return chargeamountgid;
                }

                /**
                 * Define o valor da propriedade chargeamountgid.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCHARGEAMOUNTGID(String value) {
                    this.chargeamountgid = value;
                }

                /**
                 * Obtém o valor da propriedade chargeamountbase.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCHARGEAMOUNTBASE() {
                    return chargeamountbase;
                }

                /**
                 * Define o valor da propriedade chargeamountbase.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCHARGEAMOUNTBASE(String value) {
                    this.chargeamountbase = value;
                }

                /**
                 * Obtém o valor da propriedade chargediscount.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCHARGEDISCOUNT() {
                    return chargediscount;
                }

                /**
                 * Define o valor da propriedade chargediscount.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCHARGEDISCOUNT(String value) {
                    this.chargediscount = value;
                }

                /**
                 * Obtém o valor da propriedade mincost.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getMINCOST() {
                    return mincost;
                }

                /**
                 * Define o valor da propriedade mincost.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setMINCOST(String value) {
                    this.mincost = value;
                }

                /**
                 * Obtém o valor da propriedade mincostcurrencygid.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getMINCOSTCURRENCYGID() {
                    return mincostcurrencygid;
                }

                /**
                 * Define o valor da propriedade mincostcurrencygid.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setMINCOSTCURRENCYGID(String value) {
                    this.mincostcurrencygid = value;
                }

                /**
                 * Obtém o valor da propriedade mincostbase.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getMINCOSTBASE() {
                    return mincostbase;
                }

                /**
                 * Define o valor da propriedade mincostbase.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setMINCOSTBASE(String value) {
                    this.mincostbase = value;
                }

                /**
                 * Obtém o valor da propriedade maxcost.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getMAXCOST() {
                    return maxcost;
                }

                /**
                 * Define o valor da propriedade maxcost.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setMAXCOST(String value) {
                    this.maxcost = value;
                }

                /**
                 * Obtém o valor da propriedade maxcostcurrencygid.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getMAXCOSTCURRENCYGID() {
                    return maxcostcurrencygid;
                }

                /**
                 * Define o valor da propriedade maxcostcurrencygid.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setMAXCOSTCURRENCYGID(String value) {
                    this.maxcostcurrencygid = value;
                }

                /**
                 * Obtém o valor da propriedade maxcostbase.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getMAXCOSTBASE() {
                    return maxcostbase;
                }

                /**
                 * Define o valor da propriedade maxcostbase.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setMAXCOSTBASE(String value) {
                    this.maxcostbase = value;
                }

                /**
                 * Obtém o valor da propriedade rateunitbreak2GID.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getRATEUNITBREAK2GID() {
                    return rateunitbreak2GID;
                }

                /**
                 * Define o valor da propriedade rateunitbreak2GID.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setRATEUNITBREAK2GID(String value) {
                    this.rateunitbreak2GID = value;
                }

                /**
                 * Obtém o valor da propriedade domainname.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDOMAINNAME() {
                    return domainname;
                }

                /**
                 * Define o valor da propriedade domainname.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDOMAINNAME(String value) {
                    this.domainname = value;
                }

                /**
                 * Obtém o valor da propriedade insertuser.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getINSERTUSER() {
                    return insertuser;
                }

                /**
                 * Define o valor da propriedade insertuser.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setINSERTUSER(String value) {
                    this.insertuser = value;
                }

                /**
                 * Obtém o valor da propriedade insertdate.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getINSERTDATE() {
                    return insertdate;
                }

                /**
                 * Define o valor da propriedade insertdate.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setINSERTDATE(String value) {
                    this.insertdate = value;
                }

                /**
                 * Obtém o valor da propriedade updateuser.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getUPDATEUSER() {
                    return updateuser;
                }

                /**
                 * Define o valor da propriedade updateuser.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setUPDATEUSER(String value) {
                    this.updateuser = value;
                }

                /**
                 * Obtém o valor da propriedade updatedate.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getUPDATEDATE() {
                    return updatedate;
                }

                /**
                 * Define o valor da propriedade updatedate.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setUPDATEDATE(String value) {
                    this.updatedate = value;
                }

                /**
                 * Obtém o valor da propriedade num.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getNum() {
                    return num;
                }

                /**
                 * Define o valor da propriedade num.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setNum(String value) {
                    this.num = value;
                }

            }

        }

    }

}
