
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.*;


/**
 * Defines Driver's Commercial driver license information.
 * 
 * <p>Classe Java de DriverCdlType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="DriverCdlType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TransactionCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransactionCodeType" minOccurs="0"/>
 *         &lt;element name="CdlClass" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CdlNum" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CdlExpDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType"/>
 *         &lt;element name="CdlIssueCountryGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="CdlIssueState" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DriverCdlType", propOrder = {
    "transactionCode",
    "cdlClass",
    "cdlNum",
    "cdlExpDate",
    "cdlIssueCountryGid",
    "cdlIssueState"
})
public class DriverCdlType {

    @XmlElement(name = "TransactionCode")
    @XmlSchemaType(name = "string")
    protected TransactionCodeType transactionCode;
    @XmlElement(name = "CdlClass")
    protected String cdlClass;
    @XmlElement(name = "CdlNum", required = true)
    protected String cdlNum;
    @XmlElement(name = "CdlExpDate", required = true)
    protected GLogDateTimeType cdlExpDate;
    @XmlElement(name = "CdlIssueCountryGid")
    protected GLogXMLGidType cdlIssueCountryGid;
    @XmlElement(name = "CdlIssueState", required = true)
    protected String cdlIssueState;

    /**
     * Obtém o valor da propriedade transactionCode.
     * 
     * @return
     *     possible object is
     *     {@link TransactionCodeType }
     *     
     */
    public TransactionCodeType getTransactionCode() {
        return transactionCode;
    }

    /**
     * Define o valor da propriedade transactionCode.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionCodeType }
     *     
     */
    public void setTransactionCode(TransactionCodeType value) {
        this.transactionCode = value;
    }

    /**
     * Obtém o valor da propriedade cdlClass.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCdlClass() {
        return cdlClass;
    }

    /**
     * Define o valor da propriedade cdlClass.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCdlClass(String value) {
        this.cdlClass = value;
    }

    /**
     * Obtém o valor da propriedade cdlNum.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCdlNum() {
        return cdlNum;
    }

    /**
     * Define o valor da propriedade cdlNum.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCdlNum(String value) {
        this.cdlNum = value;
    }

    /**
     * Obtém o valor da propriedade cdlExpDate.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getCdlExpDate() {
        return cdlExpDate;
    }

    /**
     * Define o valor da propriedade cdlExpDate.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setCdlExpDate(GLogDateTimeType value) {
        this.cdlExpDate = value;
    }

    /**
     * Obtém o valor da propriedade cdlIssueCountryGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getCdlIssueCountryGid() {
        return cdlIssueCountryGid;
    }

    /**
     * Define o valor da propriedade cdlIssueCountryGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setCdlIssueCountryGid(GLogXMLGidType value) {
        this.cdlIssueCountryGid = value;
    }

    /**
     * Obtém o valor da propriedade cdlIssueState.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCdlIssueState() {
        return cdlIssueState;
    }

    /**
     * Define o valor da propriedade cdlIssueState.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCdlIssueState(String value) {
        this.cdlIssueState = value;
    }

}
