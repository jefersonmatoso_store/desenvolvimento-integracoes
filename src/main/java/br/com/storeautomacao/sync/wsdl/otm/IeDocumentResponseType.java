
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 *             Refers to Document Responses.
 *          
 * 
 * <p>Classe Java de IeDocumentResponseType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="IeDocumentResponseType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SequenceNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ElementQualifier" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ElementValue" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ResponseCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ResponseSeverity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ResponseText" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IeDocumentResponseType", propOrder = {
    "sequenceNumber",
    "elementQualifier",
    "elementValue",
    "responseCode",
    "responseSeverity",
    "responseText"
})
public class IeDocumentResponseType {

    @XmlElement(name = "SequenceNumber", required = true)
    protected String sequenceNumber;
    @XmlElement(name = "ElementQualifier")
    protected String elementQualifier;
    @XmlElement(name = "ElementValue")
    protected String elementValue;
    @XmlElement(name = "ResponseCode")
    protected String responseCode;
    @XmlElement(name = "ResponseSeverity")
    protected String responseSeverity;
    @XmlElement(name = "ResponseText")
    protected String responseText;

    /**
     * Obtém o valor da propriedade sequenceNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSequenceNumber() {
        return sequenceNumber;
    }

    /**
     * Define o valor da propriedade sequenceNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSequenceNumber(String value) {
        this.sequenceNumber = value;
    }

    /**
     * Obtém o valor da propriedade elementQualifier.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getElementQualifier() {
        return elementQualifier;
    }

    /**
     * Define o valor da propriedade elementQualifier.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setElementQualifier(String value) {
        this.elementQualifier = value;
    }

    /**
     * Obtém o valor da propriedade elementValue.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getElementValue() {
        return elementValue;
    }

    /**
     * Define o valor da propriedade elementValue.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setElementValue(String value) {
        this.elementValue = value;
    }

    /**
     * Obtém o valor da propriedade responseCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResponseCode() {
        return responseCode;
    }

    /**
     * Define o valor da propriedade responseCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResponseCode(String value) {
        this.responseCode = value;
    }

    /**
     * Obtém o valor da propriedade responseSeverity.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResponseSeverity() {
        return responseSeverity;
    }

    /**
     * Define o valor da propriedade responseSeverity.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResponseSeverity(String value) {
        this.responseSeverity = value;
    }

    /**
     * Obtém o valor da propriedade responseText.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResponseText() {
        return responseText;
    }

    /**
     * Define o valor da propriedade responseText.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResponseText(String value) {
        this.responseText = value;
    }

}
