
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * 
 *             A TransOrder consists of header level information and detail information.
 *             The detail information consists of either line items, or ship units, but not both.
 *          
 * 
 * <p>Classe Java de TransOrderType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="TransOrderType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OTMTransactionInOut">
 *       &lt;sequence>
 *         &lt;element name="TransOrderHeader" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransOrderHeaderType"/>
 *         &lt;choice minOccurs="0">
 *           &lt;element name="TransOrderLineDetail" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransOrderLineDetailType" minOccurs="0"/>
 *           &lt;element name="ShipUnitDetail" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ShipUnitDetailType" minOccurs="0"/>
 *         &lt;/choice>
 *         &lt;element name="Location" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LocationType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="PackagedItem" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}PackagedItemType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TransOrderType", propOrder = {
    "transOrderHeader",
    "transOrderLineDetail",
    "shipUnitDetail",
    "location",
    "packagedItem"
})
public class TransOrderType
    extends OTMTransactionInOut
{

    @XmlElement(name = "TransOrderHeader", required = true)
    protected TransOrderHeaderType transOrderHeader;
    @XmlElement(name = "TransOrderLineDetail")
    protected TransOrderLineDetailType transOrderLineDetail;
    @XmlElement(name = "ShipUnitDetail")
    protected ShipUnitDetailType shipUnitDetail;
    @XmlElement(name = "Location")
    protected List<LocationType> location;
    @XmlElement(name = "PackagedItem")
    protected List<PackagedItemType> packagedItem;

    /**
     * Obtém o valor da propriedade transOrderHeader.
     * 
     * @return
     *     possible object is
     *     {@link TransOrderHeaderType }
     *     
     */
    public TransOrderHeaderType getTransOrderHeader() {
        return transOrderHeader;
    }

    /**
     * Define o valor da propriedade transOrderHeader.
     * 
     * @param value
     *     allowed object is
     *     {@link TransOrderHeaderType }
     *     
     */
    public void setTransOrderHeader(TransOrderHeaderType value) {
        this.transOrderHeader = value;
    }

    /**
     * Obtém o valor da propriedade transOrderLineDetail.
     * 
     * @return
     *     possible object is
     *     {@link TransOrderLineDetailType }
     *     
     */
    public TransOrderLineDetailType getTransOrderLineDetail() {
        return transOrderLineDetail;
    }

    /**
     * Define o valor da propriedade transOrderLineDetail.
     * 
     * @param value
     *     allowed object is
     *     {@link TransOrderLineDetailType }
     *     
     */
    public void setTransOrderLineDetail(TransOrderLineDetailType value) {
        this.transOrderLineDetail = value;
    }

    /**
     * Obtém o valor da propriedade shipUnitDetail.
     * 
     * @return
     *     possible object is
     *     {@link ShipUnitDetailType }
     *     
     */
    public ShipUnitDetailType getShipUnitDetail() {
        return shipUnitDetail;
    }

    /**
     * Define o valor da propriedade shipUnitDetail.
     * 
     * @param value
     *     allowed object is
     *     {@link ShipUnitDetailType }
     *     
     */
    public void setShipUnitDetail(ShipUnitDetailType value) {
        this.shipUnitDetail = value;
    }

    /**
     * Gets the value of the location property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the location property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLocation().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LocationType }
     * 
     * 
     */
    public List<LocationType> getLocation() {
        if (location == null) {
            location = new ArrayList<LocationType>();
        }
        return this.location;
    }

    /**
     * Gets the value of the packagedItem property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the packagedItem property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPackagedItem().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PackagedItemType }
     * 
     * 
     */
    public List<PackagedItemType> getPackagedItem() {
        if (packagedItem == null) {
            packagedItem = new ArrayList<PackagedItemType>();
        }
        return this.packagedItem;
    }

}
