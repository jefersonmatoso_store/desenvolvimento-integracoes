
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 *             TenderOffer is a wrapper around the Shipment element.
 *          
 * 
 * <p>Classe Java de TenderOfferType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="TenderOfferType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OTMTransactionOut">
 *       &lt;sequence>
 *         &lt;element name="Shipment" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ShipmentType"/>
 *         &lt;element name="ExpectedResponseDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="ConditionalBooking" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ConditionalBookingType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TenderOfferType", propOrder = {
    "shipment",
    "expectedResponseDt",
    "conditionalBooking"
})
public class TenderOfferType
    extends OTMTransactionOut
{

    @XmlElement(name = "Shipment", required = true)
    protected ShipmentType shipment;
    @XmlElement(name = "ExpectedResponseDt")
    protected GLogDateTimeType expectedResponseDt;
    @XmlElement(name = "ConditionalBooking")
    protected ConditionalBookingType conditionalBooking;

    /**
     * Obtém o valor da propriedade shipment.
     * 
     * @return
     *     possible object is
     *     {@link ShipmentType }
     *     
     */
    public ShipmentType getShipment() {
        return shipment;
    }

    /**
     * Define o valor da propriedade shipment.
     * 
     * @param value
     *     allowed object is
     *     {@link ShipmentType }
     *     
     */
    public void setShipment(ShipmentType value) {
        this.shipment = value;
    }

    /**
     * Obtém o valor da propriedade expectedResponseDt.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getExpectedResponseDt() {
        return expectedResponseDt;
    }

    /**
     * Define o valor da propriedade expectedResponseDt.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setExpectedResponseDt(GLogDateTimeType value) {
        this.expectedResponseDt = value;
    }

    /**
     * Obtém o valor da propriedade conditionalBooking.
     * 
     * @return
     *     possible object is
     *     {@link ConditionalBookingType }
     *     
     */
    public ConditionalBookingType getConditionalBooking() {
        return conditionalBooking;
    }

    /**
     * Define o valor da propriedade conditionalBooking.
     * 
     * @param value
     *     allowed object is
     *     {@link ConditionalBookingType }
     *     
     */
    public void setConditionalBooking(ConditionalBookingType value) {
        this.conditionalBooking = value;
    }

}
