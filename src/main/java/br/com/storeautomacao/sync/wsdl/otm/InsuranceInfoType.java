
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Specifies the insurance information.
 * 
 * <p>Classe Java de InsuranceInfoType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="InsuranceInfoType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="InsurancePolicyNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InsuranceAmount" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/>
 *         &lt;element name="InsurancePremium" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/>
 *         &lt;element name="BillOfLadingType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BillOfLadingIssuanceType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InsuranceInfoType", propOrder = {
    "insurancePolicyNumber",
    "insuranceAmount",
    "insurancePremium",
    "billOfLadingType",
    "billOfLadingIssuanceType"
})
public class InsuranceInfoType {

    @XmlElement(name = "InsurancePolicyNumber")
    protected String insurancePolicyNumber;
    @XmlElement(name = "InsuranceAmount")
    protected GLogXMLFinancialAmountType insuranceAmount;
    @XmlElement(name = "InsurancePremium")
    protected GLogXMLFinancialAmountType insurancePremium;
    @XmlElement(name = "BillOfLadingType")
    protected String billOfLadingType;
    @XmlElement(name = "BillOfLadingIssuanceType")
    protected String billOfLadingIssuanceType;

    /**
     * Obtém o valor da propriedade insurancePolicyNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInsurancePolicyNumber() {
        return insurancePolicyNumber;
    }

    /**
     * Define o valor da propriedade insurancePolicyNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInsurancePolicyNumber(String value) {
        this.insurancePolicyNumber = value;
    }

    /**
     * Obtém o valor da propriedade insuranceAmount.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getInsuranceAmount() {
        return insuranceAmount;
    }

    /**
     * Define o valor da propriedade insuranceAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setInsuranceAmount(GLogXMLFinancialAmountType value) {
        this.insuranceAmount = value;
    }

    /**
     * Obtém o valor da propriedade insurancePremium.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getInsurancePremium() {
        return insurancePremium;
    }

    /**
     * Define o valor da propriedade insurancePremium.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setInsurancePremium(GLogXMLFinancialAmountType value) {
        this.insurancePremium = value;
    }

    /**
     * Obtém o valor da propriedade billOfLadingType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillOfLadingType() {
        return billOfLadingType;
    }

    /**
     * Define o valor da propriedade billOfLadingType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillOfLadingType(String value) {
        this.billOfLadingType = value;
    }

    /**
     * Obtém o valor da propriedade billOfLadingIssuanceType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillOfLadingIssuanceType() {
        return billOfLadingIssuanceType;
    }

    /**
     * Define o valor da propriedade billOfLadingIssuanceType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillOfLadingIssuanceType(String value) {
        this.billOfLadingIssuanceType = value;
    }

}
