
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Specifies the shipment informational cost details.
 * 
 * <p>Classe Java de ShipmentInfCostDetailType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="ShipmentInfCostDetailType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SequenceNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CostType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Cost" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/>
 *         &lt;element name="AccessorialCodeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="AccessorialCostGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="RateGeoCostGroupGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="RateGeoCostSeq" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SpecialServiceGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="IsCostFixed" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ProcessAsFlowThru" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AdjustmentReasonGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="GeneralLedgerGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ShipmentInfCostDetailType", propOrder = {
    "sequenceNumber",
    "costType",
    "cost",
    "accessorialCodeGid",
    "accessorialCostGid",
    "rateGeoCostGroupGid",
    "rateGeoCostSeq",
    "specialServiceGid",
    "isCostFixed",
    "processAsFlowThru",
    "adjustmentReasonGid",
    "generalLedgerGid"
})
public class ShipmentInfCostDetailType {

    @XmlElement(name = "SequenceNumber")
    protected String sequenceNumber;
    @XmlElement(name = "CostType")
    protected String costType;
    @XmlElement(name = "Cost")
    protected GLogXMLFinancialAmountType cost;
    @XmlElement(name = "AccessorialCodeGid")
    protected GLogXMLGidType accessorialCodeGid;
    @XmlElement(name = "AccessorialCostGid")
    protected GLogXMLGidType accessorialCostGid;
    @XmlElement(name = "RateGeoCostGroupGid")
    protected GLogXMLGidType rateGeoCostGroupGid;
    @XmlElement(name = "RateGeoCostSeq")
    protected String rateGeoCostSeq;
    @XmlElement(name = "SpecialServiceGid")
    protected GLogXMLGidType specialServiceGid;
    @XmlElement(name = "IsCostFixed")
    protected String isCostFixed;
    @XmlElement(name = "ProcessAsFlowThru")
    protected String processAsFlowThru;
    @XmlElement(name = "AdjustmentReasonGid")
    protected GLogXMLGidType adjustmentReasonGid;
    @XmlElement(name = "GeneralLedgerGid")
    protected GLogXMLGidType generalLedgerGid;

    /**
     * Obtém o valor da propriedade sequenceNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSequenceNumber() {
        return sequenceNumber;
    }

    /**
     * Define o valor da propriedade sequenceNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSequenceNumber(String value) {
        this.sequenceNumber = value;
    }

    /**
     * Obtém o valor da propriedade costType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCostType() {
        return costType;
    }

    /**
     * Define o valor da propriedade costType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCostType(String value) {
        this.costType = value;
    }

    /**
     * Obtém o valor da propriedade cost.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getCost() {
        return cost;
    }

    /**
     * Define o valor da propriedade cost.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setCost(GLogXMLFinancialAmountType value) {
        this.cost = value;
    }

    /**
     * Obtém o valor da propriedade accessorialCodeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getAccessorialCodeGid() {
        return accessorialCodeGid;
    }

    /**
     * Define o valor da propriedade accessorialCodeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setAccessorialCodeGid(GLogXMLGidType value) {
        this.accessorialCodeGid = value;
    }

    /**
     * Obtém o valor da propriedade accessorialCostGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getAccessorialCostGid() {
        return accessorialCostGid;
    }

    /**
     * Define o valor da propriedade accessorialCostGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setAccessorialCostGid(GLogXMLGidType value) {
        this.accessorialCostGid = value;
    }

    /**
     * Obtém o valor da propriedade rateGeoCostGroupGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRateGeoCostGroupGid() {
        return rateGeoCostGroupGid;
    }

    /**
     * Define o valor da propriedade rateGeoCostGroupGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRateGeoCostGroupGid(GLogXMLGidType value) {
        this.rateGeoCostGroupGid = value;
    }

    /**
     * Obtém o valor da propriedade rateGeoCostSeq.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRateGeoCostSeq() {
        return rateGeoCostSeq;
    }

    /**
     * Define o valor da propriedade rateGeoCostSeq.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRateGeoCostSeq(String value) {
        this.rateGeoCostSeq = value;
    }

    /**
     * Obtém o valor da propriedade specialServiceGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getSpecialServiceGid() {
        return specialServiceGid;
    }

    /**
     * Define o valor da propriedade specialServiceGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setSpecialServiceGid(GLogXMLGidType value) {
        this.specialServiceGid = value;
    }

    /**
     * Obtém o valor da propriedade isCostFixed.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsCostFixed() {
        return isCostFixed;
    }

    /**
     * Define o valor da propriedade isCostFixed.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsCostFixed(String value) {
        this.isCostFixed = value;
    }

    /**
     * Obtém o valor da propriedade processAsFlowThru.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProcessAsFlowThru() {
        return processAsFlowThru;
    }

    /**
     * Define o valor da propriedade processAsFlowThru.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProcessAsFlowThru(String value) {
        this.processAsFlowThru = value;
    }

    /**
     * Obtém o valor da propriedade adjustmentReasonGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getAdjustmentReasonGid() {
        return adjustmentReasonGid;
    }

    /**
     * Define o valor da propriedade adjustmentReasonGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setAdjustmentReasonGid(GLogXMLGidType value) {
        this.adjustmentReasonGid = value;
    }

    /**
     * Obtém o valor da propriedade generalLedgerGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getGeneralLedgerGid() {
        return generalLedgerGid;
    }

    /**
     * Define o valor da propriedade generalLedgerGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setGeneralLedgerGid(GLogXMLGidType value) {
        this.generalLedgerGid = value;
    }

}
