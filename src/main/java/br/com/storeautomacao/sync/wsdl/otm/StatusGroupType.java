
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de StatusGroupType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="StatusGroupType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="StatusGroupGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="StatusGroupDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "StatusGroupType", propOrder = {
    "statusGroupGid",
    "statusGroupDescription"
})
public class StatusGroupType {

    @XmlElement(name = "StatusGroupGid", required = true)
    protected GLogXMLGidType statusGroupGid;
    @XmlElement(name = "StatusGroupDescription")
    protected String statusGroupDescription;

    /**
     * Obtém o valor da propriedade statusGroupGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getStatusGroupGid() {
        return statusGroupGid;
    }

    /**
     * Define o valor da propriedade statusGroupGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setStatusGroupGid(GLogXMLGidType value) {
        this.statusGroupGid = value;
    }

    /**
     * Obtém o valor da propriedade statusGroupDescription.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatusGroupDescription() {
        return statusGroupDescription;
    }

    /**
     * Define o valor da propriedade statusGroupDescription.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatusGroupDescription(String value) {
        this.statusGroupDescription = value;
    }

}
