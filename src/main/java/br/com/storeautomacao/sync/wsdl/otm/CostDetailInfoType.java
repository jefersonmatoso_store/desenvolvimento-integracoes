
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Specifies additional cost details information for the RIQ Query Result.
 * 
 * <p>Classe Java de CostDetailInfoType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="CostDetailInfoType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RateGeoCostGroupGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="RateGeoCostSeq" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RateUnitBreakGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="RATE_GEO_COST" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RATE_GEO_COST_TYPE" minOccurs="0"/>
 *         &lt;element name="RATE_GEO_COST_UNIT_BREAK" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RATE_GEO_COST_UNIT_BREAK_TYPE" minOccurs="0"/>
 *         &lt;element name="ACCESSORIAL_COST" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ACCESSORIAL_COST_TYPE" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CostDetailInfoType", propOrder = {
    "rateGeoCostGroupGid",
    "rateGeoCostSeq",
    "rateUnitBreakGid",
    "rategeocost",
    "rategeocostunitbreak",
    "accessorialcost"
})
public class CostDetailInfoType {

    @XmlElement(name = "RateGeoCostGroupGid")
    protected GLogXMLGidType rateGeoCostGroupGid;
    @XmlElement(name = "RateGeoCostSeq")
    protected String rateGeoCostSeq;
    @XmlElement(name = "RateUnitBreakGid")
    protected GLogXMLGidType rateUnitBreakGid;
    @XmlElement(name = "RATE_GEO_COST")
    protected RATEGEOCOSTTYPE rategeocost;
    @XmlElement(name = "RATE_GEO_COST_UNIT_BREAK")
    protected RATEGEOCOSTUNITBREAKTYPE rategeocostunitbreak;
    @XmlElement(name = "ACCESSORIAL_COST")
    protected ACCESSORIALCOSTTYPE accessorialcost;

    /**
     * Obtém o valor da propriedade rateGeoCostGroupGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRateGeoCostGroupGid() {
        return rateGeoCostGroupGid;
    }

    /**
     * Define o valor da propriedade rateGeoCostGroupGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRateGeoCostGroupGid(GLogXMLGidType value) {
        this.rateGeoCostGroupGid = value;
    }

    /**
     * Obtém o valor da propriedade rateGeoCostSeq.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRateGeoCostSeq() {
        return rateGeoCostSeq;
    }

    /**
     * Define o valor da propriedade rateGeoCostSeq.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRateGeoCostSeq(String value) {
        this.rateGeoCostSeq = value;
    }

    /**
     * Obtém o valor da propriedade rateUnitBreakGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRateUnitBreakGid() {
        return rateUnitBreakGid;
    }

    /**
     * Define o valor da propriedade rateUnitBreakGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRateUnitBreakGid(GLogXMLGidType value) {
        this.rateUnitBreakGid = value;
    }

    /**
     * Obtém o valor da propriedade rategeocost.
     * 
     * @return
     *     possible object is
     *     {@link RATEGEOCOSTTYPE }
     *     
     */
    public RATEGEOCOSTTYPE getRATEGEOCOST() {
        return rategeocost;
    }

    /**
     * Define o valor da propriedade rategeocost.
     * 
     * @param value
     *     allowed object is
     *     {@link RATEGEOCOSTTYPE }
     *     
     */
    public void setRATEGEOCOST(RATEGEOCOSTTYPE value) {
        this.rategeocost = value;
    }

    /**
     * Obtém o valor da propriedade rategeocostunitbreak.
     * 
     * @return
     *     possible object is
     *     {@link RATEGEOCOSTUNITBREAKTYPE }
     *     
     */
    public RATEGEOCOSTUNITBREAKTYPE getRATEGEOCOSTUNITBREAK() {
        return rategeocostunitbreak;
    }

    /**
     * Define o valor da propriedade rategeocostunitbreak.
     * 
     * @param value
     *     allowed object is
     *     {@link RATEGEOCOSTUNITBREAKTYPE }
     *     
     */
    public void setRATEGEOCOSTUNITBREAK(RATEGEOCOSTUNITBREAKTYPE value) {
        this.rategeocostunitbreak = value;
    }

    /**
     * Obtém o valor da propriedade accessorialcost.
     * 
     * @return
     *     possible object is
     *     {@link ACCESSORIALCOSTTYPE }
     *     
     */
    public ACCESSORIALCOSTTYPE getACCESSORIALCOST() {
        return accessorialcost;
    }

    /**
     * Define o valor da propriedade accessorialcost.
     * 
     * @param value
     *     allowed object is
     *     {@link ACCESSORIALCOSTTYPE }
     *     
     */
    public void setACCESSORIALCOST(ACCESSORIALCOSTTYPE value) {
        this.accessorialcost = value;
    }

}
