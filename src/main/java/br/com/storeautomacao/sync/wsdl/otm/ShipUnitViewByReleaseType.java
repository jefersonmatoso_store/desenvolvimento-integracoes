
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Provides a Release centric view of the ShipUnit(s) on the Shipment.
 * 
 * <p>Classe Java de ShipUnitViewByReleaseType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="ShipUnitViewByReleaseType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ReleaseGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="ShipUnitView" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ShipUnitViewType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ShipUnitViewByReleaseType", propOrder = {
    "releaseGid",
    "shipUnitView"
})
public class ShipUnitViewByReleaseType {

    @XmlElement(name = "ReleaseGid", required = true)
    protected GLogXMLGidType releaseGid;
    @XmlElement(name = "ShipUnitView", required = true)
    protected ShipUnitViewType shipUnitView;

    /**
     * Obtém o valor da propriedade releaseGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getReleaseGid() {
        return releaseGid;
    }

    /**
     * Define o valor da propriedade releaseGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setReleaseGid(GLogXMLGidType value) {
        this.releaseGid = value;
    }

    /**
     * Obtém o valor da propriedade shipUnitView.
     * 
     * @return
     *     possible object is
     *     {@link ShipUnitViewType }
     *     
     */
    public ShipUnitViewType getShipUnitView() {
        return shipUnitView;
    }

    /**
     * Define o valor da propriedade shipUnitView.
     * 
     * @param value
     *     allowed object is
     *     {@link ShipUnitViewType }
     *     
     */
    public void setShipUnitView(ShipUnitViewType value) {
        this.shipUnitView = value;
    }

}
