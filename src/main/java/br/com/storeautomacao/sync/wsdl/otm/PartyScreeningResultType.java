
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * (Outbound) Represents Party Screen Result
 *          
 * 
 * <p>Classe Java de PartyScreeningResultType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="PartyScreeningResultType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SequenceNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="GtmDeniedPartyGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="ScreeningStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ApprovedBy" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Comments" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}CommentsType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PartyScreeningResultType", namespace = "http://xmlns.oracle.com/apps/gtm/transmission/v6.4", propOrder = {
    "sequenceNumber",
    "gtmDeniedPartyGid",
    "screeningStatus",
    "approvedBy",
    "comments"
})
public class PartyScreeningResultType {

    @XmlElement(name = "SequenceNumber")
    protected String sequenceNumber;
    @XmlElement(name = "GtmDeniedPartyGid")
    protected GLogXMLGidType gtmDeniedPartyGid;
    @XmlElement(name = "ScreeningStatus")
    protected String screeningStatus;
    @XmlElement(name = "ApprovedBy")
    protected String approvedBy;
    @XmlElement(name = "Comments")
    protected List<CommentsType> comments;

    /**
     * Obtém o valor da propriedade sequenceNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSequenceNumber() {
        return sequenceNumber;
    }

    /**
     * Define o valor da propriedade sequenceNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSequenceNumber(String value) {
        this.sequenceNumber = value;
    }

    /**
     * Obtém o valor da propriedade gtmDeniedPartyGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getGtmDeniedPartyGid() {
        return gtmDeniedPartyGid;
    }

    /**
     * Define o valor da propriedade gtmDeniedPartyGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setGtmDeniedPartyGid(GLogXMLGidType value) {
        this.gtmDeniedPartyGid = value;
    }

    /**
     * Obtém o valor da propriedade screeningStatus.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getScreeningStatus() {
        return screeningStatus;
    }

    /**
     * Define o valor da propriedade screeningStatus.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setScreeningStatus(String value) {
        this.screeningStatus = value;
    }

    /**
     * Obtém o valor da propriedade approvedBy.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApprovedBy() {
        return approvedBy;
    }

    /**
     * Define o valor da propriedade approvedBy.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApprovedBy(String value) {
        this.approvedBy = value;
    }

    /**
     * Gets the value of the comments property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the comments property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getComments().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CommentsType }
     * 
     * 
     */
    public List<CommentsType> getComments() {
        if (comments == null) {
            comments = new ArrayList<CommentsType>();
        }
        return this.comments;
    }

}
