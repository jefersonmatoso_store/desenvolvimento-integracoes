
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * 
 *             An example would be to initiate a Bulk Plan through the Integration interface after all the TransOrders
 *             have been loaded.
 *          
 * 
 * <p>Classe Java de TopicType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="TopicType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OTMTransactionIn">
 *       &lt;sequence>
 *         &lt;element name="TopicAliasName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="TopicArg" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="TopicArgName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="TopicArgValue" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TopicType", propOrder = {
    "topicAliasName",
    "topicArg"
})
public class TopicType
    extends OTMTransactionIn
{

    @XmlElement(name = "TopicAliasName", required = true)
    protected String topicAliasName;
    @XmlElement(name = "TopicArg")
    protected List<TopicArg> topicArg;

    /**
     * Obtém o valor da propriedade topicAliasName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTopicAliasName() {
        return topicAliasName;
    }

    /**
     * Define o valor da propriedade topicAliasName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTopicAliasName(String value) {
        this.topicAliasName = value;
    }

    /**
     * Gets the value of the topicArg property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the topicArg property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTopicArg().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TopicArg }
     * 
     * 
     */
    public List<TopicArg> getTopicArg() {
        if (topicArg == null) {
            topicArg = new ArrayList<TopicArg>();
        }
        return this.topicArg;
    }


    /**
     * <p>Classe Java de anonymous complex type.
     * 
     * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="TopicArgName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="TopicArgValue" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "topicArgName",
        "topicArgValue"
    })
    public static class TopicArg {

        @XmlElement(name = "TopicArgName", required = true)
        protected String topicArgName;
        @XmlElement(name = "TopicArgValue", required = true)
        protected String topicArgValue;

        /**
         * Obtém o valor da propriedade topicArgName.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTopicArgName() {
            return topicArgName;
        }

        /**
         * Define o valor da propriedade topicArgName.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTopicArgName(String value) {
            this.topicArgName = value;
        }

        /**
         * Obtém o valor da propriedade topicArgValue.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTopicArgValue() {
            return topicArgValue;
        }

        /**
         * Define o valor da propriedade topicArgValue.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTopicArgValue(String value) {
            this.topicArgValue = value;
        }

    }

}
