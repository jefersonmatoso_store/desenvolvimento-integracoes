
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Letter of credit information.
 * 
 * <p>Classe Java de LetterOfCreditInfoType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="LetterOfCreditInfoType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="LetterOfCreditNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsLetterOfCreditRequired" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IssueDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="ExpireDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="ReceivedDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="LatestShippingDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="NegotiatedDays" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LetterOfCreditAmount" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/>
 *         &lt;element name="AdvisingNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ConfirmingNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsLetterOfCreditStale" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LetterOfCreditInfoType", propOrder = {
    "letterOfCreditNum",
    "isLetterOfCreditRequired",
    "issueDt",
    "expireDt",
    "receivedDt",
    "latestShippingDt",
    "negotiatedDays",
    "letterOfCreditAmount",
    "advisingNumber",
    "confirmingNumber",
    "isLetterOfCreditStale"
})
public class LetterOfCreditInfoType {

    @XmlElement(name = "LetterOfCreditNum")
    protected String letterOfCreditNum;
    @XmlElement(name = "IsLetterOfCreditRequired")
    protected String isLetterOfCreditRequired;
    @XmlElement(name = "IssueDt")
    protected GLogDateTimeType issueDt;
    @XmlElement(name = "ExpireDt")
    protected GLogDateTimeType expireDt;
    @XmlElement(name = "ReceivedDt")
    protected GLogDateTimeType receivedDt;
    @XmlElement(name = "LatestShippingDt")
    protected GLogDateTimeType latestShippingDt;
    @XmlElement(name = "NegotiatedDays")
    protected String negotiatedDays;
    @XmlElement(name = "LetterOfCreditAmount")
    protected GLogXMLFinancialAmountType letterOfCreditAmount;
    @XmlElement(name = "AdvisingNumber")
    protected String advisingNumber;
    @XmlElement(name = "ConfirmingNumber")
    protected String confirmingNumber;
    @XmlElement(name = "IsLetterOfCreditStale")
    protected String isLetterOfCreditStale;

    /**
     * Obtém o valor da propriedade letterOfCreditNum.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLetterOfCreditNum() {
        return letterOfCreditNum;
    }

    /**
     * Define o valor da propriedade letterOfCreditNum.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLetterOfCreditNum(String value) {
        this.letterOfCreditNum = value;
    }

    /**
     * Obtém o valor da propriedade isLetterOfCreditRequired.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsLetterOfCreditRequired() {
        return isLetterOfCreditRequired;
    }

    /**
     * Define o valor da propriedade isLetterOfCreditRequired.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsLetterOfCreditRequired(String value) {
        this.isLetterOfCreditRequired = value;
    }

    /**
     * Obtém o valor da propriedade issueDt.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getIssueDt() {
        return issueDt;
    }

    /**
     * Define o valor da propriedade issueDt.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setIssueDt(GLogDateTimeType value) {
        this.issueDt = value;
    }

    /**
     * Obtém o valor da propriedade expireDt.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getExpireDt() {
        return expireDt;
    }

    /**
     * Define o valor da propriedade expireDt.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setExpireDt(GLogDateTimeType value) {
        this.expireDt = value;
    }

    /**
     * Obtém o valor da propriedade receivedDt.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getReceivedDt() {
        return receivedDt;
    }

    /**
     * Define o valor da propriedade receivedDt.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setReceivedDt(GLogDateTimeType value) {
        this.receivedDt = value;
    }

    /**
     * Obtém o valor da propriedade latestShippingDt.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getLatestShippingDt() {
        return latestShippingDt;
    }

    /**
     * Define o valor da propriedade latestShippingDt.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setLatestShippingDt(GLogDateTimeType value) {
        this.latestShippingDt = value;
    }

    /**
     * Obtém o valor da propriedade negotiatedDays.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNegotiatedDays() {
        return negotiatedDays;
    }

    /**
     * Define o valor da propriedade negotiatedDays.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNegotiatedDays(String value) {
        this.negotiatedDays = value;
    }

    /**
     * Obtém o valor da propriedade letterOfCreditAmount.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getLetterOfCreditAmount() {
        return letterOfCreditAmount;
    }

    /**
     * Define o valor da propriedade letterOfCreditAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setLetterOfCreditAmount(GLogXMLFinancialAmountType value) {
        this.letterOfCreditAmount = value;
    }

    /**
     * Obtém o valor da propriedade advisingNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAdvisingNumber() {
        return advisingNumber;
    }

    /**
     * Define o valor da propriedade advisingNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAdvisingNumber(String value) {
        this.advisingNumber = value;
    }

    /**
     * Obtém o valor da propriedade confirmingNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConfirmingNumber() {
        return confirmingNumber;
    }

    /**
     * Define o valor da propriedade confirmingNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConfirmingNumber(String value) {
        this.confirmingNumber = value;
    }

    /**
     * Obtém o valor da propriedade isLetterOfCreditStale.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsLetterOfCreditStale() {
        return isLetterOfCreditStale;
    }

    /**
     * Define o valor da propriedade isLetterOfCreditStale.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsLetterOfCreditStale(String value) {
        this.isLetterOfCreditStale = value;
    }

}
