
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Specifies the information for the inner pack.
 * 
 * <p>Classe Java de InnerPackInfoType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="InnerPackInfoType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PackagedItemSpecRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLShipUnitSpecRefType" minOccurs="0"/>
 *         &lt;element name="InnerPackSize" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFlexQuantityType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InnerPackInfoType", propOrder = {
    "packagedItemSpecRef",
    "innerPackSize"
})
public class InnerPackInfoType {

    @XmlElement(name = "PackagedItemSpecRef")
    protected GLogXMLShipUnitSpecRefType packagedItemSpecRef;
    @XmlElement(name = "InnerPackSize")
    protected GLogXMLFlexQuantityType innerPackSize;

    /**
     * Obtém o valor da propriedade packagedItemSpecRef.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLShipUnitSpecRefType }
     *     
     */
    public GLogXMLShipUnitSpecRefType getPackagedItemSpecRef() {
        return packagedItemSpecRef;
    }

    /**
     * Define o valor da propriedade packagedItemSpecRef.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLShipUnitSpecRefType }
     *     
     */
    public void setPackagedItemSpecRef(GLogXMLShipUnitSpecRefType value) {
        this.packagedItemSpecRef = value;
    }

    /**
     * Obtém o valor da propriedade innerPackSize.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFlexQuantityType }
     *     
     */
    public GLogXMLFlexQuantityType getInnerPackSize() {
        return innerPackSize;
    }

    /**
     * Define o valor da propriedade innerPackSize.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFlexQuantityType }
     *     
     */
    public void setInnerPackSize(GLogXMLFlexQuantityType value) {
        this.innerPackSize = value;
    }

}
