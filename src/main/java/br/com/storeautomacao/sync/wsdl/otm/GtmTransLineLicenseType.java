
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * (Outbound) License Line assigned to line item.
 *          
 * 
 * <p>Classe Java de GtmTransLineLicenseType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="GtmTransLineLicenseType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SequenceNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LicenseLineGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="LicenseNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Quantity" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}QuantityType" minOccurs="0"/>
 *         &lt;element name="Currency" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}CurrencyType" minOccurs="0"/>
 *         &lt;element name="GlUserGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="LicenseAssignedDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="ControlTypeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="ControlCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LicenseExpirationDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="LicenseCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GtmTransLineLicenseType", namespace = "http://xmlns.oracle.com/apps/gtm/transmission/v6.4", propOrder = {
    "sequenceNumber",
    "licenseLineGid",
    "licenseNumber",
    "quantity",
    "currency",
    "glUserGid",
    "licenseAssignedDate",
    "controlTypeGid",
    "controlCode",
    "licenseExpirationDate",
    "licenseCode"
})
public class GtmTransLineLicenseType {

    @XmlElement(name = "SequenceNumber")
    protected String sequenceNumber;
    @XmlElement(name = "LicenseLineGid", required = true)
    protected GLogXMLGidType licenseLineGid;
    @XmlElement(name = "LicenseNumber", required = true)
    protected String licenseNumber;
    @XmlElement(name = "Quantity")
    protected QuantityType quantity;
    @XmlElement(name = "Currency")
    protected CurrencyType currency;
    @XmlElement(name = "GlUserGid")
    protected GLogXMLGidType glUserGid;
    @XmlElement(name = "LicenseAssignedDate")
    protected GLogDateTimeType licenseAssignedDate;
    @XmlElement(name = "ControlTypeGid")
    protected GLogXMLGidType controlTypeGid;
    @XmlElement(name = "ControlCode")
    protected String controlCode;
    @XmlElement(name = "LicenseExpirationDate")
    protected GLogDateTimeType licenseExpirationDate;
    @XmlElement(name = "LicenseCode", required = true)
    protected String licenseCode;

    /**
     * Obtém o valor da propriedade sequenceNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSequenceNumber() {
        return sequenceNumber;
    }

    /**
     * Define o valor da propriedade sequenceNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSequenceNumber(String value) {
        this.sequenceNumber = value;
    }

    /**
     * Obtém o valor da propriedade licenseLineGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getLicenseLineGid() {
        return licenseLineGid;
    }

    /**
     * Define o valor da propriedade licenseLineGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setLicenseLineGid(GLogXMLGidType value) {
        this.licenseLineGid = value;
    }

    /**
     * Obtém o valor da propriedade licenseNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLicenseNumber() {
        return licenseNumber;
    }

    /**
     * Define o valor da propriedade licenseNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLicenseNumber(String value) {
        this.licenseNumber = value;
    }

    /**
     * Obtém o valor da propriedade quantity.
     * 
     * @return
     *     possible object is
     *     {@link QuantityType }
     *     
     */
    public QuantityType getQuantity() {
        return quantity;
    }

    /**
     * Define o valor da propriedade quantity.
     * 
     * @param value
     *     allowed object is
     *     {@link QuantityType }
     *     
     */
    public void setQuantity(QuantityType value) {
        this.quantity = value;
    }

    /**
     * Obtém o valor da propriedade currency.
     * 
     * @return
     *     possible object is
     *     {@link CurrencyType }
     *     
     */
    public CurrencyType getCurrency() {
        return currency;
    }

    /**
     * Define o valor da propriedade currency.
     * 
     * @param value
     *     allowed object is
     *     {@link CurrencyType }
     *     
     */
    public void setCurrency(CurrencyType value) {
        this.currency = value;
    }

    /**
     * Obtém o valor da propriedade glUserGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getGlUserGid() {
        return glUserGid;
    }

    /**
     * Define o valor da propriedade glUserGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setGlUserGid(GLogXMLGidType value) {
        this.glUserGid = value;
    }

    /**
     * Obtém o valor da propriedade licenseAssignedDate.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getLicenseAssignedDate() {
        return licenseAssignedDate;
    }

    /**
     * Define o valor da propriedade licenseAssignedDate.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setLicenseAssignedDate(GLogDateTimeType value) {
        this.licenseAssignedDate = value;
    }

    /**
     * Obtém o valor da propriedade controlTypeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getControlTypeGid() {
        return controlTypeGid;
    }

    /**
     * Define o valor da propriedade controlTypeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setControlTypeGid(GLogXMLGidType value) {
        this.controlTypeGid = value;
    }

    /**
     * Obtém o valor da propriedade controlCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getControlCode() {
        return controlCode;
    }

    /**
     * Define o valor da propriedade controlCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setControlCode(String value) {
        this.controlCode = value;
    }

    /**
     * Obtém o valor da propriedade licenseExpirationDate.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getLicenseExpirationDate() {
        return licenseExpirationDate;
    }

    /**
     * Define o valor da propriedade licenseExpirationDate.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setLicenseExpirationDate(GLogDateTimeType value) {
        this.licenseExpirationDate = value;
    }

    /**
     * Obtém o valor da propriedade licenseCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLicenseCode() {
        return licenseCode;
    }

    /**
     * Define o valor da propriedade licenseCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLicenseCode(String value) {
        this.licenseCode = value;
    }

}
