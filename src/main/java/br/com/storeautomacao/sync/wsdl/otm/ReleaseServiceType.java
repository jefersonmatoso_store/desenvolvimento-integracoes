
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Specifies the services for the order release.
 * 
 * <p>Classe Java de ReleaseServiceType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="ReleaseServiceType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CustomerServiceGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="ExecutedDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="IsPriorityOverwrite" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReleaseServiceType", propOrder = {
    "customerServiceGid",
    "executedDt",
    "isPriorityOverwrite"
})
public class ReleaseServiceType {

    @XmlElement(name = "CustomerServiceGid", required = true)
    protected GLogXMLGidType customerServiceGid;
    @XmlElement(name = "ExecutedDt")
    protected GLogDateTimeType executedDt;
    @XmlElement(name = "IsPriorityOverwrite")
    protected String isPriorityOverwrite;

    /**
     * Obtém o valor da propriedade customerServiceGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getCustomerServiceGid() {
        return customerServiceGid;
    }

    /**
     * Define o valor da propriedade customerServiceGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setCustomerServiceGid(GLogXMLGidType value) {
        this.customerServiceGid = value;
    }

    /**
     * Obtém o valor da propriedade executedDt.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getExecutedDt() {
        return executedDt;
    }

    /**
     * Define o valor da propriedade executedDt.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setExecutedDt(GLogDateTimeType value) {
        this.executedDt = value;
    }

    /**
     * Obtém o valor da propriedade isPriorityOverwrite.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsPriorityOverwrite() {
        return isPriorityOverwrite;
    }

    /**
     * Define o valor da propriedade isPriorityOverwrite.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsPriorityOverwrite(String value) {
        this.isPriorityOverwrite = value;
    }

}
