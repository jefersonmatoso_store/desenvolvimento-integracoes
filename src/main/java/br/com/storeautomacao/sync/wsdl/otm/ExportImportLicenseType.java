
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 *             Used to specify export and import licenses.
 *          
 * 
 * <p>Classe Java de ExportImportLicenseType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="ExportImportLicenseType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ExportLicense" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ExportLicenseType" minOccurs="0"/>
 *         &lt;element name="ImportLicense" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ImportLicenseType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ExportImportLicenseType", propOrder = {
    "exportLicense",
    "importLicense"
})
public class ExportImportLicenseType {

    @XmlElement(name = "ExportLicense")
    protected ExportLicenseType exportLicense;
    @XmlElement(name = "ImportLicense")
    protected ImportLicenseType importLicense;

    /**
     * Obtém o valor da propriedade exportLicense.
     * 
     * @return
     *     possible object is
     *     {@link ExportLicenseType }
     *     
     */
    public ExportLicenseType getExportLicense() {
        return exportLicense;
    }

    /**
     * Define o valor da propriedade exportLicense.
     * 
     * @param value
     *     allowed object is
     *     {@link ExportLicenseType }
     *     
     */
    public void setExportLicense(ExportLicenseType value) {
        this.exportLicense = value;
    }

    /**
     * Obtém o valor da propriedade importLicense.
     * 
     * @return
     *     possible object is
     *     {@link ImportLicenseType }
     *     
     */
    public ImportLicenseType getImportLicense() {
        return importLicense;
    }

    /**
     * Define o valor da propriedade importLicense.
     * 
     * @param value
     *     allowed object is
     *     {@link ImportLicenseType }
     *     
     */
    public void setImportLicense(ImportLicenseType value) {
        this.importLicense = value;
    }

}
