
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de GLogXMLCostPerUOM complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="GLogXMLCostPerUOM">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CostPerWeightVolumeUOM" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}CostPerWeightVolumeUOMType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GLogXMLCostPerUOM", propOrder = {
    "costPerWeightVolumeUOM"
})
public class GLogXMLCostPerUOM {

    @XmlElement(name = "CostPerWeightVolumeUOM", required = true)
    protected CostPerWeightVolumeUOMType costPerWeightVolumeUOM;

    /**
     * Obtém o valor da propriedade costPerWeightVolumeUOM.
     * 
     * @return
     *     possible object is
     *     {@link CostPerWeightVolumeUOMType }
     *     
     */
    public CostPerWeightVolumeUOMType getCostPerWeightVolumeUOM() {
        return costPerWeightVolumeUOM;
    }

    /**
     * Define o valor da propriedade costPerWeightVolumeUOM.
     * 
     * @param value
     *     allowed object is
     *     {@link CostPerWeightVolumeUOMType }
     *     
     */
    public void setCostPerWeightVolumeUOM(CostPerWeightVolumeUOMType value) {
        this.costPerWeightVolumeUOM = value;
    }

}
