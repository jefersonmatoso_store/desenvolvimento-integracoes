
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * cost details of bulkplan assignment
 * 
 * <p>Classe Java de FleetBulkPlanCostType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="FleetBulkPlanCostType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CostCategoryGid" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PreCost" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/>
 *         &lt;element name="AfterCost" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FleetBulkPlanCostType", propOrder = {
    "costCategoryGid",
    "preCost",
    "afterCost"
})
public class FleetBulkPlanCostType {

    @XmlElement(name = "CostCategoryGid", required = true)
    protected String costCategoryGid;
    @XmlElement(name = "PreCost")
    protected GLogXMLFinancialAmountType preCost;
    @XmlElement(name = "AfterCost")
    protected GLogXMLFinancialAmountType afterCost;

    /**
     * Obtém o valor da propriedade costCategoryGid.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCostCategoryGid() {
        return costCategoryGid;
    }

    /**
     * Define o valor da propriedade costCategoryGid.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCostCategoryGid(String value) {
        this.costCategoryGid = value;
    }

    /**
     * Obtém o valor da propriedade preCost.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getPreCost() {
        return preCost;
    }

    /**
     * Define o valor da propriedade preCost.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setPreCost(GLogXMLFinancialAmountType value) {
        this.preCost = value;
    }

    /**
     * Obtém o valor da propriedade afterCost.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getAfterCost() {
        return afterCost;
    }

    /**
     * Define o valor da propriedade afterCost.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setAfterCost(GLogXMLFinancialAmountType value) {
        this.afterCost = value;
    }

}
