
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Demurrage Transaction Note.
 * 
 * <p>Classe Java de DemurrageTransactionNoteType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="DemurrageTransactionNoteType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DmTransactionSummary" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DmTransactionNote" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsSystemGenerated" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DemurrageTransactionNoteType", propOrder = {
    "dmTransactionSummary",
    "dmTransactionNote",
    "isSystemGenerated"
})
public class DemurrageTransactionNoteType {

    @XmlElement(name = "DmTransactionSummary")
    protected String dmTransactionSummary;
    @XmlElement(name = "DmTransactionNote")
    protected String dmTransactionNote;
    @XmlElement(name = "IsSystemGenerated")
    protected String isSystemGenerated;

    /**
     * Obtém o valor da propriedade dmTransactionSummary.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDmTransactionSummary() {
        return dmTransactionSummary;
    }

    /**
     * Define o valor da propriedade dmTransactionSummary.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDmTransactionSummary(String value) {
        this.dmTransactionSummary = value;
    }

    /**
     * Obtém o valor da propriedade dmTransactionNote.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDmTransactionNote() {
        return dmTransactionNote;
    }

    /**
     * Define o valor da propriedade dmTransactionNote.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDmTransactionNote(String value) {
        this.dmTransactionNote = value;
    }

    /**
     * Obtém o valor da propriedade isSystemGenerated.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsSystemGenerated() {
        return isSystemGenerated;
    }

    /**
     * Define o valor da propriedade isSystemGenerated.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsSystemGenerated(String value) {
        this.isSystemGenerated = value;
    }

}
