
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 *             ShipUnitRefnum is an alternate method for identifying an ShipUnit in a Shipment. It consists of a qualifier and a value.
 *          
 * 
 * <p>Classe Java de ShipUnitRefnumType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="ShipUnitRefnumType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ShipUnitRefnumQualifierGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="ShipUnitRefnumValue" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ShipUnitRefnumType", propOrder = {
    "shipUnitRefnumQualifierGid",
    "shipUnitRefnumValue"
})
public class ShipUnitRefnumType {

    @XmlElement(name = "ShipUnitRefnumQualifierGid", required = true)
    protected GLogXMLGidType shipUnitRefnumQualifierGid;
    @XmlElement(name = "ShipUnitRefnumValue", required = true)
    protected String shipUnitRefnumValue;

    /**
     * Obtém o valor da propriedade shipUnitRefnumQualifierGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getShipUnitRefnumQualifierGid() {
        return shipUnitRefnumQualifierGid;
    }

    /**
     * Define o valor da propriedade shipUnitRefnumQualifierGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setShipUnitRefnumQualifierGid(GLogXMLGidType value) {
        this.shipUnitRefnumQualifierGid = value;
    }

    /**
     * Obtém o valor da propriedade shipUnitRefnumValue.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipUnitRefnumValue() {
        return shipUnitRefnumValue;
    }

    /**
     * Define o valor da propriedade shipUnitRefnumValue.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipUnitRefnumValue(String value) {
        this.shipUnitRefnumValue = value;
    }

}
