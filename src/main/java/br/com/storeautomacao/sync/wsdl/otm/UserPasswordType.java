
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 *             User password specification for insert or update.
 *          
 * 
 * <p>Classe Java de UserPasswordType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="UserPasswordType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="OldPassword" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGlPassword" minOccurs="0"/>
 *         &lt;element name="NewPassword" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGlPassword"/>
 *         &lt;element name="NewPasswordExpirationDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserPasswordType", propOrder = {
    "oldPassword",
    "newPassword",
    "newPasswordExpirationDate"
})
public class UserPasswordType {

    @XmlElement(name = "OldPassword")
    protected GLogXMLGlPassword oldPassword;
    @XmlElement(name = "NewPassword", required = true)
    protected GLogXMLGlPassword newPassword;
    @XmlElement(name = "NewPasswordExpirationDate")
    protected GLogDateTimeType newPasswordExpirationDate;

    /**
     * Obtém o valor da propriedade oldPassword.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGlPassword }
     *     
     */
    public GLogXMLGlPassword getOldPassword() {
        return oldPassword;
    }

    /**
     * Define o valor da propriedade oldPassword.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGlPassword }
     *     
     */
    public void setOldPassword(GLogXMLGlPassword value) {
        this.oldPassword = value;
    }

    /**
     * Obtém o valor da propriedade newPassword.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGlPassword }
     *     
     */
    public GLogXMLGlPassword getNewPassword() {
        return newPassword;
    }

    /**
     * Define o valor da propriedade newPassword.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGlPassword }
     *     
     */
    public void setNewPassword(GLogXMLGlPassword value) {
        this.newPassword = value;
    }

    /**
     * Obtém o valor da propriedade newPasswordExpirationDate.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getNewPasswordExpirationDate() {
        return newPasswordExpirationDate;
    }

    /**
     * Define o valor da propriedade newPasswordExpirationDate.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setNewPasswordExpirationDate(GLogDateTimeType value) {
        this.newPasswordExpirationDate = value;
    }

}
