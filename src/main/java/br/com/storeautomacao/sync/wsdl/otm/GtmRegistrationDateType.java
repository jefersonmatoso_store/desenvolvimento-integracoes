
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * (Both) This tag will hold dates other then EFFECTIVE_DATE ,EXPIRATION_DATE ,REQUESTED_DATE
 *             ,APPROVAL_DATE ,RENEWAL_DATE.  
 *          
 * 
 * <p>Classe Java de GtmRegistrationDateType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="GtmRegistrationDateType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GtmRegistrationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="DateQualifierGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="RegistrationDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GtmRegistrationDateType", namespace = "http://xmlns.oracle.com/apps/gtm/transmission/v6.4", propOrder = {
    "gtmRegistrationGid",
    "dateQualifierGid",
    "registrationDate"
})
public class GtmRegistrationDateType {

    @XmlElement(name = "GtmRegistrationGid", required = true)
    protected GLogXMLGidType gtmRegistrationGid;
    @XmlElement(name = "DateQualifierGid", required = true)
    protected GLogXMLGidType dateQualifierGid;
    @XmlElement(name = "RegistrationDate")
    protected GLogDateTimeType registrationDate;

    /**
     * Obtém o valor da propriedade gtmRegistrationGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getGtmRegistrationGid() {
        return gtmRegistrationGid;
    }

    /**
     * Define o valor da propriedade gtmRegistrationGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setGtmRegistrationGid(GLogXMLGidType value) {
        this.gtmRegistrationGid = value;
    }

    /**
     * Obtém o valor da propriedade dateQualifierGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getDateQualifierGid() {
        return dateQualifierGid;
    }

    /**
     * Define o valor da propriedade dateQualifierGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setDateQualifierGid(GLogXMLGidType value) {
        this.dateQualifierGid = value;
    }

    /**
     * Obtém o valor da propriedade registrationDate.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getRegistrationDate() {
        return registrationDate;
    }

    /**
     * Define o valor da propriedade registrationDate.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setRegistrationDate(GLogDateTimeType value) {
        this.registrationDate = value;
    }

}
