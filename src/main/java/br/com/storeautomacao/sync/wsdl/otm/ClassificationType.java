
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * (Inbound) Classification is the lookup classification request for the item description sent via the ServiceRequest interface.
 *          
 * 
 * <p>Classe Java de ClassificationType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="ClassificationType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ClassificationCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CountryName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Language" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ProdClassType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ClassificationCategory" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ClassificationType", namespace = "http://xmlns.oracle.com/apps/gtm/transmission/v6.4", propOrder = {
    "classificationCode",
    "description",
    "countryName",
    "language",
    "prodClassType",
    "classificationCategory"
})
public class ClassificationType {

    @XmlElement(name = "ClassificationCode")
    protected String classificationCode;
    @XmlElement(name = "Description")
    protected String description;
    @XmlElement(name = "CountryName")
    protected String countryName;
    @XmlElement(name = "Language")
    protected String language;
    @XmlElement(name = "ProdClassType", required = true)
    protected String prodClassType;
    @XmlElement(name = "ClassificationCategory")
    protected String classificationCategory;

    /**
     * Obtém o valor da propriedade classificationCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClassificationCode() {
        return classificationCode;
    }

    /**
     * Define o valor da propriedade classificationCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClassificationCode(String value) {
        this.classificationCode = value;
    }

    /**
     * Obtém o valor da propriedade description.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Define o valor da propriedade description.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Obtém o valor da propriedade countryName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCountryName() {
        return countryName;
    }

    /**
     * Define o valor da propriedade countryName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCountryName(String value) {
        this.countryName = value;
    }

    /**
     * Obtém o valor da propriedade language.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLanguage() {
        return language;
    }

    /**
     * Define o valor da propriedade language.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLanguage(String value) {
        this.language = value;
    }

    /**
     * Obtém o valor da propriedade prodClassType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProdClassType() {
        return prodClassType;
    }

    /**
     * Define o valor da propriedade prodClassType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProdClassType(String value) {
        this.prodClassType = value;
    }

    /**
     * Obtém o valor da propriedade classificationCategory.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClassificationCategory() {
        return classificationCategory;
    }

    /**
     * Define o valor da propriedade classificationCategory.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClassificationCategory(String value) {
        this.classificationCategory = value;
    }

}
