
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * An OrderRefnum is an alternate way of referring to a TransOrder.
 * 
 * <p>Classe Java de OrderRefnumType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="OrderRefnumType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="OrderRefnumQualifierGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="OrderRefnumValue" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OrderRefnumType", propOrder = {
    "orderRefnumQualifierGid",
    "orderRefnumValue"
})
public class OrderRefnumType {

    @XmlElement(name = "OrderRefnumQualifierGid", required = true)
    protected GLogXMLGidType orderRefnumQualifierGid;
    @XmlElement(name = "OrderRefnumValue", required = true)
    protected String orderRefnumValue;

    /**
     * Obtém o valor da propriedade orderRefnumQualifierGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getOrderRefnumQualifierGid() {
        return orderRefnumQualifierGid;
    }

    /**
     * Define o valor da propriedade orderRefnumQualifierGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setOrderRefnumQualifierGid(GLogXMLGidType value) {
        this.orderRefnumQualifierGid = value;
    }

    /**
     * Obtém o valor da propriedade orderRefnumValue.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrderRefnumValue() {
        return orderRefnumValue;
    }

    /**
     * Define o valor da propriedade orderRefnumValue.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrderRefnumValue(String value) {
        this.orderRefnumValue = value;
    }

}
