
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * 
 *             Contains reference information from the Release for the Accrual.
 *          
 * 
 * <p>Classe Java de AccrualReleaseInfoType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="AccrualReleaseInfoType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="EarlyPickupDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="ReleaseRefnum" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ReleaseRefnumType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AccrualReleaseInfoType", propOrder = {
    "earlyPickupDt",
    "releaseRefnum"
})
public class AccrualReleaseInfoType {

    @XmlElement(name = "EarlyPickupDt")
    protected GLogDateTimeType earlyPickupDt;
    @XmlElement(name = "ReleaseRefnum")
    protected List<ReleaseRefnumType> releaseRefnum;

    /**
     * Obtém o valor da propriedade earlyPickupDt.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getEarlyPickupDt() {
        return earlyPickupDt;
    }

    /**
     * Define o valor da propriedade earlyPickupDt.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setEarlyPickupDt(GLogDateTimeType value) {
        this.earlyPickupDt = value;
    }

    /**
     * Gets the value of the releaseRefnum property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the releaseRefnum property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReleaseRefnum().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ReleaseRefnumType }
     * 
     * 
     */
    public List<ReleaseRefnumType> getReleaseRefnum() {
        if (releaseRefnum == null) {
            releaseRefnum = new ArrayList<ReleaseRefnumType>();
        }
        return this.releaseRefnum;
    }

}
