
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Holds item specific uom-to-uom converstion rates.
 * 
 * <p>Classe Java de GtmItemUomConversionType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="GtmItemUomConversionType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SequenceNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="GtmProdClassTypeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="FromUomCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ToUomCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ConversionRate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GtmItemUomConversionType", propOrder = {
    "sequenceNumber",
    "gtmProdClassTypeGid",
    "fromUomCode",
    "toUomCode",
    "conversionRate"
})
public class GtmItemUomConversionType {

    @XmlElement(name = "SequenceNumber", required = true)
    protected String sequenceNumber;
    @XmlElement(name = "GtmProdClassTypeGid", required = true)
    protected GLogXMLGidType gtmProdClassTypeGid;
    @XmlElement(name = "FromUomCode", required = true)
    protected String fromUomCode;
    @XmlElement(name = "ToUomCode", required = true)
    protected String toUomCode;
    @XmlElement(name = "ConversionRate", required = true)
    protected String conversionRate;

    /**
     * Obtém o valor da propriedade sequenceNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSequenceNumber() {
        return sequenceNumber;
    }

    /**
     * Define o valor da propriedade sequenceNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSequenceNumber(String value) {
        this.sequenceNumber = value;
    }

    /**
     * Obtém o valor da propriedade gtmProdClassTypeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getGtmProdClassTypeGid() {
        return gtmProdClassTypeGid;
    }

    /**
     * Define o valor da propriedade gtmProdClassTypeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setGtmProdClassTypeGid(GLogXMLGidType value) {
        this.gtmProdClassTypeGid = value;
    }

    /**
     * Obtém o valor da propriedade fromUomCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFromUomCode() {
        return fromUomCode;
    }

    /**
     * Define o valor da propriedade fromUomCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFromUomCode(String value) {
        this.fromUomCode = value;
    }

    /**
     * Obtém o valor da propriedade toUomCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getToUomCode() {
        return toUomCode;
    }

    /**
     * Define o valor da propriedade toUomCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setToUomCode(String value) {
        this.toUomCode = value;
    }

    /**
     * Obtém o valor da propriedade conversionRate.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConversionRate() {
        return conversionRate;
    }

    /**
     * Define o valor da propriedade conversionRate.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConversionRate(String value) {
        this.conversionRate = value;
    }

}
