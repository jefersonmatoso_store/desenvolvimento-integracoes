
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Specifies optional quantity change that is attached to the SKU event.
 * 
 * <p>Classe Java de SkuEventQuantityType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="SkuEventQuantityType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SkuQuantityTypeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="Quantity" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="QuantityUpdateType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SkuEventQuantityType", propOrder = {
    "skuQuantityTypeGid",
    "quantity",
    "quantityUpdateType"
})
public class SkuEventQuantityType {

    @XmlElement(name = "SkuQuantityTypeGid", required = true)
    protected GLogXMLGidType skuQuantityTypeGid;
    @XmlElement(name = "Quantity", required = true)
    protected String quantity;
    @XmlElement(name = "QuantityUpdateType", required = true)
    protected String quantityUpdateType;

    /**
     * Obtém o valor da propriedade skuQuantityTypeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getSkuQuantityTypeGid() {
        return skuQuantityTypeGid;
    }

    /**
     * Define o valor da propriedade skuQuantityTypeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setSkuQuantityTypeGid(GLogXMLGidType value) {
        this.skuQuantityTypeGid = value;
    }

    /**
     * Obtém o valor da propriedade quantity.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQuantity() {
        return quantity;
    }

    /**
     * Define o valor da propriedade quantity.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQuantity(String value) {
        this.quantity = value;
    }

    /**
     * Obtém o valor da propriedade quantityUpdateType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQuantityUpdateType() {
        return quantityUpdateType;
    }

    /**
     * Define o valor da propriedade quantityUpdateType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQuantityUpdateType(String value) {
        this.quantityUpdateType = value;
    }

}
