
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Volume is a group of fields for specifying volume.
 *             It includes sub-elements for specifying unit of measure and a volume value.
 *          
 * 
 * <p>Classe Java de VolumeType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="VolumeType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="VolumeValue" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="VolumeUOMGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VolumeType", propOrder = {
    "volumeValue",
    "volumeUOMGid"
})
public class VolumeType {

    @XmlElement(name = "VolumeValue", required = true)
    protected String volumeValue;
    @XmlElement(name = "VolumeUOMGid", required = true)
    protected GLogXMLGidType volumeUOMGid;

    /**
     * Obtém o valor da propriedade volumeValue.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVolumeValue() {
        return volumeValue;
    }

    /**
     * Define o valor da propriedade volumeValue.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVolumeValue(String value) {
        this.volumeValue = value;
    }

    /**
     * Obtém o valor da propriedade volumeUOMGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getVolumeUOMGid() {
        return volumeUOMGid;
    }

    /**
     * Define o valor da propriedade volumeUOMGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setVolumeUOMGid(GLogXMLGidType value) {
        this.volumeUOMGid = value;
    }

}
