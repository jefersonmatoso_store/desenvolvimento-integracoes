
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * 
 *             The Voyage is used to specify world wide vessel schedule information.
 *          
 * 
 * <p>Classe Java de VoyageType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="VoyageType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OTMTransactionOut">
 *       &lt;sequence>
 *         &lt;element name="SendReason" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}SendReasonType" minOccurs="0"/>
 *         &lt;element name="VoyageGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="ServiceProviderGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="VoyageServiceTypeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="VoyageName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DepartureRegionName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ArrivalRegionName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DepartureDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="Vessel" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}VesselType" minOccurs="0"/>
 *         &lt;element name="VoyageLoc" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}VoyageLocType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Location" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LocationType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VoyageType", propOrder = {
    "sendReason",
    "voyageGid",
    "serviceProviderGid",
    "voyageServiceTypeGid",
    "voyageName",
    "departureRegionName",
    "arrivalRegionName",
    "departureDt",
    "vessel",
    "voyageLoc",
    "location"
})
public class VoyageType
    extends OTMTransactionOut
{

    @XmlElement(name = "SendReason")
    protected SendReasonType sendReason;
    @XmlElement(name = "VoyageGid", required = true)
    protected GLogXMLGidType voyageGid;
    @XmlElement(name = "ServiceProviderGid", required = true)
    protected GLogXMLGidType serviceProviderGid;
    @XmlElement(name = "VoyageServiceTypeGid", required = true)
    protected GLogXMLGidType voyageServiceTypeGid;
    @XmlElement(name = "VoyageName")
    protected String voyageName;
    @XmlElement(name = "DepartureRegionName")
    protected String departureRegionName;
    @XmlElement(name = "ArrivalRegionName")
    protected String arrivalRegionName;
    @XmlElement(name = "DepartureDt")
    protected GLogDateTimeType departureDt;
    @XmlElement(name = "Vessel")
    protected VesselType vessel;
    @XmlElement(name = "VoyageLoc")
    protected List<VoyageLocType> voyageLoc;
    @XmlElement(name = "Location")
    protected List<LocationType> location;

    /**
     * Obtém o valor da propriedade sendReason.
     * 
     * @return
     *     possible object is
     *     {@link SendReasonType }
     *     
     */
    public SendReasonType getSendReason() {
        return sendReason;
    }

    /**
     * Define o valor da propriedade sendReason.
     * 
     * @param value
     *     allowed object is
     *     {@link SendReasonType }
     *     
     */
    public void setSendReason(SendReasonType value) {
        this.sendReason = value;
    }

    /**
     * Obtém o valor da propriedade voyageGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getVoyageGid() {
        return voyageGid;
    }

    /**
     * Define o valor da propriedade voyageGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setVoyageGid(GLogXMLGidType value) {
        this.voyageGid = value;
    }

    /**
     * Obtém o valor da propriedade serviceProviderGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getServiceProviderGid() {
        return serviceProviderGid;
    }

    /**
     * Define o valor da propriedade serviceProviderGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setServiceProviderGid(GLogXMLGidType value) {
        this.serviceProviderGid = value;
    }

    /**
     * Obtém o valor da propriedade voyageServiceTypeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getVoyageServiceTypeGid() {
        return voyageServiceTypeGid;
    }

    /**
     * Define o valor da propriedade voyageServiceTypeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setVoyageServiceTypeGid(GLogXMLGidType value) {
        this.voyageServiceTypeGid = value;
    }

    /**
     * Obtém o valor da propriedade voyageName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVoyageName() {
        return voyageName;
    }

    /**
     * Define o valor da propriedade voyageName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVoyageName(String value) {
        this.voyageName = value;
    }

    /**
     * Obtém o valor da propriedade departureRegionName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDepartureRegionName() {
        return departureRegionName;
    }

    /**
     * Define o valor da propriedade departureRegionName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDepartureRegionName(String value) {
        this.departureRegionName = value;
    }

    /**
     * Obtém o valor da propriedade arrivalRegionName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getArrivalRegionName() {
        return arrivalRegionName;
    }

    /**
     * Define o valor da propriedade arrivalRegionName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setArrivalRegionName(String value) {
        this.arrivalRegionName = value;
    }

    /**
     * Obtém o valor da propriedade departureDt.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getDepartureDt() {
        return departureDt;
    }

    /**
     * Define o valor da propriedade departureDt.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setDepartureDt(GLogDateTimeType value) {
        this.departureDt = value;
    }

    /**
     * Obtém o valor da propriedade vessel.
     * 
     * @return
     *     possible object is
     *     {@link VesselType }
     *     
     */
    public VesselType getVessel() {
        return vessel;
    }

    /**
     * Define o valor da propriedade vessel.
     * 
     * @param value
     *     allowed object is
     *     {@link VesselType }
     *     
     */
    public void setVessel(VesselType value) {
        this.vessel = value;
    }

    /**
     * Gets the value of the voyageLoc property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the voyageLoc property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getVoyageLoc().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link VoyageLocType }
     * 
     * 
     */
    public List<VoyageLocType> getVoyageLoc() {
        if (voyageLoc == null) {
            voyageLoc = new ArrayList<VoyageLocType>();
        }
        return this.voyageLoc;
    }

    /**
     * Gets the value of the location property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the location property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLocation().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LocationType }
     * 
     * 
     */
    public List<LocationType> getLocation() {
        if (location == null) {
            location = new ArrayList<LocationType>();
        }
        return this.location;
    }

}
