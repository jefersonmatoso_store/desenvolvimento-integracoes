
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.*;


/**
 * 
 *             An XLane specifies a link from a source node to a destination node.
 *             The source and destination nodes may specify either vague or specific geography.
 *             For example, a source could be an exact location, or an entire state.
 *          
 * 
 * <p>Classe Java de XLaneType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="XLaneType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OTMTransactionInOut">
 *       &lt;sequence>
 *         &lt;element name="SendReason" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}SendReasonType" minOccurs="0"/>
 *         &lt;element name="XLaneGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="TransactionCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransactionCodeType" minOccurs="0"/>
 *         &lt;element name="XLaneSource" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLXLaneNodeType"/>
 *         &lt;element name="XLaneDest" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLXLaneNodeType"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "XLaneType", propOrder = {
    "sendReason",
    "xLaneGid",
    "transactionCode",
    "xLaneSource",
    "xLaneDest"
})
public class XLaneType
    extends OTMTransactionInOut
{

    @XmlElement(name = "SendReason")
    protected SendReasonType sendReason;
    @XmlElement(name = "XLaneGid", required = true)
    protected GLogXMLGidType xLaneGid;
    @XmlElement(name = "TransactionCode")
    @XmlSchemaType(name = "string")
    protected TransactionCodeType transactionCode;
    @XmlElement(name = "XLaneSource", required = true)
    protected GLogXMLXLaneNodeType xLaneSource;
    @XmlElement(name = "XLaneDest", required = true)
    protected GLogXMLXLaneNodeType xLaneDest;

    /**
     * Obtém o valor da propriedade sendReason.
     * 
     * @return
     *     possible object is
     *     {@link SendReasonType }
     *     
     */
    public SendReasonType getSendReason() {
        return sendReason;
    }

    /**
     * Define o valor da propriedade sendReason.
     * 
     * @param value
     *     allowed object is
     *     {@link SendReasonType }
     *     
     */
    public void setSendReason(SendReasonType value) {
        this.sendReason = value;
    }

    /**
     * Obtém o valor da propriedade xLaneGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getXLaneGid() {
        return xLaneGid;
    }

    /**
     * Define o valor da propriedade xLaneGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setXLaneGid(GLogXMLGidType value) {
        this.xLaneGid = value;
    }

    /**
     * Obtém o valor da propriedade transactionCode.
     * 
     * @return
     *     possible object is
     *     {@link TransactionCodeType }
     *     
     */
    public TransactionCodeType getTransactionCode() {
        return transactionCode;
    }

    /**
     * Define o valor da propriedade transactionCode.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionCodeType }
     *     
     */
    public void setTransactionCode(TransactionCodeType value) {
        this.transactionCode = value;
    }

    /**
     * Obtém o valor da propriedade xLaneSource.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLXLaneNodeType }
     *     
     */
    public GLogXMLXLaneNodeType getXLaneSource() {
        return xLaneSource;
    }

    /**
     * Define o valor da propriedade xLaneSource.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLXLaneNodeType }
     *     
     */
    public void setXLaneSource(GLogXMLXLaneNodeType value) {
        this.xLaneSource = value;
    }

    /**
     * Obtém o valor da propriedade xLaneDest.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLXLaneNodeType }
     *     
     */
    public GLogXMLXLaneNodeType getXLaneDest() {
        return xLaneDest;
    }

    /**
     * Define o valor da propriedade xLaneDest.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLXLaneNodeType }
     *     
     */
    public void setXLaneDest(GLogXMLXLaneNodeType value) {
        this.xLaneDest = value;
    }

}
