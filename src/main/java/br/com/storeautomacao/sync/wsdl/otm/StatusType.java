
package br.com.storeautomacao.sync.wsdl.otm;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Indicates the status type and value for an object (eg. Shipment, TransOrder).
 * 
 * <p>Classe Java de StatusType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="StatusType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="StatusTypeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="StatusValueGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "StatusType", propOrder = {
    "statusTypeGid",
    "statusValueGid"
})
@XStreamAlias("StatusType")
public class StatusType {

    @XmlElement(name = "StatusTypeGid", required = true)
    protected GLogXMLGidType statusTypeGid;
    @XmlElement(name = "StatusValueGid", required = true)
    protected GLogXMLGidType statusValueGid;

    /**
     * Obtém o valor da propriedade statusTypeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getStatusTypeGid() {
        return statusTypeGid;
    }

    /**
     * Define o valor da propriedade statusTypeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setStatusTypeGid(GLogXMLGidType value) {
        this.statusTypeGid = value;
    }

    /**
     * Obtém o valor da propriedade statusValueGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getStatusValueGid() {
        return statusValueGid;
    }

    /**
     * Define o valor da propriedade statusValueGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setStatusValueGid(GLogXMLGidType value) {
        this.statusValueGid = value;
    }

}
