
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de ReasonGroupType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="ReasonGroupType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ReasonGroupGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="ReasonGroupDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReasonGroupType", propOrder = {
    "reasonGroupGid",
    "reasonGroupDescription"
})
public class ReasonGroupType {

    @XmlElement(name = "ReasonGroupGid", required = true)
    protected GLogXMLGidType reasonGroupGid;
    @XmlElement(name = "ReasonGroupDescription")
    protected String reasonGroupDescription;

    /**
     * Obtém o valor da propriedade reasonGroupGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getReasonGroupGid() {
        return reasonGroupGid;
    }

    /**
     * Define o valor da propriedade reasonGroupGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setReasonGroupGid(GLogXMLGidType value) {
        this.reasonGroupGid = value;
    }

    /**
     * Obtém o valor da propriedade reasonGroupDescription.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReasonGroupDescription() {
        return reasonGroupDescription;
    }

    /**
     * Define o valor da propriedade reasonGroupDescription.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReasonGroupDescription(String value) {
        this.reasonGroupDescription = value;
    }

}
