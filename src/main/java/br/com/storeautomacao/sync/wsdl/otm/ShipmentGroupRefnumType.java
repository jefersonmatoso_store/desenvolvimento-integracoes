
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 *             ShipmentGroupRefnum is an alternate method for identifying a Shipment Group. It consists of a qualifier and a value.
 *          
 * 
 * <p>Classe Java de ShipmentGroupRefnumType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="ShipmentGroupRefnumType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ShipmentGroupRefnumQualifierGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="ShipmentGroupRefnumValue" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ShipmentGroupRefnumType", propOrder = {
    "shipmentGroupRefnumQualifierGid",
    "shipmentGroupRefnumValue"
})
public class ShipmentGroupRefnumType {

    @XmlElement(name = "ShipmentGroupRefnumQualifierGid", required = true)
    protected GLogXMLGidType shipmentGroupRefnumQualifierGid;
    @XmlElement(name = "ShipmentGroupRefnumValue", required = true)
    protected String shipmentGroupRefnumValue;

    /**
     * Obtém o valor da propriedade shipmentGroupRefnumQualifierGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getShipmentGroupRefnumQualifierGid() {
        return shipmentGroupRefnumQualifierGid;
    }

    /**
     * Define o valor da propriedade shipmentGroupRefnumQualifierGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setShipmentGroupRefnumQualifierGid(GLogXMLGidType value) {
        this.shipmentGroupRefnumQualifierGid = value;
    }

    /**
     * Obtém o valor da propriedade shipmentGroupRefnumValue.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipmentGroupRefnumValue() {
        return shipmentGroupRefnumValue;
    }

    /**
     * Define o valor da propriedade shipmentGroupRefnumValue.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipmentGroupRefnumValue(String value) {
        this.shipmentGroupRefnumValue = value;
    }

}
