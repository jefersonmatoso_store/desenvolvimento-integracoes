
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * (Outbound) Trade control screening result.
 *          
 * 
 * <p>Classe Java de ControlScreeningResultType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="ControlScreeningResultType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GtmControlTypeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="ControlCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ComplianceRuleGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="ComplianceRuleGroupGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="GtmregulationRefGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="GtmExceptionControlTypeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="GtmExceptionControlCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Authorization" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="LicenseQuantity" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="LicenseUom" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="LicenseAddedBy" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="LicenseAddedOn" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType"/>
 *         &lt;element name="InvolvedPartyQualifierGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="ControlCodeDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RegimeId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ControlScreeningResultType", namespace = "http://xmlns.oracle.com/apps/gtm/transmission/v6.4", propOrder = {
    "gtmControlTypeGid",
    "controlCode",
    "complianceRuleGid",
    "complianceRuleGroupGid",
    "gtmregulationRefGid",
    "gtmExceptionControlTypeGid",
    "gtmExceptionControlCode",
    "authorization",
    "licenseQuantity",
    "licenseUom",
    "licenseAddedBy",
    "licenseAddedOn",
    "involvedPartyQualifierGid",
    "controlCodeDescription",
    "regimeId"
})
public class ControlScreeningResultType {

    @XmlElement(name = "GtmControlTypeGid", required = true)
    protected GLogXMLGidType gtmControlTypeGid;
    @XmlElement(name = "ControlCode", required = true)
    protected String controlCode;
    @XmlElement(name = "ComplianceRuleGid", required = true)
    protected GLogXMLGidType complianceRuleGid;
    @XmlElement(name = "ComplianceRuleGroupGid", required = true)
    protected GLogXMLGidType complianceRuleGroupGid;
    @XmlElement(name = "GtmregulationRefGid", required = true)
    protected GLogXMLGidType gtmregulationRefGid;
    @XmlElement(name = "GtmExceptionControlTypeGid", required = true)
    protected GLogXMLGidType gtmExceptionControlTypeGid;
    @XmlElement(name = "GtmExceptionControlCode", required = true)
    protected String gtmExceptionControlCode;
    @XmlElement(name = "Authorization", required = true)
    protected String authorization;
    @XmlElement(name = "LicenseQuantity", required = true)
    protected String licenseQuantity;
    @XmlElement(name = "LicenseUom", required = true)
    protected String licenseUom;
    @XmlElement(name = "LicenseAddedBy", required = true)
    protected String licenseAddedBy;
    @XmlElement(name = "LicenseAddedOn", required = true)
    protected GLogDateTimeType licenseAddedOn;
    @XmlElement(name = "InvolvedPartyQualifierGid")
    protected GLogXMLGidType involvedPartyQualifierGid;
    @XmlElement(name = "ControlCodeDescription")
    protected String controlCodeDescription;
    @XmlElement(name = "RegimeId")
    protected String regimeId;

    /**
     * Obtém o valor da propriedade gtmControlTypeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getGtmControlTypeGid() {
        return gtmControlTypeGid;
    }

    /**
     * Define o valor da propriedade gtmControlTypeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setGtmControlTypeGid(GLogXMLGidType value) {
        this.gtmControlTypeGid = value;
    }

    /**
     * Obtém o valor da propriedade controlCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getControlCode() {
        return controlCode;
    }

    /**
     * Define o valor da propriedade controlCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setControlCode(String value) {
        this.controlCode = value;
    }

    /**
     * Obtém o valor da propriedade complianceRuleGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getComplianceRuleGid() {
        return complianceRuleGid;
    }

    /**
     * Define o valor da propriedade complianceRuleGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setComplianceRuleGid(GLogXMLGidType value) {
        this.complianceRuleGid = value;
    }

    /**
     * Obtém o valor da propriedade complianceRuleGroupGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getComplianceRuleGroupGid() {
        return complianceRuleGroupGid;
    }

    /**
     * Define o valor da propriedade complianceRuleGroupGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setComplianceRuleGroupGid(GLogXMLGidType value) {
        this.complianceRuleGroupGid = value;
    }

    /**
     * Obtém o valor da propriedade gtmregulationRefGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getGtmregulationRefGid() {
        return gtmregulationRefGid;
    }

    /**
     * Define o valor da propriedade gtmregulationRefGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setGtmregulationRefGid(GLogXMLGidType value) {
        this.gtmregulationRefGid = value;
    }

    /**
     * Obtém o valor da propriedade gtmExceptionControlTypeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getGtmExceptionControlTypeGid() {
        return gtmExceptionControlTypeGid;
    }

    /**
     * Define o valor da propriedade gtmExceptionControlTypeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setGtmExceptionControlTypeGid(GLogXMLGidType value) {
        this.gtmExceptionControlTypeGid = value;
    }

    /**
     * Obtém o valor da propriedade gtmExceptionControlCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGtmExceptionControlCode() {
        return gtmExceptionControlCode;
    }

    /**
     * Define o valor da propriedade gtmExceptionControlCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGtmExceptionControlCode(String value) {
        this.gtmExceptionControlCode = value;
    }

    /**
     * Obtém o valor da propriedade authorization.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAuthorization() {
        return authorization;
    }

    /**
     * Define o valor da propriedade authorization.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAuthorization(String value) {
        this.authorization = value;
    }

    /**
     * Obtém o valor da propriedade licenseQuantity.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLicenseQuantity() {
        return licenseQuantity;
    }

    /**
     * Define o valor da propriedade licenseQuantity.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLicenseQuantity(String value) {
        this.licenseQuantity = value;
    }

    /**
     * Obtém o valor da propriedade licenseUom.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLicenseUom() {
        return licenseUom;
    }

    /**
     * Define o valor da propriedade licenseUom.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLicenseUom(String value) {
        this.licenseUom = value;
    }

    /**
     * Obtém o valor da propriedade licenseAddedBy.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLicenseAddedBy() {
        return licenseAddedBy;
    }

    /**
     * Define o valor da propriedade licenseAddedBy.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLicenseAddedBy(String value) {
        this.licenseAddedBy = value;
    }

    /**
     * Obtém o valor da propriedade licenseAddedOn.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getLicenseAddedOn() {
        return licenseAddedOn;
    }

    /**
     * Define o valor da propriedade licenseAddedOn.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setLicenseAddedOn(GLogDateTimeType value) {
        this.licenseAddedOn = value;
    }

    /**
     * Obtém o valor da propriedade involvedPartyQualifierGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getInvolvedPartyQualifierGid() {
        return involvedPartyQualifierGid;
    }

    /**
     * Define o valor da propriedade involvedPartyQualifierGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setInvolvedPartyQualifierGid(GLogXMLGidType value) {
        this.involvedPartyQualifierGid = value;
    }

    /**
     * Obtém o valor da propriedade controlCodeDescription.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getControlCodeDescription() {
        return controlCodeDescription;
    }

    /**
     * Define o valor da propriedade controlCodeDescription.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setControlCodeDescription(String value) {
        this.controlCodeDescription = value;
    }

    /**
     * Obtém o valor da propriedade regimeId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRegimeId() {
        return regimeId;
    }

    /**
     * Define o valor da propriedade regimeId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegimeId(String value) {
        this.regimeId = value;
    }

}
