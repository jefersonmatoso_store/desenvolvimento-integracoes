
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.*;


/**
 * Remark allows one to specify structured or unstructured remarks.
 *             Structured remarks may have a RemarkSequence,RemarkQualifierGid, and RemarkText.
 *             Unstructured remarks may have just RemarkText.
 *          
 * 
 * <p>Classe Java de RemarkType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="RemarkType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TransactionCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransactionCodeType" minOccurs="0"/>
 *         &lt;element name="RemarkSequence" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RemarkQualifierGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="RemarkLevel" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RemarkText" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RemarkType", propOrder = {
    "transactionCode",
    "remarkSequence",
    "remarkQualifierGid",
    "remarkLevel",
    "remarkText"
})
public class RemarkType {

    @XmlElement(name = "TransactionCode")
    @XmlSchemaType(name = "string")
    protected TransactionCodeType transactionCode;
    @XmlElement(name = "RemarkSequence")
    protected String remarkSequence;
    @XmlElement(name = "RemarkQualifierGid")
    protected GLogXMLGidType remarkQualifierGid;
    @XmlElement(name = "RemarkLevel")
    protected String remarkLevel;
    @XmlElement(name = "RemarkText", required = true)
    protected String remarkText;

    /**
     * Obtém o valor da propriedade transactionCode.
     * 
     * @return
     *     possible object is
     *     {@link TransactionCodeType }
     *     
     */
    public TransactionCodeType getTransactionCode() {
        return transactionCode;
    }

    /**
     * Define o valor da propriedade transactionCode.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionCodeType }
     *     
     */
    public void setTransactionCode(TransactionCodeType value) {
        this.transactionCode = value;
    }

    /**
     * Obtém o valor da propriedade remarkSequence.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRemarkSequence() {
        return remarkSequence;
    }

    /**
     * Define o valor da propriedade remarkSequence.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRemarkSequence(String value) {
        this.remarkSequence = value;
    }

    /**
     * Obtém o valor da propriedade remarkQualifierGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRemarkQualifierGid() {
        return remarkQualifierGid;
    }

    /**
     * Define o valor da propriedade remarkQualifierGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRemarkQualifierGid(GLogXMLGidType value) {
        this.remarkQualifierGid = value;
    }

    /**
     * Obtém o valor da propriedade remarkLevel.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRemarkLevel() {
        return remarkLevel;
    }

    /**
     * Define o valor da propriedade remarkLevel.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRemarkLevel(String value) {
        this.remarkLevel = value;
    }

    /**
     * Obtém o valor da propriedade remarkText.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRemarkText() {
        return remarkText;
    }

    /**
     * Define o valor da propriedade remarkText.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRemarkText(String value) {
        this.remarkText = value;
    }

}
