
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * This element specifies the related reference information from the Primary Shipment. The Primary Shipment in a
 *             multi-leg movement has the Shipment.ShipmentHeader2.IsPrimary = 'Y', and is generated from the primary leg of
 *             the Itinerary. Refer to the IsPrimary element.
 *          
 * 
 * <p>Classe Java de PrimaryShipmentRefInfoTYpe complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="PrimaryShipmentRefInfoTYpe">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ShipmentGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="ShipmentRefnum" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ShipmentRefnumType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Remark" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RemarkType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="AccessorialRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}AccessorialRefType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="SpecialServiceRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}AccessorialRefType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="ShipmentCostRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ShipmentCostRefType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="PriShipRefObject" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}PriShipRefObjectType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PrimaryShipmentRefInfoTYpe", propOrder = {
    "shipmentGid",
    "shipmentRefnum",
    "remark",
    "accessorialRef",
    "specialServiceRef",
    "shipmentCostRef",
    "priShipRefObject"
})
public class PrimaryShipmentRefInfoTYpe {

    @XmlElement(name = "ShipmentGid", required = true)
    protected GLogXMLGidType shipmentGid;
    @XmlElement(name = "ShipmentRefnum")
    protected List<ShipmentRefnumType> shipmentRefnum;
    @XmlElement(name = "Remark")
    protected List<RemarkType> remark;
    @XmlElement(name = "AccessorialRef")
    protected List<AccessorialRefType> accessorialRef;
    @XmlElement(name = "SpecialServiceRef")
    protected List<AccessorialRefType> specialServiceRef;
    @XmlElement(name = "ShipmentCostRef")
    protected List<ShipmentCostRefType> shipmentCostRef;
    @XmlElement(name = "PriShipRefObject")
    protected List<PriShipRefObjectType> priShipRefObject;

    /**
     * Obtém o valor da propriedade shipmentGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getShipmentGid() {
        return shipmentGid;
    }

    /**
     * Define o valor da propriedade shipmentGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setShipmentGid(GLogXMLGidType value) {
        this.shipmentGid = value;
    }

    /**
     * Gets the value of the shipmentRefnum property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the shipmentRefnum property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getShipmentRefnum().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ShipmentRefnumType }
     * 
     * 
     */
    public List<ShipmentRefnumType> getShipmentRefnum() {
        if (shipmentRefnum == null) {
            shipmentRefnum = new ArrayList<ShipmentRefnumType>();
        }
        return this.shipmentRefnum;
    }

    /**
     * Gets the value of the remark property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the remark property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRemark().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RemarkType }
     * 
     * 
     */
    public List<RemarkType> getRemark() {
        if (remark == null) {
            remark = new ArrayList<RemarkType>();
        }
        return this.remark;
    }

    /**
     * Gets the value of the accessorialRef property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the accessorialRef property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAccessorialRef().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AccessorialRefType }
     * 
     * 
     */
    public List<AccessorialRefType> getAccessorialRef() {
        if (accessorialRef == null) {
            accessorialRef = new ArrayList<AccessorialRefType>();
        }
        return this.accessorialRef;
    }

    /**
     * Gets the value of the specialServiceRef property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the specialServiceRef property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSpecialServiceRef().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AccessorialRefType }
     * 
     * 
     */
    public List<AccessorialRefType> getSpecialServiceRef() {
        if (specialServiceRef == null) {
            specialServiceRef = new ArrayList<AccessorialRefType>();
        }
        return this.specialServiceRef;
    }

    /**
     * Gets the value of the shipmentCostRef property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the shipmentCostRef property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getShipmentCostRef().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ShipmentCostRefType }
     * 
     * 
     */
    public List<ShipmentCostRefType> getShipmentCostRef() {
        if (shipmentCostRef == null) {
            shipmentCostRef = new ArrayList<ShipmentCostRefType>();
        }
        return this.shipmentCostRef;
    }

    /**
     * Gets the value of the priShipRefObject property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the priShipRefObject property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPriShipRefObject().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PriShipRefObjectType }
     * 
     * 
     */
    public List<PriShipRefObjectType> getPriShipRefObject() {
        if (priShipRefObject == null) {
            priShipRefObject = new ArrayList<PriShipRefObjectType>();
        }
        return this.priShipRefObject;
    }

}
