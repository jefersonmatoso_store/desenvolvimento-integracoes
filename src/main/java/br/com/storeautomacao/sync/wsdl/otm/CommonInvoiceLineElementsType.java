
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * CommonInvoiceLineElement is used to Used to convey commodity, tariff, remark, freight rate, and quantity information.
 * 
 * <p>Classe Java de CommonInvoiceLineElementsType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="CommonInvoiceLineElementsType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Commodity" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}CommodityType" minOccurs="0"/>
 *         &lt;element name="FP_Tariff" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FPTariffType" minOccurs="0"/>
 *         &lt;element name="Remark" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RemarkType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="FreightRate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FreightRateType" minOccurs="0"/>
 *         &lt;element name="BilledRated" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}BilledRatedType" minOccurs="0"/>
 *         &lt;element name="VatCodeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="AdjustmentReasonGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="GeneralLedgerGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="LineItemVat" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LineItemVatType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="FlexFieldStrings" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldStringType" minOccurs="0"/>
 *         &lt;element name="FlexFieldNumbers" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldNumberType" minOccurs="0"/>
 *         &lt;element name="FlexFieldDates" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldDateType" minOccurs="0"/>
 *         &lt;element name="FlexFieldCurrencies" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldCurrencyType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CommonInvoiceLineElementsType", propOrder = {
    "commodity",
    "fpTariff",
    "remark",
    "freightRate",
    "billedRated",
    "vatCodeGid",
    "adjustmentReasonGid",
    "generalLedgerGid",
    "lineItemVat",
    "flexFieldStrings",
    "flexFieldNumbers",
    "flexFieldDates",
    "flexFieldCurrencies"
})
public class CommonInvoiceLineElementsType {

    @XmlElement(name = "Commodity")
    protected CommodityType commodity;
    @XmlElement(name = "FP_Tariff")
    protected FPTariffType fpTariff;
    @XmlElement(name = "Remark")
    protected List<RemarkType> remark;
    @XmlElement(name = "FreightRate")
    protected FreightRateType freightRate;
    @XmlElement(name = "BilledRated")
    protected BilledRatedType billedRated;
    @XmlElement(name = "VatCodeGid")
    protected GLogXMLGidType vatCodeGid;
    @XmlElement(name = "AdjustmentReasonGid")
    protected GLogXMLGidType adjustmentReasonGid;
    @XmlElement(name = "GeneralLedgerGid")
    protected GLogXMLGidType generalLedgerGid;
    @XmlElement(name = "LineItemVat")
    protected List<LineItemVatType> lineItemVat;
    @XmlElement(name = "FlexFieldStrings")
    protected FlexFieldStringType flexFieldStrings;
    @XmlElement(name = "FlexFieldNumbers")
    protected FlexFieldNumberType flexFieldNumbers;
    @XmlElement(name = "FlexFieldDates")
    protected FlexFieldDateType flexFieldDates;
    @XmlElement(name = "FlexFieldCurrencies")
    protected FlexFieldCurrencyType flexFieldCurrencies;

    /**
     * Obtém o valor da propriedade commodity.
     * 
     * @return
     *     possible object is
     *     {@link CommodityType }
     *     
     */
    public CommodityType getCommodity() {
        return commodity;
    }

    /**
     * Define o valor da propriedade commodity.
     * 
     * @param value
     *     allowed object is
     *     {@link CommodityType }
     *     
     */
    public void setCommodity(CommodityType value) {
        this.commodity = value;
    }

    /**
     * Obtém o valor da propriedade fpTariff.
     * 
     * @return
     *     possible object is
     *     {@link FPTariffType }
     *     
     */
    public FPTariffType getFPTariff() {
        return fpTariff;
    }

    /**
     * Define o valor da propriedade fpTariff.
     * 
     * @param value
     *     allowed object is
     *     {@link FPTariffType }
     *     
     */
    public void setFPTariff(FPTariffType value) {
        this.fpTariff = value;
    }

    /**
     * Gets the value of the remark property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the remark property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRemark().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RemarkType }
     * 
     * 
     */
    public List<RemarkType> getRemark() {
        if (remark == null) {
            remark = new ArrayList<RemarkType>();
        }
        return this.remark;
    }

    /**
     * Obtém o valor da propriedade freightRate.
     * 
     * @return
     *     possible object is
     *     {@link FreightRateType }
     *     
     */
    public FreightRateType getFreightRate() {
        return freightRate;
    }

    /**
     * Define o valor da propriedade freightRate.
     * 
     * @param value
     *     allowed object is
     *     {@link FreightRateType }
     *     
     */
    public void setFreightRate(FreightRateType value) {
        this.freightRate = value;
    }

    /**
     * Obtém o valor da propriedade billedRated.
     * 
     * @return
     *     possible object is
     *     {@link BilledRatedType }
     *     
     */
    public BilledRatedType getBilledRated() {
        return billedRated;
    }

    /**
     * Define o valor da propriedade billedRated.
     * 
     * @param value
     *     allowed object is
     *     {@link BilledRatedType }
     *     
     */
    public void setBilledRated(BilledRatedType value) {
        this.billedRated = value;
    }

    /**
     * Obtém o valor da propriedade vatCodeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getVatCodeGid() {
        return vatCodeGid;
    }

    /**
     * Define o valor da propriedade vatCodeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setVatCodeGid(GLogXMLGidType value) {
        this.vatCodeGid = value;
    }

    /**
     * Obtém o valor da propriedade adjustmentReasonGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getAdjustmentReasonGid() {
        return adjustmentReasonGid;
    }

    /**
     * Define o valor da propriedade adjustmentReasonGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setAdjustmentReasonGid(GLogXMLGidType value) {
        this.adjustmentReasonGid = value;
    }

    /**
     * Obtém o valor da propriedade generalLedgerGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getGeneralLedgerGid() {
        return generalLedgerGid;
    }

    /**
     * Define o valor da propriedade generalLedgerGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setGeneralLedgerGid(GLogXMLGidType value) {
        this.generalLedgerGid = value;
    }

    /**
     * Gets the value of the lineItemVat property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the lineItemVat property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLineItemVat().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LineItemVatType }
     * 
     * 
     */
    public List<LineItemVatType> getLineItemVat() {
        if (lineItemVat == null) {
            lineItemVat = new ArrayList<LineItemVatType>();
        }
        return this.lineItemVat;
    }

    /**
     * Obtém o valor da propriedade flexFieldStrings.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldStringType }
     *     
     */
    public FlexFieldStringType getFlexFieldStrings() {
        return flexFieldStrings;
    }

    /**
     * Define o valor da propriedade flexFieldStrings.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldStringType }
     *     
     */
    public void setFlexFieldStrings(FlexFieldStringType value) {
        this.flexFieldStrings = value;
    }

    /**
     * Obtém o valor da propriedade flexFieldNumbers.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldNumberType }
     *     
     */
    public FlexFieldNumberType getFlexFieldNumbers() {
        return flexFieldNumbers;
    }

    /**
     * Define o valor da propriedade flexFieldNumbers.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldNumberType }
     *     
     */
    public void setFlexFieldNumbers(FlexFieldNumberType value) {
        this.flexFieldNumbers = value;
    }

    /**
     * Obtém o valor da propriedade flexFieldDates.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldDateType }
     *     
     */
    public FlexFieldDateType getFlexFieldDates() {
        return flexFieldDates;
    }

    /**
     * Define o valor da propriedade flexFieldDates.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldDateType }
     *     
     */
    public void setFlexFieldDates(FlexFieldDateType value) {
        this.flexFieldDates = value;
    }

    /**
     * Obtém o valor da propriedade flexFieldCurrencies.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldCurrencyType }
     *     
     */
    public FlexFieldCurrencyType getFlexFieldCurrencies() {
        return flexFieldCurrencies;
    }

    /**
     * Define o valor da propriedade flexFieldCurrencies.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldCurrencyType }
     *     
     */
    public void setFlexFieldCurrencies(FlexFieldCurrencyType value) {
        this.flexFieldCurrencies = value;
    }

}
