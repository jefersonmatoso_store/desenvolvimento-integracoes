
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 *             Tariff is data from the rate offering used to rate the shipment.
 *          
 * 
 * <p>Classe Java de TariffType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="TariffType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TariffOrganizationNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TariffOrganizationName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TariffNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TariffName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TariffDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TariffType", propOrder = {
    "tariffOrganizationNumber",
    "tariffOrganizationName",
    "tariffNumber",
    "tariffName",
    "tariffDt"
})
public class TariffType {

    @XmlElement(name = "TariffOrganizationNumber")
    protected String tariffOrganizationNumber;
    @XmlElement(name = "TariffOrganizationName")
    protected String tariffOrganizationName;
    @XmlElement(name = "TariffNumber")
    protected String tariffNumber;
    @XmlElement(name = "TariffName")
    protected String tariffName;
    @XmlElement(name = "TariffDt")
    protected GLogDateTimeType tariffDt;

    /**
     * Obtém o valor da propriedade tariffOrganizationNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTariffOrganizationNumber() {
        return tariffOrganizationNumber;
    }

    /**
     * Define o valor da propriedade tariffOrganizationNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTariffOrganizationNumber(String value) {
        this.tariffOrganizationNumber = value;
    }

    /**
     * Obtém o valor da propriedade tariffOrganizationName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTariffOrganizationName() {
        return tariffOrganizationName;
    }

    /**
     * Define o valor da propriedade tariffOrganizationName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTariffOrganizationName(String value) {
        this.tariffOrganizationName = value;
    }

    /**
     * Obtém o valor da propriedade tariffNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTariffNumber() {
        return tariffNumber;
    }

    /**
     * Define o valor da propriedade tariffNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTariffNumber(String value) {
        this.tariffNumber = value;
    }

    /**
     * Obtém o valor da propriedade tariffName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTariffName() {
        return tariffName;
    }

    /**
     * Define o valor da propriedade tariffName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTariffName(String value) {
        this.tariffName = value;
    }

    /**
     * Obtém o valor da propriedade tariffDt.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getTariffDt() {
        return tariffDt;
    }

    /**
     * Define o valor da propriedade tariffDt.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setTariffDt(GLogDateTimeType value) {
        this.tariffDt = value;
    }

}
