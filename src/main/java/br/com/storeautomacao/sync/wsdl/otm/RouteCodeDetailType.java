
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Contains the detailed information for the Route Code.
 * 
 * <p>Classe Java de RouteCodeDetailType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="RouteCodeDetailType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SequenceNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ScacGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="SequenceCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="RailJunctionCodeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RouteCodeDetailType", propOrder = {
    "sequenceNumber",
    "scacGid",
    "sequenceCode",
    "railJunctionCodeGid"
})
public class RouteCodeDetailType {

    @XmlElement(name = "SequenceNumber", required = true)
    protected String sequenceNumber;
    @XmlElement(name = "ScacGid")
    protected GLogXMLGidType scacGid;
    @XmlElement(name = "SequenceCode", required = true)
    protected String sequenceCode;
    @XmlElement(name = "RailJunctionCodeGid")
    protected GLogXMLGidType railJunctionCodeGid;

    /**
     * Obtém o valor da propriedade sequenceNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSequenceNumber() {
        return sequenceNumber;
    }

    /**
     * Define o valor da propriedade sequenceNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSequenceNumber(String value) {
        this.sequenceNumber = value;
    }

    /**
     * Obtém o valor da propriedade scacGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getScacGid() {
        return scacGid;
    }

    /**
     * Define o valor da propriedade scacGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setScacGid(GLogXMLGidType value) {
        this.scacGid = value;
    }

    /**
     * Obtém o valor da propriedade sequenceCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSequenceCode() {
        return sequenceCode;
    }

    /**
     * Define o valor da propriedade sequenceCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSequenceCode(String value) {
        this.sequenceCode = value;
    }

    /**
     * Obtém o valor da propriedade railJunctionCodeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRailJunctionCodeGid() {
        return railJunctionCodeGid;
    }

    /**
     * Define o valor da propriedade railJunctionCodeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRailJunctionCodeGid(GLogXMLGidType value) {
        this.railJunctionCodeGid = value;
    }

}
