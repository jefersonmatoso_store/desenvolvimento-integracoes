
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * 
 *             Refer to the Data Management Guide document for details on the CSV file format that applies to the CsvColumnList and
 *             CsvRow elements.
 *          
 * 
 * <p>Classe Java de CSVDataLoadType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="CSVDataLoadType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OTMTransactionInOut">
 *       &lt;sequence>
 *         &lt;element name="CsvCommand" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CsvTableName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CsvColumnList" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CsvRow" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CSVDataLoadType", propOrder = {
    "csvCommand",
    "csvTableName",
    "csvColumnList",
    "csvRow"
})
public class CSVDataLoadType
    extends OTMTransactionInOut
{

    @XmlElement(name = "CsvCommand", required = true)
    protected String csvCommand;
    @XmlElement(name = "CsvTableName", required = true)
    protected String csvTableName;
    @XmlElement(name = "CsvColumnList", required = true)
    protected String csvColumnList;
    @XmlElement(name = "CsvRow")
    protected List<String> csvRow;

    /**
     * Obtém o valor da propriedade csvCommand.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCsvCommand() {
        return csvCommand;
    }

    /**
     * Define o valor da propriedade csvCommand.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCsvCommand(String value) {
        this.csvCommand = value;
    }

    /**
     * Obtém o valor da propriedade csvTableName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCsvTableName() {
        return csvTableName;
    }

    /**
     * Define o valor da propriedade csvTableName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCsvTableName(String value) {
        this.csvTableName = value;
    }

    /**
     * Obtém o valor da propriedade csvColumnList.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCsvColumnList() {
        return csvColumnList;
    }

    /**
     * Define o valor da propriedade csvColumnList.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCsvColumnList(String value) {
        this.csvColumnList = value;
    }

    /**
     * Gets the value of the csvRow property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the csvRow property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCsvRow().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getCsvRow() {
        if (csvRow == null) {
            csvRow = new ArrayList<String>();
        }
        return this.csvRow;
    }

}
