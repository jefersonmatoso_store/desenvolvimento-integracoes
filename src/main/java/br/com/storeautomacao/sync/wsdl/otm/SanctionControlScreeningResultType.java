
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * (Outbound) Sanction control screening result.
 *          
 * 
 * <p>Classe Java de SanctionControlScreeningResultType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="SanctionControlScreeningResultType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GtmControlTypeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="ControlCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ComplianceRuleGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="ComplianceRuleGroupGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="Authorization" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AuthorizationAddedBy" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AuthorizationAddedOn" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType"/>
 *         &lt;element name="ControlCodeDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RegimeId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SanctionControlScreeningResultType", namespace = "http://xmlns.oracle.com/apps/gtm/transmission/v6.4", propOrder = {
    "gtmControlTypeGid",
    "controlCode",
    "complianceRuleGid",
    "complianceRuleGroupGid",
    "authorization",
    "authorizationAddedBy",
    "authorizationAddedOn",
    "controlCodeDescription",
    "regimeId"
})
public class SanctionControlScreeningResultType {

    @XmlElement(name = "GtmControlTypeGid", required = true)
    protected GLogXMLGidType gtmControlTypeGid;
    @XmlElement(name = "ControlCode", required = true)
    protected String controlCode;
    @XmlElement(name = "ComplianceRuleGid", required = true)
    protected GLogXMLGidType complianceRuleGid;
    @XmlElement(name = "ComplianceRuleGroupGid", required = true)
    protected GLogXMLGidType complianceRuleGroupGid;
    @XmlElement(name = "Authorization", required = true)
    protected String authorization;
    @XmlElement(name = "AuthorizationAddedBy", required = true)
    protected String authorizationAddedBy;
    @XmlElement(name = "AuthorizationAddedOn", required = true)
    protected GLogDateTimeType authorizationAddedOn;
    @XmlElement(name = "ControlCodeDescription")
    protected String controlCodeDescription;
    @XmlElement(name = "RegimeId")
    protected String regimeId;

    /**
     * Obtém o valor da propriedade gtmControlTypeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getGtmControlTypeGid() {
        return gtmControlTypeGid;
    }

    /**
     * Define o valor da propriedade gtmControlTypeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setGtmControlTypeGid(GLogXMLGidType value) {
        this.gtmControlTypeGid = value;
    }

    /**
     * Obtém o valor da propriedade controlCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getControlCode() {
        return controlCode;
    }

    /**
     * Define o valor da propriedade controlCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setControlCode(String value) {
        this.controlCode = value;
    }

    /**
     * Obtém o valor da propriedade complianceRuleGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getComplianceRuleGid() {
        return complianceRuleGid;
    }

    /**
     * Define o valor da propriedade complianceRuleGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setComplianceRuleGid(GLogXMLGidType value) {
        this.complianceRuleGid = value;
    }

    /**
     * Obtém o valor da propriedade complianceRuleGroupGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getComplianceRuleGroupGid() {
        return complianceRuleGroupGid;
    }

    /**
     * Define o valor da propriedade complianceRuleGroupGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setComplianceRuleGroupGid(GLogXMLGidType value) {
        this.complianceRuleGroupGid = value;
    }

    /**
     * Obtém o valor da propriedade authorization.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAuthorization() {
        return authorization;
    }

    /**
     * Define o valor da propriedade authorization.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAuthorization(String value) {
        this.authorization = value;
    }

    /**
     * Obtém o valor da propriedade authorizationAddedBy.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAuthorizationAddedBy() {
        return authorizationAddedBy;
    }

    /**
     * Define o valor da propriedade authorizationAddedBy.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAuthorizationAddedBy(String value) {
        this.authorizationAddedBy = value;
    }

    /**
     * Obtém o valor da propriedade authorizationAddedOn.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getAuthorizationAddedOn() {
        return authorizationAddedOn;
    }

    /**
     * Define o valor da propriedade authorizationAddedOn.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setAuthorizationAddedOn(GLogDateTimeType value) {
        this.authorizationAddedOn = value;
    }

    /**
     * Obtém o valor da propriedade controlCodeDescription.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getControlCodeDescription() {
        return controlCodeDescription;
    }

    /**
     * Define o valor da propriedade controlCodeDescription.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setControlCodeDescription(String value) {
        this.controlCodeDescription = value;
    }

    /**
     * Obtém o valor da propriedade regimeId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRegimeId() {
        return regimeId;
    }

    /**
     * Define o valor da propriedade regimeId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegimeId(String value) {
        this.regimeId = value;
    }

}
