
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * ContactRef may be used to reference an existing contact or define a new one.
 * 
 * <p>Classe Java de ContactRefType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="ContactRefType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element name="ContactGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="Contact" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ContactType"/>
 *         &lt;element name="ContactGroup" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ContactGroupType"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ContactRefType", propOrder = {
    "contactGid",
    "contact",
    "contactGroup"
})
public class ContactRefType {

    @XmlElement(name = "ContactGid")
    protected GLogXMLGidType contactGid;
    @XmlElement(name = "Contact")
    protected ContactType contact;
    @XmlElement(name = "ContactGroup")
    protected ContactGroupType contactGroup;

    /**
     * Obtém o valor da propriedade contactGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getContactGid() {
        return contactGid;
    }

    /**
     * Define o valor da propriedade contactGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setContactGid(GLogXMLGidType value) {
        this.contactGid = value;
    }

    /**
     * Obtém o valor da propriedade contact.
     * 
     * @return
     *     possible object is
     *     {@link ContactType }
     *     
     */
    public ContactType getContact() {
        return contact;
    }

    /**
     * Define o valor da propriedade contact.
     * 
     * @param value
     *     allowed object is
     *     {@link ContactType }
     *     
     */
    public void setContact(ContactType value) {
        this.contact = value;
    }

    /**
     * Obtém o valor da propriedade contactGroup.
     * 
     * @return
     *     possible object is
     *     {@link ContactGroupType }
     *     
     */
    public ContactGroupType getContactGroup() {
        return contactGroup;
    }

    /**
     * Define o valor da propriedade contactGroup.
     * 
     * @param value
     *     allowed object is
     *     {@link ContactGroupType }
     *     
     */
    public void setContactGroup(ContactGroupType value) {
        this.contactGroup = value;
    }

}
