
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * An EventTime consists of a PlannedTime, an ActualTime, and a IsPlannedTimeFixed flag,
 *             indicating if the PlannedTime cannot be changed.
 *          
 * 
 * <p>Classe Java de EventTimeType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="EventTimeType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PlannedTime" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="EstimatedTime" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="ActualTime" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="IsPlannedTimeFixed" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EventTimeType", propOrder = {
    "plannedTime",
    "estimatedTime",
    "actualTime",
    "isPlannedTimeFixed"
})
public class EventTimeType {

    @XmlElement(name = "PlannedTime")
    protected GLogDateTimeType plannedTime;
    @XmlElement(name = "EstimatedTime")
    protected GLogDateTimeType estimatedTime;
    @XmlElement(name = "ActualTime")
    protected GLogDateTimeType actualTime;
    @XmlElement(name = "IsPlannedTimeFixed")
    protected String isPlannedTimeFixed;

    /**
     * Obtém o valor da propriedade plannedTime.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getPlannedTime() {
        return plannedTime;
    }

    /**
     * Define o valor da propriedade plannedTime.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setPlannedTime(GLogDateTimeType value) {
        this.plannedTime = value;
    }

    /**
     * Obtém o valor da propriedade estimatedTime.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getEstimatedTime() {
        return estimatedTime;
    }

    /**
     * Define o valor da propriedade estimatedTime.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setEstimatedTime(GLogDateTimeType value) {
        this.estimatedTime = value;
    }

    /**
     * Obtém o valor da propriedade actualTime.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getActualTime() {
        return actualTime;
    }

    /**
     * Define o valor da propriedade actualTime.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setActualTime(GLogDateTimeType value) {
        this.actualTime = value;
    }

    /**
     * Obtém o valor da propriedade isPlannedTimeFixed.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsPlannedTimeFixed() {
        return isPlannedTimeFixed;
    }

    /**
     * Define o valor da propriedade isPlannedTimeFixed.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsPlannedTimeFixed(String value) {
        this.isPlannedTimeFixed = value;
    }

}
