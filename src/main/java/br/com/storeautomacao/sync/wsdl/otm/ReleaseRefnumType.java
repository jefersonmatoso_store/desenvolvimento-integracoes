
package br.com.storeautomacao.sync.wsdl.otm;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * ReleaseRefnum is an alternate method for identifying a Release. It consists of a
 *             qualifier and a value.
 *          
 * 
 * <p>Classe Java de ReleaseRefnumType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="ReleaseRefnumType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ReleaseRefnumQualifierGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="ReleaseRefnumValue" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReleaseRefnumType", propOrder = {
    "releaseRefnumQualifierGid",
    "releaseRefnumValue"
})
@XStreamAlias("ReleaseRefnumType")
public class ReleaseRefnumType {

    @XmlElement(name = "ReleaseRefnumQualifierGid", required = true)
    protected GLogXMLGidType releaseRefnumQualifierGid;
    @XmlElement(name = "ReleaseRefnumValue", required = true)
    protected String releaseRefnumValue;

    /**
     * Obtém o valor da propriedade releaseRefnumQualifierGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getReleaseRefnumQualifierGid() {
        return releaseRefnumQualifierGid;
    }

    /**
     * Define o valor da propriedade releaseRefnumQualifierGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setReleaseRefnumQualifierGid(GLogXMLGidType value) {
        this.releaseRefnumQualifierGid = value;
    }

    /**
     * Obtém o valor da propriedade releaseRefnumValue.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReleaseRefnumValue() {
        return releaseRefnumValue;
    }

    /**
     * Define o valor da propriedade releaseRefnumValue.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReleaseRefnumValue(String value) {
        this.releaseRefnumValue = value;
    }

}
