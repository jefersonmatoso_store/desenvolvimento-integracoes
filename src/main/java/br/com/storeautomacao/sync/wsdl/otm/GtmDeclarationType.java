
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;


/**
 * 
 *             Global Trade Management Declaration and it's details.
 *          
 * 
 * <p>Classe Java de GtmDeclarationType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="GtmDeclarationType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OTMTransactionInOut">
 *       &lt;sequence>
 *         &lt;element name="GtmDeclarationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="GtmDeclarationLine" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}GtmDeclarationLineType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="IntSavedQuery" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}IntSavedQueryType" minOccurs="0"/>
 *         &lt;element name="TransactionCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransactionCodeType" minOccurs="0"/>
 *         &lt;element name="ReplaceChildren" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ReplaceChildrenType" minOccurs="0"/>
 *         &lt;element name="IncoTermGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="IncoTermLocation" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsHazardousCargo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="GtmTransactionType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Remark" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RemarkType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="InvolvedParty" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}GtmInvolvedPartyType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="TransactionInvLocation" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}TransactionInvLocationType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Refnum" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RefnumType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Status" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}StatusType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="UserDefinedClassification" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}UserDefinedClassificationType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="TransDate" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}TransDateType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="CarrierCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TransactionQuantity" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}QuantityType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="TransactionCurrency" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}CurrencyType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="TransactionPolicy" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}TransactionPolicyType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="TransactionRequiredDocument" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}TransactionRequiredDocumentType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="PortInfo" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}PortInfoType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="FlexFieldStrings" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldStringType" minOccurs="0"/>
 *         &lt;element name="FlexFieldNumbers" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldNumberType" minOccurs="0"/>
 *         &lt;element name="FlexFieldDates" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldDateType" minOccurs="0"/>
 *         &lt;element name="BorderTransportModeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="BorderConveyanceName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BorderConveyanceFlag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InlandTransportModeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="InlandConveyanceName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InlandConveyanceFlag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DepartureTransportModeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="DepartureConveyanceName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DepartureConveyanceFlag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DeclarationType" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}DeclarationTypeType" minOccurs="0"/>
 *         &lt;element name="DeclarationSubType" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}DeclarationSubTypeType" minOccurs="0"/>
 *         &lt;element name="TariffPreferenceType" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}TariffPreferenceTypeType" minOccurs="0"/>
 *         &lt;element name="Procedure" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}ProcedureType" minOccurs="0"/>
 *         &lt;element name="PriorProcedure" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}ProcedureType" minOccurs="0"/>
 *         &lt;element name="ProcedureDetail" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}ProcedureDetailType" minOccurs="0"/>
 *         &lt;element name="GtmBond" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}GtmBondType" minOccurs="0"/>
 *         &lt;element name="Document" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}DocumentType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="ExchangeRateInfo" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ExchangeRateInfoType" minOccurs="0"/>
 *         &lt;element name="CurrencyGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="RequestStatusGroupGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="RequestStatusCodeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="ResponseStatusGroupGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="ResponseStatusCodeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="ValuationMethod" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}ValuationMethodType" minOccurs="0"/>
 *         &lt;element name="Conveyance" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}ConveyanceType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GtmDeclarationType", namespace = "http://xmlns.oracle.com/apps/gtm/transmission/v6.4", propOrder = {
    "gtmDeclarationGid",
    "gtmDeclarationLine",
    "intSavedQuery",
    "transactionCode",
    "replaceChildren",
    "incoTermGid",
    "incoTermLocation",
    "isHazardousCargo",
    "gtmTransactionType",
    "remark",
    "involvedParty",
    "transactionInvLocation",
    "refnum",
    "status",
    "userDefinedClassification",
    "transDate",
    "carrierCode",
    "transactionQuantity",
    "transactionCurrency",
    "transactionPolicy",
    "transactionRequiredDocument",
    "portInfo",
    "flexFieldStrings",
    "flexFieldNumbers",
    "flexFieldDates",
    "borderTransportModeGid",
    "borderConveyanceName",
    "borderConveyanceFlag",
    "inlandTransportModeGid",
    "inlandConveyanceName",
    "inlandConveyanceFlag",
    "departureTransportModeGid",
    "departureConveyanceName",
    "departureConveyanceFlag",
    "declarationType",
    "declarationSubType",
    "tariffPreferenceType",
    "procedure",
    "priorProcedure",
    "procedureDetail",
    "gtmBond",
    "document",
    "exchangeRateInfo",
    "currencyGid",
    "requestStatusGroupGid",
    "requestStatusCodeGid",
    "responseStatusGroupGid",
    "responseStatusCodeGid",
    "valuationMethod",
    "conveyance"
})
public class GtmDeclarationType
    extends OTMTransactionInOut
{

    @XmlElement(name = "GtmDeclarationGid")
    protected GLogXMLGidType gtmDeclarationGid;
    @XmlElement(name = "GtmDeclarationLine")
    protected List<GtmDeclarationLineType> gtmDeclarationLine;
    @XmlElement(name = "IntSavedQuery")
    protected IntSavedQueryType intSavedQuery;
    @XmlElement(name = "TransactionCode")
    @XmlSchemaType(name = "string")
    protected TransactionCodeType transactionCode;
    @XmlElement(name = "ReplaceChildren")
    protected ReplaceChildrenType replaceChildren;
    @XmlElement(name = "IncoTermGid")
    protected GLogXMLGidType incoTermGid;
    @XmlElement(name = "IncoTermLocation")
    protected String incoTermLocation;
    @XmlElement(name = "IsHazardousCargo")
    protected String isHazardousCargo;
    @XmlElement(name = "GtmTransactionType")
    protected String gtmTransactionType;
    @XmlElement(name = "Remark")
    protected List<RemarkType> remark;
    @XmlElement(name = "InvolvedParty")
    protected List<GtmInvolvedPartyType> involvedParty;
    @XmlElement(name = "TransactionInvLocation")
    protected List<TransactionInvLocationType> transactionInvLocation;
    @XmlElement(name = "Refnum")
    protected List<RefnumType> refnum;
    @XmlElement(name = "Status")
    protected List<StatusType> status;
    @XmlElement(name = "UserDefinedClassification")
    protected List<UserDefinedClassificationType> userDefinedClassification;
    @XmlElement(name = "TransDate")
    protected List<TransDateType> transDate;
    @XmlElement(name = "CarrierCode")
    protected String carrierCode;
    @XmlElement(name = "TransactionQuantity")
    protected List<QuantityType> transactionQuantity;
    @XmlElement(name = "TransactionCurrency")
    protected List<CurrencyType> transactionCurrency;
    @XmlElement(name = "TransactionPolicy")
    protected List<TransactionPolicyType> transactionPolicy;
    @XmlElement(name = "TransactionRequiredDocument")
    protected List<TransactionRequiredDocumentType> transactionRequiredDocument;
    @XmlElement(name = "PortInfo")
    protected List<PortInfoType> portInfo;
    @XmlElement(name = "FlexFieldStrings")
    protected FlexFieldStringType flexFieldStrings;
    @XmlElement(name = "FlexFieldNumbers")
    protected FlexFieldNumberType flexFieldNumbers;
    @XmlElement(name = "FlexFieldDates")
    protected FlexFieldDateType flexFieldDates;
    @XmlElement(name = "BorderTransportModeGid")
    protected GLogXMLGidType borderTransportModeGid;
    @XmlElement(name = "BorderConveyanceName")
    protected String borderConveyanceName;
    @XmlElement(name = "BorderConveyanceFlag")
    protected String borderConveyanceFlag;
    @XmlElement(name = "InlandTransportModeGid")
    protected GLogXMLGidType inlandTransportModeGid;
    @XmlElement(name = "InlandConveyanceName")
    protected String inlandConveyanceName;
    @XmlElement(name = "InlandConveyanceFlag")
    protected String inlandConveyanceFlag;
    @XmlElement(name = "DepartureTransportModeGid")
    protected GLogXMLGidType departureTransportModeGid;
    @XmlElement(name = "DepartureConveyanceName")
    protected String departureConveyanceName;
    @XmlElement(name = "DepartureConveyanceFlag")
    protected String departureConveyanceFlag;
    @XmlElement(name = "DeclarationType")
    protected DeclarationTypeType declarationType;
    @XmlElement(name = "DeclarationSubType")
    protected DeclarationSubTypeType declarationSubType;
    @XmlElement(name = "TariffPreferenceType")
    protected TariffPreferenceTypeType tariffPreferenceType;
    @XmlElement(name = "Procedure")
    protected ProcedureType procedure;
    @XmlElement(name = "PriorProcedure")
    protected ProcedureType priorProcedure;
    @XmlElement(name = "ProcedureDetail")
    protected ProcedureDetailType procedureDetail;
    @XmlElement(name = "GtmBond")
    protected GtmBondType gtmBond;
    @XmlElement(name = "Document")
    protected List<DocumentType> document;
    @XmlElement(name = "ExchangeRateInfo")
    protected ExchangeRateInfoType exchangeRateInfo;
    @XmlElement(name = "CurrencyGid")
    protected GLogXMLGidType currencyGid;
    @XmlElement(name = "RequestStatusGroupGid")
    protected GLogXMLGidType requestStatusGroupGid;
    @XmlElement(name = "RequestStatusCodeGid")
    protected GLogXMLGidType requestStatusCodeGid;
    @XmlElement(name = "ResponseStatusGroupGid")
    protected GLogXMLGidType responseStatusGroupGid;
    @XmlElement(name = "ResponseStatusCodeGid")
    protected GLogXMLGidType responseStatusCodeGid;
    @XmlElement(name = "ValuationMethod")
    protected ValuationMethodType valuationMethod;
    @XmlElement(name = "Conveyance")
    protected List<ConveyanceType> conveyance;

    /**
     * Obtém o valor da propriedade gtmDeclarationGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getGtmDeclarationGid() {
        return gtmDeclarationGid;
    }

    /**
     * Define o valor da propriedade gtmDeclarationGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setGtmDeclarationGid(GLogXMLGidType value) {
        this.gtmDeclarationGid = value;
    }

    /**
     * Gets the value of the gtmDeclarationLine property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the gtmDeclarationLine property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGtmDeclarationLine().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GtmDeclarationLineType }
     * 
     * 
     */
    public List<GtmDeclarationLineType> getGtmDeclarationLine() {
        if (gtmDeclarationLine == null) {
            gtmDeclarationLine = new ArrayList<GtmDeclarationLineType>();
        }
        return this.gtmDeclarationLine;
    }

    /**
     * Obtém o valor da propriedade intSavedQuery.
     * 
     * @return
     *     possible object is
     *     {@link IntSavedQueryType }
     *     
     */
    public IntSavedQueryType getIntSavedQuery() {
        return intSavedQuery;
    }

    /**
     * Define o valor da propriedade intSavedQuery.
     * 
     * @param value
     *     allowed object is
     *     {@link IntSavedQueryType }
     *     
     */
    public void setIntSavedQuery(IntSavedQueryType value) {
        this.intSavedQuery = value;
    }

    /**
     * Obtém o valor da propriedade transactionCode.
     * 
     * @return
     *     possible object is
     *     {@link TransactionCodeType }
     *     
     */
    public TransactionCodeType getTransactionCode() {
        return transactionCode;
    }

    /**
     * Define o valor da propriedade transactionCode.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionCodeType }
     *     
     */
    public void setTransactionCode(TransactionCodeType value) {
        this.transactionCode = value;
    }

    /**
     * Obtém o valor da propriedade replaceChildren.
     * 
     * @return
     *     possible object is
     *     {@link ReplaceChildrenType }
     *     
     */
    public ReplaceChildrenType getReplaceChildren() {
        return replaceChildren;
    }

    /**
     * Define o valor da propriedade replaceChildren.
     * 
     * @param value
     *     allowed object is
     *     {@link ReplaceChildrenType }
     *     
     */
    public void setReplaceChildren(ReplaceChildrenType value) {
        this.replaceChildren = value;
    }

    /**
     * Obtém o valor da propriedade incoTermGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getIncoTermGid() {
        return incoTermGid;
    }

    /**
     * Define o valor da propriedade incoTermGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setIncoTermGid(GLogXMLGidType value) {
        this.incoTermGid = value;
    }

    /**
     * Obtém o valor da propriedade incoTermLocation.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIncoTermLocation() {
        return incoTermLocation;
    }

    /**
     * Define o valor da propriedade incoTermLocation.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIncoTermLocation(String value) {
        this.incoTermLocation = value;
    }

    /**
     * Obtém o valor da propriedade isHazardousCargo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsHazardousCargo() {
        return isHazardousCargo;
    }

    /**
     * Define o valor da propriedade isHazardousCargo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsHazardousCargo(String value) {
        this.isHazardousCargo = value;
    }

    /**
     * Obtém o valor da propriedade gtmTransactionType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGtmTransactionType() {
        return gtmTransactionType;
    }

    /**
     * Define o valor da propriedade gtmTransactionType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGtmTransactionType(String value) {
        this.gtmTransactionType = value;
    }

    /**
     * Gets the value of the remark property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the remark property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRemark().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RemarkType }
     * 
     * 
     */
    public List<RemarkType> getRemark() {
        if (remark == null) {
            remark = new ArrayList<RemarkType>();
        }
        return this.remark;
    }

    /**
     * Gets the value of the involvedParty property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the involvedParty property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInvolvedParty().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GtmInvolvedPartyType }
     * 
     * 
     */
    public List<GtmInvolvedPartyType> getInvolvedParty() {
        if (involvedParty == null) {
            involvedParty = new ArrayList<GtmInvolvedPartyType>();
        }
        return this.involvedParty;
    }

    /**
     * Gets the value of the transactionInvLocation property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the transactionInvLocation property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTransactionInvLocation().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TransactionInvLocationType }
     * 
     * 
     */
    public List<TransactionInvLocationType> getTransactionInvLocation() {
        if (transactionInvLocation == null) {
            transactionInvLocation = new ArrayList<TransactionInvLocationType>();
        }
        return this.transactionInvLocation;
    }

    /**
     * Gets the value of the refnum property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the refnum property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRefnum().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RefnumType }
     * 
     * 
     */
    public List<RefnumType> getRefnum() {
        if (refnum == null) {
            refnum = new ArrayList<RefnumType>();
        }
        return this.refnum;
    }

    /**
     * Gets the value of the status property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the status property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getStatus().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link StatusType }
     * 
     * 
     */
    public List<StatusType> getStatus() {
        if (status == null) {
            status = new ArrayList<StatusType>();
        }
        return this.status;
    }

    /**
     * Gets the value of the userDefinedClassification property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the userDefinedClassification property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getUserDefinedClassification().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link UserDefinedClassificationType }
     * 
     * 
     */
    public List<UserDefinedClassificationType> getUserDefinedClassification() {
        if (userDefinedClassification == null) {
            userDefinedClassification = new ArrayList<UserDefinedClassificationType>();
        }
        return this.userDefinedClassification;
    }

    /**
     * Gets the value of the transDate property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the transDate property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTransDate().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TransDateType }
     * 
     * 
     */
    public List<TransDateType> getTransDate() {
        if (transDate == null) {
            transDate = new ArrayList<TransDateType>();
        }
        return this.transDate;
    }

    /**
     * Obtém o valor da propriedade carrierCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCarrierCode() {
        return carrierCode;
    }

    /**
     * Define o valor da propriedade carrierCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCarrierCode(String value) {
        this.carrierCode = value;
    }

    /**
     * Gets the value of the transactionQuantity property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the transactionQuantity property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTransactionQuantity().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link QuantityType }
     * 
     * 
     */
    public List<QuantityType> getTransactionQuantity() {
        if (transactionQuantity == null) {
            transactionQuantity = new ArrayList<QuantityType>();
        }
        return this.transactionQuantity;
    }

    /**
     * Gets the value of the transactionCurrency property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the transactionCurrency property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTransactionCurrency().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CurrencyType }
     * 
     * 
     */
    public List<CurrencyType> getTransactionCurrency() {
        if (transactionCurrency == null) {
            transactionCurrency = new ArrayList<CurrencyType>();
        }
        return this.transactionCurrency;
    }

    /**
     * Gets the value of the transactionPolicy property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the transactionPolicy property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTransactionPolicy().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TransactionPolicyType }
     * 
     * 
     */
    public List<TransactionPolicyType> getTransactionPolicy() {
        if (transactionPolicy == null) {
            transactionPolicy = new ArrayList<TransactionPolicyType>();
        }
        return this.transactionPolicy;
    }

    /**
     * Gets the value of the transactionRequiredDocument property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the transactionRequiredDocument property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTransactionRequiredDocument().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TransactionRequiredDocumentType }
     * 
     * 
     */
    public List<TransactionRequiredDocumentType> getTransactionRequiredDocument() {
        if (transactionRequiredDocument == null) {
            transactionRequiredDocument = new ArrayList<TransactionRequiredDocumentType>();
        }
        return this.transactionRequiredDocument;
    }

    /**
     * Gets the value of the portInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the portInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPortInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PortInfoType }
     * 
     * 
     */
    public List<PortInfoType> getPortInfo() {
        if (portInfo == null) {
            portInfo = new ArrayList<PortInfoType>();
        }
        return this.portInfo;
    }

    /**
     * Obtém o valor da propriedade flexFieldStrings.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldStringType }
     *     
     */
    public FlexFieldStringType getFlexFieldStrings() {
        return flexFieldStrings;
    }

    /**
     * Define o valor da propriedade flexFieldStrings.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldStringType }
     *     
     */
    public void setFlexFieldStrings(FlexFieldStringType value) {
        this.flexFieldStrings = value;
    }

    /**
     * Obtém o valor da propriedade flexFieldNumbers.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldNumberType }
     *     
     */
    public FlexFieldNumberType getFlexFieldNumbers() {
        return flexFieldNumbers;
    }

    /**
     * Define o valor da propriedade flexFieldNumbers.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldNumberType }
     *     
     */
    public void setFlexFieldNumbers(FlexFieldNumberType value) {
        this.flexFieldNumbers = value;
    }

    /**
     * Obtém o valor da propriedade flexFieldDates.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldDateType }
     *     
     */
    public FlexFieldDateType getFlexFieldDates() {
        return flexFieldDates;
    }

    /**
     * Define o valor da propriedade flexFieldDates.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldDateType }
     *     
     */
    public void setFlexFieldDates(FlexFieldDateType value) {
        this.flexFieldDates = value;
    }

    /**
     * Obtém o valor da propriedade borderTransportModeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getBorderTransportModeGid() {
        return borderTransportModeGid;
    }

    /**
     * Define o valor da propriedade borderTransportModeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setBorderTransportModeGid(GLogXMLGidType value) {
        this.borderTransportModeGid = value;
    }

    /**
     * Obtém o valor da propriedade borderConveyanceName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBorderConveyanceName() {
        return borderConveyanceName;
    }

    /**
     * Define o valor da propriedade borderConveyanceName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBorderConveyanceName(String value) {
        this.borderConveyanceName = value;
    }

    /**
     * Obtém o valor da propriedade borderConveyanceFlag.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBorderConveyanceFlag() {
        return borderConveyanceFlag;
    }

    /**
     * Define o valor da propriedade borderConveyanceFlag.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBorderConveyanceFlag(String value) {
        this.borderConveyanceFlag = value;
    }

    /**
     * Obtém o valor da propriedade inlandTransportModeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getInlandTransportModeGid() {
        return inlandTransportModeGid;
    }

    /**
     * Define o valor da propriedade inlandTransportModeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setInlandTransportModeGid(GLogXMLGidType value) {
        this.inlandTransportModeGid = value;
    }

    /**
     * Obtém o valor da propriedade inlandConveyanceName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInlandConveyanceName() {
        return inlandConveyanceName;
    }

    /**
     * Define o valor da propriedade inlandConveyanceName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInlandConveyanceName(String value) {
        this.inlandConveyanceName = value;
    }

    /**
     * Obtém o valor da propriedade inlandConveyanceFlag.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInlandConveyanceFlag() {
        return inlandConveyanceFlag;
    }

    /**
     * Define o valor da propriedade inlandConveyanceFlag.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInlandConveyanceFlag(String value) {
        this.inlandConveyanceFlag = value;
    }

    /**
     * Obtém o valor da propriedade departureTransportModeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getDepartureTransportModeGid() {
        return departureTransportModeGid;
    }

    /**
     * Define o valor da propriedade departureTransportModeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setDepartureTransportModeGid(GLogXMLGidType value) {
        this.departureTransportModeGid = value;
    }

    /**
     * Obtém o valor da propriedade departureConveyanceName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDepartureConveyanceName() {
        return departureConveyanceName;
    }

    /**
     * Define o valor da propriedade departureConveyanceName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDepartureConveyanceName(String value) {
        this.departureConveyanceName = value;
    }

    /**
     * Obtém o valor da propriedade departureConveyanceFlag.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDepartureConveyanceFlag() {
        return departureConveyanceFlag;
    }

    /**
     * Define o valor da propriedade departureConveyanceFlag.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDepartureConveyanceFlag(String value) {
        this.departureConveyanceFlag = value;
    }

    /**
     * Obtém o valor da propriedade declarationType.
     * 
     * @return
     *     possible object is
     *     {@link DeclarationTypeType }
     *     
     */
    public DeclarationTypeType getDeclarationType() {
        return declarationType;
    }

    /**
     * Define o valor da propriedade declarationType.
     * 
     * @param value
     *     allowed object is
     *     {@link DeclarationTypeType }
     *     
     */
    public void setDeclarationType(DeclarationTypeType value) {
        this.declarationType = value;
    }

    /**
     * Obtém o valor da propriedade declarationSubType.
     * 
     * @return
     *     possible object is
     *     {@link DeclarationSubTypeType }
     *     
     */
    public DeclarationSubTypeType getDeclarationSubType() {
        return declarationSubType;
    }

    /**
     * Define o valor da propriedade declarationSubType.
     * 
     * @param value
     *     allowed object is
     *     {@link DeclarationSubTypeType }
     *     
     */
    public void setDeclarationSubType(DeclarationSubTypeType value) {
        this.declarationSubType = value;
    }

    /**
     * Obtém o valor da propriedade tariffPreferenceType.
     * 
     * @return
     *     possible object is
     *     {@link TariffPreferenceTypeType }
     *     
     */
    public TariffPreferenceTypeType getTariffPreferenceType() {
        return tariffPreferenceType;
    }

    /**
     * Define o valor da propriedade tariffPreferenceType.
     * 
     * @param value
     *     allowed object is
     *     {@link TariffPreferenceTypeType }
     *     
     */
    public void setTariffPreferenceType(TariffPreferenceTypeType value) {
        this.tariffPreferenceType = value;
    }

    /**
     * Obtém o valor da propriedade procedure.
     * 
     * @return
     *     possible object is
     *     {@link ProcedureType }
     *     
     */
    public ProcedureType getProcedure() {
        return procedure;
    }

    /**
     * Define o valor da propriedade procedure.
     * 
     * @param value
     *     allowed object is
     *     {@link ProcedureType }
     *     
     */
    public void setProcedure(ProcedureType value) {
        this.procedure = value;
    }

    /**
     * Obtém o valor da propriedade priorProcedure.
     * 
     * @return
     *     possible object is
     *     {@link ProcedureType }
     *     
     */
    public ProcedureType getPriorProcedure() {
        return priorProcedure;
    }

    /**
     * Define o valor da propriedade priorProcedure.
     * 
     * @param value
     *     allowed object is
     *     {@link ProcedureType }
     *     
     */
    public void setPriorProcedure(ProcedureType value) {
        this.priorProcedure = value;
    }

    /**
     * Obtém o valor da propriedade procedureDetail.
     * 
     * @return
     *     possible object is
     *     {@link ProcedureDetailType }
     *     
     */
    public ProcedureDetailType getProcedureDetail() {
        return procedureDetail;
    }

    /**
     * Define o valor da propriedade procedureDetail.
     * 
     * @param value
     *     allowed object is
     *     {@link ProcedureDetailType }
     *     
     */
    public void setProcedureDetail(ProcedureDetailType value) {
        this.procedureDetail = value;
    }

    /**
     * Obtém o valor da propriedade gtmBond.
     * 
     * @return
     *     possible object is
     *     {@link GtmBondType }
     *     
     */
    public GtmBondType getGtmBond() {
        return gtmBond;
    }

    /**
     * Define o valor da propriedade gtmBond.
     * 
     * @param value
     *     allowed object is
     *     {@link GtmBondType }
     *     
     */
    public void setGtmBond(GtmBondType value) {
        this.gtmBond = value;
    }

    /**
     * Gets the value of the document property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the document property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDocument().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DocumentType }
     * 
     * 
     */
    public List<DocumentType> getDocument() {
        if (document == null) {
            document = new ArrayList<DocumentType>();
        }
        return this.document;
    }

    /**
     * Obtém o valor da propriedade exchangeRateInfo.
     * 
     * @return
     *     possible object is
     *     {@link ExchangeRateInfoType }
     *     
     */
    public ExchangeRateInfoType getExchangeRateInfo() {
        return exchangeRateInfo;
    }

    /**
     * Define o valor da propriedade exchangeRateInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link ExchangeRateInfoType }
     *     
     */
    public void setExchangeRateInfo(ExchangeRateInfoType value) {
        this.exchangeRateInfo = value;
    }

    /**
     * Obtém o valor da propriedade currencyGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getCurrencyGid() {
        return currencyGid;
    }

    /**
     * Define o valor da propriedade currencyGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setCurrencyGid(GLogXMLGidType value) {
        this.currencyGid = value;
    }

    /**
     * Obtém o valor da propriedade requestStatusGroupGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRequestStatusGroupGid() {
        return requestStatusGroupGid;
    }

    /**
     * Define o valor da propriedade requestStatusGroupGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRequestStatusGroupGid(GLogXMLGidType value) {
        this.requestStatusGroupGid = value;
    }

    /**
     * Obtém o valor da propriedade requestStatusCodeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRequestStatusCodeGid() {
        return requestStatusCodeGid;
    }

    /**
     * Define o valor da propriedade requestStatusCodeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRequestStatusCodeGid(GLogXMLGidType value) {
        this.requestStatusCodeGid = value;
    }

    /**
     * Obtém o valor da propriedade responseStatusGroupGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getResponseStatusGroupGid() {
        return responseStatusGroupGid;
    }

    /**
     * Define o valor da propriedade responseStatusGroupGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setResponseStatusGroupGid(GLogXMLGidType value) {
        this.responseStatusGroupGid = value;
    }

    /**
     * Obtém o valor da propriedade responseStatusCodeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getResponseStatusCodeGid() {
        return responseStatusCodeGid;
    }

    /**
     * Define o valor da propriedade responseStatusCodeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setResponseStatusCodeGid(GLogXMLGidType value) {
        this.responseStatusCodeGid = value;
    }

    /**
     * Obtém o valor da propriedade valuationMethod.
     * 
     * @return
     *     possible object is
     *     {@link ValuationMethodType }
     *     
     */
    public ValuationMethodType getValuationMethod() {
        return valuationMethod;
    }

    /**
     * Define o valor da propriedade valuationMethod.
     * 
     * @param value
     *     allowed object is
     *     {@link ValuationMethodType }
     *     
     */
    public void setValuationMethod(ValuationMethodType value) {
        this.valuationMethod = value;
    }

    /**
     * Gets the value of the conveyance property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the conveyance property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getConveyance().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ConveyanceType }
     * 
     * 
     */
    public List<ConveyanceType> getConveyance() {
        if (conveyance == null) {
            conveyance = new ArrayList<ConveyanceType>();
        }
        return this.conveyance;
    }

}
