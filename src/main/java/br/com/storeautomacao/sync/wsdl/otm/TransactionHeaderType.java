
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * 
 *             TransactionHeader contains the header level information for the Transaction (GLogXMLElement).
 *             The TimeZoneGid is used as an overide for the times specified in that Transaction.
 *             Typically, the times are assumed to be in the time zone of the corresponding location.
 *          
 * 
 * <p>Classe Java de TransactionHeaderType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="TransactionHeaderType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SenderTransactionId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ProcessInfo" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ProcessInfoType" minOccurs="0"/>
 *         &lt;element name="TimeZoneGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="ObjectModInfo" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="InsertDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *                   &lt;element name="UpdateDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="SendReason" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}SendReasonType" minOccurs="0"/>
 *         &lt;element name="Refnum" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RefnumType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TransactionHeaderType", propOrder = {
    "senderTransactionId",
    "processInfo",
    "timeZoneGid",
    "objectModInfo",
    "sendReason",
    "refnum"
})
public class TransactionHeaderType {

    @XmlElement(name = "SenderTransactionId")
    protected String senderTransactionId;
    @XmlElement(name = "ProcessInfo")
    protected ProcessInfoType processInfo;
    @XmlElement(name = "TimeZoneGid")
    protected GLogXMLGidType timeZoneGid;
    @XmlElement(name = "ObjectModInfo")
    protected ObjectModInfo objectModInfo;
    @XmlElement(name = "SendReason")
    protected SendReasonType sendReason;
    @XmlElement(name = "Refnum")
    protected List<RefnumType> refnum;

    /**
     * Obtém o valor da propriedade senderTransactionId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSenderTransactionId() {
        return senderTransactionId;
    }

    /**
     * Define o valor da propriedade senderTransactionId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSenderTransactionId(String value) {
        this.senderTransactionId = value;
    }

    /**
     * Obtém o valor da propriedade processInfo.
     * 
     * @return
     *     possible object is
     *     {@link ProcessInfoType }
     *     
     */
    public ProcessInfoType getProcessInfo() {
        return processInfo;
    }

    /**
     * Define o valor da propriedade processInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link ProcessInfoType }
     *     
     */
    public void setProcessInfo(ProcessInfoType value) {
        this.processInfo = value;
    }

    /**
     * Obtém o valor da propriedade timeZoneGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getTimeZoneGid() {
        return timeZoneGid;
    }

    /**
     * Define o valor da propriedade timeZoneGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setTimeZoneGid(GLogXMLGidType value) {
        this.timeZoneGid = value;
    }

    /**
     * Obtém o valor da propriedade objectModInfo.
     * 
     * @return
     *     possible object is
     *     {@link ObjectModInfo }
     *     
     */
    public ObjectModInfo getObjectModInfo() {
        return objectModInfo;
    }

    /**
     * Define o valor da propriedade objectModInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjectModInfo }
     *     
     */
    public void setObjectModInfo(ObjectModInfo value) {
        this.objectModInfo = value;
    }

    /**
     * Obtém o valor da propriedade sendReason.
     * 
     * @return
     *     possible object is
     *     {@link SendReasonType }
     *     
     */
    public SendReasonType getSendReason() {
        return sendReason;
    }

    /**
     * Define o valor da propriedade sendReason.
     * 
     * @param value
     *     allowed object is
     *     {@link SendReasonType }
     *     
     */
    public void setSendReason(SendReasonType value) {
        this.sendReason = value;
    }

    /**
     * Gets the value of the refnum property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the refnum property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRefnum().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RefnumType }
     * 
     * 
     */
    public List<RefnumType> getRefnum() {
        if (refnum == null) {
            refnum = new ArrayList<RefnumType>();
        }
        return this.refnum;
    }


    /**
     * <p>Classe Java de anonymous complex type.
     * 
     * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="InsertDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
     *         &lt;element name="UpdateDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "insertDt",
        "updateDt"
    })
    public static class ObjectModInfo {

        @XmlElement(name = "InsertDt")
        protected GLogDateTimeType insertDt;
        @XmlElement(name = "UpdateDt")
        protected GLogDateTimeType updateDt;

        /**
         * Obtém o valor da propriedade insertDt.
         * 
         * @return
         *     possible object is
         *     {@link GLogDateTimeType }
         *     
         */
        public GLogDateTimeType getInsertDt() {
            return insertDt;
        }

        /**
         * Define o valor da propriedade insertDt.
         * 
         * @param value
         *     allowed object is
         *     {@link GLogDateTimeType }
         *     
         */
        public void setInsertDt(GLogDateTimeType value) {
            this.insertDt = value;
        }

        /**
         * Obtém o valor da propriedade updateDt.
         * 
         * @return
         *     possible object is
         *     {@link GLogDateTimeType }
         *     
         */
        public GLogDateTimeType getUpdateDt() {
            return updateDt;
        }

        /**
         * Define o valor da propriedade updateDt.
         * 
         * @param value
         *     allowed object is
         *     {@link GLogDateTimeType }
         *     
         */
        public void setUpdateDt(GLogDateTimeType value) {
            this.updateDt = value;
        }

    }

}
