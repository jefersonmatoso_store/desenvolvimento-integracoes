
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Spefies the Value Added Tax Registration ID for the Corporation.
 * 
 * <p>Classe Java de VatRegistrationType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="VatRegistrationType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="VatRegistrationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="CountryCode3Gid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="IsPrimaryRegistration" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CustVatRegCountryCode3Gid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="DefaultIntraEUVatCodeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VatRegistrationType", propOrder = {
    "vatRegistrationGid",
    "countryCode3Gid",
    "isPrimaryRegistration",
    "custVatRegCountryCode3Gid",
    "defaultIntraEUVatCodeGid"
})
public class VatRegistrationType {

    @XmlElement(name = "VatRegistrationGid", required = true)
    protected GLogXMLGidType vatRegistrationGid;
    @XmlElement(name = "CountryCode3Gid", required = true)
    protected GLogXMLGidType countryCode3Gid;
    @XmlElement(name = "IsPrimaryRegistration")
    protected String isPrimaryRegistration;
    @XmlElement(name = "CustVatRegCountryCode3Gid")
    protected GLogXMLGidType custVatRegCountryCode3Gid;
    @XmlElement(name = "DefaultIntraEUVatCodeGid")
    protected GLogXMLGidType defaultIntraEUVatCodeGid;

    /**
     * Obtém o valor da propriedade vatRegistrationGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getVatRegistrationGid() {
        return vatRegistrationGid;
    }

    /**
     * Define o valor da propriedade vatRegistrationGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setVatRegistrationGid(GLogXMLGidType value) {
        this.vatRegistrationGid = value;
    }

    /**
     * Obtém o valor da propriedade countryCode3Gid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getCountryCode3Gid() {
        return countryCode3Gid;
    }

    /**
     * Define o valor da propriedade countryCode3Gid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setCountryCode3Gid(GLogXMLGidType value) {
        this.countryCode3Gid = value;
    }

    /**
     * Obtém o valor da propriedade isPrimaryRegistration.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsPrimaryRegistration() {
        return isPrimaryRegistration;
    }

    /**
     * Define o valor da propriedade isPrimaryRegistration.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsPrimaryRegistration(String value) {
        this.isPrimaryRegistration = value;
    }

    /**
     * Obtém o valor da propriedade custVatRegCountryCode3Gid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getCustVatRegCountryCode3Gid() {
        return custVatRegCountryCode3Gid;
    }

    /**
     * Define o valor da propriedade custVatRegCountryCode3Gid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setCustVatRegCountryCode3Gid(GLogXMLGidType value) {
        this.custVatRegCountryCode3Gid = value;
    }

    /**
     * Obtém o valor da propriedade defaultIntraEUVatCodeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getDefaultIntraEUVatCodeGid() {
        return defaultIntraEUVatCodeGid;
    }

    /**
     * Define o valor da propriedade defaultIntraEUVatCodeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setDefaultIntraEUVatCodeGid(GLogXMLGidType value) {
        this.defaultIntraEUVatCodeGid = value;
    }

}
