
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.*;


/**
 * Specifies time window preferences for appointment on a location for a service provider.
 *             When adding a new LocationServProvPrefDetail, set the TransactionCode = I to auto generate a new sequence.
 *             When updating a LocationServProfPrefDetail, use the IntSavedQuery to search for the sequence to update.
 *          
 * 
 * <p>Classe Java de LocationServProvPrefDetailType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="LocationServProvPrefDetailType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IntSavedQuery" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}IntSavedQueryType" minOccurs="0"/>
 *         &lt;element name="SequenceNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TransactionCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransactionCodeType" minOccurs="0"/>
 *         &lt;element name="DayofWeek" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="BeginTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="MaxSlot" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PreferenceLevel" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="DoorDuration" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="IsStanding" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LocationResourceGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LocationServProvPrefDetailType", propOrder = {
    "intSavedQuery",
    "sequenceNumber",
    "transactionCode",
    "dayofWeek",
    "beginTime",
    "maxSlot",
    "preferenceLevel",
    "doorDuration",
    "isStanding",
    "locationResourceGid"
})
public class LocationServProvPrefDetailType {

    @XmlElement(name = "IntSavedQuery")
    protected IntSavedQueryType intSavedQuery;
    @XmlElement(name = "SequenceNumber")
    protected String sequenceNumber;
    @XmlElement(name = "TransactionCode")
    @XmlSchemaType(name = "string")
    protected TransactionCodeType transactionCode;
    @XmlElement(name = "DayofWeek", required = true)
    protected String dayofWeek;
    @XmlElement(name = "BeginTime", required = true)
    protected String beginTime;
    @XmlElement(name = "MaxSlot")
    protected String maxSlot;
    @XmlElement(name = "PreferenceLevel", required = true)
    protected String preferenceLevel;
    @XmlElement(name = "DoorDuration", required = true)
    protected String doorDuration;
    @XmlElement(name = "IsStanding")
    protected String isStanding;
    @XmlElement(name = "LocationResourceGid")
    protected GLogXMLGidType locationResourceGid;

    /**
     * Obtém o valor da propriedade intSavedQuery.
     * 
     * @return
     *     possible object is
     *     {@link IntSavedQueryType }
     *     
     */
    public IntSavedQueryType getIntSavedQuery() {
        return intSavedQuery;
    }

    /**
     * Define o valor da propriedade intSavedQuery.
     * 
     * @param value
     *     allowed object is
     *     {@link IntSavedQueryType }
     *     
     */
    public void setIntSavedQuery(IntSavedQueryType value) {
        this.intSavedQuery = value;
    }

    /**
     * Obtém o valor da propriedade sequenceNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSequenceNumber() {
        return sequenceNumber;
    }

    /**
     * Define o valor da propriedade sequenceNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSequenceNumber(String value) {
        this.sequenceNumber = value;
    }

    /**
     * Obtém o valor da propriedade transactionCode.
     * 
     * @return
     *     possible object is
     *     {@link TransactionCodeType }
     *     
     */
    public TransactionCodeType getTransactionCode() {
        return transactionCode;
    }

    /**
     * Define o valor da propriedade transactionCode.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionCodeType }
     *     
     */
    public void setTransactionCode(TransactionCodeType value) {
        this.transactionCode = value;
    }

    /**
     * Obtém o valor da propriedade dayofWeek.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDayofWeek() {
        return dayofWeek;
    }

    /**
     * Define o valor da propriedade dayofWeek.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDayofWeek(String value) {
        this.dayofWeek = value;
    }

    /**
     * Obtém o valor da propriedade beginTime.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBeginTime() {
        return beginTime;
    }

    /**
     * Define o valor da propriedade beginTime.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBeginTime(String value) {
        this.beginTime = value;
    }

    /**
     * Obtém o valor da propriedade maxSlot.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMaxSlot() {
        return maxSlot;
    }

    /**
     * Define o valor da propriedade maxSlot.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMaxSlot(String value) {
        this.maxSlot = value;
    }

    /**
     * Obtém o valor da propriedade preferenceLevel.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPreferenceLevel() {
        return preferenceLevel;
    }

    /**
     * Define o valor da propriedade preferenceLevel.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPreferenceLevel(String value) {
        this.preferenceLevel = value;
    }

    /**
     * Obtém o valor da propriedade doorDuration.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDoorDuration() {
        return doorDuration;
    }

    /**
     * Define o valor da propriedade doorDuration.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDoorDuration(String value) {
        this.doorDuration = value;
    }

    /**
     * Obtém o valor da propriedade isStanding.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsStanding() {
        return isStanding;
    }

    /**
     * Define o valor da propriedade isStanding.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsStanding(String value) {
        this.isStanding = value;
    }

    /**
     * Obtém o valor da propriedade locationResourceGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getLocationResourceGid() {
        return locationResourceGid;
    }

    /**
     * Define o valor da propriedade locationResourceGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setLocationResourceGid(GLogXMLGidType value) {
        this.locationResourceGid = value;
    }

}
