
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * (Both) Region Information.
 *          
 * 
 * <p>Classe Java de RegionInfoType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="RegionInfoType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RegionQualifier" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="RegionId" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RegionInfoType", namespace = "http://xmlns.oracle.com/apps/gtm/transmission/v6.4", propOrder = {
    "regionQualifier",
    "regionId"
})
public class RegionInfoType {

    @XmlElement(name = "RegionQualifier", required = true)
    protected GLogXMLGidType regionQualifier;
    @XmlElement(name = "RegionId", required = true)
    protected GLogXMLGidType regionId;

    /**
     * Obtém o valor da propriedade regionQualifier.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRegionQualifier() {
        return regionQualifier;
    }

    /**
     * Define o valor da propriedade regionQualifier.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRegionQualifier(GLogXMLGidType value) {
        this.regionQualifier = value;
    }

    /**
     * Obtém o valor da propriedade regionId.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRegionId() {
        return regionId;
    }

    /**
     * Define o valor da propriedade regionId.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRegionId(GLogXMLGidType value) {
        this.regionId = value;
    }

}
