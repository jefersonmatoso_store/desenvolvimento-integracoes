
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 *             PowerUnitRefnum is a qualifier/value pair used to refer to a power unit.
 *          
 * 
 * <p>Classe Java de PowerUnitRefnumType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="PowerUnitRefnumType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PURefnumQualGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="PURefnumValue" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PowerUnitRefnumType", propOrder = {
    "puRefnumQualGid",
    "puRefnumValue"
})
public class PowerUnitRefnumType {

    @XmlElement(name = "PURefnumQualGid", required = true)
    protected GLogXMLGidType puRefnumQualGid;
    @XmlElement(name = "PURefnumValue", required = true)
    protected String puRefnumValue;

    /**
     * Obtém o valor da propriedade puRefnumQualGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getPURefnumQualGid() {
        return puRefnumQualGid;
    }

    /**
     * Define o valor da propriedade puRefnumQualGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setPURefnumQualGid(GLogXMLGidType value) {
        this.puRefnumQualGid = value;
    }

    /**
     * Obtém o valor da propriedade puRefnumValue.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPURefnumValue() {
        return puRefnumValue;
    }

    /**
     * Define o valor da propriedade puRefnumValue.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPURefnumValue(String value) {
        this.puRefnumValue = value;
    }

}
