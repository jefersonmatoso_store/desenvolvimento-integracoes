
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;


/**
 * 
 *             The object is identified within OTM using the Gid and SequenceNumber elements. The Gid is used to specify
 *             primary key of the object(e.g. for SHIPMENT, this would be the
 *             ShipmentGid). And the SequenceNumber is used in conjunction with the Gid element to identify the object
 *             when the Gid is insufficient. For example, the S_SHIP_UNIT_LINE object has a S_SHIP_UNIT_LINE_NO field as
 *             part of its primary key, so the SequenceNumber would correspond to the S_SHIP_UNIT_LINE_NO. Other objects
 *             requiring the sequence number include the INVOICE_LINEITEM and SHIPMENT_STOP.
 * 
 *             For the Refnum objects that have the qualifier and value as part of the primary key, the Transaction code
 *             will indicate whether the new qualifier/value pair should be added (Insert), or used to replace all of the
 *             current records with the same qualifier (Update). For example, the Shipment_Refnum table has a composite
 *             primary key made up of the ShipmentGid, RefnumQualifier, and RefnumValue. Assume a Shipment has the
 *             following ShipmentRefnum Qualifier/Value pairs in the system:
 *             - CO/A-12345, CO/B-89387, CN/C-83920.
 *             If a new refnum qualifier/value of CO/D-23849 is being sent in using the GenericStatusUpdate interface,
 *             the change would be affected by the TransactionCode as follows:
 *             i. TransactionCode = I The new refnum would be added, resulting in all of the following being
 *             present in the table:
 *             CO/A-12345, CO/B-89387, CN/C-83920, CO/D-23849
 *             ii. TransactionCode = U The current refnums with the same qualifier would be deleted, and replaced
 *             by the new one. In this case, the result would leave the following in the table:
 *             CN/C-83920, CO/D-23849
 *             The TransactionCode is only applicable for the Refnum and Remark elements. It is not used for the Status or
 *             Indicator elements, which are only intended to be updated.
 *          
 * 
 * <p>Classe Java de GenericStatusUpdateType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="GenericStatusUpdateType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OTMTransactionIn">
 *       &lt;sequence>
 *         &lt;element name="GenericStatusObjectType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="IntSavedQuery" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}IntSavedQueryType" minOccurs="0"/>
 *         &lt;element name="Gid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GidType"/>
 *         &lt;element name="SequenceNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TransactionCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransactionCodeType"/>
 *         &lt;element name="Refnum" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RefnumType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Remark" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RemarkType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Status" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}StatusType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Indicator" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GenericStatusUpdateType", propOrder = {
    "genericStatusObjectType",
    "intSavedQuery",
    "gid",
    "sequenceNumber",
    "transactionCode",
    "refnum",
    "remark",
    "status",
    "indicator"
})
public class GenericStatusUpdateType
    extends OTMTransactionIn
{

    @XmlElement(name = "GenericStatusObjectType", required = true)
    protected String genericStatusObjectType;
    @XmlElement(name = "IntSavedQuery")
    protected IntSavedQueryType intSavedQuery;
    @XmlElement(name = "Gid", required = true)
    protected GidType gid;
    @XmlElement(name = "SequenceNumber")
    protected String sequenceNumber;
    @XmlElement(name = "TransactionCode", required = true)
    @XmlSchemaType(name = "string")
    protected TransactionCodeType transactionCode;
    @XmlElement(name = "Refnum")
    protected List<RefnumType> refnum;
    @XmlElement(name = "Remark")
    protected List<RemarkType> remark;
    @XmlElement(name = "Status")
    protected List<StatusType> status;
    @XmlElement(name = "Indicator")
    protected String indicator;

    /**
     * Obtém o valor da propriedade genericStatusObjectType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGenericStatusObjectType() {
        return genericStatusObjectType;
    }

    /**
     * Define o valor da propriedade genericStatusObjectType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGenericStatusObjectType(String value) {
        this.genericStatusObjectType = value;
    }

    /**
     * Obtém o valor da propriedade intSavedQuery.
     * 
     * @return
     *     possible object is
     *     {@link IntSavedQueryType }
     *     
     */
    public IntSavedQueryType getIntSavedQuery() {
        return intSavedQuery;
    }

    /**
     * Define o valor da propriedade intSavedQuery.
     * 
     * @param value
     *     allowed object is
     *     {@link IntSavedQueryType }
     *     
     */
    public void setIntSavedQuery(IntSavedQueryType value) {
        this.intSavedQuery = value;
    }

    /**
     * Obtém o valor da propriedade gid.
     * 
     * @return
     *     possible object is
     *     {@link GidType }
     *     
     */
    public GidType getGid() {
        return gid;
    }

    /**
     * Define o valor da propriedade gid.
     * 
     * @param value
     *     allowed object is
     *     {@link GidType }
     *     
     */
    public void setGid(GidType value) {
        this.gid = value;
    }

    /**
     * Obtém o valor da propriedade sequenceNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSequenceNumber() {
        return sequenceNumber;
    }

    /**
     * Define o valor da propriedade sequenceNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSequenceNumber(String value) {
        this.sequenceNumber = value;
    }

    /**
     * Obtém o valor da propriedade transactionCode.
     * 
     * @return
     *     possible object is
     *     {@link TransactionCodeType }
     *     
     */
    public TransactionCodeType getTransactionCode() {
        return transactionCode;
    }

    /**
     * Define o valor da propriedade transactionCode.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionCodeType }
     *     
     */
    public void setTransactionCode(TransactionCodeType value) {
        this.transactionCode = value;
    }

    /**
     * Gets the value of the refnum property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the refnum property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRefnum().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RefnumType }
     * 
     * 
     */
    public List<RefnumType> getRefnum() {
        if (refnum == null) {
            refnum = new ArrayList<RefnumType>();
        }
        return this.refnum;
    }

    /**
     * Gets the value of the remark property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the remark property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRemark().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RemarkType }
     * 
     * 
     */
    public List<RemarkType> getRemark() {
        if (remark == null) {
            remark = new ArrayList<RemarkType>();
        }
        return this.remark;
    }

    /**
     * Gets the value of the status property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the status property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getStatus().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link StatusType }
     * 
     * 
     */
    public List<StatusType> getStatus() {
        if (status == null) {
            status = new ArrayList<StatusType>();
        }
        return this.status;
    }

    /**
     * Obtém o valor da propriedade indicator.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIndicator() {
        return indicator;
    }

    /**
     * Define o valor da propriedade indicator.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIndicator(String value) {
        this.indicator = value;
    }

}
