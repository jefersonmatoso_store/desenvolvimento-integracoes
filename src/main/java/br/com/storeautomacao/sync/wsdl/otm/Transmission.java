
package br.com.storeautomacao.sync.wsdl.otm;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Classe Java de anonymous complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransmissionDocumentType">
 *       &lt;sequence>
 *         &lt;element name="TransmissionHeader" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransmissionHeaderType"/>
 *         &lt;element name="TransmissionBody">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="GLogXMLElement" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLElementType" maxOccurs="unbounded"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *       &lt;attribute name="Version" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "transmissionHeader",
    "transmissionBody"
})
@XmlRootElement(name = "Transmission")
@XStreamAlias("Transmission")
public class Transmission
    extends TransmissionDocumentType
{

    @XmlElement(name = "TransmissionHeader", required = true)
    protected TransmissionHeaderType transmissionHeader;
    @XmlElement(name = "TransmissionBody", required = true)
    protected TransmissionBody transmissionBody;
    @XmlAttribute(name = "Version")
    protected String version;

    /**
     * Obtém o valor da propriedade transmissionHeader.
     * 
     * @return
     *     possible object is
     *     {@link TransmissionHeaderType }
     *     
     */
    public TransmissionHeaderType getTransmissionHeader() {
        return transmissionHeader;
    }

    /**
     * Define o valor da propriedade transmissionHeader.
     * 
     * @param value
     *     allowed object is
     *     {@link TransmissionHeaderType }
     *     
     */
    public void setTransmissionHeader(TransmissionHeaderType value) {
        this.transmissionHeader = value;
    }

    /**
     * Obtém o valor da propriedade transmissionBody.
     * 
     * @return
     *     possible object is
     *     {@link TransmissionBody }
     *     
     */
    public TransmissionBody getTransmissionBody() {
        return transmissionBody;
    }

    /**
     * Define o valor da propriedade transmissionBody.
     * 
     * @param value
     *     allowed object is
     *     {@link TransmissionBody }
     *     
     */
    public void setTransmissionBody(TransmissionBody value) {
        this.transmissionBody = value;
    }

    /**
     * Obtém o valor da propriedade version.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVersion() {
        return version;
    }

    /**
     * Define o valor da propriedade version.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVersion(String value) {
        this.version = value;
    }


    /**
     * <p>Classe Java de anonymous complex type.
     * 
     * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="GLogXMLElement" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLElementType" maxOccurs="unbounded"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "gLogXMLElement"
    })
    @XStreamAlias("TransmissionBody")
    public static class TransmissionBody {
        @XStreamImplicit
        @XmlElement(name = "GLogXMLElement", required = true)
        protected List<GLogXMLElementType> gLogXMLElement;

        /**
         * Gets the value of the gLogXMLElement property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the gLogXMLElement property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getGLogXMLElement().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link GLogXMLElementType }
         * 
         * 
         */
        public List<GLogXMLElementType> getGLogXMLElement() {
            if (gLogXMLElement == null) {
                gLogXMLElement = new ArrayList<GLogXMLElementType>();
            }
            return this.gLogXMLElement;
        }

    }

}
