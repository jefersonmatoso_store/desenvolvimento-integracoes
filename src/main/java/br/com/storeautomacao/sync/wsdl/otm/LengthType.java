
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Length contains sub-elements for length value and length unit of measure.
 * 
 * <p>Classe Java de LengthType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="LengthType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="LengthValue" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="LengthUOMGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LengthType", propOrder = {
    "lengthValue",
    "lengthUOMGid"
})
public class LengthType {

    @XmlElement(name = "LengthValue", required = true)
    protected String lengthValue;
    @XmlElement(name = "LengthUOMGid", required = true)
    protected GLogXMLGidType lengthUOMGid;

    /**
     * Obtém o valor da propriedade lengthValue.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLengthValue() {
        return lengthValue;
    }

    /**
     * Define o valor da propriedade lengthValue.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLengthValue(String value) {
        this.lengthValue = value;
    }

    /**
     * Obtém o valor da propriedade lengthUOMGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getLengthUOMGid() {
        return lengthUOMGid;
    }

    /**
     * Define o valor da propriedade lengthUOMGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setLengthUOMGid(GLogXMLGidType value) {
        this.lengthUOMGid = value;
    }

}
