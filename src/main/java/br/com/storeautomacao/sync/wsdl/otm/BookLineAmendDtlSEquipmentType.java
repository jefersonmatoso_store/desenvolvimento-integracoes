
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Specifies the Booking Line Amendment Changes for a SEquipment (container).
 * 
 * <p>Classe Java de BookLineAmendDtlSEquipmentType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="BookLineAmendDtlSEquipmentType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ChangeAction" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SEquipmentGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="WeightVolume" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}WeightVolumeType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BookLineAmendDtlSEquipmentType", propOrder = {
    "changeAction",
    "sEquipmentGid",
    "weightVolume"
})
public class BookLineAmendDtlSEquipmentType {

    @XmlElement(name = "ChangeAction")
    protected String changeAction;
    @XmlElement(name = "SEquipmentGid")
    protected GLogXMLGidType sEquipmentGid;
    @XmlElement(name = "WeightVolume")
    protected WeightVolumeType weightVolume;

    /**
     * Obtém o valor da propriedade changeAction.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChangeAction() {
        return changeAction;
    }

    /**
     * Define o valor da propriedade changeAction.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChangeAction(String value) {
        this.changeAction = value;
    }

    /**
     * Obtém o valor da propriedade sEquipmentGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getSEquipmentGid() {
        return sEquipmentGid;
    }

    /**
     * Define o valor da propriedade sEquipmentGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setSEquipmentGid(GLogXMLGidType value) {
        this.sEquipmentGid = value;
    }

    /**
     * Obtém o valor da propriedade weightVolume.
     * 
     * @return
     *     possible object is
     *     {@link WeightVolumeType }
     *     
     */
    public WeightVolumeType getWeightVolume() {
        return weightVolume;
    }

    /**
     * Define o valor da propriedade weightVolume.
     * 
     * @param value
     *     allowed object is
     *     {@link WeightVolumeType }
     *     
     */
    public void setWeightVolume(WeightVolumeType value) {
        this.weightVolume = value;
    }

}
