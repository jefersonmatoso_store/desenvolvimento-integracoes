
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Specifies the set of exchange rate information to use for currency conversions. This element is supported on the outbound only, except in the
 *             CommercialInvoice.
 *          
 * 
 * <p>Classe Java de ExchangeRateInfoType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="ExchangeRateInfoType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ExchangeRateGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="ExchangeRateDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ExchangeRateInfoType", propOrder = {
    "exchangeRateGid",
    "exchangeRateDt"
})
public class ExchangeRateInfoType {

    @XmlElement(name = "ExchangeRateGid")
    protected GLogXMLGidType exchangeRateGid;
    @XmlElement(name = "ExchangeRateDt")
    protected GLogDateTimeType exchangeRateDt;

    /**
     * Obtém o valor da propriedade exchangeRateGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getExchangeRateGid() {
        return exchangeRateGid;
    }

    /**
     * Define o valor da propriedade exchangeRateGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setExchangeRateGid(GLogXMLGidType value) {
        this.exchangeRateGid = value;
    }

    /**
     * Obtém o valor da propriedade exchangeRateDt.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getExchangeRateDt() {
        return exchangeRateDt;
    }

    /**
     * Define o valor da propriedade exchangeRateDt.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setExchangeRateDt(GLogDateTimeType value) {
        this.exchangeRateDt = value;
    }

}
