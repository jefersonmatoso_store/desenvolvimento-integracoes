
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * 
 *             Defines special services required for this shipment status
 *          
 * 
 * <p>Classe Java de ShipStatusSpclServiceType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="ShipStatusSpclServiceType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;choice>
 *           &lt;element name="SpclService" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}SpclServiceType"/>
 *           &lt;element name="SpecialServiceGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;/choice>
 *         &lt;element name="ServiceDuration" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDurationType" minOccurs="0"/>
 *         &lt;element name="ServiceStartTime" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="Distance" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}DistanceType" minOccurs="0"/>
 *         &lt;element name="Weight" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}WeightType" minOccurs="0"/>
 *         &lt;element name="Volume" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}VolumeType" minOccurs="0"/>
 *         &lt;element name="ItemCount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ShipUnitCount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Remark" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RemarkType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="ServiceCompletionState" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ShipStatusSpclServiceType", propOrder = {
    "spclService",
    "specialServiceGid",
    "serviceDuration",
    "serviceStartTime",
    "distance",
    "weight",
    "volume",
    "itemCount",
    "shipUnitCount",
    "remark",
    "serviceCompletionState"
})
public class ShipStatusSpclServiceType {

    @XmlElement(name = "SpclService")
    protected SpclServiceType spclService;
    @XmlElement(name = "SpecialServiceGid")
    protected GLogXMLGidType specialServiceGid;
    @XmlElement(name = "ServiceDuration")
    protected GLogXMLDurationType serviceDuration;
    @XmlElement(name = "ServiceStartTime")
    protected GLogDateTimeType serviceStartTime;
    @XmlElement(name = "Distance")
    protected DistanceType distance;
    @XmlElement(name = "Weight")
    protected WeightType weight;
    @XmlElement(name = "Volume")
    protected VolumeType volume;
    @XmlElement(name = "ItemCount")
    protected String itemCount;
    @XmlElement(name = "ShipUnitCount")
    protected String shipUnitCount;
    @XmlElement(name = "Remark")
    protected List<RemarkType> remark;
    @XmlElement(name = "ServiceCompletionState")
    protected String serviceCompletionState;

    /**
     * Obtém o valor da propriedade spclService.
     * 
     * @return
     *     possible object is
     *     {@link SpclServiceType }
     *     
     */
    public SpclServiceType getSpclService() {
        return spclService;
    }

    /**
     * Define o valor da propriedade spclService.
     * 
     * @param value
     *     allowed object is
     *     {@link SpclServiceType }
     *     
     */
    public void setSpclService(SpclServiceType value) {
        this.spclService = value;
    }

    /**
     * Obtém o valor da propriedade specialServiceGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getSpecialServiceGid() {
        return specialServiceGid;
    }

    /**
     * Define o valor da propriedade specialServiceGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setSpecialServiceGid(GLogXMLGidType value) {
        this.specialServiceGid = value;
    }

    /**
     * Obtém o valor da propriedade serviceDuration.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public GLogXMLDurationType getServiceDuration() {
        return serviceDuration;
    }

    /**
     * Define o valor da propriedade serviceDuration.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public void setServiceDuration(GLogXMLDurationType value) {
        this.serviceDuration = value;
    }

    /**
     * Obtém o valor da propriedade serviceStartTime.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getServiceStartTime() {
        return serviceStartTime;
    }

    /**
     * Define o valor da propriedade serviceStartTime.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setServiceStartTime(GLogDateTimeType value) {
        this.serviceStartTime = value;
    }

    /**
     * Obtém o valor da propriedade distance.
     * 
     * @return
     *     possible object is
     *     {@link DistanceType }
     *     
     */
    public DistanceType getDistance() {
        return distance;
    }

    /**
     * Define o valor da propriedade distance.
     * 
     * @param value
     *     allowed object is
     *     {@link DistanceType }
     *     
     */
    public void setDistance(DistanceType value) {
        this.distance = value;
    }

    /**
     * Obtém o valor da propriedade weight.
     * 
     * @return
     *     possible object is
     *     {@link WeightType }
     *     
     */
    public WeightType getWeight() {
        return weight;
    }

    /**
     * Define o valor da propriedade weight.
     * 
     * @param value
     *     allowed object is
     *     {@link WeightType }
     *     
     */
    public void setWeight(WeightType value) {
        this.weight = value;
    }

    /**
     * Obtém o valor da propriedade volume.
     * 
     * @return
     *     possible object is
     *     {@link VolumeType }
     *     
     */
    public VolumeType getVolume() {
        return volume;
    }

    /**
     * Define o valor da propriedade volume.
     * 
     * @param value
     *     allowed object is
     *     {@link VolumeType }
     *     
     */
    public void setVolume(VolumeType value) {
        this.volume = value;
    }

    /**
     * Obtém o valor da propriedade itemCount.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItemCount() {
        return itemCount;
    }

    /**
     * Define o valor da propriedade itemCount.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItemCount(String value) {
        this.itemCount = value;
    }

    /**
     * Obtém o valor da propriedade shipUnitCount.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipUnitCount() {
        return shipUnitCount;
    }

    /**
     * Define o valor da propriedade shipUnitCount.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipUnitCount(String value) {
        this.shipUnitCount = value;
    }

    /**
     * Gets the value of the remark property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the remark property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRemark().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RemarkType }
     * 
     * 
     */
    public List<RemarkType> getRemark() {
        if (remark == null) {
            remark = new ArrayList<RemarkType>();
        }
        return this.remark;
    }

    /**
     * Obtém o valor da propriedade serviceCompletionState.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceCompletionState() {
        return serviceCompletionState;
    }

    /**
     * Define o valor da propriedade serviceCompletionState.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceCompletionState(String value) {
        this.serviceCompletionState = value;
    }

}
