
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Inspection and survey information.
 * 
 * <p>Classe Java de InspectionAndSurveyInfoType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="InspectionAndSurveyInfoType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="InspectionNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InspectionRequired" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InspectionScheduleDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="InspectionDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="SurveyLayCanDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InspectionAndSurveyInfoType", propOrder = {
    "inspectionNumber",
    "inspectionRequired",
    "inspectionScheduleDt",
    "inspectionDt",
    "surveyLayCanDt"
})
public class InspectionAndSurveyInfoType {

    @XmlElement(name = "InspectionNumber")
    protected String inspectionNumber;
    @XmlElement(name = "InspectionRequired")
    protected String inspectionRequired;
    @XmlElement(name = "InspectionScheduleDt")
    protected GLogDateTimeType inspectionScheduleDt;
    @XmlElement(name = "InspectionDt")
    protected GLogDateTimeType inspectionDt;
    @XmlElement(name = "SurveyLayCanDt")
    protected GLogDateTimeType surveyLayCanDt;

    /**
     * Obtém o valor da propriedade inspectionNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInspectionNumber() {
        return inspectionNumber;
    }

    /**
     * Define o valor da propriedade inspectionNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInspectionNumber(String value) {
        this.inspectionNumber = value;
    }

    /**
     * Obtém o valor da propriedade inspectionRequired.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInspectionRequired() {
        return inspectionRequired;
    }

    /**
     * Define o valor da propriedade inspectionRequired.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInspectionRequired(String value) {
        this.inspectionRequired = value;
    }

    /**
     * Obtém o valor da propriedade inspectionScheduleDt.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getInspectionScheduleDt() {
        return inspectionScheduleDt;
    }

    /**
     * Define o valor da propriedade inspectionScheduleDt.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setInspectionScheduleDt(GLogDateTimeType value) {
        this.inspectionScheduleDt = value;
    }

    /**
     * Obtém o valor da propriedade inspectionDt.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getInspectionDt() {
        return inspectionDt;
    }

    /**
     * Define o valor da propriedade inspectionDt.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setInspectionDt(GLogDateTimeType value) {
        this.inspectionDt = value;
    }

    /**
     * Obtém o valor da propriedade surveyLayCanDt.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getSurveyLayCanDt() {
        return surveyLayCanDt;
    }

    /**
     * Define o valor da propriedade surveyLayCanDt.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setSurveyLayCanDt(GLogDateTimeType value) {
        this.surveyLayCanDt = value;
    }

}
