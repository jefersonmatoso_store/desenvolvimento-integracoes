
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * A SkuTransactionDescriptor provides additional detailed information for a particular
 *             SkuTransaction.
 *          
 * 
 * <p>Classe Java de SkuTransactionDescriptorType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="SkuTransactionDescriptorType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SkuDescriptorSeq" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SkuDescriptorType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SkuDescriptorValue" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Quantity1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Quantity2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ParentSkuDescriptorSeq" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="UserDefinedXml" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Remark" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RemarkType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SkuTransactionDescriptorType", propOrder = {
    "skuDescriptorSeq",
    "skuDescriptorType",
    "skuDescriptorValue",
    "quantity1",
    "quantity2",
    "parentSkuDescriptorSeq",
    "userDefinedXml",
    "remark"
})
public class SkuTransactionDescriptorType {

    @XmlElement(name = "SkuDescriptorSeq", required = true)
    protected String skuDescriptorSeq;
    @XmlElement(name = "SkuDescriptorType")
    protected String skuDescriptorType;
    @XmlElement(name = "SkuDescriptorValue")
    protected String skuDescriptorValue;
    @XmlElement(name = "Quantity1")
    protected String quantity1;
    @XmlElement(name = "Quantity2")
    protected String quantity2;
    @XmlElement(name = "ParentSkuDescriptorSeq")
    protected String parentSkuDescriptorSeq;
    @XmlElement(name = "UserDefinedXml")
    protected String userDefinedXml;
    @XmlElement(name = "Remark")
    protected List<RemarkType> remark;

    /**
     * Obtém o valor da propriedade skuDescriptorSeq.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSkuDescriptorSeq() {
        return skuDescriptorSeq;
    }

    /**
     * Define o valor da propriedade skuDescriptorSeq.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSkuDescriptorSeq(String value) {
        this.skuDescriptorSeq = value;
    }

    /**
     * Obtém o valor da propriedade skuDescriptorType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSkuDescriptorType() {
        return skuDescriptorType;
    }

    /**
     * Define o valor da propriedade skuDescriptorType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSkuDescriptorType(String value) {
        this.skuDescriptorType = value;
    }

    /**
     * Obtém o valor da propriedade skuDescriptorValue.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSkuDescriptorValue() {
        return skuDescriptorValue;
    }

    /**
     * Define o valor da propriedade skuDescriptorValue.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSkuDescriptorValue(String value) {
        this.skuDescriptorValue = value;
    }

    /**
     * Obtém o valor da propriedade quantity1.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQuantity1() {
        return quantity1;
    }

    /**
     * Define o valor da propriedade quantity1.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQuantity1(String value) {
        this.quantity1 = value;
    }

    /**
     * Obtém o valor da propriedade quantity2.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQuantity2() {
        return quantity2;
    }

    /**
     * Define o valor da propriedade quantity2.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQuantity2(String value) {
        this.quantity2 = value;
    }

    /**
     * Obtém o valor da propriedade parentSkuDescriptorSeq.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getParentSkuDescriptorSeq() {
        return parentSkuDescriptorSeq;
    }

    /**
     * Define o valor da propriedade parentSkuDescriptorSeq.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setParentSkuDescriptorSeq(String value) {
        this.parentSkuDescriptorSeq = value;
    }

    /**
     * Obtém o valor da propriedade userDefinedXml.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserDefinedXml() {
        return userDefinedXml;
    }

    /**
     * Define o valor da propriedade userDefinedXml.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserDefinedXml(String value) {
        this.userDefinedXml = value;
    }

    /**
     * Gets the value of the remark property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the remark property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRemark().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RemarkType }
     * 
     * 
     */
    public List<RemarkType> getRemark() {
        if (remark == null) {
            remark = new ArrayList<RemarkType>();
        }
        return this.remark;
    }

}
