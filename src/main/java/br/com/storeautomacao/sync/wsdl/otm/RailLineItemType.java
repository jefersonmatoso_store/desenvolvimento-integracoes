
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * 
 *             Provides line item invoice data specific to rail carriers.
 *          
 * 
 * <p>Classe Java de RailLineItemType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="RailLineItemType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AssignedNum" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="LineItemRefNum" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LineItemRefNumType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="CompartmentIDCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CommonInvoiceLineElements" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}CommonInvoiceLineElementsType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RailLineItemType", propOrder = {
    "assignedNum",
    "lineItemRefNum",
    "compartmentIDCode",
    "commonInvoiceLineElements"
})
public class RailLineItemType {

    @XmlElement(name = "AssignedNum", required = true)
    protected String assignedNum;
    @XmlElement(name = "LineItemRefNum")
    protected List<LineItemRefNumType> lineItemRefNum;
    @XmlElement(name = "CompartmentIDCode")
    protected String compartmentIDCode;
    @XmlElement(name = "CommonInvoiceLineElements", required = true)
    protected CommonInvoiceLineElementsType commonInvoiceLineElements;

    /**
     * Obtém o valor da propriedade assignedNum.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAssignedNum() {
        return assignedNum;
    }

    /**
     * Define o valor da propriedade assignedNum.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAssignedNum(String value) {
        this.assignedNum = value;
    }

    /**
     * Gets the value of the lineItemRefNum property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the lineItemRefNum property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLineItemRefNum().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LineItemRefNumType }
     * 
     * 
     */
    public List<LineItemRefNumType> getLineItemRefNum() {
        if (lineItemRefNum == null) {
            lineItemRefNum = new ArrayList<LineItemRefNumType>();
        }
        return this.lineItemRefNum;
    }

    /**
     * Obtém o valor da propriedade compartmentIDCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCompartmentIDCode() {
        return compartmentIDCode;
    }

    /**
     * Define o valor da propriedade compartmentIDCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCompartmentIDCode(String value) {
        this.compartmentIDCode = value;
    }

    /**
     * Obtém o valor da propriedade commonInvoiceLineElements.
     * 
     * @return
     *     possible object is
     *     {@link CommonInvoiceLineElementsType }
     *     
     */
    public CommonInvoiceLineElementsType getCommonInvoiceLineElements() {
        return commonInvoiceLineElements;
    }

    /**
     * Define o valor da propriedade commonInvoiceLineElements.
     * 
     * @param value
     *     allowed object is
     *     {@link CommonInvoiceLineElementsType }
     *     
     */
    public void setCommonInvoiceLineElements(CommonInvoiceLineElementsType value) {
        this.commonInvoiceLineElements = value;
    }

}
