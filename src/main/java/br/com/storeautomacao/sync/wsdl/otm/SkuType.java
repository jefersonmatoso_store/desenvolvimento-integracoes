
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;


/**
 * 
 *             Corresponds to the sku table.
 *          
 * 
 * <p>Classe Java de SkuType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="SkuType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OTMTransactionInOut">
 *       &lt;sequence>
 *         &lt;element name="SkuGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="TransactionCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransactionCodeType"/>
 *         &lt;element name="ReplaceChildren" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ReplaceChildrenType" minOccurs="0"/>
 *         &lt;choice>
 *           &lt;sequence>
 *             &lt;element name="SkuObjectType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *             &lt;element name="SkuObjectGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *           &lt;/sequence>
 *         &lt;/choice>
 *         &lt;element name="WarehouseLocationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="SupplierCorporationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="OwnerCorporationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="ShipUnitSpecGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="LastInventoryDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="AverageAge" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Indicator" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SkuQuantity" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}SkuQuantityType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="SkuLevel" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}SkuLevelType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="SkuCost" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}SkuCostType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="SkuDescriptor" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}SkuDescriptorType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Status" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}StatusType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="InvolvedParty" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}InvolvedPartyType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="SkuQuantityAsset" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}SkuQuantityAssetType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SkuType", propOrder = {
    "skuGid",
    "transactionCode",
    "replaceChildren",
    "skuObjectType",
    "skuObjectGid",
    "warehouseLocationGid",
    "supplierCorporationGid",
    "ownerCorporationGid",
    "shipUnitSpecGid",
    "lastInventoryDt",
    "averageAge",
    "description",
    "indicator",
    "skuQuantity",
    "skuLevel",
    "skuCost",
    "skuDescriptor",
    "status",
    "involvedParty",
    "skuQuantityAsset"
})
public class SkuType
    extends OTMTransactionInOut
{

    @XmlElement(name = "SkuGid", required = true)
    protected GLogXMLGidType skuGid;
    @XmlElement(name = "TransactionCode", required = true)
    @XmlSchemaType(name = "string")
    protected TransactionCodeType transactionCode;
    @XmlElement(name = "ReplaceChildren")
    protected ReplaceChildrenType replaceChildren;
    @XmlElement(name = "SkuObjectType")
    protected String skuObjectType;
    @XmlElement(name = "SkuObjectGid")
    protected GLogXMLGidType skuObjectGid;
    @XmlElement(name = "WarehouseLocationGid", required = true)
    protected GLogXMLGidType warehouseLocationGid;
    @XmlElement(name = "SupplierCorporationGid")
    protected GLogXMLGidType supplierCorporationGid;
    @XmlElement(name = "OwnerCorporationGid")
    protected GLogXMLGidType ownerCorporationGid;
    @XmlElement(name = "ShipUnitSpecGid")
    protected GLogXMLGidType shipUnitSpecGid;
    @XmlElement(name = "LastInventoryDt")
    protected GLogDateTimeType lastInventoryDt;
    @XmlElement(name = "AverageAge")
    protected String averageAge;
    @XmlElement(name = "Description")
    protected String description;
    @XmlElement(name = "Indicator")
    protected String indicator;
    @XmlElement(name = "SkuQuantity")
    protected List<SkuQuantityType> skuQuantity;
    @XmlElement(name = "SkuLevel")
    protected List<SkuLevelType> skuLevel;
    @XmlElement(name = "SkuCost")
    protected List<SkuCostType> skuCost;
    @XmlElement(name = "SkuDescriptor")
    protected List<SkuDescriptorType> skuDescriptor;
    @XmlElement(name = "Status")
    protected List<StatusType> status;
    @XmlElement(name = "InvolvedParty")
    protected List<InvolvedPartyType> involvedParty;
    @XmlElement(name = "SkuQuantityAsset")
    protected List<SkuQuantityAssetType> skuQuantityAsset;

    /**
     * Obtém o valor da propriedade skuGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getSkuGid() {
        return skuGid;
    }

    /**
     * Define o valor da propriedade skuGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setSkuGid(GLogXMLGidType value) {
        this.skuGid = value;
    }

    /**
     * Obtém o valor da propriedade transactionCode.
     * 
     * @return
     *     possible object is
     *     {@link TransactionCodeType }
     *     
     */
    public TransactionCodeType getTransactionCode() {
        return transactionCode;
    }

    /**
     * Define o valor da propriedade transactionCode.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionCodeType }
     *     
     */
    public void setTransactionCode(TransactionCodeType value) {
        this.transactionCode = value;
    }

    /**
     * Obtém o valor da propriedade replaceChildren.
     * 
     * @return
     *     possible object is
     *     {@link ReplaceChildrenType }
     *     
     */
    public ReplaceChildrenType getReplaceChildren() {
        return replaceChildren;
    }

    /**
     * Define o valor da propriedade replaceChildren.
     * 
     * @param value
     *     allowed object is
     *     {@link ReplaceChildrenType }
     *     
     */
    public void setReplaceChildren(ReplaceChildrenType value) {
        this.replaceChildren = value;
    }

    /**
     * Obtém o valor da propriedade skuObjectType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSkuObjectType() {
        return skuObjectType;
    }

    /**
     * Define o valor da propriedade skuObjectType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSkuObjectType(String value) {
        this.skuObjectType = value;
    }

    /**
     * Obtém o valor da propriedade skuObjectGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getSkuObjectGid() {
        return skuObjectGid;
    }

    /**
     * Define o valor da propriedade skuObjectGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setSkuObjectGid(GLogXMLGidType value) {
        this.skuObjectGid = value;
    }

    /**
     * Obtém o valor da propriedade warehouseLocationGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getWarehouseLocationGid() {
        return warehouseLocationGid;
    }

    /**
     * Define o valor da propriedade warehouseLocationGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setWarehouseLocationGid(GLogXMLGidType value) {
        this.warehouseLocationGid = value;
    }

    /**
     * Obtém o valor da propriedade supplierCorporationGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getSupplierCorporationGid() {
        return supplierCorporationGid;
    }

    /**
     * Define o valor da propriedade supplierCorporationGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setSupplierCorporationGid(GLogXMLGidType value) {
        this.supplierCorporationGid = value;
    }

    /**
     * Obtém o valor da propriedade ownerCorporationGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getOwnerCorporationGid() {
        return ownerCorporationGid;
    }

    /**
     * Define o valor da propriedade ownerCorporationGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setOwnerCorporationGid(GLogXMLGidType value) {
        this.ownerCorporationGid = value;
    }

    /**
     * Obtém o valor da propriedade shipUnitSpecGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getShipUnitSpecGid() {
        return shipUnitSpecGid;
    }

    /**
     * Define o valor da propriedade shipUnitSpecGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setShipUnitSpecGid(GLogXMLGidType value) {
        this.shipUnitSpecGid = value;
    }

    /**
     * Obtém o valor da propriedade lastInventoryDt.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getLastInventoryDt() {
        return lastInventoryDt;
    }

    /**
     * Define o valor da propriedade lastInventoryDt.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setLastInventoryDt(GLogDateTimeType value) {
        this.lastInventoryDt = value;
    }

    /**
     * Obtém o valor da propriedade averageAge.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAverageAge() {
        return averageAge;
    }

    /**
     * Define o valor da propriedade averageAge.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAverageAge(String value) {
        this.averageAge = value;
    }

    /**
     * Obtém o valor da propriedade description.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Define o valor da propriedade description.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Obtém o valor da propriedade indicator.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIndicator() {
        return indicator;
    }

    /**
     * Define o valor da propriedade indicator.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIndicator(String value) {
        this.indicator = value;
    }

    /**
     * Gets the value of the skuQuantity property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the skuQuantity property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSkuQuantity().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SkuQuantityType }
     * 
     * 
     */
    public List<SkuQuantityType> getSkuQuantity() {
        if (skuQuantity == null) {
            skuQuantity = new ArrayList<SkuQuantityType>();
        }
        return this.skuQuantity;
    }

    /**
     * Gets the value of the skuLevel property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the skuLevel property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSkuLevel().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SkuLevelType }
     * 
     * 
     */
    public List<SkuLevelType> getSkuLevel() {
        if (skuLevel == null) {
            skuLevel = new ArrayList<SkuLevelType>();
        }
        return this.skuLevel;
    }

    /**
     * Gets the value of the skuCost property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the skuCost property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSkuCost().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SkuCostType }
     * 
     * 
     */
    public List<SkuCostType> getSkuCost() {
        if (skuCost == null) {
            skuCost = new ArrayList<SkuCostType>();
        }
        return this.skuCost;
    }

    /**
     * Gets the value of the skuDescriptor property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the skuDescriptor property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSkuDescriptor().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SkuDescriptorType }
     * 
     * 
     */
    public List<SkuDescriptorType> getSkuDescriptor() {
        if (skuDescriptor == null) {
            skuDescriptor = new ArrayList<SkuDescriptorType>();
        }
        return this.skuDescriptor;
    }

    /**
     * Gets the value of the status property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the status property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getStatus().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link StatusType }
     * 
     * 
     */
    public List<StatusType> getStatus() {
        if (status == null) {
            status = new ArrayList<StatusType>();
        }
        return this.status;
    }

    /**
     * Gets the value of the involvedParty property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the involvedParty property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInvolvedParty().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link InvolvedPartyType }
     * 
     * 
     */
    public List<InvolvedPartyType> getInvolvedParty() {
        if (involvedParty == null) {
            involvedParty = new ArrayList<InvolvedPartyType>();
        }
        return this.involvedParty;
    }

    /**
     * Gets the value of the skuQuantityAsset property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the skuQuantityAsset property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSkuQuantityAsset().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SkuQuantityAssetType }
     * 
     * 
     */
    public List<SkuQuantityAssetType> getSkuQuantityAsset() {
        if (skuQuantityAsset == null) {
            skuQuantityAsset = new ArrayList<SkuQuantityAssetType>();
        }
        return this.skuQuantityAsset;
    }

}
