
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Classe Java de TransactionAckType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="TransactionAckType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OTMTransactionIn">
 *       &lt;sequence>
 *         &lt;element name="ReferencedSendReason" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="ProcessControlRequestID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="SendReasonGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *                   &lt;element name="ObjectType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="ITransactionNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SenderTransactionId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AckCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AckReason" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Refnum" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RefnumType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TransactionAckType", propOrder = {
    "referencedSendReason",
    "iTransactionNo",
    "senderTransactionId",
    "ackCode",
    "ackReason",
    "refnum"
})
public class TransactionAckType
    extends OTMTransactionIn
{

    @XmlElement(name = "ReferencedSendReason")
    protected ReferencedSendReason referencedSendReason;
    @XmlElement(name = "ITransactionNo", required = true)
    protected String iTransactionNo;
    @XmlElement(name = "SenderTransactionId")
    protected String senderTransactionId;
    @XmlElement(name = "AckCode", required = true)
    protected String ackCode;
    @XmlElement(name = "AckReason")
    protected String ackReason;
    @XmlElement(name = "Refnum")
    protected List<RefnumType> refnum;

    /**
     * Obtém o valor da propriedade referencedSendReason.
     * 
     * @return
     *     possible object is
     *     {@link ReferencedSendReason }
     *     
     */
    public ReferencedSendReason getReferencedSendReason() {
        return referencedSendReason;
    }

    /**
     * Define o valor da propriedade referencedSendReason.
     * 
     * @param value
     *     allowed object is
     *     {@link ReferencedSendReason }
     *     
     */
    public void setReferencedSendReason(ReferencedSendReason value) {
        this.referencedSendReason = value;
    }

    /**
     * Obtém o valor da propriedade iTransactionNo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getITransactionNo() {
        return iTransactionNo;
    }

    /**
     * Define o valor da propriedade iTransactionNo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setITransactionNo(String value) {
        this.iTransactionNo = value;
    }

    /**
     * Obtém o valor da propriedade senderTransactionId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSenderTransactionId() {
        return senderTransactionId;
    }

    /**
     * Define o valor da propriedade senderTransactionId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSenderTransactionId(String value) {
        this.senderTransactionId = value;
    }

    /**
     * Obtém o valor da propriedade ackCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAckCode() {
        return ackCode;
    }

    /**
     * Define o valor da propriedade ackCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAckCode(String value) {
        this.ackCode = value;
    }

    /**
     * Obtém o valor da propriedade ackReason.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAckReason() {
        return ackReason;
    }

    /**
     * Define o valor da propriedade ackReason.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAckReason(String value) {
        this.ackReason = value;
    }

    /**
     * Gets the value of the refnum property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the refnum property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRefnum().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RefnumType }
     * 
     * 
     */
    public List<RefnumType> getRefnum() {
        if (refnum == null) {
            refnum = new ArrayList<RefnumType>();
        }
        return this.refnum;
    }


    /**
     * <p>Classe Java de anonymous complex type.
     * 
     * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="ProcessControlRequestID" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="SendReasonGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
     *         &lt;element name="ObjectType" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "processControlRequestID",
        "sendReasonGid",
        "objectType"
    })
    public static class ReferencedSendReason {

        @XmlElement(name = "ProcessControlRequestID", required = true)
        protected String processControlRequestID;
        @XmlElement(name = "SendReasonGid", required = true)
        protected GLogXMLGidType sendReasonGid;
        @XmlElement(name = "ObjectType", required = true)
        protected String objectType;

        /**
         * Obtém o valor da propriedade processControlRequestID.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getProcessControlRequestID() {
            return processControlRequestID;
        }

        /**
         * Define o valor da propriedade processControlRequestID.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setProcessControlRequestID(String value) {
            this.processControlRequestID = value;
        }

        /**
         * Obtém o valor da propriedade sendReasonGid.
         * 
         * @return
         *     possible object is
         *     {@link GLogXMLGidType }
         *     
         */
        public GLogXMLGidType getSendReasonGid() {
            return sendReasonGid;
        }

        /**
         * Define o valor da propriedade sendReasonGid.
         * 
         * @param value
         *     allowed object is
         *     {@link GLogXMLGidType }
         *     
         */
        public void setSendReasonGid(GLogXMLGidType value) {
            this.sendReasonGid = value;
        }

        /**
         * Obtém o valor da propriedade objectType.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getObjectType() {
            return objectType;
        }

        /**
         * Define o valor da propriedade objectType.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setObjectType(String value) {
            this.objectType = value;
        }

    }

}
