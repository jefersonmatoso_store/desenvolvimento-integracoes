
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.*;


/**
 * Used to specify the use of a text template.
 *             On inbound, either TextTemplateGid or TextTemplate can be specified.
 *             On outbound, only TextTemplateGid is provided.
 *             TextPatternOverride is inbound only.
 *             TextInstance is outbound only.
 *             TextPatternOverride and TextItem are currently applicable only to Documents.
 *          
 * 
 * <p>Classe Java de TextType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="TextType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;choice minOccurs="0">
 *           &lt;element name="TextTemplateGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *           &lt;element name="TextTemplate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TextTemplateType" minOccurs="0"/>
 *         &lt;/choice>
 *         &lt;element name="TextDocumentDefinition" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TextDocumentDefinitionType" minOccurs="0"/>
 *         &lt;element name="TextPatternOverride" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TextItem" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TextItemType" minOccurs="0"/>
 *         &lt;element name="TextInstance" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TransactionCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransactionCodeType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TextType", propOrder = {
    "textTemplateGid",
    "textTemplate",
    "textDocumentDefinition",
    "textPatternOverride",
    "textItem",
    "textInstance",
    "transactionCode"
})
public class TextType {

    @XmlElement(name = "TextTemplateGid")
    protected GLogXMLGidType textTemplateGid;
    @XmlElement(name = "TextTemplate")
    protected TextTemplateType textTemplate;
    @XmlElement(name = "TextDocumentDefinition")
    protected TextDocumentDefinitionType textDocumentDefinition;
    @XmlElement(name = "TextPatternOverride")
    protected String textPatternOverride;
    @XmlElement(name = "TextItem")
    protected TextItemType textItem;
    @XmlElement(name = "TextInstance")
    protected String textInstance;
    @XmlElement(name = "TransactionCode")
    @XmlSchemaType(name = "string")
    protected TransactionCodeType transactionCode;

    /**
     * Obtém o valor da propriedade textTemplateGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getTextTemplateGid() {
        return textTemplateGid;
    }

    /**
     * Define o valor da propriedade textTemplateGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setTextTemplateGid(GLogXMLGidType value) {
        this.textTemplateGid = value;
    }

    /**
     * Obtém o valor da propriedade textTemplate.
     * 
     * @return
     *     possible object is
     *     {@link TextTemplateType }
     *     
     */
    public TextTemplateType getTextTemplate() {
        return textTemplate;
    }

    /**
     * Define o valor da propriedade textTemplate.
     * 
     * @param value
     *     allowed object is
     *     {@link TextTemplateType }
     *     
     */
    public void setTextTemplate(TextTemplateType value) {
        this.textTemplate = value;
    }

    /**
     * Obtém o valor da propriedade textDocumentDefinition.
     * 
     * @return
     *     possible object is
     *     {@link TextDocumentDefinitionType }
     *     
     */
    public TextDocumentDefinitionType getTextDocumentDefinition() {
        return textDocumentDefinition;
    }

    /**
     * Define o valor da propriedade textDocumentDefinition.
     * 
     * @param value
     *     allowed object is
     *     {@link TextDocumentDefinitionType }
     *     
     */
    public void setTextDocumentDefinition(TextDocumentDefinitionType value) {
        this.textDocumentDefinition = value;
    }

    /**
     * Obtém o valor da propriedade textPatternOverride.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTextPatternOverride() {
        return textPatternOverride;
    }

    /**
     * Define o valor da propriedade textPatternOverride.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTextPatternOverride(String value) {
        this.textPatternOverride = value;
    }

    /**
     * Obtém o valor da propriedade textItem.
     * 
     * @return
     *     possible object is
     *     {@link TextItemType }
     *     
     */
    public TextItemType getTextItem() {
        return textItem;
    }

    /**
     * Define o valor da propriedade textItem.
     * 
     * @param value
     *     allowed object is
     *     {@link TextItemType }
     *     
     */
    public void setTextItem(TextItemType value) {
        this.textItem = value;
    }

    /**
     * Obtém o valor da propriedade textInstance.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTextInstance() {
        return textInstance;
    }

    /**
     * Define o valor da propriedade textInstance.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTextInstance(String value) {
        this.textInstance = value;
    }

    /**
     * Obtém o valor da propriedade transactionCode.
     * 
     * @return
     *     possible object is
     *     {@link TransactionCodeType }
     *     
     */
    public TransactionCodeType getTransactionCode() {
        return transactionCode;
    }

    /**
     * Define o valor da propriedade transactionCode.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionCodeType }
     *     
     */
    public void setTransactionCode(TransactionCodeType value) {
        this.transactionCode = value;
    }

}
