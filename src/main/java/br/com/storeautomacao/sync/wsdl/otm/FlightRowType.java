
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.*;


/**
 * <p>Classe Java de FlightRowType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="FlightRowType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FLIGHT_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FLIGHT_XID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MAJOR_RECORD_TYPE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IS_AIR_CARGO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LEAVE_TIME_AFTER_MIDNIGHT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ARRIVAL_TIME_AFTER_MIDNIGHT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ELAPSED_TIME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FLIGHT_CATEGORY" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SRC_IATA_CITY_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DEST_IATA_CITY_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DAY_ADJUSTMENT_COUNTER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EFFECTIVE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DISCONTINUE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FREQUENCY" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TRAFFIC_RESTRICTION" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SRC_LOCATION_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DEST_LOCATION_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AR_TM_AFTER_MIDNIGHT_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AR_TM_AFTER_MIDNIGHT_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ELAPSED_TM_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ELAPSED_TM_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LV_TM_AFTER_MIDNIGHT_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LV_TM_AFTER_MIDNIGHT_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FlightRowType", propOrder = {
    "flightgid",
    "flightxid",
    "majorrecordtype",
    "isaircargo",
    "leavetimeaftermidnight",
    "arrivaltimeaftermidnight",
    "elapsedtime",
    "flightcategory",
    "srciatacitycode",
    "destiatacitycode",
    "dayadjustmentcounter",
    "effectivedate",
    "discontinuedate",
    "frequency",
    "trafficrestriction",
    "srclocationgid",
    "destlocationgid",
    "domainname",
    "artmaftermidnightuomcode",
    "artmaftermidnightbase",
    "elapsedtmuomcode",
    "elapsedtmbase",
    "lvtmaftermidnightuomcode",
    "lvtmaftermidnightbase",
    "insertdate",
    "updatedate",
    "insertuser",
    "updateuser"
})
public class FlightRowType {

    @XmlElement(name = "FLIGHT_GID")
    protected String flightgid;
    @XmlElement(name = "FLIGHT_XID")
    protected String flightxid;
    @XmlElement(name = "MAJOR_RECORD_TYPE")
    protected String majorrecordtype;
    @XmlElement(name = "IS_AIR_CARGO")
    protected String isaircargo;
    @XmlElement(name = "LEAVE_TIME_AFTER_MIDNIGHT")
    protected String leavetimeaftermidnight;
    @XmlElement(name = "ARRIVAL_TIME_AFTER_MIDNIGHT")
    protected String arrivaltimeaftermidnight;
    @XmlElement(name = "ELAPSED_TIME")
    protected String elapsedtime;
    @XmlElement(name = "FLIGHT_CATEGORY")
    protected String flightcategory;
    @XmlElement(name = "SRC_IATA_CITY_CODE")
    protected String srciatacitycode;
    @XmlElement(name = "DEST_IATA_CITY_CODE")
    protected String destiatacitycode;
    @XmlElement(name = "DAY_ADJUSTMENT_COUNTER")
    protected String dayadjustmentcounter;
    @XmlElement(name = "EFFECTIVE_DATE")
    protected String effectivedate;
    @XmlElement(name = "DISCONTINUE_DATE")
    protected String discontinuedate;
    @XmlElement(name = "FREQUENCY")
    protected String frequency;
    @XmlElement(name = "TRAFFIC_RESTRICTION")
    protected String trafficrestriction;
    @XmlElement(name = "SRC_LOCATION_GID")
    protected String srclocationgid;
    @XmlElement(name = "DEST_LOCATION_GID")
    protected String destlocationgid;
    @XmlElement(name = "DOMAIN_NAME")
    protected String domainname;
    @XmlElement(name = "AR_TM_AFTER_MIDNIGHT_UOM_CODE")
    protected String artmaftermidnightuomcode;
    @XmlElement(name = "AR_TM_AFTER_MIDNIGHT_BASE")
    protected String artmaftermidnightbase;
    @XmlElement(name = "ELAPSED_TM_UOM_CODE")
    protected String elapsedtmuomcode;
    @XmlElement(name = "ELAPSED_TM_BASE")
    protected String elapsedtmbase;
    @XmlElement(name = "LV_TM_AFTER_MIDNIGHT_UOM_CODE")
    protected String lvtmaftermidnightuomcode;
    @XmlElement(name = "LV_TM_AFTER_MIDNIGHT_BASE")
    protected String lvtmaftermidnightbase;
    @XmlElement(name = "INSERT_DATE")
    protected String insertdate;
    @XmlElement(name = "UPDATE_DATE")
    protected String updatedate;
    @XmlElement(name = "INSERT_USER")
    protected String insertuser;
    @XmlElement(name = "UPDATE_USER")
    protected String updateuser;
    @XmlAttribute(name = "num")
    protected String num;

    /**
     * Obtém o valor da propriedade flightgid.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFLIGHTGID() {
        return flightgid;
    }

    /**
     * Define o valor da propriedade flightgid.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFLIGHTGID(String value) {
        this.flightgid = value;
    }

    /**
     * Obtém o valor da propriedade flightxid.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFLIGHTXID() {
        return flightxid;
    }

    /**
     * Define o valor da propriedade flightxid.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFLIGHTXID(String value) {
        this.flightxid = value;
    }

    /**
     * Obtém o valor da propriedade majorrecordtype.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMAJORRECORDTYPE() {
        return majorrecordtype;
    }

    /**
     * Define o valor da propriedade majorrecordtype.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMAJORRECORDTYPE(String value) {
        this.majorrecordtype = value;
    }

    /**
     * Obtém o valor da propriedade isaircargo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getISAIRCARGO() {
        return isaircargo;
    }

    /**
     * Define o valor da propriedade isaircargo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setISAIRCARGO(String value) {
        this.isaircargo = value;
    }

    /**
     * Obtém o valor da propriedade leavetimeaftermidnight.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLEAVETIMEAFTERMIDNIGHT() {
        return leavetimeaftermidnight;
    }

    /**
     * Define o valor da propriedade leavetimeaftermidnight.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLEAVETIMEAFTERMIDNIGHT(String value) {
        this.leavetimeaftermidnight = value;
    }

    /**
     * Obtém o valor da propriedade arrivaltimeaftermidnight.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getARRIVALTIMEAFTERMIDNIGHT() {
        return arrivaltimeaftermidnight;
    }

    /**
     * Define o valor da propriedade arrivaltimeaftermidnight.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setARRIVALTIMEAFTERMIDNIGHT(String value) {
        this.arrivaltimeaftermidnight = value;
    }

    /**
     * Obtém o valor da propriedade elapsedtime.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getELAPSEDTIME() {
        return elapsedtime;
    }

    /**
     * Define o valor da propriedade elapsedtime.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setELAPSEDTIME(String value) {
        this.elapsedtime = value;
    }

    /**
     * Obtém o valor da propriedade flightcategory.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFLIGHTCATEGORY() {
        return flightcategory;
    }

    /**
     * Define o valor da propriedade flightcategory.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFLIGHTCATEGORY(String value) {
        this.flightcategory = value;
    }

    /**
     * Obtém o valor da propriedade srciatacitycode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSRCIATACITYCODE() {
        return srciatacitycode;
    }

    /**
     * Define o valor da propriedade srciatacitycode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSRCIATACITYCODE(String value) {
        this.srciatacitycode = value;
    }

    /**
     * Obtém o valor da propriedade destiatacitycode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDESTIATACITYCODE() {
        return destiatacitycode;
    }

    /**
     * Define o valor da propriedade destiatacitycode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDESTIATACITYCODE(String value) {
        this.destiatacitycode = value;
    }

    /**
     * Obtém o valor da propriedade dayadjustmentcounter.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDAYADJUSTMENTCOUNTER() {
        return dayadjustmentcounter;
    }

    /**
     * Define o valor da propriedade dayadjustmentcounter.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDAYADJUSTMENTCOUNTER(String value) {
        this.dayadjustmentcounter = value;
    }

    /**
     * Obtém o valor da propriedade effectivedate.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEFFECTIVEDATE() {
        return effectivedate;
    }

    /**
     * Define o valor da propriedade effectivedate.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEFFECTIVEDATE(String value) {
        this.effectivedate = value;
    }

    /**
     * Obtém o valor da propriedade discontinuedate.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDISCONTINUEDATE() {
        return discontinuedate;
    }

    /**
     * Define o valor da propriedade discontinuedate.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDISCONTINUEDATE(String value) {
        this.discontinuedate = value;
    }

    /**
     * Obtém o valor da propriedade frequency.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFREQUENCY() {
        return frequency;
    }

    /**
     * Define o valor da propriedade frequency.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFREQUENCY(String value) {
        this.frequency = value;
    }

    /**
     * Obtém o valor da propriedade trafficrestriction.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTRAFFICRESTRICTION() {
        return trafficrestriction;
    }

    /**
     * Define o valor da propriedade trafficrestriction.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTRAFFICRESTRICTION(String value) {
        this.trafficrestriction = value;
    }

    /**
     * Obtém o valor da propriedade srclocationgid.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSRCLOCATIONGID() {
        return srclocationgid;
    }

    /**
     * Define o valor da propriedade srclocationgid.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSRCLOCATIONGID(String value) {
        this.srclocationgid = value;
    }

    /**
     * Obtém o valor da propriedade destlocationgid.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDESTLOCATIONGID() {
        return destlocationgid;
    }

    /**
     * Define o valor da propriedade destlocationgid.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDESTLOCATIONGID(String value) {
        this.destlocationgid = value;
    }

    /**
     * Obtém o valor da propriedade domainname.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDOMAINNAME() {
        return domainname;
    }

    /**
     * Define o valor da propriedade domainname.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDOMAINNAME(String value) {
        this.domainname = value;
    }

    /**
     * Obtém o valor da propriedade artmaftermidnightuomcode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getARTMAFTERMIDNIGHTUOMCODE() {
        return artmaftermidnightuomcode;
    }

    /**
     * Define o valor da propriedade artmaftermidnightuomcode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setARTMAFTERMIDNIGHTUOMCODE(String value) {
        this.artmaftermidnightuomcode = value;
    }

    /**
     * Obtém o valor da propriedade artmaftermidnightbase.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getARTMAFTERMIDNIGHTBASE() {
        return artmaftermidnightbase;
    }

    /**
     * Define o valor da propriedade artmaftermidnightbase.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setARTMAFTERMIDNIGHTBASE(String value) {
        this.artmaftermidnightbase = value;
    }

    /**
     * Obtém o valor da propriedade elapsedtmuomcode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getELAPSEDTMUOMCODE() {
        return elapsedtmuomcode;
    }

    /**
     * Define o valor da propriedade elapsedtmuomcode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setELAPSEDTMUOMCODE(String value) {
        this.elapsedtmuomcode = value;
    }

    /**
     * Obtém o valor da propriedade elapsedtmbase.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getELAPSEDTMBASE() {
        return elapsedtmbase;
    }

    /**
     * Define o valor da propriedade elapsedtmbase.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setELAPSEDTMBASE(String value) {
        this.elapsedtmbase = value;
    }

    /**
     * Obtém o valor da propriedade lvtmaftermidnightuomcode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLVTMAFTERMIDNIGHTUOMCODE() {
        return lvtmaftermidnightuomcode;
    }

    /**
     * Define o valor da propriedade lvtmaftermidnightuomcode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLVTMAFTERMIDNIGHTUOMCODE(String value) {
        this.lvtmaftermidnightuomcode = value;
    }

    /**
     * Obtém o valor da propriedade lvtmaftermidnightbase.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLVTMAFTERMIDNIGHTBASE() {
        return lvtmaftermidnightbase;
    }

    /**
     * Define o valor da propriedade lvtmaftermidnightbase.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLVTMAFTERMIDNIGHTBASE(String value) {
        this.lvtmaftermidnightbase = value;
    }

    /**
     * Obtém o valor da propriedade insertdate.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getINSERTDATE() {
        return insertdate;
    }

    /**
     * Define o valor da propriedade insertdate.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setINSERTDATE(String value) {
        this.insertdate = value;
    }

    /**
     * Obtém o valor da propriedade updatedate.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUPDATEDATE() {
        return updatedate;
    }

    /**
     * Define o valor da propriedade updatedate.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUPDATEDATE(String value) {
        this.updatedate = value;
    }

    /**
     * Obtém o valor da propriedade insertuser.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getINSERTUSER() {
        return insertuser;
    }

    /**
     * Define o valor da propriedade insertuser.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setINSERTUSER(String value) {
        this.insertuser = value;
    }

    /**
     * Obtém o valor da propriedade updateuser.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUPDATEUSER() {
        return updateuser;
    }

    /**
     * Define o valor da propriedade updateuser.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUPDATEUSER(String value) {
        this.updateuser = value;
    }

    /**
     * Obtém o valor da propriedade num.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNum() {
        return num;
    }

    /**
     * Define o valor da propriedade num.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNum(String value) {
        this.num = value;
    }

}
