
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de LocationActivityTimeDefType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="LocationActivityTimeDefType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element name="ActivityTimeDefGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="ActivityTimeDef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ActivityTimeDefType"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LocationActivityTimeDefType", propOrder = {
    "activityTimeDefGid",
    "activityTimeDef"
})
public class LocationActivityTimeDefType {

    @XmlElement(name = "ActivityTimeDefGid")
    protected GLogXMLGidType activityTimeDefGid;
    @XmlElement(name = "ActivityTimeDef")
    protected ActivityTimeDefType activityTimeDef;

    /**
     * Obtém o valor da propriedade activityTimeDefGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getActivityTimeDefGid() {
        return activityTimeDefGid;
    }

    /**
     * Define o valor da propriedade activityTimeDefGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setActivityTimeDefGid(GLogXMLGidType value) {
        this.activityTimeDefGid = value;
    }

    /**
     * Obtém o valor da propriedade activityTimeDef.
     * 
     * @return
     *     possible object is
     *     {@link ActivityTimeDefType }
     *     
     */
    public ActivityTimeDefType getActivityTimeDef() {
        return activityTimeDef;
    }

    /**
     * Define o valor da propriedade activityTimeDef.
     * 
     * @param value
     *     allowed object is
     *     {@link ActivityTimeDefType }
     *     
     */
    public void setActivityTimeDef(ActivityTimeDefType value) {
        this.activityTimeDef = value;
    }

}
