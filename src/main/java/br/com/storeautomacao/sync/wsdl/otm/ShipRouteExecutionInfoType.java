
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Specifies the route execution information.
 *             The OnRouteExecution element is outbound only.
 *             When the AssignShipmentToLeg element is Y, the RouteTemplateGid, RouteInstanceGid, RouteInstanceLegGid, and LegNum are used to determine the appropriate route instance
 *             leg to assign the shipment.
 *          
 * 
 * <p>Classe Java de ShipRouteExecutionInfoType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="ShipRouteExecutionInfoType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AssignShipmentToLeg" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RouteTemplateGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="RouteInstanceGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="RouteInstanceLegGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="LegNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ShipRouteExecutionInfoType", propOrder = {
    "assignShipmentToLeg",
    "routeTemplateGid",
    "routeInstanceGid",
    "routeInstanceLegGid",
    "legNum"
})
public class ShipRouteExecutionInfoType {

    @XmlElement(name = "AssignShipmentToLeg")
    protected String assignShipmentToLeg;
    @XmlElement(name = "RouteTemplateGid")
    protected GLogXMLGidType routeTemplateGid;
    @XmlElement(name = "RouteInstanceGid")
    protected GLogXMLGidType routeInstanceGid;
    @XmlElement(name = "RouteInstanceLegGid")
    protected GLogXMLGidType routeInstanceLegGid;
    @XmlElement(name = "LegNum")
    protected String legNum;

    /**
     * Obtém o valor da propriedade assignShipmentToLeg.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAssignShipmentToLeg() {
        return assignShipmentToLeg;
    }

    /**
     * Define o valor da propriedade assignShipmentToLeg.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAssignShipmentToLeg(String value) {
        this.assignShipmentToLeg = value;
    }

    /**
     * Obtém o valor da propriedade routeTemplateGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRouteTemplateGid() {
        return routeTemplateGid;
    }

    /**
     * Define o valor da propriedade routeTemplateGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRouteTemplateGid(GLogXMLGidType value) {
        this.routeTemplateGid = value;
    }

    /**
     * Obtém o valor da propriedade routeInstanceGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRouteInstanceGid() {
        return routeInstanceGid;
    }

    /**
     * Define o valor da propriedade routeInstanceGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRouteInstanceGid(GLogXMLGidType value) {
        this.routeInstanceGid = value;
    }

    /**
     * Obtém o valor da propriedade routeInstanceLegGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRouteInstanceLegGid() {
        return routeInstanceLegGid;
    }

    /**
     * Define o valor da propriedade routeInstanceLegGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRouteInstanceLegGid(GLogXMLGidType value) {
        this.routeInstanceLegGid = value;
    }

    /**
     * Obtém o valor da propriedade legNum.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLegNum() {
        return legNum;
    }

    /**
     * Define o valor da propriedade legNum.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLegNum(String value) {
        this.legNum = value;
    }

}
