
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * 
 *             (Both) Payment is a common xml structure used to represent invoices, bills, and vouchers.
 *          
 * 
 * <p>Classe Java de PaymentType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="PaymentType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PaymentHeader" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}PaymentHeaderType"/>
 *         &lt;element name="PaymentModeDetail" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}PaymentModeDetailType" minOccurs="0"/>
 *         &lt;element name="PaymentSummary" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}PaymentSummaryType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="ChildPayments" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ChildPaymentsType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PaymentType", propOrder = {
    "paymentHeader",
    "paymentModeDetail",
    "paymentSummary",
    "childPayments"
})
public class PaymentType {

    @XmlElement(name = "PaymentHeader", required = true)
    protected PaymentHeaderType paymentHeader;
    @XmlElement(name = "PaymentModeDetail")
    protected PaymentModeDetailType paymentModeDetail;
    @XmlElement(name = "PaymentSummary")
    protected List<PaymentSummaryType> paymentSummary;
    @XmlElement(name = "ChildPayments")
    protected ChildPaymentsType childPayments;

    /**
     * Obtém o valor da propriedade paymentHeader.
     * 
     * @return
     *     possible object is
     *     {@link PaymentHeaderType }
     *     
     */
    public PaymentHeaderType getPaymentHeader() {
        return paymentHeader;
    }

    /**
     * Define o valor da propriedade paymentHeader.
     * 
     * @param value
     *     allowed object is
     *     {@link PaymentHeaderType }
     *     
     */
    public void setPaymentHeader(PaymentHeaderType value) {
        this.paymentHeader = value;
    }

    /**
     * Obtém o valor da propriedade paymentModeDetail.
     * 
     * @return
     *     possible object is
     *     {@link PaymentModeDetailType }
     *     
     */
    public PaymentModeDetailType getPaymentModeDetail() {
        return paymentModeDetail;
    }

    /**
     * Define o valor da propriedade paymentModeDetail.
     * 
     * @param value
     *     allowed object is
     *     {@link PaymentModeDetailType }
     *     
     */
    public void setPaymentModeDetail(PaymentModeDetailType value) {
        this.paymentModeDetail = value;
    }

    /**
     * Gets the value of the paymentSummary property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the paymentSummary property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPaymentSummary().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PaymentSummaryType }
     * 
     * 
     */
    public List<PaymentSummaryType> getPaymentSummary() {
        if (paymentSummary == null) {
            paymentSummary = new ArrayList<PaymentSummaryType>();
        }
        return this.paymentSummary;
    }

    /**
     * Obtém o valor da propriedade childPayments.
     * 
     * @return
     *     possible object is
     *     {@link ChildPaymentsType }
     *     
     */
    public ChildPaymentsType getChildPayments() {
        return childPayments;
    }

    /**
     * Define o valor da propriedade childPayments.
     * 
     * @param value
     *     allowed object is
     *     {@link ChildPaymentsType }
     *     
     */
    public void setChildPayments(ChildPaymentsType value) {
        this.childPayments = value;
    }

}
