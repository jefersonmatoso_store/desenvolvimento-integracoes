
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 *             CommercialInvoiceDataIndicates the commercial value * unit count.
 *          
 * 
 * <p>Classe Java de CommercialInvoiceDataType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="CommercialInvoiceDataType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CommercialUnitPrice" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/>
 *         &lt;element name="CommercialUnitPriceQualifier" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LineItemTotalCommercialValue" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/>
 *         &lt;element name="UnitCount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PackagedItemSpecRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLShipUnitSpecRefType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CommercialInvoiceDataType", propOrder = {
    "commercialUnitPrice",
    "commercialUnitPriceQualifier",
    "lineItemTotalCommercialValue",
    "unitCount",
    "packagedItemSpecRef"
})
public class CommercialInvoiceDataType {

    @XmlElement(name = "CommercialUnitPrice")
    protected GLogXMLFinancialAmountType commercialUnitPrice;
    @XmlElement(name = "CommercialUnitPriceQualifier")
    protected String commercialUnitPriceQualifier;
    @XmlElement(name = "LineItemTotalCommercialValue")
    protected GLogXMLFinancialAmountType lineItemTotalCommercialValue;
    @XmlElement(name = "UnitCount")
    protected String unitCount;
    @XmlElement(name = "PackagedItemSpecRef")
    protected GLogXMLShipUnitSpecRefType packagedItemSpecRef;

    /**
     * Obtém o valor da propriedade commercialUnitPrice.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getCommercialUnitPrice() {
        return commercialUnitPrice;
    }

    /**
     * Define o valor da propriedade commercialUnitPrice.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setCommercialUnitPrice(GLogXMLFinancialAmountType value) {
        this.commercialUnitPrice = value;
    }

    /**
     * Obtém o valor da propriedade commercialUnitPriceQualifier.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCommercialUnitPriceQualifier() {
        return commercialUnitPriceQualifier;
    }

    /**
     * Define o valor da propriedade commercialUnitPriceQualifier.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCommercialUnitPriceQualifier(String value) {
        this.commercialUnitPriceQualifier = value;
    }

    /**
     * Obtém o valor da propriedade lineItemTotalCommercialValue.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getLineItemTotalCommercialValue() {
        return lineItemTotalCommercialValue;
    }

    /**
     * Define o valor da propriedade lineItemTotalCommercialValue.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setLineItemTotalCommercialValue(GLogXMLFinancialAmountType value) {
        this.lineItemTotalCommercialValue = value;
    }

    /**
     * Obtém o valor da propriedade unitCount.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUnitCount() {
        return unitCount;
    }

    /**
     * Define o valor da propriedade unitCount.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUnitCount(String value) {
        this.unitCount = value;
    }

    /**
     * Obtém o valor da propriedade packagedItemSpecRef.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLShipUnitSpecRefType }
     *     
     */
    public GLogXMLShipUnitSpecRefType getPackagedItemSpecRef() {
        return packagedItemSpecRef;
    }

    /**
     * Define o valor da propriedade packagedItemSpecRef.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLShipUnitSpecRefType }
     *     
     */
    public void setPackagedItemSpecRef(GLogXMLShipUnitSpecRefType value) {
        this.packagedItemSpecRef = value;
    }

}
