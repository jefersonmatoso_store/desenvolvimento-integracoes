
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * CommunicationMethod is a structure for describing when and how to communicate
 *             with a business partner.
 *          
 * 
 * <p>Classe Java de CommunicationMethodType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="CommunicationMethodType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ComMethodRank" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ComMethodGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="CalendarGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="ExpectedResponseTime" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDurationType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CommunicationMethodType", propOrder = {
    "comMethodRank",
    "comMethodGid",
    "calendarGid",
    "expectedResponseTime"
})
public class CommunicationMethodType {

    @XmlElement(name = "ComMethodRank", required = true)
    protected String comMethodRank;
    @XmlElement(name = "ComMethodGid", required = true)
    protected GLogXMLGidType comMethodGid;
    @XmlElement(name = "CalendarGid")
    protected GLogXMLGidType calendarGid;
    @XmlElement(name = "ExpectedResponseTime")
    protected GLogXMLDurationType expectedResponseTime;

    /**
     * Obtém o valor da propriedade comMethodRank.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComMethodRank() {
        return comMethodRank;
    }

    /**
     * Define o valor da propriedade comMethodRank.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComMethodRank(String value) {
        this.comMethodRank = value;
    }

    /**
     * Obtém o valor da propriedade comMethodGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getComMethodGid() {
        return comMethodGid;
    }

    /**
     * Define o valor da propriedade comMethodGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setComMethodGid(GLogXMLGidType value) {
        this.comMethodGid = value;
    }

    /**
     * Obtém o valor da propriedade calendarGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getCalendarGid() {
        return calendarGid;
    }

    /**
     * Define o valor da propriedade calendarGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setCalendarGid(GLogXMLGidType value) {
        this.calendarGid = value;
    }

    /**
     * Obtém o valor da propriedade expectedResponseTime.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public GLogXMLDurationType getExpectedResponseTime() {
        return expectedResponseTime;
    }

    /**
     * Define o valor da propriedade expectedResponseTime.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public void setExpectedResponseTime(GLogXMLDurationType value) {
        this.expectedResponseTime = value;
    }

}
