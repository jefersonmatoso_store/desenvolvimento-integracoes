
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;


/**
 * Equipment is a structure representing the equipment assigned to a shipment.
 * 
 * <p>Classe Java de EquipmentType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="EquipmentType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OTMTransactionInOut">
 *       &lt;sequence>
 *         &lt;element name="IntSavedQuery" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}IntSavedQueryType" minOccurs="0"/>
 *         &lt;element name="EquipmentGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="TransactionCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransactionCodeType"/>
 *         &lt;element name="ReplaceChildren" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ReplaceChildrenType" minOccurs="0"/>
 *         &lt;element name="EquipmentName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EquipmentInitial" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EquipmentNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EquipmentInitialNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EquipmentTypeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="EquipmentGroupGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="EquipmentSeal" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}EquipmentSealType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="EquipmentAttribute" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}EquipmentAttributeType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="OwnershipCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EquipmentOwner" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OwnerType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AARCarType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="WeightQualifier" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ScaleWeight" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLWeightType" minOccurs="0"/>
 *         &lt;element name="TareWeight" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLWeightType" minOccurs="0"/>
 *         &lt;element name="ScaleLocation" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ScaleName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ScaleTicket" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IntermodalEquipLength" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LesseeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="LicenseState" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LicensePlate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EquipmentTag1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EquipmentTag2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EquipmentTag3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EquipmentTag4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DateBuilt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="PurchasedDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="EquipmentRegistrationNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EquipmentRegistrationDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="SightingLocGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="SightingDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="PrevSightingLocGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="PrevSightingDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="InterchangeRecvLocGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="InterchangeRecvDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="ParkLocationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="Indicator" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsContainer" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ChassisInitial" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ChassisNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LastOutgateLocGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="LastOutgateTime" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="LastIngateTime" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="UserDefIconInfo" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}UserDefIconInfoType" minOccurs="0"/>
 *         &lt;element name="Refnum" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RefnumType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Remark" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RemarkType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Status" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}StatusType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="EquipmentSpecialService" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}EquipmentSpecialServiceType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="FlexFieldStrings" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldStringType" minOccurs="0"/>
 *         &lt;element name="FlexFieldNumbers" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldNumberType" minOccurs="0"/>
 *         &lt;element name="FlexFieldDates" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldDateType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EquipmentType", propOrder = {
    "intSavedQuery",
    "equipmentGid",
    "transactionCode",
    "replaceChildren",
    "equipmentName",
    "equipmentInitial",
    "equipmentNumber",
    "equipmentInitialNumber",
    "equipmentTypeGid",
    "equipmentGroupGid",
    "equipmentSeal",
    "equipmentAttribute",
    "ownershipCode",
    "equipmentOwner",
    "ownerType",
    "aarCarType",
    "weightQualifier",
    "scaleWeight",
    "tareWeight",
    "scaleLocation",
    "scaleName",
    "scaleTicket",
    "intermodalEquipLength",
    "lesseeGid",
    "licenseState",
    "licensePlate",
    "equipmentTag1",
    "equipmentTag2",
    "equipmentTag3",
    "equipmentTag4",
    "dateBuilt",
    "purchasedDate",
    "equipmentRegistrationNum",
    "equipmentRegistrationDate",
    "sightingLocGid",
    "sightingDate",
    "prevSightingLocGid",
    "prevSightingDate",
    "interchangeRecvLocGid",
    "interchangeRecvDate",
    "parkLocationGid",
    "indicator",
    "isContainer",
    "chassisInitial",
    "chassisNumber",
    "lastOutgateLocGid",
    "lastOutgateTime",
    "lastIngateTime",
    "userDefIconInfo",
    "refnum",
    "remark",
    "status",
    "equipmentSpecialService",
    "flexFieldStrings",
    "flexFieldNumbers",
    "flexFieldDates"
})
public class EquipmentType
    extends OTMTransactionInOut
{

    @XmlElement(name = "IntSavedQuery")
    protected IntSavedQueryType intSavedQuery;
    @XmlElement(name = "EquipmentGid", required = true)
    protected GLogXMLGidType equipmentGid;
    @XmlElement(name = "TransactionCode", required = true)
    @XmlSchemaType(name = "string")
    protected TransactionCodeType transactionCode;
    @XmlElement(name = "ReplaceChildren")
    protected ReplaceChildrenType replaceChildren;
    @XmlElement(name = "EquipmentName")
    protected String equipmentName;
    @XmlElement(name = "EquipmentInitial")
    protected String equipmentInitial;
    @XmlElement(name = "EquipmentNumber")
    protected String equipmentNumber;
    @XmlElement(name = "EquipmentInitialNumber")
    protected String equipmentInitialNumber;
    @XmlElement(name = "EquipmentTypeGid")
    protected GLogXMLGidType equipmentTypeGid;
    @XmlElement(name = "EquipmentGroupGid")
    protected GLogXMLGidType equipmentGroupGid;
    @XmlElement(name = "EquipmentSeal")
    protected List<EquipmentSealType> equipmentSeal;
    @XmlElement(name = "EquipmentAttribute")
    protected List<EquipmentAttributeType> equipmentAttribute;
    @XmlElement(name = "OwnershipCode")
    protected String ownershipCode;
    @XmlElement(name = "EquipmentOwner")
    protected String equipmentOwner;
    @XmlElement(name = "OwnerType")
    protected String ownerType;
    @XmlElement(name = "AARCarType")
    protected String aarCarType;
    @XmlElement(name = "WeightQualifier")
    protected String weightQualifier;
    @XmlElement(name = "ScaleWeight")
    protected GLogXMLWeightType scaleWeight;
    @XmlElement(name = "TareWeight")
    protected GLogXMLWeightType tareWeight;
    @XmlElement(name = "ScaleLocation")
    protected String scaleLocation;
    @XmlElement(name = "ScaleName")
    protected String scaleName;
    @XmlElement(name = "ScaleTicket")
    protected String scaleTicket;
    @XmlElement(name = "IntermodalEquipLength")
    protected String intermodalEquipLength;
    @XmlElement(name = "LesseeGid")
    protected GLogXMLGidType lesseeGid;
    @XmlElement(name = "LicenseState")
    protected String licenseState;
    @XmlElement(name = "LicensePlate")
    protected String licensePlate;
    @XmlElement(name = "EquipmentTag1")
    protected String equipmentTag1;
    @XmlElement(name = "EquipmentTag2")
    protected String equipmentTag2;
    @XmlElement(name = "EquipmentTag3")
    protected String equipmentTag3;
    @XmlElement(name = "EquipmentTag4")
    protected String equipmentTag4;
    @XmlElement(name = "DateBuilt")
    protected GLogDateTimeType dateBuilt;
    @XmlElement(name = "PurchasedDate")
    protected GLogDateTimeType purchasedDate;
    @XmlElement(name = "EquipmentRegistrationNum")
    protected String equipmentRegistrationNum;
    @XmlElement(name = "EquipmentRegistrationDate")
    protected GLogDateTimeType equipmentRegistrationDate;
    @XmlElement(name = "SightingLocGid")
    protected GLogXMLGidType sightingLocGid;
    @XmlElement(name = "SightingDate")
    protected GLogDateTimeType sightingDate;
    @XmlElement(name = "PrevSightingLocGid")
    protected GLogXMLGidType prevSightingLocGid;
    @XmlElement(name = "PrevSightingDate")
    protected GLogDateTimeType prevSightingDate;
    @XmlElement(name = "InterchangeRecvLocGid")
    protected GLogXMLGidType interchangeRecvLocGid;
    @XmlElement(name = "InterchangeRecvDate")
    protected GLogDateTimeType interchangeRecvDate;
    @XmlElement(name = "ParkLocationGid")
    protected GLogXMLGidType parkLocationGid;
    @XmlElement(name = "Indicator")
    protected String indicator;
    @XmlElement(name = "IsContainer")
    protected String isContainer;
    @XmlElement(name = "ChassisInitial")
    protected String chassisInitial;
    @XmlElement(name = "ChassisNumber")
    protected String chassisNumber;
    @XmlElement(name = "LastOutgateLocGid")
    protected GLogXMLGidType lastOutgateLocGid;
    @XmlElement(name = "LastOutgateTime")
    protected GLogDateTimeType lastOutgateTime;
    @XmlElement(name = "LastIngateTime")
    protected GLogDateTimeType lastIngateTime;
    @XmlElement(name = "UserDefIconInfo")
    protected UserDefIconInfoType userDefIconInfo;
    @XmlElement(name = "Refnum")
    protected List<RefnumType> refnum;
    @XmlElement(name = "Remark")
    protected List<RemarkType> remark;
    @XmlElement(name = "Status")
    protected List<StatusType> status;
    @XmlElement(name = "EquipmentSpecialService")
    protected List<EquipmentSpecialServiceType> equipmentSpecialService;
    @XmlElement(name = "FlexFieldStrings")
    protected FlexFieldStringType flexFieldStrings;
    @XmlElement(name = "FlexFieldNumbers")
    protected FlexFieldNumberType flexFieldNumbers;
    @XmlElement(name = "FlexFieldDates")
    protected FlexFieldDateType flexFieldDates;

    /**
     * Obtém o valor da propriedade intSavedQuery.
     * 
     * @return
     *     possible object is
     *     {@link IntSavedQueryType }
     *     
     */
    public IntSavedQueryType getIntSavedQuery() {
        return intSavedQuery;
    }

    /**
     * Define o valor da propriedade intSavedQuery.
     * 
     * @param value
     *     allowed object is
     *     {@link IntSavedQueryType }
     *     
     */
    public void setIntSavedQuery(IntSavedQueryType value) {
        this.intSavedQuery = value;
    }

    /**
     * Obtém o valor da propriedade equipmentGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getEquipmentGid() {
        return equipmentGid;
    }

    /**
     * Define o valor da propriedade equipmentGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setEquipmentGid(GLogXMLGidType value) {
        this.equipmentGid = value;
    }

    /**
     * Obtém o valor da propriedade transactionCode.
     * 
     * @return
     *     possible object is
     *     {@link TransactionCodeType }
     *     
     */
    public TransactionCodeType getTransactionCode() {
        return transactionCode;
    }

    /**
     * Define o valor da propriedade transactionCode.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionCodeType }
     *     
     */
    public void setTransactionCode(TransactionCodeType value) {
        this.transactionCode = value;
    }

    /**
     * Obtém o valor da propriedade replaceChildren.
     * 
     * @return
     *     possible object is
     *     {@link ReplaceChildrenType }
     *     
     */
    public ReplaceChildrenType getReplaceChildren() {
        return replaceChildren;
    }

    /**
     * Define o valor da propriedade replaceChildren.
     * 
     * @param value
     *     allowed object is
     *     {@link ReplaceChildrenType }
     *     
     */
    public void setReplaceChildren(ReplaceChildrenType value) {
        this.replaceChildren = value;
    }

    /**
     * Obtém o valor da propriedade equipmentName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEquipmentName() {
        return equipmentName;
    }

    /**
     * Define o valor da propriedade equipmentName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEquipmentName(String value) {
        this.equipmentName = value;
    }

    /**
     * Obtém o valor da propriedade equipmentInitial.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEquipmentInitial() {
        return equipmentInitial;
    }

    /**
     * Define o valor da propriedade equipmentInitial.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEquipmentInitial(String value) {
        this.equipmentInitial = value;
    }

    /**
     * Obtém o valor da propriedade equipmentNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEquipmentNumber() {
        return equipmentNumber;
    }

    /**
     * Define o valor da propriedade equipmentNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEquipmentNumber(String value) {
        this.equipmentNumber = value;
    }

    /**
     * Obtém o valor da propriedade equipmentInitialNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEquipmentInitialNumber() {
        return equipmentInitialNumber;
    }

    /**
     * Define o valor da propriedade equipmentInitialNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEquipmentInitialNumber(String value) {
        this.equipmentInitialNumber = value;
    }

    /**
     * Obtém o valor da propriedade equipmentTypeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getEquipmentTypeGid() {
        return equipmentTypeGid;
    }

    /**
     * Define o valor da propriedade equipmentTypeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setEquipmentTypeGid(GLogXMLGidType value) {
        this.equipmentTypeGid = value;
    }

    /**
     * Obtém o valor da propriedade equipmentGroupGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getEquipmentGroupGid() {
        return equipmentGroupGid;
    }

    /**
     * Define o valor da propriedade equipmentGroupGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setEquipmentGroupGid(GLogXMLGidType value) {
        this.equipmentGroupGid = value;
    }

    /**
     * Gets the value of the equipmentSeal property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the equipmentSeal property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEquipmentSeal().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EquipmentSealType }
     * 
     * 
     */
    public List<EquipmentSealType> getEquipmentSeal() {
        if (equipmentSeal == null) {
            equipmentSeal = new ArrayList<EquipmentSealType>();
        }
        return this.equipmentSeal;
    }

    /**
     * Gets the value of the equipmentAttribute property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the equipmentAttribute property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEquipmentAttribute().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EquipmentAttributeType }
     * 
     * 
     */
    public List<EquipmentAttributeType> getEquipmentAttribute() {
        if (equipmentAttribute == null) {
            equipmentAttribute = new ArrayList<EquipmentAttributeType>();
        }
        return this.equipmentAttribute;
    }

    /**
     * Obtém o valor da propriedade ownershipCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOwnershipCode() {
        return ownershipCode;
    }

    /**
     * Define o valor da propriedade ownershipCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOwnershipCode(String value) {
        this.ownershipCode = value;
    }

    /**
     * Obtém o valor da propriedade equipmentOwner.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEquipmentOwner() {
        return equipmentOwner;
    }

    /**
     * Define o valor da propriedade equipmentOwner.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEquipmentOwner(String value) {
        this.equipmentOwner = value;
    }

    /**
     * Obtém o valor da propriedade ownerType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOwnerType() {
        return ownerType;
    }

    /**
     * Define o valor da propriedade ownerType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOwnerType(String value) {
        this.ownerType = value;
    }

    /**
     * Obtém o valor da propriedade aarCarType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAARCarType() {
        return aarCarType;
    }

    /**
     * Define o valor da propriedade aarCarType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAARCarType(String value) {
        this.aarCarType = value;
    }

    /**
     * Obtém o valor da propriedade weightQualifier.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWeightQualifier() {
        return weightQualifier;
    }

    /**
     * Define o valor da propriedade weightQualifier.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWeightQualifier(String value) {
        this.weightQualifier = value;
    }

    /**
     * Obtém o valor da propriedade scaleWeight.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLWeightType }
     *     
     */
    public GLogXMLWeightType getScaleWeight() {
        return scaleWeight;
    }

    /**
     * Define o valor da propriedade scaleWeight.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLWeightType }
     *     
     */
    public void setScaleWeight(GLogXMLWeightType value) {
        this.scaleWeight = value;
    }

    /**
     * Obtém o valor da propriedade tareWeight.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLWeightType }
     *     
     */
    public GLogXMLWeightType getTareWeight() {
        return tareWeight;
    }

    /**
     * Define o valor da propriedade tareWeight.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLWeightType }
     *     
     */
    public void setTareWeight(GLogXMLWeightType value) {
        this.tareWeight = value;
    }

    /**
     * Obtém o valor da propriedade scaleLocation.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getScaleLocation() {
        return scaleLocation;
    }

    /**
     * Define o valor da propriedade scaleLocation.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setScaleLocation(String value) {
        this.scaleLocation = value;
    }

    /**
     * Obtém o valor da propriedade scaleName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getScaleName() {
        return scaleName;
    }

    /**
     * Define o valor da propriedade scaleName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setScaleName(String value) {
        this.scaleName = value;
    }

    /**
     * Obtém o valor da propriedade scaleTicket.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getScaleTicket() {
        return scaleTicket;
    }

    /**
     * Define o valor da propriedade scaleTicket.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setScaleTicket(String value) {
        this.scaleTicket = value;
    }

    /**
     * Obtém o valor da propriedade intermodalEquipLength.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIntermodalEquipLength() {
        return intermodalEquipLength;
    }

    /**
     * Define o valor da propriedade intermodalEquipLength.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIntermodalEquipLength(String value) {
        this.intermodalEquipLength = value;
    }

    /**
     * Obtém o valor da propriedade lesseeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getLesseeGid() {
        return lesseeGid;
    }

    /**
     * Define o valor da propriedade lesseeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setLesseeGid(GLogXMLGidType value) {
        this.lesseeGid = value;
    }

    /**
     * Obtém o valor da propriedade licenseState.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLicenseState() {
        return licenseState;
    }

    /**
     * Define o valor da propriedade licenseState.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLicenseState(String value) {
        this.licenseState = value;
    }

    /**
     * Obtém o valor da propriedade licensePlate.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLicensePlate() {
        return licensePlate;
    }

    /**
     * Define o valor da propriedade licensePlate.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLicensePlate(String value) {
        this.licensePlate = value;
    }

    /**
     * Obtém o valor da propriedade equipmentTag1.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEquipmentTag1() {
        return equipmentTag1;
    }

    /**
     * Define o valor da propriedade equipmentTag1.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEquipmentTag1(String value) {
        this.equipmentTag1 = value;
    }

    /**
     * Obtém o valor da propriedade equipmentTag2.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEquipmentTag2() {
        return equipmentTag2;
    }

    /**
     * Define o valor da propriedade equipmentTag2.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEquipmentTag2(String value) {
        this.equipmentTag2 = value;
    }

    /**
     * Obtém o valor da propriedade equipmentTag3.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEquipmentTag3() {
        return equipmentTag3;
    }

    /**
     * Define o valor da propriedade equipmentTag3.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEquipmentTag3(String value) {
        this.equipmentTag3 = value;
    }

    /**
     * Obtém o valor da propriedade equipmentTag4.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEquipmentTag4() {
        return equipmentTag4;
    }

    /**
     * Define o valor da propriedade equipmentTag4.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEquipmentTag4(String value) {
        this.equipmentTag4 = value;
    }

    /**
     * Obtém o valor da propriedade dateBuilt.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getDateBuilt() {
        return dateBuilt;
    }

    /**
     * Define o valor da propriedade dateBuilt.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setDateBuilt(GLogDateTimeType value) {
        this.dateBuilt = value;
    }

    /**
     * Obtém o valor da propriedade purchasedDate.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getPurchasedDate() {
        return purchasedDate;
    }

    /**
     * Define o valor da propriedade purchasedDate.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setPurchasedDate(GLogDateTimeType value) {
        this.purchasedDate = value;
    }

    /**
     * Obtém o valor da propriedade equipmentRegistrationNum.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEquipmentRegistrationNum() {
        return equipmentRegistrationNum;
    }

    /**
     * Define o valor da propriedade equipmentRegistrationNum.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEquipmentRegistrationNum(String value) {
        this.equipmentRegistrationNum = value;
    }

    /**
     * Obtém o valor da propriedade equipmentRegistrationDate.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getEquipmentRegistrationDate() {
        return equipmentRegistrationDate;
    }

    /**
     * Define o valor da propriedade equipmentRegistrationDate.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setEquipmentRegistrationDate(GLogDateTimeType value) {
        this.equipmentRegistrationDate = value;
    }

    /**
     * Obtém o valor da propriedade sightingLocGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getSightingLocGid() {
        return sightingLocGid;
    }

    /**
     * Define o valor da propriedade sightingLocGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setSightingLocGid(GLogXMLGidType value) {
        this.sightingLocGid = value;
    }

    /**
     * Obtém o valor da propriedade sightingDate.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getSightingDate() {
        return sightingDate;
    }

    /**
     * Define o valor da propriedade sightingDate.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setSightingDate(GLogDateTimeType value) {
        this.sightingDate = value;
    }

    /**
     * Obtém o valor da propriedade prevSightingLocGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getPrevSightingLocGid() {
        return prevSightingLocGid;
    }

    /**
     * Define o valor da propriedade prevSightingLocGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setPrevSightingLocGid(GLogXMLGidType value) {
        this.prevSightingLocGid = value;
    }

    /**
     * Obtém o valor da propriedade prevSightingDate.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getPrevSightingDate() {
        return prevSightingDate;
    }

    /**
     * Define o valor da propriedade prevSightingDate.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setPrevSightingDate(GLogDateTimeType value) {
        this.prevSightingDate = value;
    }

    /**
     * Obtém o valor da propriedade interchangeRecvLocGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getInterchangeRecvLocGid() {
        return interchangeRecvLocGid;
    }

    /**
     * Define o valor da propriedade interchangeRecvLocGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setInterchangeRecvLocGid(GLogXMLGidType value) {
        this.interchangeRecvLocGid = value;
    }

    /**
     * Obtém o valor da propriedade interchangeRecvDate.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getInterchangeRecvDate() {
        return interchangeRecvDate;
    }

    /**
     * Define o valor da propriedade interchangeRecvDate.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setInterchangeRecvDate(GLogDateTimeType value) {
        this.interchangeRecvDate = value;
    }

    /**
     * Obtém o valor da propriedade parkLocationGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getParkLocationGid() {
        return parkLocationGid;
    }

    /**
     * Define o valor da propriedade parkLocationGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setParkLocationGid(GLogXMLGidType value) {
        this.parkLocationGid = value;
    }

    /**
     * Obtém o valor da propriedade indicator.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIndicator() {
        return indicator;
    }

    /**
     * Define o valor da propriedade indicator.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIndicator(String value) {
        this.indicator = value;
    }

    /**
     * Obtém o valor da propriedade isContainer.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsContainer() {
        return isContainer;
    }

    /**
     * Define o valor da propriedade isContainer.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsContainer(String value) {
        this.isContainer = value;
    }

    /**
     * Obtém o valor da propriedade chassisInitial.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChassisInitial() {
        return chassisInitial;
    }

    /**
     * Define o valor da propriedade chassisInitial.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChassisInitial(String value) {
        this.chassisInitial = value;
    }

    /**
     * Obtém o valor da propriedade chassisNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChassisNumber() {
        return chassisNumber;
    }

    /**
     * Define o valor da propriedade chassisNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChassisNumber(String value) {
        this.chassisNumber = value;
    }

    /**
     * Obtém o valor da propriedade lastOutgateLocGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getLastOutgateLocGid() {
        return lastOutgateLocGid;
    }

    /**
     * Define o valor da propriedade lastOutgateLocGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setLastOutgateLocGid(GLogXMLGidType value) {
        this.lastOutgateLocGid = value;
    }

    /**
     * Obtém o valor da propriedade lastOutgateTime.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getLastOutgateTime() {
        return lastOutgateTime;
    }

    /**
     * Define o valor da propriedade lastOutgateTime.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setLastOutgateTime(GLogDateTimeType value) {
        this.lastOutgateTime = value;
    }

    /**
     * Obtém o valor da propriedade lastIngateTime.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getLastIngateTime() {
        return lastIngateTime;
    }

    /**
     * Define o valor da propriedade lastIngateTime.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setLastIngateTime(GLogDateTimeType value) {
        this.lastIngateTime = value;
    }

    /**
     * Obtém o valor da propriedade userDefIconInfo.
     * 
     * @return
     *     possible object is
     *     {@link UserDefIconInfoType }
     *     
     */
    public UserDefIconInfoType getUserDefIconInfo() {
        return userDefIconInfo;
    }

    /**
     * Define o valor da propriedade userDefIconInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link UserDefIconInfoType }
     *     
     */
    public void setUserDefIconInfo(UserDefIconInfoType value) {
        this.userDefIconInfo = value;
    }

    /**
     * Gets the value of the refnum property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the refnum property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRefnum().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RefnumType }
     * 
     * 
     */
    public List<RefnumType> getRefnum() {
        if (refnum == null) {
            refnum = new ArrayList<RefnumType>();
        }
        return this.refnum;
    }

    /**
     * Gets the value of the remark property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the remark property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRemark().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RemarkType }
     * 
     * 
     */
    public List<RemarkType> getRemark() {
        if (remark == null) {
            remark = new ArrayList<RemarkType>();
        }
        return this.remark;
    }

    /**
     * Gets the value of the status property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the status property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getStatus().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link StatusType }
     * 
     * 
     */
    public List<StatusType> getStatus() {
        if (status == null) {
            status = new ArrayList<StatusType>();
        }
        return this.status;
    }

    /**
     * Gets the value of the equipmentSpecialService property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the equipmentSpecialService property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEquipmentSpecialService().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EquipmentSpecialServiceType }
     * 
     * 
     */
    public List<EquipmentSpecialServiceType> getEquipmentSpecialService() {
        if (equipmentSpecialService == null) {
            equipmentSpecialService = new ArrayList<EquipmentSpecialServiceType>();
        }
        return this.equipmentSpecialService;
    }

    /**
     * Obtém o valor da propriedade flexFieldStrings.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldStringType }
     *     
     */
    public FlexFieldStringType getFlexFieldStrings() {
        return flexFieldStrings;
    }

    /**
     * Define o valor da propriedade flexFieldStrings.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldStringType }
     *     
     */
    public void setFlexFieldStrings(FlexFieldStringType value) {
        this.flexFieldStrings = value;
    }

    /**
     * Obtém o valor da propriedade flexFieldNumbers.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldNumberType }
     *     
     */
    public FlexFieldNumberType getFlexFieldNumbers() {
        return flexFieldNumbers;
    }

    /**
     * Define o valor da propriedade flexFieldNumbers.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldNumberType }
     *     
     */
    public void setFlexFieldNumbers(FlexFieldNumberType value) {
        this.flexFieldNumbers = value;
    }

    /**
     * Obtém o valor da propriedade flexFieldDates.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldDateType }
     *     
     */
    public FlexFieldDateType getFlexFieldDates() {
        return flexFieldDates;
    }

    /**
     * Define o valor da propriedade flexFieldDates.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldDateType }
     *     
     */
    public void setFlexFieldDates(FlexFieldDateType value) {
        this.flexFieldDates = value;
    }

}
