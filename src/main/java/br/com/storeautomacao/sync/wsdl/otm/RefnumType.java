
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Refnum is a reference number for an object. It consists of a qualifier and a value.
 * 
 * <p>Classe Java de RefnumType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="RefnumType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RefnumQualifierGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="RefnumValue" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RefnumType", propOrder = {
    "refnumQualifierGid",
    "refnumValue"
})
public class RefnumType {

    @XmlElement(name = "RefnumQualifierGid", required = true)
    protected GLogXMLGidType refnumQualifierGid;
    @XmlElement(name = "RefnumValue", required = true)
    protected String refnumValue;

    /**
     * Obtém o valor da propriedade refnumQualifierGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRefnumQualifierGid() {
        return refnumQualifierGid;
    }

    /**
     * Define o valor da propriedade refnumQualifierGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRefnumQualifierGid(GLogXMLGidType value) {
        this.refnumQualifierGid = value;
    }

    /**
     * Obtém o valor da propriedade refnumValue.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRefnumValue() {
        return refnumValue;
    }

    /**
     * Define o valor da propriedade refnumValue.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRefnumValue(String value) {
        this.refnumValue = value;
    }

}
