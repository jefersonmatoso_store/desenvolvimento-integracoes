
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 *             LetterofCredit is an element of OceanDetail and GenericDetail used to indicate letter of credit details.
 *          
 * 
 * <p>Classe Java de LetterOfCreditType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="LetterOfCreditType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="LetterOfCreditNum" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="IssuanceDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="ExpirationDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LetterOfCreditType", propOrder = {
    "letterOfCreditNum",
    "issuanceDate",
    "expirationDate"
})
public class LetterOfCreditType {

    @XmlElement(name = "LetterOfCreditNum", required = true)
    protected String letterOfCreditNum;
    @XmlElement(name = "IssuanceDate")
    protected GLogDateTimeType issuanceDate;
    @XmlElement(name = "ExpirationDate")
    protected GLogDateTimeType expirationDate;

    /**
     * Obtém o valor da propriedade letterOfCreditNum.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLetterOfCreditNum() {
        return letterOfCreditNum;
    }

    /**
     * Define o valor da propriedade letterOfCreditNum.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLetterOfCreditNum(String value) {
        this.letterOfCreditNum = value;
    }

    /**
     * Obtém o valor da propriedade issuanceDate.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getIssuanceDate() {
        return issuanceDate;
    }

    /**
     * Define o valor da propriedade issuanceDate.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setIssuanceDate(GLogDateTimeType value) {
        this.issuanceDate = value;
    }

    /**
     * Obtém o valor da propriedade expirationDate.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getExpirationDate() {
        return expirationDate;
    }

    /**
     * Define o valor da propriedade expirationDate.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setExpirationDate(GLogDateTimeType value) {
        this.expirationDate = value;
    }

}
