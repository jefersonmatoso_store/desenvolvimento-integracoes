
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * (Both) This represents user defined type and code for a business object
 *          
 * 
 * <p>Classe Java de CategoryTypeCodeType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="CategoryTypeCodeType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GtmCategory" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="GtmType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="GtmCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CategoryTypeCodeType", namespace = "http://xmlns.oracle.com/apps/gtm/transmission/v6.4", propOrder = {
    "gtmCategory",
    "gtmType",
    "gtmCode"
})
public class CategoryTypeCodeType {

    @XmlElement(name = "GtmCategory")
    protected String gtmCategory;
    @XmlElement(name = "GtmType", required = true)
    protected String gtmType;
    @XmlElement(name = "GtmCode", required = true)
    protected String gtmCode;

    /**
     * Obtém o valor da propriedade gtmCategory.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGtmCategory() {
        return gtmCategory;
    }

    /**
     * Define o valor da propriedade gtmCategory.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGtmCategory(String value) {
        this.gtmCategory = value;
    }

    /**
     * Obtém o valor da propriedade gtmType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGtmType() {
        return gtmType;
    }

    /**
     * Define o valor da propriedade gtmType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGtmType(String value) {
        this.gtmType = value;
    }

    /**
     * Obtém o valor da propriedade gtmCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGtmCode() {
        return gtmCode;
    }

    /**
     * Define o valor da propriedade gtmCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGtmCode(String value) {
        this.gtmCode = value;
    }

}
