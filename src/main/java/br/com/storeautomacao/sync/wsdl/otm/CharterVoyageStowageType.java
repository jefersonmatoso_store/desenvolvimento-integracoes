
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * Represents a compartment within the vessel on a charter voyage. A ocean carrier specifies the available capacities
 *             on the vessel by stowage mode. The shippers provide their stowage mode preferences for their transport orders.
 * 
 *             Note: The VoyageStowageModeGid element has been replaced by the more general StowageModeGid element.
 *             The VoyageStowageModeGid element was added in Release 5.0, and the StowageModeGid element was added in a 5.0 patch.
 *             The VoyageStowageModeGid element was removed in a subsequent patch to clean up the interface after approval from the client.
 *          
 * 
 * <p>Classe Java de CharterVoyageStowageType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="CharterVoyageStowageType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="StowageModeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="SourceTerminalLocationRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocRefType" minOccurs="0"/>
 *         &lt;element name="DestTerminalLocationRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocRefType" minOccurs="0"/>
 *         &lt;element name="TerminalStartTime" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDurationType" minOccurs="0"/>
 *         &lt;element name="TerminalCloseTime" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDurationType" minOccurs="0"/>
 *         &lt;element name="TerminalCloseTimeNonstuff" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDurationType" minOccurs="0"/>
 *         &lt;element name="MaterialAvailTime" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDurationType" minOccurs="0"/>
 *         &lt;element name="MaterialAvailTimeDestuff" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDurationType" minOccurs="0"/>
 *         &lt;element name="BookingReferenceNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Consol" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ConsolType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CharterVoyageStowageType", propOrder = {
    "stowageModeGid",
    "sourceTerminalLocationRef",
    "destTerminalLocationRef",
    "terminalStartTime",
    "terminalCloseTime",
    "terminalCloseTimeNonstuff",
    "materialAvailTime",
    "materialAvailTimeDestuff",
    "bookingReferenceNum",
    "consol"
})
public class CharterVoyageStowageType {

    @XmlElement(name = "StowageModeGid")
    protected GLogXMLGidType stowageModeGid;
    @XmlElement(name = "SourceTerminalLocationRef")
    protected GLogXMLLocRefType sourceTerminalLocationRef;
    @XmlElement(name = "DestTerminalLocationRef")
    protected GLogXMLLocRefType destTerminalLocationRef;
    @XmlElement(name = "TerminalStartTime")
    protected GLogXMLDurationType terminalStartTime;
    @XmlElement(name = "TerminalCloseTime")
    protected GLogXMLDurationType terminalCloseTime;
    @XmlElement(name = "TerminalCloseTimeNonstuff")
    protected GLogXMLDurationType terminalCloseTimeNonstuff;
    @XmlElement(name = "MaterialAvailTime")
    protected GLogXMLDurationType materialAvailTime;
    @XmlElement(name = "MaterialAvailTimeDestuff")
    protected GLogXMLDurationType materialAvailTimeDestuff;
    @XmlElement(name = "BookingReferenceNum")
    protected String bookingReferenceNum;
    @XmlElement(name = "Consol")
    protected List<ConsolType> consol;

    /**
     * Obtém o valor da propriedade stowageModeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getStowageModeGid() {
        return stowageModeGid;
    }

    /**
     * Define o valor da propriedade stowageModeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setStowageModeGid(GLogXMLGidType value) {
        this.stowageModeGid = value;
    }

    /**
     * Obtém o valor da propriedade sourceTerminalLocationRef.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public GLogXMLLocRefType getSourceTerminalLocationRef() {
        return sourceTerminalLocationRef;
    }

    /**
     * Define o valor da propriedade sourceTerminalLocationRef.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public void setSourceTerminalLocationRef(GLogXMLLocRefType value) {
        this.sourceTerminalLocationRef = value;
    }

    /**
     * Obtém o valor da propriedade destTerminalLocationRef.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public GLogXMLLocRefType getDestTerminalLocationRef() {
        return destTerminalLocationRef;
    }

    /**
     * Define o valor da propriedade destTerminalLocationRef.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public void setDestTerminalLocationRef(GLogXMLLocRefType value) {
        this.destTerminalLocationRef = value;
    }

    /**
     * Obtém o valor da propriedade terminalStartTime.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public GLogXMLDurationType getTerminalStartTime() {
        return terminalStartTime;
    }

    /**
     * Define o valor da propriedade terminalStartTime.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public void setTerminalStartTime(GLogXMLDurationType value) {
        this.terminalStartTime = value;
    }

    /**
     * Obtém o valor da propriedade terminalCloseTime.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public GLogXMLDurationType getTerminalCloseTime() {
        return terminalCloseTime;
    }

    /**
     * Define o valor da propriedade terminalCloseTime.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public void setTerminalCloseTime(GLogXMLDurationType value) {
        this.terminalCloseTime = value;
    }

    /**
     * Obtém o valor da propriedade terminalCloseTimeNonstuff.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public GLogXMLDurationType getTerminalCloseTimeNonstuff() {
        return terminalCloseTimeNonstuff;
    }

    /**
     * Define o valor da propriedade terminalCloseTimeNonstuff.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public void setTerminalCloseTimeNonstuff(GLogXMLDurationType value) {
        this.terminalCloseTimeNonstuff = value;
    }

    /**
     * Obtém o valor da propriedade materialAvailTime.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public GLogXMLDurationType getMaterialAvailTime() {
        return materialAvailTime;
    }

    /**
     * Define o valor da propriedade materialAvailTime.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public void setMaterialAvailTime(GLogXMLDurationType value) {
        this.materialAvailTime = value;
    }

    /**
     * Obtém o valor da propriedade materialAvailTimeDestuff.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public GLogXMLDurationType getMaterialAvailTimeDestuff() {
        return materialAvailTimeDestuff;
    }

    /**
     * Define o valor da propriedade materialAvailTimeDestuff.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public void setMaterialAvailTimeDestuff(GLogXMLDurationType value) {
        this.materialAvailTimeDestuff = value;
    }

    /**
     * Obtém o valor da propriedade bookingReferenceNum.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBookingReferenceNum() {
        return bookingReferenceNum;
    }

    /**
     * Define o valor da propriedade bookingReferenceNum.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBookingReferenceNum(String value) {
        this.bookingReferenceNum = value;
    }

    /**
     * Gets the value of the consol property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the consol property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getConsol().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ConsolType }
     * 
     * 
     */
    public List<ConsolType> getConsol() {
        if (consol == null) {
            consol = new ArrayList<ConsolType>();
        }
        return this.consol;
    }

}
