
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * Specifies the appointment information for the shipment stop.
 * 
 * <p>Classe Java de AppointmentType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="AppointmentType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AppointmentInfo" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}AppointmentInfoType"/>
 *         &lt;element name="Remark" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RemarkType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AppointmentType", propOrder = {
    "appointmentInfo",
    "remark"
})
public class AppointmentType {

    @XmlElement(name = "AppointmentInfo", required = true)
    protected AppointmentInfoType appointmentInfo;
    @XmlElement(name = "Remark")
    protected List<RemarkType> remark;

    /**
     * Obtém o valor da propriedade appointmentInfo.
     * 
     * @return
     *     possible object is
     *     {@link AppointmentInfoType }
     *     
     */
    public AppointmentInfoType getAppointmentInfo() {
        return appointmentInfo;
    }

    /**
     * Define o valor da propriedade appointmentInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link AppointmentInfoType }
     *     
     */
    public void setAppointmentInfo(AppointmentInfoType value) {
        this.appointmentInfo = value;
    }

    /**
     * Gets the value of the remark property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the remark property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRemark().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RemarkType }
     * 
     * 
     */
    public List<RemarkType> getRemark() {
        if (remark == null) {
            remark = new ArrayList<RemarkType>();
        }
        return this.remark;
    }

}
