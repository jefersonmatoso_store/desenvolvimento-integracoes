
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de GLogDateTimeType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="GLogDateTimeType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GLogDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="TZId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TZOffset" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GLogDateTimeType", propOrder = {
    "gLogDate",
    "tzId",
    "tzOffset"
})
public class GLogDateTimeType {

    @XmlElement(name = "GLogDate", required = true)
    protected String gLogDate;
    @XmlElement(name = "TZId")
    protected String tzId;
    @XmlElement(name = "TZOffset")
    protected String tzOffset;

    /**
     * Obtém o valor da propriedade gLogDate.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGLogDate() {
        return gLogDate;
    }

    /**
     * Define o valor da propriedade gLogDate.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGLogDate(String value) {
        this.gLogDate = value;
    }

    /**
     * Obtém o valor da propriedade tzId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTZId() {
        return tzId;
    }

    /**
     * Define o valor da propriedade tzId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTZId(String value) {
        this.tzId = value;
    }

    /**
     * Obtém o valor da propriedade tzOffset.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTZOffset() {
        return tzOffset;
    }

    /**
     * Define o valor da propriedade tzOffset.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTZOffset(String value) {
        this.tzOffset = value;
    }

}
