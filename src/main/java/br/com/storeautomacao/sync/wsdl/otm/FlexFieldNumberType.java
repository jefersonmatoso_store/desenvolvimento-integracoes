
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Common type for tables which contain Flex Field columns for NUMBER data type
 * 
 * <p>Classe Java de FlexFieldNumberType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="FlexFieldNumberType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AttributeNumber1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AttributeNumber2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AttributeNumber3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AttributeNumber4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AttributeNumber5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AttributeNumber6" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AttributeNumber7" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AttributeNumber8" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AttributeNumber9" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AttributeNumber10" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FlexFieldNumberType", propOrder = {
    "attributeNumber1",
    "attributeNumber2",
    "attributeNumber3",
    "attributeNumber4",
    "attributeNumber5",
    "attributeNumber6",
    "attributeNumber7",
    "attributeNumber8",
    "attributeNumber9",
    "attributeNumber10"
})
public class FlexFieldNumberType {

    @XmlElement(name = "AttributeNumber1")
    protected String attributeNumber1;
    @XmlElement(name = "AttributeNumber2")
    protected String attributeNumber2;
    @XmlElement(name = "AttributeNumber3")
    protected String attributeNumber3;
    @XmlElement(name = "AttributeNumber4")
    protected String attributeNumber4;
    @XmlElement(name = "AttributeNumber5")
    protected String attributeNumber5;
    @XmlElement(name = "AttributeNumber6")
    protected String attributeNumber6;
    @XmlElement(name = "AttributeNumber7")
    protected String attributeNumber7;
    @XmlElement(name = "AttributeNumber8")
    protected String attributeNumber8;
    @XmlElement(name = "AttributeNumber9")
    protected String attributeNumber9;
    @XmlElement(name = "AttributeNumber10")
    protected String attributeNumber10;

    /**
     * Obtém o valor da propriedade attributeNumber1.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAttributeNumber1() {
        return attributeNumber1;
    }

    /**
     * Define o valor da propriedade attributeNumber1.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAttributeNumber1(String value) {
        this.attributeNumber1 = value;
    }

    /**
     * Obtém o valor da propriedade attributeNumber2.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAttributeNumber2() {
        return attributeNumber2;
    }

    /**
     * Define o valor da propriedade attributeNumber2.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAttributeNumber2(String value) {
        this.attributeNumber2 = value;
    }

    /**
     * Obtém o valor da propriedade attributeNumber3.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAttributeNumber3() {
        return attributeNumber3;
    }

    /**
     * Define o valor da propriedade attributeNumber3.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAttributeNumber3(String value) {
        this.attributeNumber3 = value;
    }

    /**
     * Obtém o valor da propriedade attributeNumber4.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAttributeNumber4() {
        return attributeNumber4;
    }

    /**
     * Define o valor da propriedade attributeNumber4.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAttributeNumber4(String value) {
        this.attributeNumber4 = value;
    }

    /**
     * Obtém o valor da propriedade attributeNumber5.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAttributeNumber5() {
        return attributeNumber5;
    }

    /**
     * Define o valor da propriedade attributeNumber5.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAttributeNumber5(String value) {
        this.attributeNumber5 = value;
    }

    /**
     * Obtém o valor da propriedade attributeNumber6.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAttributeNumber6() {
        return attributeNumber6;
    }

    /**
     * Define o valor da propriedade attributeNumber6.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAttributeNumber6(String value) {
        this.attributeNumber6 = value;
    }

    /**
     * Obtém o valor da propriedade attributeNumber7.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAttributeNumber7() {
        return attributeNumber7;
    }

    /**
     * Define o valor da propriedade attributeNumber7.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAttributeNumber7(String value) {
        this.attributeNumber7 = value;
    }

    /**
     * Obtém o valor da propriedade attributeNumber8.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAttributeNumber8() {
        return attributeNumber8;
    }

    /**
     * Define o valor da propriedade attributeNumber8.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAttributeNumber8(String value) {
        this.attributeNumber8 = value;
    }

    /**
     * Obtém o valor da propriedade attributeNumber9.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAttributeNumber9() {
        return attributeNumber9;
    }

    /**
     * Define o valor da propriedade attributeNumber9.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAttributeNumber9(String value) {
        this.attributeNumber9 = value;
    }

    /**
     * Obtém o valor da propriedade attributeNumber10.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAttributeNumber10() {
        return attributeNumber10;
    }

    /**
     * Define o valor da propriedade attributeNumber10.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAttributeNumber10(String value) {
        this.attributeNumber10 = value;
    }

}
