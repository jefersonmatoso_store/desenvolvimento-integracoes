
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Classe Java de RATE_GEO_COST_UNIT_BREAK_TYPE complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="RATE_GEO_COST_UNIT_BREAK_TYPE">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RATE_GEO_COST_UNIT_BREAK_ROW" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="RATE_GEO_COST_GROUP_GID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="RATE_GEO_COST_SEQ" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="RATE_UNIT_BREAK_GID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="CHARGE_AMOUNT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="CHARGE_AMOUNT_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="CHARGE_AMOUNT_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="CHARGE_DISCOUNT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="MIN_COST" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="MIN_COST_CURRENCY_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="MIN_COST_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="MAX_COST" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="MAX_COST_CURRENCY_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="MAX_COST_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="RATE_UNIT_BREAK2_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                 &lt;/sequence>
 *                 &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" />
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RATE_GEO_COST_UNIT_BREAK_TYPE", propOrder = {
    "rategeocostunitbreakrow"
})
public class RATEGEOCOSTUNITBREAKTYPE {

    @XmlElement(name = "RATE_GEO_COST_UNIT_BREAK_ROW")
    protected List<RATEGEOCOSTUNITBREAKROW> rategeocostunitbreakrow;

    /**
     * Gets the value of the rategeocostunitbreakrow property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the rategeocostunitbreakrow property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRATEGEOCOSTUNITBREAKROW().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RATEGEOCOSTUNITBREAKROW }
     * 
     * 
     */
    public List<RATEGEOCOSTUNITBREAKROW> getRATEGEOCOSTUNITBREAKROW() {
        if (rategeocostunitbreakrow == null) {
            rategeocostunitbreakrow = new ArrayList<RATEGEOCOSTUNITBREAKROW>();
        }
        return this.rategeocostunitbreakrow;
    }


    /**
     * <p>Classe Java de anonymous complex type.
     * 
     * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="RATE_GEO_COST_GROUP_GID" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="RATE_GEO_COST_SEQ" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="RATE_UNIT_BREAK_GID" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="CHARGE_AMOUNT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="CHARGE_AMOUNT_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="CHARGE_AMOUNT_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="CHARGE_DISCOUNT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="MIN_COST" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="MIN_COST_CURRENCY_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="MIN_COST_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="MAX_COST" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="MAX_COST_CURRENCY_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="MAX_COST_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="RATE_UNIT_BREAK2_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *       &lt;/sequence>
     *       &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" />
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "rategeocostgroupgid",
        "rategeocostseq",
        "rateunitbreakgid",
        "chargeamount",
        "chargeamountgid",
        "chargeamountbase",
        "chargediscount",
        "mincost",
        "mincostcurrencygid",
        "mincostbase",
        "maxcost",
        "maxcostcurrencygid",
        "maxcostbase",
        "rateunitbreak2GID",
        "domainname",
        "insertuser",
        "insertdate",
        "updateuser",
        "updatedate"
    })
    public static class RATEGEOCOSTUNITBREAKROW {

        @XmlElement(name = "RATE_GEO_COST_GROUP_GID", required = true)
        protected String rategeocostgroupgid;
        @XmlElement(name = "RATE_GEO_COST_SEQ", required = true)
        protected String rategeocostseq;
        @XmlElement(name = "RATE_UNIT_BREAK_GID", required = true)
        protected String rateunitbreakgid;
        @XmlElement(name = "CHARGE_AMOUNT")
        protected String chargeamount;
        @XmlElement(name = "CHARGE_AMOUNT_GID")
        protected String chargeamountgid;
        @XmlElement(name = "CHARGE_AMOUNT_BASE")
        protected String chargeamountbase;
        @XmlElement(name = "CHARGE_DISCOUNT")
        protected String chargediscount;
        @XmlElement(name = "MIN_COST")
        protected String mincost;
        @XmlElement(name = "MIN_COST_CURRENCY_GID")
        protected String mincostcurrencygid;
        @XmlElement(name = "MIN_COST_BASE")
        protected String mincostbase;
        @XmlElement(name = "MAX_COST")
        protected String maxcost;
        @XmlElement(name = "MAX_COST_CURRENCY_GID")
        protected String maxcostcurrencygid;
        @XmlElement(name = "MAX_COST_BASE")
        protected String maxcostbase;
        @XmlElement(name = "RATE_UNIT_BREAK2_GID")
        protected String rateunitbreak2GID;
        @XmlElement(name = "DOMAIN_NAME", required = true)
        protected String domainname;
        @XmlElement(name = "INSERT_USER", required = true)
        protected String insertuser;
        @XmlElement(name = "INSERT_DATE", required = true)
        protected String insertdate;
        @XmlElement(name = "UPDATE_USER")
        protected String updateuser;
        @XmlElement(name = "UPDATE_DATE")
        protected String updatedate;
        @XmlAttribute(name = "num")
        protected String num;

        /**
         * Obtém o valor da propriedade rategeocostgroupgid.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRATEGEOCOSTGROUPGID() {
            return rategeocostgroupgid;
        }

        /**
         * Define o valor da propriedade rategeocostgroupgid.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRATEGEOCOSTGROUPGID(String value) {
            this.rategeocostgroupgid = value;
        }

        /**
         * Obtém o valor da propriedade rategeocostseq.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRATEGEOCOSTSEQ() {
            return rategeocostseq;
        }

        /**
         * Define o valor da propriedade rategeocostseq.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRATEGEOCOSTSEQ(String value) {
            this.rategeocostseq = value;
        }

        /**
         * Obtém o valor da propriedade rateunitbreakgid.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRATEUNITBREAKGID() {
            return rateunitbreakgid;
        }

        /**
         * Define o valor da propriedade rateunitbreakgid.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRATEUNITBREAKGID(String value) {
            this.rateunitbreakgid = value;
        }

        /**
         * Obtém o valor da propriedade chargeamount.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCHARGEAMOUNT() {
            return chargeamount;
        }

        /**
         * Define o valor da propriedade chargeamount.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCHARGEAMOUNT(String value) {
            this.chargeamount = value;
        }

        /**
         * Obtém o valor da propriedade chargeamountgid.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCHARGEAMOUNTGID() {
            return chargeamountgid;
        }

        /**
         * Define o valor da propriedade chargeamountgid.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCHARGEAMOUNTGID(String value) {
            this.chargeamountgid = value;
        }

        /**
         * Obtém o valor da propriedade chargeamountbase.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCHARGEAMOUNTBASE() {
            return chargeamountbase;
        }

        /**
         * Define o valor da propriedade chargeamountbase.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCHARGEAMOUNTBASE(String value) {
            this.chargeamountbase = value;
        }

        /**
         * Obtém o valor da propriedade chargediscount.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCHARGEDISCOUNT() {
            return chargediscount;
        }

        /**
         * Define o valor da propriedade chargediscount.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCHARGEDISCOUNT(String value) {
            this.chargediscount = value;
        }

        /**
         * Obtém o valor da propriedade mincost.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMINCOST() {
            return mincost;
        }

        /**
         * Define o valor da propriedade mincost.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMINCOST(String value) {
            this.mincost = value;
        }

        /**
         * Obtém o valor da propriedade mincostcurrencygid.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMINCOSTCURRENCYGID() {
            return mincostcurrencygid;
        }

        /**
         * Define o valor da propriedade mincostcurrencygid.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMINCOSTCURRENCYGID(String value) {
            this.mincostcurrencygid = value;
        }

        /**
         * Obtém o valor da propriedade mincostbase.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMINCOSTBASE() {
            return mincostbase;
        }

        /**
         * Define o valor da propriedade mincostbase.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMINCOSTBASE(String value) {
            this.mincostbase = value;
        }

        /**
         * Obtém o valor da propriedade maxcost.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMAXCOST() {
            return maxcost;
        }

        /**
         * Define o valor da propriedade maxcost.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMAXCOST(String value) {
            this.maxcost = value;
        }

        /**
         * Obtém o valor da propriedade maxcostcurrencygid.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMAXCOSTCURRENCYGID() {
            return maxcostcurrencygid;
        }

        /**
         * Define o valor da propriedade maxcostcurrencygid.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMAXCOSTCURRENCYGID(String value) {
            this.maxcostcurrencygid = value;
        }

        /**
         * Obtém o valor da propriedade maxcostbase.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMAXCOSTBASE() {
            return maxcostbase;
        }

        /**
         * Define o valor da propriedade maxcostbase.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMAXCOSTBASE(String value) {
            this.maxcostbase = value;
        }

        /**
         * Obtém o valor da propriedade rateunitbreak2GID.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRATEUNITBREAK2GID() {
            return rateunitbreak2GID;
        }

        /**
         * Define o valor da propriedade rateunitbreak2GID.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRATEUNITBREAK2GID(String value) {
            this.rateunitbreak2GID = value;
        }

        /**
         * Obtém o valor da propriedade domainname.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDOMAINNAME() {
            return domainname;
        }

        /**
         * Define o valor da propriedade domainname.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDOMAINNAME(String value) {
            this.domainname = value;
        }

        /**
         * Obtém o valor da propriedade insertuser.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getINSERTUSER() {
            return insertuser;
        }

        /**
         * Define o valor da propriedade insertuser.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setINSERTUSER(String value) {
            this.insertuser = value;
        }

        /**
         * Obtém o valor da propriedade insertdate.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getINSERTDATE() {
            return insertdate;
        }

        /**
         * Define o valor da propriedade insertdate.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setINSERTDATE(String value) {
            this.insertdate = value;
        }

        /**
         * Obtém o valor da propriedade updateuser.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getUPDATEUSER() {
            return updateuser;
        }

        /**
         * Define o valor da propriedade updateuser.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setUPDATEUSER(String value) {
            this.updateuser = value;
        }

        /**
         * Obtém o valor da propriedade updatedate.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getUPDATEDATE() {
            return updatedate;
        }

        /**
         * Define o valor da propriedade updatedate.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setUPDATEDATE(String value) {
            this.updatedate = value;
        }

        /**
         * Obtém o valor da propriedade num.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNum() {
            return num;
        }

        /**
         * Define o valor da propriedade num.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNum(String value) {
            this.num = value;
        }

    }

}
