
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * 
 *             The ReleaseHeader contains various order related attributes and control information.
 *          
 * 
 * <p>Classe Java de ReleaseHeaderType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="ReleaseHeaderType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ReleaseName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ExternalSystemId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ReleaseMethodGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="QuoteGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="MovePerspectiveGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="CommercialTerms" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}CommercialTermsType" minOccurs="0"/>
 *         &lt;element name="FinalCommercialTerms" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FinalCommercialTermsType" minOccurs="0"/>
 *         &lt;element name="CommercialInvoiceTerms" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}CommercialInvoiceTermsType" minOccurs="0"/>
 *         &lt;element name="PlanningGroupGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="TimeWindowEmphasisGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;choice minOccurs="0">
 *           &lt;element name="RateServiceGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *           &lt;element name="RateServiceProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;/choice>
 *         &lt;choice minOccurs="0">
 *           &lt;element name="ServiceProviderGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *           &lt;element name="ServiceProviderProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;/choice>
 *         &lt;choice minOccurs="0">
 *           &lt;element name="SellServiceProviderGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *           &lt;element name="SellServiceProviderProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;/choice>
 *         &lt;choice minOccurs="0">
 *           &lt;element name="TransportModeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *           &lt;element name="ModeProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;/choice>
 *         &lt;choice minOccurs="0">
 *           &lt;element name="EquipmentGroupGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *           &lt;element name="EquipmentGroupProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;/choice>
 *         &lt;element name="ShipWithGroup" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsTemplate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TemplateType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RateOfferingGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="RateGeoGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="SellRateOfferingGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="SellRateGeoGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="FixedItineraryGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="FixedSellItineraryGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="MustShipDirect" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MustShipThruXDock" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MustShipThruPool" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BundlingType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsSplitAllowed" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsShipperKnown" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DimRateFactorGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="PickupRoutingSeqGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="DropoffRoutingSeqGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="ItineraryProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="SellItineraryProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="InspectionAndSurveyInfo" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}InspectionAndSurveyInfoType" minOccurs="0"/>
 *         &lt;element name="LetterOfCreditInfo" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LetterOfCreditInfoType" minOccurs="0"/>
 *         &lt;element name="ImportLicenseInfo" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ImportLicenseInfoType" minOccurs="0"/>
 *         &lt;element name="ConsolidationTypeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="StuffLocation" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocRefType" minOccurs="0"/>
 *         &lt;element name="DestuffLocation" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocRefType" minOccurs="0"/>
 *         &lt;element name="StowageModeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="UnitizationConditionName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CustomerUnitizationRequest" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsIgnoreLocationCalendar" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsConsolidateREquipment" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="UltimateDestCountryCode3Gid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="BufferType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DutyPaid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OnRouteTempExec" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsPreEnteredPU" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AccessorialCodeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="ReleaseSpecialService" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ReleaseSpecialServiceType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="ReleaseLegConstraint" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ReleaseLegConstraintType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="OrderPriority" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EquipmentTypeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="PickupRailCarrier" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="DeliveryRailCarrier" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="RailRouteCodeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="EmergPhoneNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FlexFieldStrings" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldStringType" minOccurs="0"/>
 *         &lt;element name="FlexFieldNumbers" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldNumberType" minOccurs="0"/>
 *         &lt;element name="FlexFieldDates" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldDateType" minOccurs="0"/>
 *         &lt;element name="FlexFieldCurrencies" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldCurrencyType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReleaseHeaderType", propOrder = {
    "releaseName",
    "externalSystemId",
    "releaseMethodGid",
    "quoteGid",
    "movePerspectiveGid",
    "commercialTerms",
    "finalCommercialTerms",
    "commercialInvoiceTerms",
    "planningGroupGid",
    "timeWindowEmphasisGid",
    "rateServiceGid",
    "rateServiceProfileGid",
    "serviceProviderGid",
    "serviceProviderProfileGid",
    "sellServiceProviderGid",
    "sellServiceProviderProfileGid",
    "transportModeGid",
    "modeProfileGid",
    "equipmentGroupGid",
    "equipmentGroupProfileGid",
    "shipWithGroup",
    "isTemplate",
    "templateType",
    "rateOfferingGid",
    "rateGeoGid",
    "sellRateOfferingGid",
    "sellRateGeoGid",
    "fixedItineraryGid",
    "fixedSellItineraryGid",
    "mustShipDirect",
    "mustShipThruXDock",
    "mustShipThruPool",
    "bundlingType",
    "isSplitAllowed",
    "isShipperKnown",
    "dimRateFactorGid",
    "pickupRoutingSeqGid",
    "dropoffRoutingSeqGid",
    "itineraryProfileGid",
    "sellItineraryProfileGid",
    "inspectionAndSurveyInfo",
    "letterOfCreditInfo",
    "importLicenseInfo",
    "consolidationTypeGid",
    "stuffLocation",
    "destuffLocation",
    "stowageModeGid",
    "unitizationConditionName",
    "customerUnitizationRequest",
    "isIgnoreLocationCalendar",
    "isConsolidateREquipment",
    "ultimateDestCountryCode3Gid",
    "bufferType",
    "dutyPaid",
    "onRouteTempExec",
    "isPreEnteredPU",
    "accessorialCodeGid",
    "releaseSpecialService",
    "releaseLegConstraint",
    "orderPriority",
    "equipmentTypeGid",
    "pickupRailCarrier",
    "deliveryRailCarrier",
    "railRouteCodeGid",
    "emergPhoneNum",
    "flexFieldStrings",
    "flexFieldNumbers",
    "flexFieldDates",
    "flexFieldCurrencies"
})
public class ReleaseHeaderType {

    @XmlElement(name = "ReleaseName")
    protected String releaseName;
    @XmlElement(name = "ExternalSystemId")
    protected String externalSystemId;
    @XmlElement(name = "ReleaseMethodGid")
    protected GLogXMLGidType releaseMethodGid;
    @XmlElement(name = "QuoteGid")
    protected GLogXMLGidType quoteGid;
    @XmlElement(name = "MovePerspectiveGid")
    protected GLogXMLGidType movePerspectiveGid;
    @XmlElement(name = "CommercialTerms")
    protected CommercialTermsType commercialTerms;
    @XmlElement(name = "FinalCommercialTerms")
    protected FinalCommercialTermsType finalCommercialTerms;
    @XmlElement(name = "CommercialInvoiceTerms")
    protected CommercialInvoiceTermsType commercialInvoiceTerms;
    @XmlElement(name = "PlanningGroupGid")
    protected GLogXMLGidType planningGroupGid;
    @XmlElement(name = "TimeWindowEmphasisGid")
    protected GLogXMLGidType timeWindowEmphasisGid;
    @XmlElement(name = "RateServiceGid")
    protected GLogXMLGidType rateServiceGid;
    @XmlElement(name = "RateServiceProfileGid")
    protected GLogXMLGidType rateServiceProfileGid;
    @XmlElement(name = "ServiceProviderGid")
    protected GLogXMLGidType serviceProviderGid;
    @XmlElement(name = "ServiceProviderProfileGid")
    protected GLogXMLGidType serviceProviderProfileGid;
    @XmlElement(name = "SellServiceProviderGid")
    protected GLogXMLGidType sellServiceProviderGid;
    @XmlElement(name = "SellServiceProviderProfileGid")
    protected GLogXMLGidType sellServiceProviderProfileGid;
    @XmlElement(name = "TransportModeGid")
    protected GLogXMLGidType transportModeGid;
    @XmlElement(name = "ModeProfileGid")
    protected GLogXMLGidType modeProfileGid;
    @XmlElement(name = "EquipmentGroupGid")
    protected GLogXMLGidType equipmentGroupGid;
    @XmlElement(name = "EquipmentGroupProfileGid")
    protected GLogXMLGidType equipmentGroupProfileGid;
    @XmlElement(name = "ShipWithGroup")
    protected String shipWithGroup;
    @XmlElement(name = "IsTemplate")
    protected String isTemplate;
    @XmlElement(name = "TemplateType")
    protected String templateType;
    @XmlElement(name = "RateOfferingGid")
    protected GLogXMLGidType rateOfferingGid;
    @XmlElement(name = "RateGeoGid")
    protected GLogXMLGidType rateGeoGid;
    @XmlElement(name = "SellRateOfferingGid")
    protected GLogXMLGidType sellRateOfferingGid;
    @XmlElement(name = "SellRateGeoGid")
    protected GLogXMLGidType sellRateGeoGid;
    @XmlElement(name = "FixedItineraryGid")
    protected GLogXMLGidType fixedItineraryGid;
    @XmlElement(name = "FixedSellItineraryGid")
    protected GLogXMLGidType fixedSellItineraryGid;
    @XmlElement(name = "MustShipDirect")
    protected String mustShipDirect;
    @XmlElement(name = "MustShipThruXDock")
    protected String mustShipThruXDock;
    @XmlElement(name = "MustShipThruPool")
    protected String mustShipThruPool;
    @XmlElement(name = "BundlingType")
    protected String bundlingType;
    @XmlElement(name = "IsSplitAllowed")
    protected String isSplitAllowed;
    @XmlElement(name = "IsShipperKnown")
    protected String isShipperKnown;
    @XmlElement(name = "DimRateFactorGid")
    protected GLogXMLGidType dimRateFactorGid;
    @XmlElement(name = "PickupRoutingSeqGid")
    protected GLogXMLGidType pickupRoutingSeqGid;
    @XmlElement(name = "DropoffRoutingSeqGid")
    protected GLogXMLGidType dropoffRoutingSeqGid;
    @XmlElement(name = "ItineraryProfileGid")
    protected GLogXMLGidType itineraryProfileGid;
    @XmlElement(name = "SellItineraryProfileGid")
    protected GLogXMLGidType sellItineraryProfileGid;
    @XmlElement(name = "InspectionAndSurveyInfo")
    protected InspectionAndSurveyInfoType inspectionAndSurveyInfo;
    @XmlElement(name = "LetterOfCreditInfo")
    protected LetterOfCreditInfoType letterOfCreditInfo;
    @XmlElement(name = "ImportLicenseInfo")
    protected ImportLicenseInfoType importLicenseInfo;
    @XmlElement(name = "ConsolidationTypeGid")
    protected GLogXMLGidType consolidationTypeGid;
    @XmlElement(name = "StuffLocation")
    protected GLogXMLLocRefType stuffLocation;
    @XmlElement(name = "DestuffLocation")
    protected GLogXMLLocRefType destuffLocation;
    @XmlElement(name = "StowageModeGid")
    protected GLogXMLGidType stowageModeGid;
    @XmlElement(name = "UnitizationConditionName")
    protected String unitizationConditionName;
    @XmlElement(name = "CustomerUnitizationRequest")
    protected String customerUnitizationRequest;
    @XmlElement(name = "IsIgnoreLocationCalendar")
    protected String isIgnoreLocationCalendar;
    @XmlElement(name = "IsConsolidateREquipment")
    protected String isConsolidateREquipment;
    @XmlElement(name = "UltimateDestCountryCode3Gid")
    protected GLogXMLGidType ultimateDestCountryCode3Gid;
    @XmlElement(name = "BufferType")
    protected String bufferType;
    @XmlElement(name = "DutyPaid")
    protected String dutyPaid;
    @XmlElement(name = "OnRouteTempExec")
    protected String onRouteTempExec;
    @XmlElement(name = "IsPreEnteredPU")
    protected String isPreEnteredPU;
    @XmlElement(name = "AccessorialCodeGid")
    protected List<GLogXMLGidType> accessorialCodeGid;
    @XmlElement(name = "ReleaseSpecialService")
    protected List<ReleaseSpecialServiceType> releaseSpecialService;
    @XmlElement(name = "ReleaseLegConstraint")
    protected List<ReleaseLegConstraintType> releaseLegConstraint;
    @XmlElement(name = "OrderPriority")
    protected String orderPriority;
    @XmlElement(name = "EquipmentTypeGid")
    protected GLogXMLGidType equipmentTypeGid;
    @XmlElement(name = "PickupRailCarrier")
    protected GLogXMLGidType pickupRailCarrier;
    @XmlElement(name = "DeliveryRailCarrier")
    protected GLogXMLGidType deliveryRailCarrier;
    @XmlElement(name = "RailRouteCodeGid")
    protected GLogXMLGidType railRouteCodeGid;
    @XmlElement(name = "EmergPhoneNum")
    protected String emergPhoneNum;
    @XmlElement(name = "FlexFieldStrings")
    protected FlexFieldStringType flexFieldStrings;
    @XmlElement(name = "FlexFieldNumbers")
    protected FlexFieldNumberType flexFieldNumbers;
    @XmlElement(name = "FlexFieldDates")
    protected FlexFieldDateType flexFieldDates;
    @XmlElement(name = "FlexFieldCurrencies")
    protected FlexFieldCurrencyType flexFieldCurrencies;

    /**
     * Obtém o valor da propriedade releaseName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReleaseName() {
        return releaseName;
    }

    /**
     * Define o valor da propriedade releaseName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReleaseName(String value) {
        this.releaseName = value;
    }

    /**
     * Obtém o valor da propriedade externalSystemId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExternalSystemId() {
        return externalSystemId;
    }

    /**
     * Define o valor da propriedade externalSystemId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExternalSystemId(String value) {
        this.externalSystemId = value;
    }

    /**
     * Obtém o valor da propriedade releaseMethodGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getReleaseMethodGid() {
        return releaseMethodGid;
    }

    /**
     * Define o valor da propriedade releaseMethodGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setReleaseMethodGid(GLogXMLGidType value) {
        this.releaseMethodGid = value;
    }

    /**
     * Obtém o valor da propriedade quoteGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getQuoteGid() {
        return quoteGid;
    }

    /**
     * Define o valor da propriedade quoteGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setQuoteGid(GLogXMLGidType value) {
        this.quoteGid = value;
    }

    /**
     * Obtém o valor da propriedade movePerspectiveGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getMovePerspectiveGid() {
        return movePerspectiveGid;
    }

    /**
     * Define o valor da propriedade movePerspectiveGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setMovePerspectiveGid(GLogXMLGidType value) {
        this.movePerspectiveGid = value;
    }

    /**
     * Obtém o valor da propriedade commercialTerms.
     * 
     * @return
     *     possible object is
     *     {@link CommercialTermsType }
     *     
     */
    public CommercialTermsType getCommercialTerms() {
        return commercialTerms;
    }

    /**
     * Define o valor da propriedade commercialTerms.
     * 
     * @param value
     *     allowed object is
     *     {@link CommercialTermsType }
     *     
     */
    public void setCommercialTerms(CommercialTermsType value) {
        this.commercialTerms = value;
    }

    /**
     * Obtém o valor da propriedade finalCommercialTerms.
     * 
     * @return
     *     possible object is
     *     {@link FinalCommercialTermsType }
     *     
     */
    public FinalCommercialTermsType getFinalCommercialTerms() {
        return finalCommercialTerms;
    }

    /**
     * Define o valor da propriedade finalCommercialTerms.
     * 
     * @param value
     *     allowed object is
     *     {@link FinalCommercialTermsType }
     *     
     */
    public void setFinalCommercialTerms(FinalCommercialTermsType value) {
        this.finalCommercialTerms = value;
    }

    /**
     * Obtém o valor da propriedade commercialInvoiceTerms.
     * 
     * @return
     *     possible object is
     *     {@link CommercialInvoiceTermsType }
     *     
     */
    public CommercialInvoiceTermsType getCommercialInvoiceTerms() {
        return commercialInvoiceTerms;
    }

    /**
     * Define o valor da propriedade commercialInvoiceTerms.
     * 
     * @param value
     *     allowed object is
     *     {@link CommercialInvoiceTermsType }
     *     
     */
    public void setCommercialInvoiceTerms(CommercialInvoiceTermsType value) {
        this.commercialInvoiceTerms = value;
    }

    /**
     * Obtém o valor da propriedade planningGroupGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getPlanningGroupGid() {
        return planningGroupGid;
    }

    /**
     * Define o valor da propriedade planningGroupGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setPlanningGroupGid(GLogXMLGidType value) {
        this.planningGroupGid = value;
    }

    /**
     * Obtém o valor da propriedade timeWindowEmphasisGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getTimeWindowEmphasisGid() {
        return timeWindowEmphasisGid;
    }

    /**
     * Define o valor da propriedade timeWindowEmphasisGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setTimeWindowEmphasisGid(GLogXMLGidType value) {
        this.timeWindowEmphasisGid = value;
    }

    /**
     * Obtém o valor da propriedade rateServiceGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRateServiceGid() {
        return rateServiceGid;
    }

    /**
     * Define o valor da propriedade rateServiceGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRateServiceGid(GLogXMLGidType value) {
        this.rateServiceGid = value;
    }

    /**
     * Obtém o valor da propriedade rateServiceProfileGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRateServiceProfileGid() {
        return rateServiceProfileGid;
    }

    /**
     * Define o valor da propriedade rateServiceProfileGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRateServiceProfileGid(GLogXMLGidType value) {
        this.rateServiceProfileGid = value;
    }

    /**
     * Obtém o valor da propriedade serviceProviderGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getServiceProviderGid() {
        return serviceProviderGid;
    }

    /**
     * Define o valor da propriedade serviceProviderGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setServiceProviderGid(GLogXMLGidType value) {
        this.serviceProviderGid = value;
    }

    /**
     * Obtém o valor da propriedade serviceProviderProfileGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getServiceProviderProfileGid() {
        return serviceProviderProfileGid;
    }

    /**
     * Define o valor da propriedade serviceProviderProfileGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setServiceProviderProfileGid(GLogXMLGidType value) {
        this.serviceProviderProfileGid = value;
    }

    /**
     * Obtém o valor da propriedade sellServiceProviderGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getSellServiceProviderGid() {
        return sellServiceProviderGid;
    }

    /**
     * Define o valor da propriedade sellServiceProviderGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setSellServiceProviderGid(GLogXMLGidType value) {
        this.sellServiceProviderGid = value;
    }

    /**
     * Obtém o valor da propriedade sellServiceProviderProfileGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getSellServiceProviderProfileGid() {
        return sellServiceProviderProfileGid;
    }

    /**
     * Define o valor da propriedade sellServiceProviderProfileGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setSellServiceProviderProfileGid(GLogXMLGidType value) {
        this.sellServiceProviderProfileGid = value;
    }

    /**
     * Obtém o valor da propriedade transportModeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getTransportModeGid() {
        return transportModeGid;
    }

    /**
     * Define o valor da propriedade transportModeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setTransportModeGid(GLogXMLGidType value) {
        this.transportModeGid = value;
    }

    /**
     * Obtém o valor da propriedade modeProfileGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getModeProfileGid() {
        return modeProfileGid;
    }

    /**
     * Define o valor da propriedade modeProfileGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setModeProfileGid(GLogXMLGidType value) {
        this.modeProfileGid = value;
    }

    /**
     * Obtém o valor da propriedade equipmentGroupGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getEquipmentGroupGid() {
        return equipmentGroupGid;
    }

    /**
     * Define o valor da propriedade equipmentGroupGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setEquipmentGroupGid(GLogXMLGidType value) {
        this.equipmentGroupGid = value;
    }

    /**
     * Obtém o valor da propriedade equipmentGroupProfileGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getEquipmentGroupProfileGid() {
        return equipmentGroupProfileGid;
    }

    /**
     * Define o valor da propriedade equipmentGroupProfileGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setEquipmentGroupProfileGid(GLogXMLGidType value) {
        this.equipmentGroupProfileGid = value;
    }

    /**
     * Obtém o valor da propriedade shipWithGroup.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipWithGroup() {
        return shipWithGroup;
    }

    /**
     * Define o valor da propriedade shipWithGroup.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipWithGroup(String value) {
        this.shipWithGroup = value;
    }

    /**
     * Obtém o valor da propriedade isTemplate.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsTemplate() {
        return isTemplate;
    }

    /**
     * Define o valor da propriedade isTemplate.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsTemplate(String value) {
        this.isTemplate = value;
    }

    /**
     * Obtém o valor da propriedade templateType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTemplateType() {
        return templateType;
    }

    /**
     * Define o valor da propriedade templateType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTemplateType(String value) {
        this.templateType = value;
    }

    /**
     * Obtém o valor da propriedade rateOfferingGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRateOfferingGid() {
        return rateOfferingGid;
    }

    /**
     * Define o valor da propriedade rateOfferingGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRateOfferingGid(GLogXMLGidType value) {
        this.rateOfferingGid = value;
    }

    /**
     * Obtém o valor da propriedade rateGeoGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRateGeoGid() {
        return rateGeoGid;
    }

    /**
     * Define o valor da propriedade rateGeoGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRateGeoGid(GLogXMLGidType value) {
        this.rateGeoGid = value;
    }

    /**
     * Obtém o valor da propriedade sellRateOfferingGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getSellRateOfferingGid() {
        return sellRateOfferingGid;
    }

    /**
     * Define o valor da propriedade sellRateOfferingGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setSellRateOfferingGid(GLogXMLGidType value) {
        this.sellRateOfferingGid = value;
    }

    /**
     * Obtém o valor da propriedade sellRateGeoGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getSellRateGeoGid() {
        return sellRateGeoGid;
    }

    /**
     * Define o valor da propriedade sellRateGeoGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setSellRateGeoGid(GLogXMLGidType value) {
        this.sellRateGeoGid = value;
    }

    /**
     * Obtém o valor da propriedade fixedItineraryGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getFixedItineraryGid() {
        return fixedItineraryGid;
    }

    /**
     * Define o valor da propriedade fixedItineraryGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setFixedItineraryGid(GLogXMLGidType value) {
        this.fixedItineraryGid = value;
    }

    /**
     * Obtém o valor da propriedade fixedSellItineraryGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getFixedSellItineraryGid() {
        return fixedSellItineraryGid;
    }

    /**
     * Define o valor da propriedade fixedSellItineraryGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setFixedSellItineraryGid(GLogXMLGidType value) {
        this.fixedSellItineraryGid = value;
    }

    /**
     * Obtém o valor da propriedade mustShipDirect.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMustShipDirect() {
        return mustShipDirect;
    }

    /**
     * Define o valor da propriedade mustShipDirect.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMustShipDirect(String value) {
        this.mustShipDirect = value;
    }

    /**
     * Obtém o valor da propriedade mustShipThruXDock.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMustShipThruXDock() {
        return mustShipThruXDock;
    }

    /**
     * Define o valor da propriedade mustShipThruXDock.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMustShipThruXDock(String value) {
        this.mustShipThruXDock = value;
    }

    /**
     * Obtém o valor da propriedade mustShipThruPool.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMustShipThruPool() {
        return mustShipThruPool;
    }

    /**
     * Define o valor da propriedade mustShipThruPool.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMustShipThruPool(String value) {
        this.mustShipThruPool = value;
    }

    /**
     * Obtém o valor da propriedade bundlingType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBundlingType() {
        return bundlingType;
    }

    /**
     * Define o valor da propriedade bundlingType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBundlingType(String value) {
        this.bundlingType = value;
    }

    /**
     * Obtém o valor da propriedade isSplitAllowed.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsSplitAllowed() {
        return isSplitAllowed;
    }

    /**
     * Define o valor da propriedade isSplitAllowed.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsSplitAllowed(String value) {
        this.isSplitAllowed = value;
    }

    /**
     * Obtém o valor da propriedade isShipperKnown.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsShipperKnown() {
        return isShipperKnown;
    }

    /**
     * Define o valor da propriedade isShipperKnown.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsShipperKnown(String value) {
        this.isShipperKnown = value;
    }

    /**
     * Obtém o valor da propriedade dimRateFactorGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getDimRateFactorGid() {
        return dimRateFactorGid;
    }

    /**
     * Define o valor da propriedade dimRateFactorGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setDimRateFactorGid(GLogXMLGidType value) {
        this.dimRateFactorGid = value;
    }

    /**
     * Obtém o valor da propriedade pickupRoutingSeqGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getPickupRoutingSeqGid() {
        return pickupRoutingSeqGid;
    }

    /**
     * Define o valor da propriedade pickupRoutingSeqGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setPickupRoutingSeqGid(GLogXMLGidType value) {
        this.pickupRoutingSeqGid = value;
    }

    /**
     * Obtém o valor da propriedade dropoffRoutingSeqGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getDropoffRoutingSeqGid() {
        return dropoffRoutingSeqGid;
    }

    /**
     * Define o valor da propriedade dropoffRoutingSeqGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setDropoffRoutingSeqGid(GLogXMLGidType value) {
        this.dropoffRoutingSeqGid = value;
    }

    /**
     * Obtém o valor da propriedade itineraryProfileGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getItineraryProfileGid() {
        return itineraryProfileGid;
    }

    /**
     * Define o valor da propriedade itineraryProfileGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setItineraryProfileGid(GLogXMLGidType value) {
        this.itineraryProfileGid = value;
    }

    /**
     * Obtém o valor da propriedade sellItineraryProfileGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getSellItineraryProfileGid() {
        return sellItineraryProfileGid;
    }

    /**
     * Define o valor da propriedade sellItineraryProfileGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setSellItineraryProfileGid(GLogXMLGidType value) {
        this.sellItineraryProfileGid = value;
    }

    /**
     * Obtém o valor da propriedade inspectionAndSurveyInfo.
     * 
     * @return
     *     possible object is
     *     {@link InspectionAndSurveyInfoType }
     *     
     */
    public InspectionAndSurveyInfoType getInspectionAndSurveyInfo() {
        return inspectionAndSurveyInfo;
    }

    /**
     * Define o valor da propriedade inspectionAndSurveyInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link InspectionAndSurveyInfoType }
     *     
     */
    public void setInspectionAndSurveyInfo(InspectionAndSurveyInfoType value) {
        this.inspectionAndSurveyInfo = value;
    }

    /**
     * Obtém o valor da propriedade letterOfCreditInfo.
     * 
     * @return
     *     possible object is
     *     {@link LetterOfCreditInfoType }
     *     
     */
    public LetterOfCreditInfoType getLetterOfCreditInfo() {
        return letterOfCreditInfo;
    }

    /**
     * Define o valor da propriedade letterOfCreditInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link LetterOfCreditInfoType }
     *     
     */
    public void setLetterOfCreditInfo(LetterOfCreditInfoType value) {
        this.letterOfCreditInfo = value;
    }

    /**
     * Obtém o valor da propriedade importLicenseInfo.
     * 
     * @return
     *     possible object is
     *     {@link ImportLicenseInfoType }
     *     
     */
    public ImportLicenseInfoType getImportLicenseInfo() {
        return importLicenseInfo;
    }

    /**
     * Define o valor da propriedade importLicenseInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link ImportLicenseInfoType }
     *     
     */
    public void setImportLicenseInfo(ImportLicenseInfoType value) {
        this.importLicenseInfo = value;
    }

    /**
     * Obtém o valor da propriedade consolidationTypeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getConsolidationTypeGid() {
        return consolidationTypeGid;
    }

    /**
     * Define o valor da propriedade consolidationTypeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setConsolidationTypeGid(GLogXMLGidType value) {
        this.consolidationTypeGid = value;
    }

    /**
     * Obtém o valor da propriedade stuffLocation.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public GLogXMLLocRefType getStuffLocation() {
        return stuffLocation;
    }

    /**
     * Define o valor da propriedade stuffLocation.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public void setStuffLocation(GLogXMLLocRefType value) {
        this.stuffLocation = value;
    }

    /**
     * Obtém o valor da propriedade destuffLocation.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public GLogXMLLocRefType getDestuffLocation() {
        return destuffLocation;
    }

    /**
     * Define o valor da propriedade destuffLocation.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public void setDestuffLocation(GLogXMLLocRefType value) {
        this.destuffLocation = value;
    }

    /**
     * Obtém o valor da propriedade stowageModeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getStowageModeGid() {
        return stowageModeGid;
    }

    /**
     * Define o valor da propriedade stowageModeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setStowageModeGid(GLogXMLGidType value) {
        this.stowageModeGid = value;
    }

    /**
     * Obtém o valor da propriedade unitizationConditionName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUnitizationConditionName() {
        return unitizationConditionName;
    }

    /**
     * Define o valor da propriedade unitizationConditionName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUnitizationConditionName(String value) {
        this.unitizationConditionName = value;
    }

    /**
     * Obtém o valor da propriedade customerUnitizationRequest.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomerUnitizationRequest() {
        return customerUnitizationRequest;
    }

    /**
     * Define o valor da propriedade customerUnitizationRequest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomerUnitizationRequest(String value) {
        this.customerUnitizationRequest = value;
    }

    /**
     * Obtém o valor da propriedade isIgnoreLocationCalendar.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsIgnoreLocationCalendar() {
        return isIgnoreLocationCalendar;
    }

    /**
     * Define o valor da propriedade isIgnoreLocationCalendar.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsIgnoreLocationCalendar(String value) {
        this.isIgnoreLocationCalendar = value;
    }

    /**
     * Obtém o valor da propriedade isConsolidateREquipment.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsConsolidateREquipment() {
        return isConsolidateREquipment;
    }

    /**
     * Define o valor da propriedade isConsolidateREquipment.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsConsolidateREquipment(String value) {
        this.isConsolidateREquipment = value;
    }

    /**
     * Obtém o valor da propriedade ultimateDestCountryCode3Gid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getUltimateDestCountryCode3Gid() {
        return ultimateDestCountryCode3Gid;
    }

    /**
     * Define o valor da propriedade ultimateDestCountryCode3Gid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setUltimateDestCountryCode3Gid(GLogXMLGidType value) {
        this.ultimateDestCountryCode3Gid = value;
    }

    /**
     * Obtém o valor da propriedade bufferType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBufferType() {
        return bufferType;
    }

    /**
     * Define o valor da propriedade bufferType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBufferType(String value) {
        this.bufferType = value;
    }

    /**
     * Obtém o valor da propriedade dutyPaid.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDutyPaid() {
        return dutyPaid;
    }

    /**
     * Define o valor da propriedade dutyPaid.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDutyPaid(String value) {
        this.dutyPaid = value;
    }

    /**
     * Obtém o valor da propriedade onRouteTempExec.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOnRouteTempExec() {
        return onRouteTempExec;
    }

    /**
     * Define o valor da propriedade onRouteTempExec.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOnRouteTempExec(String value) {
        this.onRouteTempExec = value;
    }

    /**
     * Obtém o valor da propriedade isPreEnteredPU.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsPreEnteredPU() {
        return isPreEnteredPU;
    }

    /**
     * Define o valor da propriedade isPreEnteredPU.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsPreEnteredPU(String value) {
        this.isPreEnteredPU = value;
    }

    /**
     * Gets the value of the accessorialCodeGid property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the accessorialCodeGid property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAccessorialCodeGid().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GLogXMLGidType }
     * 
     * 
     */
    public List<GLogXMLGidType> getAccessorialCodeGid() {
        if (accessorialCodeGid == null) {
            accessorialCodeGid = new ArrayList<GLogXMLGidType>();
        }
        return this.accessorialCodeGid;
    }

    /**
     * Gets the value of the releaseSpecialService property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the releaseSpecialService property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReleaseSpecialService().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ReleaseSpecialServiceType }
     * 
     * 
     */
    public List<ReleaseSpecialServiceType> getReleaseSpecialService() {
        if (releaseSpecialService == null) {
            releaseSpecialService = new ArrayList<ReleaseSpecialServiceType>();
        }
        return this.releaseSpecialService;
    }

    /**
     * Gets the value of the releaseLegConstraint property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the releaseLegConstraint property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReleaseLegConstraint().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ReleaseLegConstraintType }
     * 
     * 
     */
    public List<ReleaseLegConstraintType> getReleaseLegConstraint() {
        if (releaseLegConstraint == null) {
            releaseLegConstraint = new ArrayList<ReleaseLegConstraintType>();
        }
        return this.releaseLegConstraint;
    }

    /**
     * Obtém o valor da propriedade orderPriority.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrderPriority() {
        return orderPriority;
    }

    /**
     * Define o valor da propriedade orderPriority.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrderPriority(String value) {
        this.orderPriority = value;
    }

    /**
     * Obtém o valor da propriedade equipmentTypeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getEquipmentTypeGid() {
        return equipmentTypeGid;
    }

    /**
     * Define o valor da propriedade equipmentTypeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setEquipmentTypeGid(GLogXMLGidType value) {
        this.equipmentTypeGid = value;
    }

    /**
     * Obtém o valor da propriedade pickupRailCarrier.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getPickupRailCarrier() {
        return pickupRailCarrier;
    }

    /**
     * Define o valor da propriedade pickupRailCarrier.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setPickupRailCarrier(GLogXMLGidType value) {
        this.pickupRailCarrier = value;
    }

    /**
     * Obtém o valor da propriedade deliveryRailCarrier.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getDeliveryRailCarrier() {
        return deliveryRailCarrier;
    }

    /**
     * Define o valor da propriedade deliveryRailCarrier.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setDeliveryRailCarrier(GLogXMLGidType value) {
        this.deliveryRailCarrier = value;
    }

    /**
     * Obtém o valor da propriedade railRouteCodeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRailRouteCodeGid() {
        return railRouteCodeGid;
    }

    /**
     * Define o valor da propriedade railRouteCodeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRailRouteCodeGid(GLogXMLGidType value) {
        this.railRouteCodeGid = value;
    }

    /**
     * Obtém o valor da propriedade emergPhoneNum.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmergPhoneNum() {
        return emergPhoneNum;
    }

    /**
     * Define o valor da propriedade emergPhoneNum.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmergPhoneNum(String value) {
        this.emergPhoneNum = value;
    }

    /**
     * Obtém o valor da propriedade flexFieldStrings.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldStringType }
     *     
     */
    public FlexFieldStringType getFlexFieldStrings() {
        return flexFieldStrings;
    }

    /**
     * Define o valor da propriedade flexFieldStrings.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldStringType }
     *     
     */
    public void setFlexFieldStrings(FlexFieldStringType value) {
        this.flexFieldStrings = value;
    }

    /**
     * Obtém o valor da propriedade flexFieldNumbers.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldNumberType }
     *     
     */
    public FlexFieldNumberType getFlexFieldNumbers() {
        return flexFieldNumbers;
    }

    /**
     * Define o valor da propriedade flexFieldNumbers.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldNumberType }
     *     
     */
    public void setFlexFieldNumbers(FlexFieldNumberType value) {
        this.flexFieldNumbers = value;
    }

    /**
     * Obtém o valor da propriedade flexFieldDates.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldDateType }
     *     
     */
    public FlexFieldDateType getFlexFieldDates() {
        return flexFieldDates;
    }

    /**
     * Define o valor da propriedade flexFieldDates.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldDateType }
     *     
     */
    public void setFlexFieldDates(FlexFieldDateType value) {
        this.flexFieldDates = value;
    }

    /**
     * Obtém o valor da propriedade flexFieldCurrencies.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldCurrencyType }
     *     
     */
    public FlexFieldCurrencyType getFlexFieldCurrencies() {
        return flexFieldCurrencies;
    }

    /**
     * Define o valor da propriedade flexFieldCurrencies.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldCurrencyType }
     *     
     */
    public void setFlexFieldCurrencies(FlexFieldCurrencyType value) {
        this.flexFieldCurrencies = value;
    }

}
