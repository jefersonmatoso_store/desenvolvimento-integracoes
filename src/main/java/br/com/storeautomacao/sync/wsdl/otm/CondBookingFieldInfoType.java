
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Used to specify information modified by the service provider as part of the booking.
 *             Note: The PlannerValue is sent outbound in the TenderOffer and is not needed in the TenderResponse.
 *             The ServiceProviderValue is not sent outbound in the TenderOffer, but is required in the TenderResponse.
 *          
 * 
 * <p>Classe Java de CondBookingFieldInfoType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="CondBookingFieldInfoType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ConditionalBookingFieldGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="PlannerValue" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ServiceProviderValue" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="UOMCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OrderSequence" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LocationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="StopNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LocationLongName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LocationQualifierGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="TimeQualifierGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="TransportStageQualifierGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="LocationAlias" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CondBookingFieldInfoType", propOrder = {
    "conditionalBookingFieldGid",
    "plannerValue",
    "serviceProviderValue",
    "uomCode",
    "orderSequence",
    "locationGid",
    "stopNumber",
    "locationLongName",
    "locationQualifierGid",
    "timeQualifierGid",
    "transportStageQualifierGid",
    "locationAlias"
})
public class CondBookingFieldInfoType {

    @XmlElement(name = "ConditionalBookingFieldGid", required = true)
    protected GLogXMLGidType conditionalBookingFieldGid;
    @XmlElement(name = "PlannerValue")
    protected String plannerValue;
    @XmlElement(name = "ServiceProviderValue")
    protected String serviceProviderValue;
    @XmlElement(name = "UOMCode")
    protected String uomCode;
    @XmlElement(name = "OrderSequence")
    protected String orderSequence;
    @XmlElement(name = "LocationGid")
    protected GLogXMLGidType locationGid;
    @XmlElement(name = "StopNumber")
    protected String stopNumber;
    @XmlElement(name = "LocationLongName")
    protected String locationLongName;
    @XmlElement(name = "LocationQualifierGid")
    protected GLogXMLGidType locationQualifierGid;
    @XmlElement(name = "TimeQualifierGid")
    protected GLogXMLGidType timeQualifierGid;
    @XmlElement(name = "TransportStageQualifierGid")
    protected GLogXMLGidType transportStageQualifierGid;
    @XmlElement(name = "LocationAlias")
    protected String locationAlias;

    /**
     * Obtém o valor da propriedade conditionalBookingFieldGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getConditionalBookingFieldGid() {
        return conditionalBookingFieldGid;
    }

    /**
     * Define o valor da propriedade conditionalBookingFieldGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setConditionalBookingFieldGid(GLogXMLGidType value) {
        this.conditionalBookingFieldGid = value;
    }

    /**
     * Obtém o valor da propriedade plannerValue.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPlannerValue() {
        return plannerValue;
    }

    /**
     * Define o valor da propriedade plannerValue.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPlannerValue(String value) {
        this.plannerValue = value;
    }

    /**
     * Obtém o valor da propriedade serviceProviderValue.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderValue() {
        return serviceProviderValue;
    }

    /**
     * Define o valor da propriedade serviceProviderValue.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderValue(String value) {
        this.serviceProviderValue = value;
    }

    /**
     * Obtém o valor da propriedade uomCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUOMCode() {
        return uomCode;
    }

    /**
     * Define o valor da propriedade uomCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUOMCode(String value) {
        this.uomCode = value;
    }

    /**
     * Obtém o valor da propriedade orderSequence.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrderSequence() {
        return orderSequence;
    }

    /**
     * Define o valor da propriedade orderSequence.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrderSequence(String value) {
        this.orderSequence = value;
    }

    /**
     * Obtém o valor da propriedade locationGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getLocationGid() {
        return locationGid;
    }

    /**
     * Define o valor da propriedade locationGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setLocationGid(GLogXMLGidType value) {
        this.locationGid = value;
    }

    /**
     * Obtém o valor da propriedade stopNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStopNumber() {
        return stopNumber;
    }

    /**
     * Define o valor da propriedade stopNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStopNumber(String value) {
        this.stopNumber = value;
    }

    /**
     * Obtém o valor da propriedade locationLongName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLocationLongName() {
        return locationLongName;
    }

    /**
     * Define o valor da propriedade locationLongName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLocationLongName(String value) {
        this.locationLongName = value;
    }

    /**
     * Obtém o valor da propriedade locationQualifierGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getLocationQualifierGid() {
        return locationQualifierGid;
    }

    /**
     * Define o valor da propriedade locationQualifierGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setLocationQualifierGid(GLogXMLGidType value) {
        this.locationQualifierGid = value;
    }

    /**
     * Obtém o valor da propriedade timeQualifierGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getTimeQualifierGid() {
        return timeQualifierGid;
    }

    /**
     * Define o valor da propriedade timeQualifierGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setTimeQualifierGid(GLogXMLGidType value) {
        this.timeQualifierGid = value;
    }

    /**
     * Obtém o valor da propriedade transportStageQualifierGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getTransportStageQualifierGid() {
        return transportStageQualifierGid;
    }

    /**
     * Define o valor da propriedade transportStageQualifierGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setTransportStageQualifierGid(GLogXMLGidType value) {
        this.transportStageQualifierGid = value;
    }

    /**
     * Obtém o valor da propriedade locationAlias.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLocationAlias() {
        return locationAlias;
    }

    /**
     * Define o valor da propriedade locationAlias.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLocationAlias(String value) {
        this.locationAlias = value;
    }

}
