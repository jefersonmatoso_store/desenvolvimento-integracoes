
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Specifies the diameter. It includes sub-elements for diameter value and unit of measure.
 * 
 * <p>Classe Java de DiameterType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="DiameterType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DiameterValue" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="UOMGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DiameterType", propOrder = {
    "diameterValue",
    "uomGid"
})
public class DiameterType {

    @XmlElement(name = "DiameterValue", required = true)
    protected String diameterValue;
    @XmlElement(name = "UOMGid", required = true)
    protected GLogXMLGidType uomGid;

    /**
     * Obtém o valor da propriedade diameterValue.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDiameterValue() {
        return diameterValue;
    }

    /**
     * Define o valor da propriedade diameterValue.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDiameterValue(String value) {
        this.diameterValue = value;
    }

    /**
     * Obtém o valor da propriedade uomGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getUOMGid() {
        return uomGid;
    }

    /**
     * Define o valor da propriedade uomGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setUOMGid(GLogXMLGidType value) {
        this.uomGid = value;
    }

}
