
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * OrderLineRefnum is an alternate method for identifying an order line. It consists of a
 *             qualifier and a value.
 *          
 * 
 * <p>Classe Java de OrderLineRefnumType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="OrderLineRefnumType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="OrderLineRefnumQualifierGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="OrderLineRefnumValue" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OrderLineRefnumType", propOrder = {
    "orderLineRefnumQualifierGid",
    "orderLineRefnumValue"
})
public class OrderLineRefnumType {

    @XmlElement(name = "OrderLineRefnumQualifierGid", required = true)
    protected GLogXMLGidType orderLineRefnumQualifierGid;
    @XmlElement(name = "OrderLineRefnumValue", required = true)
    protected String orderLineRefnumValue;

    /**
     * Obtém o valor da propriedade orderLineRefnumQualifierGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getOrderLineRefnumQualifierGid() {
        return orderLineRefnumQualifierGid;
    }

    /**
     * Define o valor da propriedade orderLineRefnumQualifierGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setOrderLineRefnumQualifierGid(GLogXMLGidType value) {
        this.orderLineRefnumQualifierGid = value;
    }

    /**
     * Obtém o valor da propriedade orderLineRefnumValue.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrderLineRefnumValue() {
        return orderLineRefnumValue;
    }

    /**
     * Define o valor da propriedade orderLineRefnumValue.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrderLineRefnumValue(String value) {
        this.orderLineRefnumValue = value;
    }

}
