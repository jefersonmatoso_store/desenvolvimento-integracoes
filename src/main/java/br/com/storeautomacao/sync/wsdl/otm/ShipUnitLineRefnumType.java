
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 *             ShipUnitLineRefnum is an alternate method for identifying an ShipUnitLine (ShipUnitContent) in a ShipUnit.
 *             It consists of a qualifier and a value.
 *          
 * 
 * <p>Classe Java de ShipUnitLineRefnumType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="ShipUnitLineRefnumType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ShipUnitLineRefnumQualifierGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="ShipUnitLineRefnumValue" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ShipUnitLineRefnumType", propOrder = {
    "shipUnitLineRefnumQualifierGid",
    "shipUnitLineRefnumValue"
})
public class ShipUnitLineRefnumType {

    @XmlElement(name = "ShipUnitLineRefnumQualifierGid", required = true)
    protected GLogXMLGidType shipUnitLineRefnumQualifierGid;
    @XmlElement(name = "ShipUnitLineRefnumValue", required = true)
    protected String shipUnitLineRefnumValue;

    /**
     * Obtém o valor da propriedade shipUnitLineRefnumQualifierGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getShipUnitLineRefnumQualifierGid() {
        return shipUnitLineRefnumQualifierGid;
    }

    /**
     * Define o valor da propriedade shipUnitLineRefnumQualifierGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setShipUnitLineRefnumQualifierGid(GLogXMLGidType value) {
        this.shipUnitLineRefnumQualifierGid = value;
    }

    /**
     * Obtém o valor da propriedade shipUnitLineRefnumValue.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipUnitLineRefnumValue() {
        return shipUnitLineRefnumValue;
    }

    /**
     * Define o valor da propriedade shipUnitLineRefnumValue.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipUnitLineRefnumValue(String value) {
        this.shipUnitLineRefnumValue = value;
    }

}
