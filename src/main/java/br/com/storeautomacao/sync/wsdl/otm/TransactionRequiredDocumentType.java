
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * (Outbound) Documents Required for the conduct of business transaction
 *          
 * 
 * <p>Classe Java de TransactionRequiredDocumentType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="TransactionRequiredDocumentType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DocumentDefGid" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="TransactionRequiredText" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}RequiredTextType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="TransactionDocumentReviewer" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}InvolvedPartyDetailType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="TransactionDocumentSubscriber" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}InvolvedPartyDetailType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="ComplianceRuleGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="ComplianceRuleGroupGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="ReviewStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TransactionRequiredDocumentType", namespace = "http://xmlns.oracle.com/apps/gtm/transmission/v6.4", propOrder = {
    "documentDefGid",
    "transactionRequiredText",
    "transactionDocumentReviewer",
    "transactionDocumentSubscriber",
    "complianceRuleGid",
    "complianceRuleGroupGid",
    "reviewStatus"
})
public class TransactionRequiredDocumentType {

    @XmlElement(name = "DocumentDefGid", required = true)
    protected String documentDefGid;
    @XmlElement(name = "TransactionRequiredText")
    protected List<RequiredTextType> transactionRequiredText;
    @XmlElement(name = "TransactionDocumentReviewer")
    protected List<InvolvedPartyDetailType> transactionDocumentReviewer;
    @XmlElement(name = "TransactionDocumentSubscriber")
    protected List<InvolvedPartyDetailType> transactionDocumentSubscriber;
    @XmlElement(name = "ComplianceRuleGid")
    protected GLogXMLGidType complianceRuleGid;
    @XmlElement(name = "ComplianceRuleGroupGid")
    protected GLogXMLGidType complianceRuleGroupGid;
    @XmlElement(name = "ReviewStatus")
    protected String reviewStatus;

    /**
     * Obtém o valor da propriedade documentDefGid.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDocumentDefGid() {
        return documentDefGid;
    }

    /**
     * Define o valor da propriedade documentDefGid.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDocumentDefGid(String value) {
        this.documentDefGid = value;
    }

    /**
     * Gets the value of the transactionRequiredText property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the transactionRequiredText property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTransactionRequiredText().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RequiredTextType }
     * 
     * 
     */
    public List<RequiredTextType> getTransactionRequiredText() {
        if (transactionRequiredText == null) {
            transactionRequiredText = new ArrayList<RequiredTextType>();
        }
        return this.transactionRequiredText;
    }

    /**
     * Gets the value of the transactionDocumentReviewer property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the transactionDocumentReviewer property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTransactionDocumentReviewer().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link InvolvedPartyDetailType }
     * 
     * 
     */
    public List<InvolvedPartyDetailType> getTransactionDocumentReviewer() {
        if (transactionDocumentReviewer == null) {
            transactionDocumentReviewer = new ArrayList<InvolvedPartyDetailType>();
        }
        return this.transactionDocumentReviewer;
    }

    /**
     * Gets the value of the transactionDocumentSubscriber property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the transactionDocumentSubscriber property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTransactionDocumentSubscriber().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link InvolvedPartyDetailType }
     * 
     * 
     */
    public List<InvolvedPartyDetailType> getTransactionDocumentSubscriber() {
        if (transactionDocumentSubscriber == null) {
            transactionDocumentSubscriber = new ArrayList<InvolvedPartyDetailType>();
        }
        return this.transactionDocumentSubscriber;
    }

    /**
     * Obtém o valor da propriedade complianceRuleGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getComplianceRuleGid() {
        return complianceRuleGid;
    }

    /**
     * Define o valor da propriedade complianceRuleGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setComplianceRuleGid(GLogXMLGidType value) {
        this.complianceRuleGid = value;
    }

    /**
     * Obtém o valor da propriedade complianceRuleGroupGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getComplianceRuleGroupGid() {
        return complianceRuleGroupGid;
    }

    /**
     * Define o valor da propriedade complianceRuleGroupGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setComplianceRuleGroupGid(GLogXMLGidType value) {
        this.complianceRuleGroupGid = value;
    }

    /**
     * Obtém o valor da propriedade reviewStatus.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReviewStatus() {
        return reviewStatus;
    }

    /**
     * Define o valor da propriedade reviewStatus.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReviewStatus(String value) {
        this.reviewStatus = value;
    }

}
