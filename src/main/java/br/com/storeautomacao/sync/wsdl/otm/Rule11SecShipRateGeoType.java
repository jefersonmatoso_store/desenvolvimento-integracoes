
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Contains the information from the RATE_GEO for the second Shipment in a Rail Rule 11 move.
 * 
 * <p>Classe Java de Rule11SecShipRateGeoType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="Rule11SecShipRateGeoType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RailInterModalPlanGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="CustomerRateCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="COFC_TOFC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Rule11SecShipRateGeoType", propOrder = {
    "railInterModalPlanGid",
    "customerRateCode",
    "cofctofc"
})
public class Rule11SecShipRateGeoType {

    @XmlElement(name = "RailInterModalPlanGid")
    protected GLogXMLGidType railInterModalPlanGid;
    @XmlElement(name = "CustomerRateCode")
    protected String customerRateCode;
    @XmlElement(name = "COFC_TOFC")
    protected String cofctofc;

    /**
     * Obtém o valor da propriedade railInterModalPlanGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRailInterModalPlanGid() {
        return railInterModalPlanGid;
    }

    /**
     * Define o valor da propriedade railInterModalPlanGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRailInterModalPlanGid(GLogXMLGidType value) {
        this.railInterModalPlanGid = value;
    }

    /**
     * Obtém o valor da propriedade customerRateCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomerRateCode() {
        return customerRateCode;
    }

    /**
     * Define o valor da propriedade customerRateCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomerRateCode(String value) {
        this.customerRateCode = value;
    }

    /**
     * Obtém o valor da propriedade cofctofc.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCOFCTOFC() {
        return cofctofc;
    }

    /**
     * Define o valor da propriedade cofctofc.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCOFCTOFC(String value) {
        this.cofctofc = value;
    }

}
