
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de RemoteQueryReplyType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="RemoteQueryReplyType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OTMTransactionOut">
 *       &lt;choice>
 *         &lt;element name="RIQQueryReply" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RIQQueryReplyType"/>
 *         &lt;element name="ShipmentQueryReply" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ShipmentQueryReplyType"/>
 *         &lt;element name="TransmissionReportQueryReply" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransmissionReportQueryReplyType"/>
 *         &lt;element name="GenericQueryReply" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GenericQueryReplyType"/>
 *         &lt;element name="AppointmentQueryReply" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}AppointmentQueryReplyType"/>
 *         &lt;element name="OrderRoutingRuleReply" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OrderRoutingRuleReplyType"/>
 *       &lt;/choice>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RemoteQueryReplyType", propOrder = {
    "riqQueryReply",
    "shipmentQueryReply",
    "transmissionReportQueryReply",
    "genericQueryReply",
    "appointmentQueryReply",
    "orderRoutingRuleReply"
})
public class RemoteQueryReplyType
    extends OTMTransactionOut
{

    @XmlElement(name = "RIQQueryReply")
    protected RIQQueryReplyType riqQueryReply;
    @XmlElement(name = "ShipmentQueryReply")
    protected ShipmentQueryReplyType shipmentQueryReply;
    @XmlElement(name = "TransmissionReportQueryReply")
    protected TransmissionReportQueryReplyType transmissionReportQueryReply;
    @XmlElement(name = "GenericQueryReply")
    protected GenericQueryReplyType genericQueryReply;
    @XmlElement(name = "AppointmentQueryReply")
    protected AppointmentQueryReplyType appointmentQueryReply;
    @XmlElement(name = "OrderRoutingRuleReply")
    protected OrderRoutingRuleReplyType orderRoutingRuleReply;

    /**
     * Obtém o valor da propriedade riqQueryReply.
     * 
     * @return
     *     possible object is
     *     {@link RIQQueryReplyType }
     *     
     */
    public RIQQueryReplyType getRIQQueryReply() {
        return riqQueryReply;
    }

    /**
     * Define o valor da propriedade riqQueryReply.
     * 
     * @param value
     *     allowed object is
     *     {@link RIQQueryReplyType }
     *     
     */
    public void setRIQQueryReply(RIQQueryReplyType value) {
        this.riqQueryReply = value;
    }

    /**
     * Obtém o valor da propriedade shipmentQueryReply.
     * 
     * @return
     *     possible object is
     *     {@link ShipmentQueryReplyType }
     *     
     */
    public ShipmentQueryReplyType getShipmentQueryReply() {
        return shipmentQueryReply;
    }

    /**
     * Define o valor da propriedade shipmentQueryReply.
     * 
     * @param value
     *     allowed object is
     *     {@link ShipmentQueryReplyType }
     *     
     */
    public void setShipmentQueryReply(ShipmentQueryReplyType value) {
        this.shipmentQueryReply = value;
    }

    /**
     * Obtém o valor da propriedade transmissionReportQueryReply.
     * 
     * @return
     *     possible object is
     *     {@link TransmissionReportQueryReplyType }
     *     
     */
    public TransmissionReportQueryReplyType getTransmissionReportQueryReply() {
        return transmissionReportQueryReply;
    }

    /**
     * Define o valor da propriedade transmissionReportQueryReply.
     * 
     * @param value
     *     allowed object is
     *     {@link TransmissionReportQueryReplyType }
     *     
     */
    public void setTransmissionReportQueryReply(TransmissionReportQueryReplyType value) {
        this.transmissionReportQueryReply = value;
    }

    /**
     * Obtém o valor da propriedade genericQueryReply.
     * 
     * @return
     *     possible object is
     *     {@link GenericQueryReplyType }
     *     
     */
    public GenericQueryReplyType getGenericQueryReply() {
        return genericQueryReply;
    }

    /**
     * Define o valor da propriedade genericQueryReply.
     * 
     * @param value
     *     allowed object is
     *     {@link GenericQueryReplyType }
     *     
     */
    public void setGenericQueryReply(GenericQueryReplyType value) {
        this.genericQueryReply = value;
    }

    /**
     * Obtém o valor da propriedade appointmentQueryReply.
     * 
     * @return
     *     possible object is
     *     {@link AppointmentQueryReplyType }
     *     
     */
    public AppointmentQueryReplyType getAppointmentQueryReply() {
        return appointmentQueryReply;
    }

    /**
     * Define o valor da propriedade appointmentQueryReply.
     * 
     * @param value
     *     allowed object is
     *     {@link AppointmentQueryReplyType }
     *     
     */
    public void setAppointmentQueryReply(AppointmentQueryReplyType value) {
        this.appointmentQueryReply = value;
    }

    /**
     * Obtém o valor da propriedade orderRoutingRuleReply.
     * 
     * @return
     *     possible object is
     *     {@link OrderRoutingRuleReplyType }
     *     
     */
    public OrderRoutingRuleReplyType getOrderRoutingRuleReply() {
        return orderRoutingRuleReply;
    }

    /**
     * Define o valor da propriedade orderRoutingRuleReply.
     * 
     * @param value
     *     allowed object is
     *     {@link OrderRoutingRuleReplyType }
     *     
     */
    public void setOrderRoutingRuleReply(OrderRoutingRuleReplyType value) {
        this.orderRoutingRuleReply = value;
    }

}
