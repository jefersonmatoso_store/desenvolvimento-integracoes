
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Used to indicate the quantity.
 * 
 * <p>Classe Java de FlexQuantityType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="FlexQuantityType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="UOM" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="UOMType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Quantity" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FlexQuantityType", propOrder = {
    "uom",
    "uomType",
    "quantity"
})
public class FlexQuantityType {

    @XmlElement(name = "UOM", required = true)
    protected String uom;
    @XmlElement(name = "UOMType", required = true)
    protected String uomType;
    @XmlElement(name = "Quantity", required = true)
    protected String quantity;

    /**
     * Obtém o valor da propriedade uom.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUOM() {
        return uom;
    }

    /**
     * Define o valor da propriedade uom.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUOM(String value) {
        this.uom = value;
    }

    /**
     * Obtém o valor da propriedade uomType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUOMType() {
        return uomType;
    }

    /**
     * Define o valor da propriedade uomType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUOMType(String value) {
        this.uomType = value;
    }

    /**
     * Obtém o valor da propriedade quantity.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQuantity() {
        return quantity;
    }

    /**
     * Define o valor da propriedade quantity.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQuantity(String value) {
        this.quantity = value;
    }

}
