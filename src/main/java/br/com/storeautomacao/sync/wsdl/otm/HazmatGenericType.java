
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.*;


/**
 * Used to identify hazardous materials information that is proper shipping name centric and
 *             thus more generic in nature.
 *             Note: Within the HazmatItem, this element is outbound only.
 *          
 * 
 * <p>Classe Java de HazmatGenericType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="HazmatGenericType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OTMTransactionInOut">
 *       &lt;sequence>
 *         &lt;element name="HazmatGenericGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="TransactionCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransactionCodeType"/>
 *         &lt;element name="ProperShippingName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="IdentificationNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="HazardousClass" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}HazardousClassType" minOccurs="0"/>
 *         &lt;element name="PackagingGroup" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SubsidiaryHazard" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RegulatoryAgencyDataSource" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SpecialProvisions" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PSASingaporeGroup" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SymbolExtraMeaning" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ImdgEmsNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsNos" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsMarinePollutant" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsToxicInhalation" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InhalationHazardZone" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsPassengerAircraftForbid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsCommercialAircraftForbid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ERG" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ERGAir" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EMS" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="HazCompatGroup" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "HazmatGenericType", propOrder = {
    "hazmatGenericGid",
    "transactionCode",
    "properShippingName",
    "identificationNumber",
    "hazardousClass",
    "packagingGroup",
    "subsidiaryHazard",
    "regulatoryAgencyDataSource",
    "specialProvisions",
    "psaSingaporeGroup",
    "symbolExtraMeaning",
    "imdgEmsNumber",
    "isNos",
    "isMarinePollutant",
    "isToxicInhalation",
    "inhalationHazardZone",
    "isPassengerAircraftForbid",
    "isCommercialAircraftForbid",
    "erg",
    "ergAir",
    "ems",
    "hazCompatGroup"
})
public class HazmatGenericType
    extends OTMTransactionInOut
{

    @XmlElement(name = "HazmatGenericGid", required = true)
    protected GLogXMLGidType hazmatGenericGid;
    @XmlElement(name = "TransactionCode", required = true)
    @XmlSchemaType(name = "string")
    protected TransactionCodeType transactionCode;
    @XmlElement(name = "ProperShippingName", required = true)
    protected String properShippingName;
    @XmlElement(name = "IdentificationNumber", required = true)
    protected String identificationNumber;
    @XmlElement(name = "HazardousClass")
    protected HazardousClassType hazardousClass;
    @XmlElement(name = "PackagingGroup")
    protected String packagingGroup;
    @XmlElement(name = "SubsidiaryHazard")
    protected String subsidiaryHazard;
    @XmlElement(name = "RegulatoryAgencyDataSource")
    protected String regulatoryAgencyDataSource;
    @XmlElement(name = "SpecialProvisions")
    protected String specialProvisions;
    @XmlElement(name = "PSASingaporeGroup")
    protected String psaSingaporeGroup;
    @XmlElement(name = "SymbolExtraMeaning")
    protected String symbolExtraMeaning;
    @XmlElement(name = "ImdgEmsNumber")
    protected String imdgEmsNumber;
    @XmlElement(name = "IsNos")
    protected String isNos;
    @XmlElement(name = "IsMarinePollutant")
    protected String isMarinePollutant;
    @XmlElement(name = "IsToxicInhalation")
    protected String isToxicInhalation;
    @XmlElement(name = "InhalationHazardZone")
    protected String inhalationHazardZone;
    @XmlElement(name = "IsPassengerAircraftForbid")
    protected String isPassengerAircraftForbid;
    @XmlElement(name = "IsCommercialAircraftForbid")
    protected String isCommercialAircraftForbid;
    @XmlElement(name = "ERG")
    protected String erg;
    @XmlElement(name = "ERGAir")
    protected String ergAir;
    @XmlElement(name = "EMS")
    protected String ems;
    @XmlElement(name = "HazCompatGroup")
    protected String hazCompatGroup;

    /**
     * Obtém o valor da propriedade hazmatGenericGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getHazmatGenericGid() {
        return hazmatGenericGid;
    }

    /**
     * Define o valor da propriedade hazmatGenericGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setHazmatGenericGid(GLogXMLGidType value) {
        this.hazmatGenericGid = value;
    }

    /**
     * Obtém o valor da propriedade transactionCode.
     * 
     * @return
     *     possible object is
     *     {@link TransactionCodeType }
     *     
     */
    public TransactionCodeType getTransactionCode() {
        return transactionCode;
    }

    /**
     * Define o valor da propriedade transactionCode.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionCodeType }
     *     
     */
    public void setTransactionCode(TransactionCodeType value) {
        this.transactionCode = value;
    }

    /**
     * Obtém o valor da propriedade properShippingName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProperShippingName() {
        return properShippingName;
    }

    /**
     * Define o valor da propriedade properShippingName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProperShippingName(String value) {
        this.properShippingName = value;
    }

    /**
     * Obtém o valor da propriedade identificationNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdentificationNumber() {
        return identificationNumber;
    }

    /**
     * Define o valor da propriedade identificationNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdentificationNumber(String value) {
        this.identificationNumber = value;
    }

    /**
     * Obtém o valor da propriedade hazardousClass.
     * 
     * @return
     *     possible object is
     *     {@link HazardousClassType }
     *     
     */
    public HazardousClassType getHazardousClass() {
        return hazardousClass;
    }

    /**
     * Define o valor da propriedade hazardousClass.
     * 
     * @param value
     *     allowed object is
     *     {@link HazardousClassType }
     *     
     */
    public void setHazardousClass(HazardousClassType value) {
        this.hazardousClass = value;
    }

    /**
     * Obtém o valor da propriedade packagingGroup.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPackagingGroup() {
        return packagingGroup;
    }

    /**
     * Define o valor da propriedade packagingGroup.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPackagingGroup(String value) {
        this.packagingGroup = value;
    }

    /**
     * Obtém o valor da propriedade subsidiaryHazard.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubsidiaryHazard() {
        return subsidiaryHazard;
    }

    /**
     * Define o valor da propriedade subsidiaryHazard.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubsidiaryHazard(String value) {
        this.subsidiaryHazard = value;
    }

    /**
     * Obtém o valor da propriedade regulatoryAgencyDataSource.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRegulatoryAgencyDataSource() {
        return regulatoryAgencyDataSource;
    }

    /**
     * Define o valor da propriedade regulatoryAgencyDataSource.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegulatoryAgencyDataSource(String value) {
        this.regulatoryAgencyDataSource = value;
    }

    /**
     * Obtém o valor da propriedade specialProvisions.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSpecialProvisions() {
        return specialProvisions;
    }

    /**
     * Define o valor da propriedade specialProvisions.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSpecialProvisions(String value) {
        this.specialProvisions = value;
    }

    /**
     * Obtém o valor da propriedade psaSingaporeGroup.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPSASingaporeGroup() {
        return psaSingaporeGroup;
    }

    /**
     * Define o valor da propriedade psaSingaporeGroup.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPSASingaporeGroup(String value) {
        this.psaSingaporeGroup = value;
    }

    /**
     * Obtém o valor da propriedade symbolExtraMeaning.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSymbolExtraMeaning() {
        return symbolExtraMeaning;
    }

    /**
     * Define o valor da propriedade symbolExtraMeaning.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSymbolExtraMeaning(String value) {
        this.symbolExtraMeaning = value;
    }

    /**
     * Obtém o valor da propriedade imdgEmsNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getImdgEmsNumber() {
        return imdgEmsNumber;
    }

    /**
     * Define o valor da propriedade imdgEmsNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setImdgEmsNumber(String value) {
        this.imdgEmsNumber = value;
    }

    /**
     * Obtém o valor da propriedade isNos.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsNos() {
        return isNos;
    }

    /**
     * Define o valor da propriedade isNos.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsNos(String value) {
        this.isNos = value;
    }

    /**
     * Obtém o valor da propriedade isMarinePollutant.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsMarinePollutant() {
        return isMarinePollutant;
    }

    /**
     * Define o valor da propriedade isMarinePollutant.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsMarinePollutant(String value) {
        this.isMarinePollutant = value;
    }

    /**
     * Obtém o valor da propriedade isToxicInhalation.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsToxicInhalation() {
        return isToxicInhalation;
    }

    /**
     * Define o valor da propriedade isToxicInhalation.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsToxicInhalation(String value) {
        this.isToxicInhalation = value;
    }

    /**
     * Obtém o valor da propriedade inhalationHazardZone.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInhalationHazardZone() {
        return inhalationHazardZone;
    }

    /**
     * Define o valor da propriedade inhalationHazardZone.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInhalationHazardZone(String value) {
        this.inhalationHazardZone = value;
    }

    /**
     * Obtém o valor da propriedade isPassengerAircraftForbid.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsPassengerAircraftForbid() {
        return isPassengerAircraftForbid;
    }

    /**
     * Define o valor da propriedade isPassengerAircraftForbid.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsPassengerAircraftForbid(String value) {
        this.isPassengerAircraftForbid = value;
    }

    /**
     * Obtém o valor da propriedade isCommercialAircraftForbid.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsCommercialAircraftForbid() {
        return isCommercialAircraftForbid;
    }

    /**
     * Define o valor da propriedade isCommercialAircraftForbid.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsCommercialAircraftForbid(String value) {
        this.isCommercialAircraftForbid = value;
    }

    /**
     * Obtém o valor da propriedade erg.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getERG() {
        return erg;
    }

    /**
     * Define o valor da propriedade erg.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setERG(String value) {
        this.erg = value;
    }

    /**
     * Obtém o valor da propriedade ergAir.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getERGAir() {
        return ergAir;
    }

    /**
     * Define o valor da propriedade ergAir.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setERGAir(String value) {
        this.ergAir = value;
    }

    /**
     * Obtém o valor da propriedade ems.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEMS() {
        return ems;
    }

    /**
     * Define o valor da propriedade ems.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEMS(String value) {
        this.ems = value;
    }

    /**
     * Obtém o valor da propriedade hazCompatGroup.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHazCompatGroup() {
        return hazCompatGroup;
    }

    /**
     * Define o valor da propriedade hazCompatGroup.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHazCompatGroup(String value) {
        this.hazCompatGroup = value;
    }

}
