
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de PaymentSummaryDetailType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="PaymentSummaryDetailType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SequenceNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BilledRated" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}BilledRatedType"/>
 *         &lt;element name="FreightRate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FreightRateType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PaymentSummaryDetailType", propOrder = {
    "sequenceNumber",
    "billedRated",
    "freightRate"
})
public class PaymentSummaryDetailType {

    @XmlElement(name = "SequenceNumber")
    protected String sequenceNumber;
    @XmlElement(name = "BilledRated", required = true)
    protected BilledRatedType billedRated;
    @XmlElement(name = "FreightRate", required = true)
    protected FreightRateType freightRate;

    /**
     * Obtém o valor da propriedade sequenceNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSequenceNumber() {
        return sequenceNumber;
    }

    /**
     * Define o valor da propriedade sequenceNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSequenceNumber(String value) {
        this.sequenceNumber = value;
    }

    /**
     * Obtém o valor da propriedade billedRated.
     * 
     * @return
     *     possible object is
     *     {@link BilledRatedType }
     *     
     */
    public BilledRatedType getBilledRated() {
        return billedRated;
    }

    /**
     * Define o valor da propriedade billedRated.
     * 
     * @param value
     *     allowed object is
     *     {@link BilledRatedType }
     *     
     */
    public void setBilledRated(BilledRatedType value) {
        this.billedRated = value;
    }

    /**
     * Obtém o valor da propriedade freightRate.
     * 
     * @return
     *     possible object is
     *     {@link FreightRateType }
     *     
     */
    public FreightRateType getFreightRate() {
        return freightRate;
    }

    /**
     * Define o valor da propriedade freightRate.
     * 
     * @param value
     *     allowed object is
     *     {@link FreightRateType }
     *     
     */
    public void setFreightRate(FreightRateType value) {
        this.freightRate = value;
    }

}
