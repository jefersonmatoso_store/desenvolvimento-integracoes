
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 *             ShipmentGroupTenderOffer is a wrapper around the ShipmentGroup element. The interface currently
 *             does not support a response from the service provider for the offer.
 *          
 * 
 * <p>Classe Java de ShipmentGroupTenderOfferType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="ShipmentGroupTenderOfferType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OTMTransactionInOut">
 *       &lt;sequence>
 *         &lt;element name="ShipmentGroup" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ShipmentGroupType"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ShipmentGroupTenderOfferType", propOrder = {
    "shipmentGroup"
})
public class ShipmentGroupTenderOfferType
    extends OTMTransactionInOut
{

    @XmlElement(name = "ShipmentGroup", required = true)
    protected ShipmentGroupType shipmentGroup;

    /**
     * Obtém o valor da propriedade shipmentGroup.
     * 
     * @return
     *     possible object is
     *     {@link ShipmentGroupType }
     *     
     */
    public ShipmentGroupType getShipmentGroup() {
        return shipmentGroup;
    }

    /**
     * Define o valor da propriedade shipmentGroup.
     * 
     * @param value
     *     allowed object is
     *     {@link ShipmentGroupType }
     *     
     */
    public void setShipmentGroup(ShipmentGroupType value) {
        this.shipmentGroup = value;
    }

}
