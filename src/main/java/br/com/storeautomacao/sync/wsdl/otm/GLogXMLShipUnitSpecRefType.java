
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de GLogXMLShipUnitSpecRefType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="GLogXMLShipUnitSpecRefType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ShipUnitSpecRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ShipUnitSpecRefType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GLogXMLShipUnitSpecRefType", propOrder = {
    "shipUnitSpecRef"
})
public class GLogXMLShipUnitSpecRefType {

    @XmlElement(name = "ShipUnitSpecRef", required = true)
    protected ShipUnitSpecRefType shipUnitSpecRef;

    /**
     * Obtém o valor da propriedade shipUnitSpecRef.
     * 
     * @return
     *     possible object is
     *     {@link ShipUnitSpecRefType }
     *     
     */
    public ShipUnitSpecRefType getShipUnitSpecRef() {
        return shipUnitSpecRef;
    }

    /**
     * Define o valor da propriedade shipUnitSpecRef.
     * 
     * @param value
     *     allowed object is
     *     {@link ShipUnitSpecRefType }
     *     
     */
    public void setShipUnitSpecRef(ShipUnitSpecRefType value) {
        this.shipUnitSpecRef = value;
    }

}
