
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 *             Specifies the route template strategic info.
 *             The values are prepopulated from the cooperative route if the creation source is C.
 *          
 * 
 * <p>Classe Java de RouteTempStrategicInfoType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="RouteTempStrategicInfoType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TotalDistance" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDistanceType" minOccurs="0"/>
 *         &lt;element name="LoadedDistance" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDistanceType" minOccurs="0"/>
 *         &lt;element name="DeadheadDistance" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDistanceType" minOccurs="0"/>
 *         &lt;element name="DeadheadPct" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SavingsPerInstance" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/>
 *         &lt;element name="Duration" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}DurationType" minOccurs="0"/>
 *         &lt;element name="ConfidenceFactor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RouteTempStrategicInfoType", propOrder = {
    "totalDistance",
    "loadedDistance",
    "deadheadDistance",
    "deadheadPct",
    "savingsPerInstance",
    "duration",
    "confidenceFactor"
})
public class RouteTempStrategicInfoType {

    @XmlElement(name = "TotalDistance")
    protected GLogXMLDistanceType totalDistance;
    @XmlElement(name = "LoadedDistance")
    protected GLogXMLDistanceType loadedDistance;
    @XmlElement(name = "DeadheadDistance")
    protected GLogXMLDistanceType deadheadDistance;
    @XmlElement(name = "DeadheadPct")
    protected String deadheadPct;
    @XmlElement(name = "SavingsPerInstance")
    protected GLogXMLFinancialAmountType savingsPerInstance;
    @XmlElement(name = "Duration")
    protected DurationType duration;
    @XmlElement(name = "ConfidenceFactor")
    protected String confidenceFactor;

    /**
     * Obtém o valor da propriedade totalDistance.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDistanceType }
     *     
     */
    public GLogXMLDistanceType getTotalDistance() {
        return totalDistance;
    }

    /**
     * Define o valor da propriedade totalDistance.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDistanceType }
     *     
     */
    public void setTotalDistance(GLogXMLDistanceType value) {
        this.totalDistance = value;
    }

    /**
     * Obtém o valor da propriedade loadedDistance.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDistanceType }
     *     
     */
    public GLogXMLDistanceType getLoadedDistance() {
        return loadedDistance;
    }

    /**
     * Define o valor da propriedade loadedDistance.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDistanceType }
     *     
     */
    public void setLoadedDistance(GLogXMLDistanceType value) {
        this.loadedDistance = value;
    }

    /**
     * Obtém o valor da propriedade deadheadDistance.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDistanceType }
     *     
     */
    public GLogXMLDistanceType getDeadheadDistance() {
        return deadheadDistance;
    }

    /**
     * Define o valor da propriedade deadheadDistance.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDistanceType }
     *     
     */
    public void setDeadheadDistance(GLogXMLDistanceType value) {
        this.deadheadDistance = value;
    }

    /**
     * Obtém o valor da propriedade deadheadPct.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeadheadPct() {
        return deadheadPct;
    }

    /**
     * Define o valor da propriedade deadheadPct.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeadheadPct(String value) {
        this.deadheadPct = value;
    }

    /**
     * Obtém o valor da propriedade savingsPerInstance.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getSavingsPerInstance() {
        return savingsPerInstance;
    }

    /**
     * Define o valor da propriedade savingsPerInstance.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setSavingsPerInstance(GLogXMLFinancialAmountType value) {
        this.savingsPerInstance = value;
    }

    /**
     * Obtém o valor da propriedade duration.
     * 
     * @return
     *     possible object is
     *     {@link DurationType }
     *     
     */
    public DurationType getDuration() {
        return duration;
    }

    /**
     * Define o valor da propriedade duration.
     * 
     * @param value
     *     allowed object is
     *     {@link DurationType }
     *     
     */
    public void setDuration(DurationType value) {
        this.duration = value;
    }

    /**
     * Obtém o valor da propriedade confidenceFactor.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConfidenceFactor() {
        return confidenceFactor;
    }

    /**
     * Define o valor da propriedade confidenceFactor.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConfidenceFactor(String value) {
        this.confidenceFactor = value;
    }

}
