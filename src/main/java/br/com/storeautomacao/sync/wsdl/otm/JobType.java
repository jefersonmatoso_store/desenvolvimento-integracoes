
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Classe Java de JobType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="JobType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OTMTransactionInOut">
 *       &lt;sequence>
 *         &lt;element name="SendReason" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}SendReasonType" minOccurs="0"/>
 *         &lt;element name="JobGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="TransactionCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransactionCodeType"/>
 *         &lt;element name="ReplaceChildren" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ReplaceChildrenType" minOccurs="0"/>
 *         &lt;element name="JobQueryType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="JobTypeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="ConsolidationTypeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="MovePerspectiveGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="JobType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ConsolGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="IsMemoBL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MemoBLShipmentGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="JobInvolvedParty" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}JobInvolvedPartyType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="ShippingAgentContact" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ShippingAgentContactType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Refnum" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RefnumType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Remark" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RemarkType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Status" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}StatusType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Location" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LocationType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Release" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ReleaseType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="BuySide" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ShipmentAllocationType" minOccurs="0"/>
 *         &lt;element name="SellSide" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ShipmentAllocationType" minOccurs="0"/>
 *         &lt;element name="Billing" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}BillingType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Text" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TextType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="HouseJobs" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Job" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}JobType" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="FlexFieldStrings" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldStringType" minOccurs="0"/>
 *         &lt;element name="FlexFieldNumbers" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldNumberType" minOccurs="0"/>
 *         &lt;element name="FlexFieldDates" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldDateType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "JobType", propOrder = {
    "sendReason",
    "jobGid",
    "transactionCode",
    "replaceChildren",
    "jobQueryType",
    "jobTypeGid",
    "consolidationTypeGid",
    "movePerspectiveGid",
    "jobType",
    "consolGid",
    "isMemoBL",
    "memoBLShipmentGid",
    "jobInvolvedParty",
    "shippingAgentContact",
    "refnum",
    "remark",
    "status",
    "location",
    "release",
    "buySide",
    "sellSide",
    "billing",
    "text",
    "houseJobs",
    "flexFieldStrings",
    "flexFieldNumbers",
    "flexFieldDates"
})
public class JobType
    extends OTMTransactionInOut
{

    @XmlElement(name = "SendReason")
    protected SendReasonType sendReason;
    @XmlElement(name = "JobGid", required = true)
    protected GLogXMLGidType jobGid;
    @XmlElement(name = "TransactionCode", required = true)
    @XmlSchemaType(name = "string")
    protected TransactionCodeType transactionCode;
    @XmlElement(name = "ReplaceChildren")
    protected ReplaceChildrenType replaceChildren;
    @XmlElement(name = "JobQueryType")
    protected String jobQueryType;
    @XmlElement(name = "JobTypeGid")
    protected GLogXMLGidType jobTypeGid;
    @XmlElement(name = "ConsolidationTypeGid")
    protected GLogXMLGidType consolidationTypeGid;
    @XmlElement(name = "MovePerspectiveGid")
    protected GLogXMLGidType movePerspectiveGid;
    @XmlElement(name = "JobType")
    protected String jobType;
    @XmlElement(name = "ConsolGid")
    protected GLogXMLGidType consolGid;
    @XmlElement(name = "IsMemoBL")
    protected String isMemoBL;
    @XmlElement(name = "MemoBLShipmentGid")
    protected GLogXMLGidType memoBLShipmentGid;
    @XmlElement(name = "JobInvolvedParty")
    protected List<JobInvolvedPartyType> jobInvolvedParty;
    @XmlElement(name = "ShippingAgentContact")
    protected List<ShippingAgentContactType> shippingAgentContact;
    @XmlElement(name = "Refnum")
    protected List<RefnumType> refnum;
    @XmlElement(name = "Remark")
    protected List<RemarkType> remark;
    @XmlElement(name = "Status")
    protected List<StatusType> status;
    @XmlElement(name = "Location")
    protected List<LocationType> location;
    @XmlElement(name = "Release")
    protected List<ReleaseType> release;
    @XmlElement(name = "BuySide")
    protected ShipmentAllocationType buySide;
    @XmlElement(name = "SellSide")
    protected ShipmentAllocationType sellSide;
    @XmlElement(name = "Billing")
    protected List<BillingType> billing;
    @XmlElement(name = "Text")
    protected List<TextType> text;
    @XmlElement(name = "HouseJobs")
    protected HouseJobs houseJobs;
    @XmlElement(name = "FlexFieldStrings")
    protected FlexFieldStringType flexFieldStrings;
    @XmlElement(name = "FlexFieldNumbers")
    protected FlexFieldNumberType flexFieldNumbers;
    @XmlElement(name = "FlexFieldDates")
    protected FlexFieldDateType flexFieldDates;

    /**
     * Obtém o valor da propriedade sendReason.
     * 
     * @return
     *     possible object is
     *     {@link SendReasonType }
     *     
     */
    public SendReasonType getSendReason() {
        return sendReason;
    }

    /**
     * Define o valor da propriedade sendReason.
     * 
     * @param value
     *     allowed object is
     *     {@link SendReasonType }
     *     
     */
    public void setSendReason(SendReasonType value) {
        this.sendReason = value;
    }

    /**
     * Obtém o valor da propriedade jobGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getJobGid() {
        return jobGid;
    }

    /**
     * Define o valor da propriedade jobGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setJobGid(GLogXMLGidType value) {
        this.jobGid = value;
    }

    /**
     * Obtém o valor da propriedade transactionCode.
     * 
     * @return
     *     possible object is
     *     {@link TransactionCodeType }
     *     
     */
    public TransactionCodeType getTransactionCode() {
        return transactionCode;
    }

    /**
     * Define o valor da propriedade transactionCode.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionCodeType }
     *     
     */
    public void setTransactionCode(TransactionCodeType value) {
        this.transactionCode = value;
    }

    /**
     * Obtém o valor da propriedade replaceChildren.
     * 
     * @return
     *     possible object is
     *     {@link ReplaceChildrenType }
     *     
     */
    public ReplaceChildrenType getReplaceChildren() {
        return replaceChildren;
    }

    /**
     * Define o valor da propriedade replaceChildren.
     * 
     * @param value
     *     allowed object is
     *     {@link ReplaceChildrenType }
     *     
     */
    public void setReplaceChildren(ReplaceChildrenType value) {
        this.replaceChildren = value;
    }

    /**
     * Obtém o valor da propriedade jobQueryType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJobQueryType() {
        return jobQueryType;
    }

    /**
     * Define o valor da propriedade jobQueryType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJobQueryType(String value) {
        this.jobQueryType = value;
    }

    /**
     * Obtém o valor da propriedade jobTypeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getJobTypeGid() {
        return jobTypeGid;
    }

    /**
     * Define o valor da propriedade jobTypeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setJobTypeGid(GLogXMLGidType value) {
        this.jobTypeGid = value;
    }

    /**
     * Obtém o valor da propriedade consolidationTypeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getConsolidationTypeGid() {
        return consolidationTypeGid;
    }

    /**
     * Define o valor da propriedade consolidationTypeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setConsolidationTypeGid(GLogXMLGidType value) {
        this.consolidationTypeGid = value;
    }

    /**
     * Obtém o valor da propriedade movePerspectiveGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getMovePerspectiveGid() {
        return movePerspectiveGid;
    }

    /**
     * Define o valor da propriedade movePerspectiveGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setMovePerspectiveGid(GLogXMLGidType value) {
        this.movePerspectiveGid = value;
    }

    /**
     * Obtém o valor da propriedade jobType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJobType() {
        return jobType;
    }

    /**
     * Define o valor da propriedade jobType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJobType(String value) {
        this.jobType = value;
    }

    /**
     * Obtém o valor da propriedade consolGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getConsolGid() {
        return consolGid;
    }

    /**
     * Define o valor da propriedade consolGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setConsolGid(GLogXMLGidType value) {
        this.consolGid = value;
    }

    /**
     * Obtém o valor da propriedade isMemoBL.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsMemoBL() {
        return isMemoBL;
    }

    /**
     * Define o valor da propriedade isMemoBL.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsMemoBL(String value) {
        this.isMemoBL = value;
    }

    /**
     * Obtém o valor da propriedade memoBLShipmentGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getMemoBLShipmentGid() {
        return memoBLShipmentGid;
    }

    /**
     * Define o valor da propriedade memoBLShipmentGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setMemoBLShipmentGid(GLogXMLGidType value) {
        this.memoBLShipmentGid = value;
    }

    /**
     * Gets the value of the jobInvolvedParty property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the jobInvolvedParty property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getJobInvolvedParty().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link JobInvolvedPartyType }
     * 
     * 
     */
    public List<JobInvolvedPartyType> getJobInvolvedParty() {
        if (jobInvolvedParty == null) {
            jobInvolvedParty = new ArrayList<JobInvolvedPartyType>();
        }
        return this.jobInvolvedParty;
    }

    /**
     * Gets the value of the shippingAgentContact property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the shippingAgentContact property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getShippingAgentContact().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ShippingAgentContactType }
     * 
     * 
     */
    public List<ShippingAgentContactType> getShippingAgentContact() {
        if (shippingAgentContact == null) {
            shippingAgentContact = new ArrayList<ShippingAgentContactType>();
        }
        return this.shippingAgentContact;
    }

    /**
     * Gets the value of the refnum property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the refnum property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRefnum().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RefnumType }
     * 
     * 
     */
    public List<RefnumType> getRefnum() {
        if (refnum == null) {
            refnum = new ArrayList<RefnumType>();
        }
        return this.refnum;
    }

    /**
     * Gets the value of the remark property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the remark property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRemark().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RemarkType }
     * 
     * 
     */
    public List<RemarkType> getRemark() {
        if (remark == null) {
            remark = new ArrayList<RemarkType>();
        }
        return this.remark;
    }

    /**
     * Gets the value of the status property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the status property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getStatus().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link StatusType }
     * 
     * 
     */
    public List<StatusType> getStatus() {
        if (status == null) {
            status = new ArrayList<StatusType>();
        }
        return this.status;
    }

    /**
     * Gets the value of the location property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the location property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLocation().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LocationType }
     * 
     * 
     */
    public List<LocationType> getLocation() {
        if (location == null) {
            location = new ArrayList<LocationType>();
        }
        return this.location;
    }

    /**
     * Gets the value of the release property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the release property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRelease().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ReleaseType }
     * 
     * 
     */
    public List<ReleaseType> getRelease() {
        if (release == null) {
            release = new ArrayList<ReleaseType>();
        }
        return this.release;
    }

    /**
     * Obtém o valor da propriedade buySide.
     * 
     * @return
     *     possible object is
     *     {@link ShipmentAllocationType }
     *     
     */
    public ShipmentAllocationType getBuySide() {
        return buySide;
    }

    /**
     * Define o valor da propriedade buySide.
     * 
     * @param value
     *     allowed object is
     *     {@link ShipmentAllocationType }
     *     
     */
    public void setBuySide(ShipmentAllocationType value) {
        this.buySide = value;
    }

    /**
     * Obtém o valor da propriedade sellSide.
     * 
     * @return
     *     possible object is
     *     {@link ShipmentAllocationType }
     *     
     */
    public ShipmentAllocationType getSellSide() {
        return sellSide;
    }

    /**
     * Define o valor da propriedade sellSide.
     * 
     * @param value
     *     allowed object is
     *     {@link ShipmentAllocationType }
     *     
     */
    public void setSellSide(ShipmentAllocationType value) {
        this.sellSide = value;
    }

    /**
     * Gets the value of the billing property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the billing property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBilling().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BillingType }
     * 
     * 
     */
    public List<BillingType> getBilling() {
        if (billing == null) {
            billing = new ArrayList<BillingType>();
        }
        return this.billing;
    }

    /**
     * Gets the value of the text property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the text property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getText().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TextType }
     * 
     * 
     */
    public List<TextType> getText() {
        if (text == null) {
            text = new ArrayList<TextType>();
        }
        return this.text;
    }

    /**
     * Obtém o valor da propriedade houseJobs.
     * 
     * @return
     *     possible object is
     *     {@link HouseJobs }
     *     
     */
    public HouseJobs getHouseJobs() {
        return houseJobs;
    }

    /**
     * Define o valor da propriedade houseJobs.
     * 
     * @param value
     *     allowed object is
     *     {@link HouseJobs }
     *     
     */
    public void setHouseJobs(HouseJobs value) {
        this.houseJobs = value;
    }

    /**
     * Obtém o valor da propriedade flexFieldStrings.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldStringType }
     *     
     */
    public FlexFieldStringType getFlexFieldStrings() {
        return flexFieldStrings;
    }

    /**
     * Define o valor da propriedade flexFieldStrings.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldStringType }
     *     
     */
    public void setFlexFieldStrings(FlexFieldStringType value) {
        this.flexFieldStrings = value;
    }

    /**
     * Obtém o valor da propriedade flexFieldNumbers.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldNumberType }
     *     
     */
    public FlexFieldNumberType getFlexFieldNumbers() {
        return flexFieldNumbers;
    }

    /**
     * Define o valor da propriedade flexFieldNumbers.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldNumberType }
     *     
     */
    public void setFlexFieldNumbers(FlexFieldNumberType value) {
        this.flexFieldNumbers = value;
    }

    /**
     * Obtém o valor da propriedade flexFieldDates.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldDateType }
     *     
     */
    public FlexFieldDateType getFlexFieldDates() {
        return flexFieldDates;
    }

    /**
     * Define o valor da propriedade flexFieldDates.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldDateType }
     *     
     */
    public void setFlexFieldDates(FlexFieldDateType value) {
        this.flexFieldDates = value;
    }


    /**
     * <p>Classe Java de anonymous complex type.
     * 
     * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Job" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}JobType" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "job"
    })
    public static class HouseJobs {

        @XmlElement(name = "Job")
        protected List<JobType> job;

        /**
         * Gets the value of the job property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the job property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getJob().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link JobType }
         * 
         * 
         */
        public List<JobType> getJob() {
            if (job == null) {
                job = new ArrayList<JobType>();
            }
            return this.job;
        }

    }

}
