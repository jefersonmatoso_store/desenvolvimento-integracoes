
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;


/**
 * Contact is a structure for specifying contact information for a location.
 *             A location may have multiple contacts.
 *          
 * 
 * <p>Classe Java de ContactType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="ContactType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OTMTransactionInOut">
 *       &lt;sequence>
 *         &lt;element name="ContactGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="TransactionCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransactionCodeType" minOccurs="0"/>
 *         &lt;element name="ReplaceChildren" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ReplaceChildrenType" minOccurs="0"/>
 *         &lt;element name="EmailAddress" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FirstName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MiddleName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LastName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="JobTitle" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Phone1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Phone2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Fax" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LanguageSpoken" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsPrimaryContact" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;choice minOccurs="0">
 *           &lt;element name="ExternalSystem">
 *             &lt;complexType>
 *               &lt;complexContent>
 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   &lt;sequence>
 *                     &lt;element name="ExternalSystemGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *                     &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="URL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="IntQueueName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="UserName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="Password" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}PasswordType" minOccurs="0"/>
 *                     &lt;element name="UseGlcredential" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                     &lt;element name="MaxBytesPerTransmission" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="MaxTransactsPerTransmission" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;/sequence>
 *                 &lt;/restriction>
 *               &lt;/complexContent>
 *             &lt;/complexType>
 *           &lt;/element>
 *           &lt;element name="ExternalSystemGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;/choice>
 *         &lt;element name="Remark" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RemarkType" minOccurs="0"/>
 *         &lt;element name="CommunicationMethod" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}CommunicationMethodType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="GlUserGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="RecipientDomainName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LocationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;choice minOccurs="0">
 *           &lt;element name="ConsolidationProfile">
 *             &lt;complexType>
 *               &lt;complexContent>
 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   &lt;sequence>
 *                     &lt;element name="ConsolidationProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *                     &lt;element name="TransactionCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransactionCodeType"/>
 *                     &lt;element name="TimeWindowGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *                     &lt;element name="StylesheetProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *                     &lt;element name="BatchSize" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;/sequence>
 *                 &lt;/restriction>
 *               &lt;/complexContent>
 *             &lt;/complexType>
 *           &lt;/element>
 *           &lt;element name="ConsolidationProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;/choice>
 *         &lt;element name="ConsolidatedNotifyOnly" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsNotificationOn" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Preference" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}PreferenceType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="ContactCorporation" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="CorporationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="IsFromAddress" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FromAddrContactGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="AlternateName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CompanyName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Refnum" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RefnumType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Status" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}StatusType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="RemarkWithQual" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RemarkType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Telex" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TimeZoneGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="CellPhone" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="UseMessageHub" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MessageProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="PrinterGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="FlexFieldStrings" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldStringType" minOccurs="0"/>
 *         &lt;element name="FlexFieldNumbers" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldNumberType" minOccurs="0"/>
 *         &lt;element name="FlexFieldDates" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldDateType" minOccurs="0"/>
 *         &lt;element name="Indicator" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ContactType", propOrder = {
    "contactGid",
    "transactionCode",
    "replaceChildren",
    "emailAddress",
    "firstName",
    "middleName",
    "lastName",
    "jobTitle",
    "phone1",
    "phone2",
    "fax",
    "languageSpoken",
    "isPrimaryContact",
    "externalSystem",
    "externalSystemGid",
    "remark",
    "communicationMethod",
    "glUserGid",
    "recipientDomainName",
    "locationGid",
    "consolidationProfile",
    "consolidationProfileGid",
    "consolidatedNotifyOnly",
    "isNotificationOn",
    "preference",
    "contactCorporation",
    "isFromAddress",
    "fromAddrContactGid",
    "alternateName",
    "companyName",
    "description",
    "refnum",
    "status",
    "remarkWithQual",
    "telex",
    "timeZoneGid",
    "cellPhone",
    "useMessageHub",
    "messageProfileGid",
    "printerGid",
    "flexFieldStrings",
    "flexFieldNumbers",
    "flexFieldDates",
    "indicator"
})
public class ContactType
    extends OTMTransactionInOut
{

    @XmlElement(name = "ContactGid", required = true)
    protected GLogXMLGidType contactGid;
    @XmlElement(name = "TransactionCode")
    @XmlSchemaType(name = "string")
    protected TransactionCodeType transactionCode;
    @XmlElement(name = "ReplaceChildren")
    protected ReplaceChildrenType replaceChildren;
    @XmlElement(name = "EmailAddress")
    protected String emailAddress;
    @XmlElement(name = "FirstName")
    protected String firstName;
    @XmlElement(name = "MiddleName")
    protected String middleName;
    @XmlElement(name = "LastName")
    protected String lastName;
    @XmlElement(name = "JobTitle")
    protected String jobTitle;
    @XmlElement(name = "Phone1")
    protected String phone1;
    @XmlElement(name = "Phone2")
    protected String phone2;
    @XmlElement(name = "Fax")
    protected String fax;
    @XmlElement(name = "LanguageSpoken")
    protected String languageSpoken;
    @XmlElement(name = "IsPrimaryContact")
    protected String isPrimaryContact;
    @XmlElement(name = "ExternalSystem")
    protected ExternalSystem externalSystem;
    @XmlElement(name = "ExternalSystemGid")
    protected GLogXMLGidType externalSystemGid;
    @XmlElement(name = "Remark")
    protected RemarkType remark;
    @XmlElement(name = "CommunicationMethod")
    protected List<CommunicationMethodType> communicationMethod;
    @XmlElement(name = "GlUserGid")
    protected GLogXMLGidType glUserGid;
    @XmlElement(name = "RecipientDomainName")
    protected String recipientDomainName;
    @XmlElement(name = "LocationGid")
    protected GLogXMLGidType locationGid;
    @XmlElement(name = "ConsolidationProfile")
    protected ConsolidationProfile consolidationProfile;
    @XmlElement(name = "ConsolidationProfileGid")
    protected GLogXMLGidType consolidationProfileGid;
    @XmlElement(name = "ConsolidatedNotifyOnly")
    protected String consolidatedNotifyOnly;
    @XmlElement(name = "IsNotificationOn")
    protected String isNotificationOn;
    @XmlElement(name = "Preference")
    protected List<PreferenceType> preference;
    @XmlElement(name = "ContactCorporation")
    protected List<ContactCorporation> contactCorporation;
    @XmlElement(name = "IsFromAddress")
    protected String isFromAddress;
    @XmlElement(name = "FromAddrContactGid")
    protected GLogXMLGidType fromAddrContactGid;
    @XmlElement(name = "AlternateName")
    protected String alternateName;
    @XmlElement(name = "CompanyName")
    protected String companyName;
    @XmlElement(name = "Description")
    protected String description;
    @XmlElement(name = "Refnum")
    protected List<RefnumType> refnum;
    @XmlElement(name = "Status")
    protected List<StatusType> status;
    @XmlElement(name = "RemarkWithQual")
    protected List<RemarkType> remarkWithQual;
    @XmlElement(name = "Telex")
    protected String telex;
    @XmlElement(name = "TimeZoneGid")
    protected GLogXMLGidType timeZoneGid;
    @XmlElement(name = "CellPhone")
    protected String cellPhone;
    @XmlElement(name = "UseMessageHub")
    protected String useMessageHub;
    @XmlElement(name = "MessageProfileGid")
    protected GLogXMLGidType messageProfileGid;
    @XmlElement(name = "PrinterGid")
    protected GLogXMLGidType printerGid;
    @XmlElement(name = "FlexFieldStrings")
    protected FlexFieldStringType flexFieldStrings;
    @XmlElement(name = "FlexFieldNumbers")
    protected FlexFieldNumberType flexFieldNumbers;
    @XmlElement(name = "FlexFieldDates")
    protected FlexFieldDateType flexFieldDates;
    @XmlElement(name = "Indicator")
    protected String indicator;

    /**
     * Obtém o valor da propriedade contactGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getContactGid() {
        return contactGid;
    }

    /**
     * Define o valor da propriedade contactGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setContactGid(GLogXMLGidType value) {
        this.contactGid = value;
    }

    /**
     * Obtém o valor da propriedade transactionCode.
     * 
     * @return
     *     possible object is
     *     {@link TransactionCodeType }
     *     
     */
    public TransactionCodeType getTransactionCode() {
        return transactionCode;
    }

    /**
     * Define o valor da propriedade transactionCode.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionCodeType }
     *     
     */
    public void setTransactionCode(TransactionCodeType value) {
        this.transactionCode = value;
    }

    /**
     * Obtém o valor da propriedade replaceChildren.
     * 
     * @return
     *     possible object is
     *     {@link ReplaceChildrenType }
     *     
     */
    public ReplaceChildrenType getReplaceChildren() {
        return replaceChildren;
    }

    /**
     * Define o valor da propriedade replaceChildren.
     * 
     * @param value
     *     allowed object is
     *     {@link ReplaceChildrenType }
     *     
     */
    public void setReplaceChildren(ReplaceChildrenType value) {
        this.replaceChildren = value;
    }

    /**
     * Obtém o valor da propriedade emailAddress.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmailAddress() {
        return emailAddress;
    }

    /**
     * Define o valor da propriedade emailAddress.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmailAddress(String value) {
        this.emailAddress = value;
    }

    /**
     * Obtém o valor da propriedade firstName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Define o valor da propriedade firstName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFirstName(String value) {
        this.firstName = value;
    }

    /**
     * Obtém o valor da propriedade middleName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMiddleName() {
        return middleName;
    }

    /**
     * Define o valor da propriedade middleName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMiddleName(String value) {
        this.middleName = value;
    }

    /**
     * Obtém o valor da propriedade lastName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Define o valor da propriedade lastName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLastName(String value) {
        this.lastName = value;
    }

    /**
     * Obtém o valor da propriedade jobTitle.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJobTitle() {
        return jobTitle;
    }

    /**
     * Define o valor da propriedade jobTitle.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJobTitle(String value) {
        this.jobTitle = value;
    }

    /**
     * Obtém o valor da propriedade phone1.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPhone1() {
        return phone1;
    }

    /**
     * Define o valor da propriedade phone1.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPhone1(String value) {
        this.phone1 = value;
    }

    /**
     * Obtém o valor da propriedade phone2.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPhone2() {
        return phone2;
    }

    /**
     * Define o valor da propriedade phone2.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPhone2(String value) {
        this.phone2 = value;
    }

    /**
     * Obtém o valor da propriedade fax.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFax() {
        return fax;
    }

    /**
     * Define o valor da propriedade fax.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFax(String value) {
        this.fax = value;
    }

    /**
     * Obtém o valor da propriedade languageSpoken.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLanguageSpoken() {
        return languageSpoken;
    }

    /**
     * Define o valor da propriedade languageSpoken.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLanguageSpoken(String value) {
        this.languageSpoken = value;
    }

    /**
     * Obtém o valor da propriedade isPrimaryContact.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsPrimaryContact() {
        return isPrimaryContact;
    }

    /**
     * Define o valor da propriedade isPrimaryContact.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsPrimaryContact(String value) {
        this.isPrimaryContact = value;
    }

    /**
     * Obtém o valor da propriedade externalSystem.
     * 
     * @return
     *     possible object is
     *     {@link ExternalSystem }
     *     
     */
    public ExternalSystem getExternalSystem() {
        return externalSystem;
    }

    /**
     * Define o valor da propriedade externalSystem.
     * 
     * @param value
     *     allowed object is
     *     {@link ExternalSystem }
     *     
     */
    public void setExternalSystem(ExternalSystem value) {
        this.externalSystem = value;
    }

    /**
     * Obtém o valor da propriedade externalSystemGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getExternalSystemGid() {
        return externalSystemGid;
    }

    /**
     * Define o valor da propriedade externalSystemGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setExternalSystemGid(GLogXMLGidType value) {
        this.externalSystemGid = value;
    }

    /**
     * Obtém o valor da propriedade remark.
     * 
     * @return
     *     possible object is
     *     {@link RemarkType }
     *     
     */
    public RemarkType getRemark() {
        return remark;
    }

    /**
     * Define o valor da propriedade remark.
     * 
     * @param value
     *     allowed object is
     *     {@link RemarkType }
     *     
     */
    public void setRemark(RemarkType value) {
        this.remark = value;
    }

    /**
     * Gets the value of the communicationMethod property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the communicationMethod property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCommunicationMethod().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CommunicationMethodType }
     * 
     * 
     */
    public List<CommunicationMethodType> getCommunicationMethod() {
        if (communicationMethod == null) {
            communicationMethod = new ArrayList<CommunicationMethodType>();
        }
        return this.communicationMethod;
    }

    /**
     * Obtém o valor da propriedade glUserGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getGlUserGid() {
        return glUserGid;
    }

    /**
     * Define o valor da propriedade glUserGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setGlUserGid(GLogXMLGidType value) {
        this.glUserGid = value;
    }

    /**
     * Obtém o valor da propriedade recipientDomainName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRecipientDomainName() {
        return recipientDomainName;
    }

    /**
     * Define o valor da propriedade recipientDomainName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRecipientDomainName(String value) {
        this.recipientDomainName = value;
    }

    /**
     * Obtém o valor da propriedade locationGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getLocationGid() {
        return locationGid;
    }

    /**
     * Define o valor da propriedade locationGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setLocationGid(GLogXMLGidType value) {
        this.locationGid = value;
    }

    /**
     * Obtém o valor da propriedade consolidationProfile.
     * 
     * @return
     *     possible object is
     *     {@link ConsolidationProfile }
     *     
     */
    public ConsolidationProfile getConsolidationProfile() {
        return consolidationProfile;
    }

    /**
     * Define o valor da propriedade consolidationProfile.
     * 
     * @param value
     *     allowed object is
     *     {@link ConsolidationProfile }
     *     
     */
    public void setConsolidationProfile(ConsolidationProfile value) {
        this.consolidationProfile = value;
    }

    /**
     * Obtém o valor da propriedade consolidationProfileGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getConsolidationProfileGid() {
        return consolidationProfileGid;
    }

    /**
     * Define o valor da propriedade consolidationProfileGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setConsolidationProfileGid(GLogXMLGidType value) {
        this.consolidationProfileGid = value;
    }

    /**
     * Obtém o valor da propriedade consolidatedNotifyOnly.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConsolidatedNotifyOnly() {
        return consolidatedNotifyOnly;
    }

    /**
     * Define o valor da propriedade consolidatedNotifyOnly.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConsolidatedNotifyOnly(String value) {
        this.consolidatedNotifyOnly = value;
    }

    /**
     * Obtém o valor da propriedade isNotificationOn.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsNotificationOn() {
        return isNotificationOn;
    }

    /**
     * Define o valor da propriedade isNotificationOn.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsNotificationOn(String value) {
        this.isNotificationOn = value;
    }

    /**
     * Gets the value of the preference property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the preference property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPreference().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PreferenceType }
     * 
     * 
     */
    public List<PreferenceType> getPreference() {
        if (preference == null) {
            preference = new ArrayList<PreferenceType>();
        }
        return this.preference;
    }

    /**
     * Gets the value of the contactCorporation property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the contactCorporation property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getContactCorporation().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ContactCorporation }
     * 
     * 
     */
    public List<ContactCorporation> getContactCorporation() {
        if (contactCorporation == null) {
            contactCorporation = new ArrayList<ContactCorporation>();
        }
        return this.contactCorporation;
    }

    /**
     * Obtém o valor da propriedade isFromAddress.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsFromAddress() {
        return isFromAddress;
    }

    /**
     * Define o valor da propriedade isFromAddress.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsFromAddress(String value) {
        this.isFromAddress = value;
    }

    /**
     * Obtém o valor da propriedade fromAddrContactGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getFromAddrContactGid() {
        return fromAddrContactGid;
    }

    /**
     * Define o valor da propriedade fromAddrContactGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setFromAddrContactGid(GLogXMLGidType value) {
        this.fromAddrContactGid = value;
    }

    /**
     * Obtém o valor da propriedade alternateName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAlternateName() {
        return alternateName;
    }

    /**
     * Define o valor da propriedade alternateName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAlternateName(String value) {
        this.alternateName = value;
    }

    /**
     * Obtém o valor da propriedade companyName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCompanyName() {
        return companyName;
    }

    /**
     * Define o valor da propriedade companyName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCompanyName(String value) {
        this.companyName = value;
    }

    /**
     * Obtém o valor da propriedade description.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Define o valor da propriedade description.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the refnum property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the refnum property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRefnum().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RefnumType }
     * 
     * 
     */
    public List<RefnumType> getRefnum() {
        if (refnum == null) {
            refnum = new ArrayList<RefnumType>();
        }
        return this.refnum;
    }

    /**
     * Gets the value of the status property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the status property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getStatus().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link StatusType }
     * 
     * 
     */
    public List<StatusType> getStatus() {
        if (status == null) {
            status = new ArrayList<StatusType>();
        }
        return this.status;
    }

    /**
     * Gets the value of the remarkWithQual property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the remarkWithQual property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRemarkWithQual().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RemarkType }
     * 
     * 
     */
    public List<RemarkType> getRemarkWithQual() {
        if (remarkWithQual == null) {
            remarkWithQual = new ArrayList<RemarkType>();
        }
        return this.remarkWithQual;
    }

    /**
     * Obtém o valor da propriedade telex.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTelex() {
        return telex;
    }

    /**
     * Define o valor da propriedade telex.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTelex(String value) {
        this.telex = value;
    }

    /**
     * Obtém o valor da propriedade timeZoneGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getTimeZoneGid() {
        return timeZoneGid;
    }

    /**
     * Define o valor da propriedade timeZoneGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setTimeZoneGid(GLogXMLGidType value) {
        this.timeZoneGid = value;
    }

    /**
     * Obtém o valor da propriedade cellPhone.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCellPhone() {
        return cellPhone;
    }

    /**
     * Define o valor da propriedade cellPhone.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCellPhone(String value) {
        this.cellPhone = value;
    }

    /**
     * Obtém o valor da propriedade useMessageHub.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUseMessageHub() {
        return useMessageHub;
    }

    /**
     * Define o valor da propriedade useMessageHub.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUseMessageHub(String value) {
        this.useMessageHub = value;
    }

    /**
     * Obtém o valor da propriedade messageProfileGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getMessageProfileGid() {
        return messageProfileGid;
    }

    /**
     * Define o valor da propriedade messageProfileGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setMessageProfileGid(GLogXMLGidType value) {
        this.messageProfileGid = value;
    }

    /**
     * Obtém o valor da propriedade printerGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getPrinterGid() {
        return printerGid;
    }

    /**
     * Define o valor da propriedade printerGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setPrinterGid(GLogXMLGidType value) {
        this.printerGid = value;
    }

    /**
     * Obtém o valor da propriedade flexFieldStrings.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldStringType }
     *     
     */
    public FlexFieldStringType getFlexFieldStrings() {
        return flexFieldStrings;
    }

    /**
     * Define o valor da propriedade flexFieldStrings.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldStringType }
     *     
     */
    public void setFlexFieldStrings(FlexFieldStringType value) {
        this.flexFieldStrings = value;
    }

    /**
     * Obtém o valor da propriedade flexFieldNumbers.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldNumberType }
     *     
     */
    public FlexFieldNumberType getFlexFieldNumbers() {
        return flexFieldNumbers;
    }

    /**
     * Define o valor da propriedade flexFieldNumbers.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldNumberType }
     *     
     */
    public void setFlexFieldNumbers(FlexFieldNumberType value) {
        this.flexFieldNumbers = value;
    }

    /**
     * Obtém o valor da propriedade flexFieldDates.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldDateType }
     *     
     */
    public FlexFieldDateType getFlexFieldDates() {
        return flexFieldDates;
    }

    /**
     * Define o valor da propriedade flexFieldDates.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldDateType }
     *     
     */
    public void setFlexFieldDates(FlexFieldDateType value) {
        this.flexFieldDates = value;
    }

    /**
     * Obtém o valor da propriedade indicator.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIndicator() {
        return indicator;
    }

    /**
     * Define o valor da propriedade indicator.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIndicator(String value) {
        this.indicator = value;
    }


    /**
     * <p>Classe Java de anonymous complex type.
     * 
     * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="ConsolidationProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
     *         &lt;element name="TransactionCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransactionCodeType"/>
     *         &lt;element name="TimeWindowGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
     *         &lt;element name="StylesheetProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
     *         &lt;element name="BatchSize" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "consolidationProfileGid",
        "transactionCode",
        "timeWindowGid",
        "stylesheetProfileGid",
        "batchSize"
    })
    public static class ConsolidationProfile {

        @XmlElement(name = "ConsolidationProfileGid", required = true)
        protected GLogXMLGidType consolidationProfileGid;
        @XmlElement(name = "TransactionCode", required = true)
        @XmlSchemaType(name = "string")
        protected TransactionCodeType transactionCode;
        @XmlElement(name = "TimeWindowGid", required = true)
        protected GLogXMLGidType timeWindowGid;
        @XmlElement(name = "StylesheetProfileGid")
        protected GLogXMLGidType stylesheetProfileGid;
        @XmlElement(name = "BatchSize")
        protected String batchSize;

        /**
         * Obtém o valor da propriedade consolidationProfileGid.
         * 
         * @return
         *     possible object is
         *     {@link GLogXMLGidType }
         *     
         */
        public GLogXMLGidType getConsolidationProfileGid() {
            return consolidationProfileGid;
        }

        /**
         * Define o valor da propriedade consolidationProfileGid.
         * 
         * @param value
         *     allowed object is
         *     {@link GLogXMLGidType }
         *     
         */
        public void setConsolidationProfileGid(GLogXMLGidType value) {
            this.consolidationProfileGid = value;
        }

        /**
         * Obtém o valor da propriedade transactionCode.
         * 
         * @return
         *     possible object is
         *     {@link TransactionCodeType }
         *     
         */
        public TransactionCodeType getTransactionCode() {
            return transactionCode;
        }

        /**
         * Define o valor da propriedade transactionCode.
         * 
         * @param value
         *     allowed object is
         *     {@link TransactionCodeType }
         *     
         */
        public void setTransactionCode(TransactionCodeType value) {
            this.transactionCode = value;
        }

        /**
         * Obtém o valor da propriedade timeWindowGid.
         * 
         * @return
         *     possible object is
         *     {@link GLogXMLGidType }
         *     
         */
        public GLogXMLGidType getTimeWindowGid() {
            return timeWindowGid;
        }

        /**
         * Define o valor da propriedade timeWindowGid.
         * 
         * @param value
         *     allowed object is
         *     {@link GLogXMLGidType }
         *     
         */
        public void setTimeWindowGid(GLogXMLGidType value) {
            this.timeWindowGid = value;
        }

        /**
         * Obtém o valor da propriedade stylesheetProfileGid.
         * 
         * @return
         *     possible object is
         *     {@link GLogXMLGidType }
         *     
         */
        public GLogXMLGidType getStylesheetProfileGid() {
            return stylesheetProfileGid;
        }

        /**
         * Define o valor da propriedade stylesheetProfileGid.
         * 
         * @param value
         *     allowed object is
         *     {@link GLogXMLGidType }
         *     
         */
        public void setStylesheetProfileGid(GLogXMLGidType value) {
            this.stylesheetProfileGid = value;
        }

        /**
         * Obtém o valor da propriedade batchSize.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBatchSize() {
            return batchSize;
        }

        /**
         * Define o valor da propriedade batchSize.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBatchSize(String value) {
            this.batchSize = value;
        }

    }


    /**
     * <p>Classe Java de anonymous complex type.
     * 
     * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="CorporationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "corporationGid"
    })
    public static class ContactCorporation {

        @XmlElement(name = "CorporationGid", required = true)
        protected GLogXMLGidType corporationGid;

        /**
         * Obtém o valor da propriedade corporationGid.
         * 
         * @return
         *     possible object is
         *     {@link GLogXMLGidType }
         *     
         */
        public GLogXMLGidType getCorporationGid() {
            return corporationGid;
        }

        /**
         * Define o valor da propriedade corporationGid.
         * 
         * @param value
         *     allowed object is
         *     {@link GLogXMLGidType }
         *     
         */
        public void setCorporationGid(GLogXMLGidType value) {
            this.corporationGid = value;
        }

    }


    /**
     * <p>Classe Java de anonymous complex type.
     * 
     * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="ExternalSystemGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
     *         &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="URL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="IntQueueName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="UserName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="Password" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}PasswordType" minOccurs="0"/>
     *         &lt;element name="UseGlcredential" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="MaxBytesPerTransmission" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="MaxTransactsPerTransmission" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "externalSystemGid",
        "description",
        "url",
        "intQueueName",
        "userName",
        "password",
        "useGlcredential",
        "maxBytesPerTransmission",
        "maxTransactsPerTransmission"
    })
    public static class ExternalSystem {

        @XmlElement(name = "ExternalSystemGid", required = true)
        protected GLogXMLGidType externalSystemGid;
        @XmlElement(name = "Description")
        protected String description;
        @XmlElement(name = "URL")
        protected String url;
        @XmlElement(name = "IntQueueName")
        protected String intQueueName;
        @XmlElement(name = "UserName")
        protected String userName;
        @XmlElement(name = "Password")
        protected PasswordType password;
        @XmlElement(name = "UseGlcredential", required = true)
        protected String useGlcredential;
        @XmlElement(name = "MaxBytesPerTransmission")
        protected String maxBytesPerTransmission;
        @XmlElement(name = "MaxTransactsPerTransmission")
        protected String maxTransactsPerTransmission;

        /**
         * Obtém o valor da propriedade externalSystemGid.
         * 
         * @return
         *     possible object is
         *     {@link GLogXMLGidType }
         *     
         */
        public GLogXMLGidType getExternalSystemGid() {
            return externalSystemGid;
        }

        /**
         * Define o valor da propriedade externalSystemGid.
         * 
         * @param value
         *     allowed object is
         *     {@link GLogXMLGidType }
         *     
         */
        public void setExternalSystemGid(GLogXMLGidType value) {
            this.externalSystemGid = value;
        }

        /**
         * Obtém o valor da propriedade description.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDescription() {
            return description;
        }

        /**
         * Define o valor da propriedade description.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDescription(String value) {
            this.description = value;
        }

        /**
         * Obtém o valor da propriedade url.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getURL() {
            return url;
        }

        /**
         * Define o valor da propriedade url.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setURL(String value) {
            this.url = value;
        }

        /**
         * Obtém o valor da propriedade intQueueName.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getIntQueueName() {
            return intQueueName;
        }

        /**
         * Define o valor da propriedade intQueueName.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setIntQueueName(String value) {
            this.intQueueName = value;
        }

        /**
         * Obtém o valor da propriedade userName.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getUserName() {
            return userName;
        }

        /**
         * Define o valor da propriedade userName.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setUserName(String value) {
            this.userName = value;
        }

        /**
         * Obtém o valor da propriedade password.
         * 
         * @return
         *     possible object is
         *     {@link PasswordType }
         *     
         */
        public PasswordType getPassword() {
            return password;
        }

        /**
         * Define o valor da propriedade password.
         * 
         * @param value
         *     allowed object is
         *     {@link PasswordType }
         *     
         */
        public void setPassword(PasswordType value) {
            this.password = value;
        }

        /**
         * Obtém o valor da propriedade useGlcredential.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getUseGlcredential() {
            return useGlcredential;
        }

        /**
         * Define o valor da propriedade useGlcredential.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setUseGlcredential(String value) {
            this.useGlcredential = value;
        }

        /**
         * Obtém o valor da propriedade maxBytesPerTransmission.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMaxBytesPerTransmission() {
            return maxBytesPerTransmission;
        }

        /**
         * Define o valor da propriedade maxBytesPerTransmission.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMaxBytesPerTransmission(String value) {
            this.maxBytesPerTransmission = value;
        }

        /**
         * Obtém o valor da propriedade maxTransactsPerTransmission.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMaxTransactsPerTransmission() {
            return maxTransactsPerTransmission;
        }

        /**
         * Define o valor da propriedade maxTransactsPerTransmission.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMaxTransactsPerTransmission(String value) {
            this.maxTransactsPerTransmission = value;
        }

    }

}
