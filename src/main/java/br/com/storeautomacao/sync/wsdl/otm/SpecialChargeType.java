
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * SpecialCharge is an element of FreightRate used to convey special charges.
 * 
 * <p>Classe Java de SpecialChargeType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="SpecialChargeType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SpecialChargeCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SpecialChargeDesc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SpecialChargeType", propOrder = {
    "specialChargeCode",
    "specialChargeDesc"
})
public class SpecialChargeType {

    @XmlElement(name = "SpecialChargeCode", required = true)
    protected String specialChargeCode;
    @XmlElement(name = "SpecialChargeDesc")
    protected String specialChargeDesc;

    /**
     * Obtém o valor da propriedade specialChargeCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSpecialChargeCode() {
        return specialChargeCode;
    }

    /**
     * Define o valor da propriedade specialChargeCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSpecialChargeCode(String value) {
        this.specialChargeCode = value;
    }

    /**
     * Obtém o valor da propriedade specialChargeDesc.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSpecialChargeDesc() {
        return specialChargeDesc;
    }

    /**
     * Define o valor da propriedade specialChargeDesc.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSpecialChargeDesc(String value) {
        this.specialChargeDesc = value;
    }

}
