
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * LineItemRefNum is an element of RailLineItem used to identify the line item.
 * 
 * <p>Classe Java de LineItemRefNumType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="LineItemRefNumType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="LineItemRefNumValue" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="LineItemRefNumQualifierGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LineItemRefNumType", propOrder = {
    "lineItemRefNumValue",
    "lineItemRefNumQualifierGid"
})
public class LineItemRefNumType {

    @XmlElement(name = "LineItemRefNumValue", required = true)
    protected String lineItemRefNumValue;
    @XmlElement(name = "LineItemRefNumQualifierGid", required = true)
    protected GLogXMLGidType lineItemRefNumQualifierGid;

    /**
     * Obtém o valor da propriedade lineItemRefNumValue.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLineItemRefNumValue() {
        return lineItemRefNumValue;
    }

    /**
     * Define o valor da propriedade lineItemRefNumValue.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLineItemRefNumValue(String value) {
        this.lineItemRefNumValue = value;
    }

    /**
     * Obtém o valor da propriedade lineItemRefNumQualifierGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getLineItemRefNumQualifierGid() {
        return lineItemRefNumQualifierGid;
    }

    /**
     * Define o valor da propriedade lineItemRefNumQualifierGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setLineItemRefNumQualifierGid(GLogXMLGidType value) {
        this.lineItemRefNumQualifierGid = value;
    }

}
