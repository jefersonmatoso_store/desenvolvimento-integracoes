
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.*;


/**
 * <p>Classe Java de GtmInvolvedPartyType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="GtmInvolvedPartyType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TransactionCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransactionCodeType" minOccurs="0"/>
 *         &lt;element name="InvolvedPartyQualifierGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;choice>
 *           &lt;element name="GtmContactGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *           &lt;element name="GtmContact" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}GtmContactType" minOccurs="0"/>
 *         &lt;/choice>
 *         &lt;element name="ComMethodGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="LocationOverrideInfo" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LocationOverrideInfoType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GtmInvolvedPartyType", namespace = "http://xmlns.oracle.com/apps/gtm/transmission/v6.4", propOrder = {
    "transactionCode",
    "involvedPartyQualifierGid",
    "gtmContactGid",
    "gtmContact",
    "comMethodGid",
    "locationOverrideInfo"
})
public class GtmInvolvedPartyType {

    @XmlElement(name = "TransactionCode")
    @XmlSchemaType(name = "string")
    protected TransactionCodeType transactionCode;
    @XmlElement(name = "InvolvedPartyQualifierGid", required = true)
    protected GLogXMLGidType involvedPartyQualifierGid;
    @XmlElement(name = "GtmContactGid")
    protected GLogXMLGidType gtmContactGid;
    @XmlElement(name = "GtmContact")
    protected GtmContactType gtmContact;
    @XmlElement(name = "ComMethodGid")
    protected GLogXMLGidType comMethodGid;
    @XmlElement(name = "LocationOverrideInfo")
    protected LocationOverrideInfoType locationOverrideInfo;

    /**
     * Obtém o valor da propriedade transactionCode.
     * 
     * @return
     *     possible object is
     *     {@link TransactionCodeType }
     *     
     */
    public TransactionCodeType getTransactionCode() {
        return transactionCode;
    }

    /**
     * Define o valor da propriedade transactionCode.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionCodeType }
     *     
     */
    public void setTransactionCode(TransactionCodeType value) {
        this.transactionCode = value;
    }

    /**
     * Obtém o valor da propriedade involvedPartyQualifierGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getInvolvedPartyQualifierGid() {
        return involvedPartyQualifierGid;
    }

    /**
     * Define o valor da propriedade involvedPartyQualifierGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setInvolvedPartyQualifierGid(GLogXMLGidType value) {
        this.involvedPartyQualifierGid = value;
    }

    /**
     * Obtém o valor da propriedade gtmContactGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getGtmContactGid() {
        return gtmContactGid;
    }

    /**
     * Define o valor da propriedade gtmContactGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setGtmContactGid(GLogXMLGidType value) {
        this.gtmContactGid = value;
    }

    /**
     * Obtém o valor da propriedade gtmContact.
     * 
     * @return
     *     possible object is
     *     {@link GtmContactType }
     *     
     */
    public GtmContactType getGtmContact() {
        return gtmContact;
    }

    /**
     * Define o valor da propriedade gtmContact.
     * 
     * @param value
     *     allowed object is
     *     {@link GtmContactType }
     *     
     */
    public void setGtmContact(GtmContactType value) {
        this.gtmContact = value;
    }

    /**
     * Obtém o valor da propriedade comMethodGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getComMethodGid() {
        return comMethodGid;
    }

    /**
     * Define o valor da propriedade comMethodGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setComMethodGid(GLogXMLGidType value) {
        this.comMethodGid = value;
    }

    /**
     * Obtém o valor da propriedade locationOverrideInfo.
     * 
     * @return
     *     possible object is
     *     {@link LocationOverrideInfoType }
     *     
     */
    public LocationOverrideInfoType getLocationOverrideInfo() {
        return locationOverrideInfo;
    }

    /**
     * Define o valor da propriedade locationOverrideInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link LocationOverrideInfoType }
     *     
     */
    public void setLocationOverrideInfo(LocationOverrideInfoType value) {
        this.locationOverrideInfo = value;
    }

}
