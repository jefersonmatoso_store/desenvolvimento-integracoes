
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Specifies the Import License information.
 * 
 * <p>Classe Java de ImportLicenseInfoType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="ImportLicenseInfoType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="LicenseNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LicenseRequired" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IssueDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="ExpireDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="ReceivedDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ImportLicenseInfoType", propOrder = {
    "licenseNum",
    "licenseRequired",
    "issueDt",
    "expireDt",
    "receivedDt"
})
public class ImportLicenseInfoType {

    @XmlElement(name = "LicenseNum")
    protected String licenseNum;
    @XmlElement(name = "LicenseRequired")
    protected String licenseRequired;
    @XmlElement(name = "IssueDt")
    protected GLogDateTimeType issueDt;
    @XmlElement(name = "ExpireDt")
    protected GLogDateTimeType expireDt;
    @XmlElement(name = "ReceivedDt")
    protected GLogDateTimeType receivedDt;

    /**
     * Obtém o valor da propriedade licenseNum.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLicenseNum() {
        return licenseNum;
    }

    /**
     * Define o valor da propriedade licenseNum.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLicenseNum(String value) {
        this.licenseNum = value;
    }

    /**
     * Obtém o valor da propriedade licenseRequired.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLicenseRequired() {
        return licenseRequired;
    }

    /**
     * Define o valor da propriedade licenseRequired.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLicenseRequired(String value) {
        this.licenseRequired = value;
    }

    /**
     * Obtém o valor da propriedade issueDt.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getIssueDt() {
        return issueDt;
    }

    /**
     * Define o valor da propriedade issueDt.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setIssueDt(GLogDateTimeType value) {
        this.issueDt = value;
    }

    /**
     * Obtém o valor da propriedade expireDt.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getExpireDt() {
        return expireDt;
    }

    /**
     * Define o valor da propriedade expireDt.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setExpireDt(GLogDateTimeType value) {
        this.expireDt = value;
    }

    /**
     * Obtém o valor da propriedade receivedDt.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getReceivedDt() {
        return receivedDt;
    }

    /**
     * Define o valor da propriedade receivedDt.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setReceivedDt(GLogDateTimeType value) {
        this.receivedDt = value;
    }

}
