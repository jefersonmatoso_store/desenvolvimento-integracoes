
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * (Both) Registrations related to another Registration.
 *          
 * 
 * <p>Classe Java de GtmRegRelatedRegistrationType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="GtmRegRelatedRegistrationType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GtmRegistrationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="GtmRelatedRegistrationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GtmRegRelatedRegistrationType", namespace = "http://xmlns.oracle.com/apps/gtm/transmission/v6.4", propOrder = {
    "gtmRegistrationGid",
    "gtmRelatedRegistrationGid"
})
public class GtmRegRelatedRegistrationType {

    @XmlElement(name = "GtmRegistrationGid", required = true)
    protected GLogXMLGidType gtmRegistrationGid;
    @XmlElement(name = "GtmRelatedRegistrationGid", required = true)
    protected GLogXMLGidType gtmRelatedRegistrationGid;

    /**
     * Obtém o valor da propriedade gtmRegistrationGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getGtmRegistrationGid() {
        return gtmRegistrationGid;
    }

    /**
     * Define o valor da propriedade gtmRegistrationGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setGtmRegistrationGid(GLogXMLGidType value) {
        this.gtmRegistrationGid = value;
    }

    /**
     * Obtém o valor da propriedade gtmRelatedRegistrationGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getGtmRelatedRegistrationGid() {
        return gtmRelatedRegistrationGid;
    }

    /**
     * Define o valor da propriedade gtmRelatedRegistrationGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setGtmRelatedRegistrationGid(GLogXMLGidType value) {
        this.gtmRelatedRegistrationGid = value;
    }

}
