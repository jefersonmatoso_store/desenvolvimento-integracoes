
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * 
 *             SStatusShipUnit is a structure for specifying received ship unit quantities.
 *          
 * 
 * <p>Classe Java de SStatusShipUnitType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="SStatusShipUnitType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;choice>
 *           &lt;element name="IntSavedQuery" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}IntSavedQueryType"/>
 *           &lt;element name="ShipUnitGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;/choice>
 *         &lt;element name="ReceivedWeightVolume" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}WeightVolumeType" minOccurs="0"/>
 *         &lt;element name="ReceivedShipUnitCount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SStatusShipUnitContent" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}SStatusShipUnitContentType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SStatusShipUnitType", propOrder = {
    "intSavedQuery",
    "shipUnitGid",
    "receivedWeightVolume",
    "receivedShipUnitCount",
    "sStatusShipUnitContent"
})
public class SStatusShipUnitType {

    @XmlElement(name = "IntSavedQuery")
    protected IntSavedQueryType intSavedQuery;
    @XmlElement(name = "ShipUnitGid")
    protected GLogXMLGidType shipUnitGid;
    @XmlElement(name = "ReceivedWeightVolume")
    protected WeightVolumeType receivedWeightVolume;
    @XmlElement(name = "ReceivedShipUnitCount")
    protected String receivedShipUnitCount;
    @XmlElement(name = "SStatusShipUnitContent")
    protected List<SStatusShipUnitContentType> sStatusShipUnitContent;

    /**
     * Obtém o valor da propriedade intSavedQuery.
     * 
     * @return
     *     possible object is
     *     {@link IntSavedQueryType }
     *     
     */
    public IntSavedQueryType getIntSavedQuery() {
        return intSavedQuery;
    }

    /**
     * Define o valor da propriedade intSavedQuery.
     * 
     * @param value
     *     allowed object is
     *     {@link IntSavedQueryType }
     *     
     */
    public void setIntSavedQuery(IntSavedQueryType value) {
        this.intSavedQuery = value;
    }

    /**
     * Obtém o valor da propriedade shipUnitGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getShipUnitGid() {
        return shipUnitGid;
    }

    /**
     * Define o valor da propriedade shipUnitGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setShipUnitGid(GLogXMLGidType value) {
        this.shipUnitGid = value;
    }

    /**
     * Obtém o valor da propriedade receivedWeightVolume.
     * 
     * @return
     *     possible object is
     *     {@link WeightVolumeType }
     *     
     */
    public WeightVolumeType getReceivedWeightVolume() {
        return receivedWeightVolume;
    }

    /**
     * Define o valor da propriedade receivedWeightVolume.
     * 
     * @param value
     *     allowed object is
     *     {@link WeightVolumeType }
     *     
     */
    public void setReceivedWeightVolume(WeightVolumeType value) {
        this.receivedWeightVolume = value;
    }

    /**
     * Obtém o valor da propriedade receivedShipUnitCount.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReceivedShipUnitCount() {
        return receivedShipUnitCount;
    }

    /**
     * Define o valor da propriedade receivedShipUnitCount.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReceivedShipUnitCount(String value) {
        this.receivedShipUnitCount = value;
    }

    /**
     * Gets the value of the sStatusShipUnitContent property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the sStatusShipUnitContent property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSStatusShipUnitContent().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SStatusShipUnitContentType }
     * 
     * 
     */
    public List<SStatusShipUnitContentType> getSStatusShipUnitContent() {
        if (sStatusShipUnitContent == null) {
            sStatusShipUnitContent = new ArrayList<SStatusShipUnitContentType>();
        }
        return this.sStatusShipUnitContent;
    }

}
