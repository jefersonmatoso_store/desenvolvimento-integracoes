
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;


/**
 * Defines intermediate stops on an order
 * 
 * <p>Classe Java de OrStopType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="OrStopType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="OrStopGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="TransactionCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransactionCodeType" minOccurs="0"/>
 *         &lt;element name="OrStopSeq" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;choice>
 *           &lt;element name="Location" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LocationType"/>
 *           &lt;element name="LocationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;/choice>
 *         &lt;element name="EarlyArrivalTime" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="LateArrivalTime" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="IsAppointment" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="IsAppointmentRequired" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="IsApptConfirmRequired" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="IsApptConfirm" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="LegPosition" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Remark" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RemarkType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="OrStopSpecialService" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OrStopSpecialServiceType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OrStopType", propOrder = {
    "orStopGid",
    "transactionCode",
    "orStopSeq",
    "location",
    "locationGid",
    "earlyArrivalTime",
    "lateArrivalTime",
    "isAppointment",
    "isAppointmentRequired",
    "isApptConfirmRequired",
    "isApptConfirm",
    "legPosition",
    "remark",
    "orStopSpecialService"
})
public class OrStopType {

    @XmlElement(name = "OrStopGid", required = true)
    protected GLogXMLGidType orStopGid;
    @XmlElement(name = "TransactionCode")
    @XmlSchemaType(name = "string")
    protected TransactionCodeType transactionCode;
    @XmlElement(name = "OrStopSeq", required = true)
    protected String orStopSeq;
    @XmlElement(name = "Location")
    protected LocationType location;
    @XmlElement(name = "LocationGid")
    protected GLogXMLGidType locationGid;
    @XmlElement(name = "EarlyArrivalTime")
    protected GLogDateTimeType earlyArrivalTime;
    @XmlElement(name = "LateArrivalTime")
    protected GLogDateTimeType lateArrivalTime;
    @XmlElement(name = "IsAppointment", required = true)
    protected String isAppointment;
    @XmlElement(name = "IsAppointmentRequired", required = true)
    protected String isAppointmentRequired;
    @XmlElement(name = "IsApptConfirmRequired", required = true)
    protected String isApptConfirmRequired;
    @XmlElement(name = "IsApptConfirm", required = true)
    protected String isApptConfirm;
    @XmlElement(name = "LegPosition")
    protected String legPosition;
    @XmlElement(name = "Remark")
    protected List<RemarkType> remark;
    @XmlElement(name = "OrStopSpecialService")
    protected List<OrStopSpecialServiceType> orStopSpecialService;

    /**
     * Obtém o valor da propriedade orStopGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getOrStopGid() {
        return orStopGid;
    }

    /**
     * Define o valor da propriedade orStopGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setOrStopGid(GLogXMLGidType value) {
        this.orStopGid = value;
    }

    /**
     * Obtém o valor da propriedade transactionCode.
     * 
     * @return
     *     possible object is
     *     {@link TransactionCodeType }
     *     
     */
    public TransactionCodeType getTransactionCode() {
        return transactionCode;
    }

    /**
     * Define o valor da propriedade transactionCode.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionCodeType }
     *     
     */
    public void setTransactionCode(TransactionCodeType value) {
        this.transactionCode = value;
    }

    /**
     * Obtém o valor da propriedade orStopSeq.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrStopSeq() {
        return orStopSeq;
    }

    /**
     * Define o valor da propriedade orStopSeq.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrStopSeq(String value) {
        this.orStopSeq = value;
    }

    /**
     * Obtém o valor da propriedade location.
     * 
     * @return
     *     possible object is
     *     {@link LocationType }
     *     
     */
    public LocationType getLocation() {
        return location;
    }

    /**
     * Define o valor da propriedade location.
     * 
     * @param value
     *     allowed object is
     *     {@link LocationType }
     *     
     */
    public void setLocation(LocationType value) {
        this.location = value;
    }

    /**
     * Obtém o valor da propriedade locationGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getLocationGid() {
        return locationGid;
    }

    /**
     * Define o valor da propriedade locationGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setLocationGid(GLogXMLGidType value) {
        this.locationGid = value;
    }

    /**
     * Obtém o valor da propriedade earlyArrivalTime.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getEarlyArrivalTime() {
        return earlyArrivalTime;
    }

    /**
     * Define o valor da propriedade earlyArrivalTime.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setEarlyArrivalTime(GLogDateTimeType value) {
        this.earlyArrivalTime = value;
    }

    /**
     * Obtém o valor da propriedade lateArrivalTime.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getLateArrivalTime() {
        return lateArrivalTime;
    }

    /**
     * Define o valor da propriedade lateArrivalTime.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setLateArrivalTime(GLogDateTimeType value) {
        this.lateArrivalTime = value;
    }

    /**
     * Obtém o valor da propriedade isAppointment.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsAppointment() {
        return isAppointment;
    }

    /**
     * Define o valor da propriedade isAppointment.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsAppointment(String value) {
        this.isAppointment = value;
    }

    /**
     * Obtém o valor da propriedade isAppointmentRequired.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsAppointmentRequired() {
        return isAppointmentRequired;
    }

    /**
     * Define o valor da propriedade isAppointmentRequired.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsAppointmentRequired(String value) {
        this.isAppointmentRequired = value;
    }

    /**
     * Obtém o valor da propriedade isApptConfirmRequired.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsApptConfirmRequired() {
        return isApptConfirmRequired;
    }

    /**
     * Define o valor da propriedade isApptConfirmRequired.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsApptConfirmRequired(String value) {
        this.isApptConfirmRequired = value;
    }

    /**
     * Obtém o valor da propriedade isApptConfirm.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsApptConfirm() {
        return isApptConfirm;
    }

    /**
     * Define o valor da propriedade isApptConfirm.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsApptConfirm(String value) {
        this.isApptConfirm = value;
    }

    /**
     * Obtém o valor da propriedade legPosition.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLegPosition() {
        return legPosition;
    }

    /**
     * Define o valor da propriedade legPosition.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLegPosition(String value) {
        this.legPosition = value;
    }

    /**
     * Gets the value of the remark property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the remark property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRemark().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RemarkType }
     * 
     * 
     */
    public List<RemarkType> getRemark() {
        if (remark == null) {
            remark = new ArrayList<RemarkType>();
        }
        return this.remark;
    }

    /**
     * Gets the value of the orStopSpecialService property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the orStopSpecialService property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOrStopSpecialService().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OrStopSpecialServiceType }
     * 
     * 
     */
    public List<OrStopSpecialServiceType> getOrStopSpecialService() {
        if (orStopSpecialService == null) {
            orStopSpecialService = new ArrayList<OrStopSpecialServiceType>();
        }
        return this.orStopSpecialService;
    }

}
