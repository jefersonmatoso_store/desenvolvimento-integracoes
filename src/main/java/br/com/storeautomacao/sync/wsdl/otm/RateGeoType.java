
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Classe Java de RateGeoType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="RateGeoType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OTMTransactionOut">
 *       &lt;sequence>
 *         &lt;element name="SendReason" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}SendReasonType" minOccurs="0"/>
 *         &lt;sequence maxOccurs="unbounded" minOccurs="0">
 *           &lt;element name="RATE_GEO_ROW" maxOccurs="unbounded">
 *             &lt;complexType>
 *               &lt;complexContent>
 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   &lt;sequence>
 *                     &lt;element name="RATE_GEO_GID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                     &lt;element name="RATE_GEO_XID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                     &lt;element name="RATE_OFFERING_GID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                     &lt;element name="X_LANE_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="EQUIPMENT_GROUP_PROFILE_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="RATE_SERVICE_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="MIN_COST" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="MIN_COST_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="MIN_COST_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="TOTAL_STOPS_CONSTRAINT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="PICKUP_STOPS_CONSTRAINT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="DELIVERY_STOPS_CONSTRAINT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="CIRCUITY_ALLOWANCE_PERCENT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="CIRCUITY_DISTANCE_COST" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="CIRCUITY_DISTANCE_COST_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="CIRCUITY_DISTANCE_COST_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="MAX_CIRCUITY_PERCENT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="MAX_CIRCUITY_DISTANCE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="MAX_CIRCUITY_DISTANCE_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="MAX_CIRCUITY_DISTANCE_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="STOPS_INCLUDED_RATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="FLEX_COMMODITY_PROFILE_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="RATE_QUALITY_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="SHIPPER_MIN_VALUE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                     &lt;element name="MIN_STOPS" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="SHORT_LINE_COST" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="SHORT_LINE_COST_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="SHORT_LINE_COST_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="RATE_ZONE_PROFILE_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="LOCATION_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="ROUTE_CODE_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="DIM_RATE_FACTOR_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="EFFECTIVE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="EXPIRATION_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="ALLOW_UNCOSTED_LINE_ITEMS" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                     &lt;element name="SHIPPER_MIN_VALUE_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="MULTI_BASE_GROUPS_RULE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                     &lt;element name="RAIL_INTER_MODAL_PLAN_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="CUSTOMER_RATE_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="COFC_TOFC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="EXPIRE_MARK_ID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="VIA_SRC_LOC_PROF_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="VIA_DEST_LOC_PROF_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="VIA_SRC_LOC_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="VIA_DEST_LOC_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="ROUNDING_TYPE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="ROUNDING_INTERVAL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="ROUNDING_FIELDS_LEVEL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="ROUNDING_APPLICATION" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="BUY_SERVPROV_PROFILE_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="BUY_RATE_GEO_PROFILE_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="PAYMENT_METHOD_CODE_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="IS_MASTER_OVERRIDES_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="HAZARDOUS_RATE_TYPE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="DOMAIN_PROFILE_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="IS_QUOTE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="RO_TIME_PERIOD_DEF_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                     &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                     &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="RATE_GEO_STOPS" minOccurs="0">
 *                       &lt;complexType>
 *                         &lt;complexContent>
 *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                             &lt;sequence maxOccurs="unbounded" minOccurs="0">
 *                               &lt;element name="RATE_GEO_STOPS_ROW" maxOccurs="unbounded">
 *                                 &lt;complexType>
 *                                   &lt;complexContent>
 *                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                       &lt;sequence>
 *                                         &lt;element name="RATE_GEO_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                         &lt;element name="LOW_STOP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                         &lt;element name="HIGH_STOP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                         &lt;element name="PER_STOP_COST" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                         &lt;element name="PER_STOP_COST_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                         &lt;element name="PER_STOP_COST_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                         &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                         &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                         &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                         &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                         &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                       &lt;/sequence>
 *                                       &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                     &lt;/restriction>
 *                                   &lt;/complexContent>
 *                                 &lt;/complexType>
 *                               &lt;/element>
 *                             &lt;/sequence>
 *                           &lt;/restriction>
 *                         &lt;/complexContent>
 *                       &lt;/complexType>
 *                     &lt;/element>
 *                     &lt;element name="RATE_GEO_ACCESSORIAL" minOccurs="0">
 *                       &lt;complexType>
 *                         &lt;complexContent>
 *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                             &lt;sequence>
 *                               &lt;element name="RATE_GEO_ACCESSORIAL_ROW" maxOccurs="unbounded" minOccurs="0">
 *                                 &lt;complexType>
 *                                   &lt;complexContent>
 *                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                       &lt;sequence>
 *                                         &lt;element name="ACCESSORIAL_COST_GID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                         &lt;element name="RATE_GEO_GID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                         &lt;element name="ACCESSORIAL_CODE_GID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                         &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                         &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                         &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                         &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                         &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                         &lt;element name="ACCESSORIAL_COST" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ACCESSORIAL_COST_TYPE" minOccurs="0"/>
 *                                       &lt;/sequence>
 *                                       &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                     &lt;/restriction>
 *                                   &lt;/complexContent>
 *                                 &lt;/complexType>
 *                               &lt;/element>
 *                             &lt;/sequence>
 *                           &lt;/restriction>
 *                         &lt;/complexContent>
 *                       &lt;/complexType>
 *                     &lt;/element>
 *                     &lt;element name="RATE_GEO_COST_GROUP" minOccurs="0">
 *                       &lt;complexType>
 *                         &lt;complexContent>
 *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                             &lt;sequence maxOccurs="unbounded" minOccurs="0">
 *                               &lt;element name="RATE_GEO_COST_GROUP_ROW" maxOccurs="unbounded">
 *                                 &lt;complexType>
 *                                   &lt;complexContent>
 *                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                       &lt;sequence>
 *                                         &lt;element name="RATE_GEO_COST_GROUP_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                         &lt;element name="RATE_GEO_COST_GROUP_XID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                         &lt;element name="RATE_GEO_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                         &lt;element name="RATE_GEO_COST_GROUP_SEQ" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                         &lt;element name="GROUP_NAME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                         &lt;element name="USE_DEFICIT_CALCULATIONS" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                         &lt;element name="MULTI_RATES_RULE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                         &lt;element name="RATE_GROUP_TYPE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                         &lt;element name="ROUNDING_TYPE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                         &lt;element name="ROUNDING_INTERVAL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                         &lt;element name="ROUNDING_FIELDS_LEVEL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                         &lt;element name="ROUNDING_APPLICATION" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                         &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                         &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                         &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                         &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                         &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                         &lt;element name="RATE_GEO_COST" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RATE_GEO_COST_TYPE" minOccurs="0"/>
 *                                       &lt;/sequence>
 *                                       &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                     &lt;/restriction>
 *                                   &lt;/complexContent>
 *                                 &lt;/complexType>
 *                               &lt;/element>
 *                             &lt;/sequence>
 *                           &lt;/restriction>
 *                         &lt;/complexContent>
 *                       &lt;/complexType>
 *                     &lt;/element>
 *                     &lt;element name="RG_SPECIAL_SERVICE_ACCESSORIAL" minOccurs="0">
 *                       &lt;complexType>
 *                         &lt;complexContent>
 *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                             &lt;sequence>
 *                               &lt;element name="RG_SPECIAL_SERVICE_ACCESSORIAL_ROW" maxOccurs="unbounded" minOccurs="0">
 *                                 &lt;complexType>
 *                                   &lt;complexContent>
 *                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                       &lt;sequence>
 *                                         &lt;element name="ACCESSORIAL_COST_GID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                         &lt;element name="RATE_GEO_GID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                         &lt;element name="ACCESSORIAL_CODE_GID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                         &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                         &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                         &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                         &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                         &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                         &lt;element name="ACCESSORIAL_COST" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ACCESSORIAL_COST_TYPE" minOccurs="0"/>
 *                                       &lt;/sequence>
 *                                       &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                     &lt;/restriction>
 *                                   &lt;/complexContent>
 *                                 &lt;/complexType>
 *                               &lt;/element>
 *                             &lt;/sequence>
 *                           &lt;/restriction>
 *                         &lt;/complexContent>
 *                       &lt;/complexType>
 *                     &lt;/element>
 *                     &lt;element ref="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}X_LANE" minOccurs="0"/>
 *                     &lt;element name="ATTRIBUTE1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="ATTRIBUTE2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="ATTRIBUTE3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="ATTRIBUTE4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="ATTRIBUTE5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="ATTRIBUTE6" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="ATTRIBUTE7" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="ATTRIBUTE8" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="ATTRIBUTE9" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="ATTRIBUTE10" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="ATTRIBUTE11" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="ATTRIBUTE12" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="ATTRIBUTE13" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="ATTRIBUTE14" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="ATTRIBUTE15" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="ATTRIBUTE16" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="ATTRIBUTE17" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="ATTRIBUTE18" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="ATTRIBUTE19" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="ATTRIBUTE20" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="ATTRIBUTE_NUMBER1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="ATTRIBUTE_NUMBER2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="ATTRIBUTE_NUMBER3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="ATTRIBUTE_NUMBER4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="ATTRIBUTE_NUMBER5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="ATTRIBUTE_NUMBER6" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="ATTRIBUTE_NUMBER7" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="ATTRIBUTE_NUMBER8" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="ATTRIBUTE_NUMBER9" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="ATTRIBUTE_NUMBER10" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="ATTRIBUTE_DATE1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="ATTRIBUTE_DATE2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="ATTRIBUTE_DATE3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="ATTRIBUTE_DATE4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="ATTRIBUTE_DATE5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="ATTRIBUTE_DATE6" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="ATTRIBUTE_DATE7" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="ATTRIBUTE_DATE8" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="ATTRIBUTE_DATE9" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="ATTRIBUTE_DATE10" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="RATE_GEO_DESC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="RATE_GEO_REMARKS" minOccurs="0">
 *                       &lt;complexType>
 *                         &lt;complexContent>
 *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                             &lt;sequence>
 *                               &lt;element name="RATE_GEO_REMARKS_ROW" maxOccurs="unbounded">
 *                                 &lt;complexType>
 *                                   &lt;complexContent>
 *                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                       &lt;sequence>
 *                                         &lt;element name="TRANSACTION_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                         &lt;element name="REMARK_SEQUENCE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                         &lt;element name="REMARK_QUALIFIER_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                         &lt;element name="REMARK_LEVEL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                         &lt;element name="REMARK_TEXT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                         &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                         &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                         &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                         &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                         &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                       &lt;/sequence>
 *                                     &lt;/restriction>
 *                                   &lt;/complexContent>
 *                                 &lt;/complexType>
 *                               &lt;/element>
 *                             &lt;/sequence>
 *                           &lt;/restriction>
 *                         &lt;/complexContent>
 *                       &lt;/complexType>
 *                     &lt;/element>
 *                   &lt;/sequence>
 *                   &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                 &lt;/restriction>
 *               &lt;/complexContent>
 *             &lt;/complexType>
 *           &lt;/element>
 *         &lt;/sequence>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RateGeoType", propOrder = {
    "sendReason",
    "rategeorow"
})
public class RateGeoType
    extends OTMTransactionOut
{

    @XmlElement(name = "SendReason")
    protected SendReasonType sendReason;
    @XmlElement(name = "RATE_GEO_ROW")
    protected List<RATEGEOROW> rategeorow;

    /**
     * Obtém o valor da propriedade sendReason.
     * 
     * @return
     *     possible object is
     *     {@link SendReasonType }
     *     
     */
    public SendReasonType getSendReason() {
        return sendReason;
    }

    /**
     * Define o valor da propriedade sendReason.
     * 
     * @param value
     *     allowed object is
     *     {@link SendReasonType }
     *     
     */
    public void setSendReason(SendReasonType value) {
        this.sendReason = value;
    }

    /**
     * Gets the value of the rategeorow property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the rategeorow property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRATEGEOROW().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RATEGEOROW }
     * 
     * 
     */
    public List<RATEGEOROW> getRATEGEOROW() {
        if (rategeorow == null) {
            rategeorow = new ArrayList<RATEGEOROW>();
        }
        return this.rategeorow;
    }


    /**
     * <p>Classe Java de anonymous complex type.
     * 
     * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="RATE_GEO_GID" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="RATE_GEO_XID" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="RATE_OFFERING_GID" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="X_LANE_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="EQUIPMENT_GROUP_PROFILE_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="RATE_SERVICE_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="MIN_COST" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="MIN_COST_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="MIN_COST_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="TOTAL_STOPS_CONSTRAINT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="PICKUP_STOPS_CONSTRAINT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="DELIVERY_STOPS_CONSTRAINT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="CIRCUITY_ALLOWANCE_PERCENT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="CIRCUITY_DISTANCE_COST" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="CIRCUITY_DISTANCE_COST_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="CIRCUITY_DISTANCE_COST_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="MAX_CIRCUITY_PERCENT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="MAX_CIRCUITY_DISTANCE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="MAX_CIRCUITY_DISTANCE_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="MAX_CIRCUITY_DISTANCE_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="STOPS_INCLUDED_RATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="FLEX_COMMODITY_PROFILE_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="RATE_QUALITY_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="SHIPPER_MIN_VALUE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="MIN_STOPS" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="SHORT_LINE_COST" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="SHORT_LINE_COST_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="SHORT_LINE_COST_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="RATE_ZONE_PROFILE_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="LOCATION_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ROUTE_CODE_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="DIM_RATE_FACTOR_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="EFFECTIVE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="EXPIRATION_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ALLOW_UNCOSTED_LINE_ITEMS" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="SHIPPER_MIN_VALUE_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="MULTI_BASE_GROUPS_RULE" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="RAIL_INTER_MODAL_PLAN_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="CUSTOMER_RATE_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="COFC_TOFC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="EXPIRE_MARK_ID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="VIA_SRC_LOC_PROF_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="VIA_DEST_LOC_PROF_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="VIA_SRC_LOC_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="VIA_DEST_LOC_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ROUNDING_TYPE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ROUNDING_INTERVAL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ROUNDING_FIELDS_LEVEL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ROUNDING_APPLICATION" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="BUY_SERVPROV_PROFILE_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="BUY_RATE_GEO_PROFILE_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="PAYMENT_METHOD_CODE_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="IS_MASTER_OVERRIDES_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="HAZARDOUS_RATE_TYPE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="DOMAIN_PROFILE_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="IS_QUOTE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="RO_TIME_PERIOD_DEF_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="RATE_GEO_STOPS" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence maxOccurs="unbounded" minOccurs="0">
     *                   &lt;element name="RATE_GEO_STOPS_ROW" maxOccurs="unbounded">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="RATE_GEO_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                             &lt;element name="LOW_STOP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                             &lt;element name="HIGH_STOP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                             &lt;element name="PER_STOP_COST" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                             &lt;element name="PER_STOP_COST_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                             &lt;element name="PER_STOP_COST_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                             &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                             &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                             &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                             &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                             &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                           &lt;/sequence>
     *                           &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="RATE_GEO_ACCESSORIAL" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="RATE_GEO_ACCESSORIAL_ROW" maxOccurs="unbounded" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="ACCESSORIAL_COST_GID" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="RATE_GEO_GID" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="ACCESSORIAL_CODE_GID" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                             &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                             &lt;element name="ACCESSORIAL_COST" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ACCESSORIAL_COST_TYPE" minOccurs="0"/>
     *                           &lt;/sequence>
     *                           &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="RATE_GEO_COST_GROUP" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence maxOccurs="unbounded" minOccurs="0">
     *                   &lt;element name="RATE_GEO_COST_GROUP_ROW" maxOccurs="unbounded">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="RATE_GEO_COST_GROUP_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                             &lt;element name="RATE_GEO_COST_GROUP_XID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                             &lt;element name="RATE_GEO_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                             &lt;element name="RATE_GEO_COST_GROUP_SEQ" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                             &lt;element name="GROUP_NAME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                             &lt;element name="USE_DEFICIT_CALCULATIONS" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                             &lt;element name="MULTI_RATES_RULE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                             &lt;element name="RATE_GROUP_TYPE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                             &lt;element name="ROUNDING_TYPE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                             &lt;element name="ROUNDING_INTERVAL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                             &lt;element name="ROUNDING_FIELDS_LEVEL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                             &lt;element name="ROUNDING_APPLICATION" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                             &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                             &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                             &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                             &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                             &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                             &lt;element name="RATE_GEO_COST" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RATE_GEO_COST_TYPE" minOccurs="0"/>
     *                           &lt;/sequence>
     *                           &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="RG_SPECIAL_SERVICE_ACCESSORIAL" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="RG_SPECIAL_SERVICE_ACCESSORIAL_ROW" maxOccurs="unbounded" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="ACCESSORIAL_COST_GID" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="RATE_GEO_GID" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="ACCESSORIAL_CODE_GID" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                             &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                             &lt;element name="ACCESSORIAL_COST" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ACCESSORIAL_COST_TYPE" minOccurs="0"/>
     *                           &lt;/sequence>
     *                           &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element ref="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}X_LANE" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE6" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE7" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE8" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE9" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE10" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE11" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE12" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE13" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE14" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE15" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE16" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE17" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE18" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE19" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE20" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE_NUMBER1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE_NUMBER2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE_NUMBER3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE_NUMBER4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE_NUMBER5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE_NUMBER6" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE_NUMBER7" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE_NUMBER8" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE_NUMBER9" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE_NUMBER10" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE_DATE1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE_DATE2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE_DATE3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE_DATE4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE_DATE5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE_DATE6" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE_DATE7" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE_DATE8" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE_DATE9" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ATTRIBUTE_DATE10" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="RATE_GEO_DESC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="RATE_GEO_REMARKS" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="RATE_GEO_REMARKS_ROW" maxOccurs="unbounded">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="TRANSACTION_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                             &lt;element name="REMARK_SEQUENCE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                             &lt;element name="REMARK_QUALIFIER_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                             &lt;element name="REMARK_LEVEL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                             &lt;element name="REMARK_TEXT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                             &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                             &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                             &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *       &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" />
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "rategeogid",
        "rategeoxid",
        "rateofferinggid",
        "xlanegid",
        "equipmentgroupprofilegid",
        "rateservicegid",
        "mincost",
        "mincostgid",
        "mincostbase",
        "totalstopsconstraint",
        "pickupstopsconstraint",
        "deliverystopsconstraint",
        "circuityallowancepercent",
        "circuitydistancecost",
        "circuitydistancecostgid",
        "circuitydistancecostbase",
        "maxcircuitypercent",
        "maxcircuitydistance",
        "maxcircuitydistanceuomcode",
        "maxcircuitydistancebase",
        "stopsincludedrate",
        "flexcommodityprofilegid",
        "ratequalitygid",
        "shipperminvalue",
        "domainname",
        "minstops",
        "shortlinecost",
        "shortlinecostgid",
        "shortlinecostbase",
        "ratezoneprofilegid",
        "locationgid",
        "routecodegid",
        "dimratefactorgid",
        "effectivedate",
        "expirationdate",
        "allowuncostedlineitems",
        "shipperminvaluegid",
        "multibasegroupsrule",
        "railintermodalplangid",
        "customerratecode",
        "cofctofc",
        "expiremarkid",
        "viasrclocprofgid",
        "viadestlocprofgid",
        "viasrclocgid",
        "viadestlocgid",
        "roundingtype",
        "roundinginterval",
        "roundingfieldslevel",
        "roundingapplication",
        "buyservprovprofilegid",
        "buyrategeoprofilegid",
        "paymentmethodcodegid",
        "ismasteroverridesbase",
        "hazardousratetype",
        "domainprofilegid",
        "isquote",
        "rotimeperioddefgid",
        "insertuser",
        "insertdate",
        "updateuser",
        "updatedate",
        "rategeostops",
        "rategeoaccessorial",
        "rategeocostgroup",
        "rgspecialserviceaccessorial",
        "xlane",
        "attribute1",
        "attribute2",
        "attribute3",
        "attribute4",
        "attribute5",
        "attribute6",
        "attribute7",
        "attribute8",
        "attribute9",
        "attribute10",
        "attribute11",
        "attribute12",
        "attribute13",
        "attribute14",
        "attribute15",
        "attribute16",
        "attribute17",
        "attribute18",
        "attribute19",
        "attribute20",
        "attributenumber1",
        "attributenumber2",
        "attributenumber3",
        "attributenumber4",
        "attributenumber5",
        "attributenumber6",
        "attributenumber7",
        "attributenumber8",
        "attributenumber9",
        "attributenumber10",
        "attributedate1",
        "attributedate2",
        "attributedate3",
        "attributedate4",
        "attributedate5",
        "attributedate6",
        "attributedate7",
        "attributedate8",
        "attributedate9",
        "attributedate10",
        "rategeodesc",
        "rategeoremarks"
    })
    public static class RATEGEOROW {

        @XmlElement(name = "RATE_GEO_GID", required = true)
        protected String rategeogid;
        @XmlElement(name = "RATE_GEO_XID", required = true)
        protected String rategeoxid;
        @XmlElement(name = "RATE_OFFERING_GID", required = true)
        protected String rateofferinggid;
        @XmlElement(name = "X_LANE_GID")
        protected String xlanegid;
        @XmlElement(name = "EQUIPMENT_GROUP_PROFILE_GID")
        protected String equipmentgroupprofilegid;
        @XmlElement(name = "RATE_SERVICE_GID")
        protected String rateservicegid;
        @XmlElement(name = "MIN_COST")
        protected String mincost;
        @XmlElement(name = "MIN_COST_GID")
        protected String mincostgid;
        @XmlElement(name = "MIN_COST_BASE")
        protected String mincostbase;
        @XmlElement(name = "TOTAL_STOPS_CONSTRAINT")
        protected String totalstopsconstraint;
        @XmlElement(name = "PICKUP_STOPS_CONSTRAINT")
        protected String pickupstopsconstraint;
        @XmlElement(name = "DELIVERY_STOPS_CONSTRAINT")
        protected String deliverystopsconstraint;
        @XmlElement(name = "CIRCUITY_ALLOWANCE_PERCENT")
        protected String circuityallowancepercent;
        @XmlElement(name = "CIRCUITY_DISTANCE_COST")
        protected String circuitydistancecost;
        @XmlElement(name = "CIRCUITY_DISTANCE_COST_GID")
        protected String circuitydistancecostgid;
        @XmlElement(name = "CIRCUITY_DISTANCE_COST_BASE")
        protected String circuitydistancecostbase;
        @XmlElement(name = "MAX_CIRCUITY_PERCENT")
        protected String maxcircuitypercent;
        @XmlElement(name = "MAX_CIRCUITY_DISTANCE")
        protected String maxcircuitydistance;
        @XmlElement(name = "MAX_CIRCUITY_DISTANCE_UOM_CODE")
        protected String maxcircuitydistanceuomcode;
        @XmlElement(name = "MAX_CIRCUITY_DISTANCE_BASE")
        protected String maxcircuitydistancebase;
        @XmlElement(name = "STOPS_INCLUDED_RATE")
        protected String stopsincludedrate;
        @XmlElement(name = "FLEX_COMMODITY_PROFILE_GID")
        protected String flexcommodityprofilegid;
        @XmlElement(name = "RATE_QUALITY_GID")
        protected String ratequalitygid;
        @XmlElement(name = "SHIPPER_MIN_VALUE")
        protected String shipperminvalue;
        @XmlElement(name = "DOMAIN_NAME", required = true)
        protected String domainname;
        @XmlElement(name = "MIN_STOPS")
        protected String minstops;
        @XmlElement(name = "SHORT_LINE_COST")
        protected String shortlinecost;
        @XmlElement(name = "SHORT_LINE_COST_GID")
        protected String shortlinecostgid;
        @XmlElement(name = "SHORT_LINE_COST_BASE")
        protected String shortlinecostbase;
        @XmlElement(name = "RATE_ZONE_PROFILE_GID")
        protected String ratezoneprofilegid;
        @XmlElement(name = "LOCATION_GID")
        protected String locationgid;
        @XmlElement(name = "ROUTE_CODE_GID")
        protected String routecodegid;
        @XmlElement(name = "DIM_RATE_FACTOR_GID")
        protected String dimratefactorgid;
        @XmlElement(name = "EFFECTIVE_DATE")
        protected String effectivedate;
        @XmlElement(name = "EXPIRATION_DATE")
        protected String expirationdate;
        @XmlElement(name = "ALLOW_UNCOSTED_LINE_ITEMS", required = true)
        protected String allowuncostedlineitems;
        @XmlElement(name = "SHIPPER_MIN_VALUE_GID")
        protected String shipperminvaluegid;
        @XmlElement(name = "MULTI_BASE_GROUPS_RULE", required = true)
        protected String multibasegroupsrule;
        @XmlElement(name = "RAIL_INTER_MODAL_PLAN_GID")
        protected String railintermodalplangid;
        @XmlElement(name = "CUSTOMER_RATE_CODE")
        protected String customerratecode;
        @XmlElement(name = "COFC_TOFC")
        protected String cofctofc;
        @XmlElement(name = "EXPIRE_MARK_ID")
        protected String expiremarkid;
        @XmlElement(name = "VIA_SRC_LOC_PROF_GID")
        protected String viasrclocprofgid;
        @XmlElement(name = "VIA_DEST_LOC_PROF_GID")
        protected String viadestlocprofgid;
        @XmlElement(name = "VIA_SRC_LOC_GID")
        protected String viasrclocgid;
        @XmlElement(name = "VIA_DEST_LOC_GID")
        protected String viadestlocgid;
        @XmlElement(name = "ROUNDING_TYPE")
        protected String roundingtype;
        @XmlElement(name = "ROUNDING_INTERVAL")
        protected String roundinginterval;
        @XmlElement(name = "ROUNDING_FIELDS_LEVEL")
        protected String roundingfieldslevel;
        @XmlElement(name = "ROUNDING_APPLICATION")
        protected String roundingapplication;
        @XmlElement(name = "BUY_SERVPROV_PROFILE_GID")
        protected String buyservprovprofilegid;
        @XmlElement(name = "BUY_RATE_GEO_PROFILE_GID")
        protected String buyrategeoprofilegid;
        @XmlElement(name = "PAYMENT_METHOD_CODE_GID")
        protected String paymentmethodcodegid;
        @XmlElement(name = "IS_MASTER_OVERRIDES_BASE")
        protected String ismasteroverridesbase;
        @XmlElement(name = "HAZARDOUS_RATE_TYPE")
        protected String hazardousratetype;
        @XmlElement(name = "DOMAIN_PROFILE_GID")
        protected String domainprofilegid;
        @XmlElement(name = "IS_QUOTE")
        protected String isquote;
        @XmlElement(name = "RO_TIME_PERIOD_DEF_GID")
        protected String rotimeperioddefgid;
        @XmlElement(name = "INSERT_USER", required = true)
        protected String insertuser;
        @XmlElement(name = "INSERT_DATE", required = true)
        protected String insertdate;
        @XmlElement(name = "UPDATE_USER")
        protected String updateuser;
        @XmlElement(name = "UPDATE_DATE")
        protected String updatedate;
        @XmlElement(name = "RATE_GEO_STOPS")
        protected RATEGEOSTOPS rategeostops;
        @XmlElement(name = "RATE_GEO_ACCESSORIAL")
        protected RATEGEOACCESSORIAL rategeoaccessorial;
        @XmlElement(name = "RATE_GEO_COST_GROUP")
        protected RATEGEOCOSTGROUP rategeocostgroup;
        @XmlElement(name = "RG_SPECIAL_SERVICE_ACCESSORIAL")
        protected RGSPECIALSERVICEACCESSORIAL rgspecialserviceaccessorial;
        @XmlElement(name = "X_LANE")
        protected XLANE xlane;
        @XmlElement(name = "ATTRIBUTE1")
        protected String attribute1;
        @XmlElement(name = "ATTRIBUTE2")
        protected String attribute2;
        @XmlElement(name = "ATTRIBUTE3")
        protected String attribute3;
        @XmlElement(name = "ATTRIBUTE4")
        protected String attribute4;
        @XmlElement(name = "ATTRIBUTE5")
        protected String attribute5;
        @XmlElement(name = "ATTRIBUTE6")
        protected String attribute6;
        @XmlElement(name = "ATTRIBUTE7")
        protected String attribute7;
        @XmlElement(name = "ATTRIBUTE8")
        protected String attribute8;
        @XmlElement(name = "ATTRIBUTE9")
        protected String attribute9;
        @XmlElement(name = "ATTRIBUTE10")
        protected String attribute10;
        @XmlElement(name = "ATTRIBUTE11")
        protected String attribute11;
        @XmlElement(name = "ATTRIBUTE12")
        protected String attribute12;
        @XmlElement(name = "ATTRIBUTE13")
        protected String attribute13;
        @XmlElement(name = "ATTRIBUTE14")
        protected String attribute14;
        @XmlElement(name = "ATTRIBUTE15")
        protected String attribute15;
        @XmlElement(name = "ATTRIBUTE16")
        protected String attribute16;
        @XmlElement(name = "ATTRIBUTE17")
        protected String attribute17;
        @XmlElement(name = "ATTRIBUTE18")
        protected String attribute18;
        @XmlElement(name = "ATTRIBUTE19")
        protected String attribute19;
        @XmlElement(name = "ATTRIBUTE20")
        protected String attribute20;
        @XmlElement(name = "ATTRIBUTE_NUMBER1")
        protected String attributenumber1;
        @XmlElement(name = "ATTRIBUTE_NUMBER2")
        protected String attributenumber2;
        @XmlElement(name = "ATTRIBUTE_NUMBER3")
        protected String attributenumber3;
        @XmlElement(name = "ATTRIBUTE_NUMBER4")
        protected String attributenumber4;
        @XmlElement(name = "ATTRIBUTE_NUMBER5")
        protected String attributenumber5;
        @XmlElement(name = "ATTRIBUTE_NUMBER6")
        protected String attributenumber6;
        @XmlElement(name = "ATTRIBUTE_NUMBER7")
        protected String attributenumber7;
        @XmlElement(name = "ATTRIBUTE_NUMBER8")
        protected String attributenumber8;
        @XmlElement(name = "ATTRIBUTE_NUMBER9")
        protected String attributenumber9;
        @XmlElement(name = "ATTRIBUTE_NUMBER10")
        protected String attributenumber10;
        @XmlElement(name = "ATTRIBUTE_DATE1")
        protected String attributedate1;
        @XmlElement(name = "ATTRIBUTE_DATE2")
        protected String attributedate2;
        @XmlElement(name = "ATTRIBUTE_DATE3")
        protected String attributedate3;
        @XmlElement(name = "ATTRIBUTE_DATE4")
        protected String attributedate4;
        @XmlElement(name = "ATTRIBUTE_DATE5")
        protected String attributedate5;
        @XmlElement(name = "ATTRIBUTE_DATE6")
        protected String attributedate6;
        @XmlElement(name = "ATTRIBUTE_DATE7")
        protected String attributedate7;
        @XmlElement(name = "ATTRIBUTE_DATE8")
        protected String attributedate8;
        @XmlElement(name = "ATTRIBUTE_DATE9")
        protected String attributedate9;
        @XmlElement(name = "ATTRIBUTE_DATE10")
        protected String attributedate10;
        @XmlElement(name = "RATE_GEO_DESC")
        protected String rategeodesc;
        @XmlElement(name = "RATE_GEO_REMARKS")
        protected RATEGEOREMARKS rategeoremarks;
        @XmlAttribute(name = "num")
        protected String num;

        /**
         * Obtém o valor da propriedade rategeogid.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRATEGEOGID() {
            return rategeogid;
        }

        /**
         * Define o valor da propriedade rategeogid.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRATEGEOGID(String value) {
            this.rategeogid = value;
        }

        /**
         * Obtém o valor da propriedade rategeoxid.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRATEGEOXID() {
            return rategeoxid;
        }

        /**
         * Define o valor da propriedade rategeoxid.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRATEGEOXID(String value) {
            this.rategeoxid = value;
        }

        /**
         * Obtém o valor da propriedade rateofferinggid.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRATEOFFERINGGID() {
            return rateofferinggid;
        }

        /**
         * Define o valor da propriedade rateofferinggid.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRATEOFFERINGGID(String value) {
            this.rateofferinggid = value;
        }

        /**
         * Obtém o valor da propriedade xlanegid.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getXLANEGID() {
            return xlanegid;
        }

        /**
         * Define o valor da propriedade xlanegid.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setXLANEGID(String value) {
            this.xlanegid = value;
        }

        /**
         * Obtém o valor da propriedade equipmentgroupprofilegid.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEQUIPMENTGROUPPROFILEGID() {
            return equipmentgroupprofilegid;
        }

        /**
         * Define o valor da propriedade equipmentgroupprofilegid.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEQUIPMENTGROUPPROFILEGID(String value) {
            this.equipmentgroupprofilegid = value;
        }

        /**
         * Obtém o valor da propriedade rateservicegid.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRATESERVICEGID() {
            return rateservicegid;
        }

        /**
         * Define o valor da propriedade rateservicegid.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRATESERVICEGID(String value) {
            this.rateservicegid = value;
        }

        /**
         * Obtém o valor da propriedade mincost.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMINCOST() {
            return mincost;
        }

        /**
         * Define o valor da propriedade mincost.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMINCOST(String value) {
            this.mincost = value;
        }

        /**
         * Obtém o valor da propriedade mincostgid.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMINCOSTGID() {
            return mincostgid;
        }

        /**
         * Define o valor da propriedade mincostgid.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMINCOSTGID(String value) {
            this.mincostgid = value;
        }

        /**
         * Obtém o valor da propriedade mincostbase.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMINCOSTBASE() {
            return mincostbase;
        }

        /**
         * Define o valor da propriedade mincostbase.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMINCOSTBASE(String value) {
            this.mincostbase = value;
        }

        /**
         * Obtém o valor da propriedade totalstopsconstraint.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTOTALSTOPSCONSTRAINT() {
            return totalstopsconstraint;
        }

        /**
         * Define o valor da propriedade totalstopsconstraint.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTOTALSTOPSCONSTRAINT(String value) {
            this.totalstopsconstraint = value;
        }

        /**
         * Obtém o valor da propriedade pickupstopsconstraint.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPICKUPSTOPSCONSTRAINT() {
            return pickupstopsconstraint;
        }

        /**
         * Define o valor da propriedade pickupstopsconstraint.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPICKUPSTOPSCONSTRAINT(String value) {
            this.pickupstopsconstraint = value;
        }

        /**
         * Obtém o valor da propriedade deliverystopsconstraint.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDELIVERYSTOPSCONSTRAINT() {
            return deliverystopsconstraint;
        }

        /**
         * Define o valor da propriedade deliverystopsconstraint.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDELIVERYSTOPSCONSTRAINT(String value) {
            this.deliverystopsconstraint = value;
        }

        /**
         * Obtém o valor da propriedade circuityallowancepercent.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCIRCUITYALLOWANCEPERCENT() {
            return circuityallowancepercent;
        }

        /**
         * Define o valor da propriedade circuityallowancepercent.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCIRCUITYALLOWANCEPERCENT(String value) {
            this.circuityallowancepercent = value;
        }

        /**
         * Obtém o valor da propriedade circuitydistancecost.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCIRCUITYDISTANCECOST() {
            return circuitydistancecost;
        }

        /**
         * Define o valor da propriedade circuitydistancecost.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCIRCUITYDISTANCECOST(String value) {
            this.circuitydistancecost = value;
        }

        /**
         * Obtém o valor da propriedade circuitydistancecostgid.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCIRCUITYDISTANCECOSTGID() {
            return circuitydistancecostgid;
        }

        /**
         * Define o valor da propriedade circuitydistancecostgid.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCIRCUITYDISTANCECOSTGID(String value) {
            this.circuitydistancecostgid = value;
        }

        /**
         * Obtém o valor da propriedade circuitydistancecostbase.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCIRCUITYDISTANCECOSTBASE() {
            return circuitydistancecostbase;
        }

        /**
         * Define o valor da propriedade circuitydistancecostbase.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCIRCUITYDISTANCECOSTBASE(String value) {
            this.circuitydistancecostbase = value;
        }

        /**
         * Obtém o valor da propriedade maxcircuitypercent.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMAXCIRCUITYPERCENT() {
            return maxcircuitypercent;
        }

        /**
         * Define o valor da propriedade maxcircuitypercent.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMAXCIRCUITYPERCENT(String value) {
            this.maxcircuitypercent = value;
        }

        /**
         * Obtém o valor da propriedade maxcircuitydistance.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMAXCIRCUITYDISTANCE() {
            return maxcircuitydistance;
        }

        /**
         * Define o valor da propriedade maxcircuitydistance.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMAXCIRCUITYDISTANCE(String value) {
            this.maxcircuitydistance = value;
        }

        /**
         * Obtém o valor da propriedade maxcircuitydistanceuomcode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMAXCIRCUITYDISTANCEUOMCODE() {
            return maxcircuitydistanceuomcode;
        }

        /**
         * Define o valor da propriedade maxcircuitydistanceuomcode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMAXCIRCUITYDISTANCEUOMCODE(String value) {
            this.maxcircuitydistanceuomcode = value;
        }

        /**
         * Obtém o valor da propriedade maxcircuitydistancebase.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMAXCIRCUITYDISTANCEBASE() {
            return maxcircuitydistancebase;
        }

        /**
         * Define o valor da propriedade maxcircuitydistancebase.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMAXCIRCUITYDISTANCEBASE(String value) {
            this.maxcircuitydistancebase = value;
        }

        /**
         * Obtém o valor da propriedade stopsincludedrate.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSTOPSINCLUDEDRATE() {
            return stopsincludedrate;
        }

        /**
         * Define o valor da propriedade stopsincludedrate.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSTOPSINCLUDEDRATE(String value) {
            this.stopsincludedrate = value;
        }

        /**
         * Obtém o valor da propriedade flexcommodityprofilegid.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFLEXCOMMODITYPROFILEGID() {
            return flexcommodityprofilegid;
        }

        /**
         * Define o valor da propriedade flexcommodityprofilegid.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFLEXCOMMODITYPROFILEGID(String value) {
            this.flexcommodityprofilegid = value;
        }

        /**
         * Obtém o valor da propriedade ratequalitygid.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRATEQUALITYGID() {
            return ratequalitygid;
        }

        /**
         * Define o valor da propriedade ratequalitygid.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRATEQUALITYGID(String value) {
            this.ratequalitygid = value;
        }

        /**
         * Obtém o valor da propriedade shipperminvalue.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSHIPPERMINVALUE() {
            return shipperminvalue;
        }

        /**
         * Define o valor da propriedade shipperminvalue.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSHIPPERMINVALUE(String value) {
            this.shipperminvalue = value;
        }

        /**
         * Obtém o valor da propriedade domainname.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDOMAINNAME() {
            return domainname;
        }

        /**
         * Define o valor da propriedade domainname.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDOMAINNAME(String value) {
            this.domainname = value;
        }

        /**
         * Obtém o valor da propriedade minstops.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMINSTOPS() {
            return minstops;
        }

        /**
         * Define o valor da propriedade minstops.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMINSTOPS(String value) {
            this.minstops = value;
        }

        /**
         * Obtém o valor da propriedade shortlinecost.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSHORTLINECOST() {
            return shortlinecost;
        }

        /**
         * Define o valor da propriedade shortlinecost.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSHORTLINECOST(String value) {
            this.shortlinecost = value;
        }

        /**
         * Obtém o valor da propriedade shortlinecostgid.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSHORTLINECOSTGID() {
            return shortlinecostgid;
        }

        /**
         * Define o valor da propriedade shortlinecostgid.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSHORTLINECOSTGID(String value) {
            this.shortlinecostgid = value;
        }

        /**
         * Obtém o valor da propriedade shortlinecostbase.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSHORTLINECOSTBASE() {
            return shortlinecostbase;
        }

        /**
         * Define o valor da propriedade shortlinecostbase.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSHORTLINECOSTBASE(String value) {
            this.shortlinecostbase = value;
        }

        /**
         * Obtém o valor da propriedade ratezoneprofilegid.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRATEZONEPROFILEGID() {
            return ratezoneprofilegid;
        }

        /**
         * Define o valor da propriedade ratezoneprofilegid.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRATEZONEPROFILEGID(String value) {
            this.ratezoneprofilegid = value;
        }

        /**
         * Obtém o valor da propriedade locationgid.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLOCATIONGID() {
            return locationgid;
        }

        /**
         * Define o valor da propriedade locationgid.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLOCATIONGID(String value) {
            this.locationgid = value;
        }

        /**
         * Obtém o valor da propriedade routecodegid.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getROUTECODEGID() {
            return routecodegid;
        }

        /**
         * Define o valor da propriedade routecodegid.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setROUTECODEGID(String value) {
            this.routecodegid = value;
        }

        /**
         * Obtém o valor da propriedade dimratefactorgid.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDIMRATEFACTORGID() {
            return dimratefactorgid;
        }

        /**
         * Define o valor da propriedade dimratefactorgid.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDIMRATEFACTORGID(String value) {
            this.dimratefactorgid = value;
        }

        /**
         * Obtém o valor da propriedade effectivedate.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEFFECTIVEDATE() {
            return effectivedate;
        }

        /**
         * Define o valor da propriedade effectivedate.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEFFECTIVEDATE(String value) {
            this.effectivedate = value;
        }

        /**
         * Obtém o valor da propriedade expirationdate.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEXPIRATIONDATE() {
            return expirationdate;
        }

        /**
         * Define o valor da propriedade expirationdate.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEXPIRATIONDATE(String value) {
            this.expirationdate = value;
        }

        /**
         * Obtém o valor da propriedade allowuncostedlineitems.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getALLOWUNCOSTEDLINEITEMS() {
            return allowuncostedlineitems;
        }

        /**
         * Define o valor da propriedade allowuncostedlineitems.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setALLOWUNCOSTEDLINEITEMS(String value) {
            this.allowuncostedlineitems = value;
        }

        /**
         * Obtém o valor da propriedade shipperminvaluegid.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSHIPPERMINVALUEGID() {
            return shipperminvaluegid;
        }

        /**
         * Define o valor da propriedade shipperminvaluegid.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSHIPPERMINVALUEGID(String value) {
            this.shipperminvaluegid = value;
        }

        /**
         * Obtém o valor da propriedade multibasegroupsrule.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMULTIBASEGROUPSRULE() {
            return multibasegroupsrule;
        }

        /**
         * Define o valor da propriedade multibasegroupsrule.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMULTIBASEGROUPSRULE(String value) {
            this.multibasegroupsrule = value;
        }

        /**
         * Obtém o valor da propriedade railintermodalplangid.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRAILINTERMODALPLANGID() {
            return railintermodalplangid;
        }

        /**
         * Define o valor da propriedade railintermodalplangid.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRAILINTERMODALPLANGID(String value) {
            this.railintermodalplangid = value;
        }

        /**
         * Obtém o valor da propriedade customerratecode.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCUSTOMERRATECODE() {
            return customerratecode;
        }

        /**
         * Define o valor da propriedade customerratecode.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCUSTOMERRATECODE(String value) {
            this.customerratecode = value;
        }

        /**
         * Obtém o valor da propriedade cofctofc.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCOFCTOFC() {
            return cofctofc;
        }

        /**
         * Define o valor da propriedade cofctofc.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCOFCTOFC(String value) {
            this.cofctofc = value;
        }

        /**
         * Obtém o valor da propriedade expiremarkid.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEXPIREMARKID() {
            return expiremarkid;
        }

        /**
         * Define o valor da propriedade expiremarkid.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEXPIREMARKID(String value) {
            this.expiremarkid = value;
        }

        /**
         * Obtém o valor da propriedade viasrclocprofgid.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getVIASRCLOCPROFGID() {
            return viasrclocprofgid;
        }

        /**
         * Define o valor da propriedade viasrclocprofgid.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setVIASRCLOCPROFGID(String value) {
            this.viasrclocprofgid = value;
        }

        /**
         * Obtém o valor da propriedade viadestlocprofgid.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getVIADESTLOCPROFGID() {
            return viadestlocprofgid;
        }

        /**
         * Define o valor da propriedade viadestlocprofgid.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setVIADESTLOCPROFGID(String value) {
            this.viadestlocprofgid = value;
        }

        /**
         * Obtém o valor da propriedade viasrclocgid.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getVIASRCLOCGID() {
            return viasrclocgid;
        }

        /**
         * Define o valor da propriedade viasrclocgid.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setVIASRCLOCGID(String value) {
            this.viasrclocgid = value;
        }

        /**
         * Obtém o valor da propriedade viadestlocgid.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getVIADESTLOCGID() {
            return viadestlocgid;
        }

        /**
         * Define o valor da propriedade viadestlocgid.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setVIADESTLOCGID(String value) {
            this.viadestlocgid = value;
        }

        /**
         * Obtém o valor da propriedade roundingtype.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getROUNDINGTYPE() {
            return roundingtype;
        }

        /**
         * Define o valor da propriedade roundingtype.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setROUNDINGTYPE(String value) {
            this.roundingtype = value;
        }

        /**
         * Obtém o valor da propriedade roundinginterval.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getROUNDINGINTERVAL() {
            return roundinginterval;
        }

        /**
         * Define o valor da propriedade roundinginterval.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setROUNDINGINTERVAL(String value) {
            this.roundinginterval = value;
        }

        /**
         * Obtém o valor da propriedade roundingfieldslevel.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getROUNDINGFIELDSLEVEL() {
            return roundingfieldslevel;
        }

        /**
         * Define o valor da propriedade roundingfieldslevel.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setROUNDINGFIELDSLEVEL(String value) {
            this.roundingfieldslevel = value;
        }

        /**
         * Obtém o valor da propriedade roundingapplication.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getROUNDINGAPPLICATION() {
            return roundingapplication;
        }

        /**
         * Define o valor da propriedade roundingapplication.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setROUNDINGAPPLICATION(String value) {
            this.roundingapplication = value;
        }

        /**
         * Obtém o valor da propriedade buyservprovprofilegid.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBUYSERVPROVPROFILEGID() {
            return buyservprovprofilegid;
        }

        /**
         * Define o valor da propriedade buyservprovprofilegid.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBUYSERVPROVPROFILEGID(String value) {
            this.buyservprovprofilegid = value;
        }

        /**
         * Obtém o valor da propriedade buyrategeoprofilegid.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBUYRATEGEOPROFILEGID() {
            return buyrategeoprofilegid;
        }

        /**
         * Define o valor da propriedade buyrategeoprofilegid.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBUYRATEGEOPROFILEGID(String value) {
            this.buyrategeoprofilegid = value;
        }

        /**
         * Obtém o valor da propriedade paymentmethodcodegid.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPAYMENTMETHODCODEGID() {
            return paymentmethodcodegid;
        }

        /**
         * Define o valor da propriedade paymentmethodcodegid.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPAYMENTMETHODCODEGID(String value) {
            this.paymentmethodcodegid = value;
        }

        /**
         * Obtém o valor da propriedade ismasteroverridesbase.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getISMASTEROVERRIDESBASE() {
            return ismasteroverridesbase;
        }

        /**
         * Define o valor da propriedade ismasteroverridesbase.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setISMASTEROVERRIDESBASE(String value) {
            this.ismasteroverridesbase = value;
        }

        /**
         * Obtém o valor da propriedade hazardousratetype.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getHAZARDOUSRATETYPE() {
            return hazardousratetype;
        }

        /**
         * Define o valor da propriedade hazardousratetype.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setHAZARDOUSRATETYPE(String value) {
            this.hazardousratetype = value;
        }

        /**
         * Obtém o valor da propriedade domainprofilegid.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDOMAINPROFILEGID() {
            return domainprofilegid;
        }

        /**
         * Define o valor da propriedade domainprofilegid.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDOMAINPROFILEGID(String value) {
            this.domainprofilegid = value;
        }

        /**
         * Obtém o valor da propriedade isquote.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getISQUOTE() {
            return isquote;
        }

        /**
         * Define o valor da propriedade isquote.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setISQUOTE(String value) {
            this.isquote = value;
        }

        /**
         * Obtém o valor da propriedade rotimeperioddefgid.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getROTIMEPERIODDEFGID() {
            return rotimeperioddefgid;
        }

        /**
         * Define o valor da propriedade rotimeperioddefgid.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setROTIMEPERIODDEFGID(String value) {
            this.rotimeperioddefgid = value;
        }

        /**
         * Obtém o valor da propriedade insertuser.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getINSERTUSER() {
            return insertuser;
        }

        /**
         * Define o valor da propriedade insertuser.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setINSERTUSER(String value) {
            this.insertuser = value;
        }

        /**
         * Obtém o valor da propriedade insertdate.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getINSERTDATE() {
            return insertdate;
        }

        /**
         * Define o valor da propriedade insertdate.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setINSERTDATE(String value) {
            this.insertdate = value;
        }

        /**
         * Obtém o valor da propriedade updateuser.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getUPDATEUSER() {
            return updateuser;
        }

        /**
         * Define o valor da propriedade updateuser.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setUPDATEUSER(String value) {
            this.updateuser = value;
        }

        /**
         * Obtém o valor da propriedade updatedate.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getUPDATEDATE() {
            return updatedate;
        }

        /**
         * Define o valor da propriedade updatedate.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setUPDATEDATE(String value) {
            this.updatedate = value;
        }

        /**
         * Obtém o valor da propriedade rategeostops.
         * 
         * @return
         *     possible object is
         *     {@link RATEGEOSTOPS }
         *     
         */
        public RATEGEOSTOPS getRATEGEOSTOPS() {
            return rategeostops;
        }

        /**
         * Define o valor da propriedade rategeostops.
         * 
         * @param value
         *     allowed object is
         *     {@link RATEGEOSTOPS }
         *     
         */
        public void setRATEGEOSTOPS(RATEGEOSTOPS value) {
            this.rategeostops = value;
        }

        /**
         * Obtém o valor da propriedade rategeoaccessorial.
         * 
         * @return
         *     possible object is
         *     {@link RATEGEOACCESSORIAL }
         *     
         */
        public RATEGEOACCESSORIAL getRATEGEOACCESSORIAL() {
            return rategeoaccessorial;
        }

        /**
         * Define o valor da propriedade rategeoaccessorial.
         * 
         * @param value
         *     allowed object is
         *     {@link RATEGEOACCESSORIAL }
         *     
         */
        public void setRATEGEOACCESSORIAL(RATEGEOACCESSORIAL value) {
            this.rategeoaccessorial = value;
        }

        /**
         * Obtém o valor da propriedade rategeocostgroup.
         * 
         * @return
         *     possible object is
         *     {@link RATEGEOCOSTGROUP }
         *     
         */
        public RATEGEOCOSTGROUP getRATEGEOCOSTGROUP() {
            return rategeocostgroup;
        }

        /**
         * Define o valor da propriedade rategeocostgroup.
         * 
         * @param value
         *     allowed object is
         *     {@link RATEGEOCOSTGROUP }
         *     
         */
        public void setRATEGEOCOSTGROUP(RATEGEOCOSTGROUP value) {
            this.rategeocostgroup = value;
        }

        /**
         * Obtém o valor da propriedade rgspecialserviceaccessorial.
         * 
         * @return
         *     possible object is
         *     {@link RGSPECIALSERVICEACCESSORIAL }
         *     
         */
        public RGSPECIALSERVICEACCESSORIAL getRGSPECIALSERVICEACCESSORIAL() {
            return rgspecialserviceaccessorial;
        }

        /**
         * Define o valor da propriedade rgspecialserviceaccessorial.
         * 
         * @param value
         *     allowed object is
         *     {@link RGSPECIALSERVICEACCESSORIAL }
         *     
         */
        public void setRGSPECIALSERVICEACCESSORIAL(RGSPECIALSERVICEACCESSORIAL value) {
            this.rgspecialserviceaccessorial = value;
        }

        /**
         * Obtém o valor da propriedade xlane.
         * 
         * @return
         *     possible object is
         *     {@link XLANE }
         *     
         */
        public XLANE getXLANE() {
            return xlane;
        }

        /**
         * Define o valor da propriedade xlane.
         * 
         * @param value
         *     allowed object is
         *     {@link XLANE }
         *     
         */
        public void setXLANE(XLANE value) {
            this.xlane = value;
        }

        /**
         * Obtém o valor da propriedade attribute1.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE1() {
            return attribute1;
        }

        /**
         * Define o valor da propriedade attribute1.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE1(String value) {
            this.attribute1 = value;
        }

        /**
         * Obtém o valor da propriedade attribute2.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE2() {
            return attribute2;
        }

        /**
         * Define o valor da propriedade attribute2.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE2(String value) {
            this.attribute2 = value;
        }

        /**
         * Obtém o valor da propriedade attribute3.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE3() {
            return attribute3;
        }

        /**
         * Define o valor da propriedade attribute3.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE3(String value) {
            this.attribute3 = value;
        }

        /**
         * Obtém o valor da propriedade attribute4.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE4() {
            return attribute4;
        }

        /**
         * Define o valor da propriedade attribute4.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE4(String value) {
            this.attribute4 = value;
        }

        /**
         * Obtém o valor da propriedade attribute5.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE5() {
            return attribute5;
        }

        /**
         * Define o valor da propriedade attribute5.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE5(String value) {
            this.attribute5 = value;
        }

        /**
         * Obtém o valor da propriedade attribute6.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE6() {
            return attribute6;
        }

        /**
         * Define o valor da propriedade attribute6.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE6(String value) {
            this.attribute6 = value;
        }

        /**
         * Obtém o valor da propriedade attribute7.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE7() {
            return attribute7;
        }

        /**
         * Define o valor da propriedade attribute7.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE7(String value) {
            this.attribute7 = value;
        }

        /**
         * Obtém o valor da propriedade attribute8.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE8() {
            return attribute8;
        }

        /**
         * Define o valor da propriedade attribute8.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE8(String value) {
            this.attribute8 = value;
        }

        /**
         * Obtém o valor da propriedade attribute9.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE9() {
            return attribute9;
        }

        /**
         * Define o valor da propriedade attribute9.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE9(String value) {
            this.attribute9 = value;
        }

        /**
         * Obtém o valor da propriedade attribute10.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE10() {
            return attribute10;
        }

        /**
         * Define o valor da propriedade attribute10.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE10(String value) {
            this.attribute10 = value;
        }

        /**
         * Obtém o valor da propriedade attribute11.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE11() {
            return attribute11;
        }

        /**
         * Define o valor da propriedade attribute11.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE11(String value) {
            this.attribute11 = value;
        }

        /**
         * Obtém o valor da propriedade attribute12.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE12() {
            return attribute12;
        }

        /**
         * Define o valor da propriedade attribute12.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE12(String value) {
            this.attribute12 = value;
        }

        /**
         * Obtém o valor da propriedade attribute13.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE13() {
            return attribute13;
        }

        /**
         * Define o valor da propriedade attribute13.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE13(String value) {
            this.attribute13 = value;
        }

        /**
         * Obtém o valor da propriedade attribute14.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE14() {
            return attribute14;
        }

        /**
         * Define o valor da propriedade attribute14.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE14(String value) {
            this.attribute14 = value;
        }

        /**
         * Obtém o valor da propriedade attribute15.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE15() {
            return attribute15;
        }

        /**
         * Define o valor da propriedade attribute15.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE15(String value) {
            this.attribute15 = value;
        }

        /**
         * Obtém o valor da propriedade attribute16.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE16() {
            return attribute16;
        }

        /**
         * Define o valor da propriedade attribute16.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE16(String value) {
            this.attribute16 = value;
        }

        /**
         * Obtém o valor da propriedade attribute17.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE17() {
            return attribute17;
        }

        /**
         * Define o valor da propriedade attribute17.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE17(String value) {
            this.attribute17 = value;
        }

        /**
         * Obtém o valor da propriedade attribute18.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE18() {
            return attribute18;
        }

        /**
         * Define o valor da propriedade attribute18.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE18(String value) {
            this.attribute18 = value;
        }

        /**
         * Obtém o valor da propriedade attribute19.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE19() {
            return attribute19;
        }

        /**
         * Define o valor da propriedade attribute19.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE19(String value) {
            this.attribute19 = value;
        }

        /**
         * Obtém o valor da propriedade attribute20.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE20() {
            return attribute20;
        }

        /**
         * Define o valor da propriedade attribute20.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE20(String value) {
            this.attribute20 = value;
        }

        /**
         * Obtém o valor da propriedade attributenumber1.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTENUMBER1() {
            return attributenumber1;
        }

        /**
         * Define o valor da propriedade attributenumber1.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTENUMBER1(String value) {
            this.attributenumber1 = value;
        }

        /**
         * Obtém o valor da propriedade attributenumber2.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTENUMBER2() {
            return attributenumber2;
        }

        /**
         * Define o valor da propriedade attributenumber2.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTENUMBER2(String value) {
            this.attributenumber2 = value;
        }

        /**
         * Obtém o valor da propriedade attributenumber3.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTENUMBER3() {
            return attributenumber3;
        }

        /**
         * Define o valor da propriedade attributenumber3.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTENUMBER3(String value) {
            this.attributenumber3 = value;
        }

        /**
         * Obtém o valor da propriedade attributenumber4.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTENUMBER4() {
            return attributenumber4;
        }

        /**
         * Define o valor da propriedade attributenumber4.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTENUMBER4(String value) {
            this.attributenumber4 = value;
        }

        /**
         * Obtém o valor da propriedade attributenumber5.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTENUMBER5() {
            return attributenumber5;
        }

        /**
         * Define o valor da propriedade attributenumber5.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTENUMBER5(String value) {
            this.attributenumber5 = value;
        }

        /**
         * Obtém o valor da propriedade attributenumber6.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTENUMBER6() {
            return attributenumber6;
        }

        /**
         * Define o valor da propriedade attributenumber6.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTENUMBER6(String value) {
            this.attributenumber6 = value;
        }

        /**
         * Obtém o valor da propriedade attributenumber7.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTENUMBER7() {
            return attributenumber7;
        }

        /**
         * Define o valor da propriedade attributenumber7.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTENUMBER7(String value) {
            this.attributenumber7 = value;
        }

        /**
         * Obtém o valor da propriedade attributenumber8.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTENUMBER8() {
            return attributenumber8;
        }

        /**
         * Define o valor da propriedade attributenumber8.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTENUMBER8(String value) {
            this.attributenumber8 = value;
        }

        /**
         * Obtém o valor da propriedade attributenumber9.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTENUMBER9() {
            return attributenumber9;
        }

        /**
         * Define o valor da propriedade attributenumber9.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTENUMBER9(String value) {
            this.attributenumber9 = value;
        }

        /**
         * Obtém o valor da propriedade attributenumber10.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTENUMBER10() {
            return attributenumber10;
        }

        /**
         * Define o valor da propriedade attributenumber10.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTENUMBER10(String value) {
            this.attributenumber10 = value;
        }

        /**
         * Obtém o valor da propriedade attributedate1.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTEDATE1() {
            return attributedate1;
        }

        /**
         * Define o valor da propriedade attributedate1.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTEDATE1(String value) {
            this.attributedate1 = value;
        }

        /**
         * Obtém o valor da propriedade attributedate2.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTEDATE2() {
            return attributedate2;
        }

        /**
         * Define o valor da propriedade attributedate2.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTEDATE2(String value) {
            this.attributedate2 = value;
        }

        /**
         * Obtém o valor da propriedade attributedate3.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTEDATE3() {
            return attributedate3;
        }

        /**
         * Define o valor da propriedade attributedate3.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTEDATE3(String value) {
            this.attributedate3 = value;
        }

        /**
         * Obtém o valor da propriedade attributedate4.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTEDATE4() {
            return attributedate4;
        }

        /**
         * Define o valor da propriedade attributedate4.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTEDATE4(String value) {
            this.attributedate4 = value;
        }

        /**
         * Obtém o valor da propriedade attributedate5.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTEDATE5() {
            return attributedate5;
        }

        /**
         * Define o valor da propriedade attributedate5.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTEDATE5(String value) {
            this.attributedate5 = value;
        }

        /**
         * Obtém o valor da propriedade attributedate6.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTEDATE6() {
            return attributedate6;
        }

        /**
         * Define o valor da propriedade attributedate6.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTEDATE6(String value) {
            this.attributedate6 = value;
        }

        /**
         * Obtém o valor da propriedade attributedate7.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTEDATE7() {
            return attributedate7;
        }

        /**
         * Define o valor da propriedade attributedate7.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTEDATE7(String value) {
            this.attributedate7 = value;
        }

        /**
         * Obtém o valor da propriedade attributedate8.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTEDATE8() {
            return attributedate8;
        }

        /**
         * Define o valor da propriedade attributedate8.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTEDATE8(String value) {
            this.attributedate8 = value;
        }

        /**
         * Obtém o valor da propriedade attributedate9.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTEDATE9() {
            return attributedate9;
        }

        /**
         * Define o valor da propriedade attributedate9.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTEDATE9(String value) {
            this.attributedate9 = value;
        }

        /**
         * Obtém o valor da propriedade attributedate10.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTEDATE10() {
            return attributedate10;
        }

        /**
         * Define o valor da propriedade attributedate10.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTEDATE10(String value) {
            this.attributedate10 = value;
        }

        /**
         * Obtém o valor da propriedade rategeodesc.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRATEGEODESC() {
            return rategeodesc;
        }

        /**
         * Define o valor da propriedade rategeodesc.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRATEGEODESC(String value) {
            this.rategeodesc = value;
        }

        /**
         * Obtém o valor da propriedade rategeoremarks.
         * 
         * @return
         *     possible object is
         *     {@link RATEGEOREMARKS }
         *     
         */
        public RATEGEOREMARKS getRATEGEOREMARKS() {
            return rategeoremarks;
        }

        /**
         * Define o valor da propriedade rategeoremarks.
         * 
         * @param value
         *     allowed object is
         *     {@link RATEGEOREMARKS }
         *     
         */
        public void setRATEGEOREMARKS(RATEGEOREMARKS value) {
            this.rategeoremarks = value;
        }

        /**
         * Obtém o valor da propriedade num.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNum() {
            return num;
        }

        /**
         * Define o valor da propriedade num.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNum(String value) {
            this.num = value;
        }


        /**
         * <p>Classe Java de anonymous complex type.
         * 
         * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="RATE_GEO_ACCESSORIAL_ROW" maxOccurs="unbounded" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="ACCESSORIAL_COST_GID" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="RATE_GEO_GID" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="ACCESSORIAL_CODE_GID" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                   &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                   &lt;element name="ACCESSORIAL_COST" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ACCESSORIAL_COST_TYPE" minOccurs="0"/>
         *                 &lt;/sequence>
         *                 &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" />
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "rategeoaccessorialrow"
        })
        public static class RATEGEOACCESSORIAL {

            @XmlElement(name = "RATE_GEO_ACCESSORIAL_ROW")
            protected List<RATEGEOACCESSORIALROW> rategeoaccessorialrow;

            /**
             * Gets the value of the rategeoaccessorialrow property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the rategeoaccessorialrow property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getRATEGEOACCESSORIALROW().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link RATEGEOACCESSORIALROW }
             * 
             * 
             */
            public List<RATEGEOACCESSORIALROW> getRATEGEOACCESSORIALROW() {
                if (rategeoaccessorialrow == null) {
                    rategeoaccessorialrow = new ArrayList<RATEGEOACCESSORIALROW>();
                }
                return this.rategeoaccessorialrow;
            }


            /**
             * <p>Classe Java de anonymous complex type.
             * 
             * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="ACCESSORIAL_COST_GID" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="RATE_GEO_GID" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="ACCESSORIAL_CODE_GID" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *         &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *         &lt;element name="ACCESSORIAL_COST" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ACCESSORIAL_COST_TYPE" minOccurs="0"/>
             *       &lt;/sequence>
             *       &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" />
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "accessorialcostgid",
                "rategeogid",
                "accessorialcodegid",
                "domainname",
                "insertuser",
                "insertdate",
                "updateuser",
                "updatedate",
                "accessorialcost"
            })
            public static class RATEGEOACCESSORIALROW {

                @XmlElement(name = "ACCESSORIAL_COST_GID", required = true)
                protected String accessorialcostgid;
                @XmlElement(name = "RATE_GEO_GID", required = true)
                protected String rategeogid;
                @XmlElement(name = "ACCESSORIAL_CODE_GID", required = true)
                protected String accessorialcodegid;
                @XmlElement(name = "DOMAIN_NAME", required = true)
                protected String domainname;
                @XmlElement(name = "INSERT_USER", required = true)
                protected String insertuser;
                @XmlElement(name = "INSERT_DATE", required = true)
                protected String insertdate;
                @XmlElement(name = "UPDATE_USER")
                protected String updateuser;
                @XmlElement(name = "UPDATE_DATE")
                protected String updatedate;
                @XmlElement(name = "ACCESSORIAL_COST")
                protected ACCESSORIALCOSTTYPE accessorialcost;
                @XmlAttribute(name = "num")
                protected String num;

                /**
                 * Obtém o valor da propriedade accessorialcostgid.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getACCESSORIALCOSTGID() {
                    return accessorialcostgid;
                }

                /**
                 * Define o valor da propriedade accessorialcostgid.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setACCESSORIALCOSTGID(String value) {
                    this.accessorialcostgid = value;
                }

                /**
                 * Obtém o valor da propriedade rategeogid.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getRATEGEOGID() {
                    return rategeogid;
                }

                /**
                 * Define o valor da propriedade rategeogid.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setRATEGEOGID(String value) {
                    this.rategeogid = value;
                }

                /**
                 * Obtém o valor da propriedade accessorialcodegid.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getACCESSORIALCODEGID() {
                    return accessorialcodegid;
                }

                /**
                 * Define o valor da propriedade accessorialcodegid.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setACCESSORIALCODEGID(String value) {
                    this.accessorialcodegid = value;
                }

                /**
                 * Obtém o valor da propriedade domainname.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDOMAINNAME() {
                    return domainname;
                }

                /**
                 * Define o valor da propriedade domainname.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDOMAINNAME(String value) {
                    this.domainname = value;
                }

                /**
                 * Obtém o valor da propriedade insertuser.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getINSERTUSER() {
                    return insertuser;
                }

                /**
                 * Define o valor da propriedade insertuser.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setINSERTUSER(String value) {
                    this.insertuser = value;
                }

                /**
                 * Obtém o valor da propriedade insertdate.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getINSERTDATE() {
                    return insertdate;
                }

                /**
                 * Define o valor da propriedade insertdate.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setINSERTDATE(String value) {
                    this.insertdate = value;
                }

                /**
                 * Obtém o valor da propriedade updateuser.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getUPDATEUSER() {
                    return updateuser;
                }

                /**
                 * Define o valor da propriedade updateuser.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setUPDATEUSER(String value) {
                    this.updateuser = value;
                }

                /**
                 * Obtém o valor da propriedade updatedate.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getUPDATEDATE() {
                    return updatedate;
                }

                /**
                 * Define o valor da propriedade updatedate.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setUPDATEDATE(String value) {
                    this.updatedate = value;
                }

                /**
                 * Obtém o valor da propriedade accessorialcost.
                 * 
                 * @return
                 *     possible object is
                 *     {@link ACCESSORIALCOSTTYPE }
                 *     
                 */
                public ACCESSORIALCOSTTYPE getACCESSORIALCOST() {
                    return accessorialcost;
                }

                /**
                 * Define o valor da propriedade accessorialcost.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link ACCESSORIALCOSTTYPE }
                 *     
                 */
                public void setACCESSORIALCOST(ACCESSORIALCOSTTYPE value) {
                    this.accessorialcost = value;
                }

                /**
                 * Obtém o valor da propriedade num.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getNum() {
                    return num;
                }

                /**
                 * Define o valor da propriedade num.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setNum(String value) {
                    this.num = value;
                }

            }

        }


        /**
         * <p>Classe Java de anonymous complex type.
         * 
         * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence maxOccurs="unbounded" minOccurs="0">
         *         &lt;element name="RATE_GEO_COST_GROUP_ROW" maxOccurs="unbounded">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="RATE_GEO_COST_GROUP_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                   &lt;element name="RATE_GEO_COST_GROUP_XID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                   &lt;element name="RATE_GEO_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                   &lt;element name="RATE_GEO_COST_GROUP_SEQ" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                   &lt;element name="GROUP_NAME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                   &lt;element name="USE_DEFICIT_CALCULATIONS" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                   &lt;element name="MULTI_RATES_RULE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                   &lt;element name="RATE_GROUP_TYPE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                   &lt;element name="ROUNDING_TYPE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                   &lt;element name="ROUNDING_INTERVAL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                   &lt;element name="ROUNDING_FIELDS_LEVEL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                   &lt;element name="ROUNDING_APPLICATION" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                   &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                   &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                   &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                   &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                   &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                   &lt;element name="RATE_GEO_COST" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RATE_GEO_COST_TYPE" minOccurs="0"/>
         *                 &lt;/sequence>
         *                 &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" />
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "rategeocostgrouprow"
        })
        public static class RATEGEOCOSTGROUP {

            @XmlElement(name = "RATE_GEO_COST_GROUP_ROW")
            protected List<RATEGEOCOSTGROUPROW> rategeocostgrouprow;

            /**
             * Gets the value of the rategeocostgrouprow property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the rategeocostgrouprow property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getRATEGEOCOSTGROUPROW().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link RATEGEOCOSTGROUPROW }
             * 
             * 
             */
            public List<RATEGEOCOSTGROUPROW> getRATEGEOCOSTGROUPROW() {
                if (rategeocostgrouprow == null) {
                    rategeocostgrouprow = new ArrayList<RATEGEOCOSTGROUPROW>();
                }
                return this.rategeocostgrouprow;
            }


            /**
             * <p>Classe Java de anonymous complex type.
             * 
             * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="RATE_GEO_COST_GROUP_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *         &lt;element name="RATE_GEO_COST_GROUP_XID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *         &lt;element name="RATE_GEO_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *         &lt;element name="RATE_GEO_COST_GROUP_SEQ" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *         &lt;element name="GROUP_NAME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *         &lt;element name="USE_DEFICIT_CALCULATIONS" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *         &lt;element name="MULTI_RATES_RULE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *         &lt;element name="RATE_GROUP_TYPE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *         &lt;element name="ROUNDING_TYPE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *         &lt;element name="ROUNDING_INTERVAL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *         &lt;element name="ROUNDING_FIELDS_LEVEL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *         &lt;element name="ROUNDING_APPLICATION" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *         &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *         &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *         &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *         &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *         &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *         &lt;element name="RATE_GEO_COST" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RATE_GEO_COST_TYPE" minOccurs="0"/>
             *       &lt;/sequence>
             *       &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" />
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "rategeocostgroupgid",
                "rategeocostgroupxid",
                "rategeogid",
                "rategeocostgroupseq",
                "groupname",
                "usedeficitcalculations",
                "multiratesrule",
                "rategrouptype",
                "roundingtype",
                "roundinginterval",
                "roundingfieldslevel",
                "roundingapplication",
                "domainname",
                "insertuser",
                "insertdate",
                "updateuser",
                "updatedate",
                "rategeocost"
            })
            public static class RATEGEOCOSTGROUPROW {

                @XmlElement(name = "RATE_GEO_COST_GROUP_GID")
                protected String rategeocostgroupgid;
                @XmlElement(name = "RATE_GEO_COST_GROUP_XID")
                protected String rategeocostgroupxid;
                @XmlElement(name = "RATE_GEO_GID")
                protected String rategeogid;
                @XmlElement(name = "RATE_GEO_COST_GROUP_SEQ")
                protected String rategeocostgroupseq;
                @XmlElement(name = "GROUP_NAME")
                protected String groupname;
                @XmlElement(name = "USE_DEFICIT_CALCULATIONS")
                protected String usedeficitcalculations;
                @XmlElement(name = "MULTI_RATES_RULE")
                protected String multiratesrule;
                @XmlElement(name = "RATE_GROUP_TYPE")
                protected String rategrouptype;
                @XmlElement(name = "ROUNDING_TYPE")
                protected String roundingtype;
                @XmlElement(name = "ROUNDING_INTERVAL")
                protected String roundinginterval;
                @XmlElement(name = "ROUNDING_FIELDS_LEVEL")
                protected String roundingfieldslevel;
                @XmlElement(name = "ROUNDING_APPLICATION")
                protected String roundingapplication;
                @XmlElement(name = "DOMAIN_NAME")
                protected String domainname;
                @XmlElement(name = "INSERT_USER")
                protected String insertuser;
                @XmlElement(name = "INSERT_DATE")
                protected String insertdate;
                @XmlElement(name = "UPDATE_USER")
                protected String updateuser;
                @XmlElement(name = "UPDATE_DATE")
                protected String updatedate;
                @XmlElement(name = "RATE_GEO_COST")
                protected RATEGEOCOSTTYPE rategeocost;
                @XmlAttribute(name = "num")
                protected String num;

                /**
                 * Obtém o valor da propriedade rategeocostgroupgid.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getRATEGEOCOSTGROUPGID() {
                    return rategeocostgroupgid;
                }

                /**
                 * Define o valor da propriedade rategeocostgroupgid.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setRATEGEOCOSTGROUPGID(String value) {
                    this.rategeocostgroupgid = value;
                }

                /**
                 * Obtém o valor da propriedade rategeocostgroupxid.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getRATEGEOCOSTGROUPXID() {
                    return rategeocostgroupxid;
                }

                /**
                 * Define o valor da propriedade rategeocostgroupxid.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setRATEGEOCOSTGROUPXID(String value) {
                    this.rategeocostgroupxid = value;
                }

                /**
                 * Obtém o valor da propriedade rategeogid.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getRATEGEOGID() {
                    return rategeogid;
                }

                /**
                 * Define o valor da propriedade rategeogid.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setRATEGEOGID(String value) {
                    this.rategeogid = value;
                }

                /**
                 * Obtém o valor da propriedade rategeocostgroupseq.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getRATEGEOCOSTGROUPSEQ() {
                    return rategeocostgroupseq;
                }

                /**
                 * Define o valor da propriedade rategeocostgroupseq.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setRATEGEOCOSTGROUPSEQ(String value) {
                    this.rategeocostgroupseq = value;
                }

                /**
                 * Obtém o valor da propriedade groupname.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getGROUPNAME() {
                    return groupname;
                }

                /**
                 * Define o valor da propriedade groupname.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setGROUPNAME(String value) {
                    this.groupname = value;
                }

                /**
                 * Obtém o valor da propriedade usedeficitcalculations.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getUSEDEFICITCALCULATIONS() {
                    return usedeficitcalculations;
                }

                /**
                 * Define o valor da propriedade usedeficitcalculations.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setUSEDEFICITCALCULATIONS(String value) {
                    this.usedeficitcalculations = value;
                }

                /**
                 * Obtém o valor da propriedade multiratesrule.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getMULTIRATESRULE() {
                    return multiratesrule;
                }

                /**
                 * Define o valor da propriedade multiratesrule.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setMULTIRATESRULE(String value) {
                    this.multiratesrule = value;
                }

                /**
                 * Obtém o valor da propriedade rategrouptype.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getRATEGROUPTYPE() {
                    return rategrouptype;
                }

                /**
                 * Define o valor da propriedade rategrouptype.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setRATEGROUPTYPE(String value) {
                    this.rategrouptype = value;
                }

                /**
                 * Obtém o valor da propriedade roundingtype.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getROUNDINGTYPE() {
                    return roundingtype;
                }

                /**
                 * Define o valor da propriedade roundingtype.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setROUNDINGTYPE(String value) {
                    this.roundingtype = value;
                }

                /**
                 * Obtém o valor da propriedade roundinginterval.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getROUNDINGINTERVAL() {
                    return roundinginterval;
                }

                /**
                 * Define o valor da propriedade roundinginterval.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setROUNDINGINTERVAL(String value) {
                    this.roundinginterval = value;
                }

                /**
                 * Obtém o valor da propriedade roundingfieldslevel.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getROUNDINGFIELDSLEVEL() {
                    return roundingfieldslevel;
                }

                /**
                 * Define o valor da propriedade roundingfieldslevel.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setROUNDINGFIELDSLEVEL(String value) {
                    this.roundingfieldslevel = value;
                }

                /**
                 * Obtém o valor da propriedade roundingapplication.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getROUNDINGAPPLICATION() {
                    return roundingapplication;
                }

                /**
                 * Define o valor da propriedade roundingapplication.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setROUNDINGAPPLICATION(String value) {
                    this.roundingapplication = value;
                }

                /**
                 * Obtém o valor da propriedade domainname.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDOMAINNAME() {
                    return domainname;
                }

                /**
                 * Define o valor da propriedade domainname.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDOMAINNAME(String value) {
                    this.domainname = value;
                }

                /**
                 * Obtém o valor da propriedade insertuser.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getINSERTUSER() {
                    return insertuser;
                }

                /**
                 * Define o valor da propriedade insertuser.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setINSERTUSER(String value) {
                    this.insertuser = value;
                }

                /**
                 * Obtém o valor da propriedade insertdate.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getINSERTDATE() {
                    return insertdate;
                }

                /**
                 * Define o valor da propriedade insertdate.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setINSERTDATE(String value) {
                    this.insertdate = value;
                }

                /**
                 * Obtém o valor da propriedade updateuser.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getUPDATEUSER() {
                    return updateuser;
                }

                /**
                 * Define o valor da propriedade updateuser.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setUPDATEUSER(String value) {
                    this.updateuser = value;
                }

                /**
                 * Obtém o valor da propriedade updatedate.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getUPDATEDATE() {
                    return updatedate;
                }

                /**
                 * Define o valor da propriedade updatedate.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setUPDATEDATE(String value) {
                    this.updatedate = value;
                }

                /**
                 * Obtém o valor da propriedade rategeocost.
                 * 
                 * @return
                 *     possible object is
                 *     {@link RATEGEOCOSTTYPE }
                 *     
                 */
                public RATEGEOCOSTTYPE getRATEGEOCOST() {
                    return rategeocost;
                }

                /**
                 * Define o valor da propriedade rategeocost.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link RATEGEOCOSTTYPE }
                 *     
                 */
                public void setRATEGEOCOST(RATEGEOCOSTTYPE value) {
                    this.rategeocost = value;
                }

                /**
                 * Obtém o valor da propriedade num.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getNum() {
                    return num;
                }

                /**
                 * Define o valor da propriedade num.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setNum(String value) {
                    this.num = value;
                }

            }

        }


        /**
         * <p>Classe Java de anonymous complex type.
         * 
         * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="RATE_GEO_REMARKS_ROW" maxOccurs="unbounded">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="TRANSACTION_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                   &lt;element name="REMARK_SEQUENCE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                   &lt;element name="REMARK_QUALIFIER_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                   &lt;element name="REMARK_LEVEL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                   &lt;element name="REMARK_TEXT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                   &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                   &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                   &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "rategeoremarksrow"
        })
        public static class RATEGEOREMARKS {

            @XmlElement(name = "RATE_GEO_REMARKS_ROW", required = true)
            protected List<RATEGEOREMARKSROW> rategeoremarksrow;

            /**
             * Gets the value of the rategeoremarksrow property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the rategeoremarksrow property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getRATEGEOREMARKSROW().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link RATEGEOREMARKSROW }
             * 
             * 
             */
            public List<RATEGEOREMARKSROW> getRATEGEOREMARKSROW() {
                if (rategeoremarksrow == null) {
                    rategeoremarksrow = new ArrayList<RATEGEOREMARKSROW>();
                }
                return this.rategeoremarksrow;
            }


            /**
             * <p>Classe Java de anonymous complex type.
             * 
             * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="TRANSACTION_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *         &lt;element name="REMARK_SEQUENCE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *         &lt;element name="REMARK_QUALIFIER_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *         &lt;element name="REMARK_LEVEL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *         &lt;element name="REMARK_TEXT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *         &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *         &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *         &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "transactioncode",
                "remarksequence",
                "remarkqualifiergid",
                "remarklevel",
                "remarktext",
                "domainname",
                "insertuser",
                "insertdate",
                "updateuser",
                "updatedate"
            })
            public static class RATEGEOREMARKSROW {

                @XmlElement(name = "TRANSACTION_CODE")
                protected String transactioncode;
                @XmlElement(name = "REMARK_SEQUENCE")
                protected String remarksequence;
                @XmlElement(name = "REMARK_QUALIFIER_GID")
                protected String remarkqualifiergid;
                @XmlElement(name = "REMARK_LEVEL")
                protected String remarklevel;
                @XmlElement(name = "REMARK_TEXT")
                protected String remarktext;
                @XmlElement(name = "DOMAIN_NAME")
                protected String domainname;
                @XmlElement(name = "INSERT_USER", required = true)
                protected String insertuser;
                @XmlElement(name = "INSERT_DATE", required = true)
                protected String insertdate;
                @XmlElement(name = "UPDATE_USER")
                protected String updateuser;
                @XmlElement(name = "UPDATE_DATE")
                protected String updatedate;

                /**
                 * Obtém o valor da propriedade transactioncode.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getTRANSACTIONCODE() {
                    return transactioncode;
                }

                /**
                 * Define o valor da propriedade transactioncode.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setTRANSACTIONCODE(String value) {
                    this.transactioncode = value;
                }

                /**
                 * Obtém o valor da propriedade remarksequence.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getREMARKSEQUENCE() {
                    return remarksequence;
                }

                /**
                 * Define o valor da propriedade remarksequence.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setREMARKSEQUENCE(String value) {
                    this.remarksequence = value;
                }

                /**
                 * Obtém o valor da propriedade remarkqualifiergid.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getREMARKQUALIFIERGID() {
                    return remarkqualifiergid;
                }

                /**
                 * Define o valor da propriedade remarkqualifiergid.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setREMARKQUALIFIERGID(String value) {
                    this.remarkqualifiergid = value;
                }

                /**
                 * Obtém o valor da propriedade remarklevel.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getREMARKLEVEL() {
                    return remarklevel;
                }

                /**
                 * Define o valor da propriedade remarklevel.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setREMARKLEVEL(String value) {
                    this.remarklevel = value;
                }

                /**
                 * Obtém o valor da propriedade remarktext.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getREMARKTEXT() {
                    return remarktext;
                }

                /**
                 * Define o valor da propriedade remarktext.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setREMARKTEXT(String value) {
                    this.remarktext = value;
                }

                /**
                 * Obtém o valor da propriedade domainname.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDOMAINNAME() {
                    return domainname;
                }

                /**
                 * Define o valor da propriedade domainname.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDOMAINNAME(String value) {
                    this.domainname = value;
                }

                /**
                 * Obtém o valor da propriedade insertuser.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getINSERTUSER() {
                    return insertuser;
                }

                /**
                 * Define o valor da propriedade insertuser.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setINSERTUSER(String value) {
                    this.insertuser = value;
                }

                /**
                 * Obtém o valor da propriedade insertdate.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getINSERTDATE() {
                    return insertdate;
                }

                /**
                 * Define o valor da propriedade insertdate.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setINSERTDATE(String value) {
                    this.insertdate = value;
                }

                /**
                 * Obtém o valor da propriedade updateuser.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getUPDATEUSER() {
                    return updateuser;
                }

                /**
                 * Define o valor da propriedade updateuser.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setUPDATEUSER(String value) {
                    this.updateuser = value;
                }

                /**
                 * Obtém o valor da propriedade updatedate.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getUPDATEDATE() {
                    return updatedate;
                }

                /**
                 * Define o valor da propriedade updatedate.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setUPDATEDATE(String value) {
                    this.updatedate = value;
                }

            }

        }


        /**
         * <p>Classe Java de anonymous complex type.
         * 
         * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence maxOccurs="unbounded" minOccurs="0">
         *         &lt;element name="RATE_GEO_STOPS_ROW" maxOccurs="unbounded">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="RATE_GEO_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                   &lt;element name="LOW_STOP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                   &lt;element name="HIGH_STOP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                   &lt;element name="PER_STOP_COST" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                   &lt;element name="PER_STOP_COST_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                   &lt;element name="PER_STOP_COST_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                   &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                   &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                   &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                   &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                   &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                 &lt;/sequence>
         *                 &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" />
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "rategeostopsrow"
        })
        public static class RATEGEOSTOPS {

            @XmlElement(name = "RATE_GEO_STOPS_ROW")
            protected List<RATEGEOSTOPSROW> rategeostopsrow;

            /**
             * Gets the value of the rategeostopsrow property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the rategeostopsrow property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getRATEGEOSTOPSROW().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link RATEGEOSTOPSROW }
             * 
             * 
             */
            public List<RATEGEOSTOPSROW> getRATEGEOSTOPSROW() {
                if (rategeostopsrow == null) {
                    rategeostopsrow = new ArrayList<RATEGEOSTOPSROW>();
                }
                return this.rategeostopsrow;
            }


            /**
             * <p>Classe Java de anonymous complex type.
             * 
             * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="RATE_GEO_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *         &lt;element name="LOW_STOP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *         &lt;element name="HIGH_STOP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *         &lt;element name="PER_STOP_COST" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *         &lt;element name="PER_STOP_COST_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *         &lt;element name="PER_STOP_COST_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *         &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *         &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *         &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *         &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *         &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *       &lt;/sequence>
             *       &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" />
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "rategeogid",
                "lowstop",
                "highstop",
                "perstopcost",
                "perstopcostgid",
                "perstopcostbase",
                "domainname",
                "insertuser",
                "insertdate",
                "updateuser",
                "updatedate"
            })
            public static class RATEGEOSTOPSROW {

                @XmlElement(name = "RATE_GEO_GID")
                protected String rategeogid;
                @XmlElement(name = "LOW_STOP")
                protected String lowstop;
                @XmlElement(name = "HIGH_STOP")
                protected String highstop;
                @XmlElement(name = "PER_STOP_COST")
                protected String perstopcost;
                @XmlElement(name = "PER_STOP_COST_GID")
                protected String perstopcostgid;
                @XmlElement(name = "PER_STOP_COST_BASE")
                protected String perstopcostbase;
                @XmlElement(name = "DOMAIN_NAME")
                protected String domainname;
                @XmlElement(name = "INSERT_USER")
                protected String insertuser;
                @XmlElement(name = "INSERT_DATE")
                protected String insertdate;
                @XmlElement(name = "UPDATE_USER")
                protected String updateuser;
                @XmlElement(name = "UPDATE_DATE")
                protected String updatedate;
                @XmlAttribute(name = "num")
                protected String num;

                /**
                 * Obtém o valor da propriedade rategeogid.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getRATEGEOGID() {
                    return rategeogid;
                }

                /**
                 * Define o valor da propriedade rategeogid.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setRATEGEOGID(String value) {
                    this.rategeogid = value;
                }

                /**
                 * Obtém o valor da propriedade lowstop.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getLOWSTOP() {
                    return lowstop;
                }

                /**
                 * Define o valor da propriedade lowstop.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setLOWSTOP(String value) {
                    this.lowstop = value;
                }

                /**
                 * Obtém o valor da propriedade highstop.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getHIGHSTOP() {
                    return highstop;
                }

                /**
                 * Define o valor da propriedade highstop.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setHIGHSTOP(String value) {
                    this.highstop = value;
                }

                /**
                 * Obtém o valor da propriedade perstopcost.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getPERSTOPCOST() {
                    return perstopcost;
                }

                /**
                 * Define o valor da propriedade perstopcost.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setPERSTOPCOST(String value) {
                    this.perstopcost = value;
                }

                /**
                 * Obtém o valor da propriedade perstopcostgid.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getPERSTOPCOSTGID() {
                    return perstopcostgid;
                }

                /**
                 * Define o valor da propriedade perstopcostgid.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setPERSTOPCOSTGID(String value) {
                    this.perstopcostgid = value;
                }

                /**
                 * Obtém o valor da propriedade perstopcostbase.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getPERSTOPCOSTBASE() {
                    return perstopcostbase;
                }

                /**
                 * Define o valor da propriedade perstopcostbase.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setPERSTOPCOSTBASE(String value) {
                    this.perstopcostbase = value;
                }

                /**
                 * Obtém o valor da propriedade domainname.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDOMAINNAME() {
                    return domainname;
                }

                /**
                 * Define o valor da propriedade domainname.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDOMAINNAME(String value) {
                    this.domainname = value;
                }

                /**
                 * Obtém o valor da propriedade insertuser.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getINSERTUSER() {
                    return insertuser;
                }

                /**
                 * Define o valor da propriedade insertuser.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setINSERTUSER(String value) {
                    this.insertuser = value;
                }

                /**
                 * Obtém o valor da propriedade insertdate.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getINSERTDATE() {
                    return insertdate;
                }

                /**
                 * Define o valor da propriedade insertdate.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setINSERTDATE(String value) {
                    this.insertdate = value;
                }

                /**
                 * Obtém o valor da propriedade updateuser.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getUPDATEUSER() {
                    return updateuser;
                }

                /**
                 * Define o valor da propriedade updateuser.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setUPDATEUSER(String value) {
                    this.updateuser = value;
                }

                /**
                 * Obtém o valor da propriedade updatedate.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getUPDATEDATE() {
                    return updatedate;
                }

                /**
                 * Define o valor da propriedade updatedate.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setUPDATEDATE(String value) {
                    this.updatedate = value;
                }

                /**
                 * Obtém o valor da propriedade num.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getNum() {
                    return num;
                }

                /**
                 * Define o valor da propriedade num.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setNum(String value) {
                    this.num = value;
                }

            }

        }


        /**
         * <p>Classe Java de anonymous complex type.
         * 
         * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="RG_SPECIAL_SERVICE_ACCESSORIAL_ROW" maxOccurs="unbounded" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="ACCESSORIAL_COST_GID" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="RATE_GEO_GID" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="ACCESSORIAL_CODE_GID" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                   &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                   &lt;element name="ACCESSORIAL_COST" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ACCESSORIAL_COST_TYPE" minOccurs="0"/>
         *                 &lt;/sequence>
         *                 &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" />
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "rgspecialserviceaccessorialrow"
        })
        public static class RGSPECIALSERVICEACCESSORIAL {

            @XmlElement(name = "RG_SPECIAL_SERVICE_ACCESSORIAL_ROW")
            protected List<RGSPECIALSERVICEACCESSORIALROW> rgspecialserviceaccessorialrow;

            /**
             * Gets the value of the rgspecialserviceaccessorialrow property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the rgspecialserviceaccessorialrow property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getRGSPECIALSERVICEACCESSORIALROW().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link RGSPECIALSERVICEACCESSORIALROW }
             * 
             * 
             */
            public List<RGSPECIALSERVICEACCESSORIALROW> getRGSPECIALSERVICEACCESSORIALROW() {
                if (rgspecialserviceaccessorialrow == null) {
                    rgspecialserviceaccessorialrow = new ArrayList<RGSPECIALSERVICEACCESSORIALROW>();
                }
                return this.rgspecialserviceaccessorialrow;
            }


            /**
             * <p>Classe Java de anonymous complex type.
             * 
             * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="ACCESSORIAL_COST_GID" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="RATE_GEO_GID" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="ACCESSORIAL_CODE_GID" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *         &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *         &lt;element name="ACCESSORIAL_COST" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ACCESSORIAL_COST_TYPE" minOccurs="0"/>
             *       &lt;/sequence>
             *       &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" />
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "accessorialcostgid",
                "rategeogid",
                "accessorialcodegid",
                "domainname",
                "insertuser",
                "insertdate",
                "updateuser",
                "updatedate",
                "accessorialcost"
            })
            public static class RGSPECIALSERVICEACCESSORIALROW {

                @XmlElement(name = "ACCESSORIAL_COST_GID", required = true)
                protected String accessorialcostgid;
                @XmlElement(name = "RATE_GEO_GID", required = true)
                protected String rategeogid;
                @XmlElement(name = "ACCESSORIAL_CODE_GID", required = true)
                protected String accessorialcodegid;
                @XmlElement(name = "DOMAIN_NAME", required = true)
                protected String domainname;
                @XmlElement(name = "INSERT_USER", required = true)
                protected String insertuser;
                @XmlElement(name = "INSERT_DATE", required = true)
                protected String insertdate;
                @XmlElement(name = "UPDATE_USER")
                protected String updateuser;
                @XmlElement(name = "UPDATE_DATE")
                protected String updatedate;
                @XmlElement(name = "ACCESSORIAL_COST")
                protected ACCESSORIALCOSTTYPE accessorialcost;
                @XmlAttribute(name = "num")
                protected String num;

                /**
                 * Obtém o valor da propriedade accessorialcostgid.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getACCESSORIALCOSTGID() {
                    return accessorialcostgid;
                }

                /**
                 * Define o valor da propriedade accessorialcostgid.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setACCESSORIALCOSTGID(String value) {
                    this.accessorialcostgid = value;
                }

                /**
                 * Obtém o valor da propriedade rategeogid.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getRATEGEOGID() {
                    return rategeogid;
                }

                /**
                 * Define o valor da propriedade rategeogid.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setRATEGEOGID(String value) {
                    this.rategeogid = value;
                }

                /**
                 * Obtém o valor da propriedade accessorialcodegid.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getACCESSORIALCODEGID() {
                    return accessorialcodegid;
                }

                /**
                 * Define o valor da propriedade accessorialcodegid.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setACCESSORIALCODEGID(String value) {
                    this.accessorialcodegid = value;
                }

                /**
                 * Obtém o valor da propriedade domainname.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDOMAINNAME() {
                    return domainname;
                }

                /**
                 * Define o valor da propriedade domainname.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDOMAINNAME(String value) {
                    this.domainname = value;
                }

                /**
                 * Obtém o valor da propriedade insertuser.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getINSERTUSER() {
                    return insertuser;
                }

                /**
                 * Define o valor da propriedade insertuser.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setINSERTUSER(String value) {
                    this.insertuser = value;
                }

                /**
                 * Obtém o valor da propriedade insertdate.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getINSERTDATE() {
                    return insertdate;
                }

                /**
                 * Define o valor da propriedade insertdate.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setINSERTDATE(String value) {
                    this.insertdate = value;
                }

                /**
                 * Obtém o valor da propriedade updateuser.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getUPDATEUSER() {
                    return updateuser;
                }

                /**
                 * Define o valor da propriedade updateuser.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setUPDATEUSER(String value) {
                    this.updateuser = value;
                }

                /**
                 * Obtém o valor da propriedade updatedate.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getUPDATEDATE() {
                    return updatedate;
                }

                /**
                 * Define o valor da propriedade updatedate.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setUPDATEDATE(String value) {
                    this.updatedate = value;
                }

                /**
                 * Obtém o valor da propriedade accessorialcost.
                 * 
                 * @return
                 *     possible object is
                 *     {@link ACCESSORIALCOSTTYPE }
                 *     
                 */
                public ACCESSORIALCOSTTYPE getACCESSORIALCOST() {
                    return accessorialcost;
                }

                /**
                 * Define o valor da propriedade accessorialcost.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link ACCESSORIALCOSTTYPE }
                 *     
                 */
                public void setACCESSORIALCOST(ACCESSORIALCOSTTYPE value) {
                    this.accessorialcost = value;
                }

                /**
                 * Obtém o valor da propriedade num.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getNum() {
                    return num;
                }

                /**
                 * Define o valor da propriedade num.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setNum(String value) {
                    this.num = value;
                }

            }

        }

    }

}
