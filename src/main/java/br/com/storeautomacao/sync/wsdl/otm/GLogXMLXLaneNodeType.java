
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de GLogXMLXLaneNodeType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="GLogXMLXLaneNodeType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="XLaneNode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}XLaneNodeType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GLogXMLXLaneNodeType", propOrder = {
    "xLaneNode"
})
public class GLogXMLXLaneNodeType {

    @XmlElement(name = "XLaneNode", required = true)
    protected XLaneNodeType xLaneNode;

    /**
     * Obtém o valor da propriedade xLaneNode.
     * 
     * @return
     *     possible object is
     *     {@link XLaneNodeType }
     *     
     */
    public XLaneNodeType getXLaneNode() {
        return xLaneNode;
    }

    /**
     * Define o valor da propriedade xLaneNode.
     * 
     * @param value
     *     allowed object is
     *     {@link XLaneNodeType }
     *     
     */
    public void setXLaneNode(XLaneNodeType value) {
        this.xLaneNode = value;
    }

}
