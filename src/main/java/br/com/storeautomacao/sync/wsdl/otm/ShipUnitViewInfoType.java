
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * Provides a representation of the ShipUnitLine(s) on the Shipment grouped by Release and ReleaseLine.
 *             Can be used to identify the count and/or total weight of order lines on a shipment.
 *          
 * 
 * <p>Classe Java de ShipUnitViewInfoType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="ShipUnitViewInfoType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ShipUnitViewByRelease" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ShipUnitViewByReleaseType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="ShipUnitViewByReleaseLine" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ShipUnitViewByReleaseLineType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ShipUnitViewInfoType", propOrder = {
    "shipUnitViewByRelease",
    "shipUnitViewByReleaseLine"
})
public class ShipUnitViewInfoType {

    @XmlElement(name = "ShipUnitViewByRelease")
    protected List<ShipUnitViewByReleaseType> shipUnitViewByRelease;
    @XmlElement(name = "ShipUnitViewByReleaseLine")
    protected List<ShipUnitViewByReleaseLineType> shipUnitViewByReleaseLine;

    /**
     * Gets the value of the shipUnitViewByRelease property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the shipUnitViewByRelease property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getShipUnitViewByRelease().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ShipUnitViewByReleaseType }
     * 
     * 
     */
    public List<ShipUnitViewByReleaseType> getShipUnitViewByRelease() {
        if (shipUnitViewByRelease == null) {
            shipUnitViewByRelease = new ArrayList<ShipUnitViewByReleaseType>();
        }
        return this.shipUnitViewByRelease;
    }

    /**
     * Gets the value of the shipUnitViewByReleaseLine property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the shipUnitViewByReleaseLine property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getShipUnitViewByReleaseLine().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ShipUnitViewByReleaseLineType }
     * 
     * 
     */
    public List<ShipUnitViewByReleaseLineType> getShipUnitViewByReleaseLine() {
        if (shipUnitViewByReleaseLine == null) {
            shipUnitViewByReleaseLine = new ArrayList<ShipUnitViewByReleaseLineType>();
        }
        return this.shipUnitViewByReleaseLine;
    }

}
