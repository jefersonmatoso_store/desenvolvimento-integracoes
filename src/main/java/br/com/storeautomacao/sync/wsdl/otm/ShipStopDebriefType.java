
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Specifies the debrief information about orders on shipment stops. It is used to indicate what was collected at each pickup stop.
 * 
 * <p>Classe Java de ShipStopDebriefType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="ShipStopDebriefType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SequenceNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Activity" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ShipUnitGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="LineNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ReleaseLineGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="ReleaseShipUnitGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;choice minOccurs="0">
 *           &lt;element name="SEquipmentGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *           &lt;element name="SEquipmentGidQuery" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}SEquipmentGidQueryType" minOccurs="0"/>
 *         &lt;/choice>
 *         &lt;element name="PackagedItemCount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PackagedItemSpecRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLShipUnitSpecRefType" minOccurs="0"/>
 *         &lt;element name="PackagedItemSpecCount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TransportHandlingUnitRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLShipUnitSpecRefType" minOccurs="0"/>
 *         &lt;element name="TransportHandlingUnitCount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ItemGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="NonConfReasonCodeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="MatchType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ShipStopDebriefType", propOrder = {
    "sequenceNumber",
    "activity",
    "shipUnitGid",
    "lineNumber",
    "releaseLineGid",
    "releaseShipUnitGid",
    "sEquipmentGid",
    "sEquipmentGidQuery",
    "packagedItemCount",
    "packagedItemSpecRef",
    "packagedItemSpecCount",
    "transportHandlingUnitRef",
    "transportHandlingUnitCount",
    "itemGid",
    "nonConfReasonCodeGid",
    "matchType"
})
public class ShipStopDebriefType {

    @XmlElement(name = "SequenceNumber")
    protected String sequenceNumber;
    @XmlElement(name = "Activity", required = true)
    protected String activity;
    @XmlElement(name = "ShipUnitGid")
    protected GLogXMLGidType shipUnitGid;
    @XmlElement(name = "LineNumber")
    protected String lineNumber;
    @XmlElement(name = "ReleaseLineGid")
    protected GLogXMLGidType releaseLineGid;
    @XmlElement(name = "ReleaseShipUnitGid")
    protected GLogXMLGidType releaseShipUnitGid;
    @XmlElement(name = "SEquipmentGid")
    protected GLogXMLGidType sEquipmentGid;
    @XmlElement(name = "SEquipmentGidQuery")
    protected SEquipmentGidQueryType sEquipmentGidQuery;
    @XmlElement(name = "PackagedItemCount")
    protected String packagedItemCount;
    @XmlElement(name = "PackagedItemSpecRef")
    protected GLogXMLShipUnitSpecRefType packagedItemSpecRef;
    @XmlElement(name = "PackagedItemSpecCount")
    protected String packagedItemSpecCount;
    @XmlElement(name = "TransportHandlingUnitRef")
    protected GLogXMLShipUnitSpecRefType transportHandlingUnitRef;
    @XmlElement(name = "TransportHandlingUnitCount")
    protected String transportHandlingUnitCount;
    @XmlElement(name = "ItemGid")
    protected GLogXMLGidType itemGid;
    @XmlElement(name = "NonConfReasonCodeGid")
    protected GLogXMLGidType nonConfReasonCodeGid;
    @XmlElement(name = "MatchType")
    protected String matchType;

    /**
     * Obtém o valor da propriedade sequenceNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSequenceNumber() {
        return sequenceNumber;
    }

    /**
     * Define o valor da propriedade sequenceNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSequenceNumber(String value) {
        this.sequenceNumber = value;
    }

    /**
     * Obtém o valor da propriedade activity.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActivity() {
        return activity;
    }

    /**
     * Define o valor da propriedade activity.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActivity(String value) {
        this.activity = value;
    }

    /**
     * Obtém o valor da propriedade shipUnitGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getShipUnitGid() {
        return shipUnitGid;
    }

    /**
     * Define o valor da propriedade shipUnitGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setShipUnitGid(GLogXMLGidType value) {
        this.shipUnitGid = value;
    }

    /**
     * Obtém o valor da propriedade lineNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLineNumber() {
        return lineNumber;
    }

    /**
     * Define o valor da propriedade lineNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLineNumber(String value) {
        this.lineNumber = value;
    }

    /**
     * Obtém o valor da propriedade releaseLineGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getReleaseLineGid() {
        return releaseLineGid;
    }

    /**
     * Define o valor da propriedade releaseLineGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setReleaseLineGid(GLogXMLGidType value) {
        this.releaseLineGid = value;
    }

    /**
     * Obtém o valor da propriedade releaseShipUnitGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getReleaseShipUnitGid() {
        return releaseShipUnitGid;
    }

    /**
     * Define o valor da propriedade releaseShipUnitGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setReleaseShipUnitGid(GLogXMLGidType value) {
        this.releaseShipUnitGid = value;
    }

    /**
     * Obtém o valor da propriedade sEquipmentGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getSEquipmentGid() {
        return sEquipmentGid;
    }

    /**
     * Define o valor da propriedade sEquipmentGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setSEquipmentGid(GLogXMLGidType value) {
        this.sEquipmentGid = value;
    }

    /**
     * Obtém o valor da propriedade sEquipmentGidQuery.
     * 
     * @return
     *     possible object is
     *     {@link SEquipmentGidQueryType }
     *     
     */
    public SEquipmentGidQueryType getSEquipmentGidQuery() {
        return sEquipmentGidQuery;
    }

    /**
     * Define o valor da propriedade sEquipmentGidQuery.
     * 
     * @param value
     *     allowed object is
     *     {@link SEquipmentGidQueryType }
     *     
     */
    public void setSEquipmentGidQuery(SEquipmentGidQueryType value) {
        this.sEquipmentGidQuery = value;
    }

    /**
     * Obtém o valor da propriedade packagedItemCount.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPackagedItemCount() {
        return packagedItemCount;
    }

    /**
     * Define o valor da propriedade packagedItemCount.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPackagedItemCount(String value) {
        this.packagedItemCount = value;
    }

    /**
     * Obtém o valor da propriedade packagedItemSpecRef.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLShipUnitSpecRefType }
     *     
     */
    public GLogXMLShipUnitSpecRefType getPackagedItemSpecRef() {
        return packagedItemSpecRef;
    }

    /**
     * Define o valor da propriedade packagedItemSpecRef.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLShipUnitSpecRefType }
     *     
     */
    public void setPackagedItemSpecRef(GLogXMLShipUnitSpecRefType value) {
        this.packagedItemSpecRef = value;
    }

    /**
     * Obtém o valor da propriedade packagedItemSpecCount.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPackagedItemSpecCount() {
        return packagedItemSpecCount;
    }

    /**
     * Define o valor da propriedade packagedItemSpecCount.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPackagedItemSpecCount(String value) {
        this.packagedItemSpecCount = value;
    }

    /**
     * Obtém o valor da propriedade transportHandlingUnitRef.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLShipUnitSpecRefType }
     *     
     */
    public GLogXMLShipUnitSpecRefType getTransportHandlingUnitRef() {
        return transportHandlingUnitRef;
    }

    /**
     * Define o valor da propriedade transportHandlingUnitRef.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLShipUnitSpecRefType }
     *     
     */
    public void setTransportHandlingUnitRef(GLogXMLShipUnitSpecRefType value) {
        this.transportHandlingUnitRef = value;
    }

    /**
     * Obtém o valor da propriedade transportHandlingUnitCount.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransportHandlingUnitCount() {
        return transportHandlingUnitCount;
    }

    /**
     * Define o valor da propriedade transportHandlingUnitCount.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransportHandlingUnitCount(String value) {
        this.transportHandlingUnitCount = value;
    }

    /**
     * Obtém o valor da propriedade itemGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getItemGid() {
        return itemGid;
    }

    /**
     * Define o valor da propriedade itemGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setItemGid(GLogXMLGidType value) {
        this.itemGid = value;
    }

    /**
     * Obtém o valor da propriedade nonConfReasonCodeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getNonConfReasonCodeGid() {
        return nonConfReasonCodeGid;
    }

    /**
     * Define o valor da propriedade nonConfReasonCodeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setNonConfReasonCodeGid(GLogXMLGidType value) {
        this.nonConfReasonCodeGid = value;
    }

    /**
     * Obtém o valor da propriedade matchType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMatchType() {
        return matchType;
    }

    /**
     * Define o valor da propriedade matchType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMatchType(String value) {
        this.matchType = value;
    }

}
