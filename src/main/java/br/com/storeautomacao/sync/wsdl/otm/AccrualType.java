
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de AccrualType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="AccrualType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OTMTransactionOut">
 *       &lt;sequence>
 *         &lt;element name="SendReason" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}SendReasonType" minOccurs="0"/>
 *         &lt;element name="AccrualGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="ReleaseGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="ShipmentGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="ExchangeRateInfo" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ExchangeRateInfoType" minOccurs="0"/>
 *         &lt;element name="TotalFreightCost" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}AmountWithBaseType" minOccurs="0"/>
 *         &lt;element name="TotalFreightSentCost" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}AmountWithBaseType" minOccurs="0"/>
 *         &lt;element name="DeltaCost" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}AmountWithBaseType" minOccurs="0"/>
 *         &lt;element name="AccruedDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="SentDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="IsReversal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AccrualReleaseInfo" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}AccrualReleaseInfoType" minOccurs="0"/>
 *         &lt;element name="AccrualShipmentInfo" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}AccrualShipmentInfoType" minOccurs="0"/>
 *         &lt;element name="AccrualTransOrderInfo" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}AccrualTransOrderInfoType" minOccurs="0"/>
 *         &lt;element name="CostTypeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="AccessorialCodeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AccrualType", propOrder = {
    "sendReason",
    "accrualGid",
    "releaseGid",
    "shipmentGid",
    "exchangeRateInfo",
    "totalFreightCost",
    "totalFreightSentCost",
    "deltaCost",
    "accruedDt",
    "sentDt",
    "isReversal",
    "accrualReleaseInfo",
    "accrualShipmentInfo",
    "accrualTransOrderInfo",
    "costTypeGid",
    "accessorialCodeGid"
})
public class AccrualType
    extends OTMTransactionOut
{

    @XmlElement(name = "SendReason")
    protected SendReasonType sendReason;
    @XmlElement(name = "AccrualGid", required = true)
    protected GLogXMLGidType accrualGid;
    @XmlElement(name = "ReleaseGid", required = true)
    protected GLogXMLGidType releaseGid;
    @XmlElement(name = "ShipmentGid")
    protected GLogXMLGidType shipmentGid;
    @XmlElement(name = "ExchangeRateInfo")
    protected ExchangeRateInfoType exchangeRateInfo;
    @XmlElement(name = "TotalFreightCost")
    protected AmountWithBaseType totalFreightCost;
    @XmlElement(name = "TotalFreightSentCost")
    protected AmountWithBaseType totalFreightSentCost;
    @XmlElement(name = "DeltaCost")
    protected AmountWithBaseType deltaCost;
    @XmlElement(name = "AccruedDt")
    protected GLogDateTimeType accruedDt;
    @XmlElement(name = "SentDt")
    protected GLogDateTimeType sentDt;
    @XmlElement(name = "IsReversal")
    protected String isReversal;
    @XmlElement(name = "AccrualReleaseInfo")
    protected AccrualReleaseInfoType accrualReleaseInfo;
    @XmlElement(name = "AccrualShipmentInfo")
    protected AccrualShipmentInfoType accrualShipmentInfo;
    @XmlElement(name = "AccrualTransOrderInfo")
    protected AccrualTransOrderInfoType accrualTransOrderInfo;
    @XmlElement(name = "CostTypeGid")
    protected GLogXMLGidType costTypeGid;
    @XmlElement(name = "AccessorialCodeGid")
    protected GLogXMLGidType accessorialCodeGid;

    /**
     * Obtém o valor da propriedade sendReason.
     * 
     * @return
     *     possible object is
     *     {@link SendReasonType }
     *     
     */
    public SendReasonType getSendReason() {
        return sendReason;
    }

    /**
     * Define o valor da propriedade sendReason.
     * 
     * @param value
     *     allowed object is
     *     {@link SendReasonType }
     *     
     */
    public void setSendReason(SendReasonType value) {
        this.sendReason = value;
    }

    /**
     * Obtém o valor da propriedade accrualGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getAccrualGid() {
        return accrualGid;
    }

    /**
     * Define o valor da propriedade accrualGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setAccrualGid(GLogXMLGidType value) {
        this.accrualGid = value;
    }

    /**
     * Obtém o valor da propriedade releaseGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getReleaseGid() {
        return releaseGid;
    }

    /**
     * Define o valor da propriedade releaseGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setReleaseGid(GLogXMLGidType value) {
        this.releaseGid = value;
    }

    /**
     * Obtém o valor da propriedade shipmentGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getShipmentGid() {
        return shipmentGid;
    }

    /**
     * Define o valor da propriedade shipmentGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setShipmentGid(GLogXMLGidType value) {
        this.shipmentGid = value;
    }

    /**
     * Obtém o valor da propriedade exchangeRateInfo.
     * 
     * @return
     *     possible object is
     *     {@link ExchangeRateInfoType }
     *     
     */
    public ExchangeRateInfoType getExchangeRateInfo() {
        return exchangeRateInfo;
    }

    /**
     * Define o valor da propriedade exchangeRateInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link ExchangeRateInfoType }
     *     
     */
    public void setExchangeRateInfo(ExchangeRateInfoType value) {
        this.exchangeRateInfo = value;
    }

    /**
     * Obtém o valor da propriedade totalFreightCost.
     * 
     * @return
     *     possible object is
     *     {@link AmountWithBaseType }
     *     
     */
    public AmountWithBaseType getTotalFreightCost() {
        return totalFreightCost;
    }

    /**
     * Define o valor da propriedade totalFreightCost.
     * 
     * @param value
     *     allowed object is
     *     {@link AmountWithBaseType }
     *     
     */
    public void setTotalFreightCost(AmountWithBaseType value) {
        this.totalFreightCost = value;
    }

    /**
     * Obtém o valor da propriedade totalFreightSentCost.
     * 
     * @return
     *     possible object is
     *     {@link AmountWithBaseType }
     *     
     */
    public AmountWithBaseType getTotalFreightSentCost() {
        return totalFreightSentCost;
    }

    /**
     * Define o valor da propriedade totalFreightSentCost.
     * 
     * @param value
     *     allowed object is
     *     {@link AmountWithBaseType }
     *     
     */
    public void setTotalFreightSentCost(AmountWithBaseType value) {
        this.totalFreightSentCost = value;
    }

    /**
     * Obtém o valor da propriedade deltaCost.
     * 
     * @return
     *     possible object is
     *     {@link AmountWithBaseType }
     *     
     */
    public AmountWithBaseType getDeltaCost() {
        return deltaCost;
    }

    /**
     * Define o valor da propriedade deltaCost.
     * 
     * @param value
     *     allowed object is
     *     {@link AmountWithBaseType }
     *     
     */
    public void setDeltaCost(AmountWithBaseType value) {
        this.deltaCost = value;
    }

    /**
     * Obtém o valor da propriedade accruedDt.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getAccruedDt() {
        return accruedDt;
    }

    /**
     * Define o valor da propriedade accruedDt.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setAccruedDt(GLogDateTimeType value) {
        this.accruedDt = value;
    }

    /**
     * Obtém o valor da propriedade sentDt.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getSentDt() {
        return sentDt;
    }

    /**
     * Define o valor da propriedade sentDt.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setSentDt(GLogDateTimeType value) {
        this.sentDt = value;
    }

    /**
     * Obtém o valor da propriedade isReversal.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsReversal() {
        return isReversal;
    }

    /**
     * Define o valor da propriedade isReversal.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsReversal(String value) {
        this.isReversal = value;
    }

    /**
     * Obtém o valor da propriedade accrualReleaseInfo.
     * 
     * @return
     *     possible object is
     *     {@link AccrualReleaseInfoType }
     *     
     */
    public AccrualReleaseInfoType getAccrualReleaseInfo() {
        return accrualReleaseInfo;
    }

    /**
     * Define o valor da propriedade accrualReleaseInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link AccrualReleaseInfoType }
     *     
     */
    public void setAccrualReleaseInfo(AccrualReleaseInfoType value) {
        this.accrualReleaseInfo = value;
    }

    /**
     * Obtém o valor da propriedade accrualShipmentInfo.
     * 
     * @return
     *     possible object is
     *     {@link AccrualShipmentInfoType }
     *     
     */
    public AccrualShipmentInfoType getAccrualShipmentInfo() {
        return accrualShipmentInfo;
    }

    /**
     * Define o valor da propriedade accrualShipmentInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link AccrualShipmentInfoType }
     *     
     */
    public void setAccrualShipmentInfo(AccrualShipmentInfoType value) {
        this.accrualShipmentInfo = value;
    }

    /**
     * Obtém o valor da propriedade accrualTransOrderInfo.
     * 
     * @return
     *     possible object is
     *     {@link AccrualTransOrderInfoType }
     *     
     */
    public AccrualTransOrderInfoType getAccrualTransOrderInfo() {
        return accrualTransOrderInfo;
    }

    /**
     * Define o valor da propriedade accrualTransOrderInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link AccrualTransOrderInfoType }
     *     
     */
    public void setAccrualTransOrderInfo(AccrualTransOrderInfoType value) {
        this.accrualTransOrderInfo = value;
    }

    /**
     * Obtém o valor da propriedade costTypeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getCostTypeGid() {
        return costTypeGid;
    }

    /**
     * Define o valor da propriedade costTypeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setCostTypeGid(GLogXMLGidType value) {
        this.costTypeGid = value;
    }

    /**
     * Obtém o valor da propriedade accessorialCodeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getAccessorialCodeGid() {
        return accessorialCodeGid;
    }

    /**
     * Define o valor da propriedade accessorialCodeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setAccessorialCodeGid(GLogXMLGidType value) {
        this.accessorialCodeGid = value;
    }

}
