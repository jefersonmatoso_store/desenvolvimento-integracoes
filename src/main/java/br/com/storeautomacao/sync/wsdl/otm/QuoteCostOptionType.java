
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;


/**
 * provides information related to the quoted move, including cost breakdowns,mode(s),equipment,and remarks.
 * 
 * <p>Classe Java de QuoteCostOptionType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="QuoteCostOptionType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IntSavedQuery" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}IntSavedQueryType" minOccurs="0"/>
 *         &lt;element name="SequenceNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TransactionCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransactionCodeType" minOccurs="0"/>
 *         &lt;element name="SellCostOption">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="CostOption" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}CostOptionType"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="BuyCostOption">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="CostOption" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}CostOptionType"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="CostOptionEquip" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}CostOptionEquipType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Remark" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RemarkType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="CostOptionShip" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}CostOptionShipType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QuoteCostOptionType", propOrder = {
    "intSavedQuery",
    "sequenceNumber",
    "transactionCode",
    "sellCostOption",
    "buyCostOption",
    "costOptionEquip",
    "remark",
    "costOptionShip"
})
public class QuoteCostOptionType {

    @XmlElement(name = "IntSavedQuery")
    protected IntSavedQueryType intSavedQuery;
    @XmlElement(name = "SequenceNumber")
    protected String sequenceNumber;
    @XmlElement(name = "TransactionCode")
    @XmlSchemaType(name = "string")
    protected TransactionCodeType transactionCode;
    @XmlElement(name = "SellCostOption", required = true)
    protected SellCostOption sellCostOption;
    @XmlElement(name = "BuyCostOption", required = true)
    protected BuyCostOption buyCostOption;
    @XmlElement(name = "CostOptionEquip")
    protected List<CostOptionEquipType> costOptionEquip;
    @XmlElement(name = "Remark")
    protected List<RemarkType> remark;
    @XmlElement(name = "CostOptionShip")
    protected List<CostOptionShipType> costOptionShip;

    /**
     * Obtém o valor da propriedade intSavedQuery.
     * 
     * @return
     *     possible object is
     *     {@link IntSavedQueryType }
     *     
     */
    public IntSavedQueryType getIntSavedQuery() {
        return intSavedQuery;
    }

    /**
     * Define o valor da propriedade intSavedQuery.
     * 
     * @param value
     *     allowed object is
     *     {@link IntSavedQueryType }
     *     
     */
    public void setIntSavedQuery(IntSavedQueryType value) {
        this.intSavedQuery = value;
    }

    /**
     * Obtém o valor da propriedade sequenceNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSequenceNumber() {
        return sequenceNumber;
    }

    /**
     * Define o valor da propriedade sequenceNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSequenceNumber(String value) {
        this.sequenceNumber = value;
    }

    /**
     * Obtém o valor da propriedade transactionCode.
     * 
     * @return
     *     possible object is
     *     {@link TransactionCodeType }
     *     
     */
    public TransactionCodeType getTransactionCode() {
        return transactionCode;
    }

    /**
     * Define o valor da propriedade transactionCode.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionCodeType }
     *     
     */
    public void setTransactionCode(TransactionCodeType value) {
        this.transactionCode = value;
    }

    /**
     * Obtém o valor da propriedade sellCostOption.
     * 
     * @return
     *     possible object is
     *     {@link SellCostOption }
     *     
     */
    public SellCostOption getSellCostOption() {
        return sellCostOption;
    }

    /**
     * Define o valor da propriedade sellCostOption.
     * 
     * @param value
     *     allowed object is
     *     {@link SellCostOption }
     *     
     */
    public void setSellCostOption(SellCostOption value) {
        this.sellCostOption = value;
    }

    /**
     * Obtém o valor da propriedade buyCostOption.
     * 
     * @return
     *     possible object is
     *     {@link BuyCostOption }
     *     
     */
    public BuyCostOption getBuyCostOption() {
        return buyCostOption;
    }

    /**
     * Define o valor da propriedade buyCostOption.
     * 
     * @param value
     *     allowed object is
     *     {@link BuyCostOption }
     *     
     */
    public void setBuyCostOption(BuyCostOption value) {
        this.buyCostOption = value;
    }

    /**
     * Gets the value of the costOptionEquip property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the costOptionEquip property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCostOptionEquip().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CostOptionEquipType }
     * 
     * 
     */
    public List<CostOptionEquipType> getCostOptionEquip() {
        if (costOptionEquip == null) {
            costOptionEquip = new ArrayList<CostOptionEquipType>();
        }
        return this.costOptionEquip;
    }

    /**
     * Gets the value of the remark property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the remark property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRemark().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RemarkType }
     * 
     * 
     */
    public List<RemarkType> getRemark() {
        if (remark == null) {
            remark = new ArrayList<RemarkType>();
        }
        return this.remark;
    }

    /**
     * Gets the value of the costOptionShip property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the costOptionShip property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCostOptionShip().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CostOptionShipType }
     * 
     * 
     */
    public List<CostOptionShipType> getCostOptionShip() {
        if (costOptionShip == null) {
            costOptionShip = new ArrayList<CostOptionShipType>();
        }
        return this.costOptionShip;
    }


    /**
     * <p>Classe Java de anonymous complex type.
     * 
     * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="CostOption" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}CostOptionType"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "costOption"
    })
    public static class BuyCostOption {

        @XmlElement(name = "CostOption", required = true)
        protected CostOptionType costOption;

        /**
         * Obtém o valor da propriedade costOption.
         * 
         * @return
         *     possible object is
         *     {@link CostOptionType }
         *     
         */
        public CostOptionType getCostOption() {
            return costOption;
        }

        /**
         * Define o valor da propriedade costOption.
         * 
         * @param value
         *     allowed object is
         *     {@link CostOptionType }
         *     
         */
        public void setCostOption(CostOptionType value) {
            this.costOption = value;
        }

    }


    /**
     * <p>Classe Java de anonymous complex type.
     * 
     * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="CostOption" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}CostOptionType"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "costOption"
    })
    public static class SellCostOption {

        @XmlElement(name = "CostOption", required = true)
        protected CostOptionType costOption;

        /**
         * Obtém o valor da propriedade costOption.
         * 
         * @return
         *     possible object is
         *     {@link CostOptionType }
         *     
         */
        public CostOptionType getCostOption() {
            return costOption;
        }

        /**
         * Define o valor da propriedade costOption.
         * 
         * @param value
         *     allowed object is
         *     {@link CostOptionType }
         *     
         */
        public void setCostOption(CostOptionType value) {
            this.costOption = value;
        }

    }

}
