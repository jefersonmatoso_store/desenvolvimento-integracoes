
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * Specifies a resource defined for a location resource type.
 * 
 * <p>Classe Java de LocationResourceType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="LocationResourceType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IntSavedQuery" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}IntSavedQueryType" minOccurs="0"/>
 *         &lt;element name="LocationResourceGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="LocationResourceName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CalendarGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="FlexCommodityProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="EquipmentGroupProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="ModeProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="ServiceProviderProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="ContactProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="FlexCommodityCheckOption" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ApptActivityType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LocationResourceGroupGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="ApptRuleSet" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ApptRuleSetType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LocationResourceType", propOrder = {
    "intSavedQuery",
    "locationResourceGid",
    "locationResourceName",
    "description",
    "calendarGid",
    "flexCommodityProfileGid",
    "equipmentGroupProfileGid",
    "modeProfileGid",
    "serviceProviderProfileGid",
    "contactProfileGid",
    "flexCommodityCheckOption",
    "apptActivityType",
    "locationResourceGroupGid",
    "apptRuleSet"
})
public class LocationResourceType {

    @XmlElement(name = "IntSavedQuery")
    protected IntSavedQueryType intSavedQuery;
    @XmlElement(name = "LocationResourceGid")
    protected GLogXMLGidType locationResourceGid;
    @XmlElement(name = "LocationResourceName", required = true)
    protected String locationResourceName;
    @XmlElement(name = "Description")
    protected String description;
    @XmlElement(name = "CalendarGid")
    protected GLogXMLGidType calendarGid;
    @XmlElement(name = "FlexCommodityProfileGid")
    protected GLogXMLGidType flexCommodityProfileGid;
    @XmlElement(name = "EquipmentGroupProfileGid")
    protected GLogXMLGidType equipmentGroupProfileGid;
    @XmlElement(name = "ModeProfileGid")
    protected GLogXMLGidType modeProfileGid;
    @XmlElement(name = "ServiceProviderProfileGid")
    protected GLogXMLGidType serviceProviderProfileGid;
    @XmlElement(name = "ContactProfileGid")
    protected GLogXMLGidType contactProfileGid;
    @XmlElement(name = "FlexCommodityCheckOption")
    protected String flexCommodityCheckOption;
    @XmlElement(name = "ApptActivityType")
    protected String apptActivityType;
    @XmlElement(name = "LocationResourceGroupGid")
    protected GLogXMLGidType locationResourceGroupGid;
    @XmlElement(name = "ApptRuleSet")
    protected List<ApptRuleSetType> apptRuleSet;

    /**
     * Obtém o valor da propriedade intSavedQuery.
     * 
     * @return
     *     possible object is
     *     {@link IntSavedQueryType }
     *     
     */
    public IntSavedQueryType getIntSavedQuery() {
        return intSavedQuery;
    }

    /**
     * Define o valor da propriedade intSavedQuery.
     * 
     * @param value
     *     allowed object is
     *     {@link IntSavedQueryType }
     *     
     */
    public void setIntSavedQuery(IntSavedQueryType value) {
        this.intSavedQuery = value;
    }

    /**
     * Obtém o valor da propriedade locationResourceGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getLocationResourceGid() {
        return locationResourceGid;
    }

    /**
     * Define o valor da propriedade locationResourceGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setLocationResourceGid(GLogXMLGidType value) {
        this.locationResourceGid = value;
    }

    /**
     * Obtém o valor da propriedade locationResourceName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLocationResourceName() {
        return locationResourceName;
    }

    /**
     * Define o valor da propriedade locationResourceName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLocationResourceName(String value) {
        this.locationResourceName = value;
    }

    /**
     * Obtém o valor da propriedade description.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Define o valor da propriedade description.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Obtém o valor da propriedade calendarGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getCalendarGid() {
        return calendarGid;
    }

    /**
     * Define o valor da propriedade calendarGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setCalendarGid(GLogXMLGidType value) {
        this.calendarGid = value;
    }

    /**
     * Obtém o valor da propriedade flexCommodityProfileGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getFlexCommodityProfileGid() {
        return flexCommodityProfileGid;
    }

    /**
     * Define o valor da propriedade flexCommodityProfileGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setFlexCommodityProfileGid(GLogXMLGidType value) {
        this.flexCommodityProfileGid = value;
    }

    /**
     * Obtém o valor da propriedade equipmentGroupProfileGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getEquipmentGroupProfileGid() {
        return equipmentGroupProfileGid;
    }

    /**
     * Define o valor da propriedade equipmentGroupProfileGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setEquipmentGroupProfileGid(GLogXMLGidType value) {
        this.equipmentGroupProfileGid = value;
    }

    /**
     * Obtém o valor da propriedade modeProfileGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getModeProfileGid() {
        return modeProfileGid;
    }

    /**
     * Define o valor da propriedade modeProfileGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setModeProfileGid(GLogXMLGidType value) {
        this.modeProfileGid = value;
    }

    /**
     * Obtém o valor da propriedade serviceProviderProfileGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getServiceProviderProfileGid() {
        return serviceProviderProfileGid;
    }

    /**
     * Define o valor da propriedade serviceProviderProfileGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setServiceProviderProfileGid(GLogXMLGidType value) {
        this.serviceProviderProfileGid = value;
    }

    /**
     * Obtém o valor da propriedade contactProfileGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getContactProfileGid() {
        return contactProfileGid;
    }

    /**
     * Define o valor da propriedade contactProfileGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setContactProfileGid(GLogXMLGidType value) {
        this.contactProfileGid = value;
    }

    /**
     * Obtém o valor da propriedade flexCommodityCheckOption.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlexCommodityCheckOption() {
        return flexCommodityCheckOption;
    }

    /**
     * Define o valor da propriedade flexCommodityCheckOption.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlexCommodityCheckOption(String value) {
        this.flexCommodityCheckOption = value;
    }

    /**
     * Obtém o valor da propriedade apptActivityType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApptActivityType() {
        return apptActivityType;
    }

    /**
     * Define o valor da propriedade apptActivityType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApptActivityType(String value) {
        this.apptActivityType = value;
    }

    /**
     * Obtém o valor da propriedade locationResourceGroupGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getLocationResourceGroupGid() {
        return locationResourceGroupGid;
    }

    /**
     * Define o valor da propriedade locationResourceGroupGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setLocationResourceGroupGid(GLogXMLGidType value) {
        this.locationResourceGroupGid = value;
    }

    /**
     * Gets the value of the apptRuleSet property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the apptRuleSet property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getApptRuleSet().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ApptRuleSetType }
     * 
     * 
     */
    public List<ApptRuleSetType> getApptRuleSet() {
        if (apptRuleSet == null) {
            apptRuleSet = new ArrayList<ApptRuleSetType>();
        }
        return this.apptRuleSet;
    }

}
