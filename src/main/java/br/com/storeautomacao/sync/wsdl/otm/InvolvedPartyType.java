
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.*;


/**
 * An InvolvedParty specifies a contact that is in some way involved with a TransOrder, Shipment, or Release.
 *             Either the ContactRef is required to be specified, or the InvolvedPartyLocationRef is required and the contact will be
 *             selected as the primary contact for the location.
 *             The LocationOverrideInfo element is only applicable when the InvolvedParty is in the Shipment.ShipmentHeader or the Release elements.
 *             The TransactionCode in the InvolvedParty is only supported in the Driver interface in 6.0.
 *          
 * 
 * <p>Classe Java de InvolvedPartyType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="InvolvedPartyType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TransactionCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransactionCodeType" minOccurs="0"/>
 *         &lt;element name="InvolvedPartyQualifierGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="InvolvedPartyLocationRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocRefType" minOccurs="0"/>
 *         &lt;element name="ContactRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ContactRefType" minOccurs="0"/>
 *         &lt;element name="ComMethodGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="LocationOverrideInfo" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LocationOverrideInfoType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InvolvedPartyType", propOrder = {
    "transactionCode",
    "involvedPartyQualifierGid",
    "involvedPartyLocationRef",
    "contactRef",
    "comMethodGid",
    "locationOverrideInfo"
})
public class InvolvedPartyType {

    @XmlElement(name = "TransactionCode")
    @XmlSchemaType(name = "string")
    protected TransactionCodeType transactionCode;
    @XmlElement(name = "InvolvedPartyQualifierGid", required = true)
    protected GLogXMLGidType involvedPartyQualifierGid;
    @XmlElement(name = "InvolvedPartyLocationRef")
    protected GLogXMLLocRefType involvedPartyLocationRef;
    @XmlElement(name = "ContactRef")
    protected ContactRefType contactRef;
    @XmlElement(name = "ComMethodGid")
    protected GLogXMLGidType comMethodGid;
    @XmlElement(name = "LocationOverrideInfo")
    protected LocationOverrideInfoType locationOverrideInfo;

    /**
     * Obtém o valor da propriedade transactionCode.
     * 
     * @return
     *     possible object is
     *     {@link TransactionCodeType }
     *     
     */
    public TransactionCodeType getTransactionCode() {
        return transactionCode;
    }

    /**
     * Define o valor da propriedade transactionCode.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionCodeType }
     *     
     */
    public void setTransactionCode(TransactionCodeType value) {
        this.transactionCode = value;
    }

    /**
     * Obtém o valor da propriedade involvedPartyQualifierGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getInvolvedPartyQualifierGid() {
        return involvedPartyQualifierGid;
    }

    /**
     * Define o valor da propriedade involvedPartyQualifierGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setInvolvedPartyQualifierGid(GLogXMLGidType value) {
        this.involvedPartyQualifierGid = value;
    }

    /**
     * Obtém o valor da propriedade involvedPartyLocationRef.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public GLogXMLLocRefType getInvolvedPartyLocationRef() {
        return involvedPartyLocationRef;
    }

    /**
     * Define o valor da propriedade involvedPartyLocationRef.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public void setInvolvedPartyLocationRef(GLogXMLLocRefType value) {
        this.involvedPartyLocationRef = value;
    }

    /**
     * Obtém o valor da propriedade contactRef.
     * 
     * @return
     *     possible object is
     *     {@link ContactRefType }
     *     
     */
    public ContactRefType getContactRef() {
        return contactRef;
    }

    /**
     * Define o valor da propriedade contactRef.
     * 
     * @param value
     *     allowed object is
     *     {@link ContactRefType }
     *     
     */
    public void setContactRef(ContactRefType value) {
        this.contactRef = value;
    }

    /**
     * Obtém o valor da propriedade comMethodGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getComMethodGid() {
        return comMethodGid;
    }

    /**
     * Define o valor da propriedade comMethodGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setComMethodGid(GLogXMLGidType value) {
        this.comMethodGid = value;
    }

    /**
     * Obtém o valor da propriedade locationOverrideInfo.
     * 
     * @return
     *     possible object is
     *     {@link LocationOverrideInfoType }
     *     
     */
    public LocationOverrideInfoType getLocationOverrideInfo() {
        return locationOverrideInfo;
    }

    /**
     * Define o valor da propriedade locationOverrideInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link LocationOverrideInfoType }
     *     
     */
    public void setLocationOverrideInfo(LocationOverrideInfoType value) {
        this.locationOverrideInfo = value;
    }

}
