
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de OTMTransactionOut complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="OTMTransactionOut">
 *   &lt;complexContent>
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLTransactionType">
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OTMTransactionOut")
@XmlSeeAlso({
    TenderOfferType.class,
    AccrualType.class,
    OBShipUnitType.class,
    BulkRatingType.class,
    DataQuerySummaryType.class,
    BulkContMoveType.class,
    RateOfferingType.class,
    DemurrageTransactionType.class,
    VoyageType.class,
    RateGeoType.class,
    BulkTrailerBuildType.class,
    FleetBulkPlanType.class,
    FinancialSystemFeedType.class,
    RemoteQueryReplyType.class,
    BulkPlanType.class,
    AllocationBaseType.class,
    VoucherType.class,
    OBLineType.class,
    BookingLineAmendmentType.class,
    PlannedShipmentType.class,
    BillingType.class,
    ServiceResponseType.class
})
public abstract class OTMTransactionOut
    extends GLogXMLTransactionType
{


}
