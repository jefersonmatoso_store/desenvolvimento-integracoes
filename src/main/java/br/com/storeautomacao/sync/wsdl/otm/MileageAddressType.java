
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * MilageAddress is a grouping of address fields similar to the normal address, but with the address lines removed.
 *             The RailSPLCGid and RailStationCodeGid elements are used in the RIQQuery interface.
 *          
 * 
 * <p>Classe Java de MileageAddressType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="MileageAddressType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="City" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ProvinceCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PostalCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CountryCode3Gid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="Zone1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Zone2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Zone3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Zone4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LocationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="GeoHierarchyGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="CountyQualifier" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RailSPLCGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="RailStationCodeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MileageAddressType", propOrder = {
    "city",
    "provinceCode",
    "postalCode",
    "countryCode3Gid",
    "zone1",
    "zone2",
    "zone3",
    "zone4",
    "locationGid",
    "geoHierarchyGid",
    "countyQualifier",
    "railSPLCGid",
    "railStationCodeGid"
})
public class MileageAddressType {

    @XmlElement(name = "City")
    protected String city;
    @XmlElement(name = "ProvinceCode")
    protected String provinceCode;
    @XmlElement(name = "PostalCode")
    protected String postalCode;
    @XmlElement(name = "CountryCode3Gid")
    protected GLogXMLGidType countryCode3Gid;
    @XmlElement(name = "Zone1")
    protected String zone1;
    @XmlElement(name = "Zone2")
    protected String zone2;
    @XmlElement(name = "Zone3")
    protected String zone3;
    @XmlElement(name = "Zone4")
    protected String zone4;
    @XmlElement(name = "LocationGid")
    protected GLogXMLGidType locationGid;
    @XmlElement(name = "GeoHierarchyGid")
    protected GLogXMLGidType geoHierarchyGid;
    @XmlElement(name = "CountyQualifier")
    protected String countyQualifier;
    @XmlElement(name = "RailSPLCGid")
    protected GLogXMLGidType railSPLCGid;
    @XmlElement(name = "RailStationCodeGid")
    protected GLogXMLGidType railStationCodeGid;

    /**
     * Obtém o valor da propriedade city.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCity() {
        return city;
    }

    /**
     * Define o valor da propriedade city.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCity(String value) {
        this.city = value;
    }

    /**
     * Obtém o valor da propriedade provinceCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProvinceCode() {
        return provinceCode;
    }

    /**
     * Define o valor da propriedade provinceCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProvinceCode(String value) {
        this.provinceCode = value;
    }

    /**
     * Obtém o valor da propriedade postalCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPostalCode() {
        return postalCode;
    }

    /**
     * Define o valor da propriedade postalCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPostalCode(String value) {
        this.postalCode = value;
    }

    /**
     * Obtém o valor da propriedade countryCode3Gid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getCountryCode3Gid() {
        return countryCode3Gid;
    }

    /**
     * Define o valor da propriedade countryCode3Gid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setCountryCode3Gid(GLogXMLGidType value) {
        this.countryCode3Gid = value;
    }

    /**
     * Obtém o valor da propriedade zone1.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getZone1() {
        return zone1;
    }

    /**
     * Define o valor da propriedade zone1.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setZone1(String value) {
        this.zone1 = value;
    }

    /**
     * Obtém o valor da propriedade zone2.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getZone2() {
        return zone2;
    }

    /**
     * Define o valor da propriedade zone2.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setZone2(String value) {
        this.zone2 = value;
    }

    /**
     * Obtém o valor da propriedade zone3.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getZone3() {
        return zone3;
    }

    /**
     * Define o valor da propriedade zone3.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setZone3(String value) {
        this.zone3 = value;
    }

    /**
     * Obtém o valor da propriedade zone4.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getZone4() {
        return zone4;
    }

    /**
     * Define o valor da propriedade zone4.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setZone4(String value) {
        this.zone4 = value;
    }

    /**
     * Obtém o valor da propriedade locationGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getLocationGid() {
        return locationGid;
    }

    /**
     * Define o valor da propriedade locationGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setLocationGid(GLogXMLGidType value) {
        this.locationGid = value;
    }

    /**
     * Obtém o valor da propriedade geoHierarchyGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getGeoHierarchyGid() {
        return geoHierarchyGid;
    }

    /**
     * Define o valor da propriedade geoHierarchyGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setGeoHierarchyGid(GLogXMLGidType value) {
        this.geoHierarchyGid = value;
    }

    /**
     * Obtém o valor da propriedade countyQualifier.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCountyQualifier() {
        return countyQualifier;
    }

    /**
     * Define o valor da propriedade countyQualifier.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCountyQualifier(String value) {
        this.countyQualifier = value;
    }

    /**
     * Obtém o valor da propriedade railSPLCGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRailSPLCGid() {
        return railSPLCGid;
    }

    /**
     * Define o valor da propriedade railSPLCGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRailSPLCGid(GLogXMLGidType value) {
        this.railSPLCGid = value;
    }

    /**
     * Obtém o valor da propriedade railStationCodeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRailStationCodeGid() {
        return railStationCodeGid;
    }

    /**
     * Define o valor da propriedade railStationCodeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRailStationCodeGid(GLogXMLGidType value) {
        this.railStationCodeGid = value;
    }

}
