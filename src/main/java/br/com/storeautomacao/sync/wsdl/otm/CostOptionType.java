
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 *             Specifies possible cost options saved with the quote.
 *          
 * 
 * <p>Classe Java de CostOptionType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="CostOptionType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TransCost" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType"/>
 *         &lt;element name="IsLate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsFeasible" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ItineraryGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="NonTransCost" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CostOptionType", propOrder = {
    "transCost",
    "isLate",
    "isFeasible",
    "itineraryGid",
    "nonTransCost"
})
public class CostOptionType {

    @XmlElement(name = "TransCost", required = true)
    protected GLogXMLFinancialAmountType transCost;
    @XmlElement(name = "IsLate")
    protected String isLate;
    @XmlElement(name = "IsFeasible")
    protected String isFeasible;
    @XmlElement(name = "ItineraryGid")
    protected GLogXMLGidType itineraryGid;
    @XmlElement(name = "NonTransCost")
    protected GLogXMLFinancialAmountType nonTransCost;

    /**
     * Obtém o valor da propriedade transCost.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getTransCost() {
        return transCost;
    }

    /**
     * Define o valor da propriedade transCost.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setTransCost(GLogXMLFinancialAmountType value) {
        this.transCost = value;
    }

    /**
     * Obtém o valor da propriedade isLate.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsLate() {
        return isLate;
    }

    /**
     * Define o valor da propriedade isLate.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsLate(String value) {
        this.isLate = value;
    }

    /**
     * Obtém o valor da propriedade isFeasible.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsFeasible() {
        return isFeasible;
    }

    /**
     * Define o valor da propriedade isFeasible.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsFeasible(String value) {
        this.isFeasible = value;
    }

    /**
     * Obtém o valor da propriedade itineraryGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getItineraryGid() {
        return itineraryGid;
    }

    /**
     * Define o valor da propriedade itineraryGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setItineraryGid(GLogXMLGidType value) {
        this.itineraryGid = value;
    }

    /**
     * Obtém o valor da propriedade nonTransCost.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getNonTransCost() {
        return nonTransCost;
    }

    /**
     * Define o valor da propriedade nonTransCost.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setNonTransCost(GLogXMLFinancialAmountType value) {
        this.nonTransCost = value;
    }

}
