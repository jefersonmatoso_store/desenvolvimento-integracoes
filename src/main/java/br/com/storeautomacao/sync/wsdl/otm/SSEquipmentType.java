
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 *             SSEquipment is a structure containing various equipment elements
 *          
 * 
 * <p>Classe Java de SSEquipmentType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="SSEquipmentType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="EquipmentPrefix" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EquipmentIdentificationNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EquipmentStatusCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ISOEquipmentTypeCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SSEquipmentType", propOrder = {
    "equipmentPrefix",
    "equipmentIdentificationNum",
    "equipmentStatusCode",
    "isoEquipmentTypeCode"
})
public class SSEquipmentType {

    @XmlElement(name = "EquipmentPrefix")
    protected String equipmentPrefix;
    @XmlElement(name = "EquipmentIdentificationNum")
    protected String equipmentIdentificationNum;
    @XmlElement(name = "EquipmentStatusCode")
    protected String equipmentStatusCode;
    @XmlElement(name = "ISOEquipmentTypeCode")
    protected String isoEquipmentTypeCode;

    /**
     * Obtém o valor da propriedade equipmentPrefix.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEquipmentPrefix() {
        return equipmentPrefix;
    }

    /**
     * Define o valor da propriedade equipmentPrefix.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEquipmentPrefix(String value) {
        this.equipmentPrefix = value;
    }

    /**
     * Obtém o valor da propriedade equipmentIdentificationNum.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEquipmentIdentificationNum() {
        return equipmentIdentificationNum;
    }

    /**
     * Define o valor da propriedade equipmentIdentificationNum.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEquipmentIdentificationNum(String value) {
        this.equipmentIdentificationNum = value;
    }

    /**
     * Obtém o valor da propriedade equipmentStatusCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEquipmentStatusCode() {
        return equipmentStatusCode;
    }

    /**
     * Define o valor da propriedade equipmentStatusCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEquipmentStatusCode(String value) {
        this.equipmentStatusCode = value;
    }

    /**
     * Obtém o valor da propriedade isoEquipmentTypeCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getISOEquipmentTypeCode() {
        return isoEquipmentTypeCode;
    }

    /**
     * Define o valor da propriedade isoEquipmentTypeCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setISOEquipmentTypeCode(String value) {
        this.isoEquipmentTypeCode = value;
    }

}
