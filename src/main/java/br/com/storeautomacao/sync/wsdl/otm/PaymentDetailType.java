
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * PaymentDetail is a component of ShipmentPayment.
 *             It details the charges which comprise the TotalPayment.
 *          
 * 
 * <p>Classe Java de PaymentDetailType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="PaymentDetailType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PaymentDetailNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PaymentDetailQualifierGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="PaymentDetailValue" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PaymentDetailType", propOrder = {
    "paymentDetailNumber",
    "paymentDetailQualifierGid",
    "paymentDetailValue"
})
public class PaymentDetailType {

    @XmlElement(name = "PaymentDetailNumber", required = true)
    protected String paymentDetailNumber;
    @XmlElement(name = "PaymentDetailQualifierGid", required = true)
    protected GLogXMLGidType paymentDetailQualifierGid;
    @XmlElement(name = "PaymentDetailValue", required = true)
    protected GLogXMLFinancialAmountType paymentDetailValue;

    /**
     * Obtém o valor da propriedade paymentDetailNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaymentDetailNumber() {
        return paymentDetailNumber;
    }

    /**
     * Define o valor da propriedade paymentDetailNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaymentDetailNumber(String value) {
        this.paymentDetailNumber = value;
    }

    /**
     * Obtém o valor da propriedade paymentDetailQualifierGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getPaymentDetailQualifierGid() {
        return paymentDetailQualifierGid;
    }

    /**
     * Define o valor da propriedade paymentDetailQualifierGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setPaymentDetailQualifierGid(GLogXMLGidType value) {
        this.paymentDetailQualifierGid = value;
    }

    /**
     * Obtém o valor da propriedade paymentDetailValue.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getPaymentDetailValue() {
        return paymentDetailValue;
    }

    /**
     * Define o valor da propriedade paymentDetailValue.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setPaymentDetailValue(GLogXMLFinancialAmountType value) {
        this.paymentDetailValue = value;
    }

}
