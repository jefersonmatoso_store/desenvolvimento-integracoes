
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * (Both) Product classification code.
 * 
 *             Deprecated (6.4.3): TradeDirection. Functionality removed. Value will be ignored and field will be removed in a future release.
 *          
 * 
 * <p>Classe Java de ProductClassificationType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="ProductClassificationType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ClassificationCategory" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="GtmProdClassTypeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="ClassificationCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="TradeDirection" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Attribute" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}AttributeType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Note" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}ProdClassCodeNoteType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="UOM" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}ProdClassCodeUOMType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="ClassificationDescription" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}ProdClassCodeDescType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProductClassificationType", namespace = "http://xmlns.oracle.com/apps/gtm/transmission/v6.4", propOrder = {
    "classificationCategory",
    "gtmProdClassTypeGid",
    "classificationCode",
    "tradeDirection",
    "attribute",
    "note",
    "uom",
    "classificationDescription"
})
public class ProductClassificationType {

    @XmlElement(name = "ClassificationCategory")
    protected String classificationCategory;
    @XmlElement(name = "GtmProdClassTypeGid", required = true)
    protected GLogXMLGidType gtmProdClassTypeGid;
    @XmlElement(name = "ClassificationCode", required = true)
    protected String classificationCode;
    @XmlElement(name = "TradeDirection", required = true)
    protected String tradeDirection;
    @XmlElement(name = "Attribute")
    protected List<AttributeType> attribute;
    @XmlElement(name = "Note")
    protected List<ProdClassCodeNoteType> note;
    @XmlElement(name = "UOM")
    protected List<ProdClassCodeUOMType> uom;
    @XmlElement(name = "ClassificationDescription")
    protected List<ProdClassCodeDescType> classificationDescription;

    /**
     * Obtém o valor da propriedade classificationCategory.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClassificationCategory() {
        return classificationCategory;
    }

    /**
     * Define o valor da propriedade classificationCategory.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClassificationCategory(String value) {
        this.classificationCategory = value;
    }

    /**
     * Obtém o valor da propriedade gtmProdClassTypeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getGtmProdClassTypeGid() {
        return gtmProdClassTypeGid;
    }

    /**
     * Define o valor da propriedade gtmProdClassTypeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setGtmProdClassTypeGid(GLogXMLGidType value) {
        this.gtmProdClassTypeGid = value;
    }

    /**
     * Obtém o valor da propriedade classificationCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClassificationCode() {
        return classificationCode;
    }

    /**
     * Define o valor da propriedade classificationCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClassificationCode(String value) {
        this.classificationCode = value;
    }

    /**
     * Obtém o valor da propriedade tradeDirection.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTradeDirection() {
        return tradeDirection;
    }

    /**
     * Define o valor da propriedade tradeDirection.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTradeDirection(String value) {
        this.tradeDirection = value;
    }

    /**
     * Gets the value of the attribute property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the attribute property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAttribute().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AttributeType }
     * 
     * 
     */
    public List<AttributeType> getAttribute() {
        if (attribute == null) {
            attribute = new ArrayList<AttributeType>();
        }
        return this.attribute;
    }

    /**
     * Gets the value of the note property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the note property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getNote().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProdClassCodeNoteType }
     * 
     * 
     */
    public List<ProdClassCodeNoteType> getNote() {
        if (note == null) {
            note = new ArrayList<ProdClassCodeNoteType>();
        }
        return this.note;
    }

    /**
     * Gets the value of the uom property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the uom property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getUOM().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProdClassCodeUOMType }
     * 
     * 
     */
    public List<ProdClassCodeUOMType> getUOM() {
        if (uom == null) {
            uom = new ArrayList<ProdClassCodeUOMType>();
        }
        return this.uom;
    }

    /**
     * Gets the value of the classificationDescription property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the classificationDescription property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getClassificationDescription().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProdClassCodeDescType }
     * 
     * 
     */
    public List<ProdClassCodeDescType> getClassificationDescription() {
        if (classificationDescription == null) {
            classificationDescription = new ArrayList<ProdClassCodeDescType>();
        }
        return this.classificationDescription;
    }

}
