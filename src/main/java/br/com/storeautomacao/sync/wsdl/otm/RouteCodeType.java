
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * A Route Code is used to specify a route where the path/route that the shipment will
 *             take is pre-defined and is part of the lane level offering. For example, a rail
 *             carrier may have several options to move freight from Philadelphia to Chicago.
 *             The various options correspond to various train schedules available as well as the
 *             junctions and carriers that might be involved in the route.
 *          
 * 
 * <p>Classe Java de RouteCodeType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="RouteCodeType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RouteCodeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="RouteCodeDesc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OriginStation" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLStationType" minOccurs="0"/>
 *         &lt;element name="DestinationStation" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLStationType" minOccurs="0"/>
 *         &lt;element name="RailReturnLocationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocGidType" minOccurs="0"/>
 *         &lt;element name="RailReturnRouteGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RailReturnRouteGidType" minOccurs="0"/>
 *         &lt;element name="BorderCrossingLocationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocGidType" minOccurs="0"/>
 *         &lt;element name="RouteCodeDetail" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RouteCodeDetailType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RouteCodeType", propOrder = {
    "routeCodeGid",
    "routeCodeDesc",
    "originStation",
    "destinationStation",
    "railReturnLocationGid",
    "railReturnRouteGid",
    "borderCrossingLocationGid",
    "routeCodeDetail"
})
public class RouteCodeType {

    @XmlElement(name = "RouteCodeGid", required = true)
    protected GLogXMLGidType routeCodeGid;
    @XmlElement(name = "RouteCodeDesc")
    protected String routeCodeDesc;
    @XmlElement(name = "OriginStation")
    protected GLogXMLStationType originStation;
    @XmlElement(name = "DestinationStation")
    protected GLogXMLStationType destinationStation;
    @XmlElement(name = "RailReturnLocationGid")
    protected GLogXMLLocGidType railReturnLocationGid;
    @XmlElement(name = "RailReturnRouteGid")
    protected RailReturnRouteGidType railReturnRouteGid;
    @XmlElement(name = "BorderCrossingLocationGid")
    protected GLogXMLLocGidType borderCrossingLocationGid;
    @XmlElement(name = "RouteCodeDetail")
    protected List<RouteCodeDetailType> routeCodeDetail;

    /**
     * Obtém o valor da propriedade routeCodeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRouteCodeGid() {
        return routeCodeGid;
    }

    /**
     * Define o valor da propriedade routeCodeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRouteCodeGid(GLogXMLGidType value) {
        this.routeCodeGid = value;
    }

    /**
     * Obtém o valor da propriedade routeCodeDesc.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRouteCodeDesc() {
        return routeCodeDesc;
    }

    /**
     * Define o valor da propriedade routeCodeDesc.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRouteCodeDesc(String value) {
        this.routeCodeDesc = value;
    }

    /**
     * Obtém o valor da propriedade originStation.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLStationType }
     *     
     */
    public GLogXMLStationType getOriginStation() {
        return originStation;
    }

    /**
     * Define o valor da propriedade originStation.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLStationType }
     *     
     */
    public void setOriginStation(GLogXMLStationType value) {
        this.originStation = value;
    }

    /**
     * Obtém o valor da propriedade destinationStation.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLStationType }
     *     
     */
    public GLogXMLStationType getDestinationStation() {
        return destinationStation;
    }

    /**
     * Define o valor da propriedade destinationStation.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLStationType }
     *     
     */
    public void setDestinationStation(GLogXMLStationType value) {
        this.destinationStation = value;
    }

    /**
     * Obtém o valor da propriedade railReturnLocationGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocGidType }
     *     
     */
    public GLogXMLLocGidType getRailReturnLocationGid() {
        return railReturnLocationGid;
    }

    /**
     * Define o valor da propriedade railReturnLocationGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocGidType }
     *     
     */
    public void setRailReturnLocationGid(GLogXMLLocGidType value) {
        this.railReturnLocationGid = value;
    }

    /**
     * Obtém o valor da propriedade railReturnRouteGid.
     * 
     * @return
     *     possible object is
     *     {@link RailReturnRouteGidType }
     *     
     */
    public RailReturnRouteGidType getRailReturnRouteGid() {
        return railReturnRouteGid;
    }

    /**
     * Define o valor da propriedade railReturnRouteGid.
     * 
     * @param value
     *     allowed object is
     *     {@link RailReturnRouteGidType }
     *     
     */
    public void setRailReturnRouteGid(RailReturnRouteGidType value) {
        this.railReturnRouteGid = value;
    }

    /**
     * Obtém o valor da propriedade borderCrossingLocationGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocGidType }
     *     
     */
    public GLogXMLLocGidType getBorderCrossingLocationGid() {
        return borderCrossingLocationGid;
    }

    /**
     * Define o valor da propriedade borderCrossingLocationGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocGidType }
     *     
     */
    public void setBorderCrossingLocationGid(GLogXMLLocGidType value) {
        this.borderCrossingLocationGid = value;
    }

    /**
     * Gets the value of the routeCodeDetail property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the routeCodeDetail property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRouteCodeDetail().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RouteCodeDetailType }
     * 
     * 
     */
    public List<RouteCodeDetailType> getRouteCodeDetail() {
        if (routeCodeDetail == null) {
            routeCodeDetail = new ArrayList<RouteCodeDetailType>();
        }
        return this.routeCodeDetail;
    }

}
