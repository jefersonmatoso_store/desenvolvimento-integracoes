
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de GLogXMLLocOverrideRefType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="GLogXMLLocOverrideRefType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="LocationOverrideRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LocationOverrideRefType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GLogXMLLocOverrideRefType", propOrder = {
    "locationOverrideRef"
})
public class GLogXMLLocOverrideRefType {

    @XmlElement(name = "LocationOverrideRef", required = true)
    protected LocationOverrideRefType locationOverrideRef;

    /**
     * Obtém o valor da propriedade locationOverrideRef.
     * 
     * @return
     *     possible object is
     *     {@link LocationOverrideRefType }
     *     
     */
    public LocationOverrideRefType getLocationOverrideRef() {
        return locationOverrideRef;
    }

    /**
     * Define o valor da propriedade locationOverrideRef.
     * 
     * @param value
     *     allowed object is
     *     {@link LocationOverrideRefType }
     *     
     */
    public void setLocationOverrideRef(LocationOverrideRefType value) {
        this.locationOverrideRef = value;
    }

}
