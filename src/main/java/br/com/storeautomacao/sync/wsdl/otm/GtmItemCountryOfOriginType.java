
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Holds country of origin information of the item.
 * 
 * <p>Classe Java de GtmItemCountryOfOriginType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="GtmItemCountryOfOriginType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SupplierSiteGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="CountryOfOriginGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="ManufacturingCountryGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="IsDefault" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ManufacturingPartyNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ManufacturingDesc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GtmItemCountryOfOriginType", propOrder = {
    "supplierSiteGid",
    "countryOfOriginGid",
    "manufacturingCountryGid",
    "isDefault",
    "manufacturingPartyNo",
    "manufacturingDesc"
})
public class GtmItemCountryOfOriginType {

    @XmlElement(name = "SupplierSiteGid", required = true)
    protected GLogXMLGidType supplierSiteGid;
    @XmlElement(name = "CountryOfOriginGid")
    protected GLogXMLGidType countryOfOriginGid;
    @XmlElement(name = "ManufacturingCountryGid")
    protected GLogXMLGidType manufacturingCountryGid;
    @XmlElement(name = "IsDefault")
    protected String isDefault;
    @XmlElement(name = "ManufacturingPartyNo")
    protected String manufacturingPartyNo;
    @XmlElement(name = "ManufacturingDesc")
    protected String manufacturingDesc;

    /**
     * Obtém o valor da propriedade supplierSiteGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getSupplierSiteGid() {
        return supplierSiteGid;
    }

    /**
     * Define o valor da propriedade supplierSiteGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setSupplierSiteGid(GLogXMLGidType value) {
        this.supplierSiteGid = value;
    }

    /**
     * Obtém o valor da propriedade countryOfOriginGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getCountryOfOriginGid() {
        return countryOfOriginGid;
    }

    /**
     * Define o valor da propriedade countryOfOriginGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setCountryOfOriginGid(GLogXMLGidType value) {
        this.countryOfOriginGid = value;
    }

    /**
     * Obtém o valor da propriedade manufacturingCountryGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getManufacturingCountryGid() {
        return manufacturingCountryGid;
    }

    /**
     * Define o valor da propriedade manufacturingCountryGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setManufacturingCountryGid(GLogXMLGidType value) {
        this.manufacturingCountryGid = value;
    }

    /**
     * Obtém o valor da propriedade isDefault.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsDefault() {
        return isDefault;
    }

    /**
     * Define o valor da propriedade isDefault.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsDefault(String value) {
        this.isDefault = value;
    }

    /**
     * Obtém o valor da propriedade manufacturingPartyNo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getManufacturingPartyNo() {
        return manufacturingPartyNo;
    }

    /**
     * Define o valor da propriedade manufacturingPartyNo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setManufacturingPartyNo(String value) {
        this.manufacturingPartyNo = value;
    }

    /**
     * Obtém o valor da propriedade manufacturingDesc.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getManufacturingDesc() {
        return manufacturingDesc;
    }

    /**
     * Define o valor da propriedade manufacturingDesc.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setManufacturingDesc(String value) {
        this.manufacturingDesc = value;
    }

}
