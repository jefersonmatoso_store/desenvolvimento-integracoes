
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 *             Specifies the cost associated with a claim.
 *          
 * 
 * <p>Classe Java de ClaimCostType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="ClaimCostType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SequenceNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Cost" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType"/>
 *         &lt;element name="ClaimCostTypeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="PayorContact" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLContactRefType" minOccurs="0"/>
 *         &lt;element name="PayeeContact" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLContactRefType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ClaimCostType", propOrder = {
    "sequenceNumber",
    "cost",
    "claimCostTypeGid",
    "payorContact",
    "payeeContact"
})
public class ClaimCostType {

    @XmlElement(name = "SequenceNumber")
    protected String sequenceNumber;
    @XmlElement(name = "Cost", required = true)
    protected GLogXMLFinancialAmountType cost;
    @XmlElement(name = "ClaimCostTypeGid", required = true)
    protected GLogXMLGidType claimCostTypeGid;
    @XmlElement(name = "PayorContact")
    protected GLogXMLContactRefType payorContact;
    @XmlElement(name = "PayeeContact")
    protected GLogXMLContactRefType payeeContact;

    /**
     * Obtém o valor da propriedade sequenceNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSequenceNumber() {
        return sequenceNumber;
    }

    /**
     * Define o valor da propriedade sequenceNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSequenceNumber(String value) {
        this.sequenceNumber = value;
    }

    /**
     * Obtém o valor da propriedade cost.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getCost() {
        return cost;
    }

    /**
     * Define o valor da propriedade cost.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setCost(GLogXMLFinancialAmountType value) {
        this.cost = value;
    }

    /**
     * Obtém o valor da propriedade claimCostTypeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getClaimCostTypeGid() {
        return claimCostTypeGid;
    }

    /**
     * Define o valor da propriedade claimCostTypeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setClaimCostTypeGid(GLogXMLGidType value) {
        this.claimCostTypeGid = value;
    }

    /**
     * Obtém o valor da propriedade payorContact.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLContactRefType }
     *     
     */
    public GLogXMLContactRefType getPayorContact() {
        return payorContact;
    }

    /**
     * Define o valor da propriedade payorContact.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLContactRefType }
     *     
     */
    public void setPayorContact(GLogXMLContactRefType value) {
        this.payorContact = value;
    }

    /**
     * Obtém o valor da propriedade payeeContact.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLContactRefType }
     *     
     */
    public GLogXMLContactRefType getPayeeContact() {
        return payeeContact;
    }

    /**
     * Define o valor da propriedade payeeContact.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLContactRefType }
     *     
     */
    public void setPayeeContact(GLogXMLContactRefType value) {
        this.payeeContact = value;
    }

}
