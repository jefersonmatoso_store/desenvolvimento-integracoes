
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 *             Specifies the time window in the number of seconds after midnight of day 1 of the week.
 *          
 * 
 * <p>Classe Java de TimeWindowSecType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="TimeWindowSecType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="EarlyDepartureTimeSec" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LateDepartureTimeSec" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EarlyArrivalTimeSec" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LateArrivalTimeSec" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TimeWindowSecType", propOrder = {
    "earlyDepartureTimeSec",
    "lateDepartureTimeSec",
    "earlyArrivalTimeSec",
    "lateArrivalTimeSec"
})
public class TimeWindowSecType {

    @XmlElement(name = "EarlyDepartureTimeSec")
    protected String earlyDepartureTimeSec;
    @XmlElement(name = "LateDepartureTimeSec")
    protected String lateDepartureTimeSec;
    @XmlElement(name = "EarlyArrivalTimeSec")
    protected String earlyArrivalTimeSec;
    @XmlElement(name = "LateArrivalTimeSec")
    protected String lateArrivalTimeSec;

    /**
     * Obtém o valor da propriedade earlyDepartureTimeSec.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEarlyDepartureTimeSec() {
        return earlyDepartureTimeSec;
    }

    /**
     * Define o valor da propriedade earlyDepartureTimeSec.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEarlyDepartureTimeSec(String value) {
        this.earlyDepartureTimeSec = value;
    }

    /**
     * Obtém o valor da propriedade lateDepartureTimeSec.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLateDepartureTimeSec() {
        return lateDepartureTimeSec;
    }

    /**
     * Define o valor da propriedade lateDepartureTimeSec.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLateDepartureTimeSec(String value) {
        this.lateDepartureTimeSec = value;
    }

    /**
     * Obtém o valor da propriedade earlyArrivalTimeSec.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEarlyArrivalTimeSec() {
        return earlyArrivalTimeSec;
    }

    /**
     * Define o valor da propriedade earlyArrivalTimeSec.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEarlyArrivalTimeSec(String value) {
        this.earlyArrivalTimeSec = value;
    }

    /**
     * Obtém o valor da propriedade lateArrivalTimeSec.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLateArrivalTimeSec() {
        return lateArrivalTimeSec;
    }

    /**
     * Define o valor da propriedade lateArrivalTimeSec.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLateArrivalTimeSec(String value) {
        this.lateArrivalTimeSec = value;
    }

}
