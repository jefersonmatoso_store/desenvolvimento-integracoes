
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Specifies an Appt Rule Set defined for a location resource. This is for outbound xml.
 * 
 * <p>Classe Java de ApptRuleSetType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="ApptRuleSetType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ApptRuleSetGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="ApptRuleSetName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="EffectiveDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType"/>
 *         &lt;element name="ExpiryDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType"/>
 *         &lt;element name="FlexCommodityProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="EquipmentGroupProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="ModeProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="ServiceProviderProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="ContactProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="FlexCommodityCheckOption" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsTemperatureControl" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsHazardous" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SourceRegionGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLRegionGidType" minOccurs="0"/>
 *         &lt;element name="DestinationRegionGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLRegionGidType" minOccurs="0"/>
 *         &lt;element name="ApptSsActivityProfGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="ApptActivityType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ApptRuleSetType", propOrder = {
    "apptRuleSetGid",
    "apptRuleSetName",
    "effectiveDt",
    "expiryDt",
    "flexCommodityProfileGid",
    "equipmentGroupProfileGid",
    "modeProfileGid",
    "serviceProviderProfileGid",
    "contactProfileGid",
    "flexCommodityCheckOption",
    "isTemperatureControl",
    "isHazardous",
    "sourceRegionGid",
    "destinationRegionGid",
    "apptSsActivityProfGid",
    "apptActivityType"
})
public class ApptRuleSetType {

    @XmlElement(name = "ApptRuleSetGid")
    protected GLogXMLGidType apptRuleSetGid;
    @XmlElement(name = "ApptRuleSetName", required = true)
    protected String apptRuleSetName;
    @XmlElement(name = "EffectiveDt", required = true)
    protected GLogDateTimeType effectiveDt;
    @XmlElement(name = "ExpiryDt", required = true)
    protected GLogDateTimeType expiryDt;
    @XmlElement(name = "FlexCommodityProfileGid")
    protected GLogXMLGidType flexCommodityProfileGid;
    @XmlElement(name = "EquipmentGroupProfileGid")
    protected GLogXMLGidType equipmentGroupProfileGid;
    @XmlElement(name = "ModeProfileGid")
    protected GLogXMLGidType modeProfileGid;
    @XmlElement(name = "ServiceProviderProfileGid")
    protected GLogXMLGidType serviceProviderProfileGid;
    @XmlElement(name = "ContactProfileGid")
    protected GLogXMLGidType contactProfileGid;
    @XmlElement(name = "FlexCommodityCheckOption")
    protected String flexCommodityCheckOption;
    @XmlElement(name = "IsTemperatureControl")
    protected String isTemperatureControl;
    @XmlElement(name = "IsHazardous")
    protected String isHazardous;
    @XmlElement(name = "SourceRegionGid")
    protected GLogXMLRegionGidType sourceRegionGid;
    @XmlElement(name = "DestinationRegionGid")
    protected GLogXMLRegionGidType destinationRegionGid;
    @XmlElement(name = "ApptSsActivityProfGid")
    protected GLogXMLGidType apptSsActivityProfGid;
    @XmlElement(name = "ApptActivityType")
    protected String apptActivityType;

    /**
     * Obtém o valor da propriedade apptRuleSetGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getApptRuleSetGid() {
        return apptRuleSetGid;
    }

    /**
     * Define o valor da propriedade apptRuleSetGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setApptRuleSetGid(GLogXMLGidType value) {
        this.apptRuleSetGid = value;
    }

    /**
     * Obtém o valor da propriedade apptRuleSetName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApptRuleSetName() {
        return apptRuleSetName;
    }

    /**
     * Define o valor da propriedade apptRuleSetName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApptRuleSetName(String value) {
        this.apptRuleSetName = value;
    }

    /**
     * Obtém o valor da propriedade effectiveDt.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getEffectiveDt() {
        return effectiveDt;
    }

    /**
     * Define o valor da propriedade effectiveDt.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setEffectiveDt(GLogDateTimeType value) {
        this.effectiveDt = value;
    }

    /**
     * Obtém o valor da propriedade expiryDt.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getExpiryDt() {
        return expiryDt;
    }

    /**
     * Define o valor da propriedade expiryDt.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setExpiryDt(GLogDateTimeType value) {
        this.expiryDt = value;
    }

    /**
     * Obtém o valor da propriedade flexCommodityProfileGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getFlexCommodityProfileGid() {
        return flexCommodityProfileGid;
    }

    /**
     * Define o valor da propriedade flexCommodityProfileGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setFlexCommodityProfileGid(GLogXMLGidType value) {
        this.flexCommodityProfileGid = value;
    }

    /**
     * Obtém o valor da propriedade equipmentGroupProfileGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getEquipmentGroupProfileGid() {
        return equipmentGroupProfileGid;
    }

    /**
     * Define o valor da propriedade equipmentGroupProfileGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setEquipmentGroupProfileGid(GLogXMLGidType value) {
        this.equipmentGroupProfileGid = value;
    }

    /**
     * Obtém o valor da propriedade modeProfileGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getModeProfileGid() {
        return modeProfileGid;
    }

    /**
     * Define o valor da propriedade modeProfileGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setModeProfileGid(GLogXMLGidType value) {
        this.modeProfileGid = value;
    }

    /**
     * Obtém o valor da propriedade serviceProviderProfileGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getServiceProviderProfileGid() {
        return serviceProviderProfileGid;
    }

    /**
     * Define o valor da propriedade serviceProviderProfileGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setServiceProviderProfileGid(GLogXMLGidType value) {
        this.serviceProviderProfileGid = value;
    }

    /**
     * Obtém o valor da propriedade contactProfileGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getContactProfileGid() {
        return contactProfileGid;
    }

    /**
     * Define o valor da propriedade contactProfileGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setContactProfileGid(GLogXMLGidType value) {
        this.contactProfileGid = value;
    }

    /**
     * Obtém o valor da propriedade flexCommodityCheckOption.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlexCommodityCheckOption() {
        return flexCommodityCheckOption;
    }

    /**
     * Define o valor da propriedade flexCommodityCheckOption.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlexCommodityCheckOption(String value) {
        this.flexCommodityCheckOption = value;
    }

    /**
     * Obtém o valor da propriedade isTemperatureControl.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsTemperatureControl() {
        return isTemperatureControl;
    }

    /**
     * Define o valor da propriedade isTemperatureControl.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsTemperatureControl(String value) {
        this.isTemperatureControl = value;
    }

    /**
     * Obtém o valor da propriedade isHazardous.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsHazardous() {
        return isHazardous;
    }

    /**
     * Define o valor da propriedade isHazardous.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsHazardous(String value) {
        this.isHazardous = value;
    }

    /**
     * Obtém o valor da propriedade sourceRegionGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLRegionGidType }
     *     
     */
    public GLogXMLRegionGidType getSourceRegionGid() {
        return sourceRegionGid;
    }

    /**
     * Define o valor da propriedade sourceRegionGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLRegionGidType }
     *     
     */
    public void setSourceRegionGid(GLogXMLRegionGidType value) {
        this.sourceRegionGid = value;
    }

    /**
     * Obtém o valor da propriedade destinationRegionGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLRegionGidType }
     *     
     */
    public GLogXMLRegionGidType getDestinationRegionGid() {
        return destinationRegionGid;
    }

    /**
     * Define o valor da propriedade destinationRegionGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLRegionGidType }
     *     
     */
    public void setDestinationRegionGid(GLogXMLRegionGidType value) {
        this.destinationRegionGid = value;
    }

    /**
     * Obtém o valor da propriedade apptSsActivityProfGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getApptSsActivityProfGid() {
        return apptSsActivityProfGid;
    }

    /**
     * Define o valor da propriedade apptSsActivityProfGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setApptSsActivityProfGid(GLogXMLGidType value) {
        this.apptSsActivityProfGid = value;
    }

    /**
     * Obtém o valor da propriedade apptActivityType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApptActivityType() {
        return apptActivityType;
    }

    /**
     * Define o valor da propriedade apptActivityType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApptActivityType(String value) {
        this.apptActivityType = value;
    }

}
