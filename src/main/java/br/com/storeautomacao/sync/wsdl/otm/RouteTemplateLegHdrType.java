
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 *             The RouteTemplateLegHdr contains general information about the route template leg.
 *             Note: When creating the RouteTemplateLeg, the StartRegionGid and EndRegionGid are required.
 *          
 * 
 * <p>Classe Java de RouteTemplateLegHdrType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="RouteTemplateLegHdrType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SequenceNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="EquipmentGroupProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="StartRegionGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="StartDistThreshold" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDistanceType" minOccurs="0"/>
 *         &lt;element name="EndRegionGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="EndDistThreshold" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDistanceType" minOccurs="0"/>
 *         &lt;element name="TimeWindowSec" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TimeWindowSecType" minOccurs="0"/>
 *         &lt;element name="TimePhasedPlanThreshold" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDurationType" minOccurs="0"/>
 *         &lt;element name="MinTimeToNextLeg" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDurationType" minOccurs="0"/>
 *         &lt;element name="MaxTimeToNextLeg" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDurationType" minOccurs="0"/>
 *         &lt;element name="MatchShipToLeg" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="WarnForMissingLeg" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DomainProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RouteTemplateLegHdrType", propOrder = {
    "sequenceNumber",
    "equipmentGroupProfileGid",
    "startRegionGid",
    "startDistThreshold",
    "endRegionGid",
    "endDistThreshold",
    "timeWindowSec",
    "timePhasedPlanThreshold",
    "minTimeToNextLeg",
    "maxTimeToNextLeg",
    "matchShipToLeg",
    "warnForMissingLeg",
    "domainProfileGid"
})
public class RouteTemplateLegHdrType {

    @XmlElement(name = "SequenceNumber", required = true)
    protected String sequenceNumber;
    @XmlElement(name = "EquipmentGroupProfileGid")
    protected GLogXMLGidType equipmentGroupProfileGid;
    @XmlElement(name = "StartRegionGid")
    protected GLogXMLGidType startRegionGid;
    @XmlElement(name = "StartDistThreshold")
    protected GLogXMLDistanceType startDistThreshold;
    @XmlElement(name = "EndRegionGid")
    protected GLogXMLGidType endRegionGid;
    @XmlElement(name = "EndDistThreshold")
    protected GLogXMLDistanceType endDistThreshold;
    @XmlElement(name = "TimeWindowSec")
    protected TimeWindowSecType timeWindowSec;
    @XmlElement(name = "TimePhasedPlanThreshold")
    protected GLogXMLDurationType timePhasedPlanThreshold;
    @XmlElement(name = "MinTimeToNextLeg")
    protected GLogXMLDurationType minTimeToNextLeg;
    @XmlElement(name = "MaxTimeToNextLeg")
    protected GLogXMLDurationType maxTimeToNextLeg;
    @XmlElement(name = "MatchShipToLeg")
    protected String matchShipToLeg;
    @XmlElement(name = "WarnForMissingLeg")
    protected String warnForMissingLeg;
    @XmlElement(name = "DomainProfileGid")
    protected GLogXMLGidType domainProfileGid;

    /**
     * Obtém o valor da propriedade sequenceNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSequenceNumber() {
        return sequenceNumber;
    }

    /**
     * Define o valor da propriedade sequenceNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSequenceNumber(String value) {
        this.sequenceNumber = value;
    }

    /**
     * Obtém o valor da propriedade equipmentGroupProfileGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getEquipmentGroupProfileGid() {
        return equipmentGroupProfileGid;
    }

    /**
     * Define o valor da propriedade equipmentGroupProfileGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setEquipmentGroupProfileGid(GLogXMLGidType value) {
        this.equipmentGroupProfileGid = value;
    }

    /**
     * Obtém o valor da propriedade startRegionGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getStartRegionGid() {
        return startRegionGid;
    }

    /**
     * Define o valor da propriedade startRegionGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setStartRegionGid(GLogXMLGidType value) {
        this.startRegionGid = value;
    }

    /**
     * Obtém o valor da propriedade startDistThreshold.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDistanceType }
     *     
     */
    public GLogXMLDistanceType getStartDistThreshold() {
        return startDistThreshold;
    }

    /**
     * Define o valor da propriedade startDistThreshold.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDistanceType }
     *     
     */
    public void setStartDistThreshold(GLogXMLDistanceType value) {
        this.startDistThreshold = value;
    }

    /**
     * Obtém o valor da propriedade endRegionGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getEndRegionGid() {
        return endRegionGid;
    }

    /**
     * Define o valor da propriedade endRegionGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setEndRegionGid(GLogXMLGidType value) {
        this.endRegionGid = value;
    }

    /**
     * Obtém o valor da propriedade endDistThreshold.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDistanceType }
     *     
     */
    public GLogXMLDistanceType getEndDistThreshold() {
        return endDistThreshold;
    }

    /**
     * Define o valor da propriedade endDistThreshold.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDistanceType }
     *     
     */
    public void setEndDistThreshold(GLogXMLDistanceType value) {
        this.endDistThreshold = value;
    }

    /**
     * Obtém o valor da propriedade timeWindowSec.
     * 
     * @return
     *     possible object is
     *     {@link TimeWindowSecType }
     *     
     */
    public TimeWindowSecType getTimeWindowSec() {
        return timeWindowSec;
    }

    /**
     * Define o valor da propriedade timeWindowSec.
     * 
     * @param value
     *     allowed object is
     *     {@link TimeWindowSecType }
     *     
     */
    public void setTimeWindowSec(TimeWindowSecType value) {
        this.timeWindowSec = value;
    }

    /**
     * Obtém o valor da propriedade timePhasedPlanThreshold.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public GLogXMLDurationType getTimePhasedPlanThreshold() {
        return timePhasedPlanThreshold;
    }

    /**
     * Define o valor da propriedade timePhasedPlanThreshold.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public void setTimePhasedPlanThreshold(GLogXMLDurationType value) {
        this.timePhasedPlanThreshold = value;
    }

    /**
     * Obtém o valor da propriedade minTimeToNextLeg.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public GLogXMLDurationType getMinTimeToNextLeg() {
        return minTimeToNextLeg;
    }

    /**
     * Define o valor da propriedade minTimeToNextLeg.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public void setMinTimeToNextLeg(GLogXMLDurationType value) {
        this.minTimeToNextLeg = value;
    }

    /**
     * Obtém o valor da propriedade maxTimeToNextLeg.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public GLogXMLDurationType getMaxTimeToNextLeg() {
        return maxTimeToNextLeg;
    }

    /**
     * Define o valor da propriedade maxTimeToNextLeg.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public void setMaxTimeToNextLeg(GLogXMLDurationType value) {
        this.maxTimeToNextLeg = value;
    }

    /**
     * Obtém o valor da propriedade matchShipToLeg.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMatchShipToLeg() {
        return matchShipToLeg;
    }

    /**
     * Define o valor da propriedade matchShipToLeg.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMatchShipToLeg(String value) {
        this.matchShipToLeg = value;
    }

    /**
     * Obtém o valor da propriedade warnForMissingLeg.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWarnForMissingLeg() {
        return warnForMissingLeg;
    }

    /**
     * Define o valor da propriedade warnForMissingLeg.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWarnForMissingLeg(String value) {
        this.warnForMissingLeg = value;
    }

    /**
     * Obtém o valor da propriedade domainProfileGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getDomainProfileGid() {
        return domainProfileGid;
    }

    /**
     * Define o valor da propriedade domainProfileGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setDomainProfileGid(GLogXMLGidType value) {
        this.domainProfileGid = value;
    }

}
