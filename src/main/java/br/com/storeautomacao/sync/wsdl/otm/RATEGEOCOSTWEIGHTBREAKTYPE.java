
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Classe Java de RATE_GEO_COST_WEIGHT_BREAK_TYPE complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="RATE_GEO_COST_WEIGHT_BREAK_TYPE">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence maxOccurs="unbounded" minOccurs="0">
 *         &lt;element name="RATE_GEO_COST_WEIGHT_BREAK_ROW" maxOccurs="unbounded">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="RATE_GEO_COST_SEQ" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="WEIGHT_BREAK_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="RATE_DISCOUNT_VALUE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="RATE_DISCOUNT_VALUE_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="RATE_DISCOUNT_VALUE_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="RATE_GEO_COST_GROUP_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                 &lt;/sequence>
 *                 &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" />
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RATE_GEO_COST_WEIGHT_BREAK_TYPE", propOrder = {
    "rategeocostweightbreakrow"
})
public class RATEGEOCOSTWEIGHTBREAKTYPE {

    @XmlElement(name = "RATE_GEO_COST_WEIGHT_BREAK_ROW")
    protected List<RATEGEOCOSTWEIGHTBREAKROW> rategeocostweightbreakrow;

    /**
     * Gets the value of the rategeocostweightbreakrow property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the rategeocostweightbreakrow property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRATEGEOCOSTWEIGHTBREAKROW().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RATEGEOCOSTWEIGHTBREAKROW }
     * 
     * 
     */
    public List<RATEGEOCOSTWEIGHTBREAKROW> getRATEGEOCOSTWEIGHTBREAKROW() {
        if (rategeocostweightbreakrow == null) {
            rategeocostweightbreakrow = new ArrayList<RATEGEOCOSTWEIGHTBREAKROW>();
        }
        return this.rategeocostweightbreakrow;
    }


    /**
     * <p>Classe Java de anonymous complex type.
     * 
     * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="RATE_GEO_COST_SEQ" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="WEIGHT_BREAK_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="RATE_DISCOUNT_VALUE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="RATE_DISCOUNT_VALUE_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="RATE_DISCOUNT_VALUE_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="RATE_GEO_COST_GROUP_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *       &lt;/sequence>
     *       &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" />
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "rategeocostseq",
        "weightbreakgid",
        "ratediscountvalue",
        "ratediscountvaluegid",
        "ratediscountvaluebase",
        "rategeocostgroupgid",
        "domainname",
        "insertuser",
        "insertdate",
        "updateuser",
        "updatedate"
    })
    public static class RATEGEOCOSTWEIGHTBREAKROW {

        @XmlElement(name = "RATE_GEO_COST_SEQ")
        protected String rategeocostseq;
        @XmlElement(name = "WEIGHT_BREAK_GID")
        protected String weightbreakgid;
        @XmlElement(name = "RATE_DISCOUNT_VALUE")
        protected String ratediscountvalue;
        @XmlElement(name = "RATE_DISCOUNT_VALUE_GID")
        protected String ratediscountvaluegid;
        @XmlElement(name = "RATE_DISCOUNT_VALUE_BASE")
        protected String ratediscountvaluebase;
        @XmlElement(name = "RATE_GEO_COST_GROUP_GID")
        protected String rategeocostgroupgid;
        @XmlElement(name = "DOMAIN_NAME")
        protected String domainname;
        @XmlElement(name = "INSERT_USER")
        protected String insertuser;
        @XmlElement(name = "INSERT_DATE")
        protected String insertdate;
        @XmlElement(name = "UPDATE_USER")
        protected String updateuser;
        @XmlElement(name = "UPDATE_DATE")
        protected String updatedate;
        @XmlAttribute(name = "num")
        protected String num;

        /**
         * Obtém o valor da propriedade rategeocostseq.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRATEGEOCOSTSEQ() {
            return rategeocostseq;
        }

        /**
         * Define o valor da propriedade rategeocostseq.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRATEGEOCOSTSEQ(String value) {
            this.rategeocostseq = value;
        }

        /**
         * Obtém o valor da propriedade weightbreakgid.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getWEIGHTBREAKGID() {
            return weightbreakgid;
        }

        /**
         * Define o valor da propriedade weightbreakgid.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setWEIGHTBREAKGID(String value) {
            this.weightbreakgid = value;
        }

        /**
         * Obtém o valor da propriedade ratediscountvalue.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRATEDISCOUNTVALUE() {
            return ratediscountvalue;
        }

        /**
         * Define o valor da propriedade ratediscountvalue.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRATEDISCOUNTVALUE(String value) {
            this.ratediscountvalue = value;
        }

        /**
         * Obtém o valor da propriedade ratediscountvaluegid.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRATEDISCOUNTVALUEGID() {
            return ratediscountvaluegid;
        }

        /**
         * Define o valor da propriedade ratediscountvaluegid.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRATEDISCOUNTVALUEGID(String value) {
            this.ratediscountvaluegid = value;
        }

        /**
         * Obtém o valor da propriedade ratediscountvaluebase.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRATEDISCOUNTVALUEBASE() {
            return ratediscountvaluebase;
        }

        /**
         * Define o valor da propriedade ratediscountvaluebase.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRATEDISCOUNTVALUEBASE(String value) {
            this.ratediscountvaluebase = value;
        }

        /**
         * Obtém o valor da propriedade rategeocostgroupgid.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRATEGEOCOSTGROUPGID() {
            return rategeocostgroupgid;
        }

        /**
         * Define o valor da propriedade rategeocostgroupgid.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRATEGEOCOSTGROUPGID(String value) {
            this.rategeocostgroupgid = value;
        }

        /**
         * Obtém o valor da propriedade domainname.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDOMAINNAME() {
            return domainname;
        }

        /**
         * Define o valor da propriedade domainname.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDOMAINNAME(String value) {
            this.domainname = value;
        }

        /**
         * Obtém o valor da propriedade insertuser.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getINSERTUSER() {
            return insertuser;
        }

        /**
         * Define o valor da propriedade insertuser.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setINSERTUSER(String value) {
            this.insertuser = value;
        }

        /**
         * Obtém o valor da propriedade insertdate.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getINSERTDATE() {
            return insertdate;
        }

        /**
         * Define o valor da propriedade insertdate.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setINSERTDATE(String value) {
            this.insertdate = value;
        }

        /**
         * Obtém o valor da propriedade updateuser.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getUPDATEUSER() {
            return updateuser;
        }

        /**
         * Define o valor da propriedade updateuser.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setUPDATEUSER(String value) {
            this.updateuser = value;
        }

        /**
         * Obtém o valor da propriedade updatedate.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getUPDATEDATE() {
            return updatedate;
        }

        /**
         * Define o valor da propriedade updatedate.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setUPDATEDATE(String value) {
            this.updatedate = value;
        }

        /**
         * Obtém o valor da propriedade num.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNum() {
            return num;
        }

        /**
         * Define o valor da propriedade num.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNum(String value) {
            this.num = value;
        }

    }

}
