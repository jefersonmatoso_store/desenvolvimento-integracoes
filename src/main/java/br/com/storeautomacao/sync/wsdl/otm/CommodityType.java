
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Commodity is an element of a invoice line used to identify the "stuff" being delivered.
 * 
 * <p>Classe Java de CommodityType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="CommodityType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FlexCommodityQualifierGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="FlexCommodityValue" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Marks" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}MarksType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CommodityType", propOrder = {
    "flexCommodityQualifierGid",
    "flexCommodityValue",
    "description",
    "marks"
})
public class CommodityType {

    @XmlElement(name = "FlexCommodityQualifierGid")
    protected GLogXMLGidType flexCommodityQualifierGid;
    @XmlElement(name = "FlexCommodityValue")
    protected String flexCommodityValue;
    @XmlElement(name = "Description")
    protected String description;
    @XmlElement(name = "Marks")
    protected MarksType marks;

    /**
     * Obtém o valor da propriedade flexCommodityQualifierGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getFlexCommodityQualifierGid() {
        return flexCommodityQualifierGid;
    }

    /**
     * Define o valor da propriedade flexCommodityQualifierGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setFlexCommodityQualifierGid(GLogXMLGidType value) {
        this.flexCommodityQualifierGid = value;
    }

    /**
     * Obtém o valor da propriedade flexCommodityValue.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlexCommodityValue() {
        return flexCommodityValue;
    }

    /**
     * Define o valor da propriedade flexCommodityValue.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlexCommodityValue(String value) {
        this.flexCommodityValue = value;
    }

    /**
     * Obtém o valor da propriedade description.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Define o valor da propriedade description.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Obtém o valor da propriedade marks.
     * 
     * @return
     *     possible object is
     *     {@link MarksType }
     *     
     */
    public MarksType getMarks() {
        return marks;
    }

    /**
     * Define o valor da propriedade marks.
     * 
     * @param value
     *     allowed object is
     *     {@link MarksType }
     *     
     */
    public void setMarks(MarksType value) {
        this.marks = value;
    }

}
