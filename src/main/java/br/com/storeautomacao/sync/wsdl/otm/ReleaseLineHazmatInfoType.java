
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Hazardous material information for the release line.
 * 
 * <p>Classe Java de ReleaseLineHazmatInfoType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="ReleaseLineHazmatInfoType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IsHazardous" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="HazmatGenericGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="HazmatItemGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="IdentificationNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ProperShippingName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="HazardousClass" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}HazardousClassType" minOccurs="0"/>
 *         &lt;element name="PackagingGroup" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SubsidiaryHazard" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EmergencyResponseInfo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PSASingaporeGroup" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="HazmatCommonInfo" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}HazmatCommonInfoType" minOccurs="0"/>
 *         &lt;element name="ERG" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ERGAir" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EMS" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="HazCompatGroup" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="HazSpecialProvisions" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="HazApprovalExemptionGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReleaseLineHazmatInfoType", propOrder = {
    "isHazardous",
    "hazmatGenericGid",
    "hazmatItemGid",
    "identificationNumber",
    "properShippingName",
    "hazardousClass",
    "packagingGroup",
    "subsidiaryHazard",
    "emergencyResponseInfo",
    "psaSingaporeGroup",
    "hazmatCommonInfo",
    "erg",
    "ergAir",
    "ems",
    "hazCompatGroup",
    "hazSpecialProvisions",
    "hazApprovalExemptionGid"
})
public class ReleaseLineHazmatInfoType {

    @XmlElement(name = "IsHazardous")
    protected String isHazardous;
    @XmlElement(name = "HazmatGenericGid")
    protected GLogXMLGidType hazmatGenericGid;
    @XmlElement(name = "HazmatItemGid")
    protected GLogXMLGidType hazmatItemGid;
    @XmlElement(name = "IdentificationNumber")
    protected String identificationNumber;
    @XmlElement(name = "ProperShippingName")
    protected String properShippingName;
    @XmlElement(name = "HazardousClass")
    protected HazardousClassType hazardousClass;
    @XmlElement(name = "PackagingGroup")
    protected String packagingGroup;
    @XmlElement(name = "SubsidiaryHazard")
    protected String subsidiaryHazard;
    @XmlElement(name = "EmergencyResponseInfo")
    protected String emergencyResponseInfo;
    @XmlElement(name = "PSASingaporeGroup")
    protected String psaSingaporeGroup;
    @XmlElement(name = "HazmatCommonInfo")
    protected HazmatCommonInfoType hazmatCommonInfo;
    @XmlElement(name = "ERG")
    protected String erg;
    @XmlElement(name = "ERGAir")
    protected String ergAir;
    @XmlElement(name = "EMS")
    protected String ems;
    @XmlElement(name = "HazCompatGroup")
    protected String hazCompatGroup;
    @XmlElement(name = "HazSpecialProvisions")
    protected String hazSpecialProvisions;
    @XmlElement(name = "HazApprovalExemptionGid")
    protected GLogXMLGidType hazApprovalExemptionGid;

    /**
     * Obtém o valor da propriedade isHazardous.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsHazardous() {
        return isHazardous;
    }

    /**
     * Define o valor da propriedade isHazardous.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsHazardous(String value) {
        this.isHazardous = value;
    }

    /**
     * Obtém o valor da propriedade hazmatGenericGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getHazmatGenericGid() {
        return hazmatGenericGid;
    }

    /**
     * Define o valor da propriedade hazmatGenericGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setHazmatGenericGid(GLogXMLGidType value) {
        this.hazmatGenericGid = value;
    }

    /**
     * Obtém o valor da propriedade hazmatItemGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getHazmatItemGid() {
        return hazmatItemGid;
    }

    /**
     * Define o valor da propriedade hazmatItemGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setHazmatItemGid(GLogXMLGidType value) {
        this.hazmatItemGid = value;
    }

    /**
     * Obtém o valor da propriedade identificationNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdentificationNumber() {
        return identificationNumber;
    }

    /**
     * Define o valor da propriedade identificationNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdentificationNumber(String value) {
        this.identificationNumber = value;
    }

    /**
     * Obtém o valor da propriedade properShippingName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProperShippingName() {
        return properShippingName;
    }

    /**
     * Define o valor da propriedade properShippingName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProperShippingName(String value) {
        this.properShippingName = value;
    }

    /**
     * Obtém o valor da propriedade hazardousClass.
     * 
     * @return
     *     possible object is
     *     {@link HazardousClassType }
     *     
     */
    public HazardousClassType getHazardousClass() {
        return hazardousClass;
    }

    /**
     * Define o valor da propriedade hazardousClass.
     * 
     * @param value
     *     allowed object is
     *     {@link HazardousClassType }
     *     
     */
    public void setHazardousClass(HazardousClassType value) {
        this.hazardousClass = value;
    }

    /**
     * Obtém o valor da propriedade packagingGroup.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPackagingGroup() {
        return packagingGroup;
    }

    /**
     * Define o valor da propriedade packagingGroup.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPackagingGroup(String value) {
        this.packagingGroup = value;
    }

    /**
     * Obtém o valor da propriedade subsidiaryHazard.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubsidiaryHazard() {
        return subsidiaryHazard;
    }

    /**
     * Define o valor da propriedade subsidiaryHazard.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubsidiaryHazard(String value) {
        this.subsidiaryHazard = value;
    }

    /**
     * Obtém o valor da propriedade emergencyResponseInfo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmergencyResponseInfo() {
        return emergencyResponseInfo;
    }

    /**
     * Define o valor da propriedade emergencyResponseInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmergencyResponseInfo(String value) {
        this.emergencyResponseInfo = value;
    }

    /**
     * Obtém o valor da propriedade psaSingaporeGroup.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPSASingaporeGroup() {
        return psaSingaporeGroup;
    }

    /**
     * Define o valor da propriedade psaSingaporeGroup.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPSASingaporeGroup(String value) {
        this.psaSingaporeGroup = value;
    }

    /**
     * Obtém o valor da propriedade hazmatCommonInfo.
     * 
     * @return
     *     possible object is
     *     {@link HazmatCommonInfoType }
     *     
     */
    public HazmatCommonInfoType getHazmatCommonInfo() {
        return hazmatCommonInfo;
    }

    /**
     * Define o valor da propriedade hazmatCommonInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link HazmatCommonInfoType }
     *     
     */
    public void setHazmatCommonInfo(HazmatCommonInfoType value) {
        this.hazmatCommonInfo = value;
    }

    /**
     * Obtém o valor da propriedade erg.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getERG() {
        return erg;
    }

    /**
     * Define o valor da propriedade erg.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setERG(String value) {
        this.erg = value;
    }

    /**
     * Obtém o valor da propriedade ergAir.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getERGAir() {
        return ergAir;
    }

    /**
     * Define o valor da propriedade ergAir.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setERGAir(String value) {
        this.ergAir = value;
    }

    /**
     * Obtém o valor da propriedade ems.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEMS() {
        return ems;
    }

    /**
     * Define o valor da propriedade ems.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEMS(String value) {
        this.ems = value;
    }

    /**
     * Obtém o valor da propriedade hazCompatGroup.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHazCompatGroup() {
        return hazCompatGroup;
    }

    /**
     * Define o valor da propriedade hazCompatGroup.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHazCompatGroup(String value) {
        this.hazCompatGroup = value;
    }

    /**
     * Obtém o valor da propriedade hazSpecialProvisions.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHazSpecialProvisions() {
        return hazSpecialProvisions;
    }

    /**
     * Define o valor da propriedade hazSpecialProvisions.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHazSpecialProvisions(String value) {
        this.hazSpecialProvisions = value;
    }

    /**
     * Obtém o valor da propriedade hazApprovalExemptionGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getHazApprovalExemptionGid() {
        return hazApprovalExemptionGid;
    }

    /**
     * Define o valor da propriedade hazApprovalExemptionGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setHazApprovalExemptionGid(GLogXMLGidType value) {
        this.hazApprovalExemptionGid = value;
    }

}
