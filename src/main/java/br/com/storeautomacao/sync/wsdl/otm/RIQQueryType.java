
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * Rate Inquiry Input Information.
 *             For ShipUnit used within RIQQuery, please only specify information in ShipUnitSpecGid, ShipUnitCount,
 *             WeightVolume, LengthWidthHeight, DeclaredValue, FlexCommodityValue.
 * 
 *             The OutXmlProfileGid is used to apply an outbound profile when the IncludeRefInfo element is Y.
 *          
 * 
 * <p>Classe Java de RIQQueryType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="RIQQueryType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RIQRequestType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RateGroupGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="SourceAddress" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}MileageCorporationType"/>
 *         &lt;element name="SourceLocationProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocProfileType" minOccurs="0"/>
 *         &lt;element name="DestAddress" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}MileageCorporationType"/>
 *         &lt;element name="StopLocation" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="LocationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="TransportModeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="ServiceProviderGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="AccessorialCodeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;choice minOccurs="0">
 *           &lt;element name="SpecialServiceGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" maxOccurs="unbounded" minOccurs="0"/>
 *           &lt;element name="RIQQuerySpecialService" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RIQQuerySpecialServiceType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;/choice>
 *         &lt;element name="EquipmentGroupGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="ShipmentRefnum" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ShipmentRefnumType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="OrderRefnum" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OrderRefnumType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="AvailableBy" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}AvailableByType" minOccurs="0"/>
 *         &lt;element name="DeliveryBy" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}DeliveryByType" minOccurs="0"/>
 *         &lt;element name="FlexCommodityQualifierGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="EquipmentGroupProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="EquipmentInitial" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EquipmentNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OrigCarrierGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="DelivCarrierGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="RouteCodeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="Perspective" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsPreferred" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RateOfferingGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="RateGeoGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="RateServiceGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="UseRIQRoute" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CustomerDomain" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CustomerGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="ForceTieredRating" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EstTieredRateNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IncoTermGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="IncludeRefInfo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OutXmlProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="IntPreferenceGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="CostCategorySetGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="ShipUnit" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ShipUnitType" maxOccurs="unbounded"/>
 *         &lt;element name="UseRIQRouteNR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PortOfLoadLocationRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocRefType" minOccurs="0"/>
 *         &lt;element name="PortOfDisLocationRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocRefType" minOccurs="0"/>
 *         &lt;element name="VoyageGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="PrimaryOptionDefinition" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MaxPrimaryOptions" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MaxSupportingOptions" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PlanningParamSetGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="IsPrimaryOptionCentric" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MaxOptionsPerRoute" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ItineraryProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="RoutingConstraints" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="PortOfLoadLocation" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LocationRefType" maxOccurs="unbounded" minOccurs="0"/>
 *                   &lt;element name="PortOfDischargeLocation" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LocationRefType" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RIQQueryType", propOrder = {
    "riqRequestType",
    "rateGroupGid",
    "sourceAddress",
    "sourceLocationProfileGid",
    "destAddress",
    "stopLocation",
    "transportModeGid",
    "serviceProviderGid",
    "accessorialCodeGid",
    "specialServiceGid",
    "riqQuerySpecialService",
    "equipmentGroupGid",
    "shipmentRefnum",
    "orderRefnum",
    "availableBy",
    "deliveryBy",
    "flexCommodityQualifierGid",
    "equipmentGroupProfileGid",
    "equipmentInitial",
    "equipmentNumber",
    "origCarrierGid",
    "delivCarrierGid",
    "routeCodeGid",
    "perspective",
    "isPreferred",
    "rateOfferingGid",
    "rateGeoGid",
    "rateServiceGid",
    "useRIQRoute",
    "customerDomain",
    "customerGid",
    "forceTieredRating",
    "estTieredRateNum",
    "incoTermGid",
    "includeRefInfo",
    "outXmlProfileGid",
    "intPreferenceGid",
    "costCategorySetGid",
    "shipUnit",
    "useRIQRouteNR",
    "portOfLoadLocationRef",
    "portOfDisLocationRef",
    "voyageGid",
    "primaryOptionDefinition",
    "maxPrimaryOptions",
    "maxSupportingOptions",
    "planningParamSetGid",
    "isPrimaryOptionCentric",
    "maxOptionsPerRoute",
    "itineraryProfileGid",
    "routingConstraints"
})
public class RIQQueryType {

    @XmlElement(name = "RIQRequestType")
    protected String riqRequestType;
    @XmlElement(name = "RateGroupGid")
    protected GLogXMLGidType rateGroupGid;
    @XmlElement(name = "SourceAddress", required = true)
    protected MileageCorporationType sourceAddress;
    @XmlElement(name = "SourceLocationProfileGid")
    protected GLogXMLLocProfileType sourceLocationProfileGid;
    @XmlElement(name = "DestAddress", required = true)
    protected MileageCorporationType destAddress;
    @XmlElement(name = "StopLocation")
    protected List<StopLocation> stopLocation;
    @XmlElement(name = "TransportModeGid")
    protected List<GLogXMLGidType> transportModeGid;
    @XmlElement(name = "ServiceProviderGid")
    protected List<GLogXMLGidType> serviceProviderGid;
    @XmlElement(name = "AccessorialCodeGid")
    protected List<GLogXMLGidType> accessorialCodeGid;
    @XmlElement(name = "SpecialServiceGid")
    protected List<GLogXMLGidType> specialServiceGid;
    @XmlElement(name = "RIQQuerySpecialService")
    protected List<RIQQuerySpecialServiceType> riqQuerySpecialService;
    @XmlElement(name = "EquipmentGroupGid")
    protected List<GLogXMLGidType> equipmentGroupGid;
    @XmlElement(name = "ShipmentRefnum")
    protected List<ShipmentRefnumType> shipmentRefnum;
    @XmlElement(name = "OrderRefnum")
    protected List<OrderRefnumType> orderRefnum;
    @XmlElement(name = "AvailableBy")
    protected AvailableByType availableBy;
    @XmlElement(name = "DeliveryBy")
    protected DeliveryByType deliveryBy;
    @XmlElement(name = "FlexCommodityQualifierGid")
    protected GLogXMLGidType flexCommodityQualifierGid;
    @XmlElement(name = "EquipmentGroupProfileGid")
    protected GLogXMLGidType equipmentGroupProfileGid;
    @XmlElement(name = "EquipmentInitial")
    protected String equipmentInitial;
    @XmlElement(name = "EquipmentNumber")
    protected String equipmentNumber;
    @XmlElement(name = "OrigCarrierGid")
    protected GLogXMLGidType origCarrierGid;
    @XmlElement(name = "DelivCarrierGid")
    protected GLogXMLGidType delivCarrierGid;
    @XmlElement(name = "RouteCodeGid")
    protected GLogXMLGidType routeCodeGid;
    @XmlElement(name = "Perspective")
    protected String perspective;
    @XmlElement(name = "IsPreferred")
    protected String isPreferred;
    @XmlElement(name = "RateOfferingGid")
    protected GLogXMLGidType rateOfferingGid;
    @XmlElement(name = "RateGeoGid")
    protected GLogXMLGidType rateGeoGid;
    @XmlElement(name = "RateServiceGid")
    protected GLogXMLGidType rateServiceGid;
    @XmlElement(name = "UseRIQRoute")
    protected String useRIQRoute;
    @XmlElement(name = "CustomerDomain")
    protected String customerDomain;
    @XmlElement(name = "CustomerGid")
    protected GLogXMLGidType customerGid;
    @XmlElement(name = "ForceTieredRating")
    protected String forceTieredRating;
    @XmlElement(name = "EstTieredRateNum")
    protected String estTieredRateNum;
    @XmlElement(name = "IncoTermGid")
    protected GLogXMLGidType incoTermGid;
    @XmlElement(name = "IncludeRefInfo")
    protected String includeRefInfo;
    @XmlElement(name = "OutXmlProfileGid")
    protected GLogXMLGidType outXmlProfileGid;
    @XmlElement(name = "IntPreferenceGid")
    protected GLogXMLGidType intPreferenceGid;
    @XmlElement(name = "CostCategorySetGid")
    protected GLogXMLGidType costCategorySetGid;
    @XmlElement(name = "ShipUnit", required = true)
    protected List<ShipUnitType> shipUnit;
    @XmlElement(name = "UseRIQRouteNR")
    protected String useRIQRouteNR;
    @XmlElement(name = "PortOfLoadLocationRef")
    protected GLogXMLLocRefType portOfLoadLocationRef;
    @XmlElement(name = "PortOfDisLocationRef")
    protected GLogXMLLocRefType portOfDisLocationRef;
    @XmlElement(name = "VoyageGid")
    protected GLogXMLGidType voyageGid;
    @XmlElement(name = "PrimaryOptionDefinition")
    protected String primaryOptionDefinition;
    @XmlElement(name = "MaxPrimaryOptions")
    protected String maxPrimaryOptions;
    @XmlElement(name = "MaxSupportingOptions")
    protected String maxSupportingOptions;
    @XmlElement(name = "PlanningParamSetGid")
    protected GLogXMLGidType planningParamSetGid;
    @XmlElement(name = "IsPrimaryOptionCentric")
    protected String isPrimaryOptionCentric;
    @XmlElement(name = "MaxOptionsPerRoute")
    protected String maxOptionsPerRoute;
    @XmlElement(name = "ItineraryProfileGid")
    protected GLogXMLGidType itineraryProfileGid;
    @XmlElement(name = "RoutingConstraints")
    protected RoutingConstraints routingConstraints;

    /**
     * Obtém o valor da propriedade riqRequestType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRIQRequestType() {
        return riqRequestType;
    }

    /**
     * Define o valor da propriedade riqRequestType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRIQRequestType(String value) {
        this.riqRequestType = value;
    }

    /**
     * Obtém o valor da propriedade rateGroupGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRateGroupGid() {
        return rateGroupGid;
    }

    /**
     * Define o valor da propriedade rateGroupGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRateGroupGid(GLogXMLGidType value) {
        this.rateGroupGid = value;
    }

    /**
     * Obtém o valor da propriedade sourceAddress.
     * 
     * @return
     *     possible object is
     *     {@link MileageCorporationType }
     *     
     */
    public MileageCorporationType getSourceAddress() {
        return sourceAddress;
    }

    /**
     * Define o valor da propriedade sourceAddress.
     * 
     * @param value
     *     allowed object is
     *     {@link MileageCorporationType }
     *     
     */
    public void setSourceAddress(MileageCorporationType value) {
        this.sourceAddress = value;
    }

    /**
     * Obtém o valor da propriedade sourceLocationProfileGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocProfileType }
     *     
     */
    public GLogXMLLocProfileType getSourceLocationProfileGid() {
        return sourceLocationProfileGid;
    }

    /**
     * Define o valor da propriedade sourceLocationProfileGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocProfileType }
     *     
     */
    public void setSourceLocationProfileGid(GLogXMLLocProfileType value) {
        this.sourceLocationProfileGid = value;
    }

    /**
     * Obtém o valor da propriedade destAddress.
     * 
     * @return
     *     possible object is
     *     {@link MileageCorporationType }
     *     
     */
    public MileageCorporationType getDestAddress() {
        return destAddress;
    }

    /**
     * Define o valor da propriedade destAddress.
     * 
     * @param value
     *     allowed object is
     *     {@link MileageCorporationType }
     *     
     */
    public void setDestAddress(MileageCorporationType value) {
        this.destAddress = value;
    }

    /**
     * Gets the value of the stopLocation property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the stopLocation property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getStopLocation().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link StopLocation }
     * 
     * 
     */
    public List<StopLocation> getStopLocation() {
        if (stopLocation == null) {
            stopLocation = new ArrayList<StopLocation>();
        }
        return this.stopLocation;
    }

    /**
     * Gets the value of the transportModeGid property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the transportModeGid property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTransportModeGid().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GLogXMLGidType }
     * 
     * 
     */
    public List<GLogXMLGidType> getTransportModeGid() {
        if (transportModeGid == null) {
            transportModeGid = new ArrayList<GLogXMLGidType>();
        }
        return this.transportModeGid;
    }

    /**
     * Gets the value of the serviceProviderGid property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the serviceProviderGid property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getServiceProviderGid().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GLogXMLGidType }
     * 
     * 
     */
    public List<GLogXMLGidType> getServiceProviderGid() {
        if (serviceProviderGid == null) {
            serviceProviderGid = new ArrayList<GLogXMLGidType>();
        }
        return this.serviceProviderGid;
    }

    /**
     * Gets the value of the accessorialCodeGid property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the accessorialCodeGid property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAccessorialCodeGid().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GLogXMLGidType }
     * 
     * 
     */
    public List<GLogXMLGidType> getAccessorialCodeGid() {
        if (accessorialCodeGid == null) {
            accessorialCodeGid = new ArrayList<GLogXMLGidType>();
        }
        return this.accessorialCodeGid;
    }

    /**
     * Gets the value of the specialServiceGid property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the specialServiceGid property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSpecialServiceGid().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GLogXMLGidType }
     * 
     * 
     */
    public List<GLogXMLGidType> getSpecialServiceGid() {
        if (specialServiceGid == null) {
            specialServiceGid = new ArrayList<GLogXMLGidType>();
        }
        return this.specialServiceGid;
    }

    /**
     * Gets the value of the riqQuerySpecialService property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the riqQuerySpecialService property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRIQQuerySpecialService().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RIQQuerySpecialServiceType }
     * 
     * 
     */
    public List<RIQQuerySpecialServiceType> getRIQQuerySpecialService() {
        if (riqQuerySpecialService == null) {
            riqQuerySpecialService = new ArrayList<RIQQuerySpecialServiceType>();
        }
        return this.riqQuerySpecialService;
    }

    /**
     * Gets the value of the equipmentGroupGid property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the equipmentGroupGid property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEquipmentGroupGid().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GLogXMLGidType }
     * 
     * 
     */
    public List<GLogXMLGidType> getEquipmentGroupGid() {
        if (equipmentGroupGid == null) {
            equipmentGroupGid = new ArrayList<GLogXMLGidType>();
        }
        return this.equipmentGroupGid;
    }

    /**
     * Gets the value of the shipmentRefnum property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the shipmentRefnum property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getShipmentRefnum().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ShipmentRefnumType }
     * 
     * 
     */
    public List<ShipmentRefnumType> getShipmentRefnum() {
        if (shipmentRefnum == null) {
            shipmentRefnum = new ArrayList<ShipmentRefnumType>();
        }
        return this.shipmentRefnum;
    }

    /**
     * Gets the value of the orderRefnum property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the orderRefnum property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOrderRefnum().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OrderRefnumType }
     * 
     * 
     */
    public List<OrderRefnumType> getOrderRefnum() {
        if (orderRefnum == null) {
            orderRefnum = new ArrayList<OrderRefnumType>();
        }
        return this.orderRefnum;
    }

    /**
     * Obtém o valor da propriedade availableBy.
     * 
     * @return
     *     possible object is
     *     {@link AvailableByType }
     *     
     */
    public AvailableByType getAvailableBy() {
        return availableBy;
    }

    /**
     * Define o valor da propriedade availableBy.
     * 
     * @param value
     *     allowed object is
     *     {@link AvailableByType }
     *     
     */
    public void setAvailableBy(AvailableByType value) {
        this.availableBy = value;
    }

    /**
     * Obtém o valor da propriedade deliveryBy.
     * 
     * @return
     *     possible object is
     *     {@link DeliveryByType }
     *     
     */
    public DeliveryByType getDeliveryBy() {
        return deliveryBy;
    }

    /**
     * Define o valor da propriedade deliveryBy.
     * 
     * @param value
     *     allowed object is
     *     {@link DeliveryByType }
     *     
     */
    public void setDeliveryBy(DeliveryByType value) {
        this.deliveryBy = value;
    }

    /**
     * Obtém o valor da propriedade flexCommodityQualifierGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getFlexCommodityQualifierGid() {
        return flexCommodityQualifierGid;
    }

    /**
     * Define o valor da propriedade flexCommodityQualifierGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setFlexCommodityQualifierGid(GLogXMLGidType value) {
        this.flexCommodityQualifierGid = value;
    }

    /**
     * Obtém o valor da propriedade equipmentGroupProfileGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getEquipmentGroupProfileGid() {
        return equipmentGroupProfileGid;
    }

    /**
     * Define o valor da propriedade equipmentGroupProfileGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setEquipmentGroupProfileGid(GLogXMLGidType value) {
        this.equipmentGroupProfileGid = value;
    }

    /**
     * Obtém o valor da propriedade equipmentInitial.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEquipmentInitial() {
        return equipmentInitial;
    }

    /**
     * Define o valor da propriedade equipmentInitial.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEquipmentInitial(String value) {
        this.equipmentInitial = value;
    }

    /**
     * Obtém o valor da propriedade equipmentNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEquipmentNumber() {
        return equipmentNumber;
    }

    /**
     * Define o valor da propriedade equipmentNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEquipmentNumber(String value) {
        this.equipmentNumber = value;
    }

    /**
     * Obtém o valor da propriedade origCarrierGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getOrigCarrierGid() {
        return origCarrierGid;
    }

    /**
     * Define o valor da propriedade origCarrierGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setOrigCarrierGid(GLogXMLGidType value) {
        this.origCarrierGid = value;
    }

    /**
     * Obtém o valor da propriedade delivCarrierGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getDelivCarrierGid() {
        return delivCarrierGid;
    }

    /**
     * Define o valor da propriedade delivCarrierGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setDelivCarrierGid(GLogXMLGidType value) {
        this.delivCarrierGid = value;
    }

    /**
     * Obtém o valor da propriedade routeCodeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRouteCodeGid() {
        return routeCodeGid;
    }

    /**
     * Define o valor da propriedade routeCodeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRouteCodeGid(GLogXMLGidType value) {
        this.routeCodeGid = value;
    }

    /**
     * Obtém o valor da propriedade perspective.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPerspective() {
        return perspective;
    }

    /**
     * Define o valor da propriedade perspective.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPerspective(String value) {
        this.perspective = value;
    }

    /**
     * Obtém o valor da propriedade isPreferred.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsPreferred() {
        return isPreferred;
    }

    /**
     * Define o valor da propriedade isPreferred.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsPreferred(String value) {
        this.isPreferred = value;
    }

    /**
     * Obtém o valor da propriedade rateOfferingGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRateOfferingGid() {
        return rateOfferingGid;
    }

    /**
     * Define o valor da propriedade rateOfferingGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRateOfferingGid(GLogXMLGidType value) {
        this.rateOfferingGid = value;
    }

    /**
     * Obtém o valor da propriedade rateGeoGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRateGeoGid() {
        return rateGeoGid;
    }

    /**
     * Define o valor da propriedade rateGeoGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRateGeoGid(GLogXMLGidType value) {
        this.rateGeoGid = value;
    }

    /**
     * Obtém o valor da propriedade rateServiceGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRateServiceGid() {
        return rateServiceGid;
    }

    /**
     * Define o valor da propriedade rateServiceGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRateServiceGid(GLogXMLGidType value) {
        this.rateServiceGid = value;
    }

    /**
     * Obtém o valor da propriedade useRIQRoute.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUseRIQRoute() {
        return useRIQRoute;
    }

    /**
     * Define o valor da propriedade useRIQRoute.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUseRIQRoute(String value) {
        this.useRIQRoute = value;
    }

    /**
     * Obtém o valor da propriedade customerDomain.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomerDomain() {
        return customerDomain;
    }

    /**
     * Define o valor da propriedade customerDomain.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomerDomain(String value) {
        this.customerDomain = value;
    }

    /**
     * Obtém o valor da propriedade customerGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getCustomerGid() {
        return customerGid;
    }

    /**
     * Define o valor da propriedade customerGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setCustomerGid(GLogXMLGidType value) {
        this.customerGid = value;
    }

    /**
     * Obtém o valor da propriedade forceTieredRating.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getForceTieredRating() {
        return forceTieredRating;
    }

    /**
     * Define o valor da propriedade forceTieredRating.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setForceTieredRating(String value) {
        this.forceTieredRating = value;
    }

    /**
     * Obtém o valor da propriedade estTieredRateNum.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEstTieredRateNum() {
        return estTieredRateNum;
    }

    /**
     * Define o valor da propriedade estTieredRateNum.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEstTieredRateNum(String value) {
        this.estTieredRateNum = value;
    }

    /**
     * Obtém o valor da propriedade incoTermGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getIncoTermGid() {
        return incoTermGid;
    }

    /**
     * Define o valor da propriedade incoTermGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setIncoTermGid(GLogXMLGidType value) {
        this.incoTermGid = value;
    }

    /**
     * Obtém o valor da propriedade includeRefInfo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIncludeRefInfo() {
        return includeRefInfo;
    }

    /**
     * Define o valor da propriedade includeRefInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIncludeRefInfo(String value) {
        this.includeRefInfo = value;
    }

    /**
     * Obtém o valor da propriedade outXmlProfileGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getOutXmlProfileGid() {
        return outXmlProfileGid;
    }

    /**
     * Define o valor da propriedade outXmlProfileGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setOutXmlProfileGid(GLogXMLGidType value) {
        this.outXmlProfileGid = value;
    }

    /**
     * Obtém o valor da propriedade intPreferenceGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getIntPreferenceGid() {
        return intPreferenceGid;
    }

    /**
     * Define o valor da propriedade intPreferenceGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setIntPreferenceGid(GLogXMLGidType value) {
        this.intPreferenceGid = value;
    }

    /**
     * Obtém o valor da propriedade costCategorySetGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getCostCategorySetGid() {
        return costCategorySetGid;
    }

    /**
     * Define o valor da propriedade costCategorySetGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setCostCategorySetGid(GLogXMLGidType value) {
        this.costCategorySetGid = value;
    }

    /**
     * Gets the value of the shipUnit property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the shipUnit property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getShipUnit().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ShipUnitType }
     * 
     * 
     */
    public List<ShipUnitType> getShipUnit() {
        if (shipUnit == null) {
            shipUnit = new ArrayList<ShipUnitType>();
        }
        return this.shipUnit;
    }

    /**
     * Obtém o valor da propriedade useRIQRouteNR.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUseRIQRouteNR() {
        return useRIQRouteNR;
    }

    /**
     * Define o valor da propriedade useRIQRouteNR.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUseRIQRouteNR(String value) {
        this.useRIQRouteNR = value;
    }

    /**
     * Obtém o valor da propriedade portOfLoadLocationRef.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public GLogXMLLocRefType getPortOfLoadLocationRef() {
        return portOfLoadLocationRef;
    }

    /**
     * Define o valor da propriedade portOfLoadLocationRef.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public void setPortOfLoadLocationRef(GLogXMLLocRefType value) {
        this.portOfLoadLocationRef = value;
    }

    /**
     * Obtém o valor da propriedade portOfDisLocationRef.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public GLogXMLLocRefType getPortOfDisLocationRef() {
        return portOfDisLocationRef;
    }

    /**
     * Define o valor da propriedade portOfDisLocationRef.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public void setPortOfDisLocationRef(GLogXMLLocRefType value) {
        this.portOfDisLocationRef = value;
    }

    /**
     * Obtém o valor da propriedade voyageGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getVoyageGid() {
        return voyageGid;
    }

    /**
     * Define o valor da propriedade voyageGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setVoyageGid(GLogXMLGidType value) {
        this.voyageGid = value;
    }

    /**
     * Obtém o valor da propriedade primaryOptionDefinition.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrimaryOptionDefinition() {
        return primaryOptionDefinition;
    }

    /**
     * Define o valor da propriedade primaryOptionDefinition.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrimaryOptionDefinition(String value) {
        this.primaryOptionDefinition = value;
    }

    /**
     * Obtém o valor da propriedade maxPrimaryOptions.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMaxPrimaryOptions() {
        return maxPrimaryOptions;
    }

    /**
     * Define o valor da propriedade maxPrimaryOptions.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMaxPrimaryOptions(String value) {
        this.maxPrimaryOptions = value;
    }

    /**
     * Obtém o valor da propriedade maxSupportingOptions.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMaxSupportingOptions() {
        return maxSupportingOptions;
    }

    /**
     * Define o valor da propriedade maxSupportingOptions.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMaxSupportingOptions(String value) {
        this.maxSupportingOptions = value;
    }

    /**
     * Obtém o valor da propriedade planningParamSetGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getPlanningParamSetGid() {
        return planningParamSetGid;
    }

    /**
     * Define o valor da propriedade planningParamSetGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setPlanningParamSetGid(GLogXMLGidType value) {
        this.planningParamSetGid = value;
    }

    /**
     * Obtém o valor da propriedade isPrimaryOptionCentric.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsPrimaryOptionCentric() {
        return isPrimaryOptionCentric;
    }

    /**
     * Define o valor da propriedade isPrimaryOptionCentric.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsPrimaryOptionCentric(String value) {
        this.isPrimaryOptionCentric = value;
    }

    /**
     * Obtém o valor da propriedade maxOptionsPerRoute.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMaxOptionsPerRoute() {
        return maxOptionsPerRoute;
    }

    /**
     * Define o valor da propriedade maxOptionsPerRoute.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMaxOptionsPerRoute(String value) {
        this.maxOptionsPerRoute = value;
    }

    /**
     * Obtém o valor da propriedade itineraryProfileGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getItineraryProfileGid() {
        return itineraryProfileGid;
    }

    /**
     * Define o valor da propriedade itineraryProfileGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setItineraryProfileGid(GLogXMLGidType value) {
        this.itineraryProfileGid = value;
    }

    /**
     * Obtém o valor da propriedade routingConstraints.
     * 
     * @return
     *     possible object is
     *     {@link RoutingConstraints }
     *     
     */
    public RoutingConstraints getRoutingConstraints() {
        return routingConstraints;
    }

    /**
     * Define o valor da propriedade routingConstraints.
     * 
     * @param value
     *     allowed object is
     *     {@link RoutingConstraints }
     *     
     */
    public void setRoutingConstraints(RoutingConstraints value) {
        this.routingConstraints = value;
    }


    /**
     * <p>Classe Java de anonymous complex type.
     * 
     * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="PortOfLoadLocation" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LocationRefType" maxOccurs="unbounded" minOccurs="0"/>
     *         &lt;element name="PortOfDischargeLocation" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LocationRefType" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "portOfLoadLocation",
        "portOfDischargeLocation"
    })
    public static class RoutingConstraints {

        @XmlElement(name = "PortOfLoadLocation")
        protected List<LocationRefType> portOfLoadLocation;
        @XmlElement(name = "PortOfDischargeLocation")
        protected List<LocationRefType> portOfDischargeLocation;

        /**
         * Gets the value of the portOfLoadLocation property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the portOfLoadLocation property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getPortOfLoadLocation().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link LocationRefType }
         * 
         * 
         */
        public List<LocationRefType> getPortOfLoadLocation() {
            if (portOfLoadLocation == null) {
                portOfLoadLocation = new ArrayList<LocationRefType>();
            }
            return this.portOfLoadLocation;
        }

        /**
         * Gets the value of the portOfDischargeLocation property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the portOfDischargeLocation property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getPortOfDischargeLocation().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link LocationRefType }
         * 
         * 
         */
        public List<LocationRefType> getPortOfDischargeLocation() {
            if (portOfDischargeLocation == null) {
                portOfDischargeLocation = new ArrayList<LocationRefType>();
            }
            return this.portOfDischargeLocation;
        }

    }


    /**
     * <p>Classe Java de anonymous complex type.
     * 
     * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="LocationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "locationGid"
    })
    public static class StopLocation {

        @XmlElement(name = "LocationGid")
        protected GLogXMLGidType locationGid;

        /**
         * Obtém o valor da propriedade locationGid.
         * 
         * @return
         *     possible object is
         *     {@link GLogXMLGidType }
         *     
         */
        public GLogXMLGidType getLocationGid() {
            return locationGid;
        }

        /**
         * Define o valor da propriedade locationGid.
         * 
         * @param value
         *     allowed object is
         *     {@link GLogXMLGidType }
         *     
         */
        public void setLocationGid(GLogXMLGidType value) {
            this.locationGid = value;
        }

    }

}
