
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * 
 *             The ShipmentStatus element is used to add shipment/tracking
 *             events. Agent(s) are required to process these events. Typically an agent that consists
 *             of a match agent action (Eg: Match Shipment, Match Order Base, Match Order Release etc)
 *             would be a first step that connects the event to the business object (Eg: Shipment,
 *             Order Base, Order Release, Driver, Power Unit etc) that the event is being sent for.
 *          
 * 
 * <p>Classe Java de ShipmentStatusType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="ShipmentStatusType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OTMTransactionInOut">
 *       &lt;sequence>
 *         &lt;element name="SendReason" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}SendReasonType" minOccurs="0"/>
 *         &lt;element name="ServiceProviderAlias" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ServiceProviderAliasType" minOccurs="0"/>
 *         &lt;element name="IntSavedQuery" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}IntSavedQueryType" minOccurs="0"/>
 *         &lt;choice minOccurs="0">
 *           &lt;element name="ShipmentRefnum" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ShipmentRefnumType" maxOccurs="unbounded" minOccurs="0"/>
 *           &lt;element name="ShipmentGroupRefnum" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ShipmentGroupRefnumType" maxOccurs="unbounded" minOccurs="0"/>
 *           &lt;element name="ShipmentStatusType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;/choice>
 *         &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="StatusLevel" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="VesselInfo" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}VesselInfoType" minOccurs="0"/>
 *         &lt;element name="RailInfo" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RailInfoType" minOccurs="0"/>
 *         &lt;element name="ShipOrSailDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="DischargeOrCompletionDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="FlightOrVoyageNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="WeightVolume" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}WeightVolumeType" minOccurs="0"/>
 *         &lt;element name="InRouteCarriers" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}InRouteCarriersType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="OrderRefnum" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OrderRefnumType" minOccurs="0"/>
 *         &lt;element name="Marks" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}MarksType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="StatusCodeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="TimeZoneGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="EventDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="StatusReasonCodeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="TrainOrJCTOrBOCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SSEquipment" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}SSEquipmentType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="SStatusSEquipment" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}SStatusSEquipmentType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="SSContact" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}SSContactType" minOccurs="0"/>
 *         &lt;choice minOccurs="0">
 *           &lt;element name="SSRemarks" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *           &lt;element name="Remark" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RemarkType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;/choice>
 *         &lt;element name="SSStop" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}SSStopType" minOccurs="0"/>
 *         &lt;element name="SStatusShipUnit" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}SStatusShipUnitType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="EventGroup" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}EventGroupType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="StatusGroup" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}StatusGroupType" minOccurs="0"/>
 *         &lt;element name="ReasonGroup" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ReasonGroupType" minOccurs="0"/>
 *         &lt;element name="QuickCodeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="ResponsiblePartyGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="ReportingUser" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsContainerBased" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Temperature" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TemperatureType" minOccurs="0"/>
 *         &lt;element name="DriverGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="DriverRefnum" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}DriverRefnumType" minOccurs="0"/>
 *         &lt;element name="PowerUnitGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="PowerUnitRefnum" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}PowerUnitRefnumType" minOccurs="0"/>
 *         &lt;element name="ExtDataSource" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ExtEventRefNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EventRecdDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="EventEndDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="TimeWorked" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDurationType" minOccurs="0"/>
 *         &lt;element name="TimeDriven" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDurationType" minOccurs="0"/>
 *         &lt;element name="DriverCalEventGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="ShipStatusSpclService" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ShipStatusSpclServiceType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="SStatusHOSRuleState" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}HOSRuleState" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="TransOrderGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="ReleaseGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="ReleaseRefnum" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ReleaseRefnumType" minOccurs="0"/>
 *         &lt;element name="OBSLine" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LineGidType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="ORSLine" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LineGidType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="OBSShipUnit" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ShipUnitCriteriaType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="ORSShipUnit" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ShipUnitCriteriaType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="IeDocumentResponse" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}IeDocumentResponseType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="TrackingNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FilingResponseType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FlexFieldStrings" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldStringType" minOccurs="0"/>
 *         &lt;element name="FlexFieldNumbers" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldNumberType" minOccurs="0"/>
 *         &lt;element name="FlexFieldDates" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldDateType" minOccurs="0"/>
 *         &lt;element name="ShipmentGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="ShipmentGroupGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="ShipmentGroupTypeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="ReleaseLineGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="ORLineRefnum" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ORLineRefnumType" minOccurs="0"/>
 *         &lt;element name="MatchObjInvolvedParty" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}MatchObjInvolvedPartyType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ShipmentStatusType", propOrder = {
    "sendReason",
    "serviceProviderAlias",
    "intSavedQuery",
    "shipmentRefnum",
    "shipmentGroupRefnum",
    "shipmentStatusType",
    "description",
    "statusLevel",
    "vesselInfo",
    "railInfo",
    "shipOrSailDt",
    "dischargeOrCompletionDt",
    "flightOrVoyageNumber",
    "weightVolume",
    "inRouteCarriers",
    "orderRefnum",
    "marks",
    "statusCodeGid",
    "timeZoneGid",
    "eventDt",
    "statusReasonCodeGid",
    "trainOrJCTOrBOCode",
    "ssEquipment",
    "sStatusSEquipment",
    "ssContact",
    "ssRemarks",
    "remark",
    "ssStop",
    "sStatusShipUnit",
    "eventGroup",
    "statusGroup",
    "reasonGroup",
    "quickCodeGid",
    "responsiblePartyGid",
    "reportingUser",
    "isContainerBased",
    "temperature",
    "driverGid",
    "driverRefnum",
    "powerUnitGid",
    "powerUnitRefnum",
    "extDataSource",
    "extEventRefNo",
    "eventRecdDate",
    "eventEndDate",
    "timeWorked",
    "timeDriven",
    "driverCalEventGid",
    "shipStatusSpclService",
    "sStatusHOSRuleState",
    "transOrderGid",
    "releaseGid",
    "releaseRefnum",
    "obsLine",
    "orsLine",
    "obsShipUnit",
    "orsShipUnit",
    "ieDocumentResponse",
    "trackingNumber",
    "filingResponseType",
    "flexFieldStrings",
    "flexFieldNumbers",
    "flexFieldDates",
    "shipmentGid",
    "shipmentGroupGid",
    "shipmentGroupTypeGid",
    "releaseLineGid",
    "orLineRefnum",
    "matchObjInvolvedParty"
})
public class ShipmentStatusType
    extends OTMTransactionInOut
{

    @XmlElement(name = "SendReason")
    protected SendReasonType sendReason;
    @XmlElement(name = "ServiceProviderAlias")
    protected ServiceProviderAliasType serviceProviderAlias;
    @XmlElement(name = "IntSavedQuery")
    protected IntSavedQueryType intSavedQuery;
    @XmlElement(name = "ShipmentRefnum")
    protected List<ShipmentRefnumType> shipmentRefnum;
    @XmlElement(name = "ShipmentGroupRefnum")
    protected List<ShipmentGroupRefnumType> shipmentGroupRefnum;
    @XmlElement(name = "ShipmentStatusType")
    protected String shipmentStatusType;
    @XmlElement(name = "Description")
    protected String description;
    @XmlElement(name = "StatusLevel")
    protected String statusLevel;
    @XmlElement(name = "VesselInfo")
    protected VesselInfoType vesselInfo;
    @XmlElement(name = "RailInfo")
    protected RailInfoType railInfo;
    @XmlElement(name = "ShipOrSailDt")
    protected GLogDateTimeType shipOrSailDt;
    @XmlElement(name = "DischargeOrCompletionDt")
    protected GLogDateTimeType dischargeOrCompletionDt;
    @XmlElement(name = "FlightOrVoyageNumber")
    protected String flightOrVoyageNumber;
    @XmlElement(name = "WeightVolume")
    protected WeightVolumeType weightVolume;
    @XmlElement(name = "InRouteCarriers")
    protected List<InRouteCarriersType> inRouteCarriers;
    @XmlElement(name = "OrderRefnum")
    protected OrderRefnumType orderRefnum;
    @XmlElement(name = "Marks")
    protected List<MarksType> marks;
    @XmlElement(name = "StatusCodeGid")
    protected GLogXMLGidType statusCodeGid;
    @XmlElement(name = "TimeZoneGid")
    protected GLogXMLGidType timeZoneGid;
    @XmlElement(name = "EventDt")
    protected GLogDateTimeType eventDt;
    @XmlElement(name = "StatusReasonCodeGid")
    protected GLogXMLGidType statusReasonCodeGid;
    @XmlElement(name = "TrainOrJCTOrBOCode")
    protected String trainOrJCTOrBOCode;
    @XmlElement(name = "SSEquipment")
    protected List<SSEquipmentType> ssEquipment;
    @XmlElement(name = "SStatusSEquipment")
    protected List<SStatusSEquipmentType> sStatusSEquipment;
    @XmlElement(name = "SSContact")
    protected SSContactType ssContact;
    @XmlElement(name = "SSRemarks")
    protected List<String> ssRemarks;
    @XmlElement(name = "Remark")
    protected List<RemarkType> remark;
    @XmlElement(name = "SSStop")
    protected SSStopType ssStop;
    @XmlElement(name = "SStatusShipUnit")
    protected List<SStatusShipUnitType> sStatusShipUnit;
    @XmlElement(name = "EventGroup")
    protected List<EventGroupType> eventGroup;
    @XmlElement(name = "StatusGroup")
    protected StatusGroupType statusGroup;
    @XmlElement(name = "ReasonGroup")
    protected ReasonGroupType reasonGroup;
    @XmlElement(name = "QuickCodeGid")
    protected GLogXMLGidType quickCodeGid;
    @XmlElement(name = "ResponsiblePartyGid")
    protected GLogXMLGidType responsiblePartyGid;
    @XmlElement(name = "ReportingUser")
    protected String reportingUser;
    @XmlElement(name = "IsContainerBased")
    protected String isContainerBased;
    @XmlElement(name = "Temperature")
    protected TemperatureType temperature;
    @XmlElement(name = "DriverGid")
    protected GLogXMLGidType driverGid;
    @XmlElement(name = "DriverRefnum")
    protected DriverRefnumType driverRefnum;
    @XmlElement(name = "PowerUnitGid")
    protected GLogXMLGidType powerUnitGid;
    @XmlElement(name = "PowerUnitRefnum")
    protected PowerUnitRefnumType powerUnitRefnum;
    @XmlElement(name = "ExtDataSource")
    protected String extDataSource;
    @XmlElement(name = "ExtEventRefNo")
    protected String extEventRefNo;
    @XmlElement(name = "EventRecdDate")
    protected GLogDateTimeType eventRecdDate;
    @XmlElement(name = "EventEndDate")
    protected GLogDateTimeType eventEndDate;
    @XmlElement(name = "TimeWorked")
    protected GLogXMLDurationType timeWorked;
    @XmlElement(name = "TimeDriven")
    protected GLogXMLDurationType timeDriven;
    @XmlElement(name = "DriverCalEventGid")
    protected GLogXMLGidType driverCalEventGid;
    @XmlElement(name = "ShipStatusSpclService")
    protected List<ShipStatusSpclServiceType> shipStatusSpclService;
    @XmlElement(name = "SStatusHOSRuleState")
    protected List<HOSRuleState> sStatusHOSRuleState;
    @XmlElement(name = "TransOrderGid")
    protected GLogXMLGidType transOrderGid;
    @XmlElement(name = "ReleaseGid")
    protected GLogXMLGidType releaseGid;
    @XmlElement(name = "ReleaseRefnum")
    protected ReleaseRefnumType releaseRefnum;
    @XmlElement(name = "OBSLine")
    protected List<LineGidType> obsLine;
    @XmlElement(name = "ORSLine")
    protected List<LineGidType> orsLine;
    @XmlElement(name = "OBSShipUnit")
    protected List<ShipUnitCriteriaType> obsShipUnit;
    @XmlElement(name = "ORSShipUnit")
    protected List<ShipUnitCriteriaType> orsShipUnit;
    @XmlElement(name = "IeDocumentResponse")
    protected List<IeDocumentResponseType> ieDocumentResponse;
    @XmlElement(name = "TrackingNumber")
    protected String trackingNumber;
    @XmlElement(name = "FilingResponseType")
    protected String filingResponseType;
    @XmlElement(name = "FlexFieldStrings")
    protected FlexFieldStringType flexFieldStrings;
    @XmlElement(name = "FlexFieldNumbers")
    protected FlexFieldNumberType flexFieldNumbers;
    @XmlElement(name = "FlexFieldDates")
    protected FlexFieldDateType flexFieldDates;
    @XmlElement(name = "ShipmentGid")
    protected GLogXMLGidType shipmentGid;
    @XmlElement(name = "ShipmentGroupGid")
    protected GLogXMLGidType shipmentGroupGid;
    @XmlElement(name = "ShipmentGroupTypeGid")
    protected GLogXMLGidType shipmentGroupTypeGid;
    @XmlElement(name = "ReleaseLineGid")
    protected GLogXMLGidType releaseLineGid;
    @XmlElement(name = "ORLineRefnum")
    protected ORLineRefnumType orLineRefnum;
    @XmlElement(name = "MatchObjInvolvedParty")
    protected List<MatchObjInvolvedPartyType> matchObjInvolvedParty;

    /**
     * Obtém o valor da propriedade sendReason.
     * 
     * @return
     *     possible object is
     *     {@link SendReasonType }
     *     
     */
    public SendReasonType getSendReason() {
        return sendReason;
    }

    /**
     * Define o valor da propriedade sendReason.
     * 
     * @param value
     *     allowed object is
     *     {@link SendReasonType }
     *     
     */
    public void setSendReason(SendReasonType value) {
        this.sendReason = value;
    }

    /**
     * Obtém o valor da propriedade serviceProviderAlias.
     * 
     * @return
     *     possible object is
     *     {@link ServiceProviderAliasType }
     *     
     */
    public ServiceProviderAliasType getServiceProviderAlias() {
        return serviceProviderAlias;
    }

    /**
     * Define o valor da propriedade serviceProviderAlias.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceProviderAliasType }
     *     
     */
    public void setServiceProviderAlias(ServiceProviderAliasType value) {
        this.serviceProviderAlias = value;
    }

    /**
     * Obtém o valor da propriedade intSavedQuery.
     * 
     * @return
     *     possible object is
     *     {@link IntSavedQueryType }
     *     
     */
    public IntSavedQueryType getIntSavedQuery() {
        return intSavedQuery;
    }

    /**
     * Define o valor da propriedade intSavedQuery.
     * 
     * @param value
     *     allowed object is
     *     {@link IntSavedQueryType }
     *     
     */
    public void setIntSavedQuery(IntSavedQueryType value) {
        this.intSavedQuery = value;
    }

    /**
     * Gets the value of the shipmentRefnum property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the shipmentRefnum property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getShipmentRefnum().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ShipmentRefnumType }
     * 
     * 
     */
    public List<ShipmentRefnumType> getShipmentRefnum() {
        if (shipmentRefnum == null) {
            shipmentRefnum = new ArrayList<ShipmentRefnumType>();
        }
        return this.shipmentRefnum;
    }

    /**
     * Gets the value of the shipmentGroupRefnum property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the shipmentGroupRefnum property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getShipmentGroupRefnum().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ShipmentGroupRefnumType }
     * 
     * 
     */
    public List<ShipmentGroupRefnumType> getShipmentGroupRefnum() {
        if (shipmentGroupRefnum == null) {
            shipmentGroupRefnum = new ArrayList<ShipmentGroupRefnumType>();
        }
        return this.shipmentGroupRefnum;
    }

    /**
     * Obtém o valor da propriedade shipmentStatusType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipmentStatusType() {
        return shipmentStatusType;
    }

    /**
     * Define o valor da propriedade shipmentStatusType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipmentStatusType(String value) {
        this.shipmentStatusType = value;
    }

    /**
     * Obtém o valor da propriedade description.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Define o valor da propriedade description.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Obtém o valor da propriedade statusLevel.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatusLevel() {
        return statusLevel;
    }

    /**
     * Define o valor da propriedade statusLevel.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatusLevel(String value) {
        this.statusLevel = value;
    }

    /**
     * Obtém o valor da propriedade vesselInfo.
     * 
     * @return
     *     possible object is
     *     {@link VesselInfoType }
     *     
     */
    public VesselInfoType getVesselInfo() {
        return vesselInfo;
    }

    /**
     * Define o valor da propriedade vesselInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link VesselInfoType }
     *     
     */
    public void setVesselInfo(VesselInfoType value) {
        this.vesselInfo = value;
    }

    /**
     * Obtém o valor da propriedade railInfo.
     * 
     * @return
     *     possible object is
     *     {@link RailInfoType }
     *     
     */
    public RailInfoType getRailInfo() {
        return railInfo;
    }

    /**
     * Define o valor da propriedade railInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link RailInfoType }
     *     
     */
    public void setRailInfo(RailInfoType value) {
        this.railInfo = value;
    }

    /**
     * Obtém o valor da propriedade shipOrSailDt.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getShipOrSailDt() {
        return shipOrSailDt;
    }

    /**
     * Define o valor da propriedade shipOrSailDt.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setShipOrSailDt(GLogDateTimeType value) {
        this.shipOrSailDt = value;
    }

    /**
     * Obtém o valor da propriedade dischargeOrCompletionDt.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getDischargeOrCompletionDt() {
        return dischargeOrCompletionDt;
    }

    /**
     * Define o valor da propriedade dischargeOrCompletionDt.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setDischargeOrCompletionDt(GLogDateTimeType value) {
        this.dischargeOrCompletionDt = value;
    }

    /**
     * Obtém o valor da propriedade flightOrVoyageNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlightOrVoyageNumber() {
        return flightOrVoyageNumber;
    }

    /**
     * Define o valor da propriedade flightOrVoyageNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlightOrVoyageNumber(String value) {
        this.flightOrVoyageNumber = value;
    }

    /**
     * Obtém o valor da propriedade weightVolume.
     * 
     * @return
     *     possible object is
     *     {@link WeightVolumeType }
     *     
     */
    public WeightVolumeType getWeightVolume() {
        return weightVolume;
    }

    /**
     * Define o valor da propriedade weightVolume.
     * 
     * @param value
     *     allowed object is
     *     {@link WeightVolumeType }
     *     
     */
    public void setWeightVolume(WeightVolumeType value) {
        this.weightVolume = value;
    }

    /**
     * Gets the value of the inRouteCarriers property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the inRouteCarriers property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInRouteCarriers().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link InRouteCarriersType }
     * 
     * 
     */
    public List<InRouteCarriersType> getInRouteCarriers() {
        if (inRouteCarriers == null) {
            inRouteCarriers = new ArrayList<InRouteCarriersType>();
        }
        return this.inRouteCarriers;
    }

    /**
     * Obtém o valor da propriedade orderRefnum.
     * 
     * @return
     *     possible object is
     *     {@link OrderRefnumType }
     *     
     */
    public OrderRefnumType getOrderRefnum() {
        return orderRefnum;
    }

    /**
     * Define o valor da propriedade orderRefnum.
     * 
     * @param value
     *     allowed object is
     *     {@link OrderRefnumType }
     *     
     */
    public void setOrderRefnum(OrderRefnumType value) {
        this.orderRefnum = value;
    }

    /**
     * Gets the value of the marks property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the marks property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMarks().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MarksType }
     * 
     * 
     */
    public List<MarksType> getMarks() {
        if (marks == null) {
            marks = new ArrayList<MarksType>();
        }
        return this.marks;
    }

    /**
     * Obtém o valor da propriedade statusCodeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getStatusCodeGid() {
        return statusCodeGid;
    }

    /**
     * Define o valor da propriedade statusCodeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setStatusCodeGid(GLogXMLGidType value) {
        this.statusCodeGid = value;
    }

    /**
     * Obtém o valor da propriedade timeZoneGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getTimeZoneGid() {
        return timeZoneGid;
    }

    /**
     * Define o valor da propriedade timeZoneGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setTimeZoneGid(GLogXMLGidType value) {
        this.timeZoneGid = value;
    }

    /**
     * Obtém o valor da propriedade eventDt.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getEventDt() {
        return eventDt;
    }

    /**
     * Define o valor da propriedade eventDt.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setEventDt(GLogDateTimeType value) {
        this.eventDt = value;
    }

    /**
     * Obtém o valor da propriedade statusReasonCodeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getStatusReasonCodeGid() {
        return statusReasonCodeGid;
    }

    /**
     * Define o valor da propriedade statusReasonCodeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setStatusReasonCodeGid(GLogXMLGidType value) {
        this.statusReasonCodeGid = value;
    }

    /**
     * Obtém o valor da propriedade trainOrJCTOrBOCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTrainOrJCTOrBOCode() {
        return trainOrJCTOrBOCode;
    }

    /**
     * Define o valor da propriedade trainOrJCTOrBOCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTrainOrJCTOrBOCode(String value) {
        this.trainOrJCTOrBOCode = value;
    }

    /**
     * Gets the value of the ssEquipment property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the ssEquipment property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSSEquipment().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SSEquipmentType }
     * 
     * 
     */
    public List<SSEquipmentType> getSSEquipment() {
        if (ssEquipment == null) {
            ssEquipment = new ArrayList<SSEquipmentType>();
        }
        return this.ssEquipment;
    }

    /**
     * Gets the value of the sStatusSEquipment property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the sStatusSEquipment property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSStatusSEquipment().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SStatusSEquipmentType }
     * 
     * 
     */
    public List<SStatusSEquipmentType> getSStatusSEquipment() {
        if (sStatusSEquipment == null) {
            sStatusSEquipment = new ArrayList<SStatusSEquipmentType>();
        }
        return this.sStatusSEquipment;
    }

    /**
     * Obtém o valor da propriedade ssContact.
     * 
     * @return
     *     possible object is
     *     {@link SSContactType }
     *     
     */
    public SSContactType getSSContact() {
        return ssContact;
    }

    /**
     * Define o valor da propriedade ssContact.
     * 
     * @param value
     *     allowed object is
     *     {@link SSContactType }
     *     
     */
    public void setSSContact(SSContactType value) {
        this.ssContact = value;
    }

    /**
     * Gets the value of the ssRemarks property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the ssRemarks property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSSRemarks().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getSSRemarks() {
        if (ssRemarks == null) {
            ssRemarks = new ArrayList<String>();
        }
        return this.ssRemarks;
    }

    /**
     * Gets the value of the remark property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the remark property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRemark().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RemarkType }
     * 
     * 
     */
    public List<RemarkType> getRemark() {
        if (remark == null) {
            remark = new ArrayList<RemarkType>();
        }
        return this.remark;
    }

    /**
     * Obtém o valor da propriedade ssStop.
     * 
     * @return
     *     possible object is
     *     {@link SSStopType }
     *     
     */
    public SSStopType getSSStop() {
        return ssStop;
    }

    /**
     * Define o valor da propriedade ssStop.
     * 
     * @param value
     *     allowed object is
     *     {@link SSStopType }
     *     
     */
    public void setSSStop(SSStopType value) {
        this.ssStop = value;
    }

    /**
     * Gets the value of the sStatusShipUnit property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the sStatusShipUnit property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSStatusShipUnit().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SStatusShipUnitType }
     * 
     * 
     */
    public List<SStatusShipUnitType> getSStatusShipUnit() {
        if (sStatusShipUnit == null) {
            sStatusShipUnit = new ArrayList<SStatusShipUnitType>();
        }
        return this.sStatusShipUnit;
    }

    /**
     * Gets the value of the eventGroup property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the eventGroup property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEventGroup().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EventGroupType }
     * 
     * 
     */
    public List<EventGroupType> getEventGroup() {
        if (eventGroup == null) {
            eventGroup = new ArrayList<EventGroupType>();
        }
        return this.eventGroup;
    }

    /**
     * Obtém o valor da propriedade statusGroup.
     * 
     * @return
     *     possible object is
     *     {@link StatusGroupType }
     *     
     */
    public StatusGroupType getStatusGroup() {
        return statusGroup;
    }

    /**
     * Define o valor da propriedade statusGroup.
     * 
     * @param value
     *     allowed object is
     *     {@link StatusGroupType }
     *     
     */
    public void setStatusGroup(StatusGroupType value) {
        this.statusGroup = value;
    }

    /**
     * Obtém o valor da propriedade reasonGroup.
     * 
     * @return
     *     possible object is
     *     {@link ReasonGroupType }
     *     
     */
    public ReasonGroupType getReasonGroup() {
        return reasonGroup;
    }

    /**
     * Define o valor da propriedade reasonGroup.
     * 
     * @param value
     *     allowed object is
     *     {@link ReasonGroupType }
     *     
     */
    public void setReasonGroup(ReasonGroupType value) {
        this.reasonGroup = value;
    }

    /**
     * Obtém o valor da propriedade quickCodeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getQuickCodeGid() {
        return quickCodeGid;
    }

    /**
     * Define o valor da propriedade quickCodeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setQuickCodeGid(GLogXMLGidType value) {
        this.quickCodeGid = value;
    }

    /**
     * Obtém o valor da propriedade responsiblePartyGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getResponsiblePartyGid() {
        return responsiblePartyGid;
    }

    /**
     * Define o valor da propriedade responsiblePartyGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setResponsiblePartyGid(GLogXMLGidType value) {
        this.responsiblePartyGid = value;
    }

    /**
     * Obtém o valor da propriedade reportingUser.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReportingUser() {
        return reportingUser;
    }

    /**
     * Define o valor da propriedade reportingUser.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReportingUser(String value) {
        this.reportingUser = value;
    }

    /**
     * Obtém o valor da propriedade isContainerBased.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsContainerBased() {
        return isContainerBased;
    }

    /**
     * Define o valor da propriedade isContainerBased.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsContainerBased(String value) {
        this.isContainerBased = value;
    }

    /**
     * Obtém o valor da propriedade temperature.
     * 
     * @return
     *     possible object is
     *     {@link TemperatureType }
     *     
     */
    public TemperatureType getTemperature() {
        return temperature;
    }

    /**
     * Define o valor da propriedade temperature.
     * 
     * @param value
     *     allowed object is
     *     {@link TemperatureType }
     *     
     */
    public void setTemperature(TemperatureType value) {
        this.temperature = value;
    }

    /**
     * Obtém o valor da propriedade driverGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getDriverGid() {
        return driverGid;
    }

    /**
     * Define o valor da propriedade driverGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setDriverGid(GLogXMLGidType value) {
        this.driverGid = value;
    }

    /**
     * Obtém o valor da propriedade driverRefnum.
     * 
     * @return
     *     possible object is
     *     {@link DriverRefnumType }
     *     
     */
    public DriverRefnumType getDriverRefnum() {
        return driverRefnum;
    }

    /**
     * Define o valor da propriedade driverRefnum.
     * 
     * @param value
     *     allowed object is
     *     {@link DriverRefnumType }
     *     
     */
    public void setDriverRefnum(DriverRefnumType value) {
        this.driverRefnum = value;
    }

    /**
     * Obtém o valor da propriedade powerUnitGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getPowerUnitGid() {
        return powerUnitGid;
    }

    /**
     * Define o valor da propriedade powerUnitGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setPowerUnitGid(GLogXMLGidType value) {
        this.powerUnitGid = value;
    }

    /**
     * Obtém o valor da propriedade powerUnitRefnum.
     * 
     * @return
     *     possible object is
     *     {@link PowerUnitRefnumType }
     *     
     */
    public PowerUnitRefnumType getPowerUnitRefnum() {
        return powerUnitRefnum;
    }

    /**
     * Define o valor da propriedade powerUnitRefnum.
     * 
     * @param value
     *     allowed object is
     *     {@link PowerUnitRefnumType }
     *     
     */
    public void setPowerUnitRefnum(PowerUnitRefnumType value) {
        this.powerUnitRefnum = value;
    }

    /**
     * Obtém o valor da propriedade extDataSource.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExtDataSource() {
        return extDataSource;
    }

    /**
     * Define o valor da propriedade extDataSource.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExtDataSource(String value) {
        this.extDataSource = value;
    }

    /**
     * Obtém o valor da propriedade extEventRefNo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExtEventRefNo() {
        return extEventRefNo;
    }

    /**
     * Define o valor da propriedade extEventRefNo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExtEventRefNo(String value) {
        this.extEventRefNo = value;
    }

    /**
     * Obtém o valor da propriedade eventRecdDate.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getEventRecdDate() {
        return eventRecdDate;
    }

    /**
     * Define o valor da propriedade eventRecdDate.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setEventRecdDate(GLogDateTimeType value) {
        this.eventRecdDate = value;
    }

    /**
     * Obtém o valor da propriedade eventEndDate.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getEventEndDate() {
        return eventEndDate;
    }

    /**
     * Define o valor da propriedade eventEndDate.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setEventEndDate(GLogDateTimeType value) {
        this.eventEndDate = value;
    }

    /**
     * Obtém o valor da propriedade timeWorked.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public GLogXMLDurationType getTimeWorked() {
        return timeWorked;
    }

    /**
     * Define o valor da propriedade timeWorked.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public void setTimeWorked(GLogXMLDurationType value) {
        this.timeWorked = value;
    }

    /**
     * Obtém o valor da propriedade timeDriven.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public GLogXMLDurationType getTimeDriven() {
        return timeDriven;
    }

    /**
     * Define o valor da propriedade timeDriven.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public void setTimeDriven(GLogXMLDurationType value) {
        this.timeDriven = value;
    }

    /**
     * Obtém o valor da propriedade driverCalEventGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getDriverCalEventGid() {
        return driverCalEventGid;
    }

    /**
     * Define o valor da propriedade driverCalEventGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setDriverCalEventGid(GLogXMLGidType value) {
        this.driverCalEventGid = value;
    }

    /**
     * Gets the value of the shipStatusSpclService property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the shipStatusSpclService property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getShipStatusSpclService().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ShipStatusSpclServiceType }
     * 
     * 
     */
    public List<ShipStatusSpclServiceType> getShipStatusSpclService() {
        if (shipStatusSpclService == null) {
            shipStatusSpclService = new ArrayList<ShipStatusSpclServiceType>();
        }
        return this.shipStatusSpclService;
    }

    /**
     * Gets the value of the sStatusHOSRuleState property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the sStatusHOSRuleState property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSStatusHOSRuleState().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link HOSRuleState }
     * 
     * 
     */
    public List<HOSRuleState> getSStatusHOSRuleState() {
        if (sStatusHOSRuleState == null) {
            sStatusHOSRuleState = new ArrayList<HOSRuleState>();
        }
        return this.sStatusHOSRuleState;
    }

    /**
     * Obtém o valor da propriedade transOrderGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getTransOrderGid() {
        return transOrderGid;
    }

    /**
     * Define o valor da propriedade transOrderGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setTransOrderGid(GLogXMLGidType value) {
        this.transOrderGid = value;
    }

    /**
     * Obtém o valor da propriedade releaseGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getReleaseGid() {
        return releaseGid;
    }

    /**
     * Define o valor da propriedade releaseGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setReleaseGid(GLogXMLGidType value) {
        this.releaseGid = value;
    }

    /**
     * Obtém o valor da propriedade releaseRefnum.
     * 
     * @return
     *     possible object is
     *     {@link ReleaseRefnumType }
     *     
     */
    public ReleaseRefnumType getReleaseRefnum() {
        return releaseRefnum;
    }

    /**
     * Define o valor da propriedade releaseRefnum.
     * 
     * @param value
     *     allowed object is
     *     {@link ReleaseRefnumType }
     *     
     */
    public void setReleaseRefnum(ReleaseRefnumType value) {
        this.releaseRefnum = value;
    }

    /**
     * Gets the value of the obsLine property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the obsLine property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOBSLine().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LineGidType }
     * 
     * 
     */
    public List<LineGidType> getOBSLine() {
        if (obsLine == null) {
            obsLine = new ArrayList<LineGidType>();
        }
        return this.obsLine;
    }

    /**
     * Gets the value of the orsLine property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the orsLine property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getORSLine().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LineGidType }
     * 
     * 
     */
    public List<LineGidType> getORSLine() {
        if (orsLine == null) {
            orsLine = new ArrayList<LineGidType>();
        }
        return this.orsLine;
    }

    /**
     * Gets the value of the obsShipUnit property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the obsShipUnit property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOBSShipUnit().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ShipUnitCriteriaType }
     * 
     * 
     */
    public List<ShipUnitCriteriaType> getOBSShipUnit() {
        if (obsShipUnit == null) {
            obsShipUnit = new ArrayList<ShipUnitCriteriaType>();
        }
        return this.obsShipUnit;
    }

    /**
     * Gets the value of the orsShipUnit property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the orsShipUnit property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getORSShipUnit().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ShipUnitCriteriaType }
     * 
     * 
     */
    public List<ShipUnitCriteriaType> getORSShipUnit() {
        if (orsShipUnit == null) {
            orsShipUnit = new ArrayList<ShipUnitCriteriaType>();
        }
        return this.orsShipUnit;
    }

    /**
     * Gets the value of the ieDocumentResponse property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the ieDocumentResponse property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getIeDocumentResponse().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link IeDocumentResponseType }
     * 
     * 
     */
    public List<IeDocumentResponseType> getIeDocumentResponse() {
        if (ieDocumentResponse == null) {
            ieDocumentResponse = new ArrayList<IeDocumentResponseType>();
        }
        return this.ieDocumentResponse;
    }

    /**
     * Obtém o valor da propriedade trackingNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTrackingNumber() {
        return trackingNumber;
    }

    /**
     * Define o valor da propriedade trackingNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTrackingNumber(String value) {
        this.trackingNumber = value;
    }

    /**
     * Obtém o valor da propriedade filingResponseType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFilingResponseType() {
        return filingResponseType;
    }

    /**
     * Define o valor da propriedade filingResponseType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFilingResponseType(String value) {
        this.filingResponseType = value;
    }

    /**
     * Obtém o valor da propriedade flexFieldStrings.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldStringType }
     *     
     */
    public FlexFieldStringType getFlexFieldStrings() {
        return flexFieldStrings;
    }

    /**
     * Define o valor da propriedade flexFieldStrings.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldStringType }
     *     
     */
    public void setFlexFieldStrings(FlexFieldStringType value) {
        this.flexFieldStrings = value;
    }

    /**
     * Obtém o valor da propriedade flexFieldNumbers.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldNumberType }
     *     
     */
    public FlexFieldNumberType getFlexFieldNumbers() {
        return flexFieldNumbers;
    }

    /**
     * Define o valor da propriedade flexFieldNumbers.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldNumberType }
     *     
     */
    public void setFlexFieldNumbers(FlexFieldNumberType value) {
        this.flexFieldNumbers = value;
    }

    /**
     * Obtém o valor da propriedade flexFieldDates.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldDateType }
     *     
     */
    public FlexFieldDateType getFlexFieldDates() {
        return flexFieldDates;
    }

    /**
     * Define o valor da propriedade flexFieldDates.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldDateType }
     *     
     */
    public void setFlexFieldDates(FlexFieldDateType value) {
        this.flexFieldDates = value;
    }

    /**
     * Obtém o valor da propriedade shipmentGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getShipmentGid() {
        return shipmentGid;
    }

    /**
     * Define o valor da propriedade shipmentGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setShipmentGid(GLogXMLGidType value) {
        this.shipmentGid = value;
    }

    /**
     * Obtém o valor da propriedade shipmentGroupGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getShipmentGroupGid() {
        return shipmentGroupGid;
    }

    /**
     * Define o valor da propriedade shipmentGroupGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setShipmentGroupGid(GLogXMLGidType value) {
        this.shipmentGroupGid = value;
    }

    /**
     * Obtém o valor da propriedade shipmentGroupTypeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getShipmentGroupTypeGid() {
        return shipmentGroupTypeGid;
    }

    /**
     * Define o valor da propriedade shipmentGroupTypeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setShipmentGroupTypeGid(GLogXMLGidType value) {
        this.shipmentGroupTypeGid = value;
    }

    /**
     * Obtém o valor da propriedade releaseLineGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getReleaseLineGid() {
        return releaseLineGid;
    }

    /**
     * Define o valor da propriedade releaseLineGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setReleaseLineGid(GLogXMLGidType value) {
        this.releaseLineGid = value;
    }

    /**
     * Obtém o valor da propriedade orLineRefnum.
     * 
     * @return
     *     possible object is
     *     {@link ORLineRefnumType }
     *     
     */
    public ORLineRefnumType getORLineRefnum() {
        return orLineRefnum;
    }

    /**
     * Define o valor da propriedade orLineRefnum.
     * 
     * @param value
     *     allowed object is
     *     {@link ORLineRefnumType }
     *     
     */
    public void setORLineRefnum(ORLineRefnumType value) {
        this.orLineRefnum = value;
    }

    /**
     * Gets the value of the matchObjInvolvedParty property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the matchObjInvolvedParty property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMatchObjInvolvedParty().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MatchObjInvolvedPartyType }
     * 
     * 
     */
    public List<MatchObjInvolvedPartyType> getMatchObjInvolvedParty() {
        if (matchObjInvolvedParty == null) {
            matchObjInvolvedParty = new ArrayList<MatchObjInvolvedPartyType>();
        }
        return this.matchObjInvolvedParty;
    }

}
