
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;


/**
 * 
 *             The itinerary defines the path between two locations. It is described in terms of the legs.
 *             An itinerary can have one or more legs, and consists of the constraints for developing the shipment.
 *          
 * 
 * <p>Classe Java de ItineraryType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="ItineraryType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OTMTransactionIn">
 *       &lt;sequence>
 *         &lt;element name="ItineraryGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="TransactionCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransactionCodeType"/>
 *         &lt;element name="ReplaceChildren" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ReplaceChildrenType" minOccurs="0"/>
 *         &lt;element name="ItineraryName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MinWeightVolume" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}WeightVolumeType" minOccurs="0"/>
 *         &lt;element name="MaxWeightVolume" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}WeightVolumeType" minOccurs="0"/>
 *         &lt;element name="CorporationProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="CalendarGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="XLaneRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}XLaneRefType" minOccurs="0"/>
 *         &lt;element name="IsMultiStop" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Perspective" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="TotalCost" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType"/>
 *         &lt;element name="TotalTransitTime" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDurationType"/>
 *         &lt;element name="UseDeconsolidationPool" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="UseConsolidationPool" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MinStopoffWeightVolume" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}WeightVolumeType" minOccurs="0"/>
 *         &lt;element name="MaxPoolWeightVolume" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}WeightVolumeType" minOccurs="0"/>
 *         &lt;element name="MaxDistBtwPickupStops" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDistanceType" minOccurs="0"/>
 *         &lt;element name="MaxDistBtwDeliveryStops" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDistanceType" minOccurs="0"/>
 *         &lt;element name="RadiusForPickupStops" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDistanceType" minOccurs="0"/>
 *         &lt;element name="RadiusPctForPickupStops" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RadiusForDeliveryStops" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDistanceType" minOccurs="0"/>
 *         &lt;element name="RadiusPctForDeliveryStops" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TotalStopsConstraint" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PickupStopsConstraint" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DeliveryStopsConstraint" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MinTLWeightVolume" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}WeightVolumeType" minOccurs="0"/>
 *         &lt;element name="MinTLUsagePercentage" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MaxSmallDirectWeightVolume" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}WeightVolumeType" minOccurs="0"/>
 *         &lt;element name="MaxXDockWeightVolume" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}WeightVolumeType" minOccurs="0"/>
 *         &lt;element name="IsMatchConsolPoolToSource" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsMatchDeconsolPoolToDest" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsDestBundlePreferred" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="HazmatModeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="Rank" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="IsActive" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ItineraryType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="IsRule11" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="IncoTermProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="DepotProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="PaymentMethodProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="ItineraryDetail" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ItineraryDetailType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="FlexFieldStrings" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldStringType" minOccurs="0"/>
 *         &lt;element name="FlexFieldNumbers" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldNumberType" minOccurs="0"/>
 *         &lt;element name="FlexFieldDates" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldDateType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ItineraryType", propOrder = {
    "itineraryGid",
    "transactionCode",
    "replaceChildren",
    "itineraryName",
    "description",
    "minWeightVolume",
    "maxWeightVolume",
    "corporationProfileGid",
    "calendarGid",
    "xLaneRef",
    "isMultiStop",
    "perspective",
    "totalCost",
    "totalTransitTime",
    "useDeconsolidationPool",
    "useConsolidationPool",
    "minStopoffWeightVolume",
    "maxPoolWeightVolume",
    "maxDistBtwPickupStops",
    "maxDistBtwDeliveryStops",
    "radiusForPickupStops",
    "radiusPctForPickupStops",
    "radiusForDeliveryStops",
    "radiusPctForDeliveryStops",
    "totalStopsConstraint",
    "pickupStopsConstraint",
    "deliveryStopsConstraint",
    "minTLWeightVolume",
    "minTLUsagePercentage",
    "maxSmallDirectWeightVolume",
    "maxXDockWeightVolume",
    "isMatchConsolPoolToSource",
    "isMatchDeconsolPoolToDest",
    "isDestBundlePreferred",
    "hazmatModeGid",
    "rank",
    "isActive",
    "itineraryType",
    "isRule11",
    "incoTermProfileGid",
    "depotProfileGid",
    "paymentMethodProfileGid",
    "itineraryDetail",
    "flexFieldStrings",
    "flexFieldNumbers",
    "flexFieldDates"
})
public class ItineraryType
    extends OTMTransactionIn
{

    @XmlElement(name = "ItineraryGid", required = true)
    protected GLogXMLGidType itineraryGid;
    @XmlElement(name = "TransactionCode", required = true)
    @XmlSchemaType(name = "string")
    protected TransactionCodeType transactionCode;
    @XmlElement(name = "ReplaceChildren")
    protected ReplaceChildrenType replaceChildren;
    @XmlElement(name = "ItineraryName")
    protected String itineraryName;
    @XmlElement(name = "Description")
    protected String description;
    @XmlElement(name = "MinWeightVolume")
    protected WeightVolumeType minWeightVolume;
    @XmlElement(name = "MaxWeightVolume")
    protected WeightVolumeType maxWeightVolume;
    @XmlElement(name = "CorporationProfileGid")
    protected GLogXMLGidType corporationProfileGid;
    @XmlElement(name = "CalendarGid")
    protected GLogXMLGidType calendarGid;
    @XmlElement(name = "XLaneRef")
    protected XLaneRefType xLaneRef;
    @XmlElement(name = "IsMultiStop", required = true)
    protected String isMultiStop;
    @XmlElement(name = "Perspective", required = true)
    protected String perspective;
    @XmlElement(name = "TotalCost", required = true)
    protected GLogXMLFinancialAmountType totalCost;
    @XmlElement(name = "TotalTransitTime", required = true)
    protected GLogXMLDurationType totalTransitTime;
    @XmlElement(name = "UseDeconsolidationPool")
    protected String useDeconsolidationPool;
    @XmlElement(name = "UseConsolidationPool")
    protected String useConsolidationPool;
    @XmlElement(name = "MinStopoffWeightVolume")
    protected WeightVolumeType minStopoffWeightVolume;
    @XmlElement(name = "MaxPoolWeightVolume")
    protected WeightVolumeType maxPoolWeightVolume;
    @XmlElement(name = "MaxDistBtwPickupStops")
    protected GLogXMLDistanceType maxDistBtwPickupStops;
    @XmlElement(name = "MaxDistBtwDeliveryStops")
    protected GLogXMLDistanceType maxDistBtwDeliveryStops;
    @XmlElement(name = "RadiusForPickupStops")
    protected GLogXMLDistanceType radiusForPickupStops;
    @XmlElement(name = "RadiusPctForPickupStops")
    protected String radiusPctForPickupStops;
    @XmlElement(name = "RadiusForDeliveryStops")
    protected GLogXMLDistanceType radiusForDeliveryStops;
    @XmlElement(name = "RadiusPctForDeliveryStops")
    protected String radiusPctForDeliveryStops;
    @XmlElement(name = "TotalStopsConstraint")
    protected String totalStopsConstraint;
    @XmlElement(name = "PickupStopsConstraint")
    protected String pickupStopsConstraint;
    @XmlElement(name = "DeliveryStopsConstraint")
    protected String deliveryStopsConstraint;
    @XmlElement(name = "MinTLWeightVolume")
    protected WeightVolumeType minTLWeightVolume;
    @XmlElement(name = "MinTLUsagePercentage")
    protected String minTLUsagePercentage;
    @XmlElement(name = "MaxSmallDirectWeightVolume")
    protected WeightVolumeType maxSmallDirectWeightVolume;
    @XmlElement(name = "MaxXDockWeightVolume")
    protected WeightVolumeType maxXDockWeightVolume;
    @XmlElement(name = "IsMatchConsolPoolToSource")
    protected String isMatchConsolPoolToSource;
    @XmlElement(name = "IsMatchDeconsolPoolToDest")
    protected String isMatchDeconsolPoolToDest;
    @XmlElement(name = "IsDestBundlePreferred", required = true)
    protected String isDestBundlePreferred;
    @XmlElement(name = "HazmatModeGid")
    protected GLogXMLGidType hazmatModeGid;
    @XmlElement(name = "Rank", required = true)
    protected String rank;
    @XmlElement(name = "IsActive", required = true)
    protected String isActive;
    @XmlElement(name = "ItineraryType", required = true)
    protected String itineraryType;
    @XmlElement(name = "IsRule11", required = true)
    protected String isRule11;
    @XmlElement(name = "IncoTermProfileGid")
    protected GLogXMLGidType incoTermProfileGid;
    @XmlElement(name = "DepotProfileGid")
    protected GLogXMLGidType depotProfileGid;
    @XmlElement(name = "PaymentMethodProfileGid")
    protected GLogXMLGidType paymentMethodProfileGid;
    @XmlElement(name = "ItineraryDetail")
    protected List<ItineraryDetailType> itineraryDetail;
    @XmlElement(name = "FlexFieldStrings")
    protected FlexFieldStringType flexFieldStrings;
    @XmlElement(name = "FlexFieldNumbers")
    protected FlexFieldNumberType flexFieldNumbers;
    @XmlElement(name = "FlexFieldDates")
    protected FlexFieldDateType flexFieldDates;

    /**
     * Obtém o valor da propriedade itineraryGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getItineraryGid() {
        return itineraryGid;
    }

    /**
     * Define o valor da propriedade itineraryGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setItineraryGid(GLogXMLGidType value) {
        this.itineraryGid = value;
    }

    /**
     * Obtém o valor da propriedade transactionCode.
     * 
     * @return
     *     possible object is
     *     {@link TransactionCodeType }
     *     
     */
    public TransactionCodeType getTransactionCode() {
        return transactionCode;
    }

    /**
     * Define o valor da propriedade transactionCode.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionCodeType }
     *     
     */
    public void setTransactionCode(TransactionCodeType value) {
        this.transactionCode = value;
    }

    /**
     * Obtém o valor da propriedade replaceChildren.
     * 
     * @return
     *     possible object is
     *     {@link ReplaceChildrenType }
     *     
     */
    public ReplaceChildrenType getReplaceChildren() {
        return replaceChildren;
    }

    /**
     * Define o valor da propriedade replaceChildren.
     * 
     * @param value
     *     allowed object is
     *     {@link ReplaceChildrenType }
     *     
     */
    public void setReplaceChildren(ReplaceChildrenType value) {
        this.replaceChildren = value;
    }

    /**
     * Obtém o valor da propriedade itineraryName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItineraryName() {
        return itineraryName;
    }

    /**
     * Define o valor da propriedade itineraryName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItineraryName(String value) {
        this.itineraryName = value;
    }

    /**
     * Obtém o valor da propriedade description.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Define o valor da propriedade description.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Obtém o valor da propriedade minWeightVolume.
     * 
     * @return
     *     possible object is
     *     {@link WeightVolumeType }
     *     
     */
    public WeightVolumeType getMinWeightVolume() {
        return minWeightVolume;
    }

    /**
     * Define o valor da propriedade minWeightVolume.
     * 
     * @param value
     *     allowed object is
     *     {@link WeightVolumeType }
     *     
     */
    public void setMinWeightVolume(WeightVolumeType value) {
        this.minWeightVolume = value;
    }

    /**
     * Obtém o valor da propriedade maxWeightVolume.
     * 
     * @return
     *     possible object is
     *     {@link WeightVolumeType }
     *     
     */
    public WeightVolumeType getMaxWeightVolume() {
        return maxWeightVolume;
    }

    /**
     * Define o valor da propriedade maxWeightVolume.
     * 
     * @param value
     *     allowed object is
     *     {@link WeightVolumeType }
     *     
     */
    public void setMaxWeightVolume(WeightVolumeType value) {
        this.maxWeightVolume = value;
    }

    /**
     * Obtém o valor da propriedade corporationProfileGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getCorporationProfileGid() {
        return corporationProfileGid;
    }

    /**
     * Define o valor da propriedade corporationProfileGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setCorporationProfileGid(GLogXMLGidType value) {
        this.corporationProfileGid = value;
    }

    /**
     * Obtém o valor da propriedade calendarGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getCalendarGid() {
        return calendarGid;
    }

    /**
     * Define o valor da propriedade calendarGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setCalendarGid(GLogXMLGidType value) {
        this.calendarGid = value;
    }

    /**
     * Obtém o valor da propriedade xLaneRef.
     * 
     * @return
     *     possible object is
     *     {@link XLaneRefType }
     *     
     */
    public XLaneRefType getXLaneRef() {
        return xLaneRef;
    }

    /**
     * Define o valor da propriedade xLaneRef.
     * 
     * @param value
     *     allowed object is
     *     {@link XLaneRefType }
     *     
     */
    public void setXLaneRef(XLaneRefType value) {
        this.xLaneRef = value;
    }

    /**
     * Obtém o valor da propriedade isMultiStop.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsMultiStop() {
        return isMultiStop;
    }

    /**
     * Define o valor da propriedade isMultiStop.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsMultiStop(String value) {
        this.isMultiStop = value;
    }

    /**
     * Obtém o valor da propriedade perspective.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPerspective() {
        return perspective;
    }

    /**
     * Define o valor da propriedade perspective.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPerspective(String value) {
        this.perspective = value;
    }

    /**
     * Obtém o valor da propriedade totalCost.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getTotalCost() {
        return totalCost;
    }

    /**
     * Define o valor da propriedade totalCost.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setTotalCost(GLogXMLFinancialAmountType value) {
        this.totalCost = value;
    }

    /**
     * Obtém o valor da propriedade totalTransitTime.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public GLogXMLDurationType getTotalTransitTime() {
        return totalTransitTime;
    }

    /**
     * Define o valor da propriedade totalTransitTime.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public void setTotalTransitTime(GLogXMLDurationType value) {
        this.totalTransitTime = value;
    }

    /**
     * Obtém o valor da propriedade useDeconsolidationPool.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUseDeconsolidationPool() {
        return useDeconsolidationPool;
    }

    /**
     * Define o valor da propriedade useDeconsolidationPool.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUseDeconsolidationPool(String value) {
        this.useDeconsolidationPool = value;
    }

    /**
     * Obtém o valor da propriedade useConsolidationPool.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUseConsolidationPool() {
        return useConsolidationPool;
    }

    /**
     * Define o valor da propriedade useConsolidationPool.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUseConsolidationPool(String value) {
        this.useConsolidationPool = value;
    }

    /**
     * Obtém o valor da propriedade minStopoffWeightVolume.
     * 
     * @return
     *     possible object is
     *     {@link WeightVolumeType }
     *     
     */
    public WeightVolumeType getMinStopoffWeightVolume() {
        return minStopoffWeightVolume;
    }

    /**
     * Define o valor da propriedade minStopoffWeightVolume.
     * 
     * @param value
     *     allowed object is
     *     {@link WeightVolumeType }
     *     
     */
    public void setMinStopoffWeightVolume(WeightVolumeType value) {
        this.minStopoffWeightVolume = value;
    }

    /**
     * Obtém o valor da propriedade maxPoolWeightVolume.
     * 
     * @return
     *     possible object is
     *     {@link WeightVolumeType }
     *     
     */
    public WeightVolumeType getMaxPoolWeightVolume() {
        return maxPoolWeightVolume;
    }

    /**
     * Define o valor da propriedade maxPoolWeightVolume.
     * 
     * @param value
     *     allowed object is
     *     {@link WeightVolumeType }
     *     
     */
    public void setMaxPoolWeightVolume(WeightVolumeType value) {
        this.maxPoolWeightVolume = value;
    }

    /**
     * Obtém o valor da propriedade maxDistBtwPickupStops.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDistanceType }
     *     
     */
    public GLogXMLDistanceType getMaxDistBtwPickupStops() {
        return maxDistBtwPickupStops;
    }

    /**
     * Define o valor da propriedade maxDistBtwPickupStops.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDistanceType }
     *     
     */
    public void setMaxDistBtwPickupStops(GLogXMLDistanceType value) {
        this.maxDistBtwPickupStops = value;
    }

    /**
     * Obtém o valor da propriedade maxDistBtwDeliveryStops.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDistanceType }
     *     
     */
    public GLogXMLDistanceType getMaxDistBtwDeliveryStops() {
        return maxDistBtwDeliveryStops;
    }

    /**
     * Define o valor da propriedade maxDistBtwDeliveryStops.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDistanceType }
     *     
     */
    public void setMaxDistBtwDeliveryStops(GLogXMLDistanceType value) {
        this.maxDistBtwDeliveryStops = value;
    }

    /**
     * Obtém o valor da propriedade radiusForPickupStops.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDistanceType }
     *     
     */
    public GLogXMLDistanceType getRadiusForPickupStops() {
        return radiusForPickupStops;
    }

    /**
     * Define o valor da propriedade radiusForPickupStops.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDistanceType }
     *     
     */
    public void setRadiusForPickupStops(GLogXMLDistanceType value) {
        this.radiusForPickupStops = value;
    }

    /**
     * Obtém o valor da propriedade radiusPctForPickupStops.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRadiusPctForPickupStops() {
        return radiusPctForPickupStops;
    }

    /**
     * Define o valor da propriedade radiusPctForPickupStops.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRadiusPctForPickupStops(String value) {
        this.radiusPctForPickupStops = value;
    }

    /**
     * Obtém o valor da propriedade radiusForDeliveryStops.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDistanceType }
     *     
     */
    public GLogXMLDistanceType getRadiusForDeliveryStops() {
        return radiusForDeliveryStops;
    }

    /**
     * Define o valor da propriedade radiusForDeliveryStops.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDistanceType }
     *     
     */
    public void setRadiusForDeliveryStops(GLogXMLDistanceType value) {
        this.radiusForDeliveryStops = value;
    }

    /**
     * Obtém o valor da propriedade radiusPctForDeliveryStops.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRadiusPctForDeliveryStops() {
        return radiusPctForDeliveryStops;
    }

    /**
     * Define o valor da propriedade radiusPctForDeliveryStops.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRadiusPctForDeliveryStops(String value) {
        this.radiusPctForDeliveryStops = value;
    }

    /**
     * Obtém o valor da propriedade totalStopsConstraint.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTotalStopsConstraint() {
        return totalStopsConstraint;
    }

    /**
     * Define o valor da propriedade totalStopsConstraint.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTotalStopsConstraint(String value) {
        this.totalStopsConstraint = value;
    }

    /**
     * Obtém o valor da propriedade pickupStopsConstraint.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPickupStopsConstraint() {
        return pickupStopsConstraint;
    }

    /**
     * Define o valor da propriedade pickupStopsConstraint.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPickupStopsConstraint(String value) {
        this.pickupStopsConstraint = value;
    }

    /**
     * Obtém o valor da propriedade deliveryStopsConstraint.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeliveryStopsConstraint() {
        return deliveryStopsConstraint;
    }

    /**
     * Define o valor da propriedade deliveryStopsConstraint.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeliveryStopsConstraint(String value) {
        this.deliveryStopsConstraint = value;
    }

    /**
     * Obtém o valor da propriedade minTLWeightVolume.
     * 
     * @return
     *     possible object is
     *     {@link WeightVolumeType }
     *     
     */
    public WeightVolumeType getMinTLWeightVolume() {
        return minTLWeightVolume;
    }

    /**
     * Define o valor da propriedade minTLWeightVolume.
     * 
     * @param value
     *     allowed object is
     *     {@link WeightVolumeType }
     *     
     */
    public void setMinTLWeightVolume(WeightVolumeType value) {
        this.minTLWeightVolume = value;
    }

    /**
     * Obtém o valor da propriedade minTLUsagePercentage.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMinTLUsagePercentage() {
        return minTLUsagePercentage;
    }

    /**
     * Define o valor da propriedade minTLUsagePercentage.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMinTLUsagePercentage(String value) {
        this.minTLUsagePercentage = value;
    }

    /**
     * Obtém o valor da propriedade maxSmallDirectWeightVolume.
     * 
     * @return
     *     possible object is
     *     {@link WeightVolumeType }
     *     
     */
    public WeightVolumeType getMaxSmallDirectWeightVolume() {
        return maxSmallDirectWeightVolume;
    }

    /**
     * Define o valor da propriedade maxSmallDirectWeightVolume.
     * 
     * @param value
     *     allowed object is
     *     {@link WeightVolumeType }
     *     
     */
    public void setMaxSmallDirectWeightVolume(WeightVolumeType value) {
        this.maxSmallDirectWeightVolume = value;
    }

    /**
     * Obtém o valor da propriedade maxXDockWeightVolume.
     * 
     * @return
     *     possible object is
     *     {@link WeightVolumeType }
     *     
     */
    public WeightVolumeType getMaxXDockWeightVolume() {
        return maxXDockWeightVolume;
    }

    /**
     * Define o valor da propriedade maxXDockWeightVolume.
     * 
     * @param value
     *     allowed object is
     *     {@link WeightVolumeType }
     *     
     */
    public void setMaxXDockWeightVolume(WeightVolumeType value) {
        this.maxXDockWeightVolume = value;
    }

    /**
     * Obtém o valor da propriedade isMatchConsolPoolToSource.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsMatchConsolPoolToSource() {
        return isMatchConsolPoolToSource;
    }

    /**
     * Define o valor da propriedade isMatchConsolPoolToSource.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsMatchConsolPoolToSource(String value) {
        this.isMatchConsolPoolToSource = value;
    }

    /**
     * Obtém o valor da propriedade isMatchDeconsolPoolToDest.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsMatchDeconsolPoolToDest() {
        return isMatchDeconsolPoolToDest;
    }

    /**
     * Define o valor da propriedade isMatchDeconsolPoolToDest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsMatchDeconsolPoolToDest(String value) {
        this.isMatchDeconsolPoolToDest = value;
    }

    /**
     * Obtém o valor da propriedade isDestBundlePreferred.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsDestBundlePreferred() {
        return isDestBundlePreferred;
    }

    /**
     * Define o valor da propriedade isDestBundlePreferred.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsDestBundlePreferred(String value) {
        this.isDestBundlePreferred = value;
    }

    /**
     * Obtém o valor da propriedade hazmatModeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getHazmatModeGid() {
        return hazmatModeGid;
    }

    /**
     * Define o valor da propriedade hazmatModeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setHazmatModeGid(GLogXMLGidType value) {
        this.hazmatModeGid = value;
    }

    /**
     * Obtém o valor da propriedade rank.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRank() {
        return rank;
    }

    /**
     * Define o valor da propriedade rank.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRank(String value) {
        this.rank = value;
    }

    /**
     * Obtém o valor da propriedade isActive.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsActive() {
        return isActive;
    }

    /**
     * Define o valor da propriedade isActive.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsActive(String value) {
        this.isActive = value;
    }

    /**
     * Obtém o valor da propriedade itineraryType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItineraryType() {
        return itineraryType;
    }

    /**
     * Define o valor da propriedade itineraryType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItineraryType(String value) {
        this.itineraryType = value;
    }

    /**
     * Obtém o valor da propriedade isRule11.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsRule11() {
        return isRule11;
    }

    /**
     * Define o valor da propriedade isRule11.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsRule11(String value) {
        this.isRule11 = value;
    }

    /**
     * Obtém o valor da propriedade incoTermProfileGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getIncoTermProfileGid() {
        return incoTermProfileGid;
    }

    /**
     * Define o valor da propriedade incoTermProfileGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setIncoTermProfileGid(GLogXMLGidType value) {
        this.incoTermProfileGid = value;
    }

    /**
     * Obtém o valor da propriedade depotProfileGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getDepotProfileGid() {
        return depotProfileGid;
    }

    /**
     * Define o valor da propriedade depotProfileGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setDepotProfileGid(GLogXMLGidType value) {
        this.depotProfileGid = value;
    }

    /**
     * Obtém o valor da propriedade paymentMethodProfileGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getPaymentMethodProfileGid() {
        return paymentMethodProfileGid;
    }

    /**
     * Define o valor da propriedade paymentMethodProfileGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setPaymentMethodProfileGid(GLogXMLGidType value) {
        this.paymentMethodProfileGid = value;
    }

    /**
     * Gets the value of the itineraryDetail property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the itineraryDetail property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getItineraryDetail().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ItineraryDetailType }
     * 
     * 
     */
    public List<ItineraryDetailType> getItineraryDetail() {
        if (itineraryDetail == null) {
            itineraryDetail = new ArrayList<ItineraryDetailType>();
        }
        return this.itineraryDetail;
    }

    /**
     * Obtém o valor da propriedade flexFieldStrings.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldStringType }
     *     
     */
    public FlexFieldStringType getFlexFieldStrings() {
        return flexFieldStrings;
    }

    /**
     * Define o valor da propriedade flexFieldStrings.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldStringType }
     *     
     */
    public void setFlexFieldStrings(FlexFieldStringType value) {
        this.flexFieldStrings = value;
    }

    /**
     * Obtém o valor da propriedade flexFieldNumbers.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldNumberType }
     *     
     */
    public FlexFieldNumberType getFlexFieldNumbers() {
        return flexFieldNumbers;
    }

    /**
     * Define o valor da propriedade flexFieldNumbers.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldNumberType }
     *     
     */
    public void setFlexFieldNumbers(FlexFieldNumberType value) {
        this.flexFieldNumbers = value;
    }

    /**
     * Obtém o valor da propriedade flexFieldDates.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldDateType }
     *     
     */
    public FlexFieldDateType getFlexFieldDates() {
        return flexFieldDates;
    }

    /**
     * Define o valor da propriedade flexFieldDates.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldDateType }
     *     
     */
    public void setFlexFieldDates(FlexFieldDateType value) {
        this.flexFieldDates = value;
    }

}
