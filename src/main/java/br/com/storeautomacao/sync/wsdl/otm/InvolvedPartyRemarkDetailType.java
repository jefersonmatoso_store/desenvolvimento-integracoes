
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * (Inbound) Remarks for the party involved.
 *          
 * 
 * <p>Classe Java de InvolvedPartyRemarkDetailType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="InvolvedPartyRemarkDetailType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PartyQualifierGidForRemark" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="PartyRemarkQualifierGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="PartyRemarkText" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InvolvedPartyRemarkDetailType", namespace = "http://xmlns.oracle.com/apps/gtm/transmission/v6.4", propOrder = {
    "partyQualifierGidForRemark",
    "partyRemarkQualifierGid",
    "partyRemarkText"
})
public class InvolvedPartyRemarkDetailType {

    @XmlElement(name = "PartyQualifierGidForRemark", required = true)
    protected GLogXMLGidType partyQualifierGidForRemark;
    @XmlElement(name = "PartyRemarkQualifierGid", required = true)
    protected GLogXMLGidType partyRemarkQualifierGid;
    @XmlElement(name = "PartyRemarkText", required = true)
    protected String partyRemarkText;

    /**
     * Obtém o valor da propriedade partyQualifierGidForRemark.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getPartyQualifierGidForRemark() {
        return partyQualifierGidForRemark;
    }

    /**
     * Define o valor da propriedade partyQualifierGidForRemark.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setPartyQualifierGidForRemark(GLogXMLGidType value) {
        this.partyQualifierGidForRemark = value;
    }

    /**
     * Obtém o valor da propriedade partyRemarkQualifierGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getPartyRemarkQualifierGid() {
        return partyRemarkQualifierGid;
    }

    /**
     * Define o valor da propriedade partyRemarkQualifierGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setPartyRemarkQualifierGid(GLogXMLGidType value) {
        this.partyRemarkQualifierGid = value;
    }

    /**
     * Obtém o valor da propriedade partyRemarkText.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPartyRemarkText() {
        return partyRemarkText;
    }

    /**
     * Define o valor da propriedade partyRemarkText.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPartyRemarkText(String value) {
        this.partyRemarkText = value;
    }

}
