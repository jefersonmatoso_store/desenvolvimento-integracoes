
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de AckSpecType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="AckSpecType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ComMethodGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="EmailAddress" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SmtpHost" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ServletURL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AckOption" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ContactGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AckSpecType", propOrder = {
    "comMethodGid",
    "emailAddress",
    "smtpHost",
    "servletURL",
    "ackOption",
    "contactGid"
})
public class AckSpecType {

    @XmlElement(name = "ComMethodGid", required = true)
    protected GLogXMLGidType comMethodGid;
    @XmlElement(name = "EmailAddress")
    protected String emailAddress;
    @XmlElement(name = "SmtpHost")
    protected String smtpHost;
    @XmlElement(name = "ServletURL")
    protected String servletURL;
    @XmlElement(name = "AckOption")
    protected String ackOption;
    @XmlElement(name = "ContactGid")
    protected GLogXMLGidType contactGid;

    /**
     * Obtém o valor da propriedade comMethodGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getComMethodGid() {
        return comMethodGid;
    }

    /**
     * Define o valor da propriedade comMethodGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setComMethodGid(GLogXMLGidType value) {
        this.comMethodGid = value;
    }

    /**
     * Obtém o valor da propriedade emailAddress.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmailAddress() {
        return emailAddress;
    }

    /**
     * Define o valor da propriedade emailAddress.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmailAddress(String value) {
        this.emailAddress = value;
    }

    /**
     * Obtém o valor da propriedade smtpHost.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSmtpHost() {
        return smtpHost;
    }

    /**
     * Define o valor da propriedade smtpHost.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSmtpHost(String value) {
        this.smtpHost = value;
    }

    /**
     * Obtém o valor da propriedade servletURL.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServletURL() {
        return servletURL;
    }

    /**
     * Define o valor da propriedade servletURL.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServletURL(String value) {
        this.servletURL = value;
    }

    /**
     * Obtém o valor da propriedade ackOption.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAckOption() {
        return ackOption;
    }

    /**
     * Define o valor da propriedade ackOption.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAckOption(String value) {
        this.ackOption = value;
    }

    /**
     * Obtém o valor da propriedade contactGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getContactGid() {
        return contactGid;
    }

    /**
     * Define o valor da propriedade contactGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setContactGid(GLogXMLGidType value) {
        this.contactGid = value;
    }

}
