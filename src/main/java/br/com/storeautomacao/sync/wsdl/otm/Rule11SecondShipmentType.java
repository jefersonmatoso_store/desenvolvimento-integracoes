
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Contains the information from the second Shipment in a Rail Rule 11 move.
 * 
 * <p>Classe Java de Rule11SecondShipmentType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="Rule11SecondShipmentType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ShipmentRefnum" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ShipmentRefnumType" minOccurs="0"/>
 *         &lt;element name="Rule11SecShipRateOffering" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}Rule11SecShipRateOfferingType" minOccurs="0"/>
 *         &lt;element name="Rule11SecShipRateGeo" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}Rule11SecShipRateGeoType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Rule11SecondShipmentType", propOrder = {
    "shipmentRefnum",
    "rule11SecShipRateOffering",
    "rule11SecShipRateGeo"
})
public class Rule11SecondShipmentType {

    @XmlElement(name = "ShipmentRefnum")
    protected ShipmentRefnumType shipmentRefnum;
    @XmlElement(name = "Rule11SecShipRateOffering")
    protected Rule11SecShipRateOfferingType rule11SecShipRateOffering;
    @XmlElement(name = "Rule11SecShipRateGeo")
    protected Rule11SecShipRateGeoType rule11SecShipRateGeo;

    /**
     * Obtém o valor da propriedade shipmentRefnum.
     * 
     * @return
     *     possible object is
     *     {@link ShipmentRefnumType }
     *     
     */
    public ShipmentRefnumType getShipmentRefnum() {
        return shipmentRefnum;
    }

    /**
     * Define o valor da propriedade shipmentRefnum.
     * 
     * @param value
     *     allowed object is
     *     {@link ShipmentRefnumType }
     *     
     */
    public void setShipmentRefnum(ShipmentRefnumType value) {
        this.shipmentRefnum = value;
    }

    /**
     * Obtém o valor da propriedade rule11SecShipRateOffering.
     * 
     * @return
     *     possible object is
     *     {@link Rule11SecShipRateOfferingType }
     *     
     */
    public Rule11SecShipRateOfferingType getRule11SecShipRateOffering() {
        return rule11SecShipRateOffering;
    }

    /**
     * Define o valor da propriedade rule11SecShipRateOffering.
     * 
     * @param value
     *     allowed object is
     *     {@link Rule11SecShipRateOfferingType }
     *     
     */
    public void setRule11SecShipRateOffering(Rule11SecShipRateOfferingType value) {
        this.rule11SecShipRateOffering = value;
    }

    /**
     * Obtém o valor da propriedade rule11SecShipRateGeo.
     * 
     * @return
     *     possible object is
     *     {@link Rule11SecShipRateGeoType }
     *     
     */
    public Rule11SecShipRateGeoType getRule11SecShipRateGeo() {
        return rule11SecShipRateGeo;
    }

    /**
     * Define o valor da propriedade rule11SecShipRateGeo.
     * 
     * @param value
     *     allowed object is
     *     {@link Rule11SecShipRateGeoType }
     *     
     */
    public void setRule11SecShipRateGeo(Rule11SecShipRateGeoType value) {
        this.rule11SecShipRateGeo = value;
    }

}
