
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * This element is used to identify item packaging information.
 * 
 *             Note: When the PackagedItemSpecRef element is specified in the Packaging element, it should not be specified
 *             again in the PackagingShipUnit element.
 * 
 *          
 * 
 * <p>Classe Java de PackagingType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="PackagingType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PackagedItemGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="HazmatPackageTypeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PackagedItemSpecRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLShipUnitSpecRefType" minOccurs="0"/>
 *         &lt;element name="InnerPackCount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PackageShipUnitWeightVolume" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}WeightVolumeType" minOccurs="0"/>
 *         &lt;element name="PackageShipUnitLWH" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LengthWidthHeightType" minOccurs="0"/>
 *         &lt;element name="PackageShipUnitDiameter" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}DiameterType" minOccurs="0"/>
 *         &lt;element name="CoreDiameter" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDiameterType" minOccurs="0"/>
 *         &lt;element name="IsHandlingUnitStackable" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InnerPackInfo" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}InnerPackInfoType" minOccurs="0"/>
 *         &lt;element name="IsDefaultPackaging" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsHazardous" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PackageItemTypeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="LoadConfigRuleRank" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ShipUnitSpecProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="Refnum" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RefnumType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="PackagingShipUnit" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}PackagingShipUnitType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="PkgItemEquipRefUnit" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}PkgItemEquipRefUnitType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="FlexFieldStrings" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldStringType" minOccurs="0"/>
 *         &lt;element name="FlexFieldNumbers" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldNumberType" minOccurs="0"/>
 *         &lt;element name="FlexFieldDates" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldDateType" minOccurs="0"/>
 *         &lt;element name="PackagingInnerPack" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}PackagingInnerPackType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="PkgItemPackageRefUnit" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}PkgItemPackageRefUnitType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="IsAllowMixedFreight" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BulkMixingFamilyGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="CompartmentTypeProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="Priority" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CategoryGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PackagingType", propOrder = {
    "packagedItemGid",
    "hazmatPackageTypeGid",
    "description",
    "packagedItemSpecRef",
    "innerPackCount",
    "packageShipUnitWeightVolume",
    "packageShipUnitLWH",
    "packageShipUnitDiameter",
    "coreDiameter",
    "isHandlingUnitStackable",
    "innerPackInfo",
    "isDefaultPackaging",
    "isHazardous",
    "packageItemTypeGid",
    "loadConfigRuleRank",
    "shipUnitSpecProfileGid",
    "refnum",
    "packagingShipUnit",
    "pkgItemEquipRefUnit",
    "flexFieldStrings",
    "flexFieldNumbers",
    "flexFieldDates",
    "packagingInnerPack",
    "pkgItemPackageRefUnit",
    "isAllowMixedFreight",
    "bulkMixingFamilyGid",
    "compartmentTypeProfileGid",
    "priority",
    "categoryGid"
})
public class PackagingType {

    @XmlElement(name = "PackagedItemGid")
    protected GLogXMLGidType packagedItemGid;
    @XmlElement(name = "HazmatPackageTypeGid")
    protected GLogXMLGidType hazmatPackageTypeGid;
    @XmlElement(name = "Description")
    protected String description;
    @XmlElement(name = "PackagedItemSpecRef")
    protected GLogXMLShipUnitSpecRefType packagedItemSpecRef;
    @XmlElement(name = "InnerPackCount")
    protected String innerPackCount;
    @XmlElement(name = "PackageShipUnitWeightVolume")
    protected WeightVolumeType packageShipUnitWeightVolume;
    @XmlElement(name = "PackageShipUnitLWH")
    protected LengthWidthHeightType packageShipUnitLWH;
    @XmlElement(name = "PackageShipUnitDiameter")
    protected DiameterType packageShipUnitDiameter;
    @XmlElement(name = "CoreDiameter")
    protected GLogXMLDiameterType coreDiameter;
    @XmlElement(name = "IsHandlingUnitStackable")
    protected String isHandlingUnitStackable;
    @XmlElement(name = "InnerPackInfo")
    protected InnerPackInfoType innerPackInfo;
    @XmlElement(name = "IsDefaultPackaging")
    protected String isDefaultPackaging;
    @XmlElement(name = "IsHazardous")
    protected String isHazardous;
    @XmlElement(name = "PackageItemTypeGid")
    protected GLogXMLGidType packageItemTypeGid;
    @XmlElement(name = "LoadConfigRuleRank")
    protected String loadConfigRuleRank;
    @XmlElement(name = "ShipUnitSpecProfileGid")
    protected GLogXMLGidType shipUnitSpecProfileGid;
    @XmlElement(name = "Refnum")
    protected List<RefnumType> refnum;
    @XmlElement(name = "PackagingShipUnit")
    protected List<PackagingShipUnitType> packagingShipUnit;
    @XmlElement(name = "PkgItemEquipRefUnit")
    protected List<PkgItemEquipRefUnitType> pkgItemEquipRefUnit;
    @XmlElement(name = "FlexFieldStrings")
    protected FlexFieldStringType flexFieldStrings;
    @XmlElement(name = "FlexFieldNumbers")
    protected FlexFieldNumberType flexFieldNumbers;
    @XmlElement(name = "FlexFieldDates")
    protected FlexFieldDateType flexFieldDates;
    @XmlElement(name = "PackagingInnerPack")
    protected List<PackagingInnerPackType> packagingInnerPack;
    @XmlElement(name = "PkgItemPackageRefUnit")
    protected List<PkgItemPackageRefUnitType> pkgItemPackageRefUnit;
    @XmlElement(name = "IsAllowMixedFreight")
    protected String isAllowMixedFreight;
    @XmlElement(name = "BulkMixingFamilyGid")
    protected GLogXMLGidType bulkMixingFamilyGid;
    @XmlElement(name = "CompartmentTypeProfileGid")
    protected GLogXMLGidType compartmentTypeProfileGid;
    @XmlElement(name = "Priority")
    protected String priority;
    @XmlElement(name = "CategoryGid")
    protected GLogXMLGidType categoryGid;

    /**
     * Obtém o valor da propriedade packagedItemGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getPackagedItemGid() {
        return packagedItemGid;
    }

    /**
     * Define o valor da propriedade packagedItemGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setPackagedItemGid(GLogXMLGidType value) {
        this.packagedItemGid = value;
    }

    /**
     * Obtém o valor da propriedade hazmatPackageTypeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getHazmatPackageTypeGid() {
        return hazmatPackageTypeGid;
    }

    /**
     * Define o valor da propriedade hazmatPackageTypeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setHazmatPackageTypeGid(GLogXMLGidType value) {
        this.hazmatPackageTypeGid = value;
    }

    /**
     * Obtém o valor da propriedade description.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Define o valor da propriedade description.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Obtém o valor da propriedade packagedItemSpecRef.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLShipUnitSpecRefType }
     *     
     */
    public GLogXMLShipUnitSpecRefType getPackagedItemSpecRef() {
        return packagedItemSpecRef;
    }

    /**
     * Define o valor da propriedade packagedItemSpecRef.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLShipUnitSpecRefType }
     *     
     */
    public void setPackagedItemSpecRef(GLogXMLShipUnitSpecRefType value) {
        this.packagedItemSpecRef = value;
    }

    /**
     * Obtém o valor da propriedade innerPackCount.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInnerPackCount() {
        return innerPackCount;
    }

    /**
     * Define o valor da propriedade innerPackCount.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInnerPackCount(String value) {
        this.innerPackCount = value;
    }

    /**
     * Obtém o valor da propriedade packageShipUnitWeightVolume.
     * 
     * @return
     *     possible object is
     *     {@link WeightVolumeType }
     *     
     */
    public WeightVolumeType getPackageShipUnitWeightVolume() {
        return packageShipUnitWeightVolume;
    }

    /**
     * Define o valor da propriedade packageShipUnitWeightVolume.
     * 
     * @param value
     *     allowed object is
     *     {@link WeightVolumeType }
     *     
     */
    public void setPackageShipUnitWeightVolume(WeightVolumeType value) {
        this.packageShipUnitWeightVolume = value;
    }

    /**
     * Obtém o valor da propriedade packageShipUnitLWH.
     * 
     * @return
     *     possible object is
     *     {@link LengthWidthHeightType }
     *     
     */
    public LengthWidthHeightType getPackageShipUnitLWH() {
        return packageShipUnitLWH;
    }

    /**
     * Define o valor da propriedade packageShipUnitLWH.
     * 
     * @param value
     *     allowed object is
     *     {@link LengthWidthHeightType }
     *     
     */
    public void setPackageShipUnitLWH(LengthWidthHeightType value) {
        this.packageShipUnitLWH = value;
    }

    /**
     * Obtém o valor da propriedade packageShipUnitDiameter.
     * 
     * @return
     *     possible object is
     *     {@link DiameterType }
     *     
     */
    public DiameterType getPackageShipUnitDiameter() {
        return packageShipUnitDiameter;
    }

    /**
     * Define o valor da propriedade packageShipUnitDiameter.
     * 
     * @param value
     *     allowed object is
     *     {@link DiameterType }
     *     
     */
    public void setPackageShipUnitDiameter(DiameterType value) {
        this.packageShipUnitDiameter = value;
    }

    /**
     * Obtém o valor da propriedade coreDiameter.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDiameterType }
     *     
     */
    public GLogXMLDiameterType getCoreDiameter() {
        return coreDiameter;
    }

    /**
     * Define o valor da propriedade coreDiameter.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDiameterType }
     *     
     */
    public void setCoreDiameter(GLogXMLDiameterType value) {
        this.coreDiameter = value;
    }

    /**
     * Obtém o valor da propriedade isHandlingUnitStackable.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsHandlingUnitStackable() {
        return isHandlingUnitStackable;
    }

    /**
     * Define o valor da propriedade isHandlingUnitStackable.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsHandlingUnitStackable(String value) {
        this.isHandlingUnitStackable = value;
    }

    /**
     * Obtém o valor da propriedade innerPackInfo.
     * 
     * @return
     *     possible object is
     *     {@link InnerPackInfoType }
     *     
     */
    public InnerPackInfoType getInnerPackInfo() {
        return innerPackInfo;
    }

    /**
     * Define o valor da propriedade innerPackInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link InnerPackInfoType }
     *     
     */
    public void setInnerPackInfo(InnerPackInfoType value) {
        this.innerPackInfo = value;
    }

    /**
     * Obtém o valor da propriedade isDefaultPackaging.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsDefaultPackaging() {
        return isDefaultPackaging;
    }

    /**
     * Define o valor da propriedade isDefaultPackaging.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsDefaultPackaging(String value) {
        this.isDefaultPackaging = value;
    }

    /**
     * Obtém o valor da propriedade isHazardous.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsHazardous() {
        return isHazardous;
    }

    /**
     * Define o valor da propriedade isHazardous.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsHazardous(String value) {
        this.isHazardous = value;
    }

    /**
     * Obtém o valor da propriedade packageItemTypeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getPackageItemTypeGid() {
        return packageItemTypeGid;
    }

    /**
     * Define o valor da propriedade packageItemTypeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setPackageItemTypeGid(GLogXMLGidType value) {
        this.packageItemTypeGid = value;
    }

    /**
     * Obtém o valor da propriedade loadConfigRuleRank.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLoadConfigRuleRank() {
        return loadConfigRuleRank;
    }

    /**
     * Define o valor da propriedade loadConfigRuleRank.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLoadConfigRuleRank(String value) {
        this.loadConfigRuleRank = value;
    }

    /**
     * Obtém o valor da propriedade shipUnitSpecProfileGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getShipUnitSpecProfileGid() {
        return shipUnitSpecProfileGid;
    }

    /**
     * Define o valor da propriedade shipUnitSpecProfileGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setShipUnitSpecProfileGid(GLogXMLGidType value) {
        this.shipUnitSpecProfileGid = value;
    }

    /**
     * Gets the value of the refnum property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the refnum property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRefnum().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RefnumType }
     * 
     * 
     */
    public List<RefnumType> getRefnum() {
        if (refnum == null) {
            refnum = new ArrayList<RefnumType>();
        }
        return this.refnum;
    }

    /**
     * Gets the value of the packagingShipUnit property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the packagingShipUnit property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPackagingShipUnit().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PackagingShipUnitType }
     * 
     * 
     */
    public List<PackagingShipUnitType> getPackagingShipUnit() {
        if (packagingShipUnit == null) {
            packagingShipUnit = new ArrayList<PackagingShipUnitType>();
        }
        return this.packagingShipUnit;
    }

    /**
     * Gets the value of the pkgItemEquipRefUnit property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the pkgItemEquipRefUnit property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPkgItemEquipRefUnit().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PkgItemEquipRefUnitType }
     * 
     * 
     */
    public List<PkgItemEquipRefUnitType> getPkgItemEquipRefUnit() {
        if (pkgItemEquipRefUnit == null) {
            pkgItemEquipRefUnit = new ArrayList<PkgItemEquipRefUnitType>();
        }
        return this.pkgItemEquipRefUnit;
    }

    /**
     * Obtém o valor da propriedade flexFieldStrings.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldStringType }
     *     
     */
    public FlexFieldStringType getFlexFieldStrings() {
        return flexFieldStrings;
    }

    /**
     * Define o valor da propriedade flexFieldStrings.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldStringType }
     *     
     */
    public void setFlexFieldStrings(FlexFieldStringType value) {
        this.flexFieldStrings = value;
    }

    /**
     * Obtém o valor da propriedade flexFieldNumbers.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldNumberType }
     *     
     */
    public FlexFieldNumberType getFlexFieldNumbers() {
        return flexFieldNumbers;
    }

    /**
     * Define o valor da propriedade flexFieldNumbers.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldNumberType }
     *     
     */
    public void setFlexFieldNumbers(FlexFieldNumberType value) {
        this.flexFieldNumbers = value;
    }

    /**
     * Obtém o valor da propriedade flexFieldDates.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldDateType }
     *     
     */
    public FlexFieldDateType getFlexFieldDates() {
        return flexFieldDates;
    }

    /**
     * Define o valor da propriedade flexFieldDates.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldDateType }
     *     
     */
    public void setFlexFieldDates(FlexFieldDateType value) {
        this.flexFieldDates = value;
    }

    /**
     * Gets the value of the packagingInnerPack property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the packagingInnerPack property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPackagingInnerPack().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PackagingInnerPackType }
     * 
     * 
     */
    public List<PackagingInnerPackType> getPackagingInnerPack() {
        if (packagingInnerPack == null) {
            packagingInnerPack = new ArrayList<PackagingInnerPackType>();
        }
        return this.packagingInnerPack;
    }

    /**
     * Gets the value of the pkgItemPackageRefUnit property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the pkgItemPackageRefUnit property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPkgItemPackageRefUnit().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PkgItemPackageRefUnitType }
     * 
     * 
     */
    public List<PkgItemPackageRefUnitType> getPkgItemPackageRefUnit() {
        if (pkgItemPackageRefUnit == null) {
            pkgItemPackageRefUnit = new ArrayList<PkgItemPackageRefUnitType>();
        }
        return this.pkgItemPackageRefUnit;
    }

    /**
     * Obtém o valor da propriedade isAllowMixedFreight.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsAllowMixedFreight() {
        return isAllowMixedFreight;
    }

    /**
     * Define o valor da propriedade isAllowMixedFreight.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsAllowMixedFreight(String value) {
        this.isAllowMixedFreight = value;
    }

    /**
     * Obtém o valor da propriedade bulkMixingFamilyGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getBulkMixingFamilyGid() {
        return bulkMixingFamilyGid;
    }

    /**
     * Define o valor da propriedade bulkMixingFamilyGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setBulkMixingFamilyGid(GLogXMLGidType value) {
        this.bulkMixingFamilyGid = value;
    }

    /**
     * Obtém o valor da propriedade compartmentTypeProfileGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getCompartmentTypeProfileGid() {
        return compartmentTypeProfileGid;
    }

    /**
     * Define o valor da propriedade compartmentTypeProfileGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setCompartmentTypeProfileGid(GLogXMLGidType value) {
        this.compartmentTypeProfileGid = value;
    }

    /**
     * Obtém o valor da propriedade priority.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPriority() {
        return priority;
    }

    /**
     * Define o valor da propriedade priority.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPriority(String value) {
        this.priority = value;
    }

    /**
     * Obtém o valor da propriedade categoryGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getCategoryGid() {
        return categoryGid;
    }

    /**
     * Define o valor da propriedade categoryGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setCategoryGid(GLogXMLGidType value) {
        this.categoryGid = value;
    }

}
