
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * 
 *             SStatusSEquipment is a structure for identifying the Shipment Equipment affected by the event.
 *             The IntSavedQuery element provides a means for querying for the SEquipmentGid.
 *          
 * 
 * <p>Classe Java de SStatusSEquipmentType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="SStatusSEquipmentType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IntSavedQuery" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}IntSavedQueryType" minOccurs="0"/>
 *         &lt;element name="SEquipmentGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="EquipmentInitial" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EquipmentNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EquipmentInitialNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EquipmentTypeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="AARCarType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IntermodalEquipLength" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TareWeight" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLWeightType" minOccurs="0"/>
 *         &lt;element name="ScaleName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ScaleLocation" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ScaleTicket" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ScaleWeight" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLWeightType" minOccurs="0"/>
 *         &lt;element name="WeightQualifier" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EquipmentGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="ChassisInitial" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ChassisNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ChassisInitialNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EquipmentRefnum" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}EquipmentRefnumType" minOccurs="0"/>
 *         &lt;element name="SEquipmentSeal" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}SEquipmentSealType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SStatusSEquipmentType", propOrder = {
    "intSavedQuery",
    "sEquipmentGid",
    "equipmentInitial",
    "equipmentNumber",
    "equipmentInitialNumber",
    "equipmentTypeGid",
    "aarCarType",
    "intermodalEquipLength",
    "tareWeight",
    "scaleName",
    "scaleLocation",
    "scaleTicket",
    "scaleWeight",
    "weightQualifier",
    "equipmentGid",
    "chassisInitial",
    "chassisNumber",
    "chassisInitialNumber",
    "equipmentRefnum",
    "sEquipmentSeal"
})
public class SStatusSEquipmentType {

    @XmlElement(name = "IntSavedQuery")
    protected IntSavedQueryType intSavedQuery;
    @XmlElement(name = "SEquipmentGid")
    protected GLogXMLGidType sEquipmentGid;
    @XmlElement(name = "EquipmentInitial")
    protected String equipmentInitial;
    @XmlElement(name = "EquipmentNumber")
    protected String equipmentNumber;
    @XmlElement(name = "EquipmentInitialNumber")
    protected String equipmentInitialNumber;
    @XmlElement(name = "EquipmentTypeGid")
    protected GLogXMLGidType equipmentTypeGid;
    @XmlElement(name = "AARCarType")
    protected String aarCarType;
    @XmlElement(name = "IntermodalEquipLength")
    protected String intermodalEquipLength;
    @XmlElement(name = "TareWeight")
    protected GLogXMLWeightType tareWeight;
    @XmlElement(name = "ScaleName")
    protected String scaleName;
    @XmlElement(name = "ScaleLocation")
    protected String scaleLocation;
    @XmlElement(name = "ScaleTicket")
    protected String scaleTicket;
    @XmlElement(name = "ScaleWeight")
    protected GLogXMLWeightType scaleWeight;
    @XmlElement(name = "WeightQualifier")
    protected String weightQualifier;
    @XmlElement(name = "EquipmentGid")
    protected GLogXMLGidType equipmentGid;
    @XmlElement(name = "ChassisInitial")
    protected String chassisInitial;
    @XmlElement(name = "ChassisNumber")
    protected String chassisNumber;
    @XmlElement(name = "ChassisInitialNumber")
    protected String chassisInitialNumber;
    @XmlElement(name = "EquipmentRefnum")
    protected EquipmentRefnumType equipmentRefnum;
    @XmlElement(name = "SEquipmentSeal")
    protected List<SEquipmentSealType> sEquipmentSeal;

    /**
     * Obtém o valor da propriedade intSavedQuery.
     * 
     * @return
     *     possible object is
     *     {@link IntSavedQueryType }
     *     
     */
    public IntSavedQueryType getIntSavedQuery() {
        return intSavedQuery;
    }

    /**
     * Define o valor da propriedade intSavedQuery.
     * 
     * @param value
     *     allowed object is
     *     {@link IntSavedQueryType }
     *     
     */
    public void setIntSavedQuery(IntSavedQueryType value) {
        this.intSavedQuery = value;
    }

    /**
     * Obtém o valor da propriedade sEquipmentGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getSEquipmentGid() {
        return sEquipmentGid;
    }

    /**
     * Define o valor da propriedade sEquipmentGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setSEquipmentGid(GLogXMLGidType value) {
        this.sEquipmentGid = value;
    }

    /**
     * Obtém o valor da propriedade equipmentInitial.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEquipmentInitial() {
        return equipmentInitial;
    }

    /**
     * Define o valor da propriedade equipmentInitial.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEquipmentInitial(String value) {
        this.equipmentInitial = value;
    }

    /**
     * Obtém o valor da propriedade equipmentNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEquipmentNumber() {
        return equipmentNumber;
    }

    /**
     * Define o valor da propriedade equipmentNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEquipmentNumber(String value) {
        this.equipmentNumber = value;
    }

    /**
     * Obtém o valor da propriedade equipmentInitialNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEquipmentInitialNumber() {
        return equipmentInitialNumber;
    }

    /**
     * Define o valor da propriedade equipmentInitialNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEquipmentInitialNumber(String value) {
        this.equipmentInitialNumber = value;
    }

    /**
     * Obtém o valor da propriedade equipmentTypeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getEquipmentTypeGid() {
        return equipmentTypeGid;
    }

    /**
     * Define o valor da propriedade equipmentTypeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setEquipmentTypeGid(GLogXMLGidType value) {
        this.equipmentTypeGid = value;
    }

    /**
     * Obtém o valor da propriedade aarCarType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAARCarType() {
        return aarCarType;
    }

    /**
     * Define o valor da propriedade aarCarType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAARCarType(String value) {
        this.aarCarType = value;
    }

    /**
     * Obtém o valor da propriedade intermodalEquipLength.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIntermodalEquipLength() {
        return intermodalEquipLength;
    }

    /**
     * Define o valor da propriedade intermodalEquipLength.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIntermodalEquipLength(String value) {
        this.intermodalEquipLength = value;
    }

    /**
     * Obtém o valor da propriedade tareWeight.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLWeightType }
     *     
     */
    public GLogXMLWeightType getTareWeight() {
        return tareWeight;
    }

    /**
     * Define o valor da propriedade tareWeight.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLWeightType }
     *     
     */
    public void setTareWeight(GLogXMLWeightType value) {
        this.tareWeight = value;
    }

    /**
     * Obtém o valor da propriedade scaleName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getScaleName() {
        return scaleName;
    }

    /**
     * Define o valor da propriedade scaleName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setScaleName(String value) {
        this.scaleName = value;
    }

    /**
     * Obtém o valor da propriedade scaleLocation.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getScaleLocation() {
        return scaleLocation;
    }

    /**
     * Define o valor da propriedade scaleLocation.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setScaleLocation(String value) {
        this.scaleLocation = value;
    }

    /**
     * Obtém o valor da propriedade scaleTicket.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getScaleTicket() {
        return scaleTicket;
    }

    /**
     * Define o valor da propriedade scaleTicket.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setScaleTicket(String value) {
        this.scaleTicket = value;
    }

    /**
     * Obtém o valor da propriedade scaleWeight.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLWeightType }
     *     
     */
    public GLogXMLWeightType getScaleWeight() {
        return scaleWeight;
    }

    /**
     * Define o valor da propriedade scaleWeight.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLWeightType }
     *     
     */
    public void setScaleWeight(GLogXMLWeightType value) {
        this.scaleWeight = value;
    }

    /**
     * Obtém o valor da propriedade weightQualifier.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWeightQualifier() {
        return weightQualifier;
    }

    /**
     * Define o valor da propriedade weightQualifier.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWeightQualifier(String value) {
        this.weightQualifier = value;
    }

    /**
     * Obtém o valor da propriedade equipmentGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getEquipmentGid() {
        return equipmentGid;
    }

    /**
     * Define o valor da propriedade equipmentGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setEquipmentGid(GLogXMLGidType value) {
        this.equipmentGid = value;
    }

    /**
     * Obtém o valor da propriedade chassisInitial.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChassisInitial() {
        return chassisInitial;
    }

    /**
     * Define o valor da propriedade chassisInitial.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChassisInitial(String value) {
        this.chassisInitial = value;
    }

    /**
     * Obtém o valor da propriedade chassisNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChassisNumber() {
        return chassisNumber;
    }

    /**
     * Define o valor da propriedade chassisNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChassisNumber(String value) {
        this.chassisNumber = value;
    }

    /**
     * Obtém o valor da propriedade chassisInitialNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChassisInitialNumber() {
        return chassisInitialNumber;
    }

    /**
     * Define o valor da propriedade chassisInitialNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChassisInitialNumber(String value) {
        this.chassisInitialNumber = value;
    }

    /**
     * Obtém o valor da propriedade equipmentRefnum.
     * 
     * @return
     *     possible object is
     *     {@link EquipmentRefnumType }
     *     
     */
    public EquipmentRefnumType getEquipmentRefnum() {
        return equipmentRefnum;
    }

    /**
     * Define o valor da propriedade equipmentRefnum.
     * 
     * @param value
     *     allowed object is
     *     {@link EquipmentRefnumType }
     *     
     */
    public void setEquipmentRefnum(EquipmentRefnumType value) {
        this.equipmentRefnum = value;
    }

    /**
     * Gets the value of the sEquipmentSeal property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the sEquipmentSeal property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSEquipmentSeal().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SEquipmentSealType }
     * 
     * 
     */
    public List<SEquipmentSealType> getSEquipmentSeal() {
        if (sEquipmentSeal == null) {
            sEquipmentSeal = new ArrayList<SEquipmentSealType>();
        }
        return this.sEquipmentSeal;
    }

}
