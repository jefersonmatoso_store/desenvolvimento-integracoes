
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;


/**
 * 
 *             A WorkInvoiceType represents driver pay information for a transportation shipment. It includes list of
 *             activities performed by the driver at different stops (like loading, unloading etc) or transit
 *             (deadhead or bobtail) for a transportation shipment.
 *          
 * 
 * <p>Classe Java de WorkInvoiceType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="WorkInvoiceType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OTMTransactionInOut">
 *       &lt;sequence>
 *         &lt;element name="IntSavedQuery" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}IntSavedQueryType" minOccurs="0"/>
 *         &lt;element name="WorkInvoiceGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *         &lt;element name="TransactionCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransactionCodeType"/>
 *         &lt;element name="ReplaceChildren" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ReplaceChildrenType" minOccurs="0"/>
 *         &lt;element name="WorkInvoiceNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;choice>
 *           &lt;element name="DriverGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *           &lt;element name="Driver" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}DriverType"/>
 *         &lt;/choice>
 *         &lt;choice minOccurs="0">
 *           &lt;element name="SecondaryDriverGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *           &lt;element name="SecondaryDriver" minOccurs="0">
 *             &lt;complexType>
 *               &lt;complexContent>
 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   &lt;sequence>
 *                     &lt;element name="Driver" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}DriverType"/>
 *                   &lt;/sequence>
 *                 &lt;/restriction>
 *               &lt;/complexContent>
 *             &lt;/complexType>
 *           &lt;/element>
 *         &lt;/choice>
 *         &lt;choice>
 *           &lt;element name="ShipmentGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/>
 *           &lt;element name="Shipment" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ShipmentType"/>
 *         &lt;/choice>
 *         &lt;element name="DateSent" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/>
 *         &lt;element name="Revenue" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/>
 *         &lt;element name="WorkInvoiceActivity" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}WorkInvoiceActivityType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Status" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}StatusType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WorkInvoiceType", propOrder = {
    "intSavedQuery",
    "workInvoiceGid",
    "transactionCode",
    "replaceChildren",
    "workInvoiceNumber",
    "driverGid",
    "driver",
    "secondaryDriverGid",
    "secondaryDriver",
    "shipmentGid",
    "shipment",
    "dateSent",
    "revenue",
    "workInvoiceActivity",
    "status"
})
public class WorkInvoiceType
    extends OTMTransactionInOut
{

    @XmlElement(name = "IntSavedQuery")
    protected IntSavedQueryType intSavedQuery;
    @XmlElement(name = "WorkInvoiceGid", required = true)
    protected GLogXMLGidType workInvoiceGid;
    @XmlElement(name = "TransactionCode", required = true)
    @XmlSchemaType(name = "string")
    protected TransactionCodeType transactionCode;
    @XmlElement(name = "ReplaceChildren")
    protected ReplaceChildrenType replaceChildren;
    @XmlElement(name = "WorkInvoiceNumber")
    protected String workInvoiceNumber;
    @XmlElement(name = "DriverGid")
    protected GLogXMLGidType driverGid;
    @XmlElement(name = "Driver")
    protected DriverType driver;
    @XmlElement(name = "SecondaryDriverGid")
    protected GLogXMLGidType secondaryDriverGid;
    @XmlElement(name = "SecondaryDriver")
    protected SecondaryDriver secondaryDriver;
    @XmlElement(name = "ShipmentGid")
    protected GLogXMLGidType shipmentGid;
    @XmlElement(name = "Shipment")
    protected ShipmentType shipment;
    @XmlElement(name = "DateSent")
    protected GLogDateTimeType dateSent;
    @XmlElement(name = "Revenue")
    protected GLogXMLFinancialAmountType revenue;
    @XmlElement(name = "WorkInvoiceActivity")
    protected List<WorkInvoiceActivityType> workInvoiceActivity;
    @XmlElement(name = "Status")
    protected List<StatusType> status;

    /**
     * Obtém o valor da propriedade intSavedQuery.
     * 
     * @return
     *     possible object is
     *     {@link IntSavedQueryType }
     *     
     */
    public IntSavedQueryType getIntSavedQuery() {
        return intSavedQuery;
    }

    /**
     * Define o valor da propriedade intSavedQuery.
     * 
     * @param value
     *     allowed object is
     *     {@link IntSavedQueryType }
     *     
     */
    public void setIntSavedQuery(IntSavedQueryType value) {
        this.intSavedQuery = value;
    }

    /**
     * Obtém o valor da propriedade workInvoiceGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getWorkInvoiceGid() {
        return workInvoiceGid;
    }

    /**
     * Define o valor da propriedade workInvoiceGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setWorkInvoiceGid(GLogXMLGidType value) {
        this.workInvoiceGid = value;
    }

    /**
     * Obtém o valor da propriedade transactionCode.
     * 
     * @return
     *     possible object is
     *     {@link TransactionCodeType }
     *     
     */
    public TransactionCodeType getTransactionCode() {
        return transactionCode;
    }

    /**
     * Define o valor da propriedade transactionCode.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionCodeType }
     *     
     */
    public void setTransactionCode(TransactionCodeType value) {
        this.transactionCode = value;
    }

    /**
     * Obtém o valor da propriedade replaceChildren.
     * 
     * @return
     *     possible object is
     *     {@link ReplaceChildrenType }
     *     
     */
    public ReplaceChildrenType getReplaceChildren() {
        return replaceChildren;
    }

    /**
     * Define o valor da propriedade replaceChildren.
     * 
     * @param value
     *     allowed object is
     *     {@link ReplaceChildrenType }
     *     
     */
    public void setReplaceChildren(ReplaceChildrenType value) {
        this.replaceChildren = value;
    }

    /**
     * Obtém o valor da propriedade workInvoiceNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWorkInvoiceNumber() {
        return workInvoiceNumber;
    }

    /**
     * Define o valor da propriedade workInvoiceNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWorkInvoiceNumber(String value) {
        this.workInvoiceNumber = value;
    }

    /**
     * Obtém o valor da propriedade driverGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getDriverGid() {
        return driverGid;
    }

    /**
     * Define o valor da propriedade driverGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setDriverGid(GLogXMLGidType value) {
        this.driverGid = value;
    }

    /**
     * Obtém o valor da propriedade driver.
     * 
     * @return
     *     possible object is
     *     {@link DriverType }
     *     
     */
    public DriverType getDriver() {
        return driver;
    }

    /**
     * Define o valor da propriedade driver.
     * 
     * @param value
     *     allowed object is
     *     {@link DriverType }
     *     
     */
    public void setDriver(DriverType value) {
        this.driver = value;
    }

    /**
     * Obtém o valor da propriedade secondaryDriverGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getSecondaryDriverGid() {
        return secondaryDriverGid;
    }

    /**
     * Define o valor da propriedade secondaryDriverGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setSecondaryDriverGid(GLogXMLGidType value) {
        this.secondaryDriverGid = value;
    }

    /**
     * Obtém o valor da propriedade secondaryDriver.
     * 
     * @return
     *     possible object is
     *     {@link SecondaryDriver }
     *     
     */
    public SecondaryDriver getSecondaryDriver() {
        return secondaryDriver;
    }

    /**
     * Define o valor da propriedade secondaryDriver.
     * 
     * @param value
     *     allowed object is
     *     {@link SecondaryDriver }
     *     
     */
    public void setSecondaryDriver(SecondaryDriver value) {
        this.secondaryDriver = value;
    }

    /**
     * Obtém o valor da propriedade shipmentGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getShipmentGid() {
        return shipmentGid;
    }

    /**
     * Define o valor da propriedade shipmentGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setShipmentGid(GLogXMLGidType value) {
        this.shipmentGid = value;
    }

    /**
     * Obtém o valor da propriedade shipment.
     * 
     * @return
     *     possible object is
     *     {@link ShipmentType }
     *     
     */
    public ShipmentType getShipment() {
        return shipment;
    }

    /**
     * Define o valor da propriedade shipment.
     * 
     * @param value
     *     allowed object is
     *     {@link ShipmentType }
     *     
     */
    public void setShipment(ShipmentType value) {
        this.shipment = value;
    }

    /**
     * Obtém o valor da propriedade dateSent.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getDateSent() {
        return dateSent;
    }

    /**
     * Define o valor da propriedade dateSent.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setDateSent(GLogDateTimeType value) {
        this.dateSent = value;
    }

    /**
     * Obtém o valor da propriedade revenue.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getRevenue() {
        return revenue;
    }

    /**
     * Define o valor da propriedade revenue.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setRevenue(GLogXMLFinancialAmountType value) {
        this.revenue = value;
    }

    /**
     * Gets the value of the workInvoiceActivity property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the workInvoiceActivity property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getWorkInvoiceActivity().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link WorkInvoiceActivityType }
     * 
     * 
     */
    public List<WorkInvoiceActivityType> getWorkInvoiceActivity() {
        if (workInvoiceActivity == null) {
            workInvoiceActivity = new ArrayList<WorkInvoiceActivityType>();
        }
        return this.workInvoiceActivity;
    }

    /**
     * Gets the value of the status property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the status property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getStatus().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link StatusType }
     * 
     * 
     */
    public List<StatusType> getStatus() {
        if (status == null) {
            status = new ArrayList<StatusType>();
        }
        return this.status;
    }


    /**
     * <p>Classe Java de anonymous complex type.
     * 
     * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Driver" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}DriverType"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "driver"
    })
    public static class SecondaryDriver {

        @XmlElement(name = "Driver", required = true)
        protected DriverType driver;

        /**
         * Obtém o valor da propriedade driver.
         * 
         * @return
         *     possible object is
         *     {@link DriverType }
         *     
         */
        public DriverType getDriver() {
            return driver;
        }

        /**
         * Define o valor da propriedade driver.
         * 
         * @param value
         *     allowed object is
         *     {@link DriverType }
         *     
         */
        public void setDriver(DriverType value) {
            this.driver = value;
        }

    }

}
