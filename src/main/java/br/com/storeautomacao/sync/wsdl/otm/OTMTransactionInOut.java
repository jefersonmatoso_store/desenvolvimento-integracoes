
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de OTMTransactionInOut complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="OTMTransactionInOut">
 *   &lt;complexContent>
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLTransactionType">
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OTMTransactionInOut")
@XmlSeeAlso({
    OrderMovementType.class,
    InvoiceType.class,
    HazmatItemType.class,
    WorkInvoiceType.class,
    DocumentType.class,
    ItemType.class,
    CSVDataLoadType.class,
    XLaneType.class,
    JobType.class,
    DeviceType.class,
    CharterVoyageType.class,
    ShipmentGroupTenderOfferType.class,
    ItemMasterType.class,
    TransOrderType.class,
    ExchangeRateType.class,
    SkuTransactionType.class,
    DriverType.class,
    ConsolType.class,
    HazmatGenericType.class,
    ActivityTimeDefType.class,
    ShipmentStatusType.class,
    PowerUnitType.class,
    ReleaseType.class,
    ShipmentGroupType.class,
    ContactType.class,
    QuoteType.class,
    CorporationType.class,
    EquipmentType.class,
    SkuEventType.class,
    SkuType.class,
    ClaimType.class,
    LocationType.class,
    GtmContactType.class,
    GtmTransactionType.class,
    GtmTransactionLineType.class,
    GtmDeclarationMessageType.class,
    GtmRegistrationType.class,
    GtmDeclarationType.class,
    GtmBondType.class
})
public abstract class OTMTransactionInOut
    extends GLogXMLTransactionType
{


}
