
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * 
 *             RailDetail provides invoice data specific to rail carriers.
 *          
 * 
 * <p>Classe Java de RailDetailType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="RailDetailType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DestinationStation" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLStationType"/>
 *         &lt;element name="OriginStation" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLStationType"/>
 *         &lt;element name="RailEquipment" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RailEquipmentType" maxOccurs="unbounded"/>
 *         &lt;element name="RailStopOff" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RailStopOffType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="RailLineItem" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RailLineItemType" maxOccurs="unbounded"/>
 *         &lt;element name="ProtectiveSvc" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ProtectiveSvcType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RailDetailType", propOrder = {
    "destinationStation",
    "originStation",
    "railEquipment",
    "railStopOff",
    "railLineItem",
    "protectiveSvc"
})
public class RailDetailType {

    @XmlElement(name = "DestinationStation", required = true)
    protected GLogXMLStationType destinationStation;
    @XmlElement(name = "OriginStation", required = true)
    protected GLogXMLStationType originStation;
    @XmlElement(name = "RailEquipment", required = true)
    protected List<RailEquipmentType> railEquipment;
    @XmlElement(name = "RailStopOff")
    protected List<RailStopOffType> railStopOff;
    @XmlElement(name = "RailLineItem", required = true)
    protected List<RailLineItemType> railLineItem;
    @XmlElement(name = "ProtectiveSvc")
    protected List<ProtectiveSvcType> protectiveSvc;

    /**
     * Obtém o valor da propriedade destinationStation.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLStationType }
     *     
     */
    public GLogXMLStationType getDestinationStation() {
        return destinationStation;
    }

    /**
     * Define o valor da propriedade destinationStation.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLStationType }
     *     
     */
    public void setDestinationStation(GLogXMLStationType value) {
        this.destinationStation = value;
    }

    /**
     * Obtém o valor da propriedade originStation.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLStationType }
     *     
     */
    public GLogXMLStationType getOriginStation() {
        return originStation;
    }

    /**
     * Define o valor da propriedade originStation.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLStationType }
     *     
     */
    public void setOriginStation(GLogXMLStationType value) {
        this.originStation = value;
    }

    /**
     * Gets the value of the railEquipment property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the railEquipment property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRailEquipment().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RailEquipmentType }
     * 
     * 
     */
    public List<RailEquipmentType> getRailEquipment() {
        if (railEquipment == null) {
            railEquipment = new ArrayList<RailEquipmentType>();
        }
        return this.railEquipment;
    }

    /**
     * Gets the value of the railStopOff property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the railStopOff property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRailStopOff().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RailStopOffType }
     * 
     * 
     */
    public List<RailStopOffType> getRailStopOff() {
        if (railStopOff == null) {
            railStopOff = new ArrayList<RailStopOffType>();
        }
        return this.railStopOff;
    }

    /**
     * Gets the value of the railLineItem property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the railLineItem property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRailLineItem().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RailLineItemType }
     * 
     * 
     */
    public List<RailLineItemType> getRailLineItem() {
        if (railLineItem == null) {
            railLineItem = new ArrayList<RailLineItemType>();
        }
        return this.railLineItem;
    }

    /**
     * Gets the value of the protectiveSvc property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the protectiveSvc property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getProtectiveSvc().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProtectiveSvcType }
     * 
     * 
     */
    public List<ProtectiveSvcType> getProtectiveSvc() {
        if (protectiveSvc == null) {
            protectiveSvc = new ArrayList<ProtectiveSvcType>();
        }
        return this.protectiveSvc;
    }

}
