
package br.com.storeautomacao.sync.wsdl.otm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * 
 *             Provides a summary of the line items and the detail summary.
 *          
 * 
 * <p>Classe Java de PaymentSummaryType complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="PaymentSummaryType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SequenceNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FreightCharge" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/>
 *         &lt;element name="CommercialUnitPrice" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/>
 *         &lt;element name="InvoiceTotal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PrepaidAmount" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/>
 *         &lt;element name="UnitCount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TransportHandlingUnitRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLShipUnitSpecRefType" minOccurs="0"/>
 *         &lt;element name="WeightVolume" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}WeightVolumeType" minOccurs="0"/>
 *         &lt;element name="InvoiceServiceCodeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/>
 *         &lt;element name="Remark" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RemarkType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="PaymentSummaryDetail" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}PaymentSummaryDetailType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PaymentSummaryType", propOrder = {
    "sequenceNumber",
    "freightCharge",
    "commercialUnitPrice",
    "invoiceTotal",
    "prepaidAmount",
    "unitCount",
    "transportHandlingUnitRef",
    "weightVolume",
    "invoiceServiceCodeGid",
    "remark",
    "paymentSummaryDetail"
})
public class PaymentSummaryType {

    @XmlElement(name = "SequenceNumber")
    protected String sequenceNumber;
    @XmlElement(name = "FreightCharge")
    protected GLogXMLFinancialAmountType freightCharge;
    @XmlElement(name = "CommercialUnitPrice")
    protected GLogXMLFinancialAmountType commercialUnitPrice;
    @XmlElement(name = "InvoiceTotal")
    protected String invoiceTotal;
    @XmlElement(name = "PrepaidAmount")
    protected GLogXMLFinancialAmountType prepaidAmount;
    @XmlElement(name = "UnitCount")
    protected String unitCount;
    @XmlElement(name = "TransportHandlingUnitRef")
    protected GLogXMLShipUnitSpecRefType transportHandlingUnitRef;
    @XmlElement(name = "WeightVolume")
    protected WeightVolumeType weightVolume;
    @XmlElement(name = "InvoiceServiceCodeGid")
    protected GLogXMLGidType invoiceServiceCodeGid;
    @XmlElement(name = "Remark")
    protected List<RemarkType> remark;
    @XmlElement(name = "PaymentSummaryDetail")
    protected List<PaymentSummaryDetailType> paymentSummaryDetail;

    /**
     * Obtém o valor da propriedade sequenceNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSequenceNumber() {
        return sequenceNumber;
    }

    /**
     * Define o valor da propriedade sequenceNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSequenceNumber(String value) {
        this.sequenceNumber = value;
    }

    /**
     * Obtém o valor da propriedade freightCharge.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getFreightCharge() {
        return freightCharge;
    }

    /**
     * Define o valor da propriedade freightCharge.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setFreightCharge(GLogXMLFinancialAmountType value) {
        this.freightCharge = value;
    }

    /**
     * Obtém o valor da propriedade commercialUnitPrice.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getCommercialUnitPrice() {
        return commercialUnitPrice;
    }

    /**
     * Define o valor da propriedade commercialUnitPrice.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setCommercialUnitPrice(GLogXMLFinancialAmountType value) {
        this.commercialUnitPrice = value;
    }

    /**
     * Obtém o valor da propriedade invoiceTotal.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInvoiceTotal() {
        return invoiceTotal;
    }

    /**
     * Define o valor da propriedade invoiceTotal.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInvoiceTotal(String value) {
        this.invoiceTotal = value;
    }

    /**
     * Obtém o valor da propriedade prepaidAmount.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getPrepaidAmount() {
        return prepaidAmount;
    }

    /**
     * Define o valor da propriedade prepaidAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setPrepaidAmount(GLogXMLFinancialAmountType value) {
        this.prepaidAmount = value;
    }

    /**
     * Obtém o valor da propriedade unitCount.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUnitCount() {
        return unitCount;
    }

    /**
     * Define o valor da propriedade unitCount.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUnitCount(String value) {
        this.unitCount = value;
    }

    /**
     * Obtém o valor da propriedade transportHandlingUnitRef.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLShipUnitSpecRefType }
     *     
     */
    public GLogXMLShipUnitSpecRefType getTransportHandlingUnitRef() {
        return transportHandlingUnitRef;
    }

    /**
     * Define o valor da propriedade transportHandlingUnitRef.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLShipUnitSpecRefType }
     *     
     */
    public void setTransportHandlingUnitRef(GLogXMLShipUnitSpecRefType value) {
        this.transportHandlingUnitRef = value;
    }

    /**
     * Obtém o valor da propriedade weightVolume.
     * 
     * @return
     *     possible object is
     *     {@link WeightVolumeType }
     *     
     */
    public WeightVolumeType getWeightVolume() {
        return weightVolume;
    }

    /**
     * Define o valor da propriedade weightVolume.
     * 
     * @param value
     *     allowed object is
     *     {@link WeightVolumeType }
     *     
     */
    public void setWeightVolume(WeightVolumeType value) {
        this.weightVolume = value;
    }

    /**
     * Obtém o valor da propriedade invoiceServiceCodeGid.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getInvoiceServiceCodeGid() {
        return invoiceServiceCodeGid;
    }

    /**
     * Define o valor da propriedade invoiceServiceCodeGid.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setInvoiceServiceCodeGid(GLogXMLGidType value) {
        this.invoiceServiceCodeGid = value;
    }

    /**
     * Gets the value of the remark property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the remark property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRemark().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RemarkType }
     * 
     * 
     */
    public List<RemarkType> getRemark() {
        if (remark == null) {
            remark = new ArrayList<RemarkType>();
        }
        return this.remark;
    }

    /**
     * Gets the value of the paymentSummaryDetail property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the paymentSummaryDetail property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPaymentSummaryDetail().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PaymentSummaryDetailType }
     * 
     * 
     */
    public List<PaymentSummaryDetailType> getPaymentSummaryDetail() {
        if (paymentSummaryDetail == null) {
            paymentSummaryDetail = new ArrayList<PaymentSummaryDetailType>();
        }
        return this.paymentSummaryDetail;
    }

}
