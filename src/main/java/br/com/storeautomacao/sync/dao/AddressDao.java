package br.com.storeautomacao.sync.dao;

import br.com.storeautomacao.sync.model.entity.Address;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by alexandre on 12/09/17.
 */
@Component
public class AddressDao {

    @Autowired
    private MongoOperations mongoOperation;

    public static final String COLLECTION_NAME = "address";

    public String create(Address address) {
        mongoOperation.save(address, COLLECTION_NAME);
        return address.getId();
    }

    public List<Address> findAll() {
        return mongoOperation.findAll(Address.class, COLLECTION_NAME);
    }

    public Address findById(String id) {
        Query query = new Query(Criteria.where("id").is(id));
        return mongoOperation.findOne(query, Address.class, COLLECTION_NAME);
    }

    public List<Address> findByText(String text) {

        Query query = new Query(new Criteria().orOperator(
                Criteria.where("street").regex(".*" + text + ".*", "i"),
                Criteria.where("number").regex(".*" + text + ".*", "i"),
                Criteria.where("complement").regex(".*" + text + ".*", "i"),
                Criteria.where("neighborhood").regex(".*" + text + ".*", "i"),
                Criteria.where("state").regex(".*" + text + ".*", "i"),
                Criteria.where("cep").regex(".*" + text + ".*", "i"),
                Criteria.where("city").regex(".*" + text + ".*", "i")));
        return mongoOperation.find(query, Address.class, COLLECTION_NAME);
    }

    public void remove(String id) {
        Query query = new Query(Criteria.where("id").is(id));
        mongoOperation.remove(query, Address.class);
    }

    public void remove(Address address) {
        mongoOperation.remove(address);
    }
}
