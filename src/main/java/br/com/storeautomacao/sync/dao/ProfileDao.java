package br.com.storeautomacao.sync.dao;

import br.com.storeautomacao.sync.model.entity.Person;
import br.com.storeautomacao.sync.model.entity.Profile;
import br.com.storeautomacao.sync.model.entity.ProfileType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.List;

/**
 * Created by alexandre on 21/09/17.
 */
@Component
public class ProfileDao {

    @Autowired
    private MongoOperations mongoOperation;

    public static final String COLLECTION_NAME = "profiles";

    public String create(Profile profile) {
        mongoOperation.save(profile, COLLECTION_NAME);
        return profile.getId();
    }

    public List<Profile> findAll() {
        return mongoOperation.findAll(Profile.class, COLLECTION_NAME);
    }

    public Profile findById(String id) {
        Query query = new Query(Criteria.where("id").is(id));
        return mongoOperation.findOne(query, Profile.class, COLLECTION_NAME);
    }

    public Profile findByIdPerson(String id) {
        Query query = new Query(Criteria.where("person.id").is(id));
        return mongoOperation.findOne(query, Profile.class, COLLECTION_NAME);
    }

    public List<Profile> findByPerson(Collection<Person> persons) {
        Criteria criteria = Criteria.where("person").in(persons);
        return mongoOperation.find(new Query(criteria), Profile.class, COLLECTION_NAME);
    }

    public List<Profile> findByType(ProfileType type) {
        Query query = new Query(Criteria.where("profileType").is(type));
        return mongoOperation.find(query, Profile.class, COLLECTION_NAME);
    }

    public void remove(String id) {
        Query query = new Query(Criteria.where("id").is(id));
        mongoOperation.remove(query, Profile.class);
    }

    public void remove(Profile profile) {
        mongoOperation.remove(profile);
    }

}
