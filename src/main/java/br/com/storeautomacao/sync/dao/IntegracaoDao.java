package br.com.storeautomacao.sync.dao;


import br.com.storeautomacao.sync.model.entity.Conferencia;
import br.com.storeautomacao.sync.model.entity.Embalagem;
import br.com.storeautomacao.sync.model.entity.Integracao;
import br.com.storeautomacao.sync.model.entity.Sistema;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by eduardo on 06/07/17.
 */
@Repository
public interface IntegracaoDao {

    List<Sistema> testeConnect() ;

    List<Integracao> listaIntegracaoEntradaMercadoriaPrevisaoDeEntrada() ;

    List<Integracao> listaIntegracaoEntradaMercadoriaPrevisaoDeSaida() ;

    List<Integracao> listaIntegracaoLogix();

    List<Integracao> shipLoadConfirmado() ;

    void gravarIntegracao(String sql);

    BigDecimal gravarIntegracaoConferenciaMercadoria(String sql);

    BigDecimal gravarDocumentoIntegracao(String sql);

    void atualizarIntegracao(String sql);

    void gravarMercadoria(String sql);

    void gravarMercadoriaDetalhe(String sql);

    String salvarEmbalagem(Embalagem embalagem);

    String salvarEmbalagem(List<Embalagem> embalagems);

    List<Embalagem> obterEmbalagensPorCodigo(String code);

}
