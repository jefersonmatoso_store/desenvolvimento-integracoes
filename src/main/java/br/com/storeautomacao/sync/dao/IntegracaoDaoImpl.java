package br.com.storeautomacao.sync.dao;


import br.com.storeautomacao.sync.model.entity.Embalagem;
import br.com.storeautomacao.sync.model.entity.Integracao;
import br.com.storeautomacao.sync.model.entity.Sistema;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.List;

/**
 * Created by eduardo on 06/07/17.
 */
@Repository
public class IntegracaoDaoImpl implements IntegracaoDao {

    static Logger logger = LoggerFactory.getLogger(IntegracaoDaoImpl.class);

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private MongoOperations mongoOperation;

    public static final String EMBALAGEM = "embalagem";


  /*  @Autowired
    @Qualifier("transactionManager")
    protected PlatformTransactionManager txManager;*/

    @Transactional(readOnly = true)
    public List<Sistema> testeConnect() {
        final String sql = "SELECT * FROM VERSAO";

        try {
            List<Sistema> result = jdbcTemplate.query(
                    sql,
                    (rs, rowNum) -> new Sistema(rs.getString("sistema"),
                            rs.getString("versao"))
            );

            return result;
        } catch (Exception ex) {
            System.out.print(ex.getMessage());
            return null;
        }
    }

    public List<Integracao> listaIntegracaoEntradaMercadoriaPrevisaoDeEntrada() {

        final String sql = "SELECT DATALOG, SEQUENCIAINTEGRACAO, TIPOINTEGRACAO, ESTADOINTEGRACAO FROM INTEGRACAO WHERE SEQUENCIAINTEGRACAO IN " +
                "(SELECT SEQUENCIAINTEGRACAO " +
                "FROM DOCUMENTO " +
                "WHERE TIPOINTEGRACAO = 101 " +
                "AND CODIGODEPOSITANTE = 'M9') " +
                "AND TIPOINTEGRACAO = 101 " +
                "AND ESTADOINTEGRACAO != 2"; // ESTADOINTEGRACAO = 1
        try {
            List<Integracao> result = jdbcTemplate.query(
                    sql,
                    (rs, rowNum) -> new Integracao(rs.getTimestamp("DATALOG"), rs.getBigDecimal("SEQUENCIAINTEGRACAO"), rs.getBigDecimal("TIPOINTEGRACAO"),
                            rs.getBigDecimal("ESTADOINTEGRACAO"))
            );
            return result;
        } catch (Exception ex) {
            System.out.print(ex.getMessage());
            return null;
        }

    }


    @Override
    public List<Integracao> listaIntegracaoEntradaMercadoriaPrevisaoDeSaida() {
        final String sql = "SELECT I.DATALOG, I.SEQUENCIAINTEGRACAO, I.TIPOINTEGRACAO, I.ESTADOINTEGRACAO FROM INTEGRACAO I, DOCUMENTO D WHERE I.TIPOINTEGRACAO = 203 AND I.ESTADOINTEGRACAO != 2 AND I.SEQUENCIAINTEGRACAO = D.SEQUENCIAINTEGRACAO AND D.CODIGODEPOSITANTE = 'M9' AND D.AGRUPADOR != 0"; // ESTADOINTEGRACAO = 1
        try {
            List<Integracao> result = jdbcTemplate.query(
                    sql,
                    (rs, rowNum) -> new Integracao(rs.getTimestamp("DATALOG"), rs.getBigDecimal("SEQUENCIAINTEGRACAO"), rs.getBigDecimal("TIPOINTEGRACAO"),
                            rs.getBigDecimal("ESTADOINTEGRACAO"))
            );
            return result;
        } catch (Exception ex) {
            System.out.print(ex.getMessage());
            return null;
        }
    }

    @Override
    public List<Integracao> listaIntegracaoLogix() {
        final String sql = "SELECT DATALOG, SEQUENCIAINTEGRACAO, TIPOINTEGRACAO, ESTADOINTEGRACAO FROM INTEGRACAO WHERE TIPOINTEGRACAO = 1 AND REFERENCIA IS NULL AND ESTADOINTEGRACAO IN(2,3)";
        try {
            List<Integracao> result = jdbcTemplate.query(
                    sql,
                    (rs, rowNum) -> new Integracao(rs.getTimestamp("DATALOG"), rs.getBigDecimal("SEQUENCIAINTEGRACAO"), rs.getBigDecimal("TIPOINTEGRACAO"),
                            rs.getBigDecimal("ESTADOINTEGRACAO"))
            );
            return result;
        } catch (Exception ex) {
            System.out.print(ex.getMessage());
            return null;
        }
    }

    @Override
    public List<Integracao> shipLoadConfirmado() {
        final String sql = "##############";
        try {
            List<Integracao> result = jdbcTemplate.query(
                    sql,
                    (rs, rowNum) -> new Integracao(rs.getTimestamp("DATALOG"), rs.getBigDecimal("SEQUENCIAINTEGRACAO"), rs.getBigDecimal("TIPOINTEGRACAO"),
                            rs.getBigDecimal("ESTADOINTEGRACAO"))
            );
            return result;
        } catch (Exception ex) {
            System.out.print(ex.getMessage());
            return null;
        }
    }


    @Override
    public void gravarIntegracao(String sql) {
        logger.info("Salvar Integracao - SQL: {}", sql);
        try {
            jdbcTemplate.update(sql);
        } catch (Exception ex) {
            System.out.print(ex.getMessage());
        }
    }

    @Override
    @Transactional
    public BigDecimal gravarIntegracaoConferenciaMercadoria(String sql) {

        try {

            Connection con = jdbcTemplate.getDataSource().getConnection();

            PreparedStatement statement = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

            statement.execute();

            PreparedStatement ps = con.prepareStatement("select SEQ_INTEGRACAO.currval from dual");

            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                BigDecimal generatedKey = rs.getBigDecimal(1);
                logger.info("Salvar Integracao - Conferencia Mercadoria - Numero Sequencia Integracao: {}", generatedKey);
                return generatedKey;
            }

        } catch (Exception ex) {
            logger.error("Erro Salvar Integracao - Conferencia Mercadoria: {}", ex.getMessage());
            System.out.print(ex.getMessage());
        }

        return null;
    }

    @Override
    public BigDecimal gravarDocumentoIntegracao(String sql) {

        try {

            //logger.info("Inserir Documento Integracao - Entrada Mercadoria: {}", sql);

            jdbcTemplate.update(sql);

        } catch (Exception ex) {
            logger.error("Erro Atualizar Integracao - Entrada Mercadoria: {}", ex.getMessage());
            System.out.print(ex.getMessage());
        }

        return null;

    }

    @Override
    public void atualizarIntegracao(String sql) {
        try {

            logger.info("Atualizar Integracao");

            jdbcTemplate.update(sql);

        } catch (Exception ex) {
            logger.error("Erro Atualizar Integracao - Entrada Mercadoria: {}", ex.getMessage());
            System.out.print(ex.getMessage());
        }
    }

    @Override
    public void gravarMercadoria(String sql) {

    }

    @Override
    public void gravarMercadoriaDetalhe(String sql) {

    }

    @Override
    public String salvarEmbalagem(Embalagem embalagem) {
        mongoOperation.save(embalagem, EMBALAGEM);
        return String.valueOf(embalagem.getId());
    }

    @Override
    public String salvarEmbalagem(List<Embalagem> embalagems) {
        mongoOperation.insert(embalagems, EMBALAGEM);
        return String.valueOf(embalagems.get(0).getId());
    }

    @Override
    public List<Embalagem> obterEmbalagensPorCodigo(String code) {
        Query query = new Query(Criteria.where("facility_code").regex(code));
        return mongoOperation.find(query, Embalagem.class, EMBALAGEM);
    }

}
