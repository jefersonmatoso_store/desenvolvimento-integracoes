package br.com.storeautomacao.sync.dao;

import br.com.storeautomacao.sync.model.entity.IdentityDocument;
import br.com.storeautomacao.sync.model.entity.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.List;

/**
 * Created by alexandre on 30/07/17.
 */
@Component
public class PersonDao {

    @Autowired
    private MongoOperations mongoOperation;

    public static final String COLLECTION_NAME = "person";

    public String create(Person person) {
        mongoOperation.save(person, COLLECTION_NAME);
        return person.getId();
    }

    public List<Person> findAll() { return mongoOperation.findAll(Person.class, COLLECTION_NAME); }

    public Person findById(String id) {
        Query query = new Query(Criteria.where("id").is(id));
        return mongoOperation.findOne(query, Person.class, COLLECTION_NAME);
    }

    public List<Person> findByFirstName(String firstName) { return findByField("firstName", firstName); }

    public List<Person> findByLastName(String lastName) { return findByField("lastName", lastName); }

    public List<Person> findByEmail(String email) { return findByField("email", email); }

    public List<Person> findByNamePiece(String namePiece) {
        List<Person> all = mongoOperation.findAll(Person.class, COLLECTION_NAME);
        Query query = new Query(new Criteria().orOperator(
                Criteria.where("firstName").regex(".*" + namePiece + ".*", "i"),
                Criteria.where("lastName").regex(".*" + namePiece + ".*", "i")));
        return mongoOperation.find(query, Person.class, COLLECTION_NAME);
    }

    /**
     * Pesquisa as Pessoas que contenham o documento informado em seus Documentos de Identificação.
     *
     * @param documents
     * @return
     */
    public List<Person> findByDocumentNumber(Collection<IdentityDocument> documents) {
        Query query = new Query(Criteria.where("identityDocuments").in(documents));
        return mongoOperation.find(query, Person.class, COLLECTION_NAME);
    }


    /**
     * Retorna a Pessoa que contenha o mesmo <i>hash</i> de Reconhecimento Facial.
     *
     * @param facialRecognition
     * @return
     */
    public Person findByFacialRecognition(String facialRecognition) {
        Query query = new Query(Criteria.where("facialRecognition").is(facialRecognition));
        return mongoOperation.findOne(query, Person.class, COLLECTION_NAME);
    }

    public List<Person> findByNameOrDocumentNumber(String nameOrdocumentNumber, Collection<IdentityDocument> documents) {
        Query query = new Query(new Criteria().orOperator(
                Criteria.where("firstName").regex(".*" + nameOrdocumentNumber + ".*", "i"),
                Criteria.where("lastName").regex(".*" + nameOrdocumentNumber + ".*", "i"),
                Criteria.where("identityDocuments").in(documents)));
        return mongoOperation.find(query, Person.class, COLLECTION_NAME);
    }

    /**
     * Retorna as Pessoas que contenha o texto informado (value) no
     * campo (field) informado.<br/>
     * As pesquisas de texto sao do tipo "<i>case insensitive</i>".
     *
     * @param field
     * @param value
     * @return
     */
    // TODO: Metodo candidato a refactoring: Ficar em classe abstrata
    private List<Person> findByField(String field, String value) {
        Query query = new Query(Criteria.where(field).regex(
                ".*" + value + ".*", "i"));
        return mongoOperation.find(query, Person.class, COLLECTION_NAME);
    }

    public void remove(String id) {
        Query query = new Query(Criteria.where("id").is(id));
        mongoOperation.remove(query, Person.class);
    }

    public void remove(Person person) {
        mongoOperation.remove(person);
    }
}
