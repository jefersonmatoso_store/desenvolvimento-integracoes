package br.com.storeautomacao.sync.dao;


import br.com.storeautomacao.sync.model.entity.Mercadoria;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by eduardo on 06/07/17.
 */
@Repository
public interface MercadoriaDao {

    String salvarMercadoriaEmMovimento(Mercadoria mercadoria);

    List<Mercadoria> obterListaEntradaMercadoria();

    List<Mercadoria> obterListaMercadoriaPorNumeroDocumento( String numeroDocumento);

    Mercadoria obterMercadoriaPorNumeroDocumento(String numeroDocumento);

}
