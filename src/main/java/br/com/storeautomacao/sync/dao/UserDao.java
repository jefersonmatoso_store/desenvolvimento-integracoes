package br.com.storeautomacao.sync.dao;

import br.com.storeautomacao.sync.configuration.SpringMongoConfiguration;
import br.com.storeautomacao.sync.model.entity.Profile;
import br.com.storeautomacao.sync.model.entity.User;
import br.com.storeautomacao.sync.util.SecurityUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.BasicQuery;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.List;

@Component
public class UserDao {

    ApplicationContext ctx = new AnnotationConfigApplicationContext(SpringMongoConfiguration.class);
    MongoOperations mongoOperation = (MongoOperations) ctx.getBean("mongoTemplate");

    public static final String COLLECTION_NAME = "users";

    public String create(User user) {
        if (StringUtils.isNotEmpty(user.getPassword())) {
            user.setPassword(SecurityUtil.encript(user.getPassword()));
        }
        if (StringUtils.isNotEmpty(user.getCheckinPassword())) {
            user.setCheckinPassword(SecurityUtil.encript(user.getCheckinPassword()));
        }
        mongoOperation.save(user, COLLECTION_NAME);
        return user.getId();
    }

    public String update(User user) {

        //FIXME: IMPLEMENTAR UPDATE DE "CAMPO A CAMPO", PARA EVITAR SOBRESCREVER As SENHAs
        if (StringUtils.isNotEmpty(user.getPassword())) {
            user.setPassword(SecurityUtil.encript(user.getPassword()));
        }
        if (StringUtils.isNotEmpty(user.getCheckinPassword())) {
            user.setCheckinPassword(SecurityUtil.encript(user.getCheckinPassword()));
        }
        mongoOperation.save(user, COLLECTION_NAME);
        return user.getId();
    }

    public User updatePasswordField(String userId, String passwordField, String passwordValue) {
        Update update = new Update();
        if (StringUtils.isNotEmpty(passwordValue)) {
            update.set(passwordField, passwordValue);
        }

        Query query = new Query(Criteria.where("id").is(userId));
        return mongoOperation.findAndModify(query, update, new FindAndModifyOptions().returnNew(true), User.class);
    }

    public List<User> findAll() {
        return mongoOperation.findAll(User.class, COLLECTION_NAME);
    }

    public User findById(String idUser) {
        Query query = new Query(Criteria.where("id").is(idUser));
        return mongoOperation.findOne(query, User.class, COLLECTION_NAME);
    }

    public User findByIdProfile(String idProfile) {
        Query query = new Query(Criteria.where("profile.id").is(idProfile));
        return mongoOperation.findOne(query, User.class, COLLECTION_NAME);
    }

    public List<User> findAll(Collection<Profile> profiles) {
        Query query = new Query(Criteria.where("profile").in(profiles));
        return mongoOperation.find(query, User.class, COLLECTION_NAME);
    }

    public boolean deleteById(String idUser) {
        User user = mongoOperation.findById(idUser, User.class);
        boolean isRemoved = false;
        try {
            if (user != null) {
                mongoOperation.remove(user);
                isRemoved = true;
            }
        } catch (Exception ex) {
            new Exception(ex.getMessage());
        }
        return isRemoved;
    }

    public User findUserByUsernameAndPassword(String userName, String password) {
        String passwordEncript = SecurityUtil.encript(password);
        Query query = new Query(Criteria.where("userName").is(userName).and("password").is(passwordEncript));
        User userLogged = mongoOperation.findOne(query, User.class);
        return userLogged;
    }

    public User findUserByUserIdAndCheckinPassword(String userId, String checkinPassword) {
        String passwordEncript = SecurityUtil.encript(checkinPassword);
        Query query = new Query(Criteria.where("id").is(userId).and("checkinPassword").is(passwordEncript));
        User user = mongoOperation.findOne(query, User.class);
        return user;
    }

    public User findByUsername(String username) {
        Query query = new Query(Criteria.where("userName").is(username));
        return mongoOperation.findOne(query, User.class, COLLECTION_NAME);
    }

    @Deprecated
    public List<User> findAllVigilants() {
        Criteria findProfilesCriteria = Criteria.where("profiles").elemMatch(Criteria.where("profiles").regex("Vigilantes"));
        BasicQuery query = new BasicQuery(findProfilesCriteria.getCriteriaObject());
        List<User> users = mongoOperation.find(query, User.class);
        return users;
    }
}
