package br.com.storeautomacao.sync.dao;

import br.com.storeautomacao.sync.model.entity.DocumentoDetalheSequencia;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by iomar on 21/11/18.
 */
@Repository
public interface DocumentoDetalheSequenciaDao {
    BigDecimal gravarDocumentoDetalheSequencia(String sql);
    List<DocumentoDetalheSequencia> obterDocumentosDetalhesSequenciasPorSequenciaIntegracao(String sequenciaIntegracao);
}
