package br.com.storeautomacao.sync.dao;

import br.com.storeautomacao.sync.model.entity.Mercadoria;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class MercadoriaDaoImpl implements MercadoriaDao {

    static Logger logger = LoggerFactory.getLogger(MercadoriaDao.class);

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private MongoOperations mongoOperation;


    public static final String MERCADORIA = "mercadoria";


    @Override
    public List<Mercadoria> obterListaEntradaMercadoria() {

        try {
            List<Mercadoria> lista = mongoOperation.findAll(Mercadoria.class);
            return lista;
        } catch (Exception ex){
            logger.error(ex.getMessage());
        }

        return null;
    }

    @Override
    public String salvarMercadoriaEmMovimento(Mercadoria mercadoria) {
        mongoOperation.save(mercadoria, MERCADORIA);
        return String.valueOf(mercadoria.getSequenciaIntegracao());
    }

    @Override
    public List<Mercadoria> obterListaMercadoriaPorNumeroDocumento(String numeroDocumento) {
        Query query = new Query(Criteria.where("numeroDocumento").regex(numeroDocumento));
        query.addCriteria(Criteria.where("estadoIntegracao").regex("CANCELADA").not());
        List<Mercadoria> listaMercadoria = mongoOperation.find(query, Mercadoria.class, MERCADORIA);
        return listaMercadoria;
    }

    @Override
    public Mercadoria obterMercadoriaPorNumeroDocumento(String numeroDocumento) {
        Query query = new Query(Criteria.where("numeroDocumento").regex(numeroDocumento));
        Mercadoria mercadoria = mongoOperation.findOne(query, Mercadoria.class, MERCADORIA);
        return mercadoria;
    }
}
