package br.com.storeautomacao.sync.dao.impl;

import br.com.storeautomacao.sync.dao.DocumentoDetalheSequenciaDao;
import br.com.storeautomacao.sync.model.entity.DocumentoDetalheSequencia;
import br.com.storeautomacao.sync.model.entity.ProdutoComponente;
import br.com.storeautomacao.sync.model.mapper.DocumentoDetalheSequenciaRowMapper;
import br.com.storeautomacao.sync.model.mapper.ProdutoComponenteRowMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.List;

/**
 * Created by iomar on 21/11/18.
 */
@Repository
public class DocumentoDetalheSequenciaDaoImpl implements DocumentoDetalheSequenciaDao {

    static Logger logger = LoggerFactory.getLogger(DocumentoDetalheSequenciaDaoImpl.class);

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    @Transactional
    public BigDecimal gravarDocumentoDetalheSequencia(String sql) {

        logger.info("Salvar DocumentoDetalheSequencia - Documento Detalhe Sequencia - Numero Sequencia Integracao: {}", sql);


        /*try {

            Connection con = jdbcTemplate.getDataSource().getConnection();

            PreparedStatement statement = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

            statement.execute();

            PreparedStatement ps = con.prepareStatement("select SEQ_INTEGRACAO.currval from dual");

            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                BigDecimal generatedKey = rs.getBigDecimal(1);
                logger.info("Salvar DocumentoDetalheSequencia - Documento Detalhe Sequencia - Numero Sequencia Integracao: {}", generatedKey);
                return generatedKey;
            }

        } catch (Exception ex) {
            logger.error("Erro DocumentoDetalheSequencia - Documento Detalhe Sequencia: {}", ex.getMessage());
            System.out.print(ex.getMessage());
        }*/

        return null;
    }

    @Override
    public List<DocumentoDetalheSequencia> obterDocumentosDetalhesSequenciasPorSequenciaIntegracao(String sequenciaIntegracao) {
        String sql = "SELECT * FROM DOCUMENTODETALHESEQUENCIA WHERE SEQUENCIAINTEGRACAO = ?";
        try {
            List<DocumentoDetalheSequencia> documentoDetalheSequencias = jdbcTemplate.query(sql,  new DocumentoDetalheSequenciaRowMapper(), sequenciaIntegracao);
            return documentoDetalheSequencias;
        } catch (Exception ex) {
            logger.error("SELECT * FROM DOCUMENTODETALHESEQUENCIA WHERE SEQUENCIAINTEGRACAO = ?: {} ", ex.getMessage());
            return null;
        }
    }

}
