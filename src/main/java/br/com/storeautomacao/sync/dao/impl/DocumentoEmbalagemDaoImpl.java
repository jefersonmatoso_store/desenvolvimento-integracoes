package br.com.storeautomacao.sync.dao.impl;

import br.com.storeautomacao.sync.dao.DocumentoEmbalagemDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 * Created by iomar on 21/11/18.
 */
public class DocumentoEmbalagemDaoImpl implements DocumentoEmbalagemDao {

    static Logger logger = LoggerFactory.getLogger(DocumentoEmbalagemDaoImpl.class);

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    @Transactional
    public BigDecimal gravarDocumentoEmbalagem(String sql) {
        try {

            Connection con = jdbcTemplate.getDataSource().getConnection();

            PreparedStatement statement = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

            statement.execute();

            PreparedStatement ps = con.prepareStatement("select SEQ_INTEGRACAO.currval from dual");

            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                BigDecimal generatedKey = rs.getBigDecimal(1);
                logger.info("Salvar DocumentoEmbalagem - Documento Embalagem - Numero Sequencia Integracao: {}", generatedKey);
                return generatedKey;
            }

        } catch (Exception ex) {
            logger.error("Erro DocumentoEmbalagem - Documento Embalagem: {}", ex.getMessage());
            System.out.print(ex.getMessage());
        }

        return null;
    }
}
