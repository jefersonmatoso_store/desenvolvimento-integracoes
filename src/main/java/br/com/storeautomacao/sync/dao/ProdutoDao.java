package br.com.storeautomacao.sync.dao;

import br.com.storeautomacao.sync.dto.ItemDto;
import br.com.storeautomacao.sync.dto.ProdutoDto;
import br.com.storeautomacao.sync.model.entity.Produto;
import br.com.storeautomacao.sync.model.entity.ProdutoComponente;
import br.com.storeautomacao.sync.model.entity.TipoUc;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by iomar on 16/11/18.
 */
@Repository
public interface ProdutoDao {

    List<Produto> obterListaProdutoPorSequenciaIntegracao(String sequenciaIntegracao);

    TipoUc obterTipoUcPorCodigoProdutoESequenciaIntegracao(String SequenciaIntegracao, String codigoProduto);

    List<ProdutoComponente> obterProdutosComponentesPorCodigoProdutoESequenciaIntegracao(String sequenciaIntegracao, String codigoProduto);

    String cadastrarProduto(ProdutoDto produtoDto);

    String cadastrarItem(ItemDto itemDto);

    List<ProdutoDto> obterTodosProdutos();

}
