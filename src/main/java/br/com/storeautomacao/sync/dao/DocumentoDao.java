package br.com.storeautomacao.sync.dao;


import br.com.storeautomacao.sync.model.entity.Documento;
import br.com.storeautomacao.sync.model.entity.DocumentoDetalhe;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by eduardo on 06/07/17.
 */
@Repository
public interface DocumentoDao {

    Documento obterDocumentoPorSequenciaIntegracao(BigDecimal sequenciaIntegracao);

    Documento obterDocumentoPorSequenciaIntegracaoNumeroDocumentoCodigoDepositante(BigDecimal sequenciaIntegracao, String numeroDocumento, String codigoDepositante );

    Documento obterDocumentoPorNumeroDocumento(String numeroDocumento);

    Documento obterDocumentoDIntegracao203(String numeroDocumento, String agrupador,  String codigoDepositante, BigDecimal tipoIntegracao);

    Documento obterDocumentoParaCancelamento(String numeroDocumento, String codigoDepositante, BigDecimal tipoIntegracao);

    DocumentoDetalhe obterDocumentoDetalhePorSequenciaIntegracao(BigDecimal sequenciaIntegracao);

    DocumentoDetalhe obterDocumentoDetalhePorSequenciaIntegracaoNumedoDocumento(BigDecimal sequenciaIntegracao, BigDecimal sequenciaDocumento);

    List<DocumentoDetalhe> obterListaDocumentoDetalhePorSequenciaIntegracao(BigDecimal sequenciaIntegracao);

    DocumentoDetalhe obterDocumentoDetalhePorSequenciaIntegracaoDetalhe(BigDecimal sequenciaIntegracao, BigDecimal sequenciaDocumentoDetalhe);

    DocumentoDetalhe obterDocumentoDetalheSalvo(BigDecimal sequenciaIntegracao, BigDecimal sequenciaDocumentoDetalhe);


}
