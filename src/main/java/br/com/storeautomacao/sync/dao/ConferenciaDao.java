package br.com.storeautomacao.sync.dao;


import br.com.storeautomacao.sync.model.entity.Conferencia;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by eduardo on 06/07/17.
 */
@Repository
public interface ConferenciaDao {

    String salvarConferencia(Conferencia conferencia);

    List<Conferencia> obterListaConferenciaPorNumeroDocumento(String numeroDocumento, String sequenciaIntegracao);

    Conferencia obterConferenciaPorNumeroDocumento(String numeroDocumento);

}
