package br.com.storeautomacao.sync.dao;

import br.com.storeautomacao.sync.model.entity.Conferencia;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ConferenciaDaoImpl implements ConferenciaDao {


    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private MongoOperations mongoOperation;


    public static final String CONFERENCIA = "conferencia";


    @Override
    public String salvarConferencia(Conferencia conferencia) {
        mongoOperation.save(conferencia, CONFERENCIA);
        return String.valueOf(conferencia.getSequenciaIntegracao());
    }

    @Override
    public List<Conferencia> obterListaConferenciaPorNumeroDocumento(String numeroDocumento, String sequenciaIntegracao){
        Query query = new Query(Criteria.where("numeroDocumento").regex(numeroDocumento));
        query.addCriteria(Criteria.where("sequenciaIntegracao").regex(sequenciaIntegracao));
        List<Conferencia> listaConferencia = mongoOperation.find(query, Conferencia.class, CONFERENCIA);
        return listaConferencia;
    }

    @Override
    public Conferencia obterConferenciaPorNumeroDocumento(String numeroDocumento) {
        Query query = new Query(Criteria.where("numeroDocumento").regex(numeroDocumento));
        Conferencia conferencia = mongoOperation.findOne(query, Conferencia.class, CONFERENCIA);
        return conferencia;
    }
}
