package br.com.storeautomacao.sync.dao;


import br.com.storeautomacao.sync.model.entity.Order;
import br.com.storeautomacao.sync.model.enuns.OrderStatusEnum;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

/**
 * Created by eduardo on 06/07/17.
 */
@Component
public class OrderDao {

    @Autowired
    private MongoOperations mongoOperation;

    public static final String COLLECTION_NAME = "order";

    @Async
    public String create(Order order) {
        order.setDeliveredDate(new Date());
        order.setOrderStatusEnum(OrderStatusEnum.DISPONIVEL);
        mongoOperation.save(order, COLLECTION_NAME);
        return order.getId();
    }

    public List<Order> findAll() {
        return mongoOperation.findAll(Order.class);
    }

    public Order findById(String id) {
        Query query = new Query(Criteria.where("id").is(id));
        return mongoOperation.findOne(query, Order.class, COLLECTION_NAME);
    }

    public Order pickUpOrder(String id) {
        Update update = new Update();
        update.set("takenDate", new Date());
        update.set("orderStatusEnum", OrderStatusEnum.RETIRADO);
        Query query = new Query(Criteria.where("id").is(id));
        return mongoOperation.findAndModify(query, update, new FindAndModifyOptions().returnNew(true), Order.class);
    }

    public List<Order> findByName(String name) {
        return this.findByField("nameRecipient", name);
    }

    /**
     * Retorna os Locais que contenha o texto informado (value) no
     * campo (field) informado.<br/>
     * As pesquisas de texto sao do tipo "<i>case insensitive</i>".
     *
     * @param field
     * @param value
     * @return
     */
    // TODO: Metodo candidato a refactoring: Ficar em classe abstrata
    private List<Order> findByField(String field, String value) {
        Query query = new Query(Criteria.where(field).regex(
                ".*" + value + ".*", "i"));
        return mongoOperation.find(query, Order.class, COLLECTION_NAME);
    }

    @Async
    public void remove(String id) {
        Query query = new Query(Criteria.where("id").is(id));
        mongoOperation.remove(query, Order.class);
    }

    @Async
    public void remove(Order order) {
        mongoOperation.remove(order);
    }

    public boolean deleteAll() {
        try {
            List<Order> orders = mongoOperation.findAll(Order.class);
            for (Order order : orders) {
                mongoOperation.remove(order);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    public List<Order> findBy(String nameRecipient, Date dateStart, Date dateEnd) {
        Criteria criteria = new Criteria();
        if (StringUtils.isNotEmpty(nameRecipient)) {
            criteria = criteria.and("nameRecipient").regex(".*" + nameRecipient + ".*", "i");
        }
        if (dateStart != null) {
            if (dateEnd != null) {
                criteria = criteria.and("deliveredDate").gte(dateStart).lte(dateEnd);
            } else {
                criteria = criteria.and("deliveredDate").gte(dateStart);
            }
        } else {
            if (dateEnd != null) {
                criteria = criteria.and("deliveredDate").lte(dateEnd);
            }
        }

        Query query = new Query(criteria);
//        query = query.with(new PageRequest(
//                --pageNumber,
//                QTD_PER_PAGE,
//                new Sort(Sort.Direction.DESC, "deliveredDate")));
        query = query.with(new Sort(Sort.Direction.DESC, "deliveredDate"));
        return mongoOperation.find(query, Order.class, COLLECTION_NAME);
    }

    public List<Order> findByIdUserLogado(String idUsuario) {
        return findByField("idUserRecipient", idUsuario);
    }

}
