package br.com.storeautomacao.sync.dao;

import br.com.storeautomacao.sync.model.entity.Documento;
import br.com.storeautomacao.sync.model.entity.DocumentoDetalhe;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;

@Repository
public class DocumentoDaoImpl implements DocumentoDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;


    /**
     *
     * @param sequenciaIntegracao
     *
     * (String codigoDepositante, String cnpjCpfEmpresa, String nomeEmpresa, String enderecoEmpresa, String bairroEmpresa, String municipioEmpresa,
       String ufEmpresa, String cepEmpresa, String codigoEmpresa, BigDecimal sequenciaIntegracao, BigDecimal sequenciaDocumento, String numeroDocumento,
       BigDecimal codigoEstabelecimento, Timestamp dataEmissao, String tipoDocumento, Timestamp dataPrevisaoMovimento, Timestamp dataMovimento,
       String serieDocumento, String naturezaOperacao, String descricaoNaturezaOperacao, String cfop, String agrupador)
     *
     * @return
     */
    @Override
    public Documento obterDocumentoPorSequenciaIntegracao(BigDecimal sequenciaIntegracao) {

        try {

            final String sql = "SELECT * FROM DOCUMENTO WHERE SEQUENCIAINTEGRACAO ='"+ sequenciaIntegracao + "' AND DOCUMENTO.CODIGODEPOSITANTE = 'M9' AND DOCUMENTO.MODELODOCUMENTO IS NULL";

            Documento documento =  jdbcTemplate.queryForObject(sql,((rs, rowNum) -> new Documento(rs.getString("CODIGODEPOSITANTE"),
                    rs.getString("CNPJCPFEMPRESA"),rs.getString("NOMEEMPRESA"),rs.getString("ENDERECOEMPRESA"),
                    rs.getString("BAIRROEMPRESA"),rs.getString("MUNICIPIOEMPRESA"),rs.getString("UFEMPRESA"),
                    rs.getString("CEPEMPRESA"),rs.getString("CODIGOEMPRESA"),rs.getBigDecimal("SEQUENCIAINTEGRACAO"),
                    rs.getBigDecimal("SEQUENCIADOCUMENTO"),rs.getString("NUMERODOCUMENTO"),rs.getBigDecimal("CODIGOESTABELECIMENTO"),
                    rs.getTimestamp("DATAEMISSAO"), rs.getString("TIPODOCUMENTO"),rs.getTimestamp("DATAPREVISAOMOVIMENTO"),
                    rs.getTimestamp("DATAMOVIMENTO"),rs.getString("SERIEDOCUMENTO"),rs.getString("NATUREZAOPERACAO"),
                    rs.getString("DESCRICAONATUREZAOPERACAO"),rs.getString("CFOP"),rs.getString("AGRUPADOR"), rs.getBigDecimal("TIPOINTEGRACAO"))));

            return documento;
        } catch (Exception ex){
            return null;
        }

    }

    @Override
    public Documento obterDocumentoPorSequenciaIntegracaoNumeroDocumentoCodigoDepositante(BigDecimal sequenciaIntegracao, String numeroDocumento, String codigoDepositante ) {

        try {

            final String sql = "SELECT * FROM DOCUMENTO WHERE DOCUMENTO.NUMERODOCUMENTO = '"+ numeroDocumento + "' AND DOCUMENTO.CODIGODEPOSITANTE = '"+ codigoDepositante + "' AND DOCUMENTO.MODELODOCUMENTO IS NULL AND TIPOINTEGRACAO = 101";

            Documento documento =  jdbcTemplate.queryForObject(sql,((rs, rowNum) -> new Documento(rs.getString("CODIGODEPOSITANTE"),
                    rs.getString("CNPJCPFEMPRESA"),rs.getString("NOMEEMPRESA"),rs.getString("ENDERECOEMPRESA"),
                    rs.getString("BAIRROEMPRESA"),rs.getString("MUNICIPIOEMPRESA"),rs.getString("UFEMPRESA"),
                    rs.getString("CEPEMPRESA"),rs.getString("CODIGOEMPRESA"),rs.getBigDecimal("SEQUENCIAINTEGRACAO"),
                    rs.getBigDecimal("SEQUENCIADOCUMENTO"),rs.getString("NUMERODOCUMENTO"),rs.getBigDecimal("CODIGOESTABELECIMENTO"),
                    rs.getTimestamp("DATAEMISSAO"), rs.getString("TIPODOCUMENTO"),rs.getTimestamp("DATAPREVISAOMOVIMENTO"),
                    rs.getTimestamp("DATAMOVIMENTO"),rs.getString("SERIEDOCUMENTO"),rs.getString("NATUREZAOPERACAO"),
                    rs.getString("DESCRICAONATUREZAOPERACAO"),rs.getString("CFOP"),rs.getString("AGRUPADOR"), rs.getBigDecimal("TIPOINTEGRACAO"))));

            return documento;
        } catch (Exception ex){
            return null;
        }

    }

    @Override
    public Documento obterDocumentoPorNumeroDocumento(String numeroDocumento) {

        try {

            final String sql = "SELECT * FROM DOCUMENTO WHERE NUMERODOCUMENTO ='"+ numeroDocumento + "'";

            Documento documento =  jdbcTemplate.queryForObject(sql,((rs, rowNum) -> new Documento(rs.getString("CODIGODEPOSITANTE"),
                    rs.getString("CNPJCPFEMPRESA"),rs.getString("NOMEEMPRESA"),rs.getString("ENDERECOEMPRESA"),
                    rs.getString("BAIRROEMPRESA"),rs.getString("MUNICIPIOEMPRESA"),rs.getString("UFEMPRESA"),
                    rs.getString("CEPEMPRESA"),rs.getString("CODIGOEMPRESA"),rs.getBigDecimal("SEQUENCIAINTEGRACAO"),
                    rs.getBigDecimal("SEQUENCIADOCUMENTO"),rs.getString("NUMERODOCUMENTO"),rs.getBigDecimal("CODIGOESTABELECIMENTO"),
                    rs.getTimestamp("DATAEMISSAO"), rs.getString("TIPODOCUMENTO"),rs.getTimestamp("DATAPREVISAOMOVIMENTO"),
                    rs.getTimestamp("DATAMOVIMENTO"),rs.getString("SERIEDOCUMENTO"),rs.getString("NATUREZAOPERACAO"),
                    rs.getString("DESCRICAONATUREZAOPERACAO"),rs.getString("CFOP"),rs.getString("AGRUPADOR"), rs.getBigDecimal("TIPOINTEGRACAO"))));

            return documento;
        } catch (Exception ex){
            return null;
        }

    }

    @Override
    public Documento obterDocumentoDIntegracao203(String numeroDocumento, String agrupador,  String codigoDepositante, BigDecimal tipoIntegracao) {

        try {

            final String sql = "SELECT * FROM DOCUMENTO WHERE NUMERODOCUMENTO ='"+ numeroDocumento + "' AND AGRUPADOR = '"+ agrupador + "' AND CODIGODEPOSITANTE = '"+ codigoDepositante + "' AND TIPOINTEGRACAO = "+ tipoIntegracao + "";

            Documento documento =  jdbcTemplate.queryForObject(sql,((rs, rowNum) -> new Documento(rs.getString("CODIGODEPOSITANTE"),
                    rs.getString("CNPJCPFEMPRESA"),rs.getString("NOMEEMPRESA"),rs.getString("ENDERECOEMPRESA"),
                    rs.getString("BAIRROEMPRESA"),rs.getString("MUNICIPIOEMPRESA"),rs.getString("UFEMPRESA"),
                    rs.getString("CEPEMPRESA"),rs.getString("CODIGOEMPRESA"),rs.getBigDecimal("SEQUENCIAINTEGRACAO"),
                    rs.getBigDecimal("SEQUENCIADOCUMENTO"),rs.getString("NUMERODOCUMENTO"),rs.getBigDecimal("CODIGOESTABELECIMENTO"),
                    rs.getTimestamp("DATAEMISSAO"), rs.getString("TIPODOCUMENTO"),rs.getTimestamp("DATAPREVISAOMOVIMENTO"),
                    rs.getTimestamp("DATAMOVIMENTO"),rs.getString("SERIEDOCUMENTO"),rs.getString("NATUREZAOPERACAO"),
                    rs.getString("DESCRICAONATUREZAOPERACAO"),rs.getString("CFOP"),rs.getString("AGRUPADOR"), rs.getBigDecimal("TIPOINTEGRACAO"))));

            return documento;
        } catch (Exception ex){
            return null;
        }

    }

    @Override
    public Documento obterDocumentoParaCancelamento(String numeroDocumento, String codigoDepositante, BigDecimal tipoIntegracao) {

        try {

            final String sql = "SELECT * FROM DOCUMENTO WHERE NUMERODOCUMENTO ='"+ numeroDocumento + "' AND CODIGODEPOSITANTE = '"+ codigoDepositante + "' AND TIPOINTEGRACAO = "+ tipoIntegracao + " AND AGRUPADOR <> 0";

            Documento documento =  jdbcTemplate.queryForObject(sql,((rs, rowNum) -> new Documento(rs.getString("CODIGODEPOSITANTE"),
                    rs.getString("CNPJCPFEMPRESA"),rs.getString("NOMEEMPRESA"),rs.getString("ENDERECOEMPRESA"),
                    rs.getString("BAIRROEMPRESA"),rs.getString("MUNICIPIOEMPRESA"),rs.getString("UFEMPRESA"),
                    rs.getString("CEPEMPRESA"),rs.getString("CODIGOEMPRESA"),rs.getBigDecimal("SEQUENCIAINTEGRACAO"),
                    rs.getBigDecimal("SEQUENCIADOCUMENTO"),rs.getString("NUMERODOCUMENTO"),rs.getBigDecimal("CODIGOESTABELECIMENTO"),
                    rs.getTimestamp("DATAEMISSAO"), rs.getString("TIPODOCUMENTO"),rs.getTimestamp("DATAPREVISAOMOVIMENTO"),
                    rs.getTimestamp("DATAMOVIMENTO"),rs.getString("SERIEDOCUMENTO"),rs.getString("NATUREZAOPERACAO"),
                    rs.getString("DESCRICAONATUREZAOPERACAO"),rs.getString("CFOP"),rs.getString("AGRUPADOR"), rs.getBigDecimal("TIPOINTEGRACAO"))));

            return documento;
        } catch (Exception ex){
            return null;
        }

    }

    @Override
    public DocumentoDetalhe obterDocumentoDetalhePorSequenciaIntegracao(BigDecimal sequenciaIntegracao) {

        final String sql = "SELECT * FROM DOCUMENTODETALHE WHERE SEQUENCIAINTEGRACAO ='"+ sequenciaIntegracao + "'";

        DocumentoDetalhe documentoDetalhe =  jdbcTemplate.queryForObject(sql,((rs, rowNum) -> new DocumentoDetalhe(rs.getBigDecimal("SEQUENCIAINTEGRACAO"),
                rs.getBigDecimal("SEQUENCIADOCUMENTO"),rs.getBigDecimal("SEQUENCIADETALHE"),rs.getString("CODIGOPRODUTO"),rs.getBigDecimal("QUANTIDADEMOVIMENTO"),
                rs.getBigDecimal("VALORUNITARIO"),rs.getString("CLASSEPRODUTO"))));


        return documentoDetalhe;
    }

    @Override
    public DocumentoDetalhe obterDocumentoDetalhePorSequenciaIntegracaoNumedoDocumento(BigDecimal sequenciaIntegracao, BigDecimal sequenciaDocumento) {

        final String sql = "SELECT * FROM DOCUMENTODETALHE WHERE SEQUENCIAINTEGRACAO ='"+ sequenciaIntegracao + "' AND SEQUENCIADOCUMENTO ='"+ sequenciaDocumento + "'";

        try {
        DocumentoDetalhe documentoDetalhe =  jdbcTemplate.queryForObject(sql,((rs, rowNum) -> new DocumentoDetalhe(rs.getBigDecimal("SEQUENCIAINTEGRACAO"),
                rs.getBigDecimal("SEQUENCIADOCUMENTO"),rs.getBigDecimal("SEQUENCIADETALHE"),rs.getString("CODIGOPRODUTO"),rs.getBigDecimal("QUANTIDADEMOVIMENTO"),
                rs.getBigDecimal("VALORUNITARIO"),rs.getString("CLASSEPRODUTO"))));

            return documentoDetalhe;

        } catch (Exception ex){
            return null;
        }

    }

    @Override
    public List<DocumentoDetalhe> obterListaDocumentoDetalhePorSequenciaIntegracao(BigDecimal sequenciaIntegracao) {

        final String sql = "SELECT * FROM DOCUMENTODETALHE WHERE SEQUENCIAINTEGRACAO ='"+ sequenciaIntegracao + "'";

        List<DocumentoDetalhe> listaDocumentoDetalhe =  jdbcTemplate.query(sql,((rs, rowNum) -> new DocumentoDetalhe(rs.getBigDecimal("SEQUENCIAINTEGRACAO"),
                rs.getBigDecimal("SEQUENCIADOCUMENTO"),rs.getBigDecimal("SEQUENCIADETALHE"),rs.getString("CODIGOPRODUTO"),rs.getBigDecimal("QUANTIDADEMOVIMENTO"),
                rs.getBigDecimal("VALORUNITARIO"),rs.getString("CLASSEPRODUTO"))));

        return listaDocumentoDetalhe;
    }


    @Override
    public DocumentoDetalhe obterDocumentoDetalhePorSequenciaIntegracaoDetalhe(BigDecimal sequenciaIntegracao, BigDecimal sequenciaDocumentoDetalhe) {

        final String sql = "SELECT * FROM DOCUMENTODETALHE WHERE SEQUENCIAINTEGRACAO ='"+ sequenciaIntegracao + "' AND SEQUENCIADETALHE ='"+ sequenciaDocumentoDetalhe + "'";

        DocumentoDetalhe documentoDetalhe =  jdbcTemplate.queryForObject(sql,((rs, rowNum) -> new DocumentoDetalhe(rs.getBigDecimal("SEQUENCIAINTEGRACAO"),
                rs.getBigDecimal("SEQUENCIADOCUMENTO"),rs.getBigDecimal("SEQUENCIADETALHE"),rs.getString("CODIGOPRODUTO"),rs.getBigDecimal("QUANTIDADEMOVIMENTO"),
                rs.getBigDecimal("VALORUNITARIO"),rs.getString("CLASSEPRODUTO"))));


        return documentoDetalhe;
    }

    @Override
    public DocumentoDetalhe obterDocumentoDetalheSalvo(BigDecimal sequenciaIntegracao, BigDecimal sequenciaDocumentoDetalhe) {

        final String sql = "SELECT * FROM DOCUMENTODETALHE WHERE SEQUENCIAINTEGRACAO ='"+ sequenciaIntegracao + "' AND SEQUENCIADETALHE ='"+ sequenciaDocumentoDetalhe + "' AND TIPOINTEGRACAO = 152";

        try {
            DocumentoDetalhe documentoDetalhe =  jdbcTemplate.queryForObject(sql,((rs, rowNum) -> new DocumentoDetalhe(rs.getBigDecimal("SEQUENCIAINTEGRACAO"),
                    rs.getBigDecimal("SEQUENCIADOCUMENTO"),rs.getBigDecimal("SEQUENCIADETALHE"),rs.getString("CODIGOPRODUTO"),rs.getBigDecimal("QUANTIDADEMOVIMENTO"),
                    rs.getBigDecimal("VALORUNITARIO"),rs.getString("CLASSEPRODUTO"))));


            return documentoDetalhe;
        } catch (Exception ex){
            return null;
        }

    }
}
