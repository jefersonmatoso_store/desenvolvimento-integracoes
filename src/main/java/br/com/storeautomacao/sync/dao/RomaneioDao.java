package br.com.storeautomacao.sync.dao;

import org.springframework.stereotype.Repository;

import java.math.BigDecimal;

/**
 * Created by iomar on 21/11/18.
 */
public interface RomaneioDao {
    public BigDecimal gravarRomaneio(String sql);
}
