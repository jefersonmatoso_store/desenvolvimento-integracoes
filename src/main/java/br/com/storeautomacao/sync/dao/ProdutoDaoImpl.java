package br.com.storeautomacao.sync.dao;

import br.com.storeautomacao.sync.dto.ItemDto;
import br.com.storeautomacao.sync.dto.ProdutoDto;
import br.com.storeautomacao.sync.model.entity.Conferencia;
import br.com.storeautomacao.sync.model.entity.Produto;
import br.com.storeautomacao.sync.model.entity.ProdutoComponente;
import br.com.storeautomacao.sync.model.entity.TipoUc;
import br.com.storeautomacao.sync.model.mapper.ProdutoComponenteRowMapper;
import br.com.storeautomacao.sync.model.mapper.ProdutoRowMapper;
import br.com.storeautomacao.sync.model.mapper.TipoUCRowMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

/**
 * Created by iomar on 16/11/18.
 */
@Repository
public class ProdutoDaoImpl implements ProdutoDao {

    static Logger logger = LoggerFactory.getLogger(ProdutoDaoImpl.class);

    public static final String PRODUTO = "produto";

    public static final String ITEM = "item";

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private MongoOperations mongoOperation;

    @Override
    public List<Produto> obterListaProdutoPorSequenciaIntegracao(String sequenciaIntegracao) {
        String sql = "SELECT SEQUENCIAINTEGRACAO, TIPOINTEGRACAO, CODIGOEMPRESA, CODIGOPRODUTO, DESCRICAOPRODUTO, GRUPOPRODUTO, DESCRICAOGRUPOPRODUTO, DESCRICAOPRODUTODET, CODIGOLINHA, DESCRICAOLINHA FROM PRODUTO WHERE SEQUENCIAINTEGRACAO = ?";
        try {
            List<Produto> produtos = jdbcTemplate.query(sql,  new ProdutoRowMapper(), sequenciaIntegracao);
            return produtos;
        } catch (Exception ex) {
            logger.error("SELECT * FROM PRODUTO WHERE SEQUENCIAINTEGRACAO = ?: {} ", ex.getMessage());
            return null;
        }
    }

    @Override
    public TipoUc obterTipoUcPorCodigoProdutoESequenciaIntegracao(String sequenciaIntegracao, String codigoProduto) {
        String sql = "SELECT SEQUENCIAINTEGRACAO, TIPOINTEGRACAO, COMPRIMENTOPRODUTO, LARGURAPRODUTO, ALTURAPRODUTO, PESOLIQUIDO FROM TIPOUC WHERE SEQUENCIAINTEGRACAO = ? AND CODIGOPRODUTO = ?";
        try {
            TipoUc tipoUc = jdbcTemplate.queryForObject(sql,  new TipoUCRowMapper(), sequenciaIntegracao, codigoProduto);
            return tipoUc;
        } catch (Exception ex) {
            logger.error("SELECT * FROM TIPOUC WHERE CODIGOPRODUTO = ?: {} ", ex.getMessage());
            return null;
        }
    }

    @Override
    public List<ProdutoComponente> obterProdutosComponentesPorCodigoProdutoESequenciaIntegracao(String sequenciaIntegracao, String codigoProduto) {
        String sql = "SELECT SEQUENCIAINTEGRACAO, TIPOINTEGRACAO, CODIGOEMPRESA, CODIGOPRODUTO, CODIGOPRODUTOCOMPONENTE, QUANTIDADECOMPONENTE, SEQUENCIACOMPONENTE FROM PRODUTOCOMPONENTE WHERE SEQUENCIAINTEGRACAO = ? AND CODIGOPRODUTO = ?";
        try {
            List<ProdutoComponente> produtoComponentes = jdbcTemplate.query(sql,  new ProdutoComponenteRowMapper(), sequenciaIntegracao, codigoProduto);
            return produtoComponentes;
        } catch (Exception ex) {
            logger.error("SELECT * FROM TIPOUC WHERE CODIGOPRODUTO = ?: {} ", ex.getMessage());
            return null;
        }
    }

    @Override
    public String cadastrarProduto(ProdutoDto produtoDto) {
        mongoOperation.save(produtoDto, PRODUTO);
        return String.valueOf(produtoDto.getId());
    }

    @Override
    public String cadastrarItem(ItemDto itemDto) {
        mongoOperation.save(itemDto, ITEM);
        return String.valueOf(itemDto.getId());
    }

    @Override
    public List<ProdutoDto> obterTodosProdutos() {
        return mongoOperation.findAll(ProdutoDto.class);
    }
}
