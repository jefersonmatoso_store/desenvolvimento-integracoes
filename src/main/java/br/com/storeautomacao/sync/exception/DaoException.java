package br.com.storeautomacao.sync.exception;

public class DaoException extends StoreAutomacaoSyncServiceException {

    private static final long serialVersionUID = -8454540961921832433L;

    public DaoException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public DaoException(final String message, final Object[] params, final Throwable cause) {
        super(message, params, cause);
    }

    public DaoException(final String message) {
        super(message);
    }

}
