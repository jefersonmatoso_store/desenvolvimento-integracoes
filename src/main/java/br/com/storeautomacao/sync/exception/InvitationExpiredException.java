package br.com.storeautomacao.sync.exception;

import java.text.MessageFormat;

/**
 * Created by alexandre on 18/09/17.
 */
public class InvitationExpiredException extends StoreAutomacaoSyncServiceException {

    public InvitationExpiredException(final String mensagem) {
        super(mensagem);
    }

    public InvitationExpiredException(final String mensagem, final Object[] params) {
        super(MessageFormat.format(mensagem, params));
    }

    public InvitationExpiredException(final String mensagem, final Throwable cause) {
        super(mensagem, cause);
    }

    public InvitationExpiredException(final String mensagem, final Object[] params, final Throwable cause) {
        super(MessageFormat.format(mensagem, params), cause);
    }

    protected InvitationExpiredException() {
        super();
    }

    public InvitationExpiredException(final Throwable cause) {
        super(cause);
    }
    
}
