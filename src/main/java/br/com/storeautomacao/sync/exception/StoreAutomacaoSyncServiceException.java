package br.com.storeautomacao.sync.exception;

import java.text.MessageFormat;

public class StoreAutomacaoSyncServiceException extends RuntimeException {

    private static final long serialVersionUID = 3422494428773141388L;

    protected Object[] params;

    public StoreAutomacaoSyncServiceException(final String mensagem) {
        super(mensagem);
    }

    public StoreAutomacaoSyncServiceException(final String mensagem, final Object[] params) {
        super(MessageFormat.format(mensagem, params));
    }

    public StoreAutomacaoSyncServiceException(final String mensagem, final Throwable cause) {
        super(mensagem, cause);
    }

    public StoreAutomacaoSyncServiceException(final String mensagem, final Object[] params, final Throwable cause) {
        super(MessageFormat.format(mensagem, params), cause);
    }

    protected StoreAutomacaoSyncServiceException() {
        super();
    }

    public StoreAutomacaoSyncServiceException(final Throwable cause) {
        super(cause);
    }

    public Object[] getParams() {
        return params;
    }

}
