package br.com.storeautomacao.sync.model.wms.Sales;

import br.com.storeautomacao.sync.model.wms.Header;
import br.com.storeautomacao.sync.model.wms.ListOfInventoryHistories;
import com.thoughtworks.xstream.annotations.XStreamAlias;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="LgfData")
@XStreamAlias("LgfData")
public class LgfDataSales
{
    private Header Header;

    private ListOfPointOfSales ListOfPointOfSales;

    public Header getHeader ()
    {
        return Header;
    }

    public void setHeader (Header Header)
    {
        this.Header = Header;
    }

    public ListOfPointOfSales getListOfPointOfSales() {
        return ListOfPointOfSales;
    }

    public void setListOfPointOfSales(ListOfPointOfSales listOfPointOfSales) {
        ListOfPointOfSales = listOfPointOfSales;
    }
}
