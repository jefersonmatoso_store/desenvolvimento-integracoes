package br.com.storeautomacao.sync.model.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "embalagem")
public class Embalagem {

    @Id
    @JsonProperty("idEmbalagem")
    /** Identificacao na base de dados*/
    private String id;
    private String facility_code;
    private String company_code;
    private String ob_lpn_nbr;
    private String ob_lpn_weight;
    private String lpnType;
    private DocumentoDetalhe documentoDetalhe;



    public Embalagem(){}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFacility_code() {
        return facility_code;
    }

    public void setFacility_code(String facility_code) {
        this.facility_code = facility_code;
    }

    public String getCompany_code() {
        return company_code;
    }

    public void setCompany_code(String company_code) {
        this.company_code = company_code;
    }

    public String getOb_lpn_nbr() {
        return ob_lpn_nbr;
    }

    public void setOb_lpn_nbr(String ob_lpn_nbr) {
        this.ob_lpn_nbr = ob_lpn_nbr;
    }

    public String getOb_lpn_weight() {
        return ob_lpn_weight;
    }

    public void setOb_lpn_weight(String ob_lpn_weight) {
        this.ob_lpn_weight = ob_lpn_weight;
    }

    public String getLpnType() {
        return lpnType;
    }

    public void setLpnType(String lpnType) {
        this.lpnType = lpnType;
    }

    public DocumentoDetalhe getDocumentoDetalhe() {
        return documentoDetalhe;
    }

    public void setDocumentoDetalhe(DocumentoDetalhe documentoDetalhe) {
        this.documentoDetalhe = documentoDetalhe;
    }
}
