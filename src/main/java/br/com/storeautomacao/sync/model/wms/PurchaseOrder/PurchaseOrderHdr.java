package br.com.storeautomacao.sync.model.wms.PurchaseOrder;

public class PurchaseOrderHdr {

    private String cust_field_2;

    private String cust_field_1;

    private String company_code;

    private String rma_nbr;

    private String cust_field_4;

    private String cust_field_3;

    private String cust_addr2;

    private String cust_field_5;

    private String cust_addr3;

    private String cust_nbr;

    private String dept_code;

    private String action_code;

    private String ord_date;

    private String ref_nbr;

    private String facility_code;

    private String vendor_code;

    private String po_nbr;

    private String cust_name;

    private String cust_addr;

    private String ship_date;

    private String po_type;

    private String delivery_date;

    private String cancel_date;

    private String sold_to_legal_name;

    public String getCust_field_2 ()
    {
        return cust_field_2;
    }

    public void setCust_field_2 (String cust_field_2)
    {
        this.cust_field_2 = cust_field_2;
    }

    public String getCust_field_1 ()
    {
        return cust_field_1;
    }

    public void setCust_field_1 (String cust_field_1)
    {
        this.cust_field_1 = cust_field_1;
    }

    public String getCompany_code ()
    {
        return company_code;
    }

    public void setCompany_code (String company_code)
    {
        this.company_code = company_code;
    }

    public String getRma_nbr ()
    {
        return rma_nbr;
    }

    public void setRma_nbr (String rma_nbr)
    {
        this.rma_nbr = rma_nbr;
    }

    public String getCust_field_4 ()
    {
        return cust_field_4;
    }

    public void setCust_field_4 (String cust_field_4)
    {
        this.cust_field_4 = cust_field_4;
    }

    public String getCust_field_3 ()
    {
        return cust_field_3;
    }

    public void setCust_field_3 (String cust_field_3)
    {
        this.cust_field_3 = cust_field_3;
    }

    public String getCust_addr2 ()
    {
        return cust_addr2;
    }

    public void setCust_addr2 (String cust_addr2)
    {
        this.cust_addr2 = cust_addr2;
    }

    public String getCust_field_5 ()
    {
        return cust_field_5;
    }

    public void setCust_field_5 (String cust_field_5)
    {
        this.cust_field_5 = cust_field_5;
    }

    public String getCust_addr3 ()
    {
        return cust_addr3;
    }

    public void setCust_addr3 (String cust_addr3)
    {
        this.cust_addr3 = cust_addr3;
    }

    public String getCust_nbr ()
    {
        return cust_nbr;
    }

    public void setCust_nbr (String cust_nbr)
    {
        this.cust_nbr = cust_nbr;
    }

    public String getDept_code ()
    {
        return dept_code;
    }

    public void setDept_code (String dept_code)
    {
        this.dept_code = dept_code;
    }

    public String getAction_code ()
    {
        return action_code;
    }

    public void setAction_code (String action_code)
    {
        this.action_code = action_code;
    }

    public String getOrd_date ()
    {
        return ord_date;
    }

    public void setOrd_date (String ord_date)
    {
        this.ord_date = ord_date;
    }

    public String getRef_nbr ()
    {
        return ref_nbr;
    }

    public void setRef_nbr (String ref_nbr)
    {
        this.ref_nbr = ref_nbr;
    }

    public String getFacility_code ()
    {
        return facility_code;
    }

    public void setFacility_code (String facility_code)
    {
        this.facility_code = facility_code;
    }

    public String getVendor_code ()
    {
        return vendor_code;
    }

    public void setVendor_code (String vendor_code)
    {
        this.vendor_code = vendor_code;
    }

    public String getPo_nbr ()
    {
        return po_nbr;
    }

    public void setPo_nbr (String po_nbr)
    {
        this.po_nbr = po_nbr;
    }

    public String getCust_name ()
    {
        return cust_name;
    }

    public void setCust_name (String cust_name)
    {
        this.cust_name = cust_name;
    }

    public String getCust_addr ()
    {
        return cust_addr;
    }

    public void setCust_addr (String cust_addr)
    {
        this.cust_addr = cust_addr;
    }

    public String getShip_date ()
    {
        return ship_date;
    }

    public void setShip_date (String ship_date)
    {
        this.ship_date = ship_date;
    }

    public String getPo_type ()
    {
        return po_type;
    }

    public void setPo_type (String po_type)
    {
        this.po_type = po_type;
    }

    public String getDelivery_date ()
    {
        return delivery_date;
    }

    public void setDelivery_date (String delivery_date)
    {
        this.delivery_date = delivery_date;
    }

    public String getCancel_date ()
    {
        return cancel_date;
    }

    public void setCancel_date (String cancel_date)
    {
        this.cancel_date = cancel_date;
    }

    public String getSold_to_legal_name ()
    {
        return sold_to_legal_name;
    }

    public void setSold_to_legal_name (String sold_to_legal_name)
    {
        this.sold_to_legal_name = sold_to_legal_name;
    }

}
