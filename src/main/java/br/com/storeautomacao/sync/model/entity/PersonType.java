package br.com.storeautomacao.sync.model.entity;

public enum PersonType {
    CONDOMINO,
    VISITANTE,
    FUNCIONARIO;
}
