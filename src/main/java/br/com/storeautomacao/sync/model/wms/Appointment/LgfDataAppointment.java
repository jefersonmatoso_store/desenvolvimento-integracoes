package br.com.storeautomacao.sync.model.wms.Appointment;

import br.com.storeautomacao.sync.model.wms.Header;
import com.thoughtworks.xstream.annotations.XStreamAlias;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="LgfData")
@XStreamAlias("LgfData")
public class LgfDataAppointment {

    private Header Header;

    private ListOfAppointments ListOfAppointments;

    public Header getHeader ()
    {
        return Header;
    }

    public void setHeader (Header Header)
    {
        this.Header = Header;
    }

    public ListOfAppointments getListOfAppointments() {
        return ListOfAppointments;
    }

    public void setListOfAppointments(ListOfAppointments listOfAppointments) {
        ListOfAppointments = listOfAppointments;
    }

}
