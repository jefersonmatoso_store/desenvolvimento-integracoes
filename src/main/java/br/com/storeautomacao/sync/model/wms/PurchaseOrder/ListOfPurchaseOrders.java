package br.com.storeautomacao.sync.model.wms.PurchaseOrder;

public class ListOfPurchaseOrders {

    private PurchaseOrder purchase_order;

    public PurchaseOrder getPurchase_order() {
        return purchase_order;
    }

    public void setPurchase_order(PurchaseOrder purchase_order) {
        this.purchase_order = purchase_order;
    }
}
