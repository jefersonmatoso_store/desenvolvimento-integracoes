package br.com.storeautomacao.sync.model.wms;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="LgfData")
@XStreamAlias("LgfData")
public class LgfDataItemPrePack
{
    private Header Header;

    private ListOfItemPrePacks ListOfItemPrePacks;

    public Header getHeader ()
    {
        return Header;
    }

    public void setHeader (Header Header)
    {
        this.Header = Header;
    }

    public ListOfItemPrePacks getListOfItemPrePacks() {
        return ListOfItemPrePacks;
    }

    public void setListOfItemPrePacks(ListOfItemPrePacks listOfItemPrePacks) {
        this.ListOfItemPrePacks = listOfItemPrePacks;
    }
}
