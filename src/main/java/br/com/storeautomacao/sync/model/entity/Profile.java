package br.com.storeautomacao.sync.model.entity;


import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * <i>Entity</i> que represente os perfis no sistema.
 * <p>
 * Created by Iomar on 19/09/2017.
 */
@Document(collection = "profiles")
public class Profile {
    @Id
    @JsonProperty("idProfile")
    /** Identificacao na base de dados*/
    private String id;

    private String name;

    private String picture;

    private ProfileType profileType;

    @DBRef
    private Person person;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public ProfileType getProfileType() {
        return profileType;
    }

    public void setProfileType(ProfileType profileType) {
        this.profileType = profileType;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
