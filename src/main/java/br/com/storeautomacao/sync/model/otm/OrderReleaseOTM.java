package br.com.storeautomacao.sync.model.otm;

public class OrderReleaseOTM {

    private Transmission Transmission;

    public Transmission getTransmission ()
    {
        return Transmission;
    }

    public void setTransmission (Transmission Transmission)
    {
        this.Transmission = Transmission;
    }

}
