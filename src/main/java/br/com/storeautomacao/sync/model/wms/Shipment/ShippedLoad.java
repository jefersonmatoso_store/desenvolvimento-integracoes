package br.com.storeautomacao.sync.model.wms.Shipment;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.ArrayList;
import java.util.List;

@XStreamAlias("shipped_load")
public class ShippedLoad {

    private Load load = new Load();


    /**
     * @XStreamImplicit(itemFieldName="shipped_load")
     *     private List<ShippedLoad> shippedLoads = new ArrayList<ShippedLoad>();
     *
     *     public List<ShippedLoad> getShippedLoads() {
     *         return shippedLoads;
     *     }
     */
    @XStreamImplicit(itemFieldName="ob_stop")
    private List<ObStop> ob_stop = new ArrayList<ObStop>();

    public Load getLoad() {
        return load;
    }

    public void setLoad(Load load) {
        this.load = load;
    }

    public List<ObStop> getOb_stop() {
        return ob_stop;
    }

}
