package br.com.storeautomacao.sync.model.entity;

import java.math.BigDecimal;
import java.util.Date;

public class Romaneio {

    private BigDecimal sequenciaIntegracao;
    private BigDecimal sequenciaRomaneio;
    private BigDecimal tipoIntegracao;
    private String numeroRomaneio;
    private String documentoQualidade;
    private String enderecoDoca;
    private Date dataPrevisaoMovimento;
    private String tempoPrevistoOperacao;
    private String observacao;
    private String codigoProcedencia;
    private String cnpjCpfProcedencia;
    private String nomeProcedencia;
    private String enderecoProcedencia;
    private String bairroProcedencia;
    private String municipioProcedencia;
    private String ufProcedencia;
    private String cepProcedencia;
    private String inscricaoProcedencia;
    private String tipoProcedencia;
    private String tipoPessoaProcedencia;
    private String codigoTransportadora;
    private String cnpjCpfTransportadora;
    private String nomeTransportadora;
    private String enderecoTransportadora;
    private String bairroTransportadora;
    private String municipioTransportadora;
    private String ufTransportadora;
    private String ceptransportadora;
    private String inscricaoTransportadora;
    private String tipoTransportadora;
    private String tipoPessoaTransportadora;
    private String placaVeiculo;
    private String descricaoVeiculo;
    private String numeroCertificado;
    private String numeroChassi;
    private String numeroRenavam;
    private String municipioVeiculo;
    private String ufVeiculo;
    private String anoVeiculo;
    private String identificadorVeiculo;
    private String tipoVeiculo;
    private String descricaoTipoVeiculo;
    private String marcaModeloVeiculo;
    private String descricaoMarcaModeloVeiculo;
    private String idMarcaModeloVeiculo;
    private String corVeiculo;
    private String descricaoCorVeiculo;
    private String idCorVeiculo;
    private BigDecimal codigoCombustivel;
    private String descricaoCombustivel;
    private String idCombustivel;
    private String numeroCracha;
    private String cpfPessoa;
    private String nomePessoa;
    private String rgpessoa;
    private String municipioPessoa;
    private String ufPessoa;
    private String cnhPessoa;
    private Date dataVencimentocnh;
    private String numeroMopp;
    private Date dataVencimentoMopp;
    private BigDecimal codigoRomaneio;
    private BigDecimal codigoEstabelecimento;
    private BigDecimal tipoPicking;

    public BigDecimal getSequenciaIntegracao() {
        return sequenciaIntegracao;
    }

    public void setSequenciaIntegracao(BigDecimal sequenciaIntegracao) {
        this.sequenciaIntegracao = sequenciaIntegracao;
    }

    public BigDecimal getSequenciaRomaneio() {
        return sequenciaRomaneio;
    }

    public void setSequenciaRomaneio(BigDecimal sequenciaRomaneio) {
        this.sequenciaRomaneio = sequenciaRomaneio;
    }

    public BigDecimal getTipoIntegracao() {
        return tipoIntegracao;
    }

    public void setTipoIntegracao(BigDecimal tipoIntegracao) {
        this.tipoIntegracao = tipoIntegracao;
    }

    public String getNumeroRomaneio() {
        return numeroRomaneio;
    }

    public void setNumeroRomaneio(String numeroRomaneio) {
        this.numeroRomaneio = numeroRomaneio;
    }

    public String getDocumentoQualidade() {
        return documentoQualidade;
    }

    public void setDocumentoQualidade(String documentoQualidade) {
        this.documentoQualidade = documentoQualidade;
    }

    public String getEnderecoDoca() {
        return enderecoDoca;
    }

    public void setEnderecoDoca(String enderecoDoca) {
        this.enderecoDoca = enderecoDoca;
    }

    public Date getDataPrevisaoMovimento() {
        return dataPrevisaoMovimento;
    }

    public void setDataPrevisaoMovimento(Date dataPrevisaoMovimento) {
        this.dataPrevisaoMovimento = dataPrevisaoMovimento;
    }

    public String getTempoPrevistoOperacao() {
        return tempoPrevistoOperacao;
    }

    public void setTempoPrevistoOperacao(String tempoPrevistoOperacao) {
        this.tempoPrevistoOperacao = tempoPrevistoOperacao;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public String getCodigoProcedencia() {
        return codigoProcedencia;
    }

    public void setCodigoProcedencia(String codigoProcedencia) {
        this.codigoProcedencia = codigoProcedencia;
    }

    public String getCnpjCpfProcedencia() {
        return cnpjCpfProcedencia;
    }

    public void setCnpjCpfProcedencia(String cnpjCpfProcedencia) {
        this.cnpjCpfProcedencia = cnpjCpfProcedencia;
    }

    public String getNomeProcedencia() {
        return nomeProcedencia;
    }

    public void setNomeProcedencia(String nomeProcedencia) {
        this.nomeProcedencia = nomeProcedencia;
    }

    public String getEnderecoProcedencia() {
        return enderecoProcedencia;
    }

    public void setEnderecoProcedencia(String enderecoProcedencia) {
        this.enderecoProcedencia = enderecoProcedencia;
    }

    public String getBairroProcedencia() {
        return bairroProcedencia;
    }

    public void setBairroProcedencia(String bairroProcedencia) {
        this.bairroProcedencia = bairroProcedencia;
    }

    public String getMunicipioProcedencia() {
        return municipioProcedencia;
    }

    public void setMunicipioProcedencia(String municipioProcedencia) {
        this.municipioProcedencia = municipioProcedencia;
    }

    public String getUfProcedencia() {
        return ufProcedencia;
    }

    public void setUfProcedencia(String ufProcedencia) {
        this.ufProcedencia = ufProcedencia;
    }

    public String getCepProcedencia() {
        return cepProcedencia;
    }

    public void setCepProcedencia(String cepProcedencia) {
        this.cepProcedencia = cepProcedencia;
    }

    public String getInscricaoProcedencia() {
        return inscricaoProcedencia;
    }

    public void setInscricaoProcedencia(String inscricaoProcedencia) {
        this.inscricaoProcedencia = inscricaoProcedencia;
    }

    public String getTipoProcedencia() {
        return tipoProcedencia;
    }

    public void setTipoProcedencia(String tipoProcedencia) {
        this.tipoProcedencia = tipoProcedencia;
    }

    public String getTipoPessoaProcedencia() {
        return tipoPessoaProcedencia;
    }

    public void setTipoPessoaProcedencia(String tipoPessoaProcedencia) {
        this.tipoPessoaProcedencia = tipoPessoaProcedencia;
    }

    public String getCodigoTransportadora() {
        return codigoTransportadora;
    }

    public void setCodigoTransportadora(String codigoTransportadora) {
        this.codigoTransportadora = codigoTransportadora;
    }

    public String getCnpjCpfTransportadora() {
        return cnpjCpfTransportadora;
    }

    public void setCnpjCpfTransportadora(String cnpjCpfTransportadora) {
        this.cnpjCpfTransportadora = cnpjCpfTransportadora;
    }

    public String getNomeTransportadora() {
        return nomeTransportadora;
    }

    public void setNomeTransportadora(String nomeTransportadora) {
        this.nomeTransportadora = nomeTransportadora;
    }

    public String getEnderecoTransportadora() {
        return enderecoTransportadora;
    }

    public void setEnderecoTransportadora(String enderecoTransportadora) {
        this.enderecoTransportadora = enderecoTransportadora;
    }

    public String getBairroTransportadora() {
        return bairroTransportadora;
    }

    public void setBairroTransportadora(String bairroTransportadora) {
        this.bairroTransportadora = bairroTransportadora;
    }

    public String getMunicipioTransportadora() {
        return municipioTransportadora;
    }

    public void setMunicipioTransportadora(String municipioTransportadora) {
        this.municipioTransportadora = municipioTransportadora;
    }

    public String getUfTransportadora() {
        return ufTransportadora;
    }

    public void setUfTransportadora(String ufTransportadora) {
        this.ufTransportadora = ufTransportadora;
    }

    public String getCeptransportadora() {
        return ceptransportadora;
    }

    public void setCeptransportadora(String ceptransportadora) {
        this.ceptransportadora = ceptransportadora;
    }

    public String getInscricaoTransportadora() {
        return inscricaoTransportadora;
    }

    public void setInscricaoTransportadora(String inscricaoTransportadora) {
        this.inscricaoTransportadora = inscricaoTransportadora;
    }

    public String getTipoTransportadora() {
        return tipoTransportadora;
    }

    public void setTipoTransportadora(String tipoTransportadora) {
        this.tipoTransportadora = tipoTransportadora;
    }

    public String getTipoPessoaTransportadora() {
        return tipoPessoaTransportadora;
    }

    public void setTipoPessoaTransportadora(String tipoPessoaTransportadora) {
        this.tipoPessoaTransportadora = tipoPessoaTransportadora;
    }

    public String getPlacaVeiculo() {
        return placaVeiculo;
    }

    public void setPlacaVeiculo(String placaVeiculo) {
        this.placaVeiculo = placaVeiculo;
    }

    public String getDescricaoVeiculo() {
        return descricaoVeiculo;
    }

    public void setDescricaoVeiculo(String descricaoVeiculo) {
        this.descricaoVeiculo = descricaoVeiculo;
    }

    public String getNumeroCertificado() {
        return numeroCertificado;
    }

    public void setNumeroCertificado(String numeroCertificado) {
        this.numeroCertificado = numeroCertificado;
    }

    public String getNumeroChassi() {
        return numeroChassi;
    }

    public void setNumeroChassi(String numeroChassi) {
        this.numeroChassi = numeroChassi;
    }

    public String getNumeroRenavam() {
        return numeroRenavam;
    }

    public void setNumeroRenavam(String numeroRenavam) {
        this.numeroRenavam = numeroRenavam;
    }

    public String getMunicipioVeiculo() {
        return municipioVeiculo;
    }

    public void setMunicipioVeiculo(String municipioVeiculo) {
        this.municipioVeiculo = municipioVeiculo;
    }

    public String getUfVeiculo() {
        return ufVeiculo;
    }

    public void setUfVeiculo(String ufVeiculo) {
        this.ufVeiculo = ufVeiculo;
    }

    public String getAnoVeiculo() {
        return anoVeiculo;
    }

    public void setAnoVeiculo(String anoVeiculo) {
        this.anoVeiculo = anoVeiculo;
    }

    public String getIdentificadorVeiculo() {
        return identificadorVeiculo;
    }

    public void setIdentificadorVeiculo(String identificadorVeiculo) {
        this.identificadorVeiculo = identificadorVeiculo;
    }

    public String getTipoVeiculo() {
        return tipoVeiculo;
    }

    public void setTipoVeiculo(String tipoVeiculo) {
        this.tipoVeiculo = tipoVeiculo;
    }

    public String getDescricaoTipoVeiculo() {
        return descricaoTipoVeiculo;
    }

    public void setDescricaoTipoVeiculo(String descricaoTipoVeiculo) {
        this.descricaoTipoVeiculo = descricaoTipoVeiculo;
    }

    public String getMarcaModeloVeiculo() {
        return marcaModeloVeiculo;
    }

    public void setMarcaModeloVeiculo(String marcaModeloVeiculo) {
        this.marcaModeloVeiculo = marcaModeloVeiculo;
    }

    public String getDescricaoMarcaModeloVeiculo() {
        return descricaoMarcaModeloVeiculo;
    }

    public void setDescricaoMarcaModeloVeiculo(String descricaoMarcaModeloVeiculo) {
        this.descricaoMarcaModeloVeiculo = descricaoMarcaModeloVeiculo;
    }

    public String getIdMarcaModeloVeiculo() {
        return idMarcaModeloVeiculo;
    }

    public void setIdMarcaModeloVeiculo(String idMarcaModeloVeiculo) {
        this.idMarcaModeloVeiculo = idMarcaModeloVeiculo;
    }

    public String getCorVeiculo() {
        return corVeiculo;
    }

    public void setCorVeiculo(String corVeiculo) {
        this.corVeiculo = corVeiculo;
    }

    public String getDescricaoCorVeiculo() {
        return descricaoCorVeiculo;
    }

    public void setDescricaoCorVeiculo(String descricaoCorVeiculo) {
        this.descricaoCorVeiculo = descricaoCorVeiculo;
    }

    public String getIdCorVeiculo() {
        return idCorVeiculo;
    }

    public void setIdCorVeiculo(String idCorVeiculo) {
        this.idCorVeiculo = idCorVeiculo;
    }

    public BigDecimal getCodigoCombustivel() {
        return codigoCombustivel;
    }

    public void setCodigoCombustivel(BigDecimal codigoCombustivel) {
        this.codigoCombustivel = codigoCombustivel;
    }

    public String getDescricaoCombustivel() {
        return descricaoCombustivel;
    }

    public void setDescricaoCombustivel(String descricaoCombustivel) {
        this.descricaoCombustivel = descricaoCombustivel;
    }

    public String getIdCombustivel() {
        return idCombustivel;
    }

    public void setIdCombustivel(String idCombustivel) {
        this.idCombustivel = idCombustivel;
    }

    public String getNumeroCracha() {
        return numeroCracha;
    }

    public void setNumeroCracha(String numeroCracha) {
        this.numeroCracha = numeroCracha;
    }

    public String getCpfPessoa() {
        return cpfPessoa;
    }

    public void setCpfPessoa(String cpfPessoa) {
        this.cpfPessoa = cpfPessoa;
    }

    public String getNomePessoa() {
        return nomePessoa;
    }

    public void setNomePessoa(String nomePessoa) {
        this.nomePessoa = nomePessoa;
    }

    public String getRgpessoa() {
        return rgpessoa;
    }

    public void setRgpessoa(String rgpessoa) {
        this.rgpessoa = rgpessoa;
    }

    public String getMunicipioPessoa() {
        return municipioPessoa;
    }

    public void setMunicipioPessoa(String municipioPessoa) {
        this.municipioPessoa = municipioPessoa;
    }

    public String getUfPessoa() {
        return ufPessoa;
    }

    public void setUfPessoa(String ufPessoa) {
        this.ufPessoa = ufPessoa;
    }

    public String getCnhPessoa() {
        return cnhPessoa;
    }

    public void setCnhPessoa(String cnhPessoa) {
        this.cnhPessoa = cnhPessoa;
    }

    public Date getDataVencimentocnh() {
        return dataVencimentocnh;
    }

    public void setDataVencimentocnh(Date dataVencimentocnh) {
        this.dataVencimentocnh = dataVencimentocnh;
    }

    public String getNumeroMopp() {
        return numeroMopp;
    }

    public void setNumeroMopp(String numeroMopp) {
        this.numeroMopp = numeroMopp;
    }

    public Date getDataVencimentoMopp() {
        return dataVencimentoMopp;
    }

    public void setDataVencimentoMopp(Date dataVencimentoMopp) {
        this.dataVencimentoMopp = dataVencimentoMopp;
    }

    public BigDecimal getCodigoRomaneio() {
        return codigoRomaneio;
    }

    public void setCodigoRomaneio(BigDecimal codigoRomaneio) {
        this.codigoRomaneio = codigoRomaneio;
    }

    public BigDecimal getCodigoEstabelecimento() {
        return codigoEstabelecimento;
    }

    public void setCodigoEstabelecimento(BigDecimal codigoEstabelecimento) {
        this.codigoEstabelecimento = codigoEstabelecimento;
    }

    public BigDecimal getTipoPicking() {
        return tipoPicking;
    }

    public void setTipoPicking(BigDecimal tipoPicking) {
        this.tipoPicking = tipoPicking;
    }
}
