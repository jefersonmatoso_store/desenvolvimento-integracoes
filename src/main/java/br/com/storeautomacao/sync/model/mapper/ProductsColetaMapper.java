package br.com.storeautomacao.sync.model.mapper;

import br.com.storeautomacao.sync.model.entity.ProdutoColeta;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ProductsColetaMapper implements RowMapper<ProdutoColeta> {
    @Override
    public ProdutoColeta mapRow(ResultSet rs, int rowNum) throws SQLException {

        ProdutoColeta products = new ProdutoColeta();
        products.setPLU(rs.getString("PLU"));
        products.setAMOUNT(rs.getString("AMOUNT"));

        return products;
    }
}
