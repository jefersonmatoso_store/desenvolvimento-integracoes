package br.com.storeautomacao.sync.model.wms;

import com.thoughtworks.xstream.annotations.XStreamAlias;

public class OrderRelease {

    @XStreamAlias("LgfData")
    private LgfDataOrder LgfData;

    public LgfDataOrder getLgfData ()
    {
        return LgfData;
    }

    public void setLgfData(LgfDataOrder lgfData) {
        LgfData = lgfData;
    }
}
