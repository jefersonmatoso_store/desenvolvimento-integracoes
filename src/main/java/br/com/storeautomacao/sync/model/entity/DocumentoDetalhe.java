package br.com.storeautomacao.sync.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.data.mongodb.core.mapping.Document;

import java.math.BigDecimal;

@Document(collection = "documento_detalhe")
@JsonIgnoreProperties
public class DocumentoDetalhe implements Cloneable{

    private BigDecimal sequenciaIntegracao;
    private BigDecimal sequenciaDocumento;
    private BigDecimal sequenciaDetalhe;

    private String codigoProduto;
    private BigDecimal quantidadeMovimento;
    private BigDecimal valorUnitario;
    private String classeProduto;
    private int sequenciaDetalheNovo;
    private String tipoIntegracao;

    private String tipoUc;


    public DocumentoDetalhe() {

    }

    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }


//    public DocumentoDetalhe(BigDecimal sequenciaIntegracao, BigDecimal sequenciaDocumento, String codigoProduto, BigDecimal quantidadeMovimento, BigDecimal valorUnitario, String classeProduto) {
//        this.sequenciaIntegracao = sequenciaIntegracao;
//        this.sequenciaDocumento = sequenciaDocumento;
//        this.codigoProduto = codigoProduto;
//        this.quantidadeMovimento = quantidadeMovimento;
//        this.valorUnitario = valorUnitario;
//        this.classeProduto = classeProduto;
//    }


    public DocumentoDetalhe(BigDecimal sequenciaIntegracao, BigDecimal sequenciaDocumento, BigDecimal sequenciaDetalhe, String codigoProduto, BigDecimal quantidadeMovimento,
                            BigDecimal valorUnitario, String classeProduto) {
        this.sequenciaIntegracao = sequenciaIntegracao;
        this.sequenciaDocumento = sequenciaDocumento;
        this.sequenciaDetalhe = sequenciaDetalhe;
        this.codigoProduto = codigoProduto;
        this.quantidadeMovimento = quantidadeMovimento;
        this.valorUnitario = valorUnitario;
        this.classeProduto = classeProduto;
    }

    public DocumentoDetalhe(BigDecimal sequenciaIntegracao, BigDecimal sequenciaDocumento, BigDecimal sequenciaDetalhe, String codigoProduto, BigDecimal quantidadeMovimento,
                            BigDecimal valorUnitario, String classeProduto, String tipoUc) {
        this.sequenciaIntegracao = sequenciaIntegracao;
        this.sequenciaDocumento = sequenciaDocumento;
        this.sequenciaDetalhe = sequenciaDetalhe;
        this.codigoProduto = codigoProduto;
        this.quantidadeMovimento = quantidadeMovimento;
        this.valorUnitario = valorUnitario;
        this.classeProduto = classeProduto;

        this.tipoUc = tipoUc;
    }

    public BigDecimal getSequenciaIntegracao() {
        return sequenciaIntegracao;
    }

    public void setSequenciaIntegracao(BigDecimal sequenciaIntegracao) {
        this.sequenciaIntegracao = sequenciaIntegracao;
    }

    public BigDecimal getSequenciaDocumento() {
        return sequenciaDocumento;
    }

    public void setSequenciaDocumento(BigDecimal sequenciaDocumento) {
        this.sequenciaDocumento = sequenciaDocumento;
    }

    public BigDecimal getSequenciaDetalhe() {
        return sequenciaDetalhe;
    }

    public void setSequenciaDetalhe(BigDecimal sequenciaDetalhe) {
        this.sequenciaDetalhe = sequenciaDetalhe;
    }

    public String getCodigoProduto() {
        return codigoProduto;
    }

    public void setCodigoProduto(String codigoProduto) {
        this.codigoProduto = codigoProduto;
    }

    public BigDecimal getQuantidadeMovimento() {
        return quantidadeMovimento;
    }

    public void setQuantidadeMovimento(BigDecimal quantidadeMovimento) {
        this.quantidadeMovimento = quantidadeMovimento;
    }

    public BigDecimal getValorUnitario() {
        return valorUnitario;
    }

    public void setValorUnitario(BigDecimal valorUnitario) {
        this.valorUnitario = valorUnitario;
    }

    public String getClasseProduto() {
        return classeProduto;
    }

    public void setClasseProduto(String classeProduto) {
        this.classeProduto = classeProduto;
    }

    public String getTipoUc() {
        return tipoUc;
    }

    public void setTipoUc(String tipoUc) {
        this.tipoUc = tipoUc;
    }

    public int getSequenciaDetalheNovo() {
        return sequenciaDetalheNovo;
    }

    public void setSequenciaDetalheNovo(int sequenciaDetalheNovo) {
        this.sequenciaDetalheNovo = sequenciaDetalheNovo;
    }

    public String getTipoIntegracao() {
        return tipoIntegracao;
    }

    public void setTipoIntegracao(String tipoIntegracao) {
        this.tipoIntegracao = tipoIntegracao;
    }
}
