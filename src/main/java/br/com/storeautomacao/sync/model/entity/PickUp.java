package br.com.storeautomacao.sync.model.entity;


/**
 * <i>Entity</i> que represente os perfis no sistema.
 * <p>
 * Created by Iomar on 19/09/2017.
 */

public class PickUp {

    private String numeroOrdem;
    private String status;
    private int checkin;

    public String getNumeroOrdem() {
        return numeroOrdem;
    }

    public void setNumeroOrdem(String numeroOrdem) {
        this.numeroOrdem = numeroOrdem;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getCheckin() {
        return checkin;
    }

    public void setCheckin(int checkin) {
        this.checkin = checkin;
    }
}
