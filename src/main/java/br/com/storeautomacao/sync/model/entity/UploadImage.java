package br.com.storeautomacao.sync.model.entity;

import java.util.Collection;

/**
 * Created by alexandre on 20/03/18.
 */
public interface UploadImage {

    /** Retorna uma {@link Collection} com o(s) array(s) da(s) Imagem(ns)*/
    void addBase64Image(String base64Imagem);

    /** Retorna uma {@link Collection} com o(s) array(s) da(s) Imagem(ns)*/
    void addBase64Images(Collection<String> base64Images);

    /** Retorna uma {@link Collection} com o(s) array(s) da(s) Imagem(ns)*/
    Collection<String> getBase64Images();

    /** Retorna o ID do Objeto responsável pela Imagem*/
    String getId();

    /** Armazena a <i>URL</i> da Imagem no Objeto responsável*/
    void addUrlImages(String urlImagem);

    /** Limpa os arrays das Imagens*/
    void cleanBase64Imagens();
}
