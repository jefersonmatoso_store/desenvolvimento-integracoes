package br.com.storeautomacao.sync.model.entity;

import java.math.BigDecimal;
import java.sql.Timestamp;

public class Integracao {

    private BigDecimal sequenciaIntegracao;
    private BigDecimal tipoIntegracao;
    private BigDecimal estadoIntegracao;
    private Timestamp dataLog;
    private Timestamp dataProcessamento;
    private BigDecimal sequenciaRelacionada;
    private String referencia;
    private BigDecimal codigoProvedor;
    private Timestamp dataAgendamento;

    public Integracao(Timestamp dataLog, BigDecimal sequenciaIntegracao, BigDecimal tipointegracao, BigDecimal estadoIntegracao) {
        this.dataLog = dataLog;
        this.sequenciaIntegracao = sequenciaIntegracao;
        this.tipoIntegracao = tipointegracao;
        this.estadoIntegracao = estadoIntegracao;
    }

    public Integracao() {
    }

    public BigDecimal getSequenciaIntegracao() {
        return sequenciaIntegracao;
    }

    public void setSequenciaIntegracao(BigDecimal sequenciaIntegracao) {
        this.sequenciaIntegracao = sequenciaIntegracao;
    }

    public BigDecimal getTipoIntegracao() {
        return tipoIntegracao;
    }

    public void setTipoIntegracao(BigDecimal tipoIntegracao) {
        this.tipoIntegracao = tipoIntegracao;
    }

    public BigDecimal getEstadoIntegracao() {
        return estadoIntegracao;
    }

    public void setEstadoIntegracao(BigDecimal estadoIntegracao) {
        this.estadoIntegracao = estadoIntegracao;
    }

    public Timestamp getDataLog() {
        return dataLog;
    }

    public void setDataLog(Timestamp dataLog) {
        this.dataLog = dataLog;
    }

    public Timestamp getDataProcessamento() {
        return dataProcessamento;
    }

    public void setDataProcessamento(Timestamp dataProcessamento) {
        this.dataProcessamento = dataProcessamento;
    }

    public BigDecimal getSequenciaRelacionada() {
        return sequenciaRelacionada;
    }

    public void setSequenciaRelacionada(BigDecimal sequenciaRelacionada) {
        this.sequenciaRelacionada = sequenciaRelacionada;
    }

    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    public BigDecimal getCodigoProvedor() {
        return codigoProvedor;
    }

    public void setCodigoProvedor(BigDecimal codigoProvedor) {
        this.codigoProvedor = codigoProvedor;
    }

    public Timestamp getDataAgendamento() {
        return dataAgendamento;
    }

    public void setDataAgendamento(Timestamp dataAgendamento) {
        this.dataAgendamento = dataAgendamento;
    }


}
