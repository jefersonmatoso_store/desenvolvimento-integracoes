package br.com.storeautomacao.sync.model.entity;

import java.math.BigDecimal;
import java.util.Date;

public class IntegracaoHistorico {

    private BigDecimal sequenciaIntegracao;
    private BigDecimal sequenciaHistorico;
    private BigDecimal erroIntegracao;
    private Date dataProcessamento;
    private String detalheHistorico;
    private BigDecimal erroOracleCode;
    private String erroOracleMsg;
    private String usuario;
    private Date dataAtualizacao;
    private BigDecimal estadoIntegracao;
    private String justificativa;
    private BigDecimal estadoAnterior;

    public BigDecimal getSequenciaIntegracao() {
        return sequenciaIntegracao;
    }

    public void setSequenciaIntegracao(BigDecimal sequenciaIntegracao) {
        this.sequenciaIntegracao = sequenciaIntegracao;
    }

    public BigDecimal getSequenciaHistorico() {
        return sequenciaHistorico;
    }

    public void setSequenciaHistorico(BigDecimal sequenciaHistorico) {
        this.sequenciaHistorico = sequenciaHistorico;
    }

    public BigDecimal getErroIntegracao() {
        return erroIntegracao;
    }

    public void setErroIntegracao(BigDecimal erroIntegracao) {
        this.erroIntegracao = erroIntegracao;
    }

    public Date getDataProcessamento() {
        return dataProcessamento;
    }

    public void setDataProcessamento(Date dataProcessamento) {
        this.dataProcessamento = dataProcessamento;
    }

    public String getDetalheHistorico() {
        return detalheHistorico;
    }

    public void setDetalheHistorico(String detalheHistorico) {
        this.detalheHistorico = detalheHistorico;
    }

    public BigDecimal getErroOracleCode() {
        return erroOracleCode;
    }

    public void setErroOracleCode(BigDecimal erroOracleCode) {
        this.erroOracleCode = erroOracleCode;
    }

    public String getErroOracleMsg() {
        return erroOracleMsg;
    }

    public void setErroOracleMsg(String erroOracleMsg) {
        this.erroOracleMsg = erroOracleMsg;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public Date getDataAtualizacao() {
        return dataAtualizacao;
    }

    public void setDataAtualizacao(Date dataAtualizacao) {
        this.dataAtualizacao = dataAtualizacao;
    }

    public BigDecimal getEstadoIntegracao() {
        return estadoIntegracao;
    }

    public void setEstadoIntegracao(BigDecimal estadoIntegracao) {
        this.estadoIntegracao = estadoIntegracao;
    }

    public String getJustificativa() {
        return justificativa;
    }

    public void setJustificativa(String justificativa) {
        this.justificativa = justificativa;
    }

    public BigDecimal getEstadoAnterior() {
        return estadoAnterior;
    }

    public void setEstadoAnterior(BigDecimal estadoAnterior) {
        this.estadoAnterior = estadoAnterior;
    }
}
