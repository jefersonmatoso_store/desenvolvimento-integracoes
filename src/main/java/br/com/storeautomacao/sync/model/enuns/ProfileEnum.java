package br.com.storeautomacao.sync.model.enuns;

import br.com.storeautomacao.sync.security.model.Authority;

/**
 * Created by alexandre on 18/05/18.
 */
public enum ProfileEnum {

    ALL_ACCESS(Authority.values()),
    CONCIERGE(Authority.ROLE_READ_ORDER,
            Authority.ROLE_CREATE_VISIT, Authority.ROLE_READ_VISIT, Authority.ROLE_UPDATE_VISIT,
            Authority.ROLE_CREATE_MAINTENANCE, Authority.ROLE_READ_MAINTENANCE, Authority.ROLE_UPDATE_MAINTENANCE),
    TENANT(Authority.ROLE_READ_ORDER,
            Authority.ROLE_READ_VISIT,
            Authority.ROLE_CREATE_MAINTENANCE, Authority.ROLE_READ_MAINTENANCE, Authority.ROLE_UPDATE_MAINTENANCE),
    VISIT(Authority.ROLE_READ_VISIT)
    ;

    private Authority[] authorities;

    private ProfileEnum(Authority... authorities) {
        this.authorities = authorities;
    }

    public boolean hasRole(Authority authority) {
        for (Authority auth : authorities) {
            if (auth.equals(authority)) {
                return true;
            }
        }

        return false;
    }

    public Authority[] getAuthorities() {
        return authorities;
    }

}
