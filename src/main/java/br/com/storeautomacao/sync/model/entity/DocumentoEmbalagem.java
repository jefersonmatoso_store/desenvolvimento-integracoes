package br.com.storeautomacao.sync.model.entity;

import java.math.BigDecimal;
import java.util.Date;

public class DocumentoEmbalagem {

    private BigDecimal sequenciaIntegracao;
    private BigDecimal sequenciaDocumento;
    private BigDecimal sequenciaEmbalagem;
    private BigDecimal codigoEmbalagemExped;
    private String idEmbalagemExped;
    private BigDecimal tipoEmbalagem;
    private String descricaoTipoEmbalagem;
    private BigDecimal largura;
    private BigDecimal comprimento;
    private BigDecimal altura;
    private BigDecimal pesoTara;
    private String codigoProduto;
    private String tipoUc;
    private BigDecimal fatorTipoUc;
    private BigDecimal quantidade;
    private String classeProduto;
    private String loteFabricacao;
    private Date dataFabricacao;
    private Date dataVencimento;
    private String loteGeral;

    public BigDecimal getSequenciaIntegracao() {
        return sequenciaIntegracao;
    }

    public void setSequenciaIntegracao(BigDecimal sequenciaIntegracao) {
        this.sequenciaIntegracao = sequenciaIntegracao;
    }

    public BigDecimal getSequenciaDocumento() {
        return sequenciaDocumento;
    }

    public void setSequenciaDocumento(BigDecimal sequenciaDocumento) {
        this.sequenciaDocumento = sequenciaDocumento;
    }

    public BigDecimal getSequenciaEmbalagem() {
        return sequenciaEmbalagem;
    }

    public void setSequenciaEmbalagem(BigDecimal sequenciaEmbalagem) {
        this.sequenciaEmbalagem = sequenciaEmbalagem;
    }

    public BigDecimal getCodigoEmbalagemExped() {
        return codigoEmbalagemExped;
    }

    public void setCodigoEmbalagemExped(BigDecimal codigoEmbalagemExped) {
        this.codigoEmbalagemExped = codigoEmbalagemExped;
    }

    public String getIdEmbalagemExped() {
        return idEmbalagemExped;
    }

    public void setIdEmbalagemExped(String idEmbalagemExped) {
        this.idEmbalagemExped = idEmbalagemExped;
    }

    public BigDecimal getTipoEmbalagem() {
        return tipoEmbalagem;
    }

    public void setTipoEmbalagem(BigDecimal tipoEmbalagem) {
        this.tipoEmbalagem = tipoEmbalagem;
    }

    public String getDescricaoTipoEmbalagem() {
        return descricaoTipoEmbalagem;
    }

    public void setDescricaoTipoEmbalagem(String descricaoTipoEmbalagem) {
        this.descricaoTipoEmbalagem = descricaoTipoEmbalagem;
    }

    public BigDecimal getLargura() {
        return largura;
    }

    public void setLargura(BigDecimal largura) {
        this.largura = largura;
    }

    public BigDecimal getComprimento() {
        return comprimento;
    }

    public void setComprimento(BigDecimal comprimento) {
        this.comprimento = comprimento;
    }

    public BigDecimal getAltura() {
        return altura;
    }

    public void setAltura(BigDecimal altura) {
        this.altura = altura;
    }

    public BigDecimal getPesoTara() {
        return pesoTara;
    }

    public void setPesoTara(BigDecimal pesoTara) {
        this.pesoTara = pesoTara;
    }

    public String getCodigoProduto() {
        return codigoProduto;
    }

    public void setCodigoProduto(String codigoProduto) {
        this.codigoProduto = codigoProduto;
    }

    public String getTipoUc() {
        return tipoUc;
    }

    public void setTipoUc(String tipoUc) {
        this.tipoUc = tipoUc;
    }

    public BigDecimal getFatorTipoUc() {
        return fatorTipoUc;
    }

    public void setFatorTipoUc(BigDecimal fatorTipoUc) {
        this.fatorTipoUc = fatorTipoUc;
    }

    public BigDecimal getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(BigDecimal quantidade) {
        this.quantidade = quantidade;
    }

    public String getClasseProduto() {
        return classeProduto;
    }

    public void setClasseProduto(String classeProduto) {
        this.classeProduto = classeProduto;
    }

    public String getLoteFabricacao() {
        return loteFabricacao;
    }

    public void setLoteFabricacao(String loteFabricacao) {
        this.loteFabricacao = loteFabricacao;
    }

    public Date getDataFabricacao() {
        return dataFabricacao;
    }

    public void setDataFabricacao(Date dataFabricacao) {
        this.dataFabricacao = dataFabricacao;
    }

    public Date getDataVencimento() {
        return dataVencimento;
    }

    public void setDataVencimento(Date dataVencimento) {
        this.dataVencimento = dataVencimento;
    }

    public String getLoteGeral() {
        return loteGeral;
    }

    public void setLoteGeral(String loteGeral) {
        this.loteGeral = loteGeral;
    }
}
