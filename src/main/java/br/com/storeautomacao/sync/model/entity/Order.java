package br.com.storeautomacao.sync.model.entity;

import br.com.storeautomacao.sync.model.enuns.OrderStatusEnum;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * <i>Entity</i> que represente uma Ocorrência.
 *
 * Created by alexandre on 06/07/17.
 */
@Document(collection = "order")
public class Order implements Serializable, UploadImage {

    @Id
    @JsonProperty("idOrder")
    private String id;

    /** Nome Objeto*/
    private String nameObject;

    /** Entregue por Correio*/
    private String deliveredBy;

    /** Recebido por Manuel*/
    private String receivedBy;

    /** Dono da encomenda */
    private String idUserRecipient;

    //* Dono da encomenda */
    private String nameRecipient;

    /** Device Id do celular do Dono da encomenda */
    private String deviceTokenRecipient;

    /** Observacao */
    private String note;

    /** Lista de urls das fotos da encomenda*/
    private List<String> listPhotoString;

    @Transient
    private String base64Image;

    /** Data chegada encomenda */
    private Date deliveredDate;

    /** Data retirada encomenda */
    private Date takenDate;

    /** Status encomenda */
    private OrderStatusEnum orderStatusEnum;

    @Override
    public void addBase64Image(String base64Imagem) {
        this.setBase64Image(base64Imagem);
    }

    @Override
    public void addBase64Images(Collection<String> base64Images) {
        if (CollectionUtils.isNotEmpty(base64Images)) {
            for (String img : base64Images) {
                this.setBase64Image(img);
                break;
            }
        }
    }

    @Override
    public Collection<String> getBase64Images() {
        List<String> images = new ArrayList<String>();
        images.add(this.getBase64Image());
        return images;
    }

    @Override
    public void addUrlImages(String urlImagem) {
        if (StringUtils.isNotEmpty(urlImagem)) {
            this.addPhotoString(urlImagem);
        }
    }

    @Override
    public void cleanBase64Imagens() {
        this.setBase64Image(null);
    }

    @Override
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNameObject() {
        return nameObject;
    }

    public void setNameObject(String nameObject) {
        this.nameObject = nameObject;
    }

    public String getDeliveredBy() {
        return deliveredBy;
    }

    public void setDeliveredBy(String deliveredBy) {
        this.deliveredBy = deliveredBy;
    }

    public String getReceivedBy() {
        return receivedBy;
    }

    public void setReceivedBy(String receivedBy) {
        this.receivedBy = receivedBy;
    }

    public String getIdUserRecipient() {
        return idUserRecipient;
    }

    public void setIdUserRecipient(String idUserRecipient) {
        this.idUserRecipient = idUserRecipient;
    }

    public String getNameRecipient() {
        return nameRecipient;
    }

    public void setNameRecipient(String nameRecipient) {
        this.nameRecipient = nameRecipient;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getDeviceTokenRecipient() {
        return deviceTokenRecipient;
    }

    public void setDeviceTokenRecipient(String deviceTokenRecipient) {
        this.deviceTokenRecipient = deviceTokenRecipient;
    }

    public List<String> getListPhotoString() {
        return listPhotoString;
    }

    public void setListPhotoString(List<String> listPhotoString) {
        this.listPhotoString = listPhotoString;
    }

    public void addPhotoString(String photoString) {
        this.avoidPhotoStringNull();
        this.getListPhotoString().add(photoString);
    }

    private void avoidPhotoStringNull() {
        if (this.listPhotoString == null) {
            this.listPhotoString = new ArrayList<String>();
        }
    }

    public String getBase64Image() {
        return base64Image;
    }

    public void setBase64Image(String base64Image) {
        this.base64Image = base64Image;
    }

    public Date getDeliveredDate() {
        return deliveredDate;
    }

    public void setDeliveredDate(Date deliveredDate) {
        this.deliveredDate = deliveredDate;
    }

    public Date getTakenDate() {
        return takenDate;
    }

    public void setTakenDate(Date takenDate) {
        this.takenDate = takenDate;
    }

    public OrderStatusEnum getOrderStatusEnum() {
        return orderStatusEnum;
    }

    public void setOrderStatusEnum(OrderStatusEnum orderStatusEnum) {
        this.orderStatusEnum = orderStatusEnum;
    }
}
