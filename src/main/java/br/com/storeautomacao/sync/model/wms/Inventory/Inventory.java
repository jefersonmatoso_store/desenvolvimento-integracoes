package br.com.storeautomacao.sync.model.wms.Inventory;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import java.math.BigDecimal;

@XStreamAlias("inventory")
public class Inventory {

    private String facility_code;
    private String company_code;
    private String item_alternate_code;
    private String item_part_a;
    private String item_part_b;
    private String item_part_c;
    private String item_part_d;
    private String item_part_e;
    private String item_part_f;
    private String pre_pack_code;
    private String pre_pack_ratio;
    private String pre_pack_units;
    private String oblpn_total;
    private String active_total;
    private String active_allocated;
    private String active_available;
    private String iblpn_total;
    private String iblpn_allocated;
    private String iblpn_available;
    private String iblpn_notverified;
    private String total_allocated;
    private String total_available;
    private String total_inventory;
    private String four_wall_summary;
    private String open_order_qty;

    public Inventory(){}

    public String getFacility_code() {
        return facility_code;
    }

    public void setFacility_code(String facility_code) {
        this.facility_code = facility_code;
    }

    public String getCompany_code() {
        return company_code;
    }

    public void setCompany_code(String company_code) {
        this.company_code = company_code;
    }

    public String getItem_alternate_code() {
        return item_alternate_code;
    }

    public void setItem_alternate_code(String item_alternate_code) {
        this.item_alternate_code = item_alternate_code;
    }

    public String getItem_part_a() {
        return item_part_a;
    }

    public void setItem_part_a(String item_part_a) {
        this.item_part_a = item_part_a;
    }

    public String getItem_part_b() {
        return item_part_b;
    }

    public void setItem_part_b(String item_part_b) {
        this.item_part_b = item_part_b;
    }

    public String getItem_part_c() {
        return item_part_c;
    }

    public void setItem_part_c(String item_part_c) {
        this.item_part_c = item_part_c;
    }

    public String getItem_part_d() {
        return item_part_d;
    }

    public void setItem_part_d(String item_part_d) {
        this.item_part_d = item_part_d;
    }

    public String getItem_part_e() {
        return item_part_e;
    }

    public void setItem_part_e(String item_part_e) {
        this.item_part_e = item_part_e;
    }

    public String getItem_part_f() {
        return item_part_f;
    }

    public void setItem_part_f(String item_part_f) {
        this.item_part_f = item_part_f;
    }

    public String getPre_pack_code() {
        return pre_pack_code;
    }

    public void setPre_pack_code(String pre_pack_code) {
        this.pre_pack_code = pre_pack_code;
    }

    public String getPre_pack_ratio() {
        return pre_pack_ratio;
    }

    public void setPre_pack_ratio(String pre_pack_ratio) {
        this.pre_pack_ratio = pre_pack_ratio;
    }

    public String getPre_pack_units() {
        return pre_pack_units;
    }

    public void setPre_pack_units(String pre_pack_units) {
        this.pre_pack_units = pre_pack_units;
    }

    public String getOblpn_total() {
        return oblpn_total;
    }

    public void setOblpn_total(String oblpn_total) {
        this.oblpn_total = oblpn_total;
    }

    public String getActive_total() {
        return active_total;
    }

    public void setActive_total(String active_total) {
        this.active_total = active_total;
    }

    public String getActive_allocated() {
        return active_allocated;
    }

    public void setActive_allocated(String active_allocated) {
        this.active_allocated = active_allocated;
    }

    public String getActive_available() {
        return active_available;
    }

    public void setActive_available(String active_available) {
        this.active_available = active_available;
    }

    public String getIblpn_total() {
        return iblpn_total;
    }

    public void setIblpn_total(String iblpn_total) {
        this.iblpn_total = iblpn_total;
    }

    public String getIblpn_allocated() {
        return iblpn_allocated;
    }

    public void setIblpn_allocated(String iblpn_allocated) {
        this.iblpn_allocated = iblpn_allocated;
    }

    public String getIblpn_available() {
        return iblpn_available;
    }

    public void setIblpn_available(String iblpn_available) {
        this.iblpn_available = iblpn_available;
    }

    public String getIblpn_notverified() {
        return iblpn_notverified;
    }

    public void setIblpn_notverified(String iblpn_notverified) {
        this.iblpn_notverified = iblpn_notverified;
    }

    public String getTotal_allocated() {
        return total_allocated;
    }

    public void setTotal_allocated(String total_allocated) {
        this.total_allocated = total_allocated;
    }

    public String getTotal_available() {
        return total_available;
    }

    public void setTotal_available(String total_available) {
        this.total_available = total_available;
    }

    public String getTotal_inventory() {
        return total_inventory;
    }

    public void setTotal_inventory(String total_inventory) {
        this.total_inventory = total_inventory;
    }

    public String getFour_wall_summary() {
        return four_wall_summary;
    }

    public void setFour_wall_summary(String four_wall_summary) {
        this.four_wall_summary = four_wall_summary;
    }

    public String getOpen_order_qty() {
        return open_order_qty;
    }

    public void setOpen_order_qty(String open_order_qty) {
        this.open_order_qty = open_order_qty;
    }
}
