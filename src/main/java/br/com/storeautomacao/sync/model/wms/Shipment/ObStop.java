package br.com.storeautomacao.sync.model.wms.Shipment;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("ob_stop")
public class ObStop {

    private String line_nbr;
    private String seq_nbr;
    private String stop_shipment_nbr;
    private String stop_bol_nbr;
    private String stop_nbr_of_oblpns;
    private String stop_weight;
    private String stop_volume;
    private String stop_shipping_charge;
    private String shipto_facility_code;
    private String shipto_name;
    private String shipto_addr;
    private String shipto_addr2;
    private String shipto_addr3;
    private String shipto_city;
    private String shipto_state;
    private String shipto_zip;
    private String shipto_country;
    private String shipto_phone_nbr;
    private String shipto_email;
    private String shipto_contact;
    private String dest_facility_code;
    private String cust_name;
    private String cust_addr;
    private String cust_addr2;
    private String cust_addr3;
    private String cust_city;
    private String cust_state;
    private String cust_zip;
    private String cust_country;
    private String cust_phone_nbr;
    private String cust_email;
    private String cust_contact;
    private String cust_nbr;
    private String order_nbr;
    private String ord_date;
    private String exp_date;
    private String req_ship_date;
    private String start_ship_date;
    private String stop_ship_date;
    private String host_allocation_nbr;
    private String customer_po_nbr;
    private String sales_order_nbr;
    private String sales_channel;
    private String dest_dept_nbr;
    private String order_hdr_cust_field_1;
    private String order_hdr_cust_field_2;
    private String order_hdr_cust_field_3;
    private String order_hdr_cust_field_4;
    private String order_hdr_cust_field_5;
    private String order_seq_nbr;
    private String order_dtl_cust_field_1;
    private String order_dtl_cust_field_2;
    private String order_dtl_cust_field_3;
    private String order_dtl_cust_field_4;
    private String order_dtl_cust_field_5;
    private String ob_lpn_nbr;
    private String item_alternate_code;
    private String item_part_a;
    private String item_part_b;
    private String item_part_c;
    private String item_part_d;
    private String item_part_e;
    private String item_part_f;
    private String pre_pack_code;
    private String pre_pack_ratio;
    private String pre_pack_ratio_seq;
    private String pre_pack_total_units;
    private String invn_attr_a;
    private String invn_attr_b;
    private String invn_attr_c;
    private String hazmat;
    private String shipped_uom;
    private String shipped_qty;
    private String pallet_nbr;
    private String dest_company_code;
    private String batch_nbr;
    private String expiry_date;
    private String tracking_nbr;
    private String master_tracking_nbr;
    private String package_type;
    private String payment_method;
    private String carrier_account_nbr;
    private String ship_via_code;
    private String ob_lpn_weight;
    private String ob_lpn_volume;
    private String ob_lpn_shipping_charge;
    private String ob_lpn_type;
    private String ob_lpn_asset_nbr;
    private String ob_lpn_asset_seal_nbr;
    private String serial_nbr;
    private String customer_po_type;
    private String customer_vendor_code;
    private String order_hdr_cust_date_1;
    private String order_hdr_cust_date_2;
    private String order_hdr_cust_date_3;
    private String order_hdr_cust_date_4;
    private String order_hdr_cust_date_5;
    private String order_hdr_cust_number_1;
    private String order_hdr_cust_number_2;
    private String order_hdr_cust_number_3;
    private String order_hdr_cust_number_4;
    private String order_hdr_cust_number_5;
    private String order_hdr_cust_decimal_1;
    private String order_hdr_cust_decimal_2;
    private String order_hdr_cust_decimal_3;
    private String order_hdr_cust_decimal_4;
    private String order_hdr_cust_decimal_5;
    private String order_hdr_cust_short_text_1;
    private String order_hdr_cust_short_text_2;
    private String order_hdr_cust_short_text_3;
    private String order_hdr_cust_short_text_4;
    private String order_hdr_cust_short_text_5;
    private String order_hdr_cust_short_text_6;
    private String order_hdr_cust_short_text_7;
    private String order_hdr_cust_short_text_8;
    private String order_hdr_cust_short_text_9;
    private String order_hdr_cust_short_text_10;
    private String order_hdr_cust_short_text_11;
    private String order_hdr_cust_short_text_12;
    private String order_hdr_cust_long_text_1;
    private String order_hdr_cust_long_text_2;
    private String order_hdr_cust_long_text_3;
    private String order_dtl_cust_date_1;
    private String order_dtl_cust_date_2;
    private String order_dtl_cust_date_3;
    private String order_dtl_cust_date_4;
    private String order_dtl_cust_date_5;
    private String order_dtl_cust_number_1;
    private String order_dtl_cust_number_2;
    private String order_dtl_cust_number_3;
    private String order_dtl_cust_number_4;
    private String order_dtl_cust_number_5;
    private String order_dtl_cust_decimal_1;
    private String order_dtl_cust_decimal_2;
    private String order_dtl_cust_decimal_3;
    private String order_dtl_cust_decimal_4;
    private String order_dtl_cust_decimal_5;
    private String order_dtl_cust_short_text_1;
    private String order_dtl_cust_short_text_2;
    private String order_dtl_cust_short_text_3;
    private String order_dtl_cust_short_text_4;
    private String order_dtl_cust_short_text_5;
    private String order_dtl_cust_short_text_6;
    private String order_dtl_cust_short_text_7;
    private String order_dtl_cust_short_text_8;
    private String order_dtl_cust_short_text_9;
    private String order_dtl_cust_short_text_10;
    private String order_dtl_cust_short_text_11;
    private String order_dtl_cust_short_text_12;
    private String order_dtl_cust_long_text_1;
    private String order_dtl_cust_long_text_2;
    private String order_dtl_cust_long_text_3;
    private String invn_attr_d;
    private String invn_attr_e;
    private String invn_attr_f;
    private String invn_attr_g;
    private String order_type;
    private String rcvd_trailer_nbr;
    private String stop_seal_nbr;
    private String ship_request_line;
    private String ob_lpn_length;
    private String ob_lpn_width;
    private String ob_lpn_height;
    private String invn_attr_h;
    private String invn_attr_i;
    private String invn_attr_j;
    private String invn_attr_k;
    private String invn_attr_l;
    private String invn_attr_m;
    private String invn_attr_n;
    private String invn_attr_o;
    private String erp_source_hdr_ref;
    private String erp_source_system_ref;
    private String group_ref;
    private String erp_source_line_ref;
    private String erp_source_shipment_ref;
    private String erp_fulfillment_line_ref;
    private String sales_order_line_ref;
    private String sales_order_schedule_ref;
    private String tms_order_hdr_ref;
    private String tms_order_dtl_ref;

    public ObStop(){}

    public String getLine_nbr() {
        return line_nbr;
    }

    public void setLine_nbr(String line_nbr) {
        this.line_nbr = line_nbr;
    }

    public String getSeq_nbr() {
        return seq_nbr;
    }

    public void setSeq_nbr(String seq_nbr) {
        this.seq_nbr = seq_nbr;
    }

    public String getStop_shipment_nbr() {
        return stop_shipment_nbr;
    }

    public void setStop_shipment_nbr(String stop_shipment_nbr) {
        this.stop_shipment_nbr = stop_shipment_nbr;
    }

    public String getStop_bol_nbr() {
        return stop_bol_nbr;
    }

    public void setStop_bol_nbr(String stop_bol_nbr) {
        this.stop_bol_nbr = stop_bol_nbr;
    }

    public String getStop_nbr_of_oblpns() {
        return stop_nbr_of_oblpns;
    }

    public void setStop_nbr_of_oblpns(String stop_nbr_of_oblpns) {
        this.stop_nbr_of_oblpns = stop_nbr_of_oblpns;
    }

    public String getStop_weight() {
        return stop_weight;
    }

    public void setStop_weight(String stop_weight) {
        this.stop_weight = stop_weight;
    }

    public String getStop_volume() {
        return stop_volume;
    }

    public void setStop_volume(String stop_volume) {
        this.stop_volume = stop_volume;
    }

    public String getStop_shipping_charge() {
        return stop_shipping_charge;
    }

    public void setStop_shipping_charge(String stop_shipping_charge) {
        this.stop_shipping_charge = stop_shipping_charge;
    }

    public String getShipto_facility_code() {
        return shipto_facility_code;
    }

    public void setShipto_facility_code(String shipto_facility_code) {
        this.shipto_facility_code = shipto_facility_code;
    }

    public String getShipto_name() {
        return shipto_name;
    }

    public void setShipto_name(String shipto_name) {
        this.shipto_name = shipto_name;
    }

    public String getShipto_addr() {
        return shipto_addr;
    }

    public void setShipto_addr(String shipto_addr) {
        this.shipto_addr = shipto_addr;
    }

    public String getShipto_addr2() {
        return shipto_addr2;
    }

    public void setShipto_addr2(String shipto_addr2) {
        this.shipto_addr2 = shipto_addr2;
    }

    public String getShipto_addr3() {
        return shipto_addr3;
    }

    public void setShipto_addr3(String shipto_addr3) {
        this.shipto_addr3 = shipto_addr3;
    }

    public String getShipto_city() {
        return shipto_city;
    }

    public void setShipto_city(String shipto_city) {
        this.shipto_city = shipto_city;
    }

    public String getShipto_state() {
        return shipto_state;
    }

    public void setShipto_state(String shipto_state) {
        this.shipto_state = shipto_state;
    }

    public String getShipto_zip() {
        return shipto_zip;
    }

    public void setShipto_zip(String shipto_zip) {
        this.shipto_zip = shipto_zip;
    }

    public String getShipto_country() {
        return shipto_country;
    }

    public void setShipto_country(String shipto_country) {
        this.shipto_country = shipto_country;
    }

    public String getShipto_phone_nbr() {
        return shipto_phone_nbr;
    }

    public void setShipto_phone_nbr(String shipto_phone_nbr) {
        this.shipto_phone_nbr = shipto_phone_nbr;
    }

    public String getShipto_email() {
        return shipto_email;
    }

    public void setShipto_email(String shipto_email) {
        this.shipto_email = shipto_email;
    }

    public String getShipto_contact() {
        return shipto_contact;
    }

    public void setShipto_contact(String shipto_contact) {
        this.shipto_contact = shipto_contact;
    }

    public String getDest_facility_code() {
        return dest_facility_code;
    }

    public void setDest_facility_code(String dest_facility_code) {
        this.dest_facility_code = dest_facility_code;
    }

    public String getCust_name() {
        return cust_name;
    }

    public void setCust_name(String cust_name) {
        this.cust_name = cust_name;
    }

    public String getCust_addr() {
        return cust_addr;
    }

    public void setCust_addr(String cust_addr) {
        this.cust_addr = cust_addr;
    }

    public String getCust_addr2() {
        return cust_addr2;
    }

    public void setCust_addr2(String cust_addr2) {
        this.cust_addr2 = cust_addr2;
    }

    public String getCust_addr3() {
        return cust_addr3;
    }

    public void setCust_addr3(String cust_addr3) {
        this.cust_addr3 = cust_addr3;
    }

    public String getCust_city() {
        return cust_city;
    }

    public void setCust_city(String cust_city) {
        this.cust_city = cust_city;
    }

    public String getCust_state() {
        return cust_state;
    }

    public void setCust_state(String cust_state) {
        this.cust_state = cust_state;
    }

    public String getCust_zip() {
        return cust_zip;
    }

    public void setCust_zip(String cust_zip) {
        this.cust_zip = cust_zip;
    }

    public String getCust_country() {
        return cust_country;
    }

    public void setCust_country(String cust_country) {
        this.cust_country = cust_country;
    }

    public String getCust_phone_nbr() {
        return cust_phone_nbr;
    }

    public void setCust_phone_nbr(String cust_phone_nbr) {
        this.cust_phone_nbr = cust_phone_nbr;
    }

    public String getCust_email() {
        return cust_email;
    }

    public void setCust_email(String cust_email) {
        this.cust_email = cust_email;
    }

    public String getCust_contact() {
        return cust_contact;
    }

    public void setCust_contact(String cust_contact) {
        this.cust_contact = cust_contact;
    }

    public String getCust_nbr() {
        return cust_nbr;
    }

    public void setCust_nbr(String cust_nbr) {
        this.cust_nbr = cust_nbr;
    }

    public String getOrder_nbr() {
        return order_nbr;
    }

    public void setOrder_nbr(String order_nbr) {
        this.order_nbr = order_nbr;
    }

    public String getOrd_date() {
        return ord_date;
    }

    public void setOrd_date(String ord_date) {
        this.ord_date = ord_date;
    }

    public String getExp_date() {
        return exp_date;
    }

    public void setExp_date(String exp_date) {
        this.exp_date = exp_date;
    }

    public String getReq_ship_date() {
        return req_ship_date;
    }

    public void setReq_ship_date(String req_ship_date) {
        this.req_ship_date = req_ship_date;
    }

    public String getStart_ship_date() {
        return start_ship_date;
    }

    public void setStart_ship_date(String start_ship_date) {
        this.start_ship_date = start_ship_date;
    }

    public String getStop_ship_date() {
        return stop_ship_date;
    }

    public void setStop_ship_date(String stop_ship_date) {
        this.stop_ship_date = stop_ship_date;
    }

    public String getHost_allocation_nbr() {
        return host_allocation_nbr;
    }

    public void setHost_allocation_nbr(String host_allocation_nbr) {
        this.host_allocation_nbr = host_allocation_nbr;
    }

    public String getCustomer_po_nbr() {
        return customer_po_nbr;
    }

    public void setCustomer_po_nbr(String customer_po_nbr) {
        this.customer_po_nbr = customer_po_nbr;
    }

    public String getSales_order_nbr() {
        return sales_order_nbr;
    }

    public void setSales_order_nbr(String sales_order_nbr) {
        this.sales_order_nbr = sales_order_nbr;
    }

    public String getSales_channel() {
        return sales_channel;
    }

    public void setSales_channel(String sales_channel) {
        this.sales_channel = sales_channel;
    }

    public String getDest_dept_nbr() {
        return dest_dept_nbr;
    }

    public void setDest_dept_nbr(String dest_dept_nbr) {
        this.dest_dept_nbr = dest_dept_nbr;
    }

    public String getOrder_hdr_cust_field_1() {
        return order_hdr_cust_field_1;
    }

    public void setOrder_hdr_cust_field_1(String order_hdr_cust_field_1) {
        this.order_hdr_cust_field_1 = order_hdr_cust_field_1;
    }

    public String getOrder_hdr_cust_field_2() {
        return order_hdr_cust_field_2;
    }

    public void setOrder_hdr_cust_field_2(String order_hdr_cust_field_2) {
        this.order_hdr_cust_field_2 = order_hdr_cust_field_2;
    }

    public String getOrder_hdr_cust_field_3() {
        return order_hdr_cust_field_3;
    }

    public void setOrder_hdr_cust_field_3(String order_hdr_cust_field_3) {
        this.order_hdr_cust_field_3 = order_hdr_cust_field_3;
    }

    public String getOrder_hdr_cust_field_4() {
        return order_hdr_cust_field_4;
    }

    public void setOrder_hdr_cust_field_4(String order_hdr_cust_field_4) {
        this.order_hdr_cust_field_4 = order_hdr_cust_field_4;
    }

    public String getOrder_hdr_cust_field_5() {
        return order_hdr_cust_field_5;
    }

    public void setOrder_hdr_cust_field_5(String order_hdr_cust_field_5) {
        this.order_hdr_cust_field_5 = order_hdr_cust_field_5;
    }

    public String getOrder_seq_nbr() {
        return order_seq_nbr;
    }

    public void setOrder_seq_nbr(String order_seq_nbr) {
        this.order_seq_nbr = order_seq_nbr;
    }

    public String getOrder_dtl_cust_field_1() {
        return order_dtl_cust_field_1;
    }

    public void setOrder_dtl_cust_field_1(String order_dtl_cust_field_1) {
        this.order_dtl_cust_field_1 = order_dtl_cust_field_1;
    }

    public String getOrder_dtl_cust_field_2() {
        return order_dtl_cust_field_2;
    }

    public void setOrder_dtl_cust_field_2(String order_dtl_cust_field_2) {
        this.order_dtl_cust_field_2 = order_dtl_cust_field_2;
    }

    public String getOrder_dtl_cust_field_3() {
        return order_dtl_cust_field_3;
    }

    public void setOrder_dtl_cust_field_3(String order_dtl_cust_field_3) {
        this.order_dtl_cust_field_3 = order_dtl_cust_field_3;
    }

    public String getOrder_dtl_cust_field_4() {
        return order_dtl_cust_field_4;
    }

    public void setOrder_dtl_cust_field_4(String order_dtl_cust_field_4) {
        this.order_dtl_cust_field_4 = order_dtl_cust_field_4;
    }

    public String getOrder_dtl_cust_field_5() {
        return order_dtl_cust_field_5;
    }

    public void setOrder_dtl_cust_field_5(String order_dtl_cust_field_5) {
        this.order_dtl_cust_field_5 = order_dtl_cust_field_5;
    }

    public String getOb_lpn_nbr() {
        return ob_lpn_nbr;
    }

    public void setOb_lpn_nbr(String ob_lpn_nbr) {
        this.ob_lpn_nbr = ob_lpn_nbr;
    }

    public String getItem_alternate_code() {
        return item_alternate_code;
    }

    public void setItem_alternate_code(String item_alternate_code) {
        this.item_alternate_code = item_alternate_code;
    }

    public String getItem_part_a() {
        return item_part_a;
    }

    public void setItem_part_a(String item_part_a) {
        this.item_part_a = item_part_a;
    }

    public String getItem_part_b() {
        return item_part_b;
    }

    public void setItem_part_b(String item_part_b) {
        this.item_part_b = item_part_b;
    }

    public String getItem_part_c() {
        return item_part_c;
    }

    public void setItem_part_c(String item_part_c) {
        this.item_part_c = item_part_c;
    }

    public String getItem_part_d() {
        return item_part_d;
    }

    public void setItem_part_d(String item_part_d) {
        this.item_part_d = item_part_d;
    }

    public String getItem_part_e() {
        return item_part_e;
    }

    public void setItem_part_e(String item_part_e) {
        this.item_part_e = item_part_e;
    }

    public String getItem_part_f() {
        return item_part_f;
    }

    public void setItem_part_f(String item_part_f) {
        this.item_part_f = item_part_f;
    }

    public String getPre_pack_code() {
        return pre_pack_code;
    }

    public void setPre_pack_code(String pre_pack_code) {
        this.pre_pack_code = pre_pack_code;
    }

    public String getPre_pack_ratio() {
        return pre_pack_ratio;
    }

    public void setPre_pack_ratio(String pre_pack_ratio) {
        this.pre_pack_ratio = pre_pack_ratio;
    }

    public String getPre_pack_ratio_seq() {
        return pre_pack_ratio_seq;
    }

    public void setPre_pack_ratio_seq(String pre_pack_ratio_seq) {
        this.pre_pack_ratio_seq = pre_pack_ratio_seq;
    }

    public String getPre_pack_total_units() {
        return pre_pack_total_units;
    }

    public void setPre_pack_total_units(String pre_pack_total_units) {
        this.pre_pack_total_units = pre_pack_total_units;
    }

    public String getInvn_attr_a() {
        return invn_attr_a;
    }

    public void setInvn_attr_a(String invn_attr_a) {
        this.invn_attr_a = invn_attr_a;
    }

    public String getInvn_attr_b() {
        return invn_attr_b;
    }

    public void setInvn_attr_b(String invn_attr_b) {
        this.invn_attr_b = invn_attr_b;
    }

    public String getInvn_attr_c() {
        return invn_attr_c;
    }

    public void setInvn_attr_c(String invn_attr_c) {
        this.invn_attr_c = invn_attr_c;
    }

    public String getHazmat() {
        return hazmat;
    }

    public void setHazmat(String hazmat) {
        this.hazmat = hazmat;
    }

    public String getShipped_uom() {
        return shipped_uom;
    }

    public void setShipped_uom(String shipped_uom) {
        this.shipped_uom = shipped_uom;
    }

    public String getShipped_qty() {
        return shipped_qty;
    }

    public void setShipped_qty(String shipped_qty) {
        this.shipped_qty = shipped_qty;
    }

    public String getPallet_nbr() {
        return pallet_nbr;
    }

    public void setPallet_nbr(String pallet_nbr) {
        this.pallet_nbr = pallet_nbr;
    }

    public String getDest_company_code() {
        return dest_company_code;
    }

    public void setDest_company_code(String dest_company_code) {
        this.dest_company_code = dest_company_code;
    }

    public String getBatch_nbr() {
        return batch_nbr;
    }

    public void setBatch_nbr(String batch_nbr) {
        this.batch_nbr = batch_nbr;
    }

    public String getExpiry_date() {
        return expiry_date;
    }

    public void setExpiry_date(String expiry_date) {
        this.expiry_date = expiry_date;
    }

    public String getTracking_nbr() {
        return tracking_nbr;
    }

    public void setTracking_nbr(String tracking_nbr) {
        this.tracking_nbr = tracking_nbr;
    }

    public String getMaster_tracking_nbr() {
        return master_tracking_nbr;
    }

    public void setMaster_tracking_nbr(String master_tracking_nbr) {
        this.master_tracking_nbr = master_tracking_nbr;
    }

    public String getPackage_type() {
        return package_type;
    }

    public void setPackage_type(String package_type) {
        this.package_type = package_type;
    }

    public String getPayment_method() {
        return payment_method;
    }

    public void setPayment_method(String payment_method) {
        this.payment_method = payment_method;
    }

    public String getCarrier_account_nbr() {
        return carrier_account_nbr;
    }

    public void setCarrier_account_nbr(String carrier_account_nbr) {
        this.carrier_account_nbr = carrier_account_nbr;
    }

    public String getShip_via_code() {
        return ship_via_code;
    }

    public void setShip_via_code(String ship_via_code) {
        this.ship_via_code = ship_via_code;
    }

    public String getOb_lpn_weight() {
        return ob_lpn_weight;
    }

    public void setOb_lpn_weight(String ob_lpn_weight) {
        this.ob_lpn_weight = ob_lpn_weight;
    }

    public String getOb_lpn_volume() {
        return ob_lpn_volume;
    }

    public void setOb_lpn_volume(String ob_lpn_volume) {
        this.ob_lpn_volume = ob_lpn_volume;
    }

    public String getOb_lpn_shipping_charge() {
        return ob_lpn_shipping_charge;
    }

    public void setOb_lpn_shipping_charge(String ob_lpn_shipping_charge) {
        this.ob_lpn_shipping_charge = ob_lpn_shipping_charge;
    }

    public String getOb_lpn_type() {
        return ob_lpn_type;
    }

    public void setOb_lpn_type(String ob_lpn_type) {
        this.ob_lpn_type = ob_lpn_type;
    }

    public String getOb_lpn_asset_nbr() {
        return ob_lpn_asset_nbr;
    }

    public void setOb_lpn_asset_nbr(String ob_lpn_asset_nbr) {
        this.ob_lpn_asset_nbr = ob_lpn_asset_nbr;
    }

    public String getOb_lpn_asset_seal_nbr() {
        return ob_lpn_asset_seal_nbr;
    }

    public void setOb_lpn_asset_seal_nbr(String ob_lpn_asset_seal_nbr) {
        this.ob_lpn_asset_seal_nbr = ob_lpn_asset_seal_nbr;
    }

    public String getSerial_nbr() {
        return serial_nbr;
    }

    public void setSerial_nbr(String serial_nbr) {
        this.serial_nbr = serial_nbr;
    }

    public String getCustomer_po_type() {
        return customer_po_type;
    }

    public void setCustomer_po_type(String customer_po_type) {
        this.customer_po_type = customer_po_type;
    }

    public String getCustomer_vendor_code() {
        return customer_vendor_code;
    }

    public void setCustomer_vendor_code(String customer_vendor_code) {
        this.customer_vendor_code = customer_vendor_code;
    }

    public String getOrder_hdr_cust_date_1() {
        return order_hdr_cust_date_1;
    }

    public void setOrder_hdr_cust_date_1(String order_hdr_cust_date_1) {
        this.order_hdr_cust_date_1 = order_hdr_cust_date_1;
    }

    public String getOrder_hdr_cust_date_2() {
        return order_hdr_cust_date_2;
    }

    public void setOrder_hdr_cust_date_2(String order_hdr_cust_date_2) {
        this.order_hdr_cust_date_2 = order_hdr_cust_date_2;
    }

    public String getOrder_hdr_cust_date_3() {
        return order_hdr_cust_date_3;
    }

    public void setOrder_hdr_cust_date_3(String order_hdr_cust_date_3) {
        this.order_hdr_cust_date_3 = order_hdr_cust_date_3;
    }

    public String getOrder_hdr_cust_date_4() {
        return order_hdr_cust_date_4;
    }

    public void setOrder_hdr_cust_date_4(String order_hdr_cust_date_4) {
        this.order_hdr_cust_date_4 = order_hdr_cust_date_4;
    }

    public String getOrder_hdr_cust_date_5() {
        return order_hdr_cust_date_5;
    }

    public void setOrder_hdr_cust_date_5(String order_hdr_cust_date_5) {
        this.order_hdr_cust_date_5 = order_hdr_cust_date_5;
    }

    public String getOrder_hdr_cust_number_1() {
        return order_hdr_cust_number_1;
    }

    public void setOrder_hdr_cust_number_1(String order_hdr_cust_number_1) {
        this.order_hdr_cust_number_1 = order_hdr_cust_number_1;
    }

    public String getOrder_hdr_cust_number_2() {
        return order_hdr_cust_number_2;
    }

    public void setOrder_hdr_cust_number_2(String order_hdr_cust_number_2) {
        this.order_hdr_cust_number_2 = order_hdr_cust_number_2;
    }

    public String getOrder_hdr_cust_number_3() {
        return order_hdr_cust_number_3;
    }

    public void setOrder_hdr_cust_number_3(String order_hdr_cust_number_3) {
        this.order_hdr_cust_number_3 = order_hdr_cust_number_3;
    }

    public String getOrder_hdr_cust_number_4() {
        return order_hdr_cust_number_4;
    }

    public void setOrder_hdr_cust_number_4(String order_hdr_cust_number_4) {
        this.order_hdr_cust_number_4 = order_hdr_cust_number_4;
    }

    public String getOrder_hdr_cust_number_5() {
        return order_hdr_cust_number_5;
    }

    public void setOrder_hdr_cust_number_5(String order_hdr_cust_number_5) {
        this.order_hdr_cust_number_5 = order_hdr_cust_number_5;
    }

    public String getOrder_hdr_cust_decimal_1() {
        return order_hdr_cust_decimal_1;
    }

    public void setOrder_hdr_cust_decimal_1(String order_hdr_cust_decimal_1) {
        this.order_hdr_cust_decimal_1 = order_hdr_cust_decimal_1;
    }

    public String getOrder_hdr_cust_decimal_2() {
        return order_hdr_cust_decimal_2;
    }

    public void setOrder_hdr_cust_decimal_2(String order_hdr_cust_decimal_2) {
        this.order_hdr_cust_decimal_2 = order_hdr_cust_decimal_2;
    }

    public String getOrder_hdr_cust_decimal_3() {
        return order_hdr_cust_decimal_3;
    }

    public void setOrder_hdr_cust_decimal_3(String order_hdr_cust_decimal_3) {
        this.order_hdr_cust_decimal_3 = order_hdr_cust_decimal_3;
    }

    public String getOrder_hdr_cust_decimal_4() {
        return order_hdr_cust_decimal_4;
    }

    public void setOrder_hdr_cust_decimal_4(String order_hdr_cust_decimal_4) {
        this.order_hdr_cust_decimal_4 = order_hdr_cust_decimal_4;
    }

    public String getOrder_hdr_cust_decimal_5() {
        return order_hdr_cust_decimal_5;
    }

    public void setOrder_hdr_cust_decimal_5(String order_hdr_cust_decimal_5) {
        this.order_hdr_cust_decimal_5 = order_hdr_cust_decimal_5;
    }

    public String getOrder_hdr_cust_short_text_1() {
        return order_hdr_cust_short_text_1;
    }

    public void setOrder_hdr_cust_short_text_1(String order_hdr_cust_short_text_1) {
        this.order_hdr_cust_short_text_1 = order_hdr_cust_short_text_1;
    }

    public String getOrder_hdr_cust_short_text_2() {
        return order_hdr_cust_short_text_2;
    }

    public void setOrder_hdr_cust_short_text_2(String order_hdr_cust_short_text_2) {
        this.order_hdr_cust_short_text_2 = order_hdr_cust_short_text_2;
    }

    public String getOrder_hdr_cust_short_text_3() {
        return order_hdr_cust_short_text_3;
    }

    public void setOrder_hdr_cust_short_text_3(String order_hdr_cust_short_text_3) {
        this.order_hdr_cust_short_text_3 = order_hdr_cust_short_text_3;
    }

    public String getOrder_hdr_cust_short_text_4() {
        return order_hdr_cust_short_text_4;
    }

    public void setOrder_hdr_cust_short_text_4(String order_hdr_cust_short_text_4) {
        this.order_hdr_cust_short_text_4 = order_hdr_cust_short_text_4;
    }

    public String getOrder_hdr_cust_short_text_5() {
        return order_hdr_cust_short_text_5;
    }

    public void setOrder_hdr_cust_short_text_5(String order_hdr_cust_short_text_5) {
        this.order_hdr_cust_short_text_5 = order_hdr_cust_short_text_5;
    }

    public String getOrder_hdr_cust_short_text_6() {
        return order_hdr_cust_short_text_6;
    }

    public void setOrder_hdr_cust_short_text_6(String order_hdr_cust_short_text_6) {
        this.order_hdr_cust_short_text_6 = order_hdr_cust_short_text_6;
    }

    public String getOrder_hdr_cust_short_text_7() {
        return order_hdr_cust_short_text_7;
    }

    public void setOrder_hdr_cust_short_text_7(String order_hdr_cust_short_text_7) {
        this.order_hdr_cust_short_text_7 = order_hdr_cust_short_text_7;
    }

    public String getOrder_hdr_cust_short_text_8() {
        return order_hdr_cust_short_text_8;
    }

    public void setOrder_hdr_cust_short_text_8(String order_hdr_cust_short_text_8) {
        this.order_hdr_cust_short_text_8 = order_hdr_cust_short_text_8;
    }

    public String getOrder_hdr_cust_short_text_9() {
        return order_hdr_cust_short_text_9;
    }

    public void setOrder_hdr_cust_short_text_9(String order_hdr_cust_short_text_9) {
        this.order_hdr_cust_short_text_9 = order_hdr_cust_short_text_9;
    }

    public String getOrder_hdr_cust_short_text_10() {
        return order_hdr_cust_short_text_10;
    }

    public void setOrder_hdr_cust_short_text_10(String order_hdr_cust_short_text_10) {
        this.order_hdr_cust_short_text_10 = order_hdr_cust_short_text_10;
    }

    public String getOrder_hdr_cust_short_text_11() {
        return order_hdr_cust_short_text_11;
    }

    public void setOrder_hdr_cust_short_text_11(String order_hdr_cust_short_text_11) {
        this.order_hdr_cust_short_text_11 = order_hdr_cust_short_text_11;
    }

    public String getOrder_hdr_cust_short_text_12() {
        return order_hdr_cust_short_text_12;
    }

    public void setOrder_hdr_cust_short_text_12(String order_hdr_cust_short_text_12) {
        this.order_hdr_cust_short_text_12 = order_hdr_cust_short_text_12;
    }

    public String getOrder_hdr_cust_long_text_1() {
        return order_hdr_cust_long_text_1;
    }

    public void setOrder_hdr_cust_long_text_1(String order_hdr_cust_long_text_1) {
        this.order_hdr_cust_long_text_1 = order_hdr_cust_long_text_1;
    }

    public String getOrder_hdr_cust_long_text_2() {
        return order_hdr_cust_long_text_2;
    }

    public void setOrder_hdr_cust_long_text_2(String order_hdr_cust_long_text_2) {
        this.order_hdr_cust_long_text_2 = order_hdr_cust_long_text_2;
    }

    public String getOrder_hdr_cust_long_text_3() {
        return order_hdr_cust_long_text_3;
    }

    public void setOrder_hdr_cust_long_text_3(String order_hdr_cust_long_text_3) {
        this.order_hdr_cust_long_text_3 = order_hdr_cust_long_text_3;
    }

    public String getOrder_dtl_cust_date_1() {
        return order_dtl_cust_date_1;
    }

    public void setOrder_dtl_cust_date_1(String order_dtl_cust_date_1) {
        this.order_dtl_cust_date_1 = order_dtl_cust_date_1;
    }

    public String getOrder_dtl_cust_date_2() {
        return order_dtl_cust_date_2;
    }

    public void setOrder_dtl_cust_date_2(String order_dtl_cust_date_2) {
        this.order_dtl_cust_date_2 = order_dtl_cust_date_2;
    }

    public String getOrder_dtl_cust_date_3() {
        return order_dtl_cust_date_3;
    }

    public void setOrder_dtl_cust_date_3(String order_dtl_cust_date_3) {
        this.order_dtl_cust_date_3 = order_dtl_cust_date_3;
    }

    public String getOrder_dtl_cust_date_4() {
        return order_dtl_cust_date_4;
    }

    public void setOrder_dtl_cust_date_4(String order_dtl_cust_date_4) {
        this.order_dtl_cust_date_4 = order_dtl_cust_date_4;
    }

    public String getOrder_dtl_cust_date_5() {
        return order_dtl_cust_date_5;
    }

    public void setOrder_dtl_cust_date_5(String order_dtl_cust_date_5) {
        this.order_dtl_cust_date_5 = order_dtl_cust_date_5;
    }

    public String getOrder_dtl_cust_number_1() {
        return order_dtl_cust_number_1;
    }

    public void setOrder_dtl_cust_number_1(String order_dtl_cust_number_1) {
        this.order_dtl_cust_number_1 = order_dtl_cust_number_1;
    }

    public String getOrder_dtl_cust_number_2() {
        return order_dtl_cust_number_2;
    }

    public void setOrder_dtl_cust_number_2(String order_dtl_cust_number_2) {
        this.order_dtl_cust_number_2 = order_dtl_cust_number_2;
    }

    public String getOrder_dtl_cust_number_3() {
        return order_dtl_cust_number_3;
    }

    public void setOrder_dtl_cust_number_3(String order_dtl_cust_number_3) {
        this.order_dtl_cust_number_3 = order_dtl_cust_number_3;
    }

    public String getOrder_dtl_cust_number_4() {
        return order_dtl_cust_number_4;
    }

    public void setOrder_dtl_cust_number_4(String order_dtl_cust_number_4) {
        this.order_dtl_cust_number_4 = order_dtl_cust_number_4;
    }

    public String getOrder_dtl_cust_number_5() {
        return order_dtl_cust_number_5;
    }

    public void setOrder_dtl_cust_number_5(String order_dtl_cust_number_5) {
        this.order_dtl_cust_number_5 = order_dtl_cust_number_5;
    }

    public String getOrder_dtl_cust_decimal_1() {
        return order_dtl_cust_decimal_1;
    }

    public void setOrder_dtl_cust_decimal_1(String order_dtl_cust_decimal_1) {
        this.order_dtl_cust_decimal_1 = order_dtl_cust_decimal_1;
    }

    public String getOrder_dtl_cust_decimal_2() {
        return order_dtl_cust_decimal_2;
    }

    public void setOrder_dtl_cust_decimal_2(String order_dtl_cust_decimal_2) {
        this.order_dtl_cust_decimal_2 = order_dtl_cust_decimal_2;
    }

    public String getOrder_dtl_cust_decimal_3() {
        return order_dtl_cust_decimal_3;
    }

    public void setOrder_dtl_cust_decimal_3(String order_dtl_cust_decimal_3) {
        this.order_dtl_cust_decimal_3 = order_dtl_cust_decimal_3;
    }

    public String getOrder_dtl_cust_decimal_4() {
        return order_dtl_cust_decimal_4;
    }

    public void setOrder_dtl_cust_decimal_4(String order_dtl_cust_decimal_4) {
        this.order_dtl_cust_decimal_4 = order_dtl_cust_decimal_4;
    }

    public String getOrder_dtl_cust_decimal_5() {
        return order_dtl_cust_decimal_5;
    }

    public void setOrder_dtl_cust_decimal_5(String order_dtl_cust_decimal_5) {
        this.order_dtl_cust_decimal_5 = order_dtl_cust_decimal_5;
    }

    public String getOrder_dtl_cust_short_text_1() {
        return order_dtl_cust_short_text_1;
    }

    public void setOrder_dtl_cust_short_text_1(String order_dtl_cust_short_text_1) {
        this.order_dtl_cust_short_text_1 = order_dtl_cust_short_text_1;
    }

    public String getOrder_dtl_cust_short_text_2() {
        return order_dtl_cust_short_text_2;
    }

    public void setOrder_dtl_cust_short_text_2(String order_dtl_cust_short_text_2) {
        this.order_dtl_cust_short_text_2 = order_dtl_cust_short_text_2;
    }

    public String getOrder_dtl_cust_short_text_3() {
        return order_dtl_cust_short_text_3;
    }

    public void setOrder_dtl_cust_short_text_3(String order_dtl_cust_short_text_3) {
        this.order_dtl_cust_short_text_3 = order_dtl_cust_short_text_3;
    }

    public String getOrder_dtl_cust_short_text_4() {
        return order_dtl_cust_short_text_4;
    }

    public void setOrder_dtl_cust_short_text_4(String order_dtl_cust_short_text_4) {
        this.order_dtl_cust_short_text_4 = order_dtl_cust_short_text_4;
    }

    public String getOrder_dtl_cust_short_text_5() {
        return order_dtl_cust_short_text_5;
    }

    public void setOrder_dtl_cust_short_text_5(String order_dtl_cust_short_text_5) {
        this.order_dtl_cust_short_text_5 = order_dtl_cust_short_text_5;
    }

    public String getOrder_dtl_cust_short_text_6() {
        return order_dtl_cust_short_text_6;
    }

    public void setOrder_dtl_cust_short_text_6(String order_dtl_cust_short_text_6) {
        this.order_dtl_cust_short_text_6 = order_dtl_cust_short_text_6;
    }

    public String getOrder_dtl_cust_short_text_7() {
        return order_dtl_cust_short_text_7;
    }

    public void setOrder_dtl_cust_short_text_7(String order_dtl_cust_short_text_7) {
        this.order_dtl_cust_short_text_7 = order_dtl_cust_short_text_7;
    }

    public String getOrder_dtl_cust_short_text_8() {
        return order_dtl_cust_short_text_8;
    }

    public void setOrder_dtl_cust_short_text_8(String order_dtl_cust_short_text_8) {
        this.order_dtl_cust_short_text_8 = order_dtl_cust_short_text_8;
    }

    public String getOrder_dtl_cust_short_text_9() {
        return order_dtl_cust_short_text_9;
    }

    public void setOrder_dtl_cust_short_text_9(String order_dtl_cust_short_text_9) {
        this.order_dtl_cust_short_text_9 = order_dtl_cust_short_text_9;
    }

    public String getOrder_dtl_cust_short_text_10() {
        return order_dtl_cust_short_text_10;
    }

    public void setOrder_dtl_cust_short_text_10(String order_dtl_cust_short_text_10) {
        this.order_dtl_cust_short_text_10 = order_dtl_cust_short_text_10;
    }

    public String getOrder_dtl_cust_short_text_11() {
        return order_dtl_cust_short_text_11;
    }

    public void setOrder_dtl_cust_short_text_11(String order_dtl_cust_short_text_11) {
        this.order_dtl_cust_short_text_11 = order_dtl_cust_short_text_11;
    }

    public String getOrder_dtl_cust_short_text_12() {
        return order_dtl_cust_short_text_12;
    }

    public void setOrder_dtl_cust_short_text_12(String order_dtl_cust_short_text_12) {
        this.order_dtl_cust_short_text_12 = order_dtl_cust_short_text_12;
    }

    public String getOrder_dtl_cust_long_text_1() {
        return order_dtl_cust_long_text_1;
    }

    public void setOrder_dtl_cust_long_text_1(String order_dtl_cust_long_text_1) {
        this.order_dtl_cust_long_text_1 = order_dtl_cust_long_text_1;
    }

    public String getOrder_dtl_cust_long_text_2() {
        return order_dtl_cust_long_text_2;
    }

    public void setOrder_dtl_cust_long_text_2(String order_dtl_cust_long_text_2) {
        this.order_dtl_cust_long_text_2 = order_dtl_cust_long_text_2;
    }

    public String getOrder_dtl_cust_long_text_3() {
        return order_dtl_cust_long_text_3;
    }

    public void setOrder_dtl_cust_long_text_3(String order_dtl_cust_long_text_3) {
        this.order_dtl_cust_long_text_3 = order_dtl_cust_long_text_3;
    }

    public String getInvn_attr_d() {
        return invn_attr_d;
    }

    public void setInvn_attr_d(String invn_attr_d) {
        this.invn_attr_d = invn_attr_d;
    }

    public String getInvn_attr_e() {
        return invn_attr_e;
    }

    public void setInvn_attr_e(String invn_attr_e) {
        this.invn_attr_e = invn_attr_e;
    }

    public String getInvn_attr_f() {
        return invn_attr_f;
    }

    public void setInvn_attr_f(String invn_attr_f) {
        this.invn_attr_f = invn_attr_f;
    }

    public String getInvn_attr_g() {
        return invn_attr_g;
    }

    public void setInvn_attr_g(String invn_attr_g) {
        this.invn_attr_g = invn_attr_g;
    }

    public String getOrder_type() {
        return order_type;
    }

    public void setOrder_type(String order_type) {
        this.order_type = order_type;
    }

    public String getRcvd_trailer_nbr() {
        return rcvd_trailer_nbr;
    }

    public void setRcvd_trailer_nbr(String rcvd_trailer_nbr) {
        this.rcvd_trailer_nbr = rcvd_trailer_nbr;
    }

    public String getStop_seal_nbr() {
        return stop_seal_nbr;
    }

    public void setStop_seal_nbr(String stop_seal_nbr) {
        this.stop_seal_nbr = stop_seal_nbr;
    }

    public String getShip_request_line() {
        return ship_request_line;
    }

    public void setShip_request_line(String ship_request_line) {
        this.ship_request_line = ship_request_line;
    }

    public String getOb_lpn_length() {
        return ob_lpn_length;
    }

    public void setOb_lpn_length(String ob_lpn_length) {
        this.ob_lpn_length = ob_lpn_length;
    }

    public String getOb_lpn_width() {
        return ob_lpn_width;
    }

    public void setOb_lpn_width(String ob_lpn_width) {
        this.ob_lpn_width = ob_lpn_width;
    }

    public String getOb_lpn_height() {
        return ob_lpn_height;
    }

    public void setOb_lpn_height(String ob_lpn_height) {
        this.ob_lpn_height = ob_lpn_height;
    }

    public String getInvn_attr_h() {
        return invn_attr_h;
    }

    public void setInvn_attr_h(String invn_attr_h) {
        this.invn_attr_h = invn_attr_h;
    }

    public String getInvn_attr_i() {
        return invn_attr_i;
    }

    public void setInvn_attr_i(String invn_attr_i) {
        this.invn_attr_i = invn_attr_i;
    }

    public String getInvn_attr_j() {
        return invn_attr_j;
    }

    public void setInvn_attr_j(String invn_attr_j) {
        this.invn_attr_j = invn_attr_j;
    }

    public String getInvn_attr_k() {
        return invn_attr_k;
    }

    public void setInvn_attr_k(String invn_attr_k) {
        this.invn_attr_k = invn_attr_k;
    }

    public String getInvn_attr_l() {
        return invn_attr_l;
    }

    public void setInvn_attr_l(String invn_attr_l) {
        this.invn_attr_l = invn_attr_l;
    }

    public String getInvn_attr_m() {
        return invn_attr_m;
    }

    public void setInvn_attr_m(String invn_attr_m) {
        this.invn_attr_m = invn_attr_m;
    }

    public String getInvn_attr_n() {
        return invn_attr_n;
    }

    public void setInvn_attr_n(String invn_attr_n) {
        this.invn_attr_n = invn_attr_n;
    }

    public String getInvn_attr_o() {
        return invn_attr_o;
    }

    public void setInvn_attr_o(String invn_attr_o) {
        this.invn_attr_o = invn_attr_o;
    }

    public String getErp_source_hdr_ref() {
        return erp_source_hdr_ref;
    }

    public void setErp_source_hdr_ref(String erp_source_hdr_ref) {
        this.erp_source_hdr_ref = erp_source_hdr_ref;
    }

    public String getErp_source_system_ref() {
        return erp_source_system_ref;
    }

    public void setErp_source_system_ref(String erp_source_system_ref) {
        this.erp_source_system_ref = erp_source_system_ref;
    }

    public String getGroup_ref() {
        return group_ref;
    }

    public void setGroup_ref(String group_ref) {
        this.group_ref = group_ref;
    }

    public String getErp_source_line_ref() {
        return erp_source_line_ref;
    }

    public void setErp_source_line_ref(String erp_source_line_ref) {
        this.erp_source_line_ref = erp_source_line_ref;
    }

    public String getErp_source_shipment_ref() {
        return erp_source_shipment_ref;
    }

    public void setErp_source_shipment_ref(String erp_source_shipment_ref) {
        this.erp_source_shipment_ref = erp_source_shipment_ref;
    }

    public String getErp_fulfillment_line_ref() {
        return erp_fulfillment_line_ref;
    }

    public void setErp_fulfillment_line_ref(String erp_fulfillment_line_ref) {
        this.erp_fulfillment_line_ref = erp_fulfillment_line_ref;
    }

    public String getSales_order_line_ref() {
        return sales_order_line_ref;
    }

    public void setSales_order_line_ref(String sales_order_line_ref) {
        this.sales_order_line_ref = sales_order_line_ref;
    }

    public String getSales_order_schedule_ref() {
        return sales_order_schedule_ref;
    }

    public void setSales_order_schedule_ref(String sales_order_schedule_ref) {
        this.sales_order_schedule_ref = sales_order_schedule_ref;
    }

    public String getTms_order_hdr_ref() {
        return tms_order_hdr_ref;
    }

    public void setTms_order_hdr_ref(String tms_order_hdr_ref) {
        this.tms_order_hdr_ref = tms_order_hdr_ref;
    }

    public String getTms_order_dtl_ref() {
        return tms_order_dtl_ref;
    }

    public void setTms_order_dtl_ref(String tms_order_dtl_ref) {
        this.tms_order_dtl_ref = tms_order_dtl_ref;
    }
}
