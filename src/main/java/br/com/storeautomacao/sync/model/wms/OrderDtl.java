package br.com.storeautomacao.sync.model.wms;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("order_dtl")
public class OrderDtl {

    private String cust_field_2;

    private String pre_pack_ratio;

    private String host_ob_lpn_nbr;

    private String cust_field_1;

    private String cust_field_4;

    private String cust_field_3;

    private String cust_field_5;

    private String action_code;

    private String item_barcode;

    private String pre_pack_code;

    private String req_pallet_nbr;

    private String sale_price;

    private String voucher_exp_date;

    private String item_part_e;

    private String invn_attr_n;

    private String item_part_d;

    private String invn_attr_o;

    private String invn_attr_l;

    private String item_part_f;

    private String invn_attr_m;

    private String cust_date_4;

    private String item_part_a;

    private String cust_date_3;

    private String cost;

    private String item_part_c;

    private String cust_date_2;

    private String item_part_b;

    private String cust_date_1;

    private String po_nbr;

    private String erp_source_shipment_ref;

    private String cust_date_5;

    private String ship_request_line;

    private String cust_short_text_12;

    private String cust_short_text_10;

    private String cust_short_text_11;

    private String cust_short_text_6;

    private String cust_short_text_5;

    private String cust_short_text_4;

    private String cust_short_text_3;

    private String vas_activity_code;

    private String cust_decimal_1;

    private String req_cntr_nbr;

    private String cust_short_text_9;

    private String cust_short_text_8;

    private String cust_short_text_7;

    private String cust_decimal_5;

    private String cust_decimal_4;

    private String cust_decimal_3;

    private String cust_decimal_2;

    private String erp_fulfillment_line_ref;

    private String pre_pack_ratio_seq;

    private String batch_nbr;

    private String item_alternate_code;

    private String seq_nbr;

    private String invn_attr_a;

    private String cust_number_1;

    private String invn_attr_b;

    private String voucher_nbr;

    private String invn_attr_c;

    private String uom;

    private String invn_attr_h;

    private String invn_attr_i;

    private String invn_attr_j;

    private String invn_attr_k;

    private String cust_number_4;

    private String cust_short_text_1;

    private String invn_attr_d;

    private String cust_number_5;

    private String cust_short_text_2;

    private String invn_attr_e;

    private String cust_number_2;

    private String invn_attr_f;

    private String cust_number_3;

    private String invn_attr_g;

    private String lock_code;

    private String spl_instr;

    private String dest_facility_attr_b;

    private String ord_qty;

    private String dest_facility_attr_a;

    private String voucher_amount;

    private String dest_facility_attr_c;

    private String pre_pack_total_units;

    private String cust_long_text_3;

    private String sales_order_line_ref;

    private String unit_declared_value;

    private String ref_nbr_1;

    private String serial_nbr;

    private String shipment_nbr;

    private String sales_order_schedule_ref;

    private String erp_source_line_ref;

    private String cust_long_text_1;

    private String cust_long_text_2;

    public String getCust_field_2 ()
    {
        return cust_field_2;
    }

    public void setCust_field_2 (String cust_field_2)
    {
        this.cust_field_2 = cust_field_2;
    }

    public String getPre_pack_ratio ()
    {
        return pre_pack_ratio;
    }

    public void setPre_pack_ratio (String pre_pack_ratio)
    {
        this.pre_pack_ratio = pre_pack_ratio;
    }

    public String getHost_ob_lpn_nbr ()
    {
        return host_ob_lpn_nbr;
    }

    public void setHost_ob_lpn_nbr (String host_ob_lpn_nbr)
    {
        this.host_ob_lpn_nbr = host_ob_lpn_nbr;
    }

    public String getCust_field_1 ()
    {
        return cust_field_1;
    }

    public void setCust_field_1 (String cust_field_1)
    {
        this.cust_field_1 = cust_field_1;
    }

    public String getCust_field_4 ()
    {
        return cust_field_4;
    }

    public void setCust_field_4 (String cust_field_4)
    {
        this.cust_field_4 = cust_field_4;
    }

    public String getCust_field_3 ()
    {
        return cust_field_3;
    }

    public void setCust_field_3 (String cust_field_3)
    {
        this.cust_field_3 = cust_field_3;
    }

    public String getCust_field_5 ()
    {
        return cust_field_5;
    }

    public void setCust_field_5 (String cust_field_5)
    {
        this.cust_field_5 = cust_field_5;
    }

    public String getAction_code ()
    {
        return action_code;
    }

    public void setAction_code (String action_code)
    {
        this.action_code = action_code;
    }

    public String getItem_barcode ()
    {
        return item_barcode;
    }

    public void setItem_barcode (String item_barcode)
    {
        this.item_barcode = item_barcode;
    }

    public String getPre_pack_code ()
    {
        return pre_pack_code;
    }

    public void setPre_pack_code (String pre_pack_code)
    {
        this.pre_pack_code = pre_pack_code;
    }

    public String getReq_pallet_nbr ()
    {
        return req_pallet_nbr;
    }

    public void setReq_pallet_nbr (String req_pallet_nbr)
    {
        this.req_pallet_nbr = req_pallet_nbr;
    }

    public String getSale_price ()
    {
        return sale_price;
    }

    public void setSale_price (String sale_price)
    {
        this.sale_price = sale_price;
    }

    public String getVoucher_exp_date ()
    {
        return voucher_exp_date;
    }

    public void setVoucher_exp_date (String voucher_exp_date)
    {
        this.voucher_exp_date = voucher_exp_date;
    }

    public String getItem_part_e ()
    {
        return item_part_e;
    }

    public void setItem_part_e (String item_part_e)
    {
        this.item_part_e = item_part_e;
    }

    public String getInvn_attr_n ()
    {
        return invn_attr_n;
    }

    public void setInvn_attr_n (String invn_attr_n)
    {
        this.invn_attr_n = invn_attr_n;
    }

    public String getItem_part_d ()
    {
        return item_part_d;
    }

    public void setItem_part_d (String item_part_d)
    {
        this.item_part_d = item_part_d;
    }

    public String getInvn_attr_o ()
    {
        return invn_attr_o;
    }

    public void setInvn_attr_o (String invn_attr_o)
    {
        this.invn_attr_o = invn_attr_o;
    }

    public String getInvn_attr_l ()
    {
        return invn_attr_l;
    }

    public void setInvn_attr_l (String invn_attr_l)
    {
        this.invn_attr_l = invn_attr_l;
    }

    public String getItem_part_f ()
    {
        return item_part_f;
    }

    public void setItem_part_f (String item_part_f)
    {
        this.item_part_f = item_part_f;
    }

    public String getInvn_attr_m ()
    {
        return invn_attr_m;
    }

    public void setInvn_attr_m (String invn_attr_m)
    {
        this.invn_attr_m = invn_attr_m;
    }

    public String getCust_date_4 ()
    {
        return cust_date_4;
    }

    public void setCust_date_4 (String cust_date_4)
    {
        this.cust_date_4 = cust_date_4;
    }

    public String getItem_part_a ()
    {
        return item_part_a;
    }

    public void setItem_part_a (String item_part_a)
    {
        this.item_part_a = item_part_a;
    }

    public String getCust_date_3 ()
    {
        return cust_date_3;
    }

    public void setCust_date_3 (String cust_date_3)
    {
        this.cust_date_3 = cust_date_3;
    }

    public String getCost ()
    {
        return cost;
    }

    public void setCost (String cost)
    {
        this.cost = cost;
    }

    public String getItem_part_c ()
    {
        return item_part_c;
    }

    public void setItem_part_c (String item_part_c)
    {
        this.item_part_c = item_part_c;
    }

    public String getCust_date_2 ()
    {
        return cust_date_2;
    }

    public void setCust_date_2 (String cust_date_2)
    {
        this.cust_date_2 = cust_date_2;
    }

    public String getItem_part_b ()
    {
        return item_part_b;
    }

    public void setItem_part_b (String item_part_b)
    {
        this.item_part_b = item_part_b;
    }

    public String getCust_date_1 ()
    {
        return cust_date_1;
    }

    public void setCust_date_1 (String cust_date_1)
    {
        this.cust_date_1 = cust_date_1;
    }

    public String getPo_nbr ()
    {
        return po_nbr;
    }

    public void setPo_nbr (String po_nbr)
    {
        this.po_nbr = po_nbr;
    }

    public String getErp_source_shipment_ref ()
    {
        return erp_source_shipment_ref;
    }

    public void setErp_source_shipment_ref (String erp_source_shipment_ref)
    {
        this.erp_source_shipment_ref = erp_source_shipment_ref;
    }

    public String getCust_date_5 ()
    {
        return cust_date_5;
    }

    public void setCust_date_5 (String cust_date_5)
    {
        this.cust_date_5 = cust_date_5;
    }

    public String getShip_request_line ()
    {
        return ship_request_line;
    }

    public void setShip_request_line (String ship_request_line)
    {
        this.ship_request_line = ship_request_line;
    }

    public String getCust_short_text_12 ()
    {
        return cust_short_text_12;
    }

    public void setCust_short_text_12 (String cust_short_text_12)
    {
        this.cust_short_text_12 = cust_short_text_12;
    }

    public String getCust_short_text_10 ()
    {
        return cust_short_text_10;
    }

    public void setCust_short_text_10 (String cust_short_text_10)
    {
        this.cust_short_text_10 = cust_short_text_10;
    }

    public String getCust_short_text_11 ()
    {
        return cust_short_text_11;
    }

    public void setCust_short_text_11 (String cust_short_text_11)
    {
        this.cust_short_text_11 = cust_short_text_11;
    }

    public String getCust_short_text_6 ()
    {
        return cust_short_text_6;
    }

    public void setCust_short_text_6 (String cust_short_text_6)
    {
        this.cust_short_text_6 = cust_short_text_6;
    }

    public String getCust_short_text_5 ()
    {
        return cust_short_text_5;
    }

    public void setCust_short_text_5 (String cust_short_text_5)
    {
        this.cust_short_text_5 = cust_short_text_5;
    }

    public String getCust_short_text_4 ()
    {
        return cust_short_text_4;
    }

    public void setCust_short_text_4 (String cust_short_text_4)
    {
        this.cust_short_text_4 = cust_short_text_4;
    }

    public String getCust_short_text_3 ()
    {
        return cust_short_text_3;
    }

    public void setCust_short_text_3 (String cust_short_text_3)
    {
        this.cust_short_text_3 = cust_short_text_3;
    }

    public String getVas_activity_code ()
    {
        return vas_activity_code;
    }

    public void setVas_activity_code (String vas_activity_code)
    {
        this.vas_activity_code = vas_activity_code;
    }

    public String getCust_decimal_1 ()
    {
        return cust_decimal_1;
    }

    public void setCust_decimal_1 (String cust_decimal_1)
    {
        this.cust_decimal_1 = cust_decimal_1;
    }

    public String getReq_cntr_nbr ()
    {
        return req_cntr_nbr;
    }

    public void setReq_cntr_nbr (String req_cntr_nbr)
    {
        this.req_cntr_nbr = req_cntr_nbr;
    }

    public String getCust_short_text_9 ()
    {
        return cust_short_text_9;
    }

    public void setCust_short_text_9 (String cust_short_text_9)
    {
        this.cust_short_text_9 = cust_short_text_9;
    }

    public String getCust_short_text_8 ()
    {
        return cust_short_text_8;
    }

    public void setCust_short_text_8 (String cust_short_text_8)
    {
        this.cust_short_text_8 = cust_short_text_8;
    }

    public String getCust_short_text_7 ()
    {
        return cust_short_text_7;
    }

    public void setCust_short_text_7 (String cust_short_text_7)
    {
        this.cust_short_text_7 = cust_short_text_7;
    }

    public String getCust_decimal_5 ()
    {
        return cust_decimal_5;
    }

    public void setCust_decimal_5 (String cust_decimal_5)
    {
        this.cust_decimal_5 = cust_decimal_5;
    }

    public String getCust_decimal_4 ()
    {
        return cust_decimal_4;
    }

    public void setCust_decimal_4 (String cust_decimal_4)
    {
        this.cust_decimal_4 = cust_decimal_4;
    }

    public String getCust_decimal_3 ()
    {
        return cust_decimal_3;
    }

    public void setCust_decimal_3 (String cust_decimal_3)
    {
        this.cust_decimal_3 = cust_decimal_3;
    }

    public String getCust_decimal_2 ()
    {
        return cust_decimal_2;
    }

    public void setCust_decimal_2 (String cust_decimal_2)
    {
        this.cust_decimal_2 = cust_decimal_2;
    }

    public String getErp_fulfillment_line_ref ()
    {
        return erp_fulfillment_line_ref;
    }

    public void setErp_fulfillment_line_ref (String erp_fulfillment_line_ref)
    {
        this.erp_fulfillment_line_ref = erp_fulfillment_line_ref;
    }

    public String getPre_pack_ratio_seq ()
    {
        return pre_pack_ratio_seq;
    }

    public void setPre_pack_ratio_seq (String pre_pack_ratio_seq)
    {
        this.pre_pack_ratio_seq = pre_pack_ratio_seq;
    }

    public String getBatch_nbr ()
    {
        return batch_nbr;
    }

    public void setBatch_nbr (String batch_nbr)
    {
        this.batch_nbr = batch_nbr;
    }

    public String getItem_alternate_code ()
    {
        return item_alternate_code;
    }

    public void setItem_alternate_code (String item_alternate_code)
    {
        this.item_alternate_code = item_alternate_code;
    }

    public String getSeq_nbr ()
    {
        return seq_nbr;
    }

    public void setSeq_nbr (String seq_nbr)
    {
        this.seq_nbr = seq_nbr;
    }

    public String getInvn_attr_a ()
    {
        return invn_attr_a;
    }

    public void setInvn_attr_a (String invn_attr_a)
    {
        this.invn_attr_a = invn_attr_a;
    }

    public String getCust_number_1 ()
    {
        return cust_number_1;
    }

    public void setCust_number_1 (String cust_number_1)
    {
        this.cust_number_1 = cust_number_1;
    }

    public String getInvn_attr_b ()
    {
        return invn_attr_b;
    }

    public void setInvn_attr_b (String invn_attr_b)
    {
        this.invn_attr_b = invn_attr_b;
    }

    public String getVoucher_nbr ()
    {
        return voucher_nbr;
    }

    public void setVoucher_nbr (String voucher_nbr)
    {
        this.voucher_nbr = voucher_nbr;
    }

    public String getInvn_attr_c ()
    {
        return invn_attr_c;
    }

    public void setInvn_attr_c (String invn_attr_c)
    {
        this.invn_attr_c = invn_attr_c;
    }

    public String getUom ()
    {
        return uom;
    }

    public void setUom (String uom)
    {
        this.uom = uom;
    }

    public String getInvn_attr_h ()
    {
        return invn_attr_h;
    }

    public void setInvn_attr_h (String invn_attr_h)
    {
        this.invn_attr_h = invn_attr_h;
    }

    public String getInvn_attr_i ()
    {
        return invn_attr_i;
    }

    public void setInvn_attr_i (String invn_attr_i)
    {
        this.invn_attr_i = invn_attr_i;
    }

    public String getInvn_attr_j ()
    {
        return invn_attr_j;
    }

    public void setInvn_attr_j (String invn_attr_j)
    {
        this.invn_attr_j = invn_attr_j;
    }

    public String getInvn_attr_k ()
    {
        return invn_attr_k;
    }

    public void setInvn_attr_k (String invn_attr_k)
    {
        this.invn_attr_k = invn_attr_k;
    }

    public String getCust_number_4 ()
    {
        return cust_number_4;
    }

    public void setCust_number_4 (String cust_number_4)
    {
        this.cust_number_4 = cust_number_4;
    }

    public String getCust_short_text_1 ()
    {
        return cust_short_text_1;
    }

    public void setCust_short_text_1 (String cust_short_text_1)
    {
        this.cust_short_text_1 = cust_short_text_1;
    }

    public String getInvn_attr_d ()
    {
        return invn_attr_d;
    }

    public void setInvn_attr_d (String invn_attr_d)
    {
        this.invn_attr_d = invn_attr_d;
    }

    public String getCust_number_5 ()
    {
        return cust_number_5;
    }

    public void setCust_number_5 (String cust_number_5)
    {
        this.cust_number_5 = cust_number_5;
    }

    public String getCust_short_text_2 ()
    {
        return cust_short_text_2;
    }

    public void setCust_short_text_2 (String cust_short_text_2)
    {
        this.cust_short_text_2 = cust_short_text_2;
    }

    public String getInvn_attr_e ()
    {
        return invn_attr_e;
    }

    public void setInvn_attr_e (String invn_attr_e)
    {
        this.invn_attr_e = invn_attr_e;
    }

    public String getCust_number_2 ()
    {
        return cust_number_2;
    }

    public void setCust_number_2 (String cust_number_2)
    {
        this.cust_number_2 = cust_number_2;
    }

    public String getInvn_attr_f ()
    {
        return invn_attr_f;
    }

    public void setInvn_attr_f (String invn_attr_f)
    {
        this.invn_attr_f = invn_attr_f;
    }

    public String getCust_number_3 ()
    {
        return cust_number_3;
    }

    public void setCust_number_3 (String cust_number_3)
    {
        this.cust_number_3 = cust_number_3;
    }

    public String getInvn_attr_g ()
    {
        return invn_attr_g;
    }

    public void setInvn_attr_g (String invn_attr_g)
    {
        this.invn_attr_g = invn_attr_g;
    }

    public String getLock_code ()
    {
        return lock_code;
    }

    public void setLock_code (String lock_code)
    {
        this.lock_code = lock_code;
    }

    public String getSpl_instr ()
    {
        return spl_instr;
    }

    public void setSpl_instr (String spl_instr)
    {
        this.spl_instr = spl_instr;
    }

    public String getDest_facility_attr_b ()
    {
        return dest_facility_attr_b;
    }

    public void setDest_facility_attr_b (String dest_facility_attr_b)
    {
        this.dest_facility_attr_b = dest_facility_attr_b;
    }

    public String getOrd_qty ()
    {
        return ord_qty;
    }

    public void setOrd_qty (String ord_qty)
    {
        this.ord_qty = ord_qty;
    }

    public String getDest_facility_attr_a ()
    {
        return dest_facility_attr_a;
    }

    public void setDest_facility_attr_a (String dest_facility_attr_a)
    {
        this.dest_facility_attr_a = dest_facility_attr_a;
    }

    public String getVoucher_amount ()
    {
        return voucher_amount;
    }

    public void setVoucher_amount (String voucher_amount)
    {
        this.voucher_amount = voucher_amount;
    }

    public String getDest_facility_attr_c ()
    {
        return dest_facility_attr_c;
    }

    public void setDest_facility_attr_c (String dest_facility_attr_c)
    {
        this.dest_facility_attr_c = dest_facility_attr_c;
    }

    public String getPre_pack_total_units ()
    {
        return pre_pack_total_units;
    }

    public void setPre_pack_total_units (String pre_pack_total_units)
    {
        this.pre_pack_total_units = pre_pack_total_units;
    }

    public String getCust_long_text_3 ()
    {
        return cust_long_text_3;
    }

    public void setCust_long_text_3 (String cust_long_text_3)
    {
        this.cust_long_text_3 = cust_long_text_3;
    }

    public String getSales_order_line_ref ()
    {
        return sales_order_line_ref;
    }

    public void setSales_order_line_ref (String sales_order_line_ref)
    {
        this.sales_order_line_ref = sales_order_line_ref;
    }

    public String getUnit_declared_value ()
    {
        return unit_declared_value;
    }

    public void setUnit_declared_value (String unit_declared_value)
    {
        this.unit_declared_value = unit_declared_value;
    }

    public String getRef_nbr_1 ()
    {
        return ref_nbr_1;
    }

    public void setRef_nbr_1 (String ref_nbr_1)
    {
        this.ref_nbr_1 = ref_nbr_1;
    }

    public String getSerial_nbr ()
    {
        return serial_nbr;
    }

    public void setSerial_nbr (String serial_nbr)
    {
        this.serial_nbr = serial_nbr;
    }

    public String getShipment_nbr ()
    {
        return shipment_nbr;
    }

    public void setShipment_nbr (String shipment_nbr)
    {
        this.shipment_nbr = shipment_nbr;
    }

    public String getSales_order_schedule_ref ()
    {
        return sales_order_schedule_ref;
    }

    public void setSales_order_schedule_ref (String sales_order_schedule_ref)
    {
        this.sales_order_schedule_ref = sales_order_schedule_ref;
    }

    public String getErp_source_line_ref ()
    {
        return erp_source_line_ref;
    }

    public void setErp_source_line_ref (String erp_source_line_ref)
    {
        this.erp_source_line_ref = erp_source_line_ref;
    }

    public String getCust_long_text_1 ()
    {
        return cust_long_text_1;
    }

    public void setCust_long_text_1 (String cust_long_text_1)
    {
        this.cust_long_text_1 = cust_long_text_1;
    }

    public String getCust_long_text_2 ()
    {
        return cust_long_text_2;
    }

    public void setCust_long_text_2 (String cust_long_text_2)
    {
        this.cust_long_text_2 = cust_long_text_2;
    }
}
