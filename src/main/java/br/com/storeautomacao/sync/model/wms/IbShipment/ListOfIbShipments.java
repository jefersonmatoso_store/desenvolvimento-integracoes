package br.com.storeautomacao.sync.model.wms.IbShipment;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.ArrayList;
import java.util.List;

@XStreamAlias("ListOfIbShipments")
public class ListOfIbShipments {

    @XStreamImplicit(itemFieldName="ib_shipment")
    private List<IbShipment> ibShipments = new ArrayList<IbShipment>();

    public List<IbShipment> getIbShipments() {
        return ibShipments;
    }

}
