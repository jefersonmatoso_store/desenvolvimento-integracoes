package br.com.storeautomacao.sync.model.entity;

import java.math.BigDecimal;

public class TipoUc {

    private BigDecimal sequenciaIntegracao;
    private BigDecimal tipoIntegracao;
    private String codigoEmpresa;
    private String codigoProduto;
    private String tipoU;
    private BigDecimal sequenciaTipoUc;
    private BigDecimal fatorTipoUc;
    private BigDecimal larguraProduto;
    private BigDecimal comprimentoProduto;
    private BigDecimal alturaProduto;
    private BigDecimal pesoBruto;
    private BigDecimal pesoLiquido;
    private String codigoBarra;
    private String tipoCodigoBarra;
    private String descricaoTipoUc;
    private BigDecimal pesoVariavel;
    private String idTipoUc;
    private String informacaoAdicional1;
    private String informacaoAdicional2;
    private String informacaoAdicional3;

    public TipoUc(){}

    public BigDecimal getSequenciaIntegracao() {
        return sequenciaIntegracao;
    }

    public void setSequenciaIntegracao(BigDecimal sequenciaIntegracao) {
        this.sequenciaIntegracao = sequenciaIntegracao;
    }

    public BigDecimal getTipoIntegracao() {
        return tipoIntegracao;
    }

    public void setTipoIntegracao(BigDecimal tipoIntegracao) {
        this.tipoIntegracao = tipoIntegracao;
    }

    public String getCodigoEmpresa() {
        return codigoEmpresa;
    }

    public void setCodigoEmpresa(String codigoEmpresa) {
        this.codigoEmpresa = codigoEmpresa;
    }

    public String getCodigoProduto() {
        return codigoProduto;
    }

    public void setCodigoProduto(String codigoProduto) {
        this.codigoProduto = codigoProduto;
    }

    public String getTipoU() {
        return tipoU;
    }

    public void setTipoU(String tipoU) {
        this.tipoU = tipoU;
    }

    public BigDecimal getSequenciaTipoUc() {
        return sequenciaTipoUc;
    }

    public void setSequenciaTipoUc(BigDecimal sequenciaTipoUc) {
        this.sequenciaTipoUc = sequenciaTipoUc;
    }

    public BigDecimal getFatorTipoUc() {
        return fatorTipoUc;
    }

    public void setFatorTipoUc(BigDecimal fatorTipoUc) {
        this.fatorTipoUc = fatorTipoUc;
    }

    public BigDecimal getLarguraProduto() {
        return larguraProduto;
    }

    public void setLarguraProduto(BigDecimal larguraProduto) {
        this.larguraProduto = larguraProduto;
    }

    public BigDecimal getComprimentoProduto() {
        return comprimentoProduto;
    }

    public void setComprimentoProduto(BigDecimal comprimentoProduto) {
        this.comprimentoProduto = comprimentoProduto;
    }

    public BigDecimal getAlturaProduto() {
        return alturaProduto;
    }

    public void setAlturaProduto(BigDecimal alturaProduto) {
        this.alturaProduto = alturaProduto;
    }

    public BigDecimal getPesoBruto() {
        return pesoBruto;
    }

    public void setPesoBruto(BigDecimal pesoBruto) {
        this.pesoBruto = pesoBruto;
    }

    public BigDecimal getPesoLiquido() {
        return pesoLiquido;
    }

    public void setPesoLiquido(BigDecimal pesoLiquido) {
        this.pesoLiquido = pesoLiquido;
    }

    public String getCodigoBarra() {
        return codigoBarra;
    }

    public void setCodigoBarra(String codigoBarra) {
        this.codigoBarra = codigoBarra;
    }

    public String getTipoCodigoBarra() {
        return tipoCodigoBarra;
    }

    public void setTipoCodigoBarra(String tipoCodigoBarra) {
        this.tipoCodigoBarra = tipoCodigoBarra;
    }

    public String getDescricaoTipoUc() {
        return descricaoTipoUc;
    }

    public void setDescricaoTipoUc(String descricaoTipoUc) {
        this.descricaoTipoUc = descricaoTipoUc;
    }

    public BigDecimal getPesoVariavel() {
        return pesoVariavel;
    }

    public void setPesoVariavel(BigDecimal pesoVariavel) {
        this.pesoVariavel = pesoVariavel;
    }

    public String getIdTipoUc() {
        return idTipoUc;
    }

    public void setIdTipoUc(String idTipoUc) {
        this.idTipoUc = idTipoUc;
    }

    public String getInformacaoAdicional1() {
        return informacaoAdicional1;
    }

    public void setInformacaoAdicional1(String informacaoAdicional1) {
        this.informacaoAdicional1 = informacaoAdicional1;
    }

    public String getInformacaoAdicional2() {
        return informacaoAdicional2;
    }

    public void setInformacaoAdicional2(String informacaoAdicional2) {
        this.informacaoAdicional2 = informacaoAdicional2;
    }

    public String getInformacaoAdicional3() {
        return informacaoAdicional3;
    }

    public void setInformacaoAdicional3(String informacaoAdicional3) {
        this.informacaoAdicional3 = informacaoAdicional3;
    }
}
