package br.com.storeautomacao.sync.model.wms.IbShipment;

import br.com.storeautomacao.sync.model.wms.Header;
import com.thoughtworks.xstream.annotations.XStreamAlias;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="LgfData")
@XStreamAlias("LgfData")
public class LgfDataShipment {

    private Header Header;

    private ListOfIbShipments ListOfIbShipments;

    public Header getHeader ()
    {
        return Header;
    }

    public void setHeader (Header Header)
    {
        this.Header = Header;
    }

    public ListOfIbShipments getListOfIbShipments() {
        return ListOfIbShipments;
    }

    public void setListOfIbShipments(ListOfIbShipments listOfIbShipments) {
        ListOfIbShipments = listOfIbShipments;
    }
}
