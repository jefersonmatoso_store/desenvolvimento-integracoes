package br.com.storeautomacao.sync.model.wms.PurchaseOrder;

public class PurchaseOrderDtl {

    private String cust_field_2;

    private String cust_field_1;

    private String pre_pack_ratio;

    private String cust_field_4;

    private String cust_field_3;

    private String cust_field_5;

    private String unit_cost;

    private String unit_retail;

    private String action_code;

    private String item_barcode;

    private String internal_misc_n1;

    private String pre_pack_code;

    private String pre_pack_ratio_seq;

    private String invn_attr_a;

    private String item_alternate_code;

    private String seq_nbr;

    private String invn_attr_b;

    private String invn_attr_c;

    private String uom;

    private String invn_attr_h;

    private String invn_attr_i;

    private String invn_attr_j;

    private String invn_attr_k;

    private String vendor_item_code;

    private String invn_attr_d;

    private String invn_attr_e;

    private String invn_attr_f;

    private String invn_attr_g;

    private String line_schedule_nbrs;

    private String ord_qty;

    private String pre_pack_total_units;

    private String item_part_e;

    private String invn_attr_n;

    private String item_part_d;

    private String invn_attr_o;

    private String invn_attr_l;

    private String item_part_f;

    private String invn_attr_m;

    private String item_part_a;

    private String item_part_c;

    private String item_part_b;

    private String internal_misc_a1;

    public String getCust_field_2 ()
    {
        return cust_field_2;
    }

    public void setCust_field_2 (String cust_field_2)
    {
        this.cust_field_2 = cust_field_2;
    }

    public String getCust_field_1 ()
    {
        return cust_field_1;
    }

    public void setCust_field_1 (String cust_field_1)
    {
        this.cust_field_1 = cust_field_1;
    }

    public String getPre_pack_ratio ()
    {
        return pre_pack_ratio;
    }

    public void setPre_pack_ratio (String pre_pack_ratio)
    {
        this.pre_pack_ratio = pre_pack_ratio;
    }

    public String getCust_field_4 ()
    {
        return cust_field_4;
    }

    public void setCust_field_4 (String cust_field_4)
    {
        this.cust_field_4 = cust_field_4;
    }

    public String getCust_field_3 ()
    {
        return cust_field_3;
    }

    public void setCust_field_3 (String cust_field_3)
    {
        this.cust_field_3 = cust_field_3;
    }

    public String getCust_field_5 ()
    {
        return cust_field_5;
    }

    public void setCust_field_5 (String cust_field_5)
    {
        this.cust_field_5 = cust_field_5;
    }

    public String getUnit_cost ()
    {
        return unit_cost;
    }

    public void setUnit_cost (String unit_cost)
    {
        this.unit_cost = unit_cost;
    }

    public String getUnit_retail ()
    {
        return unit_retail;
    }

    public void setUnit_retail (String unit_retail)
    {
        this.unit_retail = unit_retail;
    }

    public String getAction_code ()
    {
        return action_code;
    }

    public void setAction_code (String action_code)
    {
        this.action_code = action_code;
    }

    public String getItem_barcode ()
    {
        return item_barcode;
    }

    public void setItem_barcode (String item_barcode)
    {
        this.item_barcode = item_barcode;
    }

    public String getInternal_misc_n1 ()
    {
        return internal_misc_n1;
    }

    public void setInternal_misc_n1 (String internal_misc_n1)
    {
        this.internal_misc_n1 = internal_misc_n1;
    }

    public String getPre_pack_code ()
    {
        return pre_pack_code;
    }

    public void setPre_pack_code (String pre_pack_code)
    {
        this.pre_pack_code = pre_pack_code;
    }

    public String getPre_pack_ratio_seq ()
    {
        return pre_pack_ratio_seq;
    }

    public void setPre_pack_ratio_seq (String pre_pack_ratio_seq)
    {
        this.pre_pack_ratio_seq = pre_pack_ratio_seq;
    }

    public String getInvn_attr_a ()
    {
        return invn_attr_a;
    }

    public void setInvn_attr_a (String invn_attr_a)
    {
        this.invn_attr_a = invn_attr_a;
    }

    public String getItem_alternate_code ()
    {
        return item_alternate_code;
    }

    public void setItem_alternate_code (String item_alternate_code)
    {
        this.item_alternate_code = item_alternate_code;
    }

    public String getSeq_nbr ()
    {
        return seq_nbr;
    }

    public void setSeq_nbr (String seq_nbr)
    {
        this.seq_nbr = seq_nbr;
    }

    public String getInvn_attr_b ()
    {
        return invn_attr_b;
    }

    public void setInvn_attr_b (String invn_attr_b)
    {
        this.invn_attr_b = invn_attr_b;
    }

    public String getInvn_attr_c ()
    {
        return invn_attr_c;
    }

    public void setInvn_attr_c (String invn_attr_c)
    {
        this.invn_attr_c = invn_attr_c;
    }

    public String getUom ()
    {
        return uom;
    }

    public void setUom (String uom)
    {
        this.uom = uom;
    }

    public String getInvn_attr_h ()
    {
        return invn_attr_h;
    }

    public void setInvn_attr_h (String invn_attr_h)
    {
        this.invn_attr_h = invn_attr_h;
    }

    public String getInvn_attr_i ()
    {
        return invn_attr_i;
    }

    public void setInvn_attr_i (String invn_attr_i)
    {
        this.invn_attr_i = invn_attr_i;
    }

    public String getInvn_attr_j ()
    {
        return invn_attr_j;
    }

    public void setInvn_attr_j (String invn_attr_j)
    {
        this.invn_attr_j = invn_attr_j;
    }

    public String getInvn_attr_k ()
    {
        return invn_attr_k;
    }

    public void setInvn_attr_k (String invn_attr_k)
    {
        this.invn_attr_k = invn_attr_k;
    }

    public String getVendor_item_code ()
    {
        return vendor_item_code;
    }

    public void setVendor_item_code (String vendor_item_code)
    {
        this.vendor_item_code = vendor_item_code;
    }

    public String getInvn_attr_d ()
    {
        return invn_attr_d;
    }

    public void setInvn_attr_d (String invn_attr_d)
    {
        this.invn_attr_d = invn_attr_d;
    }

    public String getInvn_attr_e ()
    {
        return invn_attr_e;
    }

    public void setInvn_attr_e (String invn_attr_e)
    {
        this.invn_attr_e = invn_attr_e;
    }

    public String getInvn_attr_f ()
    {
        return invn_attr_f;
    }

    public void setInvn_attr_f (String invn_attr_f)
    {
        this.invn_attr_f = invn_attr_f;
    }

    public String getInvn_attr_g ()
    {
        return invn_attr_g;
    }

    public void setInvn_attr_g (String invn_attr_g)
    {
        this.invn_attr_g = invn_attr_g;
    }

    public String getLine_schedule_nbrs ()
    {
        return line_schedule_nbrs;
    }

    public void setLine_schedule_nbrs (String line_schedule_nbrs)
    {
        this.line_schedule_nbrs = line_schedule_nbrs;
    }

    public String getOrd_qty ()
    {
        return ord_qty;
    }

    public void setOrd_qty (String ord_qty)
    {
        this.ord_qty = ord_qty;
    }

    public String getPre_pack_total_units ()
    {
        return pre_pack_total_units;
    }

    public void setPre_pack_total_units (String pre_pack_total_units)
    {
        this.pre_pack_total_units = pre_pack_total_units;
    }

    public String getItem_part_e ()
    {
        return item_part_e;
    }

    public void setItem_part_e (String item_part_e)
    {
        this.item_part_e = item_part_e;
    }

    public String getInvn_attr_n ()
    {
        return invn_attr_n;
    }

    public void setInvn_attr_n (String invn_attr_n)
    {
        this.invn_attr_n = invn_attr_n;
    }

    public String getItem_part_d ()
    {
        return item_part_d;
    }

    public void setItem_part_d (String item_part_d)
    {
        this.item_part_d = item_part_d;
    }

    public String getInvn_attr_o ()
    {
        return invn_attr_o;
    }

    public void setInvn_attr_o (String invn_attr_o)
    {
        this.invn_attr_o = invn_attr_o;
    }

    public String getInvn_attr_l ()
    {
        return invn_attr_l;
    }

    public void setInvn_attr_l (String invn_attr_l)
    {
        this.invn_attr_l = invn_attr_l;
    }

    public String getItem_part_f ()
    {
        return item_part_f;
    }

    public void setItem_part_f (String item_part_f)
    {
        this.item_part_f = item_part_f;
    }

    public String getInvn_attr_m ()
    {
        return invn_attr_m;
    }

    public void setInvn_attr_m (String invn_attr_m)
    {
        this.invn_attr_m = invn_attr_m;
    }

    public String getItem_part_a ()
    {
        return item_part_a;
    }

    public void setItem_part_a (String item_part_a)
    {
        this.item_part_a = item_part_a;
    }

    public String getItem_part_c ()
    {
        return item_part_c;
    }

    public void setItem_part_c (String item_part_c)
    {
        this.item_part_c = item_part_c;
    }

    public String getItem_part_b ()
    {
        return item_part_b;
    }

    public void setItem_part_b (String item_part_b)
    {
        this.item_part_b = item_part_b;
    }

    public String getInternal_misc_a1 ()
    {
        return internal_misc_a1;
    }

    public void setInternal_misc_a1 (String internal_misc_a1)
    {
        this.internal_misc_a1 = internal_misc_a1;
    }

}
