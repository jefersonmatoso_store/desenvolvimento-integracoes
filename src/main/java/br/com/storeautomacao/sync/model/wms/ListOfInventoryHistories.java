package br.com.storeautomacao.sync.model.wms;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("ListOfInventoryHistories")
public class ListOfInventoryHistories
{

    @XStreamImplicit
    private List<Inventory_history> inventory_history;


    public List<Inventory_history> getInventory_history() {
        return inventory_history;
    }

    public void setInventory_history(List<Inventory_history> inventory_history) {
        this.inventory_history = inventory_history;
    }
}
