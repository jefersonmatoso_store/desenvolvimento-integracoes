package br.com.storeautomacao.sync.model.entity;

public class TemperaturaData {
    private float id;
    private float abstraction_id;
    private String data;
    private String timestamp;
    private String deleted_at = null;
    private String created_at;
    private String updated_at;
    private String user_id = null;
    private float item_id_ref;


    public float getId() {
        return id;
    }

    public void setId(float id) {
        this.id = id;
    }

    public float getAbstraction_id() {
        return abstraction_id;
    }

    public void setAbstraction_id(float abstraction_id) {
        this.abstraction_id = abstraction_id;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getDeleted_at() {
        return deleted_at;
    }

    public void setDeleted_at(String deleted_at) {
        this.deleted_at = deleted_at;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public float getItem_id_ref() {
        return item_id_ref;
    }

    public void setItem_id_ref(float item_id_ref) {
        this.item_id_ref = item_id_ref;
    }
}
