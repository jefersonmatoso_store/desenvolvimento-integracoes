package br.com.storeautomacao.sync.model.wms.Shipment;

import br.com.storeautomacao.sync.model.wms.Header;
import br.com.storeautomacao.sync.model.wms.ListOfItems;
import com.thoughtworks.xstream.annotations.XStreamAlias;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "LgfData")
@XStreamAlias("LgfData")
public class LgfDataShipped
{
    private Header Header;

    private ListOfShippedLoads ListOfShippedLoads;

    public Header getHeader ()
    {
        return Header;
    }

    public void setHeader (Header Header)
    {
        this.Header = Header;
    }

    public ListOfShippedLoads getListOfShippedLoads() {
        return ListOfShippedLoads;
    }

    public void setListOfShippedLoads(ListOfShippedLoads listOfShippedLoads) {
        ListOfShippedLoads = listOfShippedLoads;
    }
}
