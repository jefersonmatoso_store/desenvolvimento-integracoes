package br.com.storeautomacao.sync.model.entity;

public class ListOrderItem {

    private int id;
    private int quantity;
    private LocationSource locationSource;
    private UnitMeasure unitMeasure;
    private CustomerOrderItem customerOrderItem;
    private Item item;
    private LocationDestiny locationDestiny;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public LocationSource getLocationSource() {
        return locationSource;
    }

    public void setLocationSource(LocationSource locationSource) {
        this.locationSource = locationSource;
    }

    public UnitMeasure getUnitMeasure() {
        return unitMeasure;
    }

    public void setUnitMeasure(UnitMeasure unitMeasure) {
        this.unitMeasure = unitMeasure;
    }

    public CustomerOrderItem getCustomerOrderItem() {
        return customerOrderItem;
    }

    public void setCustomerOrderItem(CustomerOrderItem customerOrderItem) {
        this.customerOrderItem = customerOrderItem;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public LocationDestiny getLocationDestiny() {
        return locationDestiny;
    }

    public void setLocationDestiny(LocationDestiny locationDestiny) {
        this.locationDestiny = locationDestiny;
    }
}
