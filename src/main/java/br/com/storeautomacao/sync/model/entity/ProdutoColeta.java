package br.com.storeautomacao.sync.model.entity;


/**
 * <i>Entity</i> que represente os perfis no sistema.
 * <p>
 * Created by Iomar on 19/09/2017.
 */

public class ProdutoColeta {

    private String PLU;
    private String AMOUNT;

    public String getPLU() {
        return PLU;
    }

    public void setPLU(String PLU) {
        this.PLU = PLU;
    }

    public String getAMOUNT() {
        return AMOUNT;
    }

    public void setAMOUNT(String AMOUNT) {
        this.AMOUNT = AMOUNT;
    }
}
