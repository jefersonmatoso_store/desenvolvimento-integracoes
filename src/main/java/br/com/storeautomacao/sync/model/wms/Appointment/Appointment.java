package br.com.storeautomacao.sync.model.wms.Appointment;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("appointment")
public class Appointment {

    private String facility_code;
    private String company_code;
    private String appt_nbr;
    private String load_nbr;
    private String dock_type;
    private String action_code;
    private String preferred_dock_nbr;
    private String planned_start_ts;
    private String duration;
    private String estimated_units;
    private String carrier_info;
    private String trailer_nbr;
    private String load_type;

    public String getFacility_code() {
        return facility_code;
    }

    public void setFacility_code(String facility_code) {
        this.facility_code = facility_code;
    }

    public String getCompany_code() {
        return company_code;
    }

    public void setCompany_code(String company_code) {
        this.company_code = company_code;
    }

    public String getAppt_nbr() {
        return appt_nbr;
    }

    public void setAppt_nbr(String appt_nbr) {
        this.appt_nbr = appt_nbr;
    }

    public String getLoad_nbr() {
        return load_nbr;
    }

    public void setLoad_nbr(String load_nbr) {
        this.load_nbr = load_nbr;
    }

    public String getDock_type() {
        return dock_type;
    }

    public void setDock_type(String dock_type) {
        this.dock_type = dock_type;
    }

    public String getAction_code() {
        return action_code;
    }

    public void setAction_code(String action_code) {
        this.action_code = action_code;
    }

    public String getPreferred_dock_nbr() {
        return preferred_dock_nbr;
    }

    public void setPreferred_dock_nbr(String preferred_dock_nbr) {
        this.preferred_dock_nbr = preferred_dock_nbr;
    }

    public String getPlanned_start_ts() {
        return planned_start_ts;
    }

    public void setPlanned_start_ts(String planned_start_ts) {
        this.planned_start_ts = planned_start_ts;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getEstimated_units() {
        return estimated_units;
    }

    public void setEstimated_units(String estimated_units) {
        this.estimated_units = estimated_units;
    }

    public String getCarrier_info() {
        return carrier_info;
    }

    public void setCarrier_info(String carrier_info) {
        this.carrier_info = carrier_info;
    }

    public String getTrailer_nbr() {
        return trailer_nbr;
    }

    public void setTrailer_nbr(String trailer_nbr) {
        this.trailer_nbr = trailer_nbr;
    }

    public String getLoad_type() {
        return load_type;
    }

    public void setLoad_type(String load_type) {
        this.load_type = load_type;
    }
}
