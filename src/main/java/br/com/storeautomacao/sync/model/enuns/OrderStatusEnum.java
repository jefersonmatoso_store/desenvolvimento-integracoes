package br.com.storeautomacao.sync.model.enuns;

/**
 * Created by iomar on 23/03/18.
 */
public enum OrderStatusEnum {

    RETIRADO,     // Retirado
    DISPONIVEL,   // Disponível para retirar

}
