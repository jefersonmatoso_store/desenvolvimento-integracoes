package br.com.storeautomacao.sync.model.wms.IbShipment;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("ib_shipment_dtl")
public class IbShipmentDtl {

    private String seq_nbr;
    private String action_code;
    private String lpn_nbr;
    private String lpn_weight;
    private String lpn_volume;
    private String item_alternate_code;
    private String item_part_a;
    private String item_part_b;
    private String item_part_c;
    private String item_part_d;
    private String item_part_e;
    private String item_part_f;
    private String pre_pack_code;
    private String pre_pack_ratio;
    private String pre_pack_total_units;
    private String invn_attr_a;
    private String invn_attr_b;
    private String invn_attr_c;
    private String shipped_qty;
    private String priority_date;
    private String po_nbr;
    private String pallet_nbr;
    private String putaway_type;
    private String expiry_date;
    private String batch_nbr;
    private String recv_xdock_facility_code;
    private String cust_field_1;
    private String cust_field_2;
    private String cust_field_3;
    private String cust_field_4;
    private String cust_field_5;
    private String lpn_is_physical_pallet_flg;
    private String po_seq_nbr;
    private String pre_pack_ratio_seq;
    private String lpn_lock_code;
    private String item_barcode;
    private String uom;
    private String lpn_length;
    private String lpn_width;
    private String lpn_height;
    private String dtl_rcv_flg;
    private String invn_attr_d;
    private String invn_attr_e;
    private String invn_attr_f;
    private String invn_attr_g;
    private String receipt_advice_line;
    private String invn_attr_h;
    private String invn_attr_i;
    private String invn_attr_j;
    private String invn_attr_k;
    private String invn_attr_l;
    private String invn_attr_m;
    private String invn_attr_n;
    private String invn_attr_o;


    // Getter Methods

    public String getSeq_nbr() {
        return seq_nbr;
    }

    public String getAction_code() {
        return action_code;
    }

    public String getLpn_nbr() {
        return lpn_nbr;
    }

    public String getLpn_weight() {
        return lpn_weight;
    }

    public String getLpn_volume() {
        return lpn_volume;
    }

    public String getItem_alternate_code() {
        return item_alternate_code;
    }

    public String getItem_part_a() {
        return item_part_a;
    }

    public String getItem_part_b() {
        return item_part_b;
    }

    public String getItem_part_c() {
        return item_part_c;
    }

    public String getItem_part_d() {
        return item_part_d;
    }

    public String getItem_part_e() {
        return item_part_e;
    }

    public String getItem_part_f() {
        return item_part_f;
    }

    public String getPre_pack_code() {
        return pre_pack_code;
    }

    public String getPre_pack_ratio() {
        return pre_pack_ratio;
    }

    public String getPre_pack_total_units() {
        return pre_pack_total_units;
    }

    public String getInvn_attr_a() {
        return invn_attr_a;
    }

    public String getInvn_attr_b() {
        return invn_attr_b;
    }

    public String getInvn_attr_c() {
        return invn_attr_c;
    }

    public String getShipped_qty() {
        return shipped_qty;
    }

    public String getPriority_date() {
        return priority_date;
    }

    public String getPo_nbr() {
        return po_nbr;
    }

    public String getPallet_nbr() {
        return pallet_nbr;
    }

    public String getPutaway_type() {
        return putaway_type;
    }

    public String getExpiry_date() {
        return expiry_date;
    }

    public String getBatch_nbr() {
        return batch_nbr;
    }

    public String getRecv_xdock_facility_code() {
        return recv_xdock_facility_code;
    }

    public String getCust_field_1() {
        return cust_field_1;
    }

    public String getCust_field_2() {
        return cust_field_2;
    }

    public String getCust_field_3() {
        return cust_field_3;
    }

    public String getCust_field_4() {
        return cust_field_4;
    }

    public String getCust_field_5() {
        return cust_field_5;
    }

    public String getLpn_is_physical_pallet_flg() {
        return lpn_is_physical_pallet_flg;
    }

    public String getPo_seq_nbr() {
        return po_seq_nbr;
    }

    public String getPre_pack_ratio_seq() {
        return pre_pack_ratio_seq;
    }

    public String getLpn_lock_code() {
        return lpn_lock_code;
    }

    public String getItem_barcode() {
        return item_barcode;
    }

    public String getUom() {
        return uom;
    }

    public String getLpn_length() {
        return lpn_length;
    }

    public String getLpn_width() {
        return lpn_width;
    }

    public String getLpn_height() {
        return lpn_height;
    }

    public String getDtl_rcv_flg() {
        return dtl_rcv_flg;
    }

    public String getInvn_attr_d() {
        return invn_attr_d;
    }

    public String getInvn_attr_e() {
        return invn_attr_e;
    }

    public String getInvn_attr_f() {
        return invn_attr_f;
    }

    public String getInvn_attr_g() {
        return invn_attr_g;
    }

    public String getReceipt_advice_line() {
        return receipt_advice_line;
    }

    public String getInvn_attr_h() {
        return invn_attr_h;
    }

    public String getInvn_attr_i() {
        return invn_attr_i;
    }

    public String getInvn_attr_j() {
        return invn_attr_j;
    }

    public String getInvn_attr_k() {
        return invn_attr_k;
    }

    public String getInvn_attr_l() {
        return invn_attr_l;
    }

    public String getInvn_attr_m() {
        return invn_attr_m;
    }

    public String getInvn_attr_n() {
        return invn_attr_n;
    }

    public String getInvn_attr_o() {
        return invn_attr_o;
    }

    // Setter Methods

    public void setSeq_nbr(String seq_nbr) {
        this.seq_nbr = seq_nbr;
    }

    public void setAction_code(String action_code) {
        this.action_code = action_code;
    }

    public void setLpn_nbr(String lpn_nbr) {
        this.lpn_nbr = lpn_nbr;
    }

    public void setLpn_weight(String lpn_weight) {
        this.lpn_weight = lpn_weight;
    }

    public void setLpn_volume(String lpn_volume) {
        this.lpn_volume = lpn_volume;
    }

    public void setItem_alternate_code(String item_alternate_code) {
        this.item_alternate_code = item_alternate_code;
    }

    public void setItem_part_a(String item_part_a) {
        this.item_part_a = item_part_a;
    }

    public void setItem_part_b(String item_part_b) {
        this.item_part_b = item_part_b;
    }

    public void setItem_part_c(String item_part_c) {
        this.item_part_c = item_part_c;
    }

    public void setItem_part_d(String item_part_d) {
        this.item_part_d = item_part_d;
    }

    public void setItem_part_e(String item_part_e) {
        this.item_part_e = item_part_e;
    }

    public void setItem_part_f(String item_part_f) {
        this.item_part_f = item_part_f;
    }

    public void setPre_pack_code(String pre_pack_code) {
        this.pre_pack_code = pre_pack_code;
    }

    public void setPre_pack_ratio(String pre_pack_ratio) {
        this.pre_pack_ratio = pre_pack_ratio;
    }

    public void setPre_pack_total_units(String pre_pack_total_units) {
        this.pre_pack_total_units = pre_pack_total_units;
    }

    public void setInvn_attr_a(String invn_attr_a) {
        this.invn_attr_a = invn_attr_a;
    }

    public void setInvn_attr_b(String invn_attr_b) {
        this.invn_attr_b = invn_attr_b;
    }

    public void setInvn_attr_c(String invn_attr_c) {
        this.invn_attr_c = invn_attr_c;
    }

    public void setShipped_qty(String shipped_qty) {
        this.shipped_qty = shipped_qty;
    }

    public void setPriority_date(String priority_date) {
        this.priority_date = priority_date;
    }

    public void setPo_nbr(String po_nbr) {
        this.po_nbr = po_nbr;
    }

    public void setPallet_nbr(String pallet_nbr) {
        this.pallet_nbr = pallet_nbr;
    }

    public void setPutaway_type(String putaway_type) {
        this.putaway_type = putaway_type;
    }

    public void setExpiry_date(String expiry_date) {
        this.expiry_date = expiry_date;
    }

    public void setBatch_nbr(String batch_nbr) {
        this.batch_nbr = batch_nbr;
    }

    public void setRecv_xdock_facility_code(String recv_xdock_facility_code) {
        this.recv_xdock_facility_code = recv_xdock_facility_code;
    }

    public void setCust_field_1(String cust_field_1) {
        this.cust_field_1 = cust_field_1;
    }

    public void setCust_field_2(String cust_field_2) {
        this.cust_field_2 = cust_field_2;
    }

    public void setCust_field_3(String cust_field_3) {
        this.cust_field_3 = cust_field_3;
    }

    public void setCust_field_4(String cust_field_4) {
        this.cust_field_4 = cust_field_4;
    }

    public void setCust_field_5(String cust_field_5) {
        this.cust_field_5 = cust_field_5;
    }

    public void setLpn_is_physical_pallet_flg(String lpn_is_physical_pallet_flg) {
        this.lpn_is_physical_pallet_flg = lpn_is_physical_pallet_flg;
    }

    public void setPo_seq_nbr(String po_seq_nbr) {
        this.po_seq_nbr = po_seq_nbr;
    }

    public void setPre_pack_ratio_seq(String pre_pack_ratio_seq) {
        this.pre_pack_ratio_seq = pre_pack_ratio_seq;
    }

    public void setLpn_lock_code(String lpn_lock_code) {
        this.lpn_lock_code = lpn_lock_code;
    }

    public void setItem_barcode(String item_barcode) {
        this.item_barcode = item_barcode;
    }

    public void setUom(String uom) {
        this.uom = uom;
    }

    public void setLpn_length(String lpn_length) {
        this.lpn_length = lpn_length;
    }

    public void setLpn_width(String lpn_width) {
        this.lpn_width = lpn_width;
    }

    public void setLpn_height(String lpn_height) {
        this.lpn_height = lpn_height;
    }

    public void setDtl_rcv_flg(String dtl_rcv_flg) {
        this.dtl_rcv_flg = dtl_rcv_flg;
    }

    public void setInvn_attr_d(String invn_attr_d) {
        this.invn_attr_d = invn_attr_d;
    }

    public void setInvn_attr_e(String invn_attr_e) {
        this.invn_attr_e = invn_attr_e;
    }

    public void setInvn_attr_f(String invn_attr_f) {
        this.invn_attr_f = invn_attr_f;
    }

    public void setInvn_attr_g(String invn_attr_g) {
        this.invn_attr_g = invn_attr_g;
    }

    public void setReceipt_advice_line(String receipt_advice_line) {
        this.receipt_advice_line = receipt_advice_line;
    }

    public void setInvn_attr_h(String invn_attr_h) {
        this.invn_attr_h = invn_attr_h;
    }

    public void setInvn_attr_i(String invn_attr_i) {
        this.invn_attr_i = invn_attr_i;
    }

    public void setInvn_attr_j(String invn_attr_j) {
        this.invn_attr_j = invn_attr_j;
    }

    public void setInvn_attr_k(String invn_attr_k) {
        this.invn_attr_k = invn_attr_k;
    }

    public void setInvn_attr_l(String invn_attr_l) {
        this.invn_attr_l = invn_attr_l;
    }

    public void setInvn_attr_m(String invn_attr_m) {
        this.invn_attr_m = invn_attr_m;
    }

    public void setInvn_attr_n(String invn_attr_n) {
        this.invn_attr_n = invn_attr_n;
    }

    public void setInvn_attr_o(String invn_attr_o) {
        this.invn_attr_o = invn_attr_o;
    }
}
