package br.com.storeautomacao.sync.model.wms;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("Header")
public class Header
{
    private String TimeStamp;

    private String ClientEnvCode;

    private String ParentCompanyCode;

    private String MessageId;

    private String OriginSystem;

    private String DocumentVersion;

    private String Entity;

    public String getTimeStamp ()
    {
        return TimeStamp;
    }

    public void setTimeStamp (String TimeStamp)
    {
        this.TimeStamp = TimeStamp;
    }

    public String getClientEnvCode ()
    {
        return ClientEnvCode;
    }

    public void setClientEnvCode (String ClientEnvCode)
    {
        this.ClientEnvCode = ClientEnvCode;
    }

    public String getParentCompanyCode ()
    {
        return ParentCompanyCode;
    }

    public void setParentCompanyCode (String ParentCompanyCode)
    {
        this.ParentCompanyCode = ParentCompanyCode;
    }

    public String getMessageId ()
    {
        return MessageId;
    }

    public void setMessageId (String MessageId)
    {
        this.MessageId = MessageId;
    }

    public String getOriginSystem ()
    {
        return OriginSystem;
    }

    public void setOriginSystem (String OriginSystem)
    {
        this.OriginSystem = OriginSystem;
    }

    public String getDocumentVersion ()
    {
        return DocumentVersion;
    }

    public void setDocumentVersion (String DocumentVersion)
    {
        this.DocumentVersion = DocumentVersion;
    }

    public String getEntity ()
    {
        return Entity;
    }

    public void setEntity (String Entity)
    {
        this.Entity = Entity;
    }

}