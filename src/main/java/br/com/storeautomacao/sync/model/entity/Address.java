package br.com.storeautomacao.sync.model.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

/**
 * <i>Entity</i> que representa os dados de uma Endereço.
 *
 * Created by alexandre on 12/09/17.
 */
@Document(collection = "address")
public class Address implements Serializable {

    @Id
    @JsonProperty("idAddress")
    /** Identificacao na base de dados*/
    private String id;

    private String street;
    private String number;
    private String complement;
    private String neighborhood;
    private String state;
    private String cep;
    private String city;
    private String latitude;
    private String logitude;

    public String getFormattedAddress() {
        StringBuilder str = new StringBuilder();
        str.append(street);
        str.append(number == null ? "" : " " + number);
        str.append(complement == null ? "" : " " + complement);
        str.append(neighborhood == null ? "" : " " + neighborhood);
        str.append(city == null ? "" : " " + city);
        str.append(cep == null ? "" : " " + cep);

        return str.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Address)) return false;

        Address address = (Address) o;

        if (getStreet() != null ? !getStreet().equals(address.getStreet()) : address.getStreet() != null) return false;
        if (getNumber() != null ? !getNumber().equals(address.getNumber()) : address.getNumber() != null) return false;
        if (getComplement() != null ? !getComplement().equals(address.getComplement()) : address.getComplement() != null)
            return false;
        if (getNeighborhood() != null ? !getNeighborhood().equals(address.getNeighborhood()) : address.getNeighborhood() != null)
            return false;
        if (getState() != null ? !getState().equals(address.getState()) : address.getState() != null) return false;
        if (getCep() != null ? !getCep().equals(address.getCep()) : address.getCep() != null) return false;
        return getCity() != null ? getCity().equals(address.getCity()) : address.getCity() == null;
    }

    @Override
    public int hashCode() {
        int result = getStreet() != null ? getStreet().hashCode() : 0;
        result = 31 * result + (getNumber() != null ? getNumber().hashCode() : 0);
        result = 31 * result + (getComplement() != null ? getComplement().hashCode() : 0);
        result = 31 * result + (getNeighborhood() != null ? getNeighborhood().hashCode() : 0);
        result = 31 * result + (getState() != null ? getState().hashCode() : 0);
        result = 31 * result + (getCep() != null ? getCep().hashCode() : 0);
        result = 31 * result + (getCity() != null ? getCity().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Address{" +
                "id='" + id + '\'' +
                ", street='" + street + '\'' +
                ", number='" + number + '\'' +
                ", complement='" + complement + '\'' +
                ", neighborhood='" + neighborhood + '\'' +
                ", state='" + state + '\'' +
                ", cep='" + cep + '\'' +
                ", city='" + city + '\'' +
                '}';
    }

    public double[] getLegacyCoordinates() {
        double [] coordinates = null;
        if (StringUtils.isNotEmpty(this.getLatitude()) && StringUtils.isNotEmpty(this.getLogitude())) {
            coordinates = new double[] {Double.valueOf(this.getLatitude()), Double.valueOf(this.getLogitude())};
        }

        return coordinates;
    }

    public void setCoordinates(double[] coordinates) {
        this.setLatitude(coordinates[0]);
        this.setLogitude(coordinates[1]);
    }

    public void setCoordinates(String latitude, String longitude) {
        this.setLatitude(latitude);
        this.setLogitude(longitude);
    }

    public String getId() { return id; }

    public void setId(String id) { this.id = id; }

    public String getStreet() { return street; }

    public void setStreet(String street) { this.street = street; }

    public String getNumber() { return number; }

    public void setNumber(String number) { this.number = number; }

    public String getComplement() { return complement; }

    public void setComplement(String complement) { this.complement = complement; }

    public String getNeighborhood() { return neighborhood; }

    public void setNeighborhood(String neighborhood) { this.neighborhood = neighborhood; }

    public String getState() { return state; }

    public void setState(String state) { this.state = state; }

    public String getCep() { return cep; }

    public void setCep(String cep) { this.cep = cep; }

    public String getCity() { return city; }

    public void setCity(String city) { this.city = city; }

    public String getLatitude() { return latitude; }

    public void setLatitude(String latitude) { this.latitude = latitude; }

    public void setLatitude(double latitude) { this.latitude = Double.toString(latitude); }

    public String getLogitude() { return logitude; }

    public void setLogitude(String logitude) { this.logitude = logitude; }

    public void setLogitude(double logitude) { this.logitude = Double.toString(logitude); }
}
