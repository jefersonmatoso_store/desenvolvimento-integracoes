package br.com.storeautomacao.sync.model.entity;

import br.com.storeautomacao.sync.model.enuns.ProfileEnum;
import br.com.storeautomacao.sync.security.model.Authority;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public enum ProfileType {
    ADMIN("administrador", ProfileEnum.ALL_ACCESS), //Administrador cadastra os condominios e os sindicos tambem
    SINDICO("sindico", ProfileEnum.CONCIERGE), // o sindico pode cadastra os perfis RECEPCAO e PRESTADOR_SERVICOS
    INQUILINO("inquilino", ProfileEnum.TENANT), // Morador pode enviar uma solicitacao para VISITA ou PRESTADOR_SERVICOS
    PRESTADOR_SERVICOS("prestador servico", ProfileEnum.VISIT), // Empresa que presta servicos(Funcionarios)
    RECEPCAO("recepcionista", ProfileEnum.CONCIERGE), //  responsavel por cadastrar inquilino ou enviar email para o inquilino criar o seu cadastro
    VISITANTE("visitante", ProfileEnum.VISIT), // pode criar uma conta
    VIGILANT("vigilante", ProfileEnum.CONCIERGE);

    private String name;
    private ProfileEnum[] profiles;

    private ProfileType(String name, ProfileEnum... profiles) {
        this.name = name;
        this.profiles = profiles;
    }

    public ProfileType getProfile(String name) {
        for (ProfileType profile : values()) {
            if (profile.name.equalsIgnoreCase(name)) {
                return profile;
            }
        }

        return null;
    }

    public ProfileEnum[] getProfiles() {
        return this.profiles;
    }

    public Collection<? extends GrantedAuthority> getAuthorities() {
        Set<Authority> authorities = new HashSet<Authority>();
        for (ProfileEnum profile : this.getProfiles()) {
            for (Authority auth : profile.getAuthorities()) {
                authorities.add(auth);
            }
        }

        return authorities;
    }

    public String getName() {
        return this.name;
    }

}
