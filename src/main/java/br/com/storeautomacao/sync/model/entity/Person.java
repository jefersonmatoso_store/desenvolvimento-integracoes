package br.com.storeautomacao.sync.model.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/** <i>Entity</i> que representa os dados de uma Pessoa.*/
@Document(collection = "person")
public class Person implements Serializable, UploadImage {

    /*
     * FIXME: Cenário: Mae com filha sem documento.
     * Crianca sera cadastrada? Se sim, com documento da mae?
     * Se sim, retirar falidacao de unicidade do documento.
     */

    @Id
    @JsonProperty("idPerson")
    /** Identificacao na base de dados*/
    private String id;

    /** Prenome da Pessoa*/
    private String firstName;

    /** Sobrenome da Pessoa*/
    private String lastName;

    /** Data de Nascimento da Pessoa*/
    private String birthDate;

    /** Telefone do Usuário */
    private String telephone;

    /** <i>Token</i> que identifica o aparelho (<i>device</i>) da Pessoa*/
    private String deviceToken;

    /** Documento de Identificacao da Pessoa*/
    @DBRef
    private List<IdentityDocument> identityDocuments = new ArrayList<IdentityDocument>();

    /**Veículos associados a esta pessoa*/

    /** E-mail de contato*/
    private String email;

    /** Foto de Identificacao da Pessoa*/
    private String photo;

    /** Chave <i>Hash</i> para o Reconhecimento Facial da Pessoa*/
    private String facialRecognition;

    /** Define */
    private PersonType personType;

    @DBRef
    private List<Address> addresses = new ArrayList<Address>();

    /**
     * Bytes da image da Pessoa.
     */
    @Transient
    private String base64Image;

    /**
     * Id que associa uma pessoa ao condominio.
     */
    private String idCondominium;

    public String getFullname() {
        return this.getFirstName() + " " + this.getLastName();
    }

    @Override
    public String toString() {

        StringBuilder strDocuments = new StringBuilder("[");
        for (IdentityDocument identity : this.getIdentityDocuments()) {
            strDocuments.append(identity.toString());
        }
        strDocuments.append("]");

        StringBuilder strVehicles = new StringBuilder("[");
        strVehicles.append("]");

        StringBuilder strAddresses = new StringBuilder("[");
        for (Address address : this.getAddresses()) {
            strAddresses.append(address.toString());
        }
        strAddresses.append("]");

        return "Person{" +
                "id='" + id + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", birthDate=" + birthDate + '\'' +
                ", telephone=" + telephone + '\'' +
                ", deviceToken=" + deviceToken + '\'' +
                ", email=" + email + '\'' +
                ", documents='" + strDocuments.toString() + '\'' +
                ", vehicles='" + strVehicles.toString() + '\'' +
                ", addresses='" + strAddresses.toString() + '\'' +
                ", idCondominio=" + idCondominium + '\'' +
                '}';
    }


    @Override
    public void addBase64Image(String base64Imagem) {

    }

    @Override
    public void addBase64Images(Collection<String> base64Images) {
        if (CollectionUtils.isNotEmpty(base64Images)) {
            for (String img : base64Images) {
                this.setBase64Image(img);
                break;
            }
        }
    }

    @Override
    public Collection<String> getBase64Images() {
        List<String> images = new ArrayList<String>();
        images.add(this.getBase64Image());
        return images;
    }

    @Override
    public void addUrlImages(String urlImagem) {
        if (StringUtils.isNotEmpty(urlImagem)) {
            this.setPhoto(urlImagem);
        }
    }

    @Override
    public void cleanBase64Imagens() {
        this.setBase64Image(null);
    }

    public void addIdentityDocument(IdentityDocument identityDocument) {
        if (this.getIdentityDocuments() == null || !this.getIdentityDocuments().isEmpty()) {
            this.setIdentityDocuments(new ArrayList<IdentityDocument>());
        }
        this.getIdentityDocuments().add(identityDocument);
    }

    public void addIdentityDocuments(Collection<IdentityDocument> identityDocuments) {
        if (this.getIdentityDocuments() == null || !this.getIdentityDocuments().isEmpty()) {
            this.setIdentityDocuments(new ArrayList<IdentityDocument>());
        }
        this.getIdentityDocuments().addAll(identityDocuments);
    }

    public void removeIdentityDocument(IdentityDocument identityDocument) {
        if (this.getIdentityDocuments() == null || !this.getIdentityDocuments().isEmpty()) {
            this.setIdentityDocuments(new ArrayList<IdentityDocument>());
        }
        this.getIdentityDocuments().remove(identityDocument);
    }

    public void removeIdentityDocuments(Collection<IdentityDocument> identityDocuments) {
        if (this.getIdentityDocuments() == null || !this.getIdentityDocuments().isEmpty()) {
            this.setIdentityDocuments(new ArrayList<IdentityDocument>());
        }
        this.getIdentityDocuments().removeAll(identityDocuments);
    }

    public void addAddress(Address address) {
        if (this.getAddresses() == null || !this.getAddresses().isEmpty()) {
            this.setAddresses(new ArrayList<Address>());
        }
        this.getAddresses().add(address);
    }

    public void addAddresses(Collection<Address> addresses) {
        if (this.getAddresses() == null || !this.getAddresses().isEmpty()) {
            this.setAddresses(new ArrayList<Address>());
        }
        this.getAddresses().addAll(addresses);
    }

    public void removeAddress(Address address) {
        if (this.getAddresses() == null || !this.getAddresses().isEmpty()) {
            this.setAddresses(new ArrayList<Address>());
        }
        this.getAddresses().remove(address);
    }

    public void removeAddresses(Collection<Address> addresses) {
        if (this.getAddresses() == null || !this.getAddresses().isEmpty()) {
            this.setAddresses(new ArrayList<Address>());
        }
        this.getAddresses().removeAll(addresses);
    }

    @Override
    public String getId() { return id; }

    public void setId(String id) { this.id = id; }

    public String getFirstName() { return firstName; }

    public void setFirstName(String firstName) { this.firstName = firstName; }

    public String getLastName() { return lastName; }

    public void setLastName(String lastName) { this.lastName = lastName; }

    public String getBirthDate() { return birthDate; }

    public void setBirthDate(String birthDate) { this.birthDate = birthDate; }

    public String getTelephone() { return telephone; }

    public void setTelephone(String telephone) { this.telephone = telephone; }

    public String getDeviceToken() { return deviceToken; }

    public void setDeviceToken(String deviceToken) { this.deviceToken = deviceToken; }

    public List<IdentityDocument> getIdentityDocuments() { return identityDocuments; }

    public void setIdentityDocuments(List<IdentityDocument> identityDocuments) { this.identityDocuments = identityDocuments; }

    public String getEmail() { return email; }

    public void setEmail(String email) { this.email = email; }

    public String getPhoto() { return photo; }

    public void setPhoto(String photo) { this.photo = photo; }

    public String getFacialRecognition() { return facialRecognition; }

    public void setFacialRecognition(String facialRecognition) { this.facialRecognition = facialRecognition; }

    public PersonType getPersonType() { return personType; }

    public void setPersonType(PersonType personType) { this.personType = personType; }

    public List<Address> getAddresses() { return addresses; }

    public void setAddresses(List<Address> addresses) { this.addresses = addresses; }

    public String getBase64Image() {
        return base64Image;
    }

    public void setBase64Image(String base64Image) {
        this.base64Image = base64Image;
    }

    public String getIdCondominium() {
        return idCondominium;
    }

    public void setIdCondominium(String idCondominium) {
        this.idCondominium = idCondominium;
    }

}
