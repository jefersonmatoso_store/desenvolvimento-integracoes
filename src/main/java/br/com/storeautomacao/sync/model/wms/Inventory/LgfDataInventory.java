package br.com.storeautomacao.sync.model.wms.Inventory;

import br.com.storeautomacao.sync.model.wms.Header;
import br.com.storeautomacao.sync.model.wms.ListOfItems;
import com.thoughtworks.xstream.annotations.XStreamAlias;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "LgfData")
@XStreamAlias("LgfData")
public class LgfDataInventory
{
    private Header Header;

    private ListOfInventory ListOfInventory;

    public Header getHeader ()
    {
        return Header;
    }

    public void setHeader (Header Header)
    {
        this.Header = Header;
    }

    public br.com.storeautomacao.sync.model.wms.Inventory.ListOfInventory getListOfInventory() {
        return ListOfInventory;
    }

    public void setListOfInventory(br.com.storeautomacao.sync.model.wms.Inventory.ListOfInventory listOfInventory) {
        ListOfInventory = listOfInventory;
    }
}
