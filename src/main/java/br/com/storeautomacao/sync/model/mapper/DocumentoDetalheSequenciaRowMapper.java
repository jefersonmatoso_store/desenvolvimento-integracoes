package br.com.storeautomacao.sync.model.mapper;

import br.com.storeautomacao.sync.model.entity.DocumentoDetalheSequencia;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DocumentoDetalheSequenciaRowMapper implements RowMapper<DocumentoDetalheSequencia> {
    
    public DocumentoDetalheSequencia mapRow(ResultSet rs, int rowNum) throws SQLException {

        DocumentoDetalheSequencia documentoDetalheSequencia = new DocumentoDetalheSequencia();

        documentoDetalheSequencia.setSequenciaIntegracao(rs.getBigDecimal("SEQUENCIAINTEGRACAO"));
        documentoDetalheSequencia.setSequenciaDocumento(rs.getBigDecimal("SEQUENCIADOCUMENTO"));
        documentoDetalheSequencia.setSequenciaDetalhe(rs.getBigDecimal("SEQUENCIADETALHE"));
        documentoDetalheSequencia.setSequenciaDetalheSequencia(rs.getBigDecimal("SEQUENCIADETALHESEQUENCIA"));
        documentoDetalheSequencia.setPesoBruto(rs.getBigDecimal("PESOBRUTO"));
        documentoDetalheSequencia.setPesoLiquido(rs.getBigDecimal("PESOLIQUIDO"));
        documentoDetalheSequencia.setLargura(rs.getBigDecimal("LARGURA"));
        documentoDetalheSequencia.setComprimento(rs.getBigDecimal("COMPRIMENTO"));
        documentoDetalheSequencia.setAltura(rs.getBigDecimal("ALTURA"));
        documentoDetalheSequencia.setTipouc(rs.getString("TIPOUC"));
        documentoDetalheSequencia.setFatorTipoUc(rs.getBigDecimal("FATORTIPOUC"));
        documentoDetalheSequencia.setQuantidadeInicial(rs.getBigDecimal("QUANTIDADEINICIAL"));
        documentoDetalheSequencia.setClasseProduto(rs.getString("CLASSEPRODUTO"));
        documentoDetalheSequencia.setQuantidadeMovimento(rs.getBigDecimal("QUANTIDADEMOVIMENTO"));
        documentoDetalheSequencia.setCodigoUa(rs.getBigDecimal("CODIGOUA"));

        return documentoDetalheSequencia;
    }

}