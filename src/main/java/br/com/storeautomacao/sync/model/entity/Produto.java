package br.com.storeautomacao.sync.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.math.BigDecimal;

public class Produto {

    private BigDecimal sequenciaIntegracao;
    private BigDecimal tipoIntegracao;
    private String codigoEmpresa;
    private String codigoProduto;
    private String descricaoProduto;
    private String descricaoProdutoDet;
    private String tipoProduto;
    private String modeloProduto;
    private String grupoProduto;
    private String descricaoGrupoProduto;
    private String classificacaoFiscal;
    private BigDecimal situacaoTributaria;
    private BigDecimal prazoValidade;
    private String classificacaoProduto;
    private String classificacaoPicking;
    private BigDecimal situacaoProduto;
    private String cnpjcpfEmpresa;
    private String idProduto;
    private String observacaoProduto;
    private String codigoSubGrupo;
    private String descricaoSubGrupo;
    private String codigoCategoria;
    private String descricaoCategoria;
    private String codigoLinha;
    private String descricaoLinha;
    private String codigoPublicoAlvo;
    private String descricaoPublicoAlvo;
    private String informacaoAdicional1;
    private String informacaoadicional2;
    private String informacaoAdicional3;
    private String informacaoAdicional4;
    private String informacaoAdicional5;
    private BigDecimal scrapProduto;
    private String codigocontabil;
    private String sitTributariaOrigem;
    private String origemProduto;
    private String descricaoOrigemProduto;
    private String gradeProduto;
    private String vencimentoInterbaloExp;
    private String vencimentoIntervaloRec;
    private String codigoNcm;
    private BigDecimal temperaturaMedia;
    private BigDecimal umidadeMedia;
    private String classeRisco;
    private BigDecimal temperaturaMinima;
    private BigDecimal temperaturaMaxima;
    private BigDecimal classificacaoOnu;
    private String curvaabc;
    private String descricaoCurvaAbc;
    private String classeProduto;
    private BigDecimal utilizarClassificador;
    private BigDecimal estavel;
    private BigDecimal litragem;
    private BigDecimal produtoPesado;
    private BigDecimal tipoItemFiscal;
    private String codigoSecao;
    private String descricaoSecao;

    public Produto() {}

    public BigDecimal getSequenciaIntegracao() {
        return sequenciaIntegracao;
    }

    public void setSequenciaIntegracao(BigDecimal sequenciaIntegracao) {
        this.sequenciaIntegracao = sequenciaIntegracao;
    }

    public BigDecimal getTipoIntegracao() {
        return tipoIntegracao;
    }

    public void setTipoIntegracao(BigDecimal tipoIntegracao) {
        this.tipoIntegracao = tipoIntegracao;
    }

    public String getCodigoEmpresa() {
        return codigoEmpresa;
    }

    public void setCodigoEmpresa(String codigoEmpresa) {
        this.codigoEmpresa = codigoEmpresa;
    }

    public String getCodigoProduto() {
        return codigoProduto;
    }

    public void setCodigoProduto(String codigoProduto) {
        this.codigoProduto = codigoProduto;
    }

    public String getDescricaoProduto() {
        return descricaoProduto;
    }

    public void setDescricaoProduto(String descricaoProduto) {
        this.descricaoProduto = descricaoProduto;
    }

    public String getDescricaoProdutoDet() {
        return descricaoProdutoDet;
    }

    public void setDescricaoProdutoDet(String descricaoProdutoDet) {
        this.descricaoProdutoDet = descricaoProdutoDet;
    }

    public String getTipoProduto() {
        return tipoProduto;
    }

    public void setTipoProduto(String tipoProduto) {
        this.tipoProduto = tipoProduto;
    }

    public String getModeloProduto() {
        return modeloProduto;
    }

    public void setModeloProduto(String modeloProduto) {
        this.modeloProduto = modeloProduto;
    }

    public String getGrupoProduto() {
        return grupoProduto;
    }

    public void setGrupoProduto(String grupoProduto) {
        this.grupoProduto = grupoProduto;
    }

    public String getDescricaoGrupoProduto() {
        return descricaoGrupoProduto;
    }

    public void setDescricaoGrupoProduto(String descricaoGrupoProduto) {
        this.descricaoGrupoProduto = descricaoGrupoProduto;
    }

    public String getClassificacaoFiscal() {
        return classificacaoFiscal;
    }

    public void setClassificacaoFiscal(String classificacaoFiscal) {
        this.classificacaoFiscal = classificacaoFiscal;
    }

    public BigDecimal getSituacaoTributaria() {
        return situacaoTributaria;
    }

    public void setSituacaoTributaria(BigDecimal situacaoTributaria) {
        this.situacaoTributaria = situacaoTributaria;
    }

    public BigDecimal getPrazoValidade() {
        return prazoValidade;
    }

    public void setPrazoValidade(BigDecimal prazoValidade) {
        this.prazoValidade = prazoValidade;
    }

    public String getClassificacaoProduto() {
        return classificacaoProduto;
    }

    public void setClassificacaoProduto(String classificacaoProduto) {
        this.classificacaoProduto = classificacaoProduto;
    }

    public String getClassificacaoPicking() {
        return classificacaoPicking;
    }

    public void setClassificacaoPicking(String classificacaoPicking) {
        this.classificacaoPicking = classificacaoPicking;
    }

    public BigDecimal getSituacaoProduto() {
        return situacaoProduto;
    }

    public void setSituacaoProduto(BigDecimal situacaoProduto) {
        this.situacaoProduto = situacaoProduto;
    }

    public String getCnpjcpfEmpresa() {
        return cnpjcpfEmpresa;
    }

    public void setCnpjcpfEmpresa(String cnpjcpfEmpresa) {
        this.cnpjcpfEmpresa = cnpjcpfEmpresa;
    }

    public String getIdProduto() {
        return idProduto;
    }

    public void setIdProduto(String idProduto) {
        this.idProduto = idProduto;
    }

    public String getObservacaoProduto() {
        return observacaoProduto;
    }

    public void setObservacaoProduto(String observacaoProduto) {
        this.observacaoProduto = observacaoProduto;
    }

    public String getCodigoSubGrupo() {
        return codigoSubGrupo;
    }

    public void setCodigoSubGrupo(String codigoSubGrupo) {
        this.codigoSubGrupo = codigoSubGrupo;
    }

    public String getDescricaoSubGrupo() {
        return descricaoSubGrupo;
    }

    public void setDescricaoSubGrupo(String descricaoSubGrupo) {
        this.descricaoSubGrupo = descricaoSubGrupo;
    }

    public String getCodigoCategoria() {
        return codigoCategoria;
    }

    public void setCodigoCategoria(String codigoCategoria) {
        this.codigoCategoria = codigoCategoria;
    }

    public String getDescricaoCategoria() {
        return descricaoCategoria;
    }

    public void setDescricaoCategoria(String descricaoCategoria) {
        this.descricaoCategoria = descricaoCategoria;
    }

    public String getCodigoLinha() {
        return codigoLinha;
    }

    public void setCodigoLinha(String codigoLinha) {
        this.codigoLinha = codigoLinha;
    }

    public String getDescricaoLinha() {
        return descricaoLinha;
    }

    public void setDescricaoLinha(String descricaoLinha) {
        this.descricaoLinha = descricaoLinha;
    }

    public String getCodigoPublicoAlvo() {
        return codigoPublicoAlvo;
    }

    public void setCodigoPublicoAlvo(String codigoPublicoAlvo) {
        this.codigoPublicoAlvo = codigoPublicoAlvo;
    }

    public String getDescricaoPublicoAlvo() {
        return descricaoPublicoAlvo;
    }

    public void setDescricaoPublicoAlvo(String descricaoPublicoAlvo) {
        this.descricaoPublicoAlvo = descricaoPublicoAlvo;
    }

    public String getInformacaoAdicional1() {
        return informacaoAdicional1;
    }

    public void setInformacaoAdicional1(String informacaoAdicional1) {
        this.informacaoAdicional1 = informacaoAdicional1;
    }

    public String getInformacaoadicional2() {
        return informacaoadicional2;
    }

    public void setInformacaoadicional2(String informacaoadicional2) {
        this.informacaoadicional2 = informacaoadicional2;
    }

    public String getInformacaoAdicional3() {
        return informacaoAdicional3;
    }

    public void setInformacaoAdicional3(String informacaoAdicional3) {
        this.informacaoAdicional3 = informacaoAdicional3;
    }

    public String getInformacaoAdicional4() {
        return informacaoAdicional4;
    }

    public void setInformacaoAdicional4(String informacaoAdicional4) {
        this.informacaoAdicional4 = informacaoAdicional4;
    }

    public String getInformacaoAdicional5() {
        return informacaoAdicional5;
    }

    public void setInformacaoAdicional5(String informacaoAdicional5) {
        this.informacaoAdicional5 = informacaoAdicional5;
    }

    public BigDecimal getScrapProduto() {
        return scrapProduto;
    }

    public void setScrapProduto(BigDecimal scrapProduto) {
        this.scrapProduto = scrapProduto;
    }

    public String getCodigocontabil() {
        return codigocontabil;
    }

    public void setCodigocontabil(String codigocontabil) {
        this.codigocontabil = codigocontabil;
    }

    public String getSitTributariaOrigem() {
        return sitTributariaOrigem;
    }

    public void setSitTributariaOrigem(String sitTributariaOrigem) {
        this.sitTributariaOrigem = sitTributariaOrigem;
    }

    public String getOrigemProduto() {
        return origemProduto;
    }

    public void setOrigemProduto(String origemProduto) {
        this.origemProduto = origemProduto;
    }

    public String getDescricaoOrigemProduto() {
        return descricaoOrigemProduto;
    }

    public void setDescricaoOrigemProduto(String descricaoOrigemProduto) {
        this.descricaoOrigemProduto = descricaoOrigemProduto;
    }

    public String getGradeProduto() {
        return gradeProduto;
    }

    public void setGradeProduto(String gradeProduto) {
        this.gradeProduto = gradeProduto;
    }

    public String getVencimentoInterbaloExp() {
        return vencimentoInterbaloExp;
    }

    public void setVencimentoInterbaloExp(String vencimentoInterbaloExp) {
        this.vencimentoInterbaloExp = vencimentoInterbaloExp;
    }

    public String getVencimentoIntervaloRec() {
        return vencimentoIntervaloRec;
    }

    public void setVencimentoIntervaloRec(String vencimentoIntervaloRec) {
        this.vencimentoIntervaloRec = vencimentoIntervaloRec;
    }

    public String getCodigoNcm() {
        return codigoNcm;
    }

    public void setCodigoNcm(String codigoNcm) {
        this.codigoNcm = codigoNcm;
    }

    public BigDecimal getTemperaturaMedia() {
        return temperaturaMedia;
    }

    public void setTemperaturaMedia(BigDecimal temperaturaMedia) {
        this.temperaturaMedia = temperaturaMedia;
    }

    public BigDecimal getUmidadeMedia() {
        return umidadeMedia;
    }

    public void setUmidadeMedia(BigDecimal umidadeMedia) {
        this.umidadeMedia = umidadeMedia;
    }

    public String getClasseRisco() {
        return classeRisco;
    }

    public void setClasseRisco(String classeRisco) {
        this.classeRisco = classeRisco;
    }

    public BigDecimal getTemperaturaMinima() {
        return temperaturaMinima;
    }

    public void setTemperaturaMinima(BigDecimal temperaturaMinima) {
        this.temperaturaMinima = temperaturaMinima;
    }

    public BigDecimal getTemperaturaMaxima() {
        return temperaturaMaxima;
    }

    public void setTemperaturaMaxima(BigDecimal temperaturaMaxima) {
        this.temperaturaMaxima = temperaturaMaxima;
    }

    public BigDecimal getClassificacaoOnu() {
        return classificacaoOnu;
    }

    public void setClassificacaoOnu(BigDecimal classificacaoOnu) {
        this.classificacaoOnu = classificacaoOnu;
    }

    public String getCurvaabc() {
        return curvaabc;
    }

    public void setCurvaabc(String curvaabc) {
        this.curvaabc = curvaabc;
    }

    public String getDescricaoCurvaAbc() {
        return descricaoCurvaAbc;
    }

    public void setDescricaoCurvaAbc(String descricaoCurvaAbc) {
        this.descricaoCurvaAbc = descricaoCurvaAbc;
    }

    public String getClasseProduto() {
        return classeProduto;
    }

    public void setClasseProduto(String classeProduto) {
        this.classeProduto = classeProduto;
    }

    public BigDecimal getUtilizarClassificador() {
        return utilizarClassificador;
    }

    public void setUtilizarClassificador(BigDecimal utilizarClassificador) {
        this.utilizarClassificador = utilizarClassificador;
    }

    public BigDecimal getEstavel() {
        return estavel;
    }

    public void setEstavel(BigDecimal estavel) {
        this.estavel = estavel;
    }

    public BigDecimal getLitragem() {
        return litragem;
    }

    public void setLitragem(BigDecimal litragem) {
        this.litragem = litragem;
    }

    public BigDecimal getProdutoPesado() {
        return produtoPesado;
    }

    public void setProdutoPesado(BigDecimal produtoPesado) {
        this.produtoPesado = produtoPesado;
    }

    public BigDecimal getTipoItemFiscal() {
        return tipoItemFiscal;
    }

    public void setTipoItemFiscal(BigDecimal tipoItemFiscal) {
        this.tipoItemFiscal = tipoItemFiscal;
    }

    public String getCodigoSecao() {
        return codigoSecao;
    }

    public void setCodigoSecao(String codigoSecao) {
        this.codigoSecao = codigoSecao;
    }

    public String getDescricaoSecao() {
        return descricaoSecao;
    }

    public void setDescricaoSecao(String descricaoSecao) {
        this.descricaoSecao = descricaoSecao;
    }

}
