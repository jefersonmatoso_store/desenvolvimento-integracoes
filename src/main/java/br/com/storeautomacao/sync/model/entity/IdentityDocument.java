package br.com.storeautomacao.sync.model.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * <i>Entity</i> que representa os Documentos de Identidade que uma Pessoa pode possuir.
 *
 * Created by alexandre on 31/07/17.
 */
@Document(collection = "identity_document")

// TODO: Comentado o indice para poder gravar valores vazios. Pesquisar melhor o uso do indice composto.
//@CompoundIndexes({
//        @CompoundIndex(name = "identity_document_idx", unique = true, def = "{'documentNumber': 1, 'documentType': 1}")
//})
public class IdentityDocument implements Serializable {

    @Id
    @JsonProperty("idIdentityDocumento")
    /** Identificacao na base de dados*/
    private String id;

    @NotNull
    private String documentNumber;

    @NotNull
    private IdentityDocumentType documentType;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        IdentityDocument that = (IdentityDocument) o;

        if (!documentNumber.equals(that.documentNumber)) return false;
        return documentType == that.documentType;
    }

    @Override
    public int hashCode() {
        int result = documentNumber.hashCode();
        result = 31 * result + documentType.hashCode();
        return result;
    }

    public String getId() { return id; }

    public void setId(String id) { this.id = id; }

    public String getDocumentNumber() { return documentNumber; }

    public void setDocumentNumber(String documentNumber) { this.documentNumber = documentNumber; }

    public IdentityDocumentType getDocumentType() { return documentType; }

    public void setDocumentType(IdentityDocumentType documentType) { this.documentType = documentType; }

    /** Tipos de Documentos*/
    public static enum IdentityDocumentType {
        RG, CPF, CNH, PASSAPORT;
    }
}
