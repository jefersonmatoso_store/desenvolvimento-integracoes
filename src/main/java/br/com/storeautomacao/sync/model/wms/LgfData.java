package br.com.storeautomacao.sync.model.wms;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="LgfData")
public class LgfData
{
    private Header Header;

    private ListOfInventoryHistories ListOfInventoryHistories;

    public Header getHeader ()
    {
        return Header;
    }

    public void setHeader (Header Header)
    {
        this.Header = Header;
    }

    public ListOfInventoryHistories getListOfInventoryHistories ()
    {
        return ListOfInventoryHistories;
    }

    public void setListOfInventoryHistories (ListOfInventoryHistories ListOfInventoryHistories)
    {
        this.ListOfInventoryHistories = ListOfInventoryHistories;
    }


}
