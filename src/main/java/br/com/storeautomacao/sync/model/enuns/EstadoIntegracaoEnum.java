package br.com.storeautomacao.sync.model.enuns;

/**
 * Created by iomar on 23/03/18.
 */
public enum EstadoIntegracaoEnum {

    EM_ANDAMENTO,     // Retirado
    PENDENTE,   // Disponível para retirar
    FINALIZADA,
    NAO_EXECUTADA

}
