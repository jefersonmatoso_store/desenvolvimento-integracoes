package br.com.storeautomacao.sync.model.wms;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("LgfData")
public class LgfDataOrder {

    private ListOfOrders ListOfOrders;

    private Header Header;

    public ListOfOrders getListOfOrders ()
    {
        return ListOfOrders;
    }

    public void setListOfOrders (ListOfOrders ListOfOrders)
    {
        this.ListOfOrders = ListOfOrders;
    }

    public Header getHeader ()
    {
        return Header;
    }

    public void setHeader (Header Header)
    {
        this.Header = Header;
    }

}
