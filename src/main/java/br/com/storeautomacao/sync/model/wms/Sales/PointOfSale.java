package br.com.storeautomacao.sync.model.wms.Sales;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("point_of_sale")
public class PointOfSale {
    private String facility_code;
    private String company_code;
    private String transaction_type;
    private String transactionid;
    private String seq_nbr;
    private String ref_transactionid;
    private String ref_seq_nbr;
    private String location_barcode;
    private String item_alternate_code;
    private String item_part_a;
    private String item_part_b;
    private String item_part_c;
    private String item_part_d;
    private String item_part_e;
    private String item_part_f;
    private String invn_attr_a;
    private String invn_attr_b;
    private String invn_attr_c;
    private String expiry_date;
    private String batch_nbr;
    private String serial_nbr;
    private String quantity;
    private String pos_user;
    private String invn_attr_d;
    private String invn_attr_e;
    private String invn_attr_f;
    private String invn_attr_g;
    private String invn_attr_h;
    private String invn_attr_i;
    private String invn_attr_j;
    private String invn_attr_k;
    private String invn_attr_l;
    private String invn_attr_m;
    private String invn_attr_n;
    private String invn_attr_o;

    public String getFacility_code() {
        return facility_code;
    }

    public void setFacility_code(String facility_code) {
        this.facility_code = facility_code;
    }

    public String getCompany_code() {
        return company_code;
    }

    public void setCompany_code(String company_code) {
        this.company_code = company_code;
    }

    public String getTransaction_type() {
        return transaction_type;
    }

    public void setTransaction_type(String transaction_type) {
        this.transaction_type = transaction_type;
    }

    public String getTransactionid() {
        return transactionid;
    }

    public void setTransactionid(String transactionid) {
        this.transactionid = transactionid;
    }

    public String getSeq_nbr() {
        return seq_nbr;
    }

    public void setSeq_nbr(String seq_nbr) {
        this.seq_nbr = seq_nbr;
    }

    public String getRef_transactionid() {
        return ref_transactionid;
    }

    public void setRef_transactionid(String ref_transactionid) {
        this.ref_transactionid = ref_transactionid;
    }

    public String getRef_seq_nbr() {
        return ref_seq_nbr;
    }

    public void setRef_seq_nbr(String ref_seq_nbr) {
        this.ref_seq_nbr = ref_seq_nbr;
    }

    public String getLocation_barcode() {
        return location_barcode;
    }

    public void setLocation_barcode(String location_barcode) {
        this.location_barcode = location_barcode;
    }

    public String getItem_alternate_code() {
        return item_alternate_code;
    }

    public void setItem_alternate_code(String item_alternate_code) {
        this.item_alternate_code = item_alternate_code;
    }

    public String getItem_part_a() {
        return item_part_a;
    }

    public void setItem_part_a(String item_part_a) {
        this.item_part_a = item_part_a;
    }

    public String getItem_part_b() {
        return item_part_b;
    }

    public void setItem_part_b(String item_part_b) {
        this.item_part_b = item_part_b;
    }

    public String getItem_part_c() {
        return item_part_c;
    }

    public void setItem_part_c(String item_part_c) {
        this.item_part_c = item_part_c;
    }

    public String getItem_part_d() {
        return item_part_d;
    }

    public void setItem_part_d(String item_part_d) {
        this.item_part_d = item_part_d;
    }

    public String getItem_part_e() {
        return item_part_e;
    }

    public void setItem_part_e(String item_part_e) {
        this.item_part_e = item_part_e;
    }

    public String getItem_part_f() {
        return item_part_f;
    }

    public void setItem_part_f(String item_part_f) {
        this.item_part_f = item_part_f;
    }

    public String getInvn_attr_a() {
        return invn_attr_a;
    }

    public void setInvn_attr_a(String invn_attr_a) {
        this.invn_attr_a = invn_attr_a;
    }

    public String getInvn_attr_b() {
        return invn_attr_b;
    }

    public void setInvn_attr_b(String invn_attr_b) {
        this.invn_attr_b = invn_attr_b;
    }

    public String getInvn_attr_c() {
        return invn_attr_c;
    }

    public void setInvn_attr_c(String invn_attr_c) {
        this.invn_attr_c = invn_attr_c;
    }

    public String getExpiry_date() {
        return expiry_date;
    }

    public void setExpiry_date(String expiry_date) {
        this.expiry_date = expiry_date;
    }

    public String getBatch_nbr() {
        return batch_nbr;
    }

    public void setBatch_nbr(String batch_nbr) {
        this.batch_nbr = batch_nbr;
    }

    public String getSerial_nbr() {
        return serial_nbr;
    }

    public void setSerial_nbr(String serial_nbr) {
        this.serial_nbr = serial_nbr;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getPos_user() {
        return pos_user;
    }

    public void setPos_user(String pos_user) {
        this.pos_user = pos_user;
    }

    public String getInvn_attr_d() {
        return invn_attr_d;
    }

    public void setInvn_attr_d(String invn_attr_d) {
        this.invn_attr_d = invn_attr_d;
    }

    public String getInvn_attr_e() {
        return invn_attr_e;
    }

    public void setInvn_attr_e(String invn_attr_e) {
        this.invn_attr_e = invn_attr_e;
    }

    public String getInvn_attr_f() {
        return invn_attr_f;
    }

    public void setInvn_attr_f(String invn_attr_f) {
        this.invn_attr_f = invn_attr_f;
    }

    public String getInvn_attr_g() {
        return invn_attr_g;
    }

    public void setInvn_attr_g(String invn_attr_g) {
        this.invn_attr_g = invn_attr_g;
    }

    public String getInvn_attr_h() {
        return invn_attr_h;
    }

    public void setInvn_attr_h(String invn_attr_h) {
        this.invn_attr_h = invn_attr_h;
    }

    public String getInvn_attr_i() {
        return invn_attr_i;
    }

    public void setInvn_attr_i(String invn_attr_i) {
        this.invn_attr_i = invn_attr_i;
    }

    public String getInvn_attr_j() {
        return invn_attr_j;
    }

    public void setInvn_attr_j(String invn_attr_j) {
        this.invn_attr_j = invn_attr_j;
    }

    public String getInvn_attr_k() {
        return invn_attr_k;
    }

    public void setInvn_attr_k(String invn_attr_k) {
        this.invn_attr_k = invn_attr_k;
    }

    public String getInvn_attr_l() {
        return invn_attr_l;
    }

    public void setInvn_attr_l(String invn_attr_l) {
        this.invn_attr_l = invn_attr_l;
    }

    public String getInvn_attr_m() {
        return invn_attr_m;
    }

    public void setInvn_attr_m(String invn_attr_m) {
        this.invn_attr_m = invn_attr_m;
    }

    public String getInvn_attr_n() {
        return invn_attr_n;
    }

    public void setInvn_attr_n(String invn_attr_n) {
        this.invn_attr_n = invn_attr_n;
    }

    public String getInvn_attr_o() {
        return invn_attr_o;
    }

    public void setInvn_attr_o(String invn_attr_o) {
        this.invn_attr_o = invn_attr_o;
    }
}
