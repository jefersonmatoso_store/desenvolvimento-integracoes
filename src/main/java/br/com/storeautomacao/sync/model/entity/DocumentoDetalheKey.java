package br.com.storeautomacao.sync.model.entity;

public class DocumentoDetalheKey {

    private String classeProduto;
    private int sequenciaDetalhe;
    private int quantidade;
    private int sequenciaDocumentoDetalheNovo;

    public DocumentoDetalheKey(String classeProduto, int sequenciaDetalhe, int quantidade) {
        this.classeProduto = classeProduto;
        this.sequenciaDetalhe = sequenciaDetalhe;
        this.quantidade = quantidade;
    }

    public  String getClasseProduto() {
        return classeProduto;
    }

    public void setClasseProduto(String classeProduto) {
        this.classeProduto = classeProduto;
    }

    public int getSequenciaDetalhe() {
        return sequenciaDetalhe;
    }

    public void setSequenciaDetalhe(int sequenciaDetalhe) {
        this.sequenciaDetalhe = sequenciaDetalhe;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }
}
