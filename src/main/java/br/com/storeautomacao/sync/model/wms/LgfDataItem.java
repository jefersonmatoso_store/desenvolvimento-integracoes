package br.com.storeautomacao.sync.model.wms;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "LgfData")
@XStreamAlias("LgfData")
public class LgfDataItem
{
    private Header Header;

    private ListOfItems ListOfItems;

    public Header getHeader ()
    {
        return Header;
    }

    public void setHeader (Header Header)
    {
        this.Header = Header;
    }


    public ListOfItems getListOfItems() {
        return ListOfItems;
    }

    public void setListOfItems(ListOfItems listOfItems) {
        this.ListOfItems = listOfItems;
    }
}
