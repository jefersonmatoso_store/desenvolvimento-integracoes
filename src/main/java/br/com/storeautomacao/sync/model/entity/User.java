package br.com.storeautomacao.sync.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Collections;

/**
 * <i>Entity</i> que represente um usuario no sistema.
 * <p>
 * Created by Iomar on 26/05/2017.
 */
@Document(collection = "users")
public class User implements UserDetails {

    @Id
    @JsonProperty("idUser")
    /** Identificacao na base de dados*/
    private String id;

    /**
     * Identificacao de acesso do usuario
     */
    private String userName;


    /** Senha do usuario*/
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String password;

    /**
     *  Senha para as autorizações do inquilino dentro do condominio.<br />
     *  Ex.:<br />
     *  <ul>
     *      <li><i>Check-in</i> do Inquilino na Portaria;</li>
     *      <li>Inquilino autorizar a entrada de um visitante;</li>
     *  </ul>
     *  */
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String checkinPassword;

    /**
     * Status do usuario {Ativo, Inativo}.
     */
    private String status;

    /**
     * Identificação do tipo de Perfil.
     */
    @DBRef
    private Profile profile;

    /** Token que representa a Autenticação do Usuário */
    @Transient
    private String authenticationToken;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @JsonIgnore
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        Collection<? extends GrantedAuthority> authorities = (this.getProfile() == null || this.getProfile().getProfileType() == null)
                ? Collections.<GrantedAuthority>emptyList()
                : this.getProfile().getProfileType().getAuthorities();

        return authorities;
    }

    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return null;
    }

    @Override
    public boolean isAccountNonExpired() {
        return false;
    }

    @Override
    public boolean isAccountNonLocked() {
        return false;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return false;
    }

    @Override
    public boolean isEnabled() {
        return false;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCheckinPassword() { return checkinPassword; }

    public void setCheckinPassword(String checkinPassword) { this.checkinPassword = checkinPassword; }

    public String getAuthenticationToken() { return authenticationToken; }

    public void setAuthenticationToken(String token) { this.authenticationToken = token; }

}
