package br.com.storeautomacao.sync.model.wms.Appointment;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.ArrayList;
import java.util.List;

@XStreamAlias("ListOfAppointments")
public class ListOfAppointments {

    @XStreamImplicit(itemFieldName="appointment")
    private List<Appointment> appointments = new ArrayList<Appointment>();

    public List<Appointment> getAppointments() {
        return appointments;
    }

    public void setAppointments(List<Appointment> appointments) {
        this.appointments = appointments;
    }
}
