package br.com.storeautomacao.sync.model.entity;

public class Picked {

    private int idPickupItem;
    private int quantity;
    private String dateStart;
    private String dateFinish;
    private String dateDisplacement;


    public int getIdPickupItem() {
        return idPickupItem;
    }

    public void setIdPickupItem(int idPickupItem) {
        this.idPickupItem = idPickupItem;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getDateStart() {
        return dateStart;
    }

    public void setDateStart(String dateStart) {
        this.dateStart = dateStart;
    }

    public String getDateFinish() {
        return dateFinish;
    }

    public void setDateFinish(String dateFinish) {
        this.dateFinish = dateFinish;
    }

    public String getDateDisplacement() {
        return dateDisplacement;
    }

    public void setDateDisplacement(String dateDisplacement) {
        this.dateDisplacement = dateDisplacement;
    }

}
