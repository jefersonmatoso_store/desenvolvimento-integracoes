package br.com.storeautomacao.sync.model.wms.PurchaseOrder;


import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("LgfData")
public class OrdemDeCompra {

    private br.com.storeautomacao.sync.model.wms.Header Header;

    @XStreamImplicit
    private List<ListOfPurchaseOrders> ListOfPurchaseOrders;

    public br.com.storeautomacao.sync.model.wms.Header getHeader() {
        return Header;
    }

    public void setHeader(br.com.storeautomacao.sync.model.wms.Header header) {
        Header = header;
    }

    public List<br.com.storeautomacao.sync.model.wms.PurchaseOrder.ListOfPurchaseOrders> getListOfPurchaseOrders() {
        return ListOfPurchaseOrders;
    }

    public void setListOfPurchaseOrders(List<br.com.storeautomacao.sync.model.wms.PurchaseOrder.ListOfPurchaseOrders> listOfPurchaseOrders) {
        ListOfPurchaseOrders = listOfPurchaseOrders;
    }
}
