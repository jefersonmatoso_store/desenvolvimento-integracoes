package br.com.storeautomacao.sync.model.wms.Verified;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("ib_shipment")
public class VerifiedIbShipment {

    private VerifiedIbShipmentHdr ib_shipment_hdr = new VerifiedIbShipmentHdr();

    private VerifiedIbShipmentDtl ib_shipment_dtl = new VerifiedIbShipmentDtl();
}
