package br.com.storeautomacao.sync.model.wms;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("order_hdr")
public class OrderHdr {

    private String cust_field_2;

    private String cust_field_1;

    private String cust_field_4;

    private String cust_field_3;

    private String cust_field_5;

    private String route_nbr;

    private String cust_zip;

    private String vas_group_code;

    private String cust_name;

    private String ob_lpn_type;

    private String cust_email;

    private String currency_code;

    private String erp_source_system_ref;

    private String ship_via_code;

    private String group_ref;

    private String ord_date;

    private String ref_nbr;

    private String facility_code;

    private String cust_date_4;

    private String shipto_facility_code;

    private String cust_date_3;

    private String cust_date_2;

    private String cust_date_1;

    private String cust_date_5;

    private String cust_country;

    private String payment_method;

    private String sales_channel;

    private String stop_ship_date;

    private String cust_short_text_6;

    private String cust_short_text_5;

    private String company_code;

    private String cust_short_text_4;

    private String cust_short_text_3;

    private String cust_short_text_9;

    private String cust_phone_nbr;

    private String cust_short_text_8;

    private String cust_city;

    private String cust_short_text_7;

    private String lpn_type_class;

    private String stage_location_barcode;

    private String sched_ship_date;

    private String dest_facility_code;

    private String erp_source_hdr_ref;

    private String cust_short_text_1;

    private String cust_short_text_2;

    private String duties_carrier_account_nbr;

    private String shipto_contact;

    private String customs_broker_contact;

    private String externally_planned_load_flg;

    private String order_nbr_to_replace;

    private String shipto_addr3;

    private String shipto_addr2;

    private String order_nbr;

    private String cust_contact;

    private String shipto_city;

    private String gift_msg;

    private String customer_vendor_code;

    private String action_code;

    private String shipto_addr;

    private String dest_dept_nbr;

    private String dest_company_code;

    private String priority;

    private String cust_nbr;

    private String shipto_zip;

    private String customer_po_nbr;

    private String sales_order_nbr;

    private String shipto_phone_nbr;

    private String req_ship_date;

    private String cust_short_text_12;

    private String start_ship_date;

    private String cust_short_text_10;

    private String cust_short_text_11;

    private String cust_decimal_1;

    private String cust_addr2;

    private String cust_addr3;

    private String shipto_country;

    private String cust_decimal_5;

    private String cust_decimal_4;

    private String cust_decimal_3;

    private String duties_payment_method;

    private String cust_decimal_2;

    private String cust_number_1;

    private String shipto_name;

    private String cust_number_4;

    private String cust_number_5;

    private String cust_number_2;

    private String cust_state;

    private String cust_number_3;

    private String spl_instr;

    private String cust_long_text_3;

    private String cust_addr;

    private String host_allocation_nbr;

    private String customer_po_type;

    private String billto_carrier_account_nbr;

    private String exp_date;

    private String carrier_account_nbr;

    private String shipto_email;

    private String shipto_state;

    private String cust_long_text_1;

    private String cust_long_text_2;

    private String order_type;

    public String getCust_field_2() {
        return cust_field_2;
    }

    public void setCust_field_2(String cust_field_2) {
        this.cust_field_2 = cust_field_2;
    }

    public String getCust_field_1() {
        return cust_field_1;
    }

    public void setCust_field_1(String cust_field_1) {
        this.cust_field_1 = cust_field_1;
    }

    public String getCust_field_4() {
        return cust_field_4;
    }

    public void setCust_field_4(String cust_field_4) {
        this.cust_field_4 = cust_field_4;
    }

    public String getCust_field_3() {
        return cust_field_3;
    }

    public void setCust_field_3(String cust_field_3) {
        this.cust_field_3 = cust_field_3;
    }

    public String getCust_field_5() {
        return cust_field_5;
    }

    public void setCust_field_5(String cust_field_5) {
        this.cust_field_5 = cust_field_5;
    }

    public String getRoute_nbr() {
        return route_nbr;
    }

    public void setRoute_nbr(String route_nbr) {
        this.route_nbr = route_nbr;
    }

    public String getCust_zip() {
        return cust_zip;
    }

    public void setCust_zip(String cust_zip) {
        this.cust_zip = cust_zip;
    }

    public String getVas_group_code() {
        return vas_group_code;
    }

    public void setVas_group_code(String vas_group_code) {
        this.vas_group_code = vas_group_code;
    }

    public String getCust_name() {
        return cust_name;
    }

    public void setCust_name(String cust_name) {
        this.cust_name = cust_name;
    }

    public String getOb_lpn_type() {
        return ob_lpn_type;
    }

    public void setOb_lpn_type(String ob_lpn_type) {
        this.ob_lpn_type = ob_lpn_type;
    }

    public String getCust_email() {
        return cust_email;
    }

    public void setCust_email(String cust_email) {
        this.cust_email = cust_email;
    }

    public String getCurrency_code() {
        return currency_code;
    }

    public void setCurrency_code(String currency_code) {
        this.currency_code = currency_code;
    }

    public String getErp_source_system_ref() {
        return erp_source_system_ref;
    }

    public void setErp_source_system_ref(String erp_source_system_ref) {
        this.erp_source_system_ref = erp_source_system_ref;
    }

    public String getShip_via_code() {
        return ship_via_code;
    }

    public void setShip_via_code(String ship_via_code) {
        this.ship_via_code = ship_via_code;
    }

    public String getGroup_ref() {
        return group_ref;
    }

    public void setGroup_ref(String group_ref) {
        this.group_ref = group_ref;
    }

    public String getOrd_date() {
        return ord_date;
    }

    public void setOrd_date(String ord_date) {
        this.ord_date = ord_date;
    }

    public String getRef_nbr() {
        return ref_nbr;
    }

    public void setRef_nbr(String ref_nbr) {
        this.ref_nbr = ref_nbr;
    }

    public String getFacility_code() {
        return facility_code;
    }

    public void setFacility_code(String facility_code) {
        this.facility_code = facility_code;
    }

    public String getCust_date_4() {
        return cust_date_4;
    }

    public void setCust_date_4(String cust_date_4) {
        this.cust_date_4 = cust_date_4;
    }

    public String getShipto_facility_code() {
        return shipto_facility_code;
    }

    public void setShipto_facility_code(String shipto_facility_code) {
        this.shipto_facility_code = shipto_facility_code;
    }

    public String getCust_date_3() {
        return cust_date_3;
    }

    public void setCust_date_3(String cust_date_3) {
        this.cust_date_3 = cust_date_3;
    }

    public String getCust_date_2() {
        return cust_date_2;
    }

    public void setCust_date_2(String cust_date_2) {
        this.cust_date_2 = cust_date_2;
    }

    public String getCust_date_1() {
        return cust_date_1;
    }

    public void setCust_date_1(String cust_date_1) {
        this.cust_date_1 = cust_date_1;
    }

    public String getCust_date_5() {
        return cust_date_5;
    }

    public void setCust_date_5(String cust_date_5) {
        this.cust_date_5 = cust_date_5;
    }

    public String getCust_country() {
        return cust_country;
    }

    public void setCust_country(String cust_country) {
        this.cust_country = cust_country;
    }

    public String getPayment_method() {
        return payment_method;
    }

    public void setPayment_method(String payment_method) {
        this.payment_method = payment_method;
    }

    public String getSales_channel() {
        return sales_channel;
    }

    public void setSales_channel(String sales_channel) {
        this.sales_channel = sales_channel;
    }

    public String getStop_ship_date() {
        return stop_ship_date;
    }

    public void setStop_ship_date(String stop_ship_date) {
        this.stop_ship_date = stop_ship_date;
    }

    public String getCust_short_text_6() {
        return cust_short_text_6;
    }

    public void setCust_short_text_6(String cust_short_text_6) {
        this.cust_short_text_6 = cust_short_text_6;
    }

    public String getCust_short_text_5() {
        return cust_short_text_5;
    }

    public void setCust_short_text_5(String cust_short_text_5) {
        this.cust_short_text_5 = cust_short_text_5;
    }

    public String getCompany_code() {
        return company_code;
    }

    public void setCompany_code(String company_code) {
        this.company_code = company_code;
    }

    public String getCust_short_text_4() {
        return cust_short_text_4;
    }

    public void setCust_short_text_4(String cust_short_text_4) {
        this.cust_short_text_4 = cust_short_text_4;
    }

    public String getCust_short_text_3() {
        return cust_short_text_3;
    }

    public void setCust_short_text_3(String cust_short_text_3) {
        this.cust_short_text_3 = cust_short_text_3;
    }

    public String getCust_short_text_9() {
        return cust_short_text_9;
    }

    public void setCust_short_text_9(String cust_short_text_9) {
        this.cust_short_text_9 = cust_short_text_9;
    }

    public String getCust_phone_nbr() {
        return cust_phone_nbr;
    }

    public void setCust_phone_nbr(String cust_phone_nbr) {
        this.cust_phone_nbr = cust_phone_nbr;
    }

    public String getCust_short_text_8() {
        return cust_short_text_8;
    }

    public void setCust_short_text_8(String cust_short_text_8) {
        this.cust_short_text_8 = cust_short_text_8;
    }

    public String getCust_city() {
        return cust_city;
    }

    public void setCust_city(String cust_city) {
        this.cust_city = cust_city;
    }

    public String getCust_short_text_7() {
        return cust_short_text_7;
    }

    public void setCust_short_text_7(String cust_short_text_7) {
        this.cust_short_text_7 = cust_short_text_7;
    }

    public String getLpn_type_class() {
        return lpn_type_class;
    }

    public void setLpn_type_class(String lpn_type_class) {
        this.lpn_type_class = lpn_type_class;
    }

    public String getStage_location_barcode() {
        return stage_location_barcode;
    }

    public void setStage_location_barcode(String stage_location_barcode) {
        this.stage_location_barcode = stage_location_barcode;
    }

    public String getSched_ship_date() {
        return sched_ship_date;
    }

    public void setSched_ship_date(String sched_ship_date) {
        this.sched_ship_date = sched_ship_date;
    }

    public String getDest_facility_code() {
        return dest_facility_code;
    }

    public void setDest_facility_code(String dest_facility_code) {
        this.dest_facility_code = dest_facility_code;
    }

    public String getErp_source_hdr_ref() {
        return erp_source_hdr_ref;
    }

    public void setErp_source_hdr_ref(String erp_source_hdr_ref) {
        this.erp_source_hdr_ref = erp_source_hdr_ref;
    }

    public String getCust_short_text_1() {
        return cust_short_text_1;
    }

    public void setCust_short_text_1(String cust_short_text_1) {
        this.cust_short_text_1 = cust_short_text_1;
    }

    public String getCust_short_text_2() {
        return cust_short_text_2;
    }

    public void setCust_short_text_2(String cust_short_text_2) {
        this.cust_short_text_2 = cust_short_text_2;
    }

    public String getDuties_carrier_account_nbr() {
        return duties_carrier_account_nbr;
    }

    public void setDuties_carrier_account_nbr(String duties_carrier_account_nbr) {
        this.duties_carrier_account_nbr = duties_carrier_account_nbr;
    }

    public String getShipto_contact() {
        return shipto_contact;
    }

    public void setShipto_contact(String shipto_contact) {
        this.shipto_contact = shipto_contact;
    }

    public String getCustoms_broker_contact() {
        return customs_broker_contact;
    }

    public void setCustoms_broker_contact(String customs_broker_contact) {
        this.customs_broker_contact = customs_broker_contact;
    }

    public String getExternally_planned_load_flg() {
        return externally_planned_load_flg;
    }

    public void setExternally_planned_load_flg(String externally_planned_load_flg) {
        this.externally_planned_load_flg = externally_planned_load_flg;
    }

    public String getOrder_nbr_to_replace() {
        return order_nbr_to_replace;
    }

    public void setOrder_nbr_to_replace(String order_nbr_to_replace) {
        this.order_nbr_to_replace = order_nbr_to_replace;
    }

    public String getShipto_addr3() {
        return shipto_addr3;
    }

    public void setShipto_addr3(String shipto_addr3) {
        this.shipto_addr3 = shipto_addr3;
    }

    public String getShipto_addr2() {
        return shipto_addr2;
    }

    public void setShipto_addr2(String shipto_addr2) {
        this.shipto_addr2 = shipto_addr2;
    }

    public String getOrder_nbr() {
        return order_nbr;
    }

    public void setOrder_nbr(String order_nbr) {
        this.order_nbr = order_nbr;
    }

    public String getCust_contact() {
        return cust_contact;
    }

    public void setCust_contact(String cust_contact) {
        this.cust_contact = cust_contact;
    }

    public String getShipto_city() {
        return shipto_city;
    }

    public void setShipto_city(String shipto_city) {
        this.shipto_city = shipto_city;
    }

    public String getGift_msg() {
        return gift_msg;
    }

    public void setGift_msg(String gift_msg) {
        this.gift_msg = gift_msg;
    }

    public String getCustomer_vendor_code() {
        return customer_vendor_code;
    }

    public void setCustomer_vendor_code(String customer_vendor_code) {
        this.customer_vendor_code = customer_vendor_code;
    }

    public String getAction_code() {
        return action_code;
    }

    public void setAction_code(String action_code) {
        this.action_code = action_code;
    }

    public String getShipto_addr() {
        return shipto_addr;
    }

    public void setShipto_addr(String shipto_addr) {
        this.shipto_addr = shipto_addr;
    }

    public String getDest_dept_nbr() {
        return dest_dept_nbr;
    }

    public void setDest_dept_nbr(String dest_dept_nbr) {
        this.dest_dept_nbr = dest_dept_nbr;
    }

    public String getDest_company_code() {
        return dest_company_code;
    }

    public void setDest_company_code(String dest_company_code) {
        this.dest_company_code = dest_company_code;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getCust_nbr() {
        return cust_nbr;
    }

    public void setCust_nbr(String cust_nbr) {
        this.cust_nbr = cust_nbr;
    }

    public String getShipto_zip() {
        return shipto_zip;
    }

    public void setShipto_zip(String shipto_zip) {
        this.shipto_zip = shipto_zip;
    }

    public String getCustomer_po_nbr() {
        return customer_po_nbr;
    }

    public void setCustomer_po_nbr(String customer_po_nbr) {
        this.customer_po_nbr = customer_po_nbr;
    }

    public String getSales_order_nbr() {
        return sales_order_nbr;
    }

    public void setSales_order_nbr(String sales_order_nbr) {
        this.sales_order_nbr = sales_order_nbr;
    }

    public String getShipto_phone_nbr() {
        return shipto_phone_nbr;
    }

    public void setShipto_phone_nbr(String shipto_phone_nbr) {
        this.shipto_phone_nbr = shipto_phone_nbr;
    }

    public String getReq_ship_date() {
        return req_ship_date;
    }

    public void setReq_ship_date(String req_ship_date) {
        this.req_ship_date = req_ship_date;
    }

    public String getCust_short_text_12() {
        return cust_short_text_12;
    }

    public void setCust_short_text_12(String cust_short_text_12) {
        this.cust_short_text_12 = cust_short_text_12;
    }

    public String getStart_ship_date() {
        return start_ship_date;
    }

    public void setStart_ship_date(String start_ship_date) {
        this.start_ship_date = start_ship_date;
    }

    public String getCust_short_text_10() {
        return cust_short_text_10;
    }

    public void setCust_short_text_10(String cust_short_text_10) {
        this.cust_short_text_10 = cust_short_text_10;
    }

    public String getCust_short_text_11() {
        return cust_short_text_11;
    }

    public void setCust_short_text_11(String cust_short_text_11) {
        this.cust_short_text_11 = cust_short_text_11;
    }

    public String getCust_decimal_1() {
        return cust_decimal_1;
    }

    public void setCust_decimal_1(String cust_decimal_1) {
        this.cust_decimal_1 = cust_decimal_1;
    }

    public String getCust_addr2() {
        return cust_addr2;
    }

    public void setCust_addr2(String cust_addr2) {
        this.cust_addr2 = cust_addr2;
    }

    public String getCust_addr3() {
        return cust_addr3;
    }

    public void setCust_addr3(String cust_addr3) {
        this.cust_addr3 = cust_addr3;
    }

    public String getShipto_country() {
        return shipto_country;
    }

    public void setShipto_country(String shipto_country) {
        this.shipto_country = shipto_country;
    }

    public String getCust_decimal_5() {
        return cust_decimal_5;
    }

    public void setCust_decimal_5(String cust_decimal_5) {
        this.cust_decimal_5 = cust_decimal_5;
    }

    public String getCust_decimal_4() {
        return cust_decimal_4;
    }

    public void setCust_decimal_4(String cust_decimal_4) {
        this.cust_decimal_4 = cust_decimal_4;
    }

    public String getCust_decimal_3() {
        return cust_decimal_3;
    }

    public void setCust_decimal_3(String cust_decimal_3) {
        this.cust_decimal_3 = cust_decimal_3;
    }

    public String getDuties_payment_method() {
        return duties_payment_method;
    }

    public void setDuties_payment_method(String duties_payment_method) {
        this.duties_payment_method = duties_payment_method;
    }

    public String getCust_decimal_2() {
        return cust_decimal_2;
    }

    public void setCust_decimal_2(String cust_decimal_2) {
        this.cust_decimal_2 = cust_decimal_2;
    }

    public String getCust_number_1() {
        return cust_number_1;
    }

    public void setCust_number_1(String cust_number_1) {
        this.cust_number_1 = cust_number_1;
    }

    public String getShipto_name() {
        return shipto_name;
    }

    public void setShipto_name(String shipto_name) {
        this.shipto_name = shipto_name;
    }

    public String getCust_number_4() {
        return cust_number_4;
    }

    public void setCust_number_4(String cust_number_4) {
        this.cust_number_4 = cust_number_4;
    }

    public String getCust_number_5() {
        return cust_number_5;
    }

    public void setCust_number_5(String cust_number_5) {
        this.cust_number_5 = cust_number_5;
    }

    public String getCust_number_2() {
        return cust_number_2;
    }

    public void setCust_number_2(String cust_number_2) {
        this.cust_number_2 = cust_number_2;
    }

    public String getCust_state() {
        return cust_state;
    }

    public void setCust_state(String cust_state) {
        this.cust_state = cust_state;
    }

    public String getCust_number_3() {
        return cust_number_3;
    }

    public void setCust_number_3(String cust_number_3) {
        this.cust_number_3 = cust_number_3;
    }

    public String getSpl_instr() {
        return spl_instr;
    }

    public void setSpl_instr(String spl_instr) {
        this.spl_instr = spl_instr;
    }

    public String getCust_long_text_3() {
        return cust_long_text_3;
    }

    public void setCust_long_text_3(String cust_long_text_3) {
        this.cust_long_text_3 = cust_long_text_3;
    }

    public String getCust_addr() {
        return cust_addr;
    }

    public void setCust_addr(String cust_addr) {
        this.cust_addr = cust_addr;
    }

    public String getHost_allocation_nbr() {
        return host_allocation_nbr;
    }

    public void setHost_allocation_nbr(String host_allocation_nbr) {
        this.host_allocation_nbr = host_allocation_nbr;
    }

    public String getCustomer_po_type() {
        return customer_po_type;
    }

    public void setCustomer_po_type(String customer_po_type) {
        this.customer_po_type = customer_po_type;
    }

    public String getBillto_carrier_account_nbr() {
        return billto_carrier_account_nbr;
    }

    public void setBillto_carrier_account_nbr(String billto_carrier_account_nbr) {
        this.billto_carrier_account_nbr = billto_carrier_account_nbr;
    }

    public String getExp_date() {
        return exp_date;
    }

    public void setExp_date(String exp_date) {
        this.exp_date = exp_date;
    }

    public String getCarrier_account_nbr() {
        return carrier_account_nbr;
    }

    public void setCarrier_account_nbr(String carrier_account_nbr) {
        this.carrier_account_nbr = carrier_account_nbr;
    }

    public String getShipto_email() {
        return shipto_email;
    }

    public void setShipto_email(String shipto_email) {
        this.shipto_email = shipto_email;
    }

    public String getShipto_state() {
        return shipto_state;
    }

    public void setShipto_state(String shipto_state) {
        this.shipto_state = shipto_state;
    }

    public String getCust_long_text_1() {
        return cust_long_text_1;
    }

    public void setCust_long_text_1(String cust_long_text_1) {
        this.cust_long_text_1 = cust_long_text_1;
    }

    public String getCust_long_text_2() {
        return cust_long_text_2;
    }

    public void setCust_long_text_2(String cust_long_text_2) {
        this.cust_long_text_2 = cust_long_text_2;
    }

    public String getOrder_type() {
        return order_type;
    }

    public void setOrder_type(String order_type) {
        this.order_type = order_type;
    }

}
