package br.com.storeautomacao.sync.model.wms.PurchaseOrder;

public class PurchaseOrder {

    private PurchaseOrderHdr purchase_order_hdr;

    private PurchaseOrderDtl purchase_order_dtl;


    public PurchaseOrderHdr getPurchase_order_hdr() {
        return purchase_order_hdr;
    }

    public void setPurchase_order_hdr(PurchaseOrderHdr purchase_order_hdr) {
        this.purchase_order_hdr = purchase_order_hdr;
    }

    public PurchaseOrderDtl getPurchase_order_dtl() {
        return purchase_order_dtl;
    }

    public void setPurchase_order_dtl(PurchaseOrderDtl purchase_order_dtl) {
        this.purchase_order_dtl = purchase_order_dtl;
    }
}
