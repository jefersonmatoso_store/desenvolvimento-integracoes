package br.com.storeautomacao.sync.model.enuns;

/** Códigos de Status do HTTP */
public enum TypePushEnum {

	// Informativa:
	ORDER("ORDER", "order recevied");

	private String nameType;
	private String description;


	private TypePushEnum(String nameType, String description) {
		this.nameType = nameType;
		this.description = description;
	}

	public String getNameType() {
		return nameType;
	}

	public void setNameType(String nameType) {
		this.nameType = nameType;
	}

	public String getDescription() {
		return description;
	}

}