package br.com.storeautomacao.sync.model.entity;

public class MissingCustomerOrder {

    private int idCustomerOrder;
    private String orderNumber;
    private Dock dock;
    private int quantitySKU;
    private int quantityLocations;
    private int quantityVolume;

    public int getIdCustomerOrder() {
        return idCustomerOrder;
    }

    public void setIdCustomerOrder(int idCustomerOrder) {
        this.idCustomerOrder = idCustomerOrder;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public Dock getDock() {
        return dock;
    }

    public void setDock(Dock dock) {
        this.dock = dock;
    }

    public int getQuantitySKU() {
        return quantitySKU;
    }

    public void setQuantitySKU(int quantitySKU) {
        this.quantitySKU = quantitySKU;
    }

    public int getQuantityLocations() {
        return quantityLocations;
    }

    public void setQuantityLocations(int quantityLocations) {
        this.quantityLocations = quantityLocations;
    }

    public int getQuantityVolume() {
        return quantityVolume;
    }

    public void setQuantityVolume(int quantityVolume) {
        this.quantityVolume = quantityVolume;
    }
}
