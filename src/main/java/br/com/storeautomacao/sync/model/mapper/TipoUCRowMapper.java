package br.com.storeautomacao.sync.model.mapper;

import br.com.storeautomacao.sync.model.entity.TipoUc;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class TipoUCRowMapper implements RowMapper<TipoUc> {
    
    public TipoUc mapRow(ResultSet rs, int rowNum) throws SQLException {

        TipoUc tipoUc = new TipoUc();

        tipoUc.setSequenciaIntegracao(rs.getBigDecimal("SEQUENCIAINTEGRACAO"));
        tipoUc.setTipoIntegracao(rs.getBigDecimal("TIPOINTEGRACAO"));
        tipoUc.setComprimentoProduto(rs.getBigDecimal("COMPRIMENTOPRODUTO"));
        tipoUc.setLarguraProduto(rs.getBigDecimal("LARGURAPRODUTO"));
        tipoUc.setAlturaProduto(rs.getBigDecimal("ALTURAPRODUTO"));
        tipoUc.setPesoLiquido(rs.getBigDecimal("PESOLIQUIDO"));

        return tipoUc;
    }

}