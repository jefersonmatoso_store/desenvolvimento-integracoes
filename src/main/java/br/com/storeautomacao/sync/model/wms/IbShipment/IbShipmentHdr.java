package br.com.storeautomacao.sync.model.wms.IbShipment;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("ib_shipment_hdr")
public class IbShipmentHdr {

    private String shipment_nbr;
    private String facility_code;
    private String company_code;
    private String trailer_nbr;
    private String action_code;
    private String ref_nbr;
    private String shipment_type;
    private String load_nbr;
    private String manifest_nbr;
    private String trailer_type;
    private String vendor_info;
    private String origin_info;
    private String origin_code;
    private String orig_shipped_units;
    private String lock_code;
    private String shipped_date;
    private String orig_shipped_lpns;
    private String cust_field_1;
    private String cust_field_2;
    private String cust_field_3;
    private String cust_field_4;
    private String cust_field_5;
    private String sold_to_legal_name;
    private String returned_from_facility_code;

    // Getter Methods

    public String getShipment_nbr() {
        return shipment_nbr;
    }

    public String getFacility_code() {
        return facility_code;
    }

    public String getCompany_code() {
        return company_code;
    }

    public String getTrailer_nbr() {
        return trailer_nbr;
    }

    public String getAction_code() {
        return action_code;
    }

    public String getRef_nbr() {
        return ref_nbr;
    }

    public String getShipment_type() {
        return shipment_type;
    }

    public String getLoad_nbr() {
        return load_nbr;
    }

    public String getManifest_nbr() {
        return manifest_nbr;
    }

    public String getTrailer_type() {
        return trailer_type;
    }

    public String getVendor_info() {
        return vendor_info;
    }

    public String getOrigin_info() {
        return origin_info;
    }

    public String getOrigin_code() {
        return origin_code;
    }

    public String getOrig_shipped_units() {
        return orig_shipped_units;
    }

    public String getLock_code() {
        return lock_code;
    }

    public String getShipped_date() {
        return shipped_date;
    }

    public String getOrig_shipped_lpns() {
        return orig_shipped_lpns;
    }

    public String getCust_field_1() {
        return cust_field_1;
    }

    public String getCust_field_2() {
        return cust_field_2;
    }

    public String getCust_field_3() {
        return cust_field_3;
    }

    public String getCust_field_4() {
        return cust_field_4;
    }

    public String getCust_field_5() {
        return cust_field_5;
    }

    public String getSold_to_legal_name() {
        return sold_to_legal_name;
    }

    public String getReturned_from_facility_code() {
        return returned_from_facility_code;
    }

    // Setter Methods

    public void setShipment_nbr(String shipment_nbr) {
        this.shipment_nbr = shipment_nbr;
    }

    public void setFacility_code(String facility_code) {
        this.facility_code = facility_code;
    }

    public void setCompany_code(String company_code) {
        this.company_code = company_code;
    }

    public void setTrailer_nbr(String trailer_nbr) {
        this.trailer_nbr = trailer_nbr;
    }

    public void setAction_code(String action_code) {
        this.action_code = action_code;
    }

    public void setRef_nbr(String ref_nbr) {
        this.ref_nbr = ref_nbr;
    }

    public void setShipment_type(String shipment_type) {
        this.shipment_type = shipment_type;
    }

    public void setLoad_nbr(String load_nbr) {
        this.load_nbr = load_nbr;
    }

    public void setManifest_nbr(String manifest_nbr) {
        this.manifest_nbr = manifest_nbr;
    }

    public void setTrailer_type(String trailer_type) {
        this.trailer_type = trailer_type;
    }

    public void setVendor_info(String vendor_info) {
        this.vendor_info = vendor_info;
    }

    public void setOrigin_info(String origin_info) {
        this.origin_info = origin_info;
    }

    public void setOrigin_code(String origin_code) {
        this.origin_code = origin_code;
    }

    public void setOrig_shipped_units(String orig_shipped_units) {
        this.orig_shipped_units = orig_shipped_units;
    }

    public void setLock_code(String lock_code) {
        this.lock_code = lock_code;
    }

    public void setShipped_date(String shipped_date) {
        this.shipped_date = shipped_date;
    }

    public void setOrig_shipped_lpns(String orig_shipped_lpns) {
        this.orig_shipped_lpns = orig_shipped_lpns;
    }

    public void setCust_field_1(String cust_field_1) {
        this.cust_field_1 = cust_field_1;
    }

    public void setCust_field_2(String cust_field_2) {
        this.cust_field_2 = cust_field_2;
    }

    public void setCust_field_3(String cust_field_3) {
        this.cust_field_3 = cust_field_3;
    }

    public void setCust_field_4(String cust_field_4) {
        this.cust_field_4 = cust_field_4;
    }

    public void setCust_field_5(String cust_field_5) {
        this.cust_field_5 = cust_field_5;
    }

    public void setSold_to_legal_name(String sold_to_legal_name) {
        this.sold_to_legal_name = sold_to_legal_name;
    }

    public void setReturned_from_facility_code(String returned_from_facility_code) {
        this.returned_from_facility_code = returned_from_facility_code;
    }

}
