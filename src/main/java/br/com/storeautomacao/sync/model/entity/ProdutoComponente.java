package br.com.storeautomacao.sync.model.entity;

import java.math.BigDecimal;

public class ProdutoComponente {

    private BigDecimal sequenciaIntegracao;
    private BigDecimal sequenciaComponente;
    private BigDecimal tipoIntegracao;
    private String codigoEmpresa;
    private String codigoProduto;
    private String codigoProdutocomponente;
    private String tipoUcComponente;
    private BigDecimal quantidadeComponente;
    private String numeroRevisao;
    private String descricaoRevisao;

    public ProdutoComponente(){}

    public BigDecimal getSequenciaIntegracao() {
        return sequenciaIntegracao;
    }

    public void setSequenciaIntegracao(BigDecimal sequenciaIntegracao) {
        this.sequenciaIntegracao = sequenciaIntegracao;
    }

    public BigDecimal getSequenciaComponente() {
        return sequenciaComponente;
    }

    public void setSequenciaComponente(BigDecimal sequenciaComponente) {
        this.sequenciaComponente = sequenciaComponente;
    }

    public BigDecimal getTipoIntegracao() {
        return tipoIntegracao;
    }

    public void setTipoIntegracao(BigDecimal tipoIntegracao) {
        this.tipoIntegracao = tipoIntegracao;
    }

    public String getCodigoEmpresa() {
        return codigoEmpresa;
    }

    public void setCodigoEmpresa(String codigoEmpresa) {
        this.codigoEmpresa = codigoEmpresa;
    }

    public String getCodigoProduto() {
        return codigoProduto;
    }

    public void setCodigoProduto(String codigoProduto) {
        this.codigoProduto = codigoProduto;
    }

    public String getCodigoProdutocomponente() {
        return codigoProdutocomponente;
    }

    public void setCodigoProdutocomponente(String codigoProdutocomponente) {
        this.codigoProdutocomponente = codigoProdutocomponente;
    }

    public String getTipoUcComponente() {
        return tipoUcComponente;
    }

    public void setTipoUcComponente(String tipoUcComponente) {
        this.tipoUcComponente = tipoUcComponente;
    }

    public BigDecimal getQuantidadeComponente() {
        return quantidadeComponente;
    }

    public void setQuantidadeComponente(BigDecimal quantidadeComponente) {
        this.quantidadeComponente = quantidadeComponente;
    }

    public String getNumeroRevisao() {
        return numeroRevisao;
    }

    public void setNumeroRevisao(String numeroRevisao) {
        this.numeroRevisao = numeroRevisao;
    }

    public String getDescricaoRevisao() {
        return descricaoRevisao;
    }

    public void setDescricaoRevisao(String descricaoRevisao) {
        this.descricaoRevisao = descricaoRevisao;
    }
}
