package br.com.storeautomacao.sync.model.wms;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.ArrayList;
import java.util.List;

@XStreamAlias("ListOfItems")
public class ListOfItems {

    @XStreamImplicit(itemFieldName="item")
    private List<Item> itens = new ArrayList<Item>();

    public List<Item> getItens() {
        return itens;
    }

    public void setItens(List<Item> itens) {
        this.itens = itens;
    }

   /* private Item item;

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }*/
}
