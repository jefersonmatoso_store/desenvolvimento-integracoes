package br.com.storeautomacao.sync.model.wms;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("inventory_history")
public class Inventory_history
{
    private String ref_code_9;

    private String ref_code_8;

    private String ref_code_7;

    private String ref_code_6;

    private String order_seq_nbr;

    private String create_ts;

    private String units_shipped;

    private String billing_location_type;

    private String create_date;

    private String ref_code_4;

    private String ref_code_5;

    private String ref_code_2;

    private String ref_code_3;

    private String ref_code_1;

    private String reason_code;

    private String invn_attr_n;

    private String invn_attr_o;

    private String facility_code;

    private String invn_attr_l;

    private String item_description;

    private String invn_attr_m;

    private String lpns_shipped;

    private String ref_value_8;

    private String ref_value_9;

    private String ref_value_6;

    private String ref_value_7;

    private String ref_value_4;

    private String ref_value_5;

    private String ref_value_2;

    private String ref_value_3;

    private String shipment_type;

    private String ref_value_1;

    private String ref_value_19;

    private String company_code;

    private String ref_value_17;

    private String ref_value_18;

    private String vendor_code;

    private String seq_nbr;

    private String item_alternate_code;

    private String to_location;

    private String ref_value_20;

    private String order_nbr;

    private String ref_value_10;

    private String serial_nbr;

    private String ref_value_12;

    private String ref_value_11;

    private String ref_value_14;

    private String group_nbr;

    private String ref_value_13;

    private String ref_value_16;

    private String ref_value_15;

    private String location;

    private String lpn_nbr;

    private String item_code;

    private String po_type;

    private String units_received;

    private String item_part_e;

    private String item_part_d;

    private String item_part_f;

    private String work_order_seq_nbr;

    private String item_part_a;

    private String item_part_c;

    private String activity_code;

    private String item_part_b;

    private String po_nbr;

    private String module_name;

    private String shipment_line_nbr;

    private String ref_code_20;

    private String to_facility_code;

    private String invn_attr_a;

    private String invn_attr_b;

    private String invn_attr_c;

    private String work_order_nbr;

    private String invn_attr_h;

    private String invn_attr_i;

    private String invn_attr_j;

    private String invn_attr_k;

    private String invn_attr_d;

    private String invn_attr_e;

    private String po_line_nbr;

    private String invn_attr_f;

    private String invn_attr_g;

    private String lock_code;

    private String orig_qty;

    private String ref_code_10;

    private String ref_code_11;

    private String ref_code_12;

    private String ref_code_13;

    private String trailer_nbr;

    private String ref_code_14;

    private String ref_code_15;

    private String ref_code_16;

    private String ref_code_17;

    private String ref_code_18;

    private String ref_code_19;

    private String shipment_nbr;

    private String screen_name;

    private String lpns_received;

    private String order_type;

    private String adj_qty;

    public String getRef_code_9 ()
    {
        return ref_code_9;
    }

    public void setRef_code_9 (String ref_code_9)
    {
        this.ref_code_9 = ref_code_9;
    }

    public String getRef_code_8 ()
    {
        return ref_code_8;
    }

    public void setRef_code_8 (String ref_code_8)
    {
        this.ref_code_8 = ref_code_8;
    }

    public String getRef_code_7 ()
    {
        return ref_code_7;
    }

    public void setRef_code_7 (String ref_code_7)
    {
        this.ref_code_7 = ref_code_7;
    }

    public String getRef_code_6 ()
    {
        return ref_code_6;
    }

    public void setRef_code_6 (String ref_code_6)
    {
        this.ref_code_6 = ref_code_6;
    }

    public String getOrder_seq_nbr ()
    {
        return order_seq_nbr;
    }

    public void setOrder_seq_nbr (String order_seq_nbr)
    {
        this.order_seq_nbr = order_seq_nbr;
    }

    public String getCreate_ts ()
    {
        return create_ts;
    }

    public void setCreate_ts (String create_ts)
    {
        this.create_ts = create_ts;
    }

    public String getUnits_shipped ()
    {
        return units_shipped;
    }

    public void setUnits_shipped (String units_shipped)
    {
        this.units_shipped = units_shipped;
    }

    public String getBilling_location_type ()
    {
        return billing_location_type;
    }

    public void setBilling_location_type (String billing_location_type)
    {
        this.billing_location_type = billing_location_type;
    }

    public String getCreate_date ()
    {
        return create_date;
    }

    public void setCreate_date (String create_date)
    {
        this.create_date = create_date;
    }

    public String getRef_code_4 ()
    {
        return ref_code_4;
    }

    public void setRef_code_4 (String ref_code_4)
    {
        this.ref_code_4 = ref_code_4;
    }

    public String getRef_code_5 ()
    {
        return ref_code_5;
    }

    public void setRef_code_5 (String ref_code_5)
    {
        this.ref_code_5 = ref_code_5;
    }

    public String getRef_code_2 ()
    {
        return ref_code_2;
    }

    public void setRef_code_2 (String ref_code_2)
    {
        this.ref_code_2 = ref_code_2;
    }

    public String getRef_code_3 ()
    {
        return ref_code_3;
    }

    public void setRef_code_3 (String ref_code_3)
    {
        this.ref_code_3 = ref_code_3;
    }

    public String getRef_code_1 ()
    {
        return ref_code_1;
    }

    public void setRef_code_1 (String ref_code_1)
    {
        this.ref_code_1 = ref_code_1;
    }

    public String getReason_code ()
    {
        return reason_code;
    }

    public void setReason_code (String reason_code)
    {
        this.reason_code = reason_code;
    }

    public String getInvn_attr_n ()
    {
        return invn_attr_n;
    }

    public void setInvn_attr_n (String invn_attr_n)
    {
        this.invn_attr_n = invn_attr_n;
    }

    public String getInvn_attr_o ()
    {
        return invn_attr_o;
    }

    public void setInvn_attr_o (String invn_attr_o)
    {
        this.invn_attr_o = invn_attr_o;
    }

    public String getFacility_code ()
    {
        return facility_code;
    }

    public void setFacility_code (String facility_code)
    {
        this.facility_code = facility_code;
    }

    public String getInvn_attr_l ()
    {
        return invn_attr_l;
    }

    public void setInvn_attr_l (String invn_attr_l)
    {
        this.invn_attr_l = invn_attr_l;
    }

    public String getItem_description ()
    {
        return item_description;
    }

    public void setItem_description (String item_description)
    {
        this.item_description = item_description;
    }

    public String getInvn_attr_m ()
    {
        return invn_attr_m;
    }

    public void setInvn_attr_m (String invn_attr_m)
    {
        this.invn_attr_m = invn_attr_m;
    }

    public String getLpns_shipped ()
    {
        return lpns_shipped;
    }

    public void setLpns_shipped (String lpns_shipped)
    {
        this.lpns_shipped = lpns_shipped;
    }

    public String getRef_value_8 ()
    {
        return ref_value_8;
    }

    public void setRef_value_8 (String ref_value_8)
    {
        this.ref_value_8 = ref_value_8;
    }

    public String getRef_value_9 ()
    {
        return ref_value_9;
    }

    public void setRef_value_9 (String ref_value_9)
    {
        this.ref_value_9 = ref_value_9;
    }

    public String getRef_value_6 ()
    {
        return ref_value_6;
    }

    public void setRef_value_6 (String ref_value_6)
    {
        this.ref_value_6 = ref_value_6;
    }

    public String getRef_value_7 ()
    {
        return ref_value_7;
    }

    public void setRef_value_7 (String ref_value_7)
    {
        this.ref_value_7 = ref_value_7;
    }

    public String getRef_value_4 ()
    {
        return ref_value_4;
    }

    public void setRef_value_4 (String ref_value_4)
    {
        this.ref_value_4 = ref_value_4;
    }

    public String getRef_value_5 ()
    {
        return ref_value_5;
    }

    public void setRef_value_5 (String ref_value_5)
    {
        this.ref_value_5 = ref_value_5;
    }

    public String getRef_value_2 ()
    {
        return ref_value_2;
    }

    public void setRef_value_2 (String ref_value_2)
    {
        this.ref_value_2 = ref_value_2;
    }

    public String getRef_value_3 ()
    {
        return ref_value_3;
    }

    public void setRef_value_3 (String ref_value_3)
    {
        this.ref_value_3 = ref_value_3;
    }

    public String getShipment_type ()
    {
        return shipment_type;
    }

    public void setShipment_type (String shipment_type)
    {
        this.shipment_type = shipment_type;
    }

    public String getRef_value_1 ()
    {
        return ref_value_1;
    }

    public void setRef_value_1 (String ref_value_1)
    {
        this.ref_value_1 = ref_value_1;
    }

    public String getRef_value_19 ()
    {
        return ref_value_19;
    }

    public void setRef_value_19 (String ref_value_19)
    {
        this.ref_value_19 = ref_value_19;
    }

    public String getCompany_code ()
    {
        return company_code;
    }

    public void setCompany_code (String company_code)
    {
        this.company_code = company_code;
    }

    public String getRef_value_17 ()
    {
        return ref_value_17;
    }

    public void setRef_value_17 (String ref_value_17)
    {
        this.ref_value_17 = ref_value_17;
    }

    public String getRef_value_18 ()
    {
        return ref_value_18;
    }

    public void setRef_value_18 (String ref_value_18)
    {
        this.ref_value_18 = ref_value_18;
    }

    public String getVendor_code ()
    {
        return vendor_code;
    }

    public void setVendor_code (String vendor_code)
    {
        this.vendor_code = vendor_code;
    }

    public String getSeq_nbr ()
    {
        return seq_nbr;
    }

    public void setSeq_nbr (String seq_nbr)
    {
        this.seq_nbr = seq_nbr;
    }

    public String getItem_alternate_code ()
    {
        return item_alternate_code;
    }

    public void setItem_alternate_code (String item_alternate_code)
    {
        this.item_alternate_code = item_alternate_code;
    }

    public String getTo_location ()
    {
        return to_location;
    }

    public void setTo_location (String to_location)
    {
        this.to_location = to_location;
    }

    public String getRef_value_20 ()
    {
        return ref_value_20;
    }

    public void setRef_value_20 (String ref_value_20)
    {
        this.ref_value_20 = ref_value_20;
    }

    public String getOrder_nbr ()
    {
        return order_nbr;
    }

    public void setOrder_nbr (String order_nbr)
    {
        this.order_nbr = order_nbr;
    }

    public String getRef_value_10 ()
    {
        return ref_value_10;
    }

    public void setRef_value_10 (String ref_value_10)
    {
        this.ref_value_10 = ref_value_10;
    }

    public String getSerial_nbr ()
    {
        return serial_nbr;
    }

    public void setSerial_nbr (String serial_nbr)
    {
        this.serial_nbr = serial_nbr;
    }

    public String getRef_value_12 ()
    {
        return ref_value_12;
    }

    public void setRef_value_12 (String ref_value_12)
    {
        this.ref_value_12 = ref_value_12;
    }

    public String getRef_value_11 ()
    {
        return ref_value_11;
    }

    public void setRef_value_11 (String ref_value_11)
    {
        this.ref_value_11 = ref_value_11;
    }

    public String getRef_value_14 ()
    {
        return ref_value_14;
    }

    public void setRef_value_14 (String ref_value_14)
    {
        this.ref_value_14 = ref_value_14;
    }

    public String getGroup_nbr ()
    {
        return group_nbr;
    }

    public void setGroup_nbr (String group_nbr)
    {
        this.group_nbr = group_nbr;
    }

    public String getRef_value_13 ()
    {
        return ref_value_13;
    }

    public void setRef_value_13 (String ref_value_13)
    {
        this.ref_value_13 = ref_value_13;
    }

    public String getRef_value_16 ()
    {
        return ref_value_16;
    }

    public void setRef_value_16 (String ref_value_16)
    {
        this.ref_value_16 = ref_value_16;
    }

    public String getRef_value_15 ()
    {
        return ref_value_15;
    }

    public void setRef_value_15 (String ref_value_15)
    {
        this.ref_value_15 = ref_value_15;
    }

    public String getLocation ()
    {
        return location;
    }

    public void setLocation (String location)
    {
        this.location = location;
    }

    public String getLpn_nbr ()
    {
        return lpn_nbr;
    }

    public void setLpn_nbr (String lpn_nbr)
    {
        this.lpn_nbr = lpn_nbr;
    }

    public String getItem_code ()
    {
        return item_code;
    }

    public void setItem_code (String item_code)
    {
        this.item_code = item_code;
    }

    public String getPo_type ()
    {
        return po_type;
    }

    public void setPo_type (String po_type)
    {
        this.po_type = po_type;
    }

    public String getUnits_received ()
    {
        return units_received;
    }

    public void setUnits_received (String units_received)
    {
        this.units_received = units_received;
    }

    public String getItem_part_e ()
    {
        return item_part_e;
    }

    public void setItem_part_e (String item_part_e)
    {
        this.item_part_e = item_part_e;
    }

    public String getItem_part_d ()
    {
        return item_part_d;
    }

    public void setItem_part_d (String item_part_d)
    {
        this.item_part_d = item_part_d;
    }

    public String getItem_part_f ()
    {
        return item_part_f;
    }

    public void setItem_part_f (String item_part_f)
    {
        this.item_part_f = item_part_f;
    }

    public String getWork_order_seq_nbr ()
    {
        return work_order_seq_nbr;
    }

    public void setWork_order_seq_nbr (String work_order_seq_nbr)
    {
        this.work_order_seq_nbr = work_order_seq_nbr;
    }

    public String getItem_part_a ()
    {
        return item_part_a;
    }

    public void setItem_part_a (String item_part_a)
    {
        this.item_part_a = item_part_a;
    }

    public String getItem_part_c ()
    {
        return item_part_c;
    }

    public void setItem_part_c (String item_part_c)
    {
        this.item_part_c = item_part_c;
    }

    public String getActivity_code ()
    {
        return activity_code;
    }

    public void setActivity_code (String activity_code)
    {
        this.activity_code = activity_code;
    }

    public String getItem_part_b ()
    {
        return item_part_b;
    }

    public void setItem_part_b (String item_part_b)
    {
        this.item_part_b = item_part_b;
    }

    public String getPo_nbr ()
    {
        return po_nbr;
    }

    public void setPo_nbr (String po_nbr)
    {
        this.po_nbr = po_nbr;
    }

    public String getModule_name ()
    {
        return module_name;
    }

    public void setModule_name (String module_name)
    {
        this.module_name = module_name;
    }

    public String getShipment_line_nbr ()
    {
        return shipment_line_nbr;
    }

    public void setShipment_line_nbr (String shipment_line_nbr)
    {
        this.shipment_line_nbr = shipment_line_nbr;
    }

    public String getRef_code_20 ()
    {
        return ref_code_20;
    }

    public void setRef_code_20 (String ref_code_20)
    {
        this.ref_code_20 = ref_code_20;
    }

    public String getTo_facility_code ()
    {
        return to_facility_code;
    }

    public void setTo_facility_code (String to_facility_code)
    {
        this.to_facility_code = to_facility_code;
    }

    public String getInvn_attr_a ()
    {
        return invn_attr_a;
    }

    public void setInvn_attr_a (String invn_attr_a)
    {
        this.invn_attr_a = invn_attr_a;
    }

    public String getInvn_attr_b ()
    {
        return invn_attr_b;
    }

    public void setInvn_attr_b (String invn_attr_b)
    {
        this.invn_attr_b = invn_attr_b;
    }

    public String getInvn_attr_c ()
    {
        return invn_attr_c;
    }

    public void setInvn_attr_c (String invn_attr_c)
    {
        this.invn_attr_c = invn_attr_c;
    }

    public String getWork_order_nbr ()
    {
        return work_order_nbr;
    }

    public void setWork_order_nbr (String work_order_nbr)
    {
        this.work_order_nbr = work_order_nbr;
    }

    public String getInvn_attr_h ()
    {
        return invn_attr_h;
    }

    public void setInvn_attr_h (String invn_attr_h)
    {
        this.invn_attr_h = invn_attr_h;
    }

    public String getInvn_attr_i ()
    {
        return invn_attr_i;
    }

    public void setInvn_attr_i (String invn_attr_i)
    {
        this.invn_attr_i = invn_attr_i;
    }

    public String getInvn_attr_j ()
    {
        return invn_attr_j;
    }

    public void setInvn_attr_j (String invn_attr_j)
    {
        this.invn_attr_j = invn_attr_j;
    }

    public String getInvn_attr_k ()
    {
        return invn_attr_k;
    }

    public void setInvn_attr_k (String invn_attr_k)
    {
        this.invn_attr_k = invn_attr_k;
    }

    public String getInvn_attr_d ()
    {
        return invn_attr_d;
    }

    public void setInvn_attr_d (String invn_attr_d)
    {
        this.invn_attr_d = invn_attr_d;
    }

    public String getInvn_attr_e ()
    {
        return invn_attr_e;
    }

    public void setInvn_attr_e (String invn_attr_e)
    {
        this.invn_attr_e = invn_attr_e;
    }

    public String getPo_line_nbr ()
    {
        return po_line_nbr;
    }

    public void setPo_line_nbr (String po_line_nbr)
    {
        this.po_line_nbr = po_line_nbr;
    }

    public String getInvn_attr_f ()
    {
        return invn_attr_f;
    }

    public void setInvn_attr_f (String invn_attr_f)
    {
        this.invn_attr_f = invn_attr_f;
    }

    public String getInvn_attr_g ()
    {
        return invn_attr_g;
    }

    public void setInvn_attr_g (String invn_attr_g)
    {
        this.invn_attr_g = invn_attr_g;
    }

    public String getLock_code ()
    {
        return lock_code;
    }

    public void setLock_code (String lock_code)
    {
        this.lock_code = lock_code;
    }

    public String getOrig_qty ()
    {
        return orig_qty;
    }

    public void setOrig_qty (String orig_qty)
    {
        this.orig_qty = orig_qty;
    }

    public String getRef_code_10 ()
    {
        return ref_code_10;
    }

    public void setRef_code_10 (String ref_code_10)
    {
        this.ref_code_10 = ref_code_10;
    }

    public String getRef_code_11 ()
    {
        return ref_code_11;
    }

    public void setRef_code_11 (String ref_code_11)
    {
        this.ref_code_11 = ref_code_11;
    }

    public String getRef_code_12 ()
    {
        return ref_code_12;
    }

    public void setRef_code_12 (String ref_code_12)
    {
        this.ref_code_12 = ref_code_12;
    }

    public String getRef_code_13 ()
    {
        return ref_code_13;
    }

    public void setRef_code_13 (String ref_code_13)
    {
        this.ref_code_13 = ref_code_13;
    }

    public String getTrailer_nbr ()
    {
        return trailer_nbr;
    }

    public void setTrailer_nbr (String trailer_nbr)
    {
        this.trailer_nbr = trailer_nbr;
    }

    public String getRef_code_14 ()
    {
        return ref_code_14;
    }

    public void setRef_code_14 (String ref_code_14)
    {
        this.ref_code_14 = ref_code_14;
    }

    public String getRef_code_15 ()
    {
        return ref_code_15;
    }

    public void setRef_code_15 (String ref_code_15)
    {
        this.ref_code_15 = ref_code_15;
    }

    public String getRef_code_16 ()
    {
        return ref_code_16;
    }

    public void setRef_code_16 (String ref_code_16)
    {
        this.ref_code_16 = ref_code_16;
    }

    public String getRef_code_17 ()
    {
        return ref_code_17;
    }

    public void setRef_code_17 (String ref_code_17)
    {
        this.ref_code_17 = ref_code_17;
    }

    public String getRef_code_18 ()
    {
        return ref_code_18;
    }

    public void setRef_code_18 (String ref_code_18)
    {
        this.ref_code_18 = ref_code_18;
    }

    public String getRef_code_19 ()
    {
        return ref_code_19;
    }

    public void setRef_code_19 (String ref_code_19)
    {
        this.ref_code_19 = ref_code_19;
    }

    public String getShipment_nbr ()
    {
        return shipment_nbr;
    }

    public void setShipment_nbr (String shipment_nbr)
    {
        this.shipment_nbr = shipment_nbr;
    }

    public String getScreen_name ()
    {
        return screen_name;
    }

    public void setScreen_name (String screen_name)
    {
        this.screen_name = screen_name;
    }

    public String getLpns_received ()
    {
        return lpns_received;
    }

    public void setLpns_received (String lpns_received)
    {
        this.lpns_received = lpns_received;
    }

    public String getOrder_type ()
    {
        return order_type;
    }

    public void setOrder_type (String order_type)
    {
        this.order_type = order_type;
    }

    public String getAdj_qty ()
    {
        return adj_qty;
    }

    public void setAdj_qty (String adj_qty)
    {
        this.adj_qty = adj_qty;
    }


}
