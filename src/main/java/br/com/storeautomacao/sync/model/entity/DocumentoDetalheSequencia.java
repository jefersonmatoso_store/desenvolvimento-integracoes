package br.com.storeautomacao.sync.model.entity;

import java.math.BigDecimal;
import java.util.Date;

public class DocumentoDetalheSequencia {

    private BigDecimal sequenciaIntegracao;
    private BigDecimal sequenciaDocumento;
    private BigDecimal sequenciaDetalhe;
    private BigDecimal sequenciaDetalheSequencia;
    private BigDecimal pesoBruto;
    private BigDecimal pesoLiquido;
    private BigDecimal largura;
    private BigDecimal comprimento;
    private BigDecimal altura;
    private String tipouc;
    private BigDecimal fatortipouc;
    private BigDecimal quantidadeInicial;
    private String classeProduto;
    private String loteFabricacao;
    private Date dataFabricacao;
    private Date dataVencimento;
    private String loteGeral;
    private BigDecimal quantidadeMovimento;
    private String idFracionamento;
    private String codigoEmpresa;
    private String tipoDocumento;
    private String serieDocumento;
    private String numeroDocumento;
    private String especieDocumento;
    private String nfeChaveAcesso;
    private BigDecimal lote;
    private BigDecimal codigoUa;
    private BigDecimal codigoVolume;

    public BigDecimal getSequenciaIntegracao() {
        return sequenciaIntegracao;
    }

    public void setSequenciaIntegracao(BigDecimal sequenciaIntegracao) {
        this.sequenciaIntegracao = sequenciaIntegracao;
    }

    public BigDecimal getSequenciaDocumento() {
        return sequenciaDocumento;
    }

    public void setSequenciaDocumento(BigDecimal sequenciaDocumento) {
        this.sequenciaDocumento = sequenciaDocumento;
    }

    public BigDecimal getSequenciaDetalhe() {
        return sequenciaDetalhe;
    }

    public void setSequenciaDetalhe(BigDecimal sequenciaDetalhe) {
        this.sequenciaDetalhe = sequenciaDetalhe;
    }

    public BigDecimal getSequenciaDetalheSequencia() {
        return sequenciaDetalheSequencia;
    }

    public void setSequenciaDetalheSequencia(BigDecimal sequenciaDetalheSequencia) {
        this.sequenciaDetalheSequencia = sequenciaDetalheSequencia;
    }

    public BigDecimal getPesoBruto() {
        return pesoBruto;
    }

    public void setPesoBruto(BigDecimal pesoBruto) {
        this.pesoBruto = pesoBruto;
    }

    public BigDecimal getPesoLiquido() {
        return pesoLiquido;
    }

    public void setPesoLiquido(BigDecimal pesoLiquido) {
        this.pesoLiquido = pesoLiquido;
    }

    public BigDecimal getLargura() {
        return largura;
    }

    public void setLargura(BigDecimal largura) {
        this.largura = largura;
    }

    public BigDecimal getComprimento() {
        return comprimento;
    }

    public void setComprimento(BigDecimal comprimento) {
        this.comprimento = comprimento;
    }

    public BigDecimal getAltura() {
        return altura;
    }

    public void setAltura(BigDecimal altura) {
        this.altura = altura;
    }

    public String getTipouc() {
        return tipouc;
    }

    public void setTipouc(String tipouc) {
        this.tipouc = tipouc;
    }

    public BigDecimal getFatortipouc() {
        return fatortipouc;
    }

    public void setFatorTipoUc(BigDecimal fatortipouc) {
        this.fatortipouc = fatortipouc;
    }

    public BigDecimal getQuantidadeInicial() {
        return quantidadeInicial;
    }

    public void setQuantidadeInicial(BigDecimal quantidadeInicial) {
        this.quantidadeInicial = quantidadeInicial;
    }

    public String getClasseProduto() {
        return classeProduto;
    }

    public void setClasseProduto(String classeProduto) {
        this.classeProduto = classeProduto;
    }

    public String getLoteFabricacao() {
        return loteFabricacao;
    }

    public void setLoteFabricacao(String loteFabricacao) {
        this.loteFabricacao = loteFabricacao;
    }

    public Date getDataFabricacao() {
        return dataFabricacao;
    }

    public void setDataFabricacao(Date dataFabricacao) {
        this.dataFabricacao = dataFabricacao;
    }

    public Date getDataVencimento() {
        return dataVencimento;
    }

    public void setDataVencimento(Date dataVencimento) {
        this.dataVencimento = dataVencimento;
    }

    public String getLoteGeral() {
        return loteGeral;
    }

    public void setLoteGeral(String loteGeral) {
        this.loteGeral = loteGeral;
    }

    public BigDecimal getQuantidadeMovimento() {
        return quantidadeMovimento;
    }

    public void setQuantidadeMovimento(BigDecimal quantidadeMovimento) {
        this.quantidadeMovimento = quantidadeMovimento;
    }

    public String getIdFracionamento() {
        return idFracionamento;
    }

    public void setIdFracionamento(String idFracionamento) {
        this.idFracionamento = idFracionamento;
    }

    public String getCodigoEmpresa() {
        return codigoEmpresa;
    }

    public void setCodigoEmpresa(String codigoEmpresa) {
        this.codigoEmpresa = codigoEmpresa;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public String getSerieDocumento() {
        return serieDocumento;
    }

    public void setSerieDocumento(String serieDocumento) {
        this.serieDocumento = serieDocumento;
    }

    public String getNumeroDocumento() {
        return numeroDocumento;
    }

    public void setNumeroDocumento(String numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

    public String getEspecieDocumento() {
        return especieDocumento;
    }

    public void setEspecieDocumento(String especieDocumento) {
        this.especieDocumento = especieDocumento;
    }

    public String getNfeChaveAcesso() {
        return nfeChaveAcesso;
    }

    public void setNfeChaveAcesso(String nfeChaveAcesso) {
        this.nfeChaveAcesso = nfeChaveAcesso;
    }

    public BigDecimal getLote() {
        return lote;
    }

    public void setLote(BigDecimal lote) {
        this.lote = lote;
    }

    public BigDecimal getCodigoUa() {
        return codigoUa;
    }

    public void setCodigoUa(BigDecimal codigoUa) {
        this.codigoUa = codigoUa;
    }

    public BigDecimal getCodigoVolume() {
        return codigoVolume;
    }

    public void setCodigoVolume(BigDecimal codigoVolume) {
        this.codigoVolume = codigoVolume;
    }
}
