package br.com.storeautomacao.sync.model.wms.Verified;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.ArrayList;
import java.util.List;

@XStreamAlias("ListOfVerifiedIbShipments")
public class ListOfVerifiedIbShipments {

    @XStreamImplicit(itemFieldName="ib_shipment")
    private List<VerifiedIbShipment> verifiedIbShipments = new ArrayList<VerifiedIbShipment>();

    public List<VerifiedIbShipment> getVerifiedIbShipments() {
        return verifiedIbShipments;
    }

    public void setVerifiedIbShipments(List<VerifiedIbShipment> verifiedIbShipments) {
        this.verifiedIbShipments = verifiedIbShipments;
    }

}
