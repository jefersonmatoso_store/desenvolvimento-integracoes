package br.com.storeautomacao.sync.model.mapper;

import br.com.storeautomacao.sync.model.entity.Produto;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ProdutoRowMapper implements RowMapper<Produto> {

    public Produto mapRow(ResultSet rs, int rowNum) throws SQLException {

        Produto produto = new Produto();

        produto.setSequenciaIntegracao(rs.getBigDecimal("SEQUENCIAINTEGRACAO"));
        produto.setTipoIntegracao(rs.getBigDecimal("TIPOINTEGRACAO"));
        produto.setCodigoEmpresa(rs.getString("CODIGOEMPRESA"));
        produto.setCodigoProduto(rs.getString("CODIGOPRODUTO"));
        produto.setDescricaoProduto(rs.getString("DESCRICAOPRODUTO"));
        produto.setGrupoProduto(rs.getString("GRUPOPRODUTO"));
        produto.setDescricaoGrupoProduto(rs.getString("DESCRICAOGRUPOPRODUTO"));
        produto.setDescricaoProdutoDet(rs.getString("DESCRICAOPRODUTODET"));
        produto.setCodigoLinha(rs.getString("CODIGOLINHA"));
        produto.setDescricaoLinha(rs.getString("DESCRICAOLINHA"));

        return produto;
    }

}