package br.com.storeautomacao.sync.model.entity;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.data.mongodb.core.mapping.Document;

import java.math.BigDecimal;
import java.util.Date;

@Document(collection = "conferencia")
@JsonIgnoreProperties
public class Conferencia {

        private BigDecimal sequenciaIntegracao;
        private String numeroDocumento;   // 3855664654
        private BigDecimal quantidadeMovimento; // 5 das 10 caixas
        private String estadoMercadoria; // ClasseProduto // normal // // lock code( é a situacao que o item chegou no armazem)(Pode estar com avaria, quebrado, violado, etc)
        private Mercadoria mercadoria;
        private String sequenciaDocumentoDetalhe; // item 1 // de 1 a n ( um documento tem n documento detalhe. Se tiver 3 a sequenciaDocumento detalhe vai de 1 a 3)
        private Double peso;
        private Date dataHoraConferencia;

    public BigDecimal getSequenciaIntegracao() {
        return sequenciaIntegracao;
    }

    public void setSequenciaIntegracao(BigDecimal sequenciaIntegracao) {
        this.sequenciaIntegracao = sequenciaIntegracao;
    }

    public String getNumeroDocumento() {
        return numeroDocumento;
    }

    public void setNumeroDocumento(String numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

    public BigDecimal getQuantidadeMovimento() {
        return quantidadeMovimento;
    }

    public void setQuantidadeMovimento(BigDecimal quantidadeMovimento) {
        this.quantidadeMovimento = quantidadeMovimento;
    }

    public String getEstadoMercadoria() {
        return estadoMercadoria;
    }

    public void setEstadoMercadoria(String estadoMercadoria) {
        this.estadoMercadoria = estadoMercadoria;
    }

    public Mercadoria getMercadoria() {
        return mercadoria;
    }

    public void setMercadoria(Mercadoria mercadoria) {
        this.mercadoria = mercadoria;
    }

    public String getSequenciaDocumentoDetalhe() {
        return sequenciaDocumentoDetalhe;
    }

    public void setSequenciaDocumentoDetalhe(String sequenciaDocumentoDetalhe) {
        this.sequenciaDocumentoDetalhe = sequenciaDocumentoDetalhe;
    }

    public Double getPeso() {
        return peso;
    }

    public void setPeso(Double peso) {
        this.peso = peso;
    }

    public Date getDataHoraConferencia() {
        return dataHoraConferencia;
    }

    public void setDataHoraConferencia(Date dataHoraConferencia) {
        this.dataHoraConferencia = dataHoraConferencia;
    }
}
