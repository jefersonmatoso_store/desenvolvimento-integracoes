package br.com.storeautomacao.sync.model.wms.Verified;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("ib_shipment_dtl")
public class VerifiedIbShipmentDtl {

    private String seq_nbr;
    private String lpn_nbr;
    private String lpn_weight;
    private String lpn_volume;
    private String item_alternate_code;
    private String item_part_a;
    private String item_part_b;
    private String item_part_c;
    private String item_part_d;
    private String item_part_e;
    private String item_part_f;
    private String pre_pack_code;
    private String pre_pack_ratio;
    private String pre_pack_ratio_seq;
    private String pre_pack_total_units;
    private String invn_attr_a;
    private String invn_attr_b;
    private String invn_attr_c;
    private String shipped_qty;
    private String priority_date;
    private String po_nbr;
    private String pallet_nbr;
    private String putaway_type;
    private String received_qty;
    private String expiry_date;
    private String batch_nbr;
    private String recv_xdock_facility_code;
    private String shipment_dtl_cust_field_1;
    private String shipment_dtl_cust_field_2;
    private String shipment_dtl_cust_field_3;
    private String shipment_dtl_cust_field_4;
    private String shipment_dtl_cust_field_5;
    private String lpn_is_physical_pallet_flg;
    private String po_seq_nbr;
    private String lock_code;
    private String serial_nbr;
    private String invn_attr_d;
    private String invn_attr_e;
    private String invn_attr_f;
    private String invn_attr_g;
    private String rcvd_trailer_nbr;
    private String po_dtl_line_schedule_nbrs;
    private String ref_order_nbr;
    private String ref_order_seq_nbr;
    private String invn_attr_h;
    private String invn_attr_i;
    private String invn_attr_j;
    private String invn_attr_k;
    private String invn_attr_l;
    private String invn_attr_m;
    private String invn_attr_n;
    private String invn_attr_o;

    public String getSeq_nbr() {
        return seq_nbr;
    }

    public void setSeq_nbr(String seq_nbr) {
        this.seq_nbr = seq_nbr;
    }

    public String getLpn_nbr() {
        return lpn_nbr;
    }

    public void setLpn_nbr(String lpn_nbr) {
        this.lpn_nbr = lpn_nbr;
    }

    public String getLpn_weight() {
        return lpn_weight;
    }

    public void setLpn_weight(String lpn_weight) {
        this.lpn_weight = lpn_weight;
    }

    public String getLpn_volume() {
        return lpn_volume;
    }

    public void setLpn_volume(String lpn_volume) {
        this.lpn_volume = lpn_volume;
    }

    public String getItem_alternate_code() {
        return item_alternate_code;
    }

    public void setItem_alternate_code(String item_alternate_code) {
        this.item_alternate_code = item_alternate_code;
    }

    public String getItem_part_a() {
        return item_part_a;
    }

    public void setItem_part_a(String item_part_a) {
        this.item_part_a = item_part_a;
    }

    public String getItem_part_b() {
        return item_part_b;
    }

    public void setItem_part_b(String item_part_b) {
        this.item_part_b = item_part_b;
    }

    public String getItem_part_c() {
        return item_part_c;
    }

    public void setItem_part_c(String item_part_c) {
        this.item_part_c = item_part_c;
    }

    public String getItem_part_d() {
        return item_part_d;
    }

    public void setItem_part_d(String item_part_d) {
        this.item_part_d = item_part_d;
    }

    public String getItem_part_e() {
        return item_part_e;
    }

    public void setItem_part_e(String item_part_e) {
        this.item_part_e = item_part_e;
    }

    public String getItem_part_f() {
        return item_part_f;
    }

    public void setItem_part_f(String item_part_f) {
        this.item_part_f = item_part_f;
    }

    public String getPre_pack_code() {
        return pre_pack_code;
    }

    public void setPre_pack_code(String pre_pack_code) {
        this.pre_pack_code = pre_pack_code;
    }

    public String getPre_pack_ratio() {
        return pre_pack_ratio;
    }

    public void setPre_pack_ratio(String pre_pack_ratio) {
        this.pre_pack_ratio = pre_pack_ratio;
    }

    public String getPre_pack_ratio_seq() {
        return pre_pack_ratio_seq;
    }

    public void setPre_pack_ratio_seq(String pre_pack_ratio_seq) {
        this.pre_pack_ratio_seq = pre_pack_ratio_seq;
    }

    public String getPre_pack_total_units() {
        return pre_pack_total_units;
    }

    public void setPre_pack_total_units(String pre_pack_total_units) {
        this.pre_pack_total_units = pre_pack_total_units;
    }

    public String getInvn_attr_a() {
        return invn_attr_a;
    }

    public void setInvn_attr_a(String invn_attr_a) {
        this.invn_attr_a = invn_attr_a;
    }

    public String getInvn_attr_b() {
        return invn_attr_b;
    }

    public void setInvn_attr_b(String invn_attr_b) {
        this.invn_attr_b = invn_attr_b;
    }

    public String getInvn_attr_c() {
        return invn_attr_c;
    }

    public void setInvn_attr_c(String invn_attr_c) {
        this.invn_attr_c = invn_attr_c;
    }

    public String getShipped_qty() {
        return shipped_qty;
    }

    public void setShipped_qty(String shipped_qty) {
        this.shipped_qty = shipped_qty;
    }

    public String getPriority_date() {
        return priority_date;
    }

    public void setPriority_date(String priority_date) {
        this.priority_date = priority_date;
    }

    public String getPo_nbr() {
        return po_nbr;
    }

    public void setPo_nbr(String po_nbr) {
        this.po_nbr = po_nbr;
    }

    public String getPallet_nbr() {
        return pallet_nbr;
    }

    public void setPallet_nbr(String pallet_nbr) {
        this.pallet_nbr = pallet_nbr;
    }

    public String getPutaway_type() {
        return putaway_type;
    }

    public void setPutaway_type(String putaway_type) {
        this.putaway_type = putaway_type;
    }

    public String getReceived_qty() {
        return received_qty;
    }

    public void setReceived_qty(String received_qty) {
        this.received_qty = received_qty;
    }

    public String getExpiry_date() {
        return expiry_date;
    }

    public void setExpiry_date(String expiry_date) {
        this.expiry_date = expiry_date;
    }

    public String getBatch_nbr() {
        return batch_nbr;
    }

    public void setBatch_nbr(String batch_nbr) {
        this.batch_nbr = batch_nbr;
    }

    public String getRecv_xdock_facility_code() {
        return recv_xdock_facility_code;
    }

    public void setRecv_xdock_facility_code(String recv_xdock_facility_code) {
        this.recv_xdock_facility_code = recv_xdock_facility_code;
    }

    public String getShipment_dtl_cust_field_1() {
        return shipment_dtl_cust_field_1;
    }

    public void setShipment_dtl_cust_field_1(String shipment_dtl_cust_field_1) {
        this.shipment_dtl_cust_field_1 = shipment_dtl_cust_field_1;
    }

    public String getShipment_dtl_cust_field_2() {
        return shipment_dtl_cust_field_2;
    }

    public void setShipment_dtl_cust_field_2(String shipment_dtl_cust_field_2) {
        this.shipment_dtl_cust_field_2 = shipment_dtl_cust_field_2;
    }

    public String getShipment_dtl_cust_field_3() {
        return shipment_dtl_cust_field_3;
    }

    public void setShipment_dtl_cust_field_3(String shipment_dtl_cust_field_3) {
        this.shipment_dtl_cust_field_3 = shipment_dtl_cust_field_3;
    }

    public String getShipment_dtl_cust_field_4() {
        return shipment_dtl_cust_field_4;
    }

    public void setShipment_dtl_cust_field_4(String shipment_dtl_cust_field_4) {
        this.shipment_dtl_cust_field_4 = shipment_dtl_cust_field_4;
    }

    public String getShipment_dtl_cust_field_5() {
        return shipment_dtl_cust_field_5;
    }

    public void setShipment_dtl_cust_field_5(String shipment_dtl_cust_field_5) {
        this.shipment_dtl_cust_field_5 = shipment_dtl_cust_field_5;
    }

    public String getLpn_is_physical_pallet_flg() {
        return lpn_is_physical_pallet_flg;
    }

    public void setLpn_is_physical_pallet_flg(String lpn_is_physical_pallet_flg) {
        this.lpn_is_physical_pallet_flg = lpn_is_physical_pallet_flg;
    }

    public String getPo_seq_nbr() {
        return po_seq_nbr;
    }

    public void setPo_seq_nbr(String po_seq_nbr) {
        this.po_seq_nbr = po_seq_nbr;
    }

    public String getLock_code() {
        return lock_code;
    }

    public void setLock_code(String lock_code) {
        this.lock_code = lock_code;
    }

    public String getSerial_nbr() {
        return serial_nbr;
    }

    public void setSerial_nbr(String serial_nbr) {
        this.serial_nbr = serial_nbr;
    }

    public String getInvn_attr_d() {
        return invn_attr_d;
    }

    public void setInvn_attr_d(String invn_attr_d) {
        this.invn_attr_d = invn_attr_d;
    }

    public String getInvn_attr_e() {
        return invn_attr_e;
    }

    public void setInvn_attr_e(String invn_attr_e) {
        this.invn_attr_e = invn_attr_e;
    }

    public String getInvn_attr_f() {
        return invn_attr_f;
    }

    public void setInvn_attr_f(String invn_attr_f) {
        this.invn_attr_f = invn_attr_f;
    }

    public String getInvn_attr_g() {
        return invn_attr_g;
    }

    public void setInvn_attr_g(String invn_attr_g) {
        this.invn_attr_g = invn_attr_g;
    }

    public String getRcvd_trailer_nbr() {
        return rcvd_trailer_nbr;
    }

    public void setRcvd_trailer_nbr(String rcvd_trailer_nbr) {
        this.rcvd_trailer_nbr = rcvd_trailer_nbr;
    }

    public String getPo_dtl_line_schedule_nbrs() {
        return po_dtl_line_schedule_nbrs;
    }

    public void setPo_dtl_line_schedule_nbrs(String po_dtl_line_schedule_nbrs) {
        this.po_dtl_line_schedule_nbrs = po_dtl_line_schedule_nbrs;
    }

    public String getRef_order_nbr() {
        return ref_order_nbr;
    }

    public void setRef_order_nbr(String ref_order_nbr) {
        this.ref_order_nbr = ref_order_nbr;
    }

    public String getRef_order_seq_nbr() {
        return ref_order_seq_nbr;
    }

    public void setRef_order_seq_nbr(String ref_order_seq_nbr) {
        this.ref_order_seq_nbr = ref_order_seq_nbr;
    }

    public String getInvn_attr_h() {
        return invn_attr_h;
    }

    public void setInvn_attr_h(String invn_attr_h) {
        this.invn_attr_h = invn_attr_h;
    }

    public String getInvn_attr_i() {
        return invn_attr_i;
    }

    public void setInvn_attr_i(String invn_attr_i) {
        this.invn_attr_i = invn_attr_i;
    }

    public String getInvn_attr_j() {
        return invn_attr_j;
    }

    public void setInvn_attr_j(String invn_attr_j) {
        this.invn_attr_j = invn_attr_j;
    }

    public String getInvn_attr_k() {
        return invn_attr_k;
    }

    public void setInvn_attr_k(String invn_attr_k) {
        this.invn_attr_k = invn_attr_k;
    }

    public String getInvn_attr_l() {
        return invn_attr_l;
    }

    public void setInvn_attr_l(String invn_attr_l) {
        this.invn_attr_l = invn_attr_l;
    }

    public String getInvn_attr_m() {
        return invn_attr_m;
    }

    public void setInvn_attr_m(String invn_attr_m) {
        this.invn_attr_m = invn_attr_m;
    }

    public String getInvn_attr_n() {
        return invn_attr_n;
    }

    public void setInvn_attr_n(String invn_attr_n) {
        this.invn_attr_n = invn_attr_n;
    }

    public String getInvn_attr_o() {
        return invn_attr_o;
    }

    public void setInvn_attr_o(String invn_attr_o) {
        this.invn_attr_o = invn_attr_o;
    }
}
