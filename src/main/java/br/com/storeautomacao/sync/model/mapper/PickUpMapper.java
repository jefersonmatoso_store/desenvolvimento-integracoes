package br.com.storeautomacao.sync.model.mapper;

import br.com.storeautomacao.sync.model.entity.PickUp;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class PickUpMapper implements RowMapper<PickUp> {
    @Override
    public PickUp mapRow(ResultSet rs, int rowNum) throws SQLException {

        PickUp p = new PickUp();
        p.setNumeroOrdem(rs.getString("numero_ordem"));
        p.setStatus(rs.getString("status"));


        return p;
    }
}
