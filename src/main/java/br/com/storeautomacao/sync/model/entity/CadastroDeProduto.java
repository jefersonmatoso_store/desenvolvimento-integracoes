package br.com.storeautomacao.sync.model.entity;

import java.util.ArrayList;

public class CadastroDeProduto {
    private String codigoEmpresa;
    private String codigoProduto;
    private String descricaoProduto;
    private String classeProduto;
    TipoUc TipoUcObject;
    private float aliquotaReducaoICMS;
    private float aliquotaSeguroCusto;
    private float estoqueMaximo;
    private float estoqueMinimo;
    private float loteLogisticoDias;
    private String usuario;
    private boolean ativo;
    private float pesoBruto;
    private float precoCusto;
    private String dataAtualizacao;
    private float pesoProduto;
    private float comprimento;
    private float altura;
    private float largura;
    private String descricaoProduto2;
    private float aliquotaIcms;
    private float fatorBaseIcms;
    private float aliquotaIpi;
    private float fatorBaseIpi;
    private float prazoValidade;
    ArrayList< Object > produtoRevisao = new ArrayList < Object > ();


    // Getter Methods

    public String getCodigoEmpresa() {
        return codigoEmpresa;
    }

    public String getCodigoProduto() {
        return codigoProduto;
    }

    public String getDescricaoProduto() {
        return descricaoProduto;
    }

    public String getClasseProduto() {
        return classeProduto;
    }

    public TipoUc getTipoUc() {
        return TipoUcObject;
    }

    public float getAliquotaReducaoICMS() {
        return aliquotaReducaoICMS;
    }

    public float getAliquotaSeguroCusto() {
        return aliquotaSeguroCusto;
    }

    public float getEstoqueMaximo() {
        return estoqueMaximo;
    }

    public float getEstoqueMinimo() {
        return estoqueMinimo;
    }

    public float getLoteLogisticoDias() {
        return loteLogisticoDias;
    }

    public String getUsuario() {
        return usuario;
    }

    public boolean getAtivo() {
        return ativo;
    }

    public float getPesoBruto() {
        return pesoBruto;
    }

    public float getPrecoCusto() {
        return precoCusto;
    }

    public String getDataAtualizacao() {
        return dataAtualizacao;
    }

    public float getPesoProduto() {
        return pesoProduto;
    }

    public float getComprimento() {
        return comprimento;
    }

    public float getAltura() {
        return altura;
    }

    public float getLargura() {
        return largura;
    }

    public String getDescricaoProduto2() {
        return descricaoProduto2;
    }

    public float getAliquotaIcms() {
        return aliquotaIcms;
    }

    public float getFatorBaseIcms() {
        return fatorBaseIcms;
    }

    public float getAliquotaIpi() {
        return aliquotaIpi;
    }

    public float getFatorBaseIpi() {
        return fatorBaseIpi;
    }

    public float getPrazoValidade() {
        return prazoValidade;
    }

    // Setter Methods

    public void setCodigoEmpresa(String codigoEmpresa) {
        this.codigoEmpresa = codigoEmpresa;
    }

    public void setCodigoProduto(String codigoProduto) {
        this.codigoProduto = codigoProduto;
    }

    public void setDescricaoProduto(String descricaoProduto) {
        this.descricaoProduto = descricaoProduto;
    }

    public void setClasseProduto(String classeProduto) {
        this.classeProduto = classeProduto;
    }

    public void setTipoUc(TipoUc tipoUcObject) {
        this.TipoUcObject = tipoUcObject;
    }

    public void setAliquotaReducaoICMS(float aliquotaReducaoICMS) {
        this.aliquotaReducaoICMS = aliquotaReducaoICMS;
    }

    public void setAliquotaSeguroCusto(float aliquotaSeguroCusto) {
        this.aliquotaSeguroCusto = aliquotaSeguroCusto;
    }

    public void setEstoqueMaximo(float estoqueMaximo) {
        this.estoqueMaximo = estoqueMaximo;
    }

    public void setEstoqueMinimo(float estoqueMinimo) {
        this.estoqueMinimo = estoqueMinimo;
    }

    public void setLoteLogisticoDias(float loteLogisticoDias) {
        this.loteLogisticoDias = loteLogisticoDias;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public void setPesoBruto(float pesoBruto) {
        this.pesoBruto = pesoBruto;
    }

    public void setPrecoCusto(float precoCusto) {
        this.precoCusto = precoCusto;
    }

    public void setDataAtualizacao(String dataAtualizacao) {
        this.dataAtualizacao = dataAtualizacao;
    }

    public void setPesoProduto(float pesoProduto) {
        this.pesoProduto = pesoProduto;
    }

    public void setComprimento(float comprimento) {
        this.comprimento = comprimento;
    }

    public void setAltura(float altura) {
        this.altura = altura;
    }

    public void setLargura(float largura) {
        this.largura = largura;
    }

    public void setDescricaoProduto2(String descricaoProduto2) {
        this.descricaoProduto2 = descricaoProduto2;
    }

    public void setAliquotaIcms(float aliquotaIcms) {
        this.aliquotaIcms = aliquotaIcms;
    }

    public void setFatorBaseIcms(float fatorBaseIcms) {
        this.fatorBaseIcms = fatorBaseIcms;
    }

    public void setAliquotaIpi(float aliquotaIpi) {
        this.aliquotaIpi = aliquotaIpi;
    }

    public void setFatorBaseIpi(float fatorBaseIpi) {
        this.fatorBaseIpi = fatorBaseIpi;
    }

    public void setPrazoValidade(float prazoValidade) {
        this.prazoValidade = prazoValidade;
    }
}
