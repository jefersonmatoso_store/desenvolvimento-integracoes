package br.com.storeautomacao.sync.model.wms.Verified;

public class VerifiedIbShipmentHdr {
    private String shipment_nbr;
    private String facility_code;
    private String company_code;
    private String trailer_nbr;
    private String ref_nbr;
    private String shipment_type;
    private String load_nbr;
    private String manifest_nbr;
    private String trailer_type;
    private String vendor_info;
    private String origin_info;
    private String origin_code;
    private String orig_shipped_units;
    private String shipped_date;
    private String orig_shipped_lpns;
    private String shipment_hdr_cust_field_1;
    private String shipment_hdr_cust_field_2;
    private String shipment_hdr_cust_field_3;
    private String shipment_hdr_cust_field_4;
    private String shipment_hdr_cust_field_5;
    private String verification_date;

    public String getShipment_nbr() {
        return shipment_nbr;
    }

    public void setShipment_nbr(String shipment_nbr) {
        this.shipment_nbr = shipment_nbr;
    }

    public String getFacility_code() {
        return facility_code;
    }

    public void setFacility_code(String facility_code) {
        this.facility_code = facility_code;
    }

    public String getCompany_code() {
        return company_code;
    }

    public void setCompany_code(String company_code) {
        this.company_code = company_code;
    }

    public String getTrailer_nbr() {
        return trailer_nbr;
    }

    public void setTrailer_nbr(String trailer_nbr) {
        this.trailer_nbr = trailer_nbr;
    }

    public String getRef_nbr() {
        return ref_nbr;
    }

    public void setRef_nbr(String ref_nbr) {
        this.ref_nbr = ref_nbr;
    }

    public String getShipment_type() {
        return shipment_type;
    }

    public void setShipment_type(String shipment_type) {
        this.shipment_type = shipment_type;
    }

    public String getLoad_nbr() {
        return load_nbr;
    }

    public void setLoad_nbr(String load_nbr) {
        this.load_nbr = load_nbr;
    }

    public String getManifest_nbr() {
        return manifest_nbr;
    }

    public void setManifest_nbr(String manifest_nbr) {
        this.manifest_nbr = manifest_nbr;
    }

    public String getTrailer_type() {
        return trailer_type;
    }

    public void setTrailer_type(String trailer_type) {
        this.trailer_type = trailer_type;
    }

    public String getVendor_info() {
        return vendor_info;
    }

    public void setVendor_info(String vendor_info) {
        this.vendor_info = vendor_info;
    }

    public String getOrigin_info() {
        return origin_info;
    }

    public void setOrigin_info(String origin_info) {
        this.origin_info = origin_info;
    }

    public String getOrigin_code() {
        return origin_code;
    }

    public void setOrigin_code(String origin_code) {
        this.origin_code = origin_code;
    }

    public String getOrig_shipped_units() {
        return orig_shipped_units;
    }

    public void setOrig_shipped_units(String orig_shipped_units) {
        this.orig_shipped_units = orig_shipped_units;
    }

    public String getShipped_date() {
        return shipped_date;
    }

    public void setShipped_date(String shipped_date) {
        this.shipped_date = shipped_date;
    }

    public String getOrig_shipped_lpns() {
        return orig_shipped_lpns;
    }

    public void setOrig_shipped_lpns(String orig_shipped_lpns) {
        this.orig_shipped_lpns = orig_shipped_lpns;
    }

    public String getShipment_hdr_cust_field_1() {
        return shipment_hdr_cust_field_1;
    }

    public void setShipment_hdr_cust_field_1(String shipment_hdr_cust_field_1) {
        this.shipment_hdr_cust_field_1 = shipment_hdr_cust_field_1;
    }

    public String getShipment_hdr_cust_field_2() {
        return shipment_hdr_cust_field_2;
    }

    public void setShipment_hdr_cust_field_2(String shipment_hdr_cust_field_2) {
        this.shipment_hdr_cust_field_2 = shipment_hdr_cust_field_2;
    }

    public String getShipment_hdr_cust_field_3() {
        return shipment_hdr_cust_field_3;
    }

    public void setShipment_hdr_cust_field_3(String shipment_hdr_cust_field_3) {
        this.shipment_hdr_cust_field_3 = shipment_hdr_cust_field_3;
    }

    public String getShipment_hdr_cust_field_4() {
        return shipment_hdr_cust_field_4;
    }

    public void setShipment_hdr_cust_field_4(String shipment_hdr_cust_field_4) {
        this.shipment_hdr_cust_field_4 = shipment_hdr_cust_field_4;
    }

    public String getShipment_hdr_cust_field_5() {
        return shipment_hdr_cust_field_5;
    }

    public void setShipment_hdr_cust_field_5(String shipment_hdr_cust_field_5) {
        this.shipment_hdr_cust_field_5 = shipment_hdr_cust_field_5;
    }

    public String getVerification_date() {
        return verification_date;
    }

    public void setVerification_date(String verification_date) {
        this.verification_date = verification_date;
    }
}
