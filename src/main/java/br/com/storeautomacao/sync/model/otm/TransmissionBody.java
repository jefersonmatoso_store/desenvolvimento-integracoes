package br.com.storeautomacao.sync.model.otm;

public class TransmissionBody {

    private String GLogXMLElement;

    public String getGLogXMLElement ()
    {
        return GLogXMLElement;
    }

    public void setGLogXMLElement (String GLogXMLElement)
    {
        this.GLogXMLElement = GLogXMLElement;
    }

}
