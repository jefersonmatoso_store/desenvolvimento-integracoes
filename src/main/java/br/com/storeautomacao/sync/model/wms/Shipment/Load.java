package br.com.storeautomacao.sync.model.wms.Shipment;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("load")
public class Load {

    private String facility_code;
    private String company_code;
    private String action_code;
    private String load_type;
    private String load_manifest_nbr;
    private String trailer_nbr;
    private String trailer_type;
    private String driver;
    private String seal_nbr;
    private String pro_nbr;
    private String route_nbr;
    private String freight_class;
    private String hdr_bol_nbr;
    private String total_nbr_of_oblpns;
    private String total_weight;
    private String total_volume;
    private String total_shipping_charge;
    private String ship_date;
    private String sched_delivery_date;
    private String carrier_code;
    private String externally_planned_load_nbr;
    private String ship_date_time;
    private String sched_delivery_date_time;
    private String time_zone_code;

    public Load(){}

    public String getFacility_code() {
        return facility_code;
    }

    public void setFacility_code(String facility_code) {
        this.facility_code = facility_code;
    }

    public String getCompany_code() {
        return company_code;
    }

    public void setCompany_code(String company_code) {
        this.company_code = company_code;
    }

    public String getAction_code() {
        return action_code;
    }

    public void setAction_code(String action_code) {
        this.action_code = action_code;
    }

    public String getLoad_type() {
        return load_type;
    }

    public void setLoad_type(String load_type) {
        this.load_type = load_type;
    }

    public String getLoad_manifest_nbr() {
        return load_manifest_nbr;
    }

    public void setLoad_manifest_nbr(String load_manifest_nbr) {
        this.load_manifest_nbr = load_manifest_nbr;
    }

    public String getTrailer_nbr() {
        return trailer_nbr;
    }

    public void setTrailer_nbr(String trailer_nbr) {
        this.trailer_nbr = trailer_nbr;
    }

    public String getTrailer_type() {
        return trailer_type;
    }

    public void setTrailer_type(String trailer_type) {
        this.trailer_type = trailer_type;
    }

    public String getDriver() {
        return driver;
    }

    public void setDriver(String driver) {
        this.driver = driver;
    }

    public String getSeal_nbr() {
        return seal_nbr;
    }

    public void setSeal_nbr(String seal_nbr) {
        this.seal_nbr = seal_nbr;
    }

    public String getPro_nbr() {
        return pro_nbr;
    }

    public void setPro_nbr(String pro_nbr) {
        this.pro_nbr = pro_nbr;
    }

    public String getRoute_nbr() {
        return route_nbr;
    }

    public void setRoute_nbr(String route_nbr) {
        this.route_nbr = route_nbr;
    }

    public String getFreight_class() {
        return freight_class;
    }

    public void setFreight_class(String freight_class) {
        this.freight_class = freight_class;
    }

    public String getHdr_bol_nbr() {
        return hdr_bol_nbr;
    }

    public void setHdr_bol_nbr(String hdr_bol_nbr) {
        this.hdr_bol_nbr = hdr_bol_nbr;
    }

    public String getTotal_nbr_of_oblpns() {
        return total_nbr_of_oblpns;
    }

    public void setTotal_nbr_of_oblpns(String total_nbr_of_oblpns) {
        this.total_nbr_of_oblpns = total_nbr_of_oblpns;
    }

    public String getTotal_weight() {
        return total_weight;
    }

    public void setTotal_weight(String total_weight) {
        this.total_weight = total_weight;
    }

    public String getTotal_volume() {
        return total_volume;
    }

    public void setTotal_volume(String total_volume) {
        this.total_volume = total_volume;
    }

    public String getTotal_shipping_charge() {
        return total_shipping_charge;
    }

    public void setTotal_shipping_charge(String total_shipping_charge) {
        this.total_shipping_charge = total_shipping_charge;
    }

    public String getShip_date() {
        return ship_date;
    }

    public void setShip_date(String ship_date) {
        this.ship_date = ship_date;
    }

    public String getSched_delivery_date() {
        return sched_delivery_date;
    }

    public void setSched_delivery_date(String sched_delivery_date) {
        this.sched_delivery_date = sched_delivery_date;
    }

    public String getCarrier_code() {
        return carrier_code;
    }

    public void setCarrier_code(String carrier_code) {
        this.carrier_code = carrier_code;
    }

    public String getExternally_planned_load_nbr() {
        return externally_planned_load_nbr;
    }

    public void setExternally_planned_load_nbr(String externally_planned_load_nbr) {
        this.externally_planned_load_nbr = externally_planned_load_nbr;
    }

    public String getShip_date_time() {
        return ship_date_time;
    }

    public void setShip_date_time(String ship_date_time) {
        this.ship_date_time = ship_date_time;
    }

    public String getSched_delivery_date_time() {
        return sched_delivery_date_time;
    }

    public void setSched_delivery_date_time(String sched_delivery_date_time) {
        this.sched_delivery_date_time = sched_delivery_date_time;
    }

    public String getTime_zone_code() {
        return time_zone_code;
    }

    public void setTime_zone_code(String time_zone_code) {
        this.time_zone_code = time_zone_code;
    }
}
