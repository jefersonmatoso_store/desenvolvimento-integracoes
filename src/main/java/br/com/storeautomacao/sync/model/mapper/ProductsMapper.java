package br.com.storeautomacao.sync.model.mapper;

import br.com.storeautomacao.sync.model.entity.Products;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ProductsMapper implements RowMapper<Products> {
    @Override
    public Products mapRow(ResultSet rs, int rowNum) throws SQLException {

        Products products = new Products();
        products.setNotUsed("");
        products.setProductId(rs.getString("ProductId"));
        products.setBarcode(rs.getString("Barcode"));
        products.setDescription(rs.getString("Description"));
        products.setGroup(rs.getString("Group"));
        products.setStandardPrice(rs.getString("StandardPrice"));
        products.setSellPrice(rs.getString("SellPrice"));
        products.setDiscount(rs.getString("Discount"));
        products.setContent(rs.getString("Content"));
        products.setUnit(rs.getString("Unit"));

        return products;
    }
}
