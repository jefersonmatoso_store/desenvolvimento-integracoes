package br.com.storeautomacao.sync.model.wms.Verified;

import br.com.storeautomacao.sync.model.wms.Header;
import com.thoughtworks.xstream.annotations.XStreamAlias;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="LgfData")
@XStreamAlias("LgfData")
public class LgfDataVerified {

    private Header Header;

    private ListOfVerifiedIbShipments ListOfVerifiedIbShipments;

    public Header getHeader ()
    {
        return Header;
    }

    public void setHeader (Header Header)
    {
        this.Header = Header;
    }

    public ListOfVerifiedIbShipments getListOfVerifiedIbShipments() {
        return ListOfVerifiedIbShipments;
    }

    public void setListOfVerifiedIbShipments(ListOfVerifiedIbShipments listOfVerifiedIbShipments) {
        ListOfVerifiedIbShipments = listOfVerifiedIbShipments;
    }
}
