package br.com.storeautomacao.sync.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.data.mongodb.core.mapping.Document;

import java.math.BigDecimal;
import java.util.List;

@Document(collection = "mercadoria")
@JsonIgnoreProperties
public class Mercadoria {

    private Documento documento;
    private List<DocumentoDetalhe> documentoDetalhe;
    private BigDecimal sequenciaIntegracao;
    private BigDecimal quantidadeMovimento;
    private String estadoIntegracao;
    private String numeroDocumento;

    public Documento getDocumento() {
        return documento;
    }

    public void setDocumento(Documento documento) {
        this.documento = documento;
    }

    public List<DocumentoDetalhe> getDocumentoDetalhe() {
        return documentoDetalhe;
    }

    public void setDocumentoDetalhe(List<DocumentoDetalhe> documentoDetalhe) {
        this.documentoDetalhe = documentoDetalhe;
    }

    public BigDecimal getSequenciaIntegracao() {
        return sequenciaIntegracao;
    }

    public void setSequenciaIntegracao(BigDecimal sequenciaIntegracao) {
        this.sequenciaIntegracao = sequenciaIntegracao;
    }

    public BigDecimal getQuantidadeMovimento() {
        return quantidadeMovimento;
    }

    public void setQuantidadeMovimento(BigDecimal quantidadeMovimento) {
        this.quantidadeMovimento = quantidadeMovimento;
    }

    public String getEstadoIntegracao() {
        return estadoIntegracao;
    }

    public void setEstadoIntegracao(String estadoIntegracao) {
        this.estadoIntegracao = estadoIntegracao;
    }

    public String getNumeroDocumento() {
        return numeroDocumento;
    }

    public void setNumeroDocumento(String numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }
}
