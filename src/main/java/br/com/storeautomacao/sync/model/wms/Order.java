package br.com.storeautomacao.sync.model.wms;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("order")
public class Order {

    private OrderHdr order_hdr;

    @XStreamImplicit
    private List<OrderDtl> order_dtl;

    public OrderHdr getOrder_hdr() {
        return order_hdr;
    }

    public void setOrder_hdr(OrderHdr order_hdr) {
        this.order_hdr = order_hdr;
    }

    public void setOrder_dtl(List<OrderDtl> order_dtl) {
        this.order_dtl = order_dtl;
    }
}
