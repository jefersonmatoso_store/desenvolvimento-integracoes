package br.com.storeautomacao.sync.model.entity;

public class Sistema {

    private String sistema;
    private String versao;

    public Sistema(String sistema, String versao) {
        this.sistema = sistema;
        this.versao = versao;
    }

    public Sistema() {
    }

    public String getSistema() {
        return sistema;
    }

    public void setSistema(String sistema) {
        this.sistema = sistema;
    }

    public String getVersao() {
        return versao;
    }

    public void setVersao(String versao) {
        this.versao = versao;
    }
}
