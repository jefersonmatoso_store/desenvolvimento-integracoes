package br.com.storeautomacao.sync.model.wms;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import java.math.BigDecimal;

@XStreamAlias("item")
public class Item {

    private String company_code;
    private String item_alternate_code;
    private String part_a;
    private String part_b;
    private String part_c;
    private String part_d;
    private String part_e;
    private String part_f;
    private String pre_pack_code;
    private String action_code;
    private String description;
    private String barcode;
    private String unit_cost;
    private BigDecimal unit_length;
    private BigDecimal unit_width;
    private BigDecimal unit_height;
    private BigDecimal unit_weight;
    private BigDecimal unit_volume;
    private String hazmat;
    private String recv_type;
    private String ob_lpn_type;
    private String catch_weight_method;
    private String order_consolidation_attr;
    private String season_code;
    private String brand_code;
    private String cust_attr_1;
    private String cust_attr_2;
    private String retail_price;
    private String net_cost;
    private String currency_code;
    private String std_pack_qty;
    private String std_pack_lenght;
    private String std_pack_width;
    private String std_pack_height;
    private String std_pack_weight;
    private String std_pack_volume;
    private String std_case_qty;
    private String std_case_lenght;
    private String std_case_width;
    private String std_case_height;
    private String std_case_weight;
    private String std_case_volume;
    private String dimension1;
    private String dimension2;
    private String dimension3;
    private String hierarchy1_code;
    private String hierarchy1_description;
    private String hierarchy2_code;
    private String hierarchy2_description;
    private String hierarchy3_code;
    private String hierarchy3_description;
    private String hierarchy4_code;
    private String hierarchy4_description;
    private String hierarchy5_code;
    private String hierarchy5_description;
    private String group_code;
    private String group_description;
    private String external_style;
    private String vas_group_code;
    private String short_descr;
    private String putaway_type;
    private String conveyable;
    private String stackabillity;
    private String sortable;
    private String min_dispatch;
    private String product_life;
    private String percent_acceptable_product_life;
    private String lpns_per_tier;
    private String tiers_per_pallet;
    private String velocity_code;
    private String req_batch_nbr_flg;
    private String serial_nbr_tracking;
    private String regularity_code;
    private String hamonized_tariff_code;
    private String hamonized_tariff_description;
    private String full_obipn_type;
    private String case_obipn_type;
    private String pack_obipn_type;
    private String description_2;
    private String description_3;
    private String invn_attr_a_tracking;
    private String invn_attr_a_dflt_tracking;
    private String invn_attr_b_tracking;
    private String invn_attr_b_dflt_tracking;
    private String invn_attr_c_tracking;
    private String invn_attr_c_dflt_tracking;
    private String nmfc_code;
    private String conversion_factor;
    private String invn_attr_d_tracking;
    private String invn_attr_e_tracking;
    private String invn_attr_f_tracking;
    private String invn_attr_g_tracking;
    private String host_aware_item_flg;
    private String packing_tolerance_percent;
    private String un_number;
    private String un_class;
    private String un_description;
    private String packing_group;
    private String proper_shipping_name;
    private String excepted_qty_instr;
    private String limited_qty_flg;
    private String fulldg_flg;
    private String hazard_statement;
    private String shipping_temperature_instr;
    private String carrier_commodity_description;
    private String hazard_packaging_description;
    private String shipping_conversion_factor;
    private String shipping_uom;
    private String handle_decimal_qty_flg;
    private String dummy_sku_flg;
    private String pack_with_wave_flg;

    public Item(){}

    public String getCompany_code() {
        return company_code;
    }

    public void setCompany_code(String company_code) {
        this.company_code = company_code;
    }

    public String getItem_alternate_code() {
        return item_alternate_code;
    }

    public void setItem_alternate_code(String item_alternate_code) {
        this.item_alternate_code = item_alternate_code;
    }

    public String getPart_a() {
        return part_a;
    }

    public void setPart_a(String part_a) {
        this.part_a = part_a;
    }

    public String getPart_b() {
        return part_b;
    }

    public void setPart_b(String part_b) {
        this.part_b = part_b;
    }

    public String getPart_c() {
        return part_c;
    }

    public void setPart_c(String part_c) {
        this.part_c = part_c;
    }

    public String getPart_d() {
        return part_d;
    }

    public void setPart_d(String part_d) {
        this.part_d = part_d;
    }

    public String getPart_e() {
        return part_e;
    }

    public void setPart_e(String part_e) {
        this.part_e = part_e;
    }

    public String getPart_f() {
        return part_f;
    }

    public void setPart_f(String part_f) {
        this.part_f = part_f;
    }

    public String getPre_pack_code() {
        return pre_pack_code;
    }

    public void setPre_pack_code(String pre_pack_code) {
        this.pre_pack_code = pre_pack_code;
    }

    public String getAction_code() {
        return action_code;
    }

    public void setAction_code(String action_code) {
        this.action_code = action_code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getUnit_cost() {
        return unit_cost;
    }

    public void setUnit_cost(String unit_cost) {
        this.unit_cost = unit_cost;
    }

    public BigDecimal getUnit_length() {
        return unit_length;
    }

    public void setUnit_length(BigDecimal unit_length) {
        this.unit_length = unit_length;
    }

    public BigDecimal getUnit_width() {
        return unit_width;
    }

    public void setUnit_width(BigDecimal unit_width) {
        this.unit_width = unit_width;
    }

    public BigDecimal getUnit_height() {
        return unit_height;
    }

    public void setUnit_height(BigDecimal unit_height) {
        this.unit_height = unit_height;
    }

    public BigDecimal getUnit_weight() {
        return unit_weight;
    }

    public void setUnit_weight(BigDecimal unit_weight) {
        this.unit_weight = unit_weight;
    }

    public BigDecimal getUnit_volume() {
        return unit_volume;
    }

    public void setUnit_volume(BigDecimal unit_volume) {
        this.unit_volume = unit_volume;
    }

    public String getHazmat() {
        return hazmat;
    }

    public void setHazmat(String hazmat) {
        this.hazmat = hazmat;
    }

    public String getRecv_type() {
        return recv_type;
    }

    public void setRecv_type(String recv_type) {
        this.recv_type = recv_type;
    }

    public String getOb_lpn_type() {
        return ob_lpn_type;
    }

    public void setOb_lpn_type(String ob_lpn_type) {
        this.ob_lpn_type = ob_lpn_type;
    }

    public String getCatch_weight_method() {
        return catch_weight_method;
    }

    public void setCatch_weight_method(String catch_weight_method) {
        this.catch_weight_method = catch_weight_method;
    }

    public String getOrder_consolidation_attr() {
        return order_consolidation_attr;
    }

    public void setOrder_consolidation_attr(String order_consolidation_attr) {
        this.order_consolidation_attr = order_consolidation_attr;
    }

    public String getSeason_code() {
        return season_code;
    }

    public void setSeason_code(String season_code) {
        this.season_code = season_code;
    }

    public String getBrand_code() {
        return brand_code;
    }

    public void setBrand_code(String brand_code) {
        this.brand_code = brand_code;
    }

    public String getCust_attr_1() {
        return cust_attr_1;
    }

    public void setCust_attr_1(String cust_attr_1) {
        this.cust_attr_1 = cust_attr_1;
    }

    public String getCust_attr_2() {
        return cust_attr_2;
    }

    public void setCust_attr_2(String cust_attr_2) {
        this.cust_attr_2 = cust_attr_2;
    }

    public String getRetail_price() {
        return retail_price;
    }

    public void setRetail_price(String retail_price) {
        this.retail_price = retail_price;
    }

    public String getNet_cost() {
        return net_cost;
    }

    public void setNet_cost(String net_cost) {
        this.net_cost = net_cost;
    }

    public String getCurrency_code() {
        return currency_code;
    }

    public void setCurrency_code(String currency_code) {
        this.currency_code = currency_code;
    }

    public String getStd_pack_qty() {
        return std_pack_qty;
    }

    public void setStd_pack_qty(String std_pack_qty) {
        this.std_pack_qty = std_pack_qty;
    }

    public String getStd_pack_lenght() {
        return std_pack_lenght;
    }

    public void setStd_pack_lenght(String std_pack_lenght) {
        this.std_pack_lenght = std_pack_lenght;
    }

    public String getStd_pack_width() {
        return std_pack_width;
    }

    public void setStd_pack_width(String std_pack_width) {
        this.std_pack_width = std_pack_width;
    }

    public String getStd_pack_height() {
        return std_pack_height;
    }

    public void setStd_pack_height(String std_pack_height) {
        this.std_pack_height = std_pack_height;
    }

    public String getStd_pack_weight() {
        return std_pack_weight;
    }

    public void setStd_pack_weight(String std_pack_weight) {
        this.std_pack_weight = std_pack_weight;
    }

    public String getStd_pack_volume() {
        return std_pack_volume;
    }

    public void setStd_pack_volume(String std_pack_volume) {
        this.std_pack_volume = std_pack_volume;
    }

    public String getStd_case_qty() {
        return std_case_qty;
    }

    public void setStd_case_qty(String std_case_qty) {
        this.std_case_qty = std_case_qty;
    }

    public String getStd_case_lenght() {
        return std_case_lenght;
    }

    public void setStd_case_lenght(String std_case_lenght) {
        this.std_case_lenght = std_case_lenght;
    }

    public String getStd_case_width() {
        return std_case_width;
    }

    public void setStd_case_width(String std_case_width) {
        this.std_case_width = std_case_width;
    }

    public String getStd_case_height() {
        return std_case_height;
    }

    public void setStd_case_height(String std_case_height) {
        this.std_case_height = std_case_height;
    }

    public String getStd_case_weight() {
        return std_case_weight;
    }

    public void setStd_case_weight(String std_case_weight) {
        this.std_case_weight = std_case_weight;
    }

    public String getStd_case_volume() {
        return std_case_volume;
    }

    public void setStd_case_volume(String std_case_volume) {
        this.std_case_volume = std_case_volume;
    }

    public String getDimension1() {
        return dimension1;
    }

    public void setDimension1(String dimension1) {
        this.dimension1 = dimension1;
    }

    public String getDimension2() {
        return dimension2;
    }

    public void setDimension2(String dimension2) {
        this.dimension2 = dimension2;
    }

    public String getDimension3() {
        return dimension3;
    }

    public void setDimension3(String dimension3) {
        this.dimension3 = dimension3;
    }

    public String getHierarchy1_code() {
        return hierarchy1_code;
    }

    public void setHierarchy1_code(String hierarchy1_code) {
        this.hierarchy1_code = hierarchy1_code;
    }

    public String getHierarchy1_description() {
        return hierarchy1_description;
    }

    public void setHierarchy1_description(String hierarchy1_description) {
        this.hierarchy1_description = hierarchy1_description;
    }

    public String getHierarchy2_code() {
        return hierarchy2_code;
    }

    public void setHierarchy2_code(String hierarchy2_code) {
        this.hierarchy2_code = hierarchy2_code;
    }

    public String getHierarchy2_description() {
        return hierarchy2_description;
    }

    public void setHierarchy2_description(String hierarchy2_description) {
        this.hierarchy2_description = hierarchy2_description;
    }

    public String getHierarchy3_code() {
        return hierarchy3_code;
    }

    public void setHierarchy3_code(String hierarchy3_code) {
        this.hierarchy3_code = hierarchy3_code;
    }

    public String getHierarchy3_description() {
        return hierarchy3_description;
    }

    public void setHierarchy3_description(String hierarchy3_description) {
        this.hierarchy3_description = hierarchy3_description;
    }

    public String getHierarchy4_code() {
        return hierarchy4_code;
    }

    public void setHierarchy4_code(String hierarchy4_code) {
        this.hierarchy4_code = hierarchy4_code;
    }

    public String getHierarchy4_description() {
        return hierarchy4_description;
    }

    public void setHierarchy4_description(String hierarchy4_description) {
        this.hierarchy4_description = hierarchy4_description;
    }

    public String getHierarchy5_code() {
        return hierarchy5_code;
    }

    public void setHierarchy5_code(String hierarchy5_code) {
        this.hierarchy5_code = hierarchy5_code;
    }

    public String getHierarchy5_description() {
        return hierarchy5_description;
    }

    public void setHierarchy5_description(String hierarchy5_description) {
        this.hierarchy5_description = hierarchy5_description;
    }

    public String getGroup_code() {
        return group_code;
    }

    public void setGroup_code(String group_code) {
        this.group_code = group_code;
    }

    public String getGroup_description() {
        return group_description;
    }

    public void setGroup_description(String group_description) {
        this.group_description = group_description;
    }

    public String getExternal_style() {
        return external_style;
    }

    public void setExternal_style(String external_style) {
        this.external_style = external_style;
    }

    public String getVas_group_code() {
        return vas_group_code;
    }

    public void setVas_group_code(String vas_group_code) {
        this.vas_group_code = vas_group_code;
    }

    public String getShort_descr() {
        return short_descr;
    }

    public void setShort_descr(String short_descr) {
        this.short_descr = short_descr;
    }

    public String getPutaway_type() {
        return putaway_type;
    }

    public void setPutaway_type(String putaway_type) {
        this.putaway_type = putaway_type;
    }

    public String getConveyable() {
        return conveyable;
    }

    public void setConveyable(String conveyable) {
        this.conveyable = conveyable;
    }

    public String getStackabillity() {
        return stackabillity;
    }

    public void setStackabillity(String stackabillity) {
        this.stackabillity = stackabillity;
    }

    public String getSortable() {
        return sortable;
    }

    public void setSortable(String sortable) {
        this.sortable = sortable;
    }

    public String getMin_dispatch() {
        return min_dispatch;
    }

    public void setMin_dispatch(String min_dispatch) {
        this.min_dispatch = min_dispatch;
    }

    public String getProduct_life() {
        return product_life;
    }

    public void setProduct_life(String product_life) {
        this.product_life = product_life;
    }

    public String getPercent_acceptable_product_life() {
        return percent_acceptable_product_life;
    }

    public void setPercent_acceptable_product_life(String percent_acceptable_product_life) {
        this.percent_acceptable_product_life = percent_acceptable_product_life;
    }

    public String getLpns_per_tier() {
        return lpns_per_tier;
    }

    public void setLpns_per_tier(String lpns_per_tier) {
        this.lpns_per_tier = lpns_per_tier;
    }

    public String getTiers_per_pallet() {
        return tiers_per_pallet;
    }

    public void setTiers_per_pallet(String tiers_per_pallet) {
        this.tiers_per_pallet = tiers_per_pallet;
    }

    public String getVelocity_code() {
        return velocity_code;
    }

    public void setVelocity_code(String velocity_code) {
        this.velocity_code = velocity_code;
    }

    public String getReq_batch_nbr_flg() {
        return req_batch_nbr_flg;
    }

    public void setReq_batch_nbr_flg(String req_batch_nbr_flg) {
        this.req_batch_nbr_flg = req_batch_nbr_flg;
    }

    public String getSerial_nbr_tracking() {
        return serial_nbr_tracking;
    }

    public void setSerial_nbr_tracking(String serial_nbr_tracking) {
        this.serial_nbr_tracking = serial_nbr_tracking;
    }

    public String getRegularity_code() {
        return regularity_code;
    }

    public void setRegularity_code(String regularity_code) {
        this.regularity_code = regularity_code;
    }

    public String getHamonized_tariff_code() {
        return hamonized_tariff_code;
    }

    public void setHamonized_tariff_code(String hamonized_tariff_code) {
        this.hamonized_tariff_code = hamonized_tariff_code;
    }

    public String getHamonized_tariff_description() {
        return hamonized_tariff_description;
    }

    public void setHamonized_tariff_description(String hamonized_tariff_description) {
        this.hamonized_tariff_description = hamonized_tariff_description;
    }

    public String getFull_obipn_type() {
        return full_obipn_type;
    }

    public void setFull_obipn_type(String full_obipn_type) {
        this.full_obipn_type = full_obipn_type;
    }

    public String getCase_obipn_type() {
        return case_obipn_type;
    }

    public void setCase_obipn_type(String case_obipn_type) {
        this.case_obipn_type = case_obipn_type;
    }

    public String getPack_obipn_type() {
        return pack_obipn_type;
    }

    public void setPack_obipn_type(String pack_obipn_type) {
        this.pack_obipn_type = pack_obipn_type;
    }

    public String getDescription_2() {
        return description_2;
    }

    public void setDescription_2(String description_2) {
        this.description_2 = description_2;
    }

    public String getDescription_3() {
        return description_3;
    }

    public void setDescription_3(String description_3) {
        this.description_3 = description_3;
    }

    public String getInvn_attr_a_tracking() {
        return invn_attr_a_tracking;
    }

    public void setInvn_attr_a_tracking(String invn_attr_a_tracking) {
        this.invn_attr_a_tracking = invn_attr_a_tracking;
    }

    public String getInvn_attr_a_dflt_tracking() {
        return invn_attr_a_dflt_tracking;
    }

    public void setInvn_attr_a_dflt_tracking(String invn_attr_a_dflt_tracking) {
        this.invn_attr_a_dflt_tracking = invn_attr_a_dflt_tracking;
    }

    public String getInvn_attr_b_tracking() {
        return invn_attr_b_tracking;
    }

    public void setInvn_attr_b_tracking(String invn_attr_b_tracking) {
        this.invn_attr_b_tracking = invn_attr_b_tracking;
    }

    public String getInvn_attr_b_dflt_tracking() {
        return invn_attr_b_dflt_tracking;
    }

    public void setInvn_attr_b_dflt_tracking(String invn_attr_b_dflt_tracking) {
        this.invn_attr_b_dflt_tracking = invn_attr_b_dflt_tracking;
    }

    public String getInvn_attr_c_tracking() {
        return invn_attr_c_tracking;
    }

    public void setInvn_attr_c_tracking(String invn_attr_c_tracking) {
        this.invn_attr_c_tracking = invn_attr_c_tracking;
    }

    public String getInvn_attr_c_dflt_tracking() {
        return invn_attr_c_dflt_tracking;
    }

    public void setInvn_attr_c_dflt_tracking(String invn_attr_c_dflt_tracking) {
        this.invn_attr_c_dflt_tracking = invn_attr_c_dflt_tracking;
    }

    public String getNmfc_code() {
        return nmfc_code;
    }

    public void setNmfc_code(String nmfc_code) {
        this.nmfc_code = nmfc_code;
    }

    public String getConversion_factor() {
        return conversion_factor;
    }

    public void setConversion_factor(String conversion_factor) {
        this.conversion_factor = conversion_factor;
    }

    public String getInvn_attr_d_tracking() {
        return invn_attr_d_tracking;
    }

    public void setInvn_attr_d_tracking(String invn_attr_d_tracking) {
        this.invn_attr_d_tracking = invn_attr_d_tracking;
    }

    public String getInvn_attr_e_tracking() {
        return invn_attr_e_tracking;
    }

    public void setInvn_attr_e_tracking(String invn_attr_e_tracking) {
        this.invn_attr_e_tracking = invn_attr_e_tracking;
    }

    public String getInvn_attr_f_tracking() {
        return invn_attr_f_tracking;
    }

    public void setInvn_attr_f_tracking(String invn_attr_f_tracking) {
        this.invn_attr_f_tracking = invn_attr_f_tracking;
    }

    public String getInvn_attr_g_tracking() {
        return invn_attr_g_tracking;
    }

    public void setInvn_attr_g_tracking(String invn_attr_g_tracking) {
        this.invn_attr_g_tracking = invn_attr_g_tracking;
    }

    public String getHost_aware_item_flg() {
        return host_aware_item_flg;
    }

    public void setHost_aware_item_flg(String host_aware_item_flg) {
        this.host_aware_item_flg = host_aware_item_flg;
    }

    public String getPacking_tolerance_percent() {
        return packing_tolerance_percent;
    }

    public void setPacking_tolerance_percent(String packing_tolerance_percent) {
        this.packing_tolerance_percent = packing_tolerance_percent;
    }

    public String getUn_number() {
        return un_number;
    }

    public void setUn_number(String un_number) {
        this.un_number = un_number;
    }

    public String getUn_class() {
        return un_class;
    }

    public void setUn_class(String un_class) {
        this.un_class = un_class;
    }

    public String getUn_description() {
        return un_description;
    }

    public void setUn_description(String un_description) {
        this.un_description = un_description;
    }

    public String getPacking_group() {
        return packing_group;
    }

    public void setPacking_group(String packing_group) {
        this.packing_group = packing_group;
    }

    public String getProper_shipping_name() {
        return proper_shipping_name;
    }

    public void setProper_shipping_name(String proper_shipping_name) {
        this.proper_shipping_name = proper_shipping_name;
    }

    public String getExcepted_qty_instr() {
        return excepted_qty_instr;
    }

    public void setExcepted_qty_instr(String excepted_qty_instr) {
        this.excepted_qty_instr = excepted_qty_instr;
    }

    public String getLimited_qty_flg() {
        return limited_qty_flg;
    }

    public void setLimited_qty_flg(String limited_qty_flg) {
        this.limited_qty_flg = limited_qty_flg;
    }

    public String getFulldg_flg() {
        return fulldg_flg;
    }

    public void setFulldg_flg(String fulldg_flg) {
        this.fulldg_flg = fulldg_flg;
    }

    public String getHazard_statement() {
        return hazard_statement;
    }

    public void setHazard_statement(String hazard_statement) {
        this.hazard_statement = hazard_statement;
    }

    public String getShipping_temperature_instr() {
        return shipping_temperature_instr;
    }

    public void setShipping_temperature_instr(String shipping_temperature_instr) {
        this.shipping_temperature_instr = shipping_temperature_instr;
    }

    public String getCarrier_commodity_description() {
        return carrier_commodity_description;
    }

    public void setCarrier_commodity_description(String carrier_commodity_description) {
        this.carrier_commodity_description = carrier_commodity_description;
    }

    public String getHazard_packaging_description() {
        return hazard_packaging_description;
    }

    public void setHazard_packaging_description(String hazard_packaging_description) {
        this.hazard_packaging_description = hazard_packaging_description;
    }

    public String getShipping_conversion_factor() {
        return shipping_conversion_factor;
    }

    public void setShipping_conversion_factor(String shipping_conversion_factor) {
        this.shipping_conversion_factor = shipping_conversion_factor;
    }

    public String getShipping_uom() {
        return shipping_uom;
    }

    public void setShipping_uom(String shipping_uom) {
        this.shipping_uom = shipping_uom;
    }

    public String getHandle_decimal_qty_flg() {
        return handle_decimal_qty_flg;
    }

    public void setHandle_decimal_qty_flg(String handle_decimal_qty_flg) {
        this.handle_decimal_qty_flg = handle_decimal_qty_flg;
    }

    public String getDummy_sku_flg() {
        return dummy_sku_flg;
    }

    public void setDummy_sku_flg(String dummy_sku_flg) {
        this.dummy_sku_flg = dummy_sku_flg;
    }

    public String getPack_with_wave_flg() {
        return pack_with_wave_flg;
    }

    public void setPack_with_wave_flg(String pack_with_wave_flg) {
        this.pack_with_wave_flg = pack_with_wave_flg;
    }
}
