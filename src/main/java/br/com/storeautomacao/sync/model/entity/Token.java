package br.com.storeautomacao.sync.model.entity;

import java.io.Serializable;

public class Token implements Serializable {

    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
