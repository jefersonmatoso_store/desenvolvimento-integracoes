package br.com.storeautomacao.sync.model.wms.Inventory;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.ArrayList;
import java.util.List;

@XStreamAlias("ListOfInventory")
public class ListOfInventory {

    @XStreamImplicit(itemFieldName="inventory")
    private List<Inventory> inventories = new ArrayList<Inventory>();

    public List<Inventory> getInventories() {
        return inventories;
    }

    public void setInventories(List<Inventory> inventories) {
        this.inventories = inventories;
    }
}
