package br.com.storeautomacao.sync.model.entity;


/**
 * <i>Entity</i> que represente os perfis no sistema.
 * <p>
 * Created by Iomar on 19/09/2017.
 */

public class Products {

    private String NotUsed;
    private String ProductId;
    private String Barcode;
    private String Description;
    private String Group;
    private String StandardPrice;
    private String SellPrice;
    private String Discount;
    private String Content;
    private String Unit;

    public String getNotUsed() {
        return NotUsed;
    }

    public void setNotUsed(String notUsed) {
        NotUsed = notUsed;
    }

    public String getProductId() {
        return ProductId;
    }

    public void setProductId(String productId) {
        ProductId = productId;
    }

    public String getBarcode() {
        return Barcode;
    }

    public void setBarcode(String barcode) {
        Barcode = barcode;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getGroup() {
        return Group;
    }

    public void setGroup(String group) {
        Group = group;
    }

    public String getStandardPrice() {
        return StandardPrice;
    }

    public void setStandardPrice(String standardPrice) {
        StandardPrice = standardPrice;
    }

    public String getSellPrice() {
        return SellPrice;
    }

    public void setSellPrice(String sellPrice) {
        SellPrice = sellPrice;
    }

    public String getDiscount() {
        return Discount;
    }

    public void setDiscount(String discount) {
        Discount = discount;
    }

    public String getContent() {
        return Content;
    }

    public void setContent(String content) {
        Content = content;
    }

    public String getUnit() {
        return Unit;
    }

    public void setUnit(String unit) {
        Unit = unit;
    }
}
