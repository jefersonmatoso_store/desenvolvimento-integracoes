package br.com.storeautomacao.sync.model.mapper;

import br.com.storeautomacao.sync.model.entity.ProductsStaging;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ProductStagindMapper implements RowMapper<ProductsStaging> {
    @Override
    public ProductsStaging mapRow(ResultSet rs, int rowNum) throws SQLException {

        ProductsStaging productsStaging = new ProductsStaging();
        productsStaging.setNotUsed("");
        productsStaging.setProductId(rs.getString("ProductId"));
        productsStaging.setBarcode(rs.getString("Barcode"));
        productsStaging.setDescription(rs.getString("Description"));
        productsStaging.setGroup(rs.getString("Group"));
        productsStaging.setStandardPrice(rs.getString("StandardPrice"));
        productsStaging.setSellPrice(rs.getString("SellPrice"));
        productsStaging.setDiscount(rs.getString("Discount"));
        productsStaging.setUnit(rs.getString("Unit"));
        productsStaging.setDelete(rs.getString("DELETE"));

        return productsStaging;
    }
}
