package br.com.storeautomacao.sync.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.data.mongodb.core.mapping.Document;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;

@Document(collection = "documento")
@JsonIgnoreProperties
public class Documento {
    private String codigoDepositante;
    private String cnpjCpfEmpresa;
    private String nomeEmpresa;
    private String enderecoEmpresa;
    private String bairroEmpresa;
    private String municipioEmpresa;
    private String ufEmpresa;
    private String cepEmpresa;
    private String codigoEmpresa;
    private String tipoEntrega;

    private BigDecimal sequenciaIntegracao;
    private BigDecimal sequenciaDocumento;

    private String numeroDocumento;
    private BigDecimal codigoEstabelecimento;
    private Date dataEmissao;
    private String tipoDocumento;
    private Date dataPrevisaoMovimento;
    private Date dataMovimento;
    private String serieDocumento;
    private String naturezaOperacao;
    private String descricaoNaturezaOperacao;
    private String cfop;
    private String agrupador;
    private BigDecimal tipoIntegracao;


    public Documento() {
    }

    public Documento(String codigoDepositante, String cnpjCpfEmpresa, String nomeEmpresa, String enderecoEmpresa, String bairroEmpresa, String municipioEmpresa,
                     String ufEmpresa, String cepEmpresa, String codigoEmpresa, BigDecimal sequenciaIntegracao, BigDecimal sequenciaDocumento, String numeroDocumento,
                     BigDecimal codigoEstabelecimento, Date dataEmissao, String tipoDocumento, Date dataPrevisaoMovimento, Date dataMovimento,
                     String serieDocumento, String naturezaOperacao, String descricaoNaturezaOperacao, String cfop, String agrupador, BigDecimal tipoIntegracao) {

        this.codigoDepositante = codigoDepositante;
        this.cnpjCpfEmpresa = cnpjCpfEmpresa;
        this.nomeEmpresa = nomeEmpresa;
        this.enderecoEmpresa = enderecoEmpresa;
        this.bairroEmpresa = bairroEmpresa;
        this.municipioEmpresa = municipioEmpresa;
        this.ufEmpresa = ufEmpresa;
        this.cepEmpresa = cepEmpresa;
        this.codigoEmpresa = codigoEmpresa;
        this.sequenciaIntegracao = sequenciaIntegracao;
        this.sequenciaDocumento = sequenciaDocumento;
        this.numeroDocumento = numeroDocumento;
        this.codigoEstabelecimento = codigoEstabelecimento;
        this.dataEmissao = dataEmissao;
        this.tipoDocumento = tipoDocumento;
        this.dataPrevisaoMovimento = dataPrevisaoMovimento;
        this.dataMovimento = dataMovimento;
        this.serieDocumento = serieDocumento;
        this.naturezaOperacao = naturezaOperacao;
        this.descricaoNaturezaOperacao = descricaoNaturezaOperacao;
        this.cfop = cfop;
        this.agrupador = agrupador;
        this.tipoIntegracao = tipoIntegracao;
    }

    //    public Documento(String codigoDepositante, String cnpjCpfEmpresa, String nomeEmpresa, String enderecoEmpresa, String bairroEmpresa,
//                     String municipioEmpresa, String ufEmpresa, String cepEmpresa, BigDecimal sequenciaIntegracao, BigDecimal sequenciaDocumento) {
//        this.codigoDepositante = codigoDepositante;
//        this.cnpjCpfEmpresa = cnpjCpfEmpresa;
//        this.nomeEmpresa = nomeEmpresa;
//        this.enderecoEmpresa = enderecoEmpresa;
//        this.bairroEmpresa = bairroEmpresa;
//        this.municipioEmpresa = municipioEmpresa;
//        this.ufEmpresa = ufEmpresa;
//        this.cepEmpresa = cepEmpresa;
//        this.sequenciaIntegracao = sequenciaIntegracao;
//        this.sequenciaDocumento = sequenciaDocumento;
//    }

    public String getCodigoDepositante() {
        return codigoDepositante;
    }

    public void setCodigoDepositante(String codigoDepositante) {
        this.codigoDepositante = codigoDepositante;
    }

    public String getCnpjCpfEmpresa() {
        return cnpjCpfEmpresa;
    }

    public void setCnpjCpfEmpresa(String cnpjCpfEmpresa) {
        this.cnpjCpfEmpresa = cnpjCpfEmpresa;
    }

    public String getNomeEmpresa() {
        return nomeEmpresa;
    }

    public void setNomeEmpresa(String nomeEmpresa) {
        this.nomeEmpresa = nomeEmpresa;
    }

    public String getEnderecoEmpresa() {
        return enderecoEmpresa;
    }

    public void setEnderecoEmpresa(String enderecoEmpresa) {
        this.enderecoEmpresa = enderecoEmpresa;
    }

    public String getBairroEmpresa() {
        return bairroEmpresa;
    }

    public void setBairroEmpresa(String bairroEmpresa) {
        this.bairroEmpresa = bairroEmpresa;
    }

    public String getMunicipioEmpresa() {
        return municipioEmpresa;
    }

    public void setMunicipioEmpresa(String municipioEmpresa) {
        this.municipioEmpresa = municipioEmpresa;
    }

    public String getUfEmpresa() {
        return ufEmpresa;
    }

    public void setUfEmpresa(String ufEmpresa) {
        this.ufEmpresa = ufEmpresa;
    }

    public String getCepEmpresa() {
        return cepEmpresa;
    }

    public void setCepEmpresa(String cepEmpresa) {
        this.cepEmpresa = cepEmpresa;
    }

    public String getCodigoEmpresa() {
        return codigoEmpresa;
    }

    public void setCodigoEmpresa(String codigoEmpresa) {
        this.codigoEmpresa = codigoEmpresa;
    }

    public BigDecimal getSequenciaIntegracao() {
        return sequenciaIntegracao;
    }

    public void setSequenciaIntegracao(BigDecimal sequenciaIntegracao) {
        this.sequenciaIntegracao = sequenciaIntegracao;
    }

    public BigDecimal getSequenciaDocumento() {
        return sequenciaDocumento;
    }

    public void setSequenciaDocumento(BigDecimal sequenciaDocumento) {
        this.sequenciaDocumento = sequenciaDocumento;
    }

    public String getNumeroDocumento() {
        return numeroDocumento;
    }

    public void setNumeroDocumento(String numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

    public BigDecimal getCodigoEstabelecimento() {
        return codigoEstabelecimento;
    }

    public void setCodigoEstabelecimento(BigDecimal codigoEstabelecimento) {
        this.codigoEstabelecimento = codigoEstabelecimento;
    }

    public Date getDataEmissao() {
        return dataEmissao;
    }

    public void setDataEmissao(Timestamp dataEmissao) {
        this.dataEmissao = dataEmissao;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public Date getDataPrevisaoMovimento() {
        return dataPrevisaoMovimento;
    }

    public void setDataPrevisaoMovimento(Timestamp dataPrevisaoMovimento) {
        this.dataPrevisaoMovimento = dataPrevisaoMovimento;
    }

    public Date getDataMovimento() {
        return dataMovimento;
    }

    public void setDataMovimento(Timestamp dataMovimento) {
        this.dataMovimento = dataMovimento;
    }

    public String getSerieDocumento() {
        return serieDocumento;
    }

    public void setSerieDocumento(String serieDocumento) {
        this.serieDocumento = serieDocumento;
    }

    public String getNaturezaOperacao() {
        return naturezaOperacao;
    }

    public void setNaturezaOperacao(String naturezaOperacao) {
        this.naturezaOperacao = naturezaOperacao;
    }

    public String getDescricaoNaturezaOperacao() {
        return descricaoNaturezaOperacao;
    }

    public void setDescricaoNaturezaOperacao(String descricaoNaturezaOperacao) {
        this.descricaoNaturezaOperacao = descricaoNaturezaOperacao;
    }

    public String getCfop() {
        return cfop;
    }

    public void setCfop(String cfop) {
        this.cfop = cfop;
    }

    public String getAgrupador() {
        return agrupador;
    }

    public void setAgrupador(String agrupador) {
        this.agrupador = agrupador;
    }

    public void setDataEmissao(Date dataEmissao) {
        this.dataEmissao = dataEmissao;
    }

    public void setDataPrevisaoMovimento(Date dataPrevisaoMovimento) {
        this.dataPrevisaoMovimento = dataPrevisaoMovimento;
    }

    public void setDataMovimento(Date dataMovimento) {
        this.dataMovimento = dataMovimento;
    }

    public BigDecimal getTipoIntegracao() {
        return tipoIntegracao;
    }

    public void setTipoIntegracao(BigDecimal tipoIntegracao) {
        this.tipoIntegracao = tipoIntegracao;
    }

    public String getTipoEntrega() {
        return tipoEntrega;
    }

    public void setTipoEntrega(String tipoEntrega) {
        this.tipoEntrega = tipoEntrega;
    }
}
