package br.com.storeautomacao.sync.model.wms.IbShipment;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("ib_shipment")
public class IbShipment {

    //@XStreamImplicit(itemFieldName="ib_shipment_hdr")
    private IbShipmentHdr ib_shipment_hdr = new IbShipmentHdr();

   // @XStreamImplicit(itemFieldName="ib_shipment_dtl")
    private IbShipmentDtl ib_shipment_dtl = new IbShipmentDtl();

    public IbShipmentHdr getIb_shipment_hdr() {
        return ib_shipment_hdr;
    }

    public void setIb_shipment_hdr(IbShipmentHdr ib_shipment_hdr) {
        this.ib_shipment_hdr = ib_shipment_hdr;
    }

    public IbShipmentDtl getIb_shipment_dtl() {
        return ib_shipment_dtl;
    }

    public void setIb_shipment_dtl(IbShipmentDtl ib_shipment_dtl) {
        this.ib_shipment_dtl = ib_shipment_dtl;
    }
}
