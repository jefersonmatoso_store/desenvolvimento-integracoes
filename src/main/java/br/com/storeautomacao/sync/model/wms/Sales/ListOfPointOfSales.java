package br.com.storeautomacao.sync.model.wms.Sales;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.ArrayList;
import java.util.List;

@XStreamAlias("ListOfPointOfSales")
public class ListOfPointOfSales {

    @XStreamImplicit(itemFieldName="point_of_sale")
    private List<PointOfSale> pointOfSales = new ArrayList<PointOfSale>();

    public List<PointOfSale> getPointOfSales() {
        return pointOfSales;
    }
}
