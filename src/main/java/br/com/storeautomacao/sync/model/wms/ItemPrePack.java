package br.com.storeautomacao.sync.model.wms;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import java.math.BigDecimal;

@XStreamAlias("item_prepack")
public class ItemPrePack {

    private String company_code;
    private String pre_pack_code;
    private String parent_item_code;
    private String parent_part_a;
    private String parent_part_b;
    private String parent_part_c;
    private String parent_part_d;
    private String parent_part_e;
    private String parent_part_f;
    private String child_item_code;
    private String child_part_a;
    private String child_part_b;
    private String child_part_c;
    private String child_part_d;
    private String child_part_e;
    private String child_part_f;
    private BigDecimal child_units;

    private String pre_pack_total_units;
    private String pre_pack_weight;
    private String pre_pack_volume;
    private String pre_pack_length;
    private String pre_pack_width;
    private String pre_pack_height;
    private String pre_pack_std_case_qty;
    private String pre_pack_max_case_qty;
    private String action_code;
    private BigDecimal seq_nbr;
    private String scrap_percentage;
    private String kitting_instruction;

    public  ItemPrePack(){}

    public String getCompany_code() {
        return company_code;
    }

    public void setCompany_code(String company_code) {
        this.company_code = company_code;
    }

    public String getPre_pack_code() {
        return pre_pack_code;
    }

    public void setPre_pack_code(String pre_pack_code) {
        this.pre_pack_code = pre_pack_code;
    }

    public String getParent_item_code() {
        return parent_item_code;
    }

    public void setParent_item_code(String parent_item_code) {
        this.parent_item_code = parent_item_code;
    }

    public String getParent_part_a() {
        return parent_part_a;
    }

    public void setParent_part_a(String parent_part_a) {
        this.parent_part_a = parent_part_a;
    }

    public String getParent_part_b() {
        return parent_part_b;
    }

    public void setParent_part_b(String parent_part_b) {
        this.parent_part_b = parent_part_b;
    }

    public String getParent_part_c() {
        return parent_part_c;
    }

    public void setParent_part_c(String parent_part_c) {
        this.parent_part_c = parent_part_c;
    }

    public String getParent_part_d() {
        return parent_part_d;
    }

    public void setParent_part_d(String parent_part_d) {
        this.parent_part_d = parent_part_d;
    }

    public String getParent_part_e() {
        return parent_part_e;
    }

    public void setParent_part_e(String parent_part_e) {
        this.parent_part_e = parent_part_e;
    }

    public String getParent_part_f() {
        return parent_part_f;
    }

    public void setParent_part_f(String parent_part_f) {
        this.parent_part_f = parent_part_f;
    }

    public String getChild_item_code() {
        return child_item_code;
    }

    public void setChild_item_code(String child_item_code) {
        this.child_item_code = child_item_code;
    }

    public String getChild_part_a() {
        return child_part_a;
    }

    public void setChild_part_a(String child_part_a) {
        this.child_part_a = child_part_a;
    }

    public String getChild_part_b() {
        return child_part_b;
    }

    public void setChild_part_b(String child_part_b) {
        this.child_part_b = child_part_b;
    }

    public String getChild_part_c() {
        return child_part_c;
    }

    public void setChild_part_c(String child_part_c) {
        this.child_part_c = child_part_c;
    }

    public String getChild_part_d() {
        return child_part_d;
    }

    public void setChild_part_d(String child_part_d) {
        this.child_part_d = child_part_d;
    }

    public String getChild_part_e() {
        return child_part_e;
    }

    public void setChild_part_e(String child_part_e) {
        this.child_part_e = child_part_e;
    }

    public String getChild_part_f() {
        return child_part_f;
    }

    public void setChild_part_f(String child_part_f) {
        this.child_part_f = child_part_f;
    }

    public BigDecimal getChild_units() {
        return child_units;
    }

    public void setChild_units(BigDecimal child_units) {
        this.child_units = child_units;
    }

    public String getPre_pack_total_units() {
        return pre_pack_total_units;
    }

    public void setPre_pack_total_units(String pre_pack_total_units) {
        this.pre_pack_total_units = pre_pack_total_units;
    }

    public String getPre_pack_weight() {
        return pre_pack_weight;
    }

    public void setPre_pack_weight(String pre_pack_weight) {
        this.pre_pack_weight = pre_pack_weight;
    }

    public String getPre_pack_volume() {
        return pre_pack_volume;
    }

    public void setPre_pack_volume(String pre_pack_volume) {
        this.pre_pack_volume = pre_pack_volume;
    }

    public String getPre_pack_length() {
        return pre_pack_length;
    }

    public void setPre_pack_length(String pre_pack_length) {
        this.pre_pack_length = pre_pack_length;
    }

    public String getPre_pack_width() {
        return pre_pack_width;
    }

    public void setPre_pack_width(String pre_pack_width) {
        this.pre_pack_width = pre_pack_width;
    }

    public String getPre_pack_height() {
        return pre_pack_height;
    }

    public void setPre_pack_height(String pre_pack_height) {
        this.pre_pack_height = pre_pack_height;
    }

    public String getPre_pack_std_case_qty() {
        return pre_pack_std_case_qty;
    }

    public void setPre_pack_std_case_qty(String pre_pack_std_case_qty) {
        this.pre_pack_std_case_qty = pre_pack_std_case_qty;
    }

    public String getPre_pack_max_case_qty() {
        return pre_pack_max_case_qty;
    }

    public void setPre_pack_max_case_qty(String pre_pack_max_case_qty) {
        this.pre_pack_max_case_qty = pre_pack_max_case_qty;
    }

    public String getAction_code() {
        return action_code;
    }

    public void setAction_code(String action_code) {
        this.action_code = action_code;
    }

    public BigDecimal getSeq_nbr() {
        return seq_nbr;
    }

    public void setSeq_nbr(BigDecimal seq_nbr) {
        this.seq_nbr = seq_nbr;
    }

    public String getScrap_percentage() {
        return scrap_percentage;
    }

    public void setScrap_percentage(String scrap_percentage) {
        this.scrap_percentage = scrap_percentage;
    }

    public String getKitting_instruction() {
        return kitting_instruction;
    }

    public void setKitting_instruction(String kitting_instruction) {
        this.kitting_instruction = kitting_instruction;
    }
}
