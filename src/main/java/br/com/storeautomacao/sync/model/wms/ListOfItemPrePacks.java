package br.com.storeautomacao.sync.model.wms;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.ArrayList;
import java.util.List;

@XStreamAlias("ListOfItemPrePacks")
public class ListOfItemPrePacks {

    @XStreamImplicit(itemFieldName="item_prepack")
    private List<ItemPrePack> itemPrePacks = new ArrayList<ItemPrePack>();

    public List<ItemPrePack> getItemPrePacks() {
        return itemPrePacks;
    }

    public void setItemPrePacks(List<ItemPrePack> itemPrePacks) {
        this.itemPrePacks = itemPrePacks;
    }
}
