package br.com.storeautomacao.sync.model.wms.Shipment;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.ArrayList;
import java.util.List;

@XStreamAlias("ListOfShippedLoads")
public class ListOfShippedLoads {

    @XStreamImplicit(itemFieldName="shipped_load")
    private List<ShippedLoad> shippedLoads = new ArrayList<ShippedLoad>();

    public List<ShippedLoad> getShippedLoads() {
        return shippedLoads;
    }

}
