package br.com.storeautomacao.sync.model.enuns;

/**
 * Enum para unidades de Medida.
 *
 * Created by alexandre on 10/08/17.
 */
public enum UnitOfLengthEnum {
    METER, KILOMETER, MILE;
}


