package br.com.storeautomacao.sync.model.wms;


import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.List;

@XStreamAlias("LgfData")
public class ConfirmacaoConferencia
{
    private Header Header;

    @XStreamImplicit
    private List<ListOfInventoryHistories> ListOfInventoryHistories;


    public br.com.storeautomacao.sync.model.wms.Header getHeader() {
        return Header;
    }

    public void setHeader(br.com.storeautomacao.sync.model.wms.Header header) {
        Header = header;
    }

    public List<ListOfInventoryHistories> getListOfInventoryHistories() {
        return ListOfInventoryHistories;
    }

    public void setListOfInventoryHistories(List<ListOfInventoryHistories> listOfInventoryHistories) {
        this.ListOfInventoryHistories = listOfInventoryHistories;
    }
}
