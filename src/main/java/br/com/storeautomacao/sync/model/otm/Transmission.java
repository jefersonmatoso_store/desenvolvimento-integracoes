package br.com.storeautomacao.sync.model.otm;

public class Transmission {

    private TransmissionBody TransmissionBody;

    private String TransmissionHeader;

    private String xmlns;

    public TransmissionBody getTransmissionBody ()
    {
        return TransmissionBody;
    }

    public void setTransmissionBody (TransmissionBody TransmissionBody)
    {
        this.TransmissionBody = TransmissionBody;
    }

    public String getTransmissionHeader ()
    {
        return TransmissionHeader;
    }

    public void setTransmissionHeader (String TransmissionHeader)
    {
        this.TransmissionHeader = TransmissionHeader;
    }

    public String getXmlns ()
    {
        return xmlns;
    }

    public void setXmlns (String xmlns)
    {
        this.xmlns = xmlns;
    }
}
