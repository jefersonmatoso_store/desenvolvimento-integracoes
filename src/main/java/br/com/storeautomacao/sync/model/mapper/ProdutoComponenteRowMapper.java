package br.com.storeautomacao.sync.model.mapper;

import br.com.storeautomacao.sync.model.entity.ProdutoComponente;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ProdutoComponenteRowMapper implements RowMapper<ProdutoComponente> {
    
    public ProdutoComponente mapRow(ResultSet rs, int rowNum) throws SQLException {

        ProdutoComponente produtoComponente = new ProdutoComponente();

        produtoComponente.setSequenciaIntegracao(rs.getBigDecimal("SEQUENCIAINTEGRACAO"));
        produtoComponente.setTipoIntegracao(rs.getBigDecimal("TIPOINTEGRACAO"));
        produtoComponente.setCodigoEmpresa(rs.getString("CODIGOEMPRESA"));
        produtoComponente.setCodigoProduto(rs.getString("CODIGOPRODUTO"));
        produtoComponente.setCodigoProdutocomponente(rs.getString("CODIGOPRODUTOCOMPONENTE"));
        produtoComponente.setQuantidadeComponente(rs.getBigDecimal("QUANTIDADECOMPONENTE"));
        produtoComponente.setSequenciaComponente(rs.getBigDecimal("SEQUENCIACOMPONENTE"));

        return produtoComponente;
    }

}