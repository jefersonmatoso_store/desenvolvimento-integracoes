package br.com.storeautomacao.sync.model.entity;

/**
 * Created by alexandre on 13/09/17.
 */
public class Mail {

    private String to;
    private StringBuffer body = new StringBuffer();
    private String subject;

    public Mail() { }

    public String getTo() { return this.to; }

    public void setTo(String to) { this.to = to; }

    public StringBuffer getBody() { return this.body; }

    public void setBody(StringBuffer body) { this.body = body; }

    public String getSubject() { return this.subject; }

    public void setSubject(String subject) { this.subject = subject; }

}
