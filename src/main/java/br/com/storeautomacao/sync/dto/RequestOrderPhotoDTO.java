package br.com.storeautomacao.sync.dto;

import java.io.Serializable;

/**
 * Created by eduardo on 13/03/18.
 */
public class RequestOrderPhotoDTO implements Serializable {

    private String idOrder;
    private String base64String;

    public RequestOrderPhotoDTO() { this(null, null); }

    public RequestOrderPhotoDTO(String idOrder, String base64String) {
        this.setIdOrder(idOrder);
        this.setBase64String(base64String);
    }

    public String getIdOrder() {
        return idOrder;
    }

    public void setIdOrder(String idOrder) {
        this.idOrder = idOrder;
    }

    public String getBase64String() { return base64String; }

    public void setBase64String(String base64String) { this.base64String = base64String; }

}
