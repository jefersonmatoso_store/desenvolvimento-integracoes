package br.com.storeautomacao.sync.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * Created by alexandre on 09/04/18.
 */
@JsonIgnoreProperties
public class UserDTO implements Serializable {

    private String userId;

    private String currentPassword;

    private String newPassword;

    private String confirmNewPassword;

    private String checkinPassword;

    private String confirmCheckinPassword;

    @JsonProperty("id_user")
    public String getUserId() {
        return userId;
    }

    @JsonProperty("current_password")
    public String getCurrentPassword() {
        return currentPassword;
    }

    @JsonProperty("new_password")
    public String getNewPassword() {
        return newPassword;
    }

    @JsonProperty("confirm_new_password")
    public String getConfirmNewPassword() {
        return confirmNewPassword;
    }

    @JsonProperty("checkin_password")
    public String getCheckinPassword() {
        return checkinPassword;
    }

    @JsonProperty("confirm_checkin_password")
    public String getConfirmCheckinPassword() {
        return confirmCheckinPassword;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setCheckinPassword(String checkinPassword) {
        this.checkinPassword = checkinPassword;
    }

    public void setConfirmCheckinPassword(String confirmCheckinPassword) {
        this.confirmCheckinPassword = confirmCheckinPassword;
    }

    public void setCurrentPassword(String currentPassword) {
        this.currentPassword = currentPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public void setConfirmNewPassword(String confirmNewPassword) {
        this.confirmNewPassword = confirmNewPassword;
    }

}
