package br.com.storeautomacao.sync.dto;

import java.util.List;

public class SaleDto {

    private List<ItemProdutoDto> itens;

    public List<ItemProdutoDto> getItens() {
        return itens;
    }

    public void setItens(List<ItemProdutoDto> itens) {
        this.itens = itens;
    }
}
