package br.com.storeautomacao.sync.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "produto")
@JsonIgnoreProperties
public class ProdutoDto {

    @Id
    @JsonProperty("idProduto")
    @JsonIgnore
    /** Identificacao na base de dados*/
    private String id;
    private String descricaoProduto;
    private double precoProduto;
    private String quantidadeProduto;
    private String codigoBarraProduto;

    public ProdutoDto(){}

    public String getDescricaoProduto() {
        return descricaoProduto;
    }

    public void setDescricaoProduto(String descricaoProduto) {
        this.descricaoProduto = descricaoProduto;
    }

    public double getPrecoProduto() {
        return precoProduto;
    }

    public void setPrecoProduto(double precoProduto) {
        this.precoProduto = precoProduto;
    }

    public String getQuantidadeProduto() {
        return quantidadeProduto;
    }

    public void setQuantidadeProduto(String quantidadeProduto) {
        this.quantidadeProduto = quantidadeProduto;
    }

    public String getCodigoBarraProduto() {
        return codigoBarraProduto;
    }

    public void setCodigoBarraProduto(String codigoBarraProduto) {
        this.codigoBarraProduto = codigoBarraProduto;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
