package br.com.storeautomacao.sync.dto;


import br.com.storeautomacao.sync.model.enuns.TypePushEnum;

/**
 * Created by eduardoabreu on 07/10/16.
 */
public class PushMessageDTO {

    private String title;
    private String subtitle;
    private String message;
    private String stringObject;
    private String key = "";
    private TypePushEnum type;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStringObject() {
        return stringObject;
    }

    public void setStringObject(String stringObject) {
        this.stringObject = stringObject;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public TypePushEnum getType() {
        return type;
    }

    public void setType(TypePushEnum type) {
        this.type = type;
    }
}
