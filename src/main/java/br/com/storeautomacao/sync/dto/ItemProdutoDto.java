package br.com.storeautomacao.sync.dto;

public class ItemProdutoDto {

    private String quantidadeProduto;
    private String codigoProduto;

    public ItemProdutoDto() {
    }

    public String getQuantidadeProduto() {
        return quantidadeProduto;
    }

    public void setQuantidadeProduto(String quantidadeProduto) {
        this.quantidadeProduto = quantidadeProduto;
    }

    public String getCodigoProduto() {
        return codigoProduto;
    }

    public void setCodigoProduto(String codigoProduto) {
        this.codigoProduto = codigoProduto;
    }
}
