package br.com.storeautomacao.sync.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * Created by alexandre on 03/10/17.
 */
public class DTO implements Serializable {

    @JsonProperty("message")
    private String message;

    @JsonProperty("error")
    private boolean error = false;

    public String getMessage() { return message; }

    public void setMessage(String message) { this.message = message; }

    public boolean isError() { return error; }

    public void setError(boolean error) { this.error = error; }

}
