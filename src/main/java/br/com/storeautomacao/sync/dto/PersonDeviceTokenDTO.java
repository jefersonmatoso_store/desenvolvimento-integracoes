package br.com.storeautomacao.sync.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by alexandre on 06/02/18.
 */
public class PersonDeviceTokenDTO extends DTO {

    @JsonProperty("id_person")
    private String idPerson;

    @JsonProperty("device_token")
    private String deviceToken;

    public String getIdPerson() {
        return idPerson;
    }

    public void setIdPerson(String idPerson) {
        this.idPerson = idPerson;
    }

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

}
