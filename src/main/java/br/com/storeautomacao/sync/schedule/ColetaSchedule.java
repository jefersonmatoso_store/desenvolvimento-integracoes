package br.com.storeautomacao.sync.schedule;

import br.com.storeautomacao.sync.connector.RestApiConnector;
import br.com.storeautomacao.sync.model.entity.*;
import br.com.storeautomacao.sync.repository.ProductRepository;
import br.com.storeautomacao.sync.service.IntegracaoService;
import br.com.storeautomacao.sync.util.DateUtil;
import br.com.storeautomacao.sync.util.XmlUtil;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.*;

/**
 * Created by alexandre on 03/08/17.
 */
@Component
@EnableScheduling
public class ColetaSchedule {

    @Autowired
    private RestApiConnector connector;

    @Autowired
    private ProductRepository repository;

    @Autowired
    private IntegracaoService integracaoService;

    private final long SEGUNDO = 5000;
    // Roda de 5 em 5 segundos
    //@Scheduled(fixedDelay = SEGUNDO)
    public void cancelLateSchedules() {
        LocalDateTime dt = LocalDateTime.now();

        List<ProdutoColeta> listProdutoColeta = repository.findAllProdutoColetado();
        String dataStr = DateUtil.dateToTimeZoneStringPicked(new Date());

        if(listProdutoColeta.size() == 0){
            //repository.limparTodasColetas();
        }

        for (ProdutoColeta produto : listProdutoColeta) {
            System.out.println(produto.getPLU());

            Credencial credencial = new Credencial();
            credencial.setLogin("store");
            credencial.setPassword("store");
            Token token = connector.login(credencial);

            Products products = repository.buscarPorId(produto.getPLU());

            List<Picked> listPicked = new ArrayList<>();
            if (token != null) {
                Picked picked = new Picked();
                picked.setIdPickupItem(Integer.valueOf(products.getDiscount()));
                picked.setQuantity(Integer.valueOf(products.getContent()));
                picked.setDateStart(dataStr);
                picked.setDateFinish(dataStr);
                picked.setDateDisplacement(dataStr);
                listPicked.add(picked);
                try{
                    connector.deletaItem(listPicked, token);
                } catch(Exception ex){
                    System.out.println(ex.getMessage());
                }

                repository.limparColeta(produto.getPLU());
            }


        }


    }


    private final long DEZSEGUNDO = 40000;

    // Roda de 5 em 5 segundos
    @Scheduled(fixedDelay = DEZSEGUNDO)
    public void integracaoEntradaMercadoria() {
        System.out.println("##### INICIANDO INTEGRACAO ENTRADA DE MERCADORIA ##### ");

        Serializable response = null;
        try {
            //int numeroDeOperacoes = mercadoriaService.entrada(BigDecimal.valueOf(8197594));
            List<Integracao> listaIntegracao = integracaoService.entradaMercadoria();

            if (listaIntegracao.size() > 0) {
                System.out.println("##### Integracao realizada com sucesso ##### ");
            } else {
                //System.out.println("##### Nao existe itens para integrar ##### ");
            }
        } catch (Exception e) {
            //System.out.println("##### Algum erro ocorreu ##### " + e.getMessage());
        }
        System.out.println("##### FINALIZANDO INTEGRACAO ENTRADA DE MERCADORIA ##### ");
    }



    private final long qSEGUNDO = 50000;
    // Roda de 5 em 5 segundos
    @Scheduled(fixedDelay = 70000)
    public void integracaoLogix() {
        System.out.println("##### INICIANDO INTEGRACAO LOGIX ##### ");

        Serializable response = null;
        try {
            //int numeroDeOperacoes = mercadoriaService.entrada(BigDecimal.valueOf(8197594));
            List<Integracao> listaIntegracao = integracaoService.listaIntegracaoLogix();

            if (listaIntegracao.size() > 0) {
                System.out.println("##### Integracao realizada com sucesso ##### ");
            } else {
                //System.out.println("##### Nao existe itens para integrar ##### ");
            }
        } catch (Exception e) {
            //System.out.println("##### Algum erro ocorreu ##### " + e.getMessage());
        }

    }



    // Roda de 10 em 10 segundos
    @Scheduled(fixedDelay = 10000)
    public void integracaoSaidaMercadoria() {
        System.out.println("##### INICIANDO INTEGRACAO SAIDA DE MERCADORIA ##### ");

        Serializable response = null;
        try {
            //int numeroDeOperacoes = mercadoriaService.entrada(BigDecimal.valueOf(8197594));
            List<Integracao> listaIntegracao = integracaoService.saidaMercadoria();

            if (listaIntegracao.size() > 0) {
               System.out.println("##### Integracao realizada com sucesso ##### ");
            } else {
                //System.out.println("##### Nao existe itens para integrar ##### ");
            }
        } catch (Exception e) {
            //System.out.println("##### Algum erro ocorreu ##### " + e.getMessage());
        }
        System.out.println("##### FINALIZANDO INTEGRACAO SAIDA DE MERCADORIA ##### ");

    }

    // Roda de minuto em minuto
    //@Scheduled(fixedDelay = MINUTO)
    public void postaTemepraturaSchedule() {

        System.out.println("##### INICIANDO PROCESSO DE ATUALIZACAO ##### ");
        String macAdress = null;
        String nomeDaRua = null;
        // Ruas
        Map<String, String> mapAdrresTemp= new HashMap<String, String>();
        mapAdrresTemp.put("F4:11:3C:31:6A:6F", "Rua A");
        mapAdrresTemp.put("D1:A7:E5:7F:9B:CF", "Rua B");
        mapAdrresTemp.put("F9:13:30:5C:E6:46", "Rua C");
        //mapAdrresTemp.put("C4:44:B6:D6:FD:2E", "Rua D");
        //mapAdrresTemp.put("FC:3F:5D:DD:8C:54", "Rua E");
        //mapAdrresTemp.put("C8:16:1E:94:25:4F", "Rua F");
        //mapAdrresTemp.put("F2:54:F4:68:2A:C7", "Rua G");

        for (Map.Entry<String, String> entry : mapAdrresTemp.entrySet())
        {
            macAdress = entry.getKey();
            nomeDaRua = entry.getValue();

            System.out.println("Iniciando atualizacao da temperatura em Rua: " + nomeDaRua + " - Aparelho : " + nomeDaRua);

            try {
                HttpEntity<TemperaturaResponse> str = connector.connectorGetHttpTemp(null, macAdress);
                Gson gson = new Gson();
                String stringJson = str.getBody().getData().get(0).getData();
                TemperaturaDataDescription obj2 = gson.fromJson(stringJson, TemperaturaDataDescription.class);
                float temperatura = obj2.getTemperature();
                float airPressure = obj2.getAir_pressure();
                float humidity = obj2.getHumidity();
                String xmlString = XmlUtil.getXmlBody(macAdress, macAdress, nomeDaRua, macAdress, String.valueOf(temperatura), String.valueOf(airPressure), String.valueOf(humidity));
                connector.connectorSendXmlWms(xmlString);
                System.out.println("Atualizado: " + nomeDaRua );
            } catch (Exception ex){
                System.out.println("Erro: " + ex.getMessage());
            }
        }

        System.out.println("!!!!!!! FINALIZADO PROCESSO DE ATUALIZACAO !!!!!!! ");

    }
}
