package br.com.storeautomacao.sync.repository;

import br.com.storeautomacao.sync.model.entity.Documento;
import br.com.storeautomacao.sync.model.entity.DocumentoDetalhe;

import java.math.BigDecimal;
import java.util.List;

public interface DocumentoRepository {

    Documento obterDocumentoPorSequenciaIntegracao(BigDecimal sequenciaIntegracao);

    Documento obterDocumentoPorSequenciaIntegracaoNumeroDocumentoCodigoDepositante(BigDecimal sequenciaIntegracao, String numeroDocumento, String codigoDepositante);

    Documento obterDocumentoPorNumeroDocumento(String numeroDocumento);

    Documento obterDocumentoDIntegracao203(String numeroDocumento, String agrupador,  String codigoDepositante, BigDecimal tipoIntegracao);

    Documento obterDocumentoParaCancelamento(String numeroDocumento, String codigoDepositante, BigDecimal tipoIntegracao);

    List<DocumentoDetalhe> obterDocumentoDetalhePorSequenciaIntegracao(BigDecimal sequenciaIntegracao);

    List<DocumentoDetalhe> obterListaDocumentoDetalhePorSequenciaIntegracaoNumedoDocumento(BigDecimal sequenciaIntegracao, String numeroDocumento);

    DocumentoDetalhe obterDocumentoDetalhePorSequenciaIntegracaoNumedoDocumento(BigDecimal sequenciaIntegracao, BigDecimal sequenciaDocumento);

    DocumentoDetalhe obterDocumentoDetalhePorSequenciaIntegracaoDetalhe(BigDecimal sequenciaIntegracao, BigDecimal sequenciaDocumentoDetalhe );

    DocumentoDetalhe obterDocumentoDetalheSalvo(BigDecimal sequenciaIntegracao, BigDecimal sequenciaDocumentoDetalhe );

}
