package br.com.storeautomacao.sync.repository;

import br.com.storeautomacao.sync.dao.ProfileDao;
import br.com.storeautomacao.sync.model.entity.Person;
import br.com.storeautomacao.sync.model.entity.Profile;
import br.com.storeautomacao.sync.model.entity.ProfileType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

/**
 * Created by alexandre on 21/09/17.
 */
@Repository
public class ProfileRepository {

    @Autowired
    private ProfileDao dao;

    @Async
    public String create(Profile profile) { return dao.create(profile); }

    public List<Profile> findAll() {
        return dao.findAll();
    }

    public List<Profile> findByPerson(Collection<Person> persons) { return dao.findByPerson(persons); }

    public Profile findById(String id) { return dao.findById(id); }

    public Profile findByIdPerson(String idPerson) { return dao.findByIdPerson(idPerson); }

    public List<Profile> findByType(ProfileType type) { return dao.findByType(type); }

    @Async
    public String update(Profile profile) { return dao.create(profile); }

    @Async
    public void remove(String id) { dao.remove(id); }

    @Async
    public void remove(Profile profile) {
        dao.remove(profile);
    }

}
