package br.com.storeautomacao.sync.repository;

import br.com.storeautomacao.sync.dao.IntegracaoDao;
import br.com.storeautomacao.sync.model.entity.Embalagem;
import br.com.storeautomacao.sync.model.entity.Integracao;
import br.com.storeautomacao.sync.model.entity.Mercadoria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by alexandre.
 */
@Repository
public class IntegracaoRepositoryImpl implements IntegracaoRepository{

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private MongoOperations mongoOperation;


    @Autowired
    private IntegracaoDao integracaoDao;

    public static final String DOCUMENTO = "documento";
    public static final String DOCUMENTO_PENDENTE = "documento";
    public static final String MERCADORIA = "mercadoria";

    public String salvarOperacaoEntrada(Mercadoria entradaMercadoriaEmAndamento) {
        mongoOperation.save(entradaMercadoriaEmAndamento, MERCADORIA);
        return String.valueOf(entradaMercadoriaEmAndamento.getSequenciaIntegracao());
    }

    public String atualizarOperacaoEntrada(Mercadoria entradaMercadoriaEmAndamento) {
        Query query = new Query(Criteria.where("numeroDocumento").regex(entradaMercadoriaEmAndamento.getNumeroDocumento()));
        //Mercadoria mercadoria = mongoOperation.findOne(query, Mercadoria.class, MERCADORIA);
        //Mercadoria minhaMercadoria = new Mercadoria();
       //minhaMercadoria.setEstadoIntegracao(entradaMercadoriaEmAndamento.getEstadoIntegracao());

        Update update = new Update();
        update.set("estadoIntegracao", entradaMercadoriaEmAndamento.getEstadoIntegracao());

       //mercadoria.setEstadoIntegracao(entradaMercadoriaEmAndamento.getEstadoIntegracao());
        mongoOperation.upsert(query, update, Mercadoria.class);
        return String.valueOf(entradaMercadoriaEmAndamento.getSequenciaIntegracao());
    }

    @Override
    public String salvarOperacaoSaida(Mercadoria entradaMercadoriaEmAndamento) {
        mongoOperation.save(entradaMercadoriaEmAndamento, MERCADORIA);
        return String.valueOf(entradaMercadoriaEmAndamento.getSequenciaIntegracao());
    }

    @Override
    public String salvarOperacaoPendente(Mercadoria entradaMercadoriaEmAndamento) {
        mongoOperation.save(entradaMercadoriaEmAndamento, DOCUMENTO_PENDENTE);
        return String.valueOf(entradaMercadoriaEmAndamento.getDocumento().getSequenciaIntegracao());
    }

    @Override
    public Mercadoria obterEntradaMercadoriaEmAndamentoPorSequenciaIntegracao(BigDecimal sequenciaIntegracao) {
        Query query = new Query(Criteria.where("sequenciaIntegracao").is(sequenciaIntegracao));
        return mongoOperation.findOne(query, Mercadoria.class, MERCADORIA);
    }

    @Override
    public List<Integracao> listaIntegracaoEntradaMercadoriaPrevisaoDeEntrada() {
        return integracaoDao.listaIntegracaoEntradaMercadoriaPrevisaoDeEntrada();
    }

    @Override
    public List<Integracao> listaIntegracaoEntradaMercadoriaPrevisaoDeSaida() {
        return integracaoDao.listaIntegracaoEntradaMercadoriaPrevisaoDeSaida();
    }

    @Override
    public List<Integracao> listaIntegracaoLogix() {
        return integracaoDao.listaIntegracaoLogix();
    }

    @Override
    public void atualizar(String sql) {
        integracaoDao.atualizarIntegracao(sql);
    }

    @Override
    public void salvar(String sql) {
        integracaoDao.gravarIntegracao(sql);
    }

    @Override
    public BigDecimal salvarIntegracao(String sql) {
        return integracaoDao.gravarIntegracaoConferenciaMercadoria(sql);
    }

    @Override
    public BigDecimal salvarDocumento(String sql) {
        return integracaoDao.gravarDocumentoIntegracao(sql);
    }

    @Override
    public String obterSequenciaIntegracao(String sql) {
        return "";
    }

    @Override
    public String salvarEmbalagem(List<Embalagem> embalagems) {
        return integracaoDao.salvarEmbalagem(embalagems);
    }

    @Override
    public List<Embalagem> obterEmbalagens(String code) {
        return integracaoDao.obterEmbalagensPorCodigo(code);
    }
}
