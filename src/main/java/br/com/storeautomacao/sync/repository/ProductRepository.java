package br.com.storeautomacao.sync.repository;

import br.com.storeautomacao.sync.model.entity.PickUp;
import br.com.storeautomacao.sync.model.entity.Products;
import br.com.storeautomacao.sync.model.entity.ProductsStaging;
import br.com.storeautomacao.sync.model.entity.ProdutoColeta;
import br.com.storeautomacao.sync.model.mapper.PickUpMapper;
import br.com.storeautomacao.sync.model.mapper.ProductsColetaMapper;
import br.com.storeautomacao.sync.model.mapper.ProductsMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by alexandre on 21/09/17.
 */
@Repository
public class ProductRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;


    @Transactional(readOnly=true)
    public List<Products> findAll() {
        return jdbcTemplate.query("select * from products",
                new ProductsMapper());
    }

    @Transactional(readOnly=true)
    public List<ProdutoColeta> findAllProdutoColetado() {
        return jdbcTemplate.query("select * from `900000002`",
                new ProductsColetaMapper());
    }




    public String insertLink(final String idProduto, final String macAdress)
    {
        final String sql = "INSERT INTO `links` (`ID`, `Variant`, `MAC`) VALUES (?,?,?)";

        jdbcTemplate.update(new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                PreparedStatement ps = connection.prepareStatement(sql);
                ps.setString(1, idProduto);
                ps.setString(2, "V");
                ps.setString(3, macAdress);

                return ps;
            }
        });


        return "OK";
    }


    public ProductsStaging insertProductStaging(final ProductsStaging products)
    {
        final String sql = "INSERT INTO `products_staging` (`NotUsed`, `ProductId`, `Barcode`, `Description`, `Group`, `StandardPrice`, `SellPrice`, `Discount`, `Content`, `Unit`, `DELETE`) VALUES (?,?,?,?,?,?,?,?,?,?,?)";

        KeyHolder holder = new GeneratedKeyHolder();
        jdbcTemplate.update(new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                PreparedStatement ps = connection.prepareStatement(sql);
                ps.setString(1, products.getNotUsed());
                ps.setString(2, products.getProductId());
                ps.setString(3, products.getBarcode());
                ps.setString(4, products.getDescription());
                ps.setString(5, products.getGroup());
                ps.setString(6, products.getStandardPrice());
                ps.setString(7, products.getSellPrice());
                ps.setString(8, products.getDiscount());
                ps.setString(9, products.getContent());
                ps.setString(10, products.getUnit());
                ps.setString(11, products.getDelete());

                return ps;
            }
        });


        return products;
    }


    public String acende(String macAddress, String user)
    {
        final String sql = "INSERT INTO `actions_staging` (`ACTION`, `MAC`, `USER`, `PARAM1`, `PARAM2`, `PARAM3`, `TIME`) VALUES (?,?,?,?,?,?,?)";

        KeyHolder holder = new GeneratedKeyHolder();
        jdbcTemplate.update(new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                PreparedStatement ps = connection.prepareStatement(sql);
                ps.setString(1, "REFRESH_IMAGE");
                ps.setString(2, macAddress);
                ps.setString(3, user);
                ps.setString(4, "");
                ps.setString(5, "");
                ps.setString(6, "");
                ps.setString(7, null);

                return ps;
            }
        });


        return "OK";
    }

    public String limparEtiqueta(String macAddress)
    {
        final String sql = "DELETE FROM `links` WHERE `links`.`MAC` = ?";

        KeyHolder holder = new GeneratedKeyHolder();
        jdbcTemplate.update(new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                PreparedStatement ps = connection.prepareStatement(sql);
                ps.setString(1, macAddress);
                return ps;
            }
        });


        return "OK";
    }

    public String limparColeta(String PLU)
    {
        final String sql = "DELETE FROM `900000002` WHERE `900000002`.`PLU` = ?";

        KeyHolder holder = new GeneratedKeyHolder();
        jdbcTemplate.update(new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                PreparedStatement ps = connection.prepareStatement(sql);
                ps.setString(1, PLU);
                return ps;
            }
        });


        return "OK";
    }

    public Products buscarPorId(String id)
    {
        final String sql = "SELECT * FROM `products` WHERE ProductId = ?";

        return jdbcTemplate.queryForObject
                (sql,new Object[] { id }, new ProductsMapper());
    }


    public String registrarColeta(String numeroOrdem, String status, int checkin)
    {
        final String sql = "INSERT INTO `pick_up` (`numero_ordem`, `status`, `checkin`) VALUES (?,?,?)";

        KeyHolder holder = new GeneratedKeyHolder();
        jdbcTemplate.update(new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                PreparedStatement ps = connection.prepareStatement(sql);
                ps.setString(1, numeroOrdem);
                ps.setString(2, status);
                ps.setInt(3,checkin);

                return ps;
            }
        });


        return "OK";
    }

    public Products buscarPorNumeroOrdem(String numeroOrdem)
    {
        final String sql = "SELECT * FROM `pick_up` WHERE numero_ordem = ?";

        return jdbcTemplate.queryForObject
                (sql,new Object[] { numeroOrdem }, new ProductsMapper());
    }

    @Transactional(readOnly=true)
    public List<PickUp> findColetaEmProgresso() {
        return jdbcTemplate.query("select * from `pick_up`",
                new PickUpMapper());
    }



    public String limparTodasColetas()
    {
        final String sql = "DELETE FROM `pick_up`";

        KeyHolder holder = new GeneratedKeyHolder();
        jdbcTemplate.update(new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                PreparedStatement ps = connection.prepareStatement(sql);
                return ps;
            }
        });


        return "OK";
    }

    public List<PickUp> buscarOrdemPorNumero(String numero)
    {
        final String sql = "SELECT * FROM `pick_up` WHERE numero_ordem = ?";

        return jdbcTemplate.query
                (sql,new Object[] { numero }, new PickUpMapper());
    }

}
