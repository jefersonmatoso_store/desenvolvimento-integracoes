package br.com.storeautomacao.sync.repository;

import org.springframework.stereotype.Repository;

import java.math.BigDecimal;

/**
 * Created by iomar on 21/11/18.
 */
public interface RomaneioRepository {
    BigDecimal gravarRomaneio(String sql);
}
