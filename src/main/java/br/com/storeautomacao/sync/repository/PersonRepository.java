package br.com.storeautomacao.sync.repository;

import br.com.storeautomacao.sync.dao.PersonDao;
import br.com.storeautomacao.sync.model.entity.IdentityDocument;
import br.com.storeautomacao.sync.model.entity.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

/**
 * Created by alexandre on 30/07/17.
 */
@Repository
public class PersonRepository {

    @Autowired
    private PersonDao dao;

    @Async
    public String create(Person person) { return dao.create(person); }

    public List<Person> findAll() {
        return dao.findAll();
    }

    public Person findById(String id) { return dao.findById(id); }

    public List<Person> findByFirstName(String firstName) { return dao.findByFirstName(firstName); }

    public List<Person> findByLastName(String lastName) { return dao.findByLastName(lastName); }

    public List<Person> findByEmail(String email) { return dao.findByEmail(email); }

    public List<Person> findByNamePiece(String namePiece) { return dao.findByNamePiece(namePiece); }

    public List<Person> findByDocumentNumber(Collection<IdentityDocument> documents) { return dao.findByDocumentNumber(documents); }

    public List<Person> findByNameOrDocumentNumber(String nameOrdocumentNumber, Collection<IdentityDocument> documents) {
        return dao.findByNameOrDocumentNumber(nameOrdocumentNumber, documents); }

    public Person findByFacialRecognition(String facialRecognition) { return dao.findByFacialRecognition(facialRecognition); }

    @Async
    public String update(Person person) { return dao.create(person); }

    @Async
    public void remove(String id) { dao.remove(id); }

    @Async
    public void remove(Person person) {
        dao.remove(person);
    }
}
