package br.com.storeautomacao.sync.repository;

import br.com.storeautomacao.sync.dao.OrderDao;
import br.com.storeautomacao.sync.model.entity.Order;
import br.com.storeautomacao.sync.util.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Date;
import java.util.List;

/**
 * Created by alexandre on 06/07/17.
 */
@Repository
public class OrderRepository {

    @Autowired
    private OrderDao dao;

    @Async
    public String create(Order order) { return dao.create(order); }

    public List<Order> listAll() { return dao.findAll(); }

    public Order findById(String id) { return dao.findById(id); }

    public Order pickUpOrder(String id) { return dao.pickUpOrder(id); }

    public List<Order> findByName(String name) {
        return dao.findByName(name);
    }

    public List<Order> findBy(String nameRecipient, LocalDate ldStart, LocalDate ldEnd) {
        Date dtStart = (ldStart == null ? null : DateUtil.convertToDate(LocalDateTime.of(ldStart, LocalTime.MIN)));
        Date dtEnd = (ldEnd == null ? null : DateUtil.convertToDate(LocalDateTime.of(ldEnd, LocalTime.MAX)));

        return dao.findBy(nameRecipient, dtStart, dtEnd);
    }

    @Async
    public String update(Order order) { return dao.create(order); }

    @Async
    public void remove(String id) { dao.remove(id); }

    @Async
    public void remove(Order order) {
        dao.remove(order);
    }

    public boolean deleteAll() {
        return dao.deleteAll();
    }

    public List<Order> findByIdUserLogado(String idUsuario) {
        return dao.findByIdUserLogado(idUsuario); }
}
