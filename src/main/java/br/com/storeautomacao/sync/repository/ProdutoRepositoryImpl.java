package br.com.storeautomacao.sync.repository;

import br.com.storeautomacao.sync.dao.ProdutoDao;
import br.com.storeautomacao.sync.dto.ItemDto;
import br.com.storeautomacao.sync.dto.ProdutoDto;
import br.com.storeautomacao.sync.model.entity.Produto;
import br.com.storeautomacao.sync.model.entity.ProdutoComponente;
import br.com.storeautomacao.sync.model.entity.TipoUc;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 *
 */
@Repository
public class ProdutoRepositoryImpl implements ProdutoRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private ProdutoDao produtoDao;

    @Override
    public List<Produto> obterListaProdutoPorSequenciaIntegracao(String sequenciaIntegracao) {
        return produtoDao.obterListaProdutoPorSequenciaIntegracao(sequenciaIntegracao);
    }

    @Override
    public TipoUc obterTipoUcPorCodigoProdutoESequenciaIntegracao(String sequenciaIntegracao, String codigoProduto) {
        return produtoDao.obterTipoUcPorCodigoProdutoESequenciaIntegracao(sequenciaIntegracao, codigoProduto);
    }

    @Override
    public List<ProdutoComponente> obterProdutosComponentesPorCodigoProdutoESequenciaIntegracao(String sequenciaIntegracao, String codigoProduto) {
        return produtoDao.obterProdutosComponentesPorCodigoProdutoESequenciaIntegracao(sequenciaIntegracao, codigoProduto);
    }

    @Override
    public String cadastrarProduto(ProdutoDto produtoDto) {
        return produtoDao.cadastrarProduto(produtoDto);
    }

    @Override
    public String cadastrarItem(ItemDto itemDto) {
        return produtoDao.cadastrarItem(itemDto);
    }

    @Override
    public List<ProdutoDto> obterTodosProdutos() {
        return produtoDao.obterTodosProdutos();
    }
}
