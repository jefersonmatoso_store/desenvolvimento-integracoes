package br.com.storeautomacao.sync.repository;

import br.com.storeautomacao.sync.dao.ConferenciaDao;
import br.com.storeautomacao.sync.model.entity.Conferencia;
import br.com.storeautomacao.sync.model.entity.Mercadoria;
import br.com.storeautomacao.sync.model.wms.Inventory_history;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Created by alexandre.
 */
@Repository
public class ConferenciaRepositoryImpl implements ConferenciaRepository{

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private MongoOperations mongoOperation;


    @Autowired
    private ConferenciaDao conferenciaDao;


    @Override
    public String salvarConferencia(Mercadoria mercadoria, Inventory_history inventoryHistory) {

        Conferencia conferencia = new Conferencia();
        conferencia.setSequenciaIntegracao(mercadoria.getSequenciaIntegracao());
        conferencia.setNumeroDocumento(mercadoria.getNumeroDocumento());
        conferencia.setQuantidadeMovimento(new BigDecimal(inventoryHistory.getAdj_qty()));
        String estadoMercadoria = inventoryHistory.getLock_code() == null ? "Normal" : inventoryHistory.getLock_code();
        conferencia.setEstadoMercadoria(estadoMercadoria); // Classe Produto
        conferencia.setSequenciaDocumentoDetalhe(inventoryHistory.getPo_line_nbr());
        //conferencia.setPeso(); // vou confirmar objeto peso
        conferencia.setDataHoraConferencia(new Date());

        return conferenciaDao.salvarConferencia(conferencia);
    }

    @Override
    public List<Conferencia> obterListaMercadoriaPorNumeroDocumento(String numeroDocumento, String sequenciaIntegracao) {
        return conferenciaDao.obterListaConferenciaPorNumeroDocumento(numeroDocumento,sequenciaIntegracao);
    }

    @Override
    public Conferencia obterMercadoriaPorNumeroDocumento(String numeroDocumento) {
        return conferenciaDao.obterConferenciaPorNumeroDocumento(numeroDocumento);
    }

}
