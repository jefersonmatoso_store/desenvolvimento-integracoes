package br.com.storeautomacao.sync.repository;

import br.com.storeautomacao.sync.model.entity.DocumentoDetalheSequencia;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by iomar on 21/11/18.
 */
public interface DocumentoDetalheSequenciaRepository {
    BigDecimal gravarDocumentoDetalheSequencia(String sql);
    List<DocumentoDetalheSequencia> obterDocumentoDetalheSequenciaPorSequenciaIntegracao(String sequenciaIntegracao);
}
