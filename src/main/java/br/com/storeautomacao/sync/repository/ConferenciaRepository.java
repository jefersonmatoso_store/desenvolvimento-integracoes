package br.com.storeautomacao.sync.repository;

import br.com.storeautomacao.sync.model.entity.Conferencia;
import br.com.storeautomacao.sync.model.entity.Mercadoria;
import br.com.storeautomacao.sync.model.wms.Inventory_history;

import java.util.List;

public interface ConferenciaRepository {

    String salvarConferencia(Mercadoria mercadoria, Inventory_history inventoryHistory);

    List<Conferencia> obterListaMercadoriaPorNumeroDocumento(String numeroDocumento, String sequenciaIntegracao);

    Conferencia obterMercadoriaPorNumeroDocumento(String numeroDocumento);

}
