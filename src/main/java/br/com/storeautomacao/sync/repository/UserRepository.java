package br.com.storeautomacao.sync.repository;

import br.com.storeautomacao.sync.dao.UserDao;
import br.com.storeautomacao.sync.model.entity.Profile;
import br.com.storeautomacao.sync.model.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

@Repository
public class UserRepository {

	@Autowired
    UserDao userDao;

	public String create(User user) {
		return userDao.create(user);
	}

	public String update(User user) {
		return userDao.update(user);
	}

	public User updatePassword(String userId, String password) { return userDao.updatePasswordField(userId, "password", password); }

	public User updateCheckinPassword(String userId, String checkinPassword) { return userDao.updatePasswordField(userId, "checkinPassword", checkinPassword); }

	public User findById(String idUser) {
		return userDao.findById(idUser);
	}

	public User findByIdProfile(String idUser) {
		return userDao.findByIdProfile(idUser);
	}

	public List<User> findAll() {
        return userDao.findAll();
	}

	public List<User> findAll(Collection<Profile> profiles) { return userDao.findAll(profiles); }

	public boolean deleteById(String idUser) {
        return userDao.deleteById(idUser);
    }

	public User findUserByUsernameAndPassword(String userName, String password) {
		return userDao.findUserByUsernameAndPassword(userName, password);
	}

	public User findUserByUserIdAndCheckinPassword(String userId, String checkinPassword) {
		return userDao.findUserByUserIdAndCheckinPassword(userId, checkinPassword);
	}

	public User findByUsername(String username) { return userDao.findByUsername(username); }

	@Deprecated
	public List<User> findAllVigilants() {
		return userDao.findAllVigilants();
	}
}
