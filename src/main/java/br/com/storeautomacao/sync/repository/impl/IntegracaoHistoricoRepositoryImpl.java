package br.com.storeautomacao.sync.repository.impl;

import br.com.storeautomacao.sync.dao.IntegracaoHistoricoDao;
import br.com.storeautomacao.sync.repository.IntegracaoHistoricoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;

/**
 * Created by iomar on 21/11/18.
 */
public class IntegracaoHistoricoRepositoryImpl implements IntegracaoHistoricoRepository {

    @Autowired
    private IntegracaoHistoricoDao integracaoHistoricoDao;

    @Override
    public BigDecimal gravarIntegracaoHistorico(String sql) {
        return integracaoHistoricoDao.gravarIntegracaoHistorico(sql);
    }
}