package br.com.storeautomacao.sync.repository.impl;

import br.com.storeautomacao.sync.dao.DocumentoEmbalagemDao;
import br.com.storeautomacao.sync.repository.DocumentoEmbalagemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;

/**
 * Created by iomar on 21/11/18.
 */
public class DocumentoEmbalagemRepositoryImpl implements DocumentoEmbalagemRepository {

    @Autowired
    private DocumentoEmbalagemDao documentoEmbalagemDao;

    @Override
    public BigDecimal gravarDocumentoEmbalagem(String sql) {
        return documentoEmbalagemDao.gravarDocumentoEmbalagem(sql);
    }
}
