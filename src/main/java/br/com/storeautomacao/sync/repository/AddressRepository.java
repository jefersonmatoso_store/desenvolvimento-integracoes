package br.com.storeautomacao.sync.repository;

import br.com.storeautomacao.sync.dao.AddressDao;
import br.com.storeautomacao.sync.model.entity.Address;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by alexandre on 12/09/17.
 */
@Repository
public class AddressRepository {

    @Autowired
    private AddressDao dao;

    @Async
    public String create(Address address) { return dao.create(address); }

    public List<Address> findAll() { return dao.findAll(); }

    public Address findById(String id) {return dao.findById(id); }

    public List<Address> findByText(String text) { return dao.findByText(text); }

    @Async
    public String update(Address address) { return dao.create(address); }

    @Async
    public void remove(String id) { dao.remove(id); }

    @Async
    public void remove(Address address) {
        dao.remove(address);
    }
}
