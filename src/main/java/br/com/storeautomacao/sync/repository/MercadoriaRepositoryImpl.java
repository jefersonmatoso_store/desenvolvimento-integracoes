package br.com.storeautomacao.sync.repository;

import br.com.storeautomacao.sync.dao.MercadoriaDao;
import br.com.storeautomacao.sync.model.entity.Mercadoria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by alexandre.
 */
@Repository
public class MercadoriaRepositoryImpl implements MercadoriaRepository{

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private MongoOperations mongoOperation;


    @Autowired
    private MercadoriaDao mercadoriaDao;

    @Override
    public String salvarMercadoriaEmMovimento(Mercadoria mercadoria) {
        return null;
    }

    @Override
    public List<Mercadoria> obterListaEntradaMercadoria() {
        return mercadoriaDao.obterListaEntradaMercadoria();
    }

    @Override
    public List<Mercadoria> obterListaMercadoriaPorNumeroDocumento(String numeroDocumento) {
        return mercadoriaDao.obterListaMercadoriaPorNumeroDocumento(numeroDocumento);
    }

    @Override
    public Mercadoria obterMercadoriaPorNumeroDocumento(String numeroDocumento) {
        return mercadoriaDao.obterMercadoriaPorNumeroDocumento(numeroDocumento);
    }
}
