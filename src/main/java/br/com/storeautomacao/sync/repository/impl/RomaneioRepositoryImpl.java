package br.com.storeautomacao.sync.repository.impl;

import br.com.storeautomacao.sync.dao.RomaneioDao;
import br.com.storeautomacao.sync.repository.RomaneioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;

/**
 * Created by iomar on 21/11/18.
 */
public class RomaneioRepositoryImpl implements RomaneioRepository {

    @Autowired
    private RomaneioDao romaneioDao;

    @Override
    public BigDecimal gravarRomaneio(String sql) {
        return romaneioDao.gravarRomaneio(sql);
    }
}
