package br.com.storeautomacao.sync.repository.impl;

import br.com.storeautomacao.sync.dao.DocumentoDetalheSequenciaDao;
import br.com.storeautomacao.sync.model.entity.DocumentoDetalheSequencia;
import br.com.storeautomacao.sync.repository.DocumentoDetalheSequenciaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by iomar on 21/11/18.
 */
@Repository
public class DocumentoDetalheSequenciaRepositoryImpl implements DocumentoDetalheSequenciaRepository {

    @Autowired
    private DocumentoDetalheSequenciaDao documentoDetalheSequenciaDao;

    @Override
    public BigDecimal gravarDocumentoDetalheSequencia(String sql) {
        return documentoDetalheSequenciaDao.gravarDocumentoDetalheSequencia(sql);
    }

    @Override
    public List<DocumentoDetalheSequencia> obterDocumentoDetalheSequenciaPorSequenciaIntegracao(String sequenciaIntegracao) {
        return documentoDetalheSequenciaDao.obterDocumentosDetalhesSequenciasPorSequenciaIntegracao(sequenciaIntegracao);
    }

}
