package br.com.storeautomacao.sync.repository;

import br.com.storeautomacao.sync.model.entity.Mercadoria;

import java.util.List;

public interface MercadoriaRepository {

    String salvarMercadoriaEmMovimento(Mercadoria mercadoria);

    List<Mercadoria> obterListaEntradaMercadoria();

    List<Mercadoria> obterListaMercadoriaPorNumeroDocumento( String numeroDocumento);

    Mercadoria obterMercadoriaPorNumeroDocumento( String numeroDocumento);

}
