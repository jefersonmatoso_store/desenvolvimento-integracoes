package br.com.storeautomacao.sync.repository;

import br.com.storeautomacao.sync.dao.DocumentoDao;
import br.com.storeautomacao.sync.model.entity.Documento;
import br.com.storeautomacao.sync.model.entity.DocumentoDetalhe;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by alexandre on 21/09/17.
 */
@Repository
public class DocumentoRepositoryImpl implements DocumentoRepository {

    @Autowired
    private DocumentoDao documentoDao;


    @Override
    public Documento obterDocumentoPorSequenciaIntegracao(BigDecimal sequenciaIntegracao) {
        return documentoDao.obterDocumentoPorSequenciaIntegracao(sequenciaIntegracao);
    }

    @Override
    public Documento obterDocumentoPorSequenciaIntegracaoNumeroDocumentoCodigoDepositante(BigDecimal sequenciaIntegracao, String numeroDocumento, String codigoDepositante ) {
        return documentoDao.obterDocumentoPorSequenciaIntegracaoNumeroDocumentoCodigoDepositante(sequenciaIntegracao, numeroDocumento, codigoDepositante);
    }

    @Override
    public Documento obterDocumentoPorNumeroDocumento(String numeroDocumento) {
        return documentoDao.obterDocumentoPorNumeroDocumento(numeroDocumento);
    }

    @Override
    public Documento obterDocumentoDIntegracao203(String numeroDocumento, String agrupador,  String codigoDepositante, BigDecimal tipoIntegracao){
        return documentoDao.obterDocumentoDIntegracao203(numeroDocumento, agrupador,codigoDepositante, tipoIntegracao);
    }

    @Override
    public Documento obterDocumentoParaCancelamento(String numeroDocumento, String codigoDepositante, BigDecimal tipoIntegracao){
        return documentoDao.obterDocumentoParaCancelamento(numeroDocumento, codigoDepositante, tipoIntegracao);
    }

    @Override
    public List<DocumentoDetalhe> obterDocumentoDetalhePorSequenciaIntegracao(BigDecimal sequenciaIntegracao) {
        return documentoDao.obterListaDocumentoDetalhePorSequenciaIntegracao(sequenciaIntegracao);
    }

    @Override
    public List<DocumentoDetalhe> obterListaDocumentoDetalhePorSequenciaIntegracaoNumedoDocumento(BigDecimal sequenciaIntegracao, String numeroDocumento) {
        return documentoDao.obterListaDocumentoDetalhePorSequenciaIntegracao(sequenciaIntegracao);
    }

    @Override
    public DocumentoDetalhe obterDocumentoDetalhePorSequenciaIntegracaoNumedoDocumento(BigDecimal sequenciaIntegracao, BigDecimal sequenciaDocumento) {
        return documentoDao.obterDocumentoDetalhePorSequenciaIntegracaoNumedoDocumento(sequenciaIntegracao, sequenciaDocumento);
    }

    @Override
    public DocumentoDetalhe obterDocumentoDetalhePorSequenciaIntegracaoDetalhe(BigDecimal sequenciaIntegracao, BigDecimal sequenciaDocumentoDetalhe ) {
        return documentoDao.obterDocumentoDetalhePorSequenciaIntegracaoDetalhe(sequenciaIntegracao, sequenciaDocumentoDetalhe);
    }

    @Override
    public DocumentoDetalhe obterDocumentoDetalheSalvo(BigDecimal sequenciaIntegracao, BigDecimal sequenciaDocumentoDetalhe ) {
        return documentoDao.obterDocumentoDetalheSalvo(sequenciaIntegracao, sequenciaDocumentoDetalhe);
    }


}
