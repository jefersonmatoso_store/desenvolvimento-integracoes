package br.com.storeautomacao.sync.repository;

import br.com.storeautomacao.sync.model.entity.Embalagem;
import br.com.storeautomacao.sync.model.entity.Integracao;
import br.com.storeautomacao.sync.model.entity.Mercadoria;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by alexandre on 21/09/17.
 */
@Repository
public interface IntegracaoRepository {

    String salvarOperacaoEntrada(Mercadoria entradaMercadoriaEmAndamento);

    String atualizarOperacaoEntrada(Mercadoria entradaMercadoriaEmAndamento);

    String salvarOperacaoSaida(Mercadoria entradaMercadoriaEmAndamento);

    String salvarOperacaoPendente(Mercadoria entradaMercadoriaEmAndamento);

    Mercadoria obterEntradaMercadoriaEmAndamentoPorSequenciaIntegracao(BigDecimal sequenciaIntegracao);

    List<Integracao> listaIntegracaoEntradaMercadoriaPrevisaoDeEntrada() ;

    List<Integracao> listaIntegracaoEntradaMercadoriaPrevisaoDeSaida() ;

    List<Integracao> listaIntegracaoLogix();

    void atualizar(String sql);

    void salvar(String sql);

    BigDecimal salvarIntegracao(String sql);

    BigDecimal salvarDocumento(String sql);

    String salvarEmbalagem(List<Embalagem> embalagems);

    List<Embalagem> obterEmbalagens(String code);

    String obterSequenciaIntegracao(String sql);

}
