package br.com.storeautomacao.sync.security.service;

import br.com.storeautomacao.sync.exception.ServiceException;
import br.com.storeautomacao.sync.model.entity.User;
import br.com.storeautomacao.sync.service.UserService;
import br.com.storeautomacao.sync.util.SecurityUtil;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

@Service
public class JsonWebTokenService {

    @Value("${security.token.secret.key}")
    private String tokenSecretKey;

    @Value("${security.token.expiratio.time}")
    private String tokenExpirationTime;

    @Autowired
    private UserService userService;

    public String getToken(final String username, final String password) {
        if (StringUtils.isEmpty(username) || StringUtils.isEmpty(password)) {
            return null;
        }

        User user = userService.findByUsername(username);
        Map<String, Object> tokenData = new HashMap<>();
        if (StringUtils.equals(SecurityUtil.encript(password), user.getPassword())) {

            tokenData.put("clientType", "user");
            tokenData.put("userID", user.getId());
            tokenData.put("username", user.getUserName());
            tokenData.put("token_create_date", LocalDateTime.now());
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.MINUTE, Integer.valueOf(tokenExpirationTime));
            tokenData.put("token_expiration_date", calendar.getTime());
            tokenData.put("roles", user.getAuthorities());
            JwtBuilder jwtBuilder = Jwts.builder();
            jwtBuilder.setExpiration(calendar.getTime());
            jwtBuilder.setClaims(tokenData);
            return jwtBuilder.signWith(SignatureAlgorithm.HS512, tokenSecretKey).compact();

        } else {
            throw new ServiceException("Authentication error " + this.getClass().getName());
        }
    }

}
