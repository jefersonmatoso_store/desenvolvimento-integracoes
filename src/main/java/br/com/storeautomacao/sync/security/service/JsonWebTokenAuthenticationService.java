package br.com.storeautomacao.sync.security.service;

import br.com.storeautomacao.sync.exception.ServiceException;
import br.com.storeautomacao.sync.model.entity.User;
import br.com.storeautomacao.sync.security.model.UserAuthentication;
import br.com.storeautomacao.sync.service.UserService;
import io.jsonwebtoken.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

@Service
public class JsonWebTokenAuthenticationService {

    @Value("${security.token.secret.key}")
    private String secretKey;

   @Value("${security.token.auth.header.name}")
    private String AUTH_HEADER_NAME;

    @Autowired
    private UserService userService;

    public Authentication authenticate(final HttpServletRequest request) {
        final String token = request.getHeader(AUTH_HEADER_NAME);
        final Jws<Claims> tokenData = parseToken(token);
        if (tokenData != null) {
            User user = getUserFromToken(tokenData);
            if (user != null) {
                return new UserAuthentication(user);
            }
        }
        return null;
    }

    private Jws<Claims> parseToken(final String token) {
        if (token != null) {
            try {
                return Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token);
            } catch (ExpiredJwtException | UnsupportedJwtException | MalformedJwtException
                    | SignatureException | IllegalArgumentException e) {
                return null;
            }
        }
        return null;
    }

    private User getUserFromToken(final Jws<Claims> tokenData) {
        try {
            return userService.findByUsername(tokenData.getBody().get("username").toString());
        } catch (Exception e) {
            throw new ServiceException("User " + tokenData.getBody().get("username").toString() + " not found");
        }
    }

}