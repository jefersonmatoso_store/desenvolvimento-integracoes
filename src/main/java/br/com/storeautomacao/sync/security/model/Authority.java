package br.com.storeautomacao.sync.security.model;

import org.springframework.security.core.GrantedAuthority;


public enum Authority implements GrantedAuthority {

    // Visitante
    ROLE_CREATE_VISIT,
    ROLE_READ_VISIT,
    ROLE_UPDATE_VISIT,
    ROLE_DELETE_VISIT,

    // Encomenda
    ROLE_CREATE_ORDER,
    ROLE_READ_ORDER,
    ROLE_UPDATE_ORDER,
    ROLE_DELETE_ORDER,

    // Manutencao:
    ROLE_CREATE_MAINTENANCE,
    ROLE_READ_MAINTENANCE,
    ROLE_UPDATE_MAINTENANCE,
    ROLE_DELETE_MAINTENANCE,

    ROLE_USER,
    ROLE_ADMIN,
    ANONYMOUS;

    @Override
    public String getAuthority() {
        return this.name();
    }
}
