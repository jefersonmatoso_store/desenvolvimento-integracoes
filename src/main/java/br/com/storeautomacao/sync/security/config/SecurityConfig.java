package br.com.storeautomacao.sync.security.config;

import br.com.storeautomacao.sync.security.filter.AuthenticationTokenFilter;
import br.com.storeautomacao.sync.security.service.JsonWebTokenAuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@EnableGlobalMethodSecurity(prePostEnabled = true)
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private JsonWebTokenAuthenticationService tokenAuthenticationService;

    @Override
    protected void configure(final HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/users/login").permitAll()

                .antMatchers(HttpMethod.POST, "/maintenance/**").hasRole("CREATE_MAINTENANCE")
                .antMatchers(HttpMethod.GET,"/maintenance/**").hasRole("READ_MAINTENANCE")
                .antMatchers(HttpMethod.PUT,"/maintenance/**").hasRole("UPDATE_MAINTENANCE")

                .antMatchers(HttpMethod.POST, "/order/create").hasRole("CREATE_ORDER")
                .antMatchers(HttpMethod.GET,"/order/**").hasRole("READ_ORDER")
                .antMatchers(HttpMethod.PUT,"/order/**").hasRole("UPDATE_ORDER")
                .antMatchers(HttpMethod.DELETE,"/order/**").hasRole("DELETE_ORDER")

                .antMatchers(HttpMethod.POST, "/visit/**").hasRole("CREATE_VISIT")
                .antMatchers(HttpMethod.GET,"/visit/**").hasRole("READ_VISIT")
                .antMatchers(HttpMethod.PUT,"/visit/**").hasRole("UPDATE_VISIT")

                .antMatchers("/users/register/{idPerson}").permitAll()
                .antMatchers("/person/invite/{uuid}").permitAll()
                .antMatchers("/person/create").permitAll()
                .antMatchers("/invitation/update").permitAll()
                .antMatchers("/invitation/search/{email}").permitAll()
                .antMatchers("/invitation/accept/{email}").permitAll()
                .antMatchers("/invitation/confirm/{email}").permitAll()
                .antMatchers("/**").permitAll() // TESTE
                .anyRequest().authenticated()
                .and()
                .addFilterBefore(new AuthenticationTokenFilter(tokenAuthenticationService),
                        UsernamePasswordAuthenticationFilter.class)
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .csrf().disable();
    }

    @Override
    protected void configure(final AuthenticationManagerBuilder auth) throws Exception {
        super.configure(auth);
    }

    /* To allow Pre-flight [OPTIONS] request from browser */
    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers(HttpMethod.OPTIONS, "/**");
    }

    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

}
