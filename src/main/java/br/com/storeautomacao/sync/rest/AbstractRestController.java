package br.com.storeautomacao.sync.rest;

import br.com.storeautomacao.sync.http.HttpResponse;
import br.com.storeautomacao.sync.model.entity.User;
import br.com.storeautomacao.sync.security.model.UserAuthentication;
import br.com.storeautomacao.sync.util.HTTPStatusEnum;
import br.com.storeautomacao.sync.util.UserAgentUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.security.core.context.SecurityContextHolder;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.Collection;

/**
 * Created by alexandre on 30/08/17.
 */
public abstract class AbstractRestController {

    @Value("${URL.BASE.SOCKET}")
    private String URL_BASE_SOCKET;

    private Socket socket;

    /**
     * Retorna o Usuário logado e autenticado.
     *
     * @return
     */
    protected User getUserAuthenticated() {
        UserAuthentication ua = (UserAuthentication) SecurityContextHolder.getContext().getAuthentication();
        if (ua != null) {
            if (ua.getUser() != null && ua.isAuthenticated() && ua.getUser() instanceof User) {
                return ua.getUser();
            }
        }

        return null;
    }

    protected Serializable getResponseOk(String userAgent, String message, Object object) throws IOException {

        Serializable response = null;
        if (UserAgentUtil.isMobileDevice(userAgent)) {

            if (object instanceof String) {
                response = httpResponseOk(message, object);
            } else {

                // Objetos "nao string" (POJO, Collection, etc):
                String json = objectToStringJson(object);
                response = httpResponseOk(message, json);
            }
        } else {

            response = object instanceof Collection ? ((Collection) object).toArray() : (Serializable) object;
        }

        return response;
    }

    protected Serializable getResponseNoContent(String userAgent, String message) throws IOException {

        return UserAgentUtil.isMobileDevice(userAgent)
                ? httpResponseNoContent(message, Boolean.FALSE)
                : null;
    }

    protected Serializable getResponseInternalServerError(String userAgent, String message, Object object) {

        return UserAgentUtil.isMobileDevice(userAgent)
                ? httpResponseInternalServerError(message, object)
                : (Serializable) object;
    }

    /**
     * Retorna o HTTP Response para "OK" - cod 200
     *
     * @param message
     * @return
     */
    protected HttpResponse httpResponseOk(String message, Object object) {
        return this.createHttpResponse(HTTPStatusEnum.STATUS_200, message, object);
    }

    /**
     * Retorna o HTTP Response para "Sem Conteudo" - cod 204
     *
     * @param message
     * @return
     */
    protected HttpResponse httpResponseNoContent(String message, Object object) {
        return this.createHttpResponse(HTTPStatusEnum.STATUS_204, message, object);
    }

    /**
     * Retorna o HTTP Response para "Erro Interno do Servidor" - cod 501
     *
     * @param message
     * @return
     */
    protected HttpResponse httpResponseInternalServerError(String message, Object object) {
        return this.createHttpResponse(HTTPStatusEnum.STATUS_501, message, object);
    }

    private HttpResponse createHttpResponse(HTTPStatusEnum status, String message, Object object) {
        HttpResponse httpResponse = new HttpResponse();
        httpResponse.setResponseCode(status.getStatusCode());
        httpResponse.setResponseDescription(message);
        httpResponse.setObject(object);

        return httpResponse;
    }

    /**
     * @param socketEvent
     * @param obj
     * @throws Exception
     * @deprecated Em breve, utilizar o serviço 'nicbrain - Mensageria'
     */
    @Async
    protected void emmitSocketEvent(String socketEvent, Object obj) throws Exception {

        String json = objectToStringJson(obj);

        if (socket == null){
            socket = IO.socket(URL_BASE_SOCKET);
        }

        if (!socket.connected()){
            socket.on(Socket.EVENT_CONNECT, new Emitter.Listener() {

                @Override
                public void call(Object... args) {
                    socket.emit(socketEvent, json);
                    //socket.disconnect();
                }

            }).on("event", new Emitter.Listener() {

                @Override
                public void call(Object... args) {
                }

            }).on(Socket.EVENT_DISCONNECT, new Emitter.Listener() {

                @Override
                public void call(Object... args) {
                }

            });


            socket.connect();

        } else {
            socket.emit(socketEvent, json);
        }



    }

    /**
     * Transforma um objeto em um String no formato JSON.
     *
     * @param ojb
     * @return
     * @throws IOException
     */
    protected String objectToStringJson(Object ojb) throws IOException {
        StringWriter sw = new StringWriter();
        new ObjectMapper().writeValue(sw, ojb);
        return sw.toString();
    }


    /**
     * @param socketEvent
     * @param obj
     * @throws Exception
     * @deprecated Em breve, utilizar o serviço 'nicbrain - Mensageria'
     */
    @Async
    protected void emmitSocketEvent(String socketEvent) throws Exception {

        if (socket == null){
            socket = IO.socket(URL_BASE_SOCKET);
        }

        if (!socket.connected()){
            socket.on(Socket.EVENT_CONNECT, new Emitter.Listener() {

                @Override
                public void call(Object... args) {
                    socket.emit(socketEvent);
                    //socket.disconnect();
                }

            }).on("event", new Emitter.Listener() {

                @Override
                public void call(Object... args) {
                }

            }).on(Socket.EVENT_DISCONNECT, new Emitter.Listener() {

                @Override
                public void call(Object... args) {
                }

            });


            socket.connect();

        } else {
            socket.emit(socketEvent);
        }



    }

}
