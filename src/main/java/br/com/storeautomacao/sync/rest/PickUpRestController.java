package br.com.storeautomacao.sync.rest;

import br.com.storeautomacao.sync.connector.RestApiConnector;
import br.com.storeautomacao.sync.model.entity.*;
import br.com.storeautomacao.sync.repository.ProductRepository;
import br.com.storeautomacao.sync.service.OrderService;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 */
@RestController
@RequestMapping(value = "/pick-up")
public class PickUpRestController extends AbstractRestController {

    @Autowired
    private OrderService service;

    @Autowired
    private RestApiConnector connector;

    @Autowired
    private ProductRepository repository;

    @CrossOrigin
    @RequestMapping(value = "/missing-customer-orders", method = RequestMethod.GET)
    public Serializable findAllMissingCustomerOrders() {

        Serializable response = null;
        try {

            Credencial credencial = new Credencial();
            credencial.setLogin("store");
            credencial.setPassword("store");
            Token token = connector.login(credencial);

            if (token != null) {
                response = httpResponseOk("Login realizado com sucesso", token);
            } else {
                response = httpResponseNoContent("Não foi possível realizar o login", Boolean.FALSE);
            }

            List<MissingCustomerOrder> missingCustomerOrders = connector.getMissingCustomerOrder(token);
            if (CollectionUtils.isNotEmpty(missingCustomerOrders)) {
                response = httpResponseOk("Encomendas(s) encontrada(s) com sucesso", missingCustomerOrders);

                try{
                    repository.limparTodasColetas();
                }catch (Exception ex){

                }
                String idNumeroOrdem = String.valueOf(missingCustomerOrders.get(0).getIdCustomerOrder());
                limpaOsProdutos();

            } else {
                response = httpResponseNoContent("Não foi possível encontrar nenhuma Encomenda", Boolean.FALSE);
            }
        } catch (Exception e) {
            response = httpResponseInternalServerError(e.getMessage(), e);
        }

        return response;
    }

    @CrossOrigin
    @RequestMapping(value = "/customerOrder/{idCustomerOrder}/items", method = RequestMethod.GET)
    public Serializable findAllcustomerOrderItens(@PathVariable("idCustomerOrder") String idCustomerOrder) {

        Serializable response = null;
        try {

            Credencial credencial = new Credencial();
            credencial.setLogin("store");
            credencial.setPassword("store");
            Token token = connector.login(credencial);

            if (token != null) {
                response = httpResponseOk("Login realizado com sucesso", token);
            } else {
                response = httpResponseNoContent("Não foi possível realizar o login", Boolean.FALSE);
            }

            List<ListOrderItem> listOrderItems = connector.getCustomerListOrderItem(token, idCustomerOrder);

            if (CollectionUtils.isNotEmpty(listOrderItems)) {
                response = httpResponseOk("Encomendas(s) encontrada(s) com sucesso", listOrderItems);
            } else {
                response = httpResponseNoContent("Não foi possível encontrar nenhuma Encomenda", Boolean.FALSE);
            }
        } catch (Exception e) {
            response = httpResponseInternalServerError(e.getMessage(), e);
        }

        return response;
    }

    @CrossOrigin
    @RequestMapping(value = "/nfc/{idFuncionario}/checkin", method = RequestMethod.GET)
    public Serializable customerOrdercheckin(@PathVariable("idFuncionario") String idFuncionario) {


        /**
         * ################# Configurações ######################
         */

        // Usuario do cartão
        Map<String, String> mapCard = new HashMap<String, String>();
        mapCard.put("900000001", "Eduardo");
        mapCard.put("900000002", "Jeferson");
        mapCard.put("900000003", "Gilberto");
        String nomeFuncionario = mapCard.get(idFuncionario);

        // Etiqueta
        Map<Integer, String> mapMacAdress = new HashMap<Integer, String>();
        mapMacAdress.put(1, "3889DC0002D0120D");
        mapMacAdress.put(2, "3889DC0002D01209");
        mapMacAdress.put(3, "3889DC0002D01211");


        Serializable response = null;
        try {

            Credencial credencial = new Credencial();
            credencial.setLogin("store");
            credencial.setPassword("store");
            Token token = connector.login(credencial);

            if (token != null) {
                response = httpResponseOk("Login realizado com sucesso", token);
            } else {
                response = httpResponseNoContent("Não foi possível realizar o login", Boolean.FALSE);
            }

            //Buscou os pedidos em separação
            List<MissingCustomerOrder> missingCustomerOrders = connector.getMissingCustomerOrder(token);



            if(missingCustomerOrders.size() == 0){
                response = httpResponseNoContent("Não foi possível encontrar nenhuma Encomenda", Boolean.FALSE);

                emmitSocketEvent("pick-by-display");
                //repository.insertProductStaging(productsStaging);
                return response;
            }

            // Pegou o primeiro pedido da fila
            String idNumeroOrdem = String.valueOf(missingCustomerOrders.get(0).getIdCustomerOrder());
            int numeroCheckin = 0;

            try{
                List<PickUp> ordemList = repository.buscarOrdemPorNumero(idNumeroOrdem);
                //numeroCheckin += ordemList.get(0).getCheckin();

                System.out.println("Aqui");
                if (ordemList.size() > 0) {
                    response = httpResponseNoContent("Separação em progresso ", Boolean.FALSE);
                    emmitSocketEvent("inprogress");
                    return response;
                }
            } catch (Exception ex){
                System.out.println("Aqui");
            }


            List<ListOrderItem> listOrderItems = connector.getCustomerListOrderItem(token, idNumeroOrdem);

            int cont = 1;
            for (ListOrderItem order: listOrderItems) {
                ProductsStaging productsStaging = new ProductsStaging();
                productsStaging.setNotUsed("");
                String productId = order.getItem().getCode();
                productsStaging.setProductId(productId.substring(productId.length()-5,productId.length()));
                productsStaging.setBarcode(order.getItem().getCode());
                productsStaging.setDescription(order.getItem().getDescription());
                productsStaging.setGroup(null);
                String orderNumber = order.getCustomerOrderItem().getCustomerOrder().getOrderNumber();
                productsStaging.setStandardPrice(orderNumber.substring(orderNumber.length()-8,orderNumber.length()));
                productsStaging.setSellPrice(nomeFuncionario);
                productsStaging.setDiscount(String.valueOf(order.getId()));
                productsStaging.setContent(String.valueOf(order.getQuantity()));
                productsStaging.setUnit(order.getUnitMeasure().getInitials());
                productsStaging.setDelete("");


                String macAdress = mapMacAdress.get(cont);


               // if(numeroCheckin > 0){
                    try {
                        //repository.limparEtiqueta(macAdress);
                    } catch (Exception ex) {
                        System.out.println("Problema ao limpar etiqueta: " + ex.getMessage());
                    }

                    try {
                        repository.insertProductStaging(productsStaging);
                    } catch (Exception ex) {
                        System.out.println("Problema ao inserir produto: " + ex.getMessage());
                    }

                    try {
                        repository.insertLink(productsStaging.getProductId(), macAdress);
                    } catch (Exception ex) {
                        System.out.println("Problema ao linkar produto: " + ex.getMessage());
                    }

                    try {
                        repository.acende(macAdress, nomeFuncionario);
                    } catch (Exception ex) {
                        System.out.println("Erro: " + ex.getMessage());
                    }
                }

                cont++;
            //}


            //numeroCheckin++;
            if (CollectionUtils.isNotEmpty(listOrderItems)) {
                // Dispara o evento de mudar a tela
                emmitSocketEvent("pick-by-display");
                try {
                    repository.registrarColeta(idNumeroOrdem, "EM_SEPARACAO", numeroCheckin);
                } catch (Exception ex) {
                    System.out.println("Erro: " + ex.getMessage());
                }

                response = httpResponseOk("Encomendas(s) encontrada(s) com sucesso", listOrderItems);
            } else {
                response = httpResponseNoContent("Não foi possível encontrar nenhuma Encomenda", Boolean.FALSE);
            }
        } catch (Exception e) {
            response = httpResponseInternalServerError(e.getMessage(), e);
        }

        return response;
    }


    @CrossOrigin
    @RequestMapping(value = "/missing-customer-orders/android", method = RequestMethod.GET)
    public List<MissingCustomerOrder> findAllMissingCustomerOrdersAndroid() {
        List<MissingCustomerOrder> missingCustomerOrders = new ArrayList<>();
        Serializable response = null;
        try {

            Credencial credencial = new Credencial();
            credencial.setLogin("store");
            credencial.setPassword("store");
            Token token = connector.login(credencial);

            if (token != null) {
                response = httpResponseOk("Login realizado com sucesso", token);
            } else {
                response = httpResponseNoContent("Não foi possível realizar o login", Boolean.FALSE);
            }

            missingCustomerOrders = connector.getMissingCustomerOrder(token);
            if (CollectionUtils.isNotEmpty(missingCustomerOrders)) {
                //response = httpResponseOk("Encomendas(s) encontrada(s) com sucesso", missingCustomerOrders);
            } else {
                response = httpResponseNoContent("Não foi possível encontrar nenhuma Encomenda", Boolean.FALSE);
            }
        } catch (Exception e) {
            response = httpResponseInternalServerError(e.getMessage(), e);
        }

        return missingCustomerOrders;
    }


    @CrossOrigin
    @RequestMapping(value = "/nfc/checkout", method = RequestMethod.GET)
    public Serializable checkout() {

        try {

            limpaOsProdutos();

            repository.limparTodasColetas();
        } catch (Exception ex) {
            System.out.println("Erro: " + ex.getMessage());
        }
        return null;
    }


    public void limpaOsProdutos() {


        // Usuario do cartão


        // Etiqueta
        Map<Integer, String> mapMacAdress = new HashMap<Integer, String>();
        mapMacAdress.put(1, "3889DC0002D0120D");
        mapMacAdress.put(2, "3889DC0002D01209");
        mapMacAdress.put(3, "3889DC0002D01211");

        Map<Integer, String> mapProdutos = new HashMap<Integer, String>();
        mapProdutos.put(1, "7891991001373");
        mapProdutos.put(2, "7892840120214");
        mapProdutos.put(3, "7896004002538");

        Credencial credencial = new Credencial();
        credencial.setLogin("store");
        credencial.setPassword("store");
        //Token token = connector.login(credencial);



       // List<MissingCustomerOrder> missingCustomerOrders = connector.getMissingCustomerOrder(token);


       // List<ListOrderItem> listOrderItems = connector.getCustomerListOrderItem(token, String.valueOf(missingCustomerOrders.get(0).getIdCustomerOrder()));

        for (Map.Entry<Integer, String> entry : mapProdutos.entrySet())
        {
            int id = entry.getKey();
            String barcode = entry.getValue();

            ProductsStaging productsStaging = new ProductsStaging();
            productsStaging.setNotUsed("");
            String productId = barcode;
            productsStaging.setProductId(productId.substring(productId.length() - 5, productId.length()));
            productsStaging.setBarcode(barcode);
            productsStaging.setDescription(null);
            productsStaging.setGroup(null);
            productsStaging.setStandardPrice("");
            productsStaging.setSellPrice("");
            productsStaging.setDiscount("");
            productsStaging.setContent("");
            productsStaging.setUnit(null);
            productsStaging.setDelete("");


            String macAdress = mapMacAdress.get(id);


            // if(numeroCheckin > 0){
            try {
                //repository.limparEtiqueta(macAdress);
            } catch (Exception ex) {
                System.out.println("Problema ao limpar etiqueta: " + ex.getMessage());
            }

            try {
                repository.insertProductStaging(productsStaging);
            } catch (Exception ex) {
                System.out.println("Problema ao inserir produto: " + ex.getMessage());
            }

            try {
                repository.insertLink(productsStaging.getProductId(), macAdress);
            } catch (Exception ex) {
                System.out.println("Problema ao linkar produto: " + ex.getMessage());
            }

            try {
                repository.acende(macAdress, "");
            } catch (Exception ex) {
                System.out.println("Erro: " + ex.getMessage());
            }
        }


    }




}
