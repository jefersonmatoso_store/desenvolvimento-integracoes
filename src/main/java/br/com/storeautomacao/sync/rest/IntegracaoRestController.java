package br.com.storeautomacao.sync.rest;

import br.com.storeautomacao.sync.connector.RestApiConnector;
import br.com.storeautomacao.sync.connector.SoapConnector;
import br.com.storeautomacao.sync.dto.ItemDto;
import br.com.storeautomacao.sync.dto.ProdutoDto;
import br.com.storeautomacao.sync.dto.SaleDto;
import br.com.storeautomacao.sync.model.entity.*;
import br.com.storeautomacao.sync.model.wms.ConfirmacaoConferencia;
import br.com.storeautomacao.sync.model.wms.Header;
import br.com.storeautomacao.sync.model.wms.Inventory.Inventory;
import br.com.storeautomacao.sync.model.wms.Inventory.LgfDataInventory;
import br.com.storeautomacao.sync.model.wms.PurchaseOrder.OrdemDeCompra;
import br.com.storeautomacao.sync.model.wms.Shipment.LgfDataShipped;
import br.com.storeautomacao.sync.service.*;
import br.com.storeautomacao.sync.util.SqlUtil;
import br.com.storeautomacao.sync.util.XmlUtil;
import br.com.storeautomacao.sync.wsdl.otm.ItemType;
import br.com.storeautomacao.sync.wsdl.otm.ShipmentType;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.naming.NoNameCoder;
import com.thoughtworks.xstream.io.xml.DomDriver;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBIntrospector;
import javax.xml.bind.Unmarshaller;
import java.io.Serializable;
import java.io.StringReader;
import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

/**
 *
 */
@RestController
@RequestMapping(value = "/integracao")
public class IntegracaoRestController extends AbstractRestController {

    static Logger logger = LoggerFactory.getLogger(IntegracaoRestController.class);


    @Autowired
    private OrderService service;

    @Autowired
    private RestApiConnector connector;

    @Autowired
    private MercadoriaService mercadoriaService;

    @Autowired
    private IntegracaoService integracaoService;

    @Autowired
    private SoapConnector soapConnector;

    @Autowired
    DocumentoDetalheSequenciaService documentoDetalheSequenciaService;

    @Autowired
    ProdutoService produtoService;

    @CrossOrigin
    @RequestMapping(value = "/test/oracle/conect", method = RequestMethod.GET)
    public Serializable test() {

        Serializable response = null;
        try {
            Sistema sistema = integracaoService.test();
            if (sistema != null) {
                response = httpResponseOk("Conexão executada com sucesso", sistema);
            } else {
                response = httpResponseNoContent("Conexão não executada", Boolean.FALSE);
            }
        } catch (Exception e) {
            response = httpResponseInternalServerError(e.getMessage(), e);
        }

        return response;
    }

    /**
     *
     * LOGIX
     */

    @CrossOrigin
    @RequestMapping(value = "/logix/produto", method = RequestMethod.PUT)
    public Serializable integracaoLogix() {

        Serializable response = null;
        try {
            //int numeroDeOperacoes = mercadoriaService.entrada(BigDecimal.valueOf(8197594));
            List<Integracao> listaIntegracao = integracaoService.listaIntegracaoLogix();

            if (listaIntegracao.size() > 0) {
                response = httpResponseOk("Integrações saída executada", listaIntegracao);
            } else {
                response = httpResponseNoContent("Nenhuma operação executada", Boolean.FALSE);
            }
        } catch (Exception e) {
            response = httpResponseInternalServerError(e.getMessage(), e);
        }

        return response;
    }


    @CrossOrigin
    @RequestMapping(value = "/mercadoria/entrada", method = RequestMethod.PUT)
    public Serializable executarEntradaMerdacodia() {

        Serializable response = null;
        try {
            //int numeroDeOperacoes = mercadoriaService.entrada(BigDecimal.valueOf(8197594));
            List<Integracao> listaIntegracao = integracaoService.entradaMercadoria();

            if (listaIntegracao.size() > 0) {
                response = httpResponseOk("Integrações executadas", listaIntegracao);
            } else {
                response = httpResponseNoContent("Nenhuma operação executada", Boolean.FALSE);
            }
        } catch (Exception e) {
            response = httpResponseInternalServerError(e.getMessage(), e);
        }

        return response;
    }

    @CrossOrigin
    @RequestMapping(value = "/conferencia/pedido/", method = RequestMethod.POST)
    public Serializable conferenciaPedido(@RequestBody String strXml) {

        Serializable response = null;
        ConfirmacaoConferencia confirmacaoConferencia = null;
        logger.info("######## Recebendo XML - Conferência de pedido ##########");


        try {
            XStream xstream = new XStream();
            xstream.processAnnotations(ConfirmacaoConferencia.class);
            xstream.processAnnotations(Header.class);
            String n = strXml.replace("\n", "").replace("\t", "");
            confirmacaoConferencia = (ConfirmacaoConferencia) xstream.fromXML(n);

           // logger.info("Recebendo XML -> " + strXml);
            //response = httpResponseOk("Conferencia executada", strXml);
            //return response;

        } catch (Exception e) {
            logger.info("Problema ao coverter XML -> " + e.getMessage());
            return httpResponseNoContent("Erro na conversão do XML", e.getMessage());
        }

        try {
            String estadoIntegracao = integracaoService.conferenciaPedido(confirmacaoConferencia);
            if (StringUtils.isNotEmpty(estadoIntegracao)) {
                //logger.info("++++++++ Conferencia executada ++++++++++");
                response = httpResponseOk("Conferencia executada", estadoIntegracao);
            } else {
                //logger.info("---------- Conferencia nao executada ------------");
                response = httpResponseNoContent("Operação não executada", estadoIntegracao);
            }
        } catch (Exception e) {
           //logger.info("++++++++ Erro ++++++++++" + e.getMessage());
            response = httpResponseInternalServerError(e.getMessage(), e);
        }

        return response;
    }

    @CrossOrigin
    @RequestMapping(value = "/mercadoria/saida", method = RequestMethod.PUT)
    public Serializable executarSaidaMerdacodia() {

        Serializable response = null;
        try {
            //int numeroDeOperacoes = mercadoriaService.entrada(BigDecimal.valueOf(8197594));
            List<Integracao> listaIntegracao = integracaoService.saidaMercadoria();

            if (listaIntegracao.size() > 0) {
                response = httpResponseOk("Integrações saída executada", listaIntegracao);
            } else {
                response = httpResponseNoContent("Nenhuma operação executada", Boolean.FALSE);
            }
        } catch (Exception e) {
            response = httpResponseInternalServerError(e.getMessage(), e);
        }

        return response;
    }


    @CrossOrigin
    @RequestMapping(value = "/shipment", method = RequestMethod.POST, produces = {"application/xml", "text/xml"}, consumes = MediaType.ALL_VALUE)
    public Serializable getShipment(@RequestBody String strXmlShipment) {

        logger.info("######## RECEBENDO SHIPMENT ####### ");

        Serializable response = null;
        try {

            XStream xstream = new XStream();
            xstream.processAnnotations(ConfirmacaoConferencia.class);
            xstream.processAnnotations(Header.class);
            ShipmentType shipmentType = (ShipmentType) xstream.fromXML(strXmlShipment);

            logger.info("XML -> " + shipmentType);

            JAXBContext jc = JAXBContext.newInstance(ShipmentType.class);
            Unmarshaller unmarshaller = jc.createUnmarshaller();
            JAXBIntrospector jaxbIntrospector = jc.createJAXBIntrospector();

            ShipmentType shipmentTypeJ = (ShipmentType) unmarshaller.unmarshal(new StringReader(strXmlShipment));

            logger.info("JAXB -> " + shipmentTypeJ);
            //Enviar pro connector

            String estadoIntegracao = null;//integracaoService.conferenciaPedido(sequenciaIntegracao, quantidade);
            if (StringUtils.isNotEmpty(estadoIntegracao)) {
                response = httpResponseOk("Confirmacao executada", estadoIntegracao);
            } else {
                response = httpResponseNoContent("Operação não executada", estadoIntegracao);
            }
        } catch (Exception e) {
            response = httpResponseInternalServerError(e.getMessage(), e);
        }

        return response;
    }

    @CrossOrigin
    @RequestMapping(value = "/confirmacao/viagem", method = RequestMethod.POST, produces = {"application/xml", "text/xml"}, consumes = MediaType.ALL_VALUE)
    public Serializable confirmacaoViagem(@RequestBody String strXmlShipment) {

        Serializable response = null;
        try {
            JAXBContext jc = JAXBContext.newInstance(ShipmentType.class);
            Unmarshaller unmarshaller = jc.createUnmarshaller();
            JAXBIntrospector jaxbIntrospector = jc.createJAXBIntrospector();

            ShipmentType shipmentType = (ShipmentType) unmarshaller.unmarshal(new StringReader(strXmlShipment));

            String estadoIntegracao = null;//integracaoService.conferenciaPedido(sequenciaIntegracao, quantidade);
            if (StringUtils.isNotEmpty(estadoIntegracao)) {
                response = httpResponseOk("Confirmacao executada", estadoIntegracao);
            } else {
                response = httpResponseNoContent("Operação não executada", estadoIntegracao);
            }
        } catch (Exception e) {
            response = httpResponseInternalServerError(e.getMessage(), e);
        }

        return response;
    }


    @CrossOrigin
    @RequestMapping(value = "/otm/teste", method = RequestMethod.GET)
    public Serializable testeIntegracaoOtm() {

        Serializable response = null;
        String url = "https://rotmgtm-test-a572805.otm.us2.oraclecloud.com:443/GC3Services/TransmissionService/call";
        String postData = new ItemType().toString();
        String authStr = "GAVE.INTEGRATION:intgav2018";
        String mResponse = "";

        try {

            mResponse = soapConnector.http_client();

            //mResponse = soapConnector.sendXml(null,null,null);

            connector.connectorSendXmlOtm(XmlUtil.getTransmissionHeader(XmlUtil.getXmlLocation()));

        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            if (StringUtils.isNotEmpty(mResponse)) {
                response = httpResponseOk("Confirmacao executada", mResponse);
            } else {
                response = httpResponseNoContent("Operação não executada", Boolean.FALSE);
            }
        } catch (Exception e) {
            response = httpResponseInternalServerError(e.getMessage(), e);
        }

        return response;
    }


    @CrossOrigin
    @RequestMapping(value = "/todas", method = RequestMethod.GET)
    public Serializable listarIntegracaoEmAndamento() {

        Serializable response = null;

        Mercadoria mercadoria;

        try {
            String estadoIntegracao = null;//integracaoService.conferenciaPedido(sequenciaIntegracao, quantidade);
            if (StringUtils.isNotEmpty(estadoIntegracao)) {
                response = httpResponseOk("Confirmacao executada", estadoIntegracao);
            } else {
                response = httpResponseNoContent("Operação não executada", estadoIntegracao);
            }
        } catch (Exception e) {
            response = httpResponseInternalServerError(e.getMessage(), e);
        }

        return response;
    }




    /**
     * POC - Drogaria Sao Paulo
     * Entrada de mercadoria
     */

    @CrossOrigin
    @RequestMapping(value = "/compra/pedido", method = RequestMethod.POST)
    public Serializable compraPedido(@RequestBody String strXml) {

        Serializable response = null;
        OrdemDeCompra ordemDeCompra = null;
        logger.info("Recebendo XML - Conferência de pedido");


        try {
            //String estadoIntegracao = null;
            String estadoIntegracao = integracaoService.pedidoCompraPOCDrogariaSaoPaulo(strXml);
            if (StringUtils.isNotEmpty(estadoIntegracao)) {
                response = httpResponseOk("Pedido de compra executada", estadoIntegracao);
            } else {
                response = httpResponseNoContent("Operação não executada", estadoIntegracao);
            }
        } catch (Exception e) {
            response = httpResponseInternalServerError(e.getMessage(), e);
        }

        return response;
    }


    @CrossOrigin
    @RequestMapping(value = "/conferencia/pedido/drogaria-saopaulo", method = RequestMethod.POST)
    public Serializable conferenciaPedidoDrogariaSaoPaulo(@RequestBody String strXml) {

        Serializable response = null;
        ConfirmacaoConferencia confirmacaoConferencia = null;
        logger.info("Recebendo XML - COnferência de pedido");


        try {
            XStream xstream = new XStream();
            xstream.processAnnotations(ConfirmacaoConferencia.class);
            xstream.processAnnotations(Header.class);
            String n = strXml.replace("\n", "").replace("\t", "");
            confirmacaoConferencia = (ConfirmacaoConferencia) xstream.fromXML(n);

            logger.info("Recebendo XML -> " + strXml);

            logger.info("Recebendo XML -> " + strXml);
            //response = httpResponseOk("Conferencia executada", strXml);
            //return response;

        } catch (Exception e) {
            logger.info("Problema ao coverter XML -> " + e.getMessage());
            return httpResponseNoContent("Erro na conversão do XML", e.getMessage());
        }

        try {
            //String estadoIntegracao = null;
            String estadoIntegracao = integracaoService.pedidoCompraPOCDrogariaSaoPaulo(strXml);
            if (StringUtils.isNotEmpty(estadoIntegracao)) {
                response = httpResponseOk("Pedido de compra executada", estadoIntegracao);
            } else {
                response = httpResponseNoContent("Operação não executada", estadoIntegracao);
            }
        } catch (Exception e) {
            response = httpResponseInternalServerError(e.getMessage(), e);
        }

        return response;
    }

    @CrossOrigin
    @RequestMapping(value = "/teste", method = RequestMethod.GET)
    public Serializable TesteDocumentoDetalheSequencia() {

        Serializable response = null;

        try {
            List<DocumentoDetalheSequencia> documentoDetalheSequencias = documentoDetalheSequenciaService.obterDocumentoDetalheSequenciaPorSequenciaIntegracao("9926451");
            if (CollectionUtils.isNotEmpty(documentoDetalheSequencias)) {

                response = httpResponseOk("DocumentoDetalheSequencia executada", "Sucesso");

                for (DocumentoDetalheSequencia d : documentoDetalheSequencias) {
                    documentoDetalheSequenciaService.gravarDocumentoDetalheSequencia(SqlUtil.getSqlInsertDocumentoDetalheSequencia(d));
                }

            } else {
                response = httpResponseNoContent("DocumentoDetalheSequencia não executada", "Erro");
            }
        } catch (Exception e) {
            response = httpResponseInternalServerError(e.getMessage(), e);
        }

        return response;
    }

    @CrossOrigin
    @RequestMapping(value = "/finalizarCompra", method = RequestMethod.POST)
    public Serializable finalizarCompra(@RequestBody SaleDto saleDto) {

        Serializable response = null;

        try {
            boolean resultado = integracaoService.finalizarCompra(saleDto);
            if (resultado) {
                response = httpResponseOk("Compra executada", Boolean.TRUE);
            } else {
                response = httpResponseNoContent("Compra não executada", Boolean.FALSE);
            }
        } catch (Exception e) {
            response = httpResponseInternalServerError(e.getMessage(), e);
        }

        return response;
    }

    @CrossOrigin
    @RequestMapping(value = "/saldo", method = RequestMethod.POST, produces = {"application/xml", "text/xml"}, consumes = MediaType.ALL_VALUE)
    public Serializable getBalance(@RequestBody String strXmlBalance) {

        Serializable response = null;
        LgfDataInventory saldo = null;
        logger.info("Recebendo XML - COnferência de pedido");

        try {
            XStream xstream = new XStream(new DomDriver("UTF_8", new NoNameCoder()));
            xstream.processAnnotations(LgfDataInventory.class);
            xstream.processAnnotations(Header.class);
            saldo = (LgfDataInventory) xstream.fromXML(strXmlBalance);

            for (Inventory inventory : saldo.getListOfInventory().getInventories()) {
                // produtoService.obterItemPorCodigo(inventory.getItem_alternate_code());
            }

            logger.info("Recebendo XML:  {} ", strXmlBalance);

        } catch (Exception e) {
            logger.info("Problema ao coverter XML -> " + e.getMessage());
            return httpResponseNoContent("Erro na conversão do XML", e.getMessage());
        }

        try {
            response = httpResponseOk("Pedido de compra executada", "Sucesso");

            /*String estadoIntegracao = integracaoService.pedidoCompraPOCDrogariaSaoPaulo(strXmlBalance);
            if (StringUtils.isNotEmpty(estadoIntegracao)) {
                response = httpResponseOk("Pedido de compra executada", estadoIntegracao);
            } else {
                response = httpResponseNoContent("Operação não executada", estadoIntegracao);
            }*/
        } catch (Exception e) {
            response = httpResponseInternalServerError(e.getMessage(), e);
        }

        return response;

    }

    @CrossOrigin
    @RequestMapping(value = "/shipped_load", method = RequestMethod.POST)
    public Serializable getShippedLoad(@RequestBody String strXmlShipped) {
        Serializable response = null;
        try {
            String cargaEnviada = integracaoService.shipload252(strXmlShipped);
            if (StringUtils.isNotEmpty(cargaEnviada)) {
                response = httpResponseOk("Operação executada com sucesso", Boolean.TRUE);
            } else {
                response = httpResponseNoContent("Operação não executada", Boolean.FALSE);
            }
        } catch (Exception e) {
            response = httpResponseInternalServerError(e.getMessage(), e);
        }
        response = httpResponseOk("Shipload recebido com sucesso", Boolean.TRUE);
        return response;
    }

    /**
     * @param <?xml version="1.0" encoding="utf-8"?>
     *               <sync>
     *               <header>
     *               <DocumentVersion>1</DocumentVersion>
     *               <OriginSystem>Compudeck</OriginSystem>
     *               <ClientEnvCode>COW</ClientEnvCode>
     *               <TimeStamp>2012-12-13T12:12:12</TimeStamp>
     *               </header>
     *               <conference>
     *               <code>o que amarraremos aqui?</code>
     *               <weight>12.5</<weight>
     *               <conference>
     *               </sync>
     * @return
     */
    @CrossOrigin
    @RequestMapping(value = "/compudeck/{weight}/weight/{code}/code", method = RequestMethod.GET)
    public String receberPeso(@PathVariable String weight, @PathVariable String code) {

        Serializable response = null;
        OrdemDeCompra ordemDeCompra = null;
        logger.info("Recebendo XML - Conferência de peso" + code);

        // Desenvolver comparacao de peso real x peso teorico e enviar o OK ou NOK para Compudeck
        List<Embalagem> embalagems =  integracaoService.obterEmbalagens(code);

        Optional<Embalagem> optionalEmbalagem = embalagems.stream().filter(e -> e.getOb_lpn_weight().equals(weight)).findFirst();
        if (optionalEmbalagem.isPresent()) {
            logger.info("Embalagens {} ", embalagems);
            // // Em caso de peso Ok Aplicação envia integração para endpoint do WMS Cloud realizando o processo de embarque
            //connector.connectorSendWms(optionalEmbalagem.get());
        }




        //logger.info(strXml);

        return "Conferência realizada com sucesso";
        //return "Peso incorreto. Verifiar o volume";

        /*try {
            //String estadoIntegracao = null;
            String estadoIntegracao = integracaoService.pedidoCompraPOCDrogariaSaoPaulo(strXml);
            if (StringUtils.isNotEmpty(estadoIntegracao)) {
                response = httpResponseOk("Pedido de compra executada", estadoIntegracao);
            } else {
                response = httpResponseNoContent("Operação não executada", estadoIntegracao);
            }
        } catch (Exception e) {
            response = httpResponseInternalServerError(e.getMessage(), e);
        }

        return response;*/
    }


    /**
     * @param <?xml version="1.0" encoding="utf-8"?>
     *               <sync>
     *               <header>
     *               <DocumentVersion>1</DocumentVersion>
     *               <OriginSystem>Compudeck</OriginSystem>
     *               <ClientEnvCode>COW</ClientEnvCode>
     *               <TimeStamp>2012-12-13T12:12:12</TimeStamp>
     *               </header>
     *               <shipload></shipload>
     *               <status></status>
     *               </sync>
     * @return
     */
    @CrossOrigin
    @RequestMapping(value = "/compudeck/shipload/{status}status", method = RequestMethod.GET)
    public String receberDadosViagem(@PathVariable String status) {

        Serializable response = null;
        OrdemDeCompra ordemDeCompra = null;
        logger.info("Recebendo XML - Status da viagem - " + status);

        // logger.info(strXml);

        return "OK";

        /*try {
            //String estadoIntegracao = null;
            String estadoIntegracao = integracaoService.pedidoCompraPOCDrogariaSaoPaulo(strXml);
            if (StringUtils.isNotEmpty(estadoIntegracao)) {
                response = httpResponseOk("Pedido de compra executada", estadoIntegracao);
            } else {
                response = httpResponseNoContent("Operação não executada", estadoIntegracao);
            }
        } catch (Exception e) {
            response = httpResponseInternalServerError(e.getMessage(), e);
        }
    }

        return response;*/
    }


}