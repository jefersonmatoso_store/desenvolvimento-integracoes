package br.com.storeautomacao.sync.rest;

import br.com.storeautomacao.sync.dto.ItemDto;
import br.com.storeautomacao.sync.dto.ProdutoDto;
import br.com.storeautomacao.sync.model.entity.Produto;
import br.com.storeautomacao.sync.model.wms.PurchaseOrder.OrdemDeCompra;
import br.com.storeautomacao.sync.service.ProdutoService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 */
@RestController
@RequestMapping(value = "/produtos")
public class ProdutoRestController extends AbstractRestController {

    static Logger logger = LoggerFactory.getLogger(ProdutoRestController.class);

    @Autowired
    private ProdutoService produtoService;

    @CrossOrigin
    @RequestMapping(value = "/cadastrar", method = RequestMethod.POST)
    public Serializable cadastrarProduto(@RequestBody ProdutoDto produtoDto) {

        Serializable response = null;

        try {
            String id = produtoService.cadastrarProduto(produtoDto);
            if (StringUtils.isNotEmpty(id)) {
                response = httpResponseOk("Produto cadastrado com sucesso", Boolean.TRUE);
            } else {
                response = httpResponseNoContent("Produto não cadastrado", Boolean.FALSE);
            }
        } catch (Exception e) {
            response = httpResponseInternalServerError(e.getMessage(), e);
        }

        return response;
    }

    @CrossOrigin
    @RequestMapping(value = "/item/cadastrar", method = RequestMethod.POST)
    public Serializable cadastrarItem(@RequestBody ItemDto itemDto) {

        Serializable response = null;

        try {
            String id = produtoService.cadastrarItem(itemDto);
            if (StringUtils.isNotEmpty(id)) {
                response = httpResponseOk("Item cadastrado com sucesso", Boolean.TRUE);
            } else {
                response = httpResponseNoContent("Item não cadastrado", Boolean.FALSE);
            }
        } catch (Exception e) {
            response = httpResponseInternalServerError(e.getMessage(), e);
        }

        return response;
    }

    @CrossOrigin
    @RequestMapping(value = "/listar", method = RequestMethod.GET)
    public Serializable listarTodosPordutos() {

        Serializable response = null;

        try {
            List<ProdutoDto> produtos = produtoService.obterTodosProdutos();
            if (CollectionUtils.isNotEmpty(produtos)) {
                response = httpResponseOk("Produtos encontrados com sucesso", produtos);
            } else {
                response = httpResponseNoContent("Nenhum Produto encontrado", new ArrayList<>());
            }
        } catch (Exception e) {
            response = httpResponseInternalServerError(e.getMessage(), e);
        }

        return response;
    }

}
