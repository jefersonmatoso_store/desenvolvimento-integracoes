package br.com.storeautomacao.sync.rest;

import br.com.storeautomacao.sync.model.entity.Mercadoria;
import br.com.storeautomacao.sync.service.MercadoriaService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.Serializable;
import java.util.List;

/**
 *
 */
@RestController
@RequestMapping(value = "/mercadoria")
public class MercadoriaRestController extends AbstractRestController {

    static Logger logger = LoggerFactory.getLogger(MercadoriaRestController.class);



    @Autowired
    private MercadoriaService mercadoriaService;

    @CrossOrigin
    @RequestMapping(value = "/todas", method = RequestMethod.GET)
    public List<Mercadoria> listarIntegracaoEmAndamento() {

        Serializable response = null;

        List<Mercadoria> mercadoria = null;

        try {
           mercadoria = mercadoriaService.listaTodasMercadorias();
            if (mercadoria != null) {
                response = httpResponseOk("Confirmacao executada", mercadoria);
            } else {
                response = httpResponseNoContent("Operação não executada", false);
            }
        } catch (Exception e) {
            response = httpResponseInternalServerError(e.getMessage(), e);
        }

        return mercadoria;
    }


}
