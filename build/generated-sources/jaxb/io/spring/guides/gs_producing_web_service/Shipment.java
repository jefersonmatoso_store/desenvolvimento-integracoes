//
// Este arquivo foi gerado pela Arquitetura JavaTM para Implementação de Referência (JAXB) de Bind XML, v2.2.11 
// Consulte <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas as modificações neste arquivo serão perdidas após a recompilação do esquema de origem. 
// Gerado em: 2018.11.05 às 01:00:08 PM BRST 
//


package io.spring.guides.gs_producing_web_service;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de Shipment complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="Shipment"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ShipmentHeader" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="ShipmentHeader2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SEquipment" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="ContainerGroup" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="ShipmentStop" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Location" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;choice minOccurs="0"&gt;
 *           &lt;element name="ShipUnit" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/&gt;
 *           &lt;element name="SShipUnit" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;/choice&gt;
 *         &lt;element name="ShipUnitViewInfo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Release" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="ShipmentOrderRelease" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="PackagedItem" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="ShipUnitSpec" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="RATE_OFFERING" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="RATE_GEO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TransOrder" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="ShipmentStatus" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="FLIGHT_INSTANCE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SecondaryCharges" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PrimaryShipmentRefInfo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Driver" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PowerUnit" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Shipment", propOrder = {
    "shipmentHeader",
    "shipmentHeader2",
    "sEquipment",
    "containerGroup",
    "shipmentStop",
    "location",
    "shipUnit",
    "sShipUnit",
    "shipUnitViewInfo",
    "release",
    "shipmentOrderRelease",
    "packagedItem",
    "shipUnitSpec",
    "rateoffering",
    "rategeo",
    "transOrder",
    "shipmentStatus",
    "flightinstance",
    "secondaryCharges",
    "primaryShipmentRefInfo",
    "driver",
    "powerUnit"
})
public class Shipment {

    @XmlElement(name = "ShipmentHeader", required = true)
    protected String shipmentHeader;
    @XmlElement(name = "ShipmentHeader2")
    protected String shipmentHeader2;
    @XmlElement(name = "SEquipment")
    protected List<String> sEquipment;
    @XmlElement(name = "ContainerGroup")
    protected List<String> containerGroup;
    @XmlElement(name = "ShipmentStop")
    protected List<String> shipmentStop;
    @XmlElement(name = "Location")
    protected List<String> location;
    @XmlElement(name = "ShipUnit")
    protected List<String> shipUnit;
    @XmlElement(name = "SShipUnit")
    protected List<String> sShipUnit;
    @XmlElement(name = "ShipUnitViewInfo")
    protected String shipUnitViewInfo;
    @XmlElement(name = "Release")
    protected List<String> release;
    @XmlElement(name = "ShipmentOrderRelease")
    protected List<String> shipmentOrderRelease;
    @XmlElement(name = "PackagedItem")
    protected List<String> packagedItem;
    @XmlElement(name = "ShipUnitSpec")
    protected List<String> shipUnitSpec;
    @XmlElement(name = "RATE_OFFERING")
    protected String rateoffering;
    @XmlElement(name = "RATE_GEO")
    protected String rategeo;
    @XmlElement(name = "TransOrder")
    protected List<String> transOrder;
    @XmlElement(name = "ShipmentStatus")
    protected List<String> shipmentStatus;
    @XmlElement(name = "FLIGHT_INSTANCE")
    protected String flightinstance;
    @XmlElement(name = "SecondaryCharges")
    protected String secondaryCharges;
    @XmlElement(name = "PrimaryShipmentRefInfo")
    protected String primaryShipmentRefInfo;
    @XmlElement(name = "Driver")
    protected String driver;
    @XmlElement(name = "PowerUnit")
    protected String powerUnit;

    /**
     * Obtém o valor da propriedade shipmentHeader.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipmentHeader() {
        return shipmentHeader;
    }

    /**
     * Define o valor da propriedade shipmentHeader.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipmentHeader(String value) {
        this.shipmentHeader = value;
    }

    /**
     * Obtém o valor da propriedade shipmentHeader2.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipmentHeader2() {
        return shipmentHeader2;
    }

    /**
     * Define o valor da propriedade shipmentHeader2.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipmentHeader2(String value) {
        this.shipmentHeader2 = value;
    }

    /**
     * Gets the value of the sEquipment property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the sEquipment property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSEquipment().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getSEquipment() {
        if (sEquipment == null) {
            sEquipment = new ArrayList<String>();
        }
        return this.sEquipment;
    }

    /**
     * Gets the value of the containerGroup property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the containerGroup property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getContainerGroup().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getContainerGroup() {
        if (containerGroup == null) {
            containerGroup = new ArrayList<String>();
        }
        return this.containerGroup;
    }

    /**
     * Gets the value of the shipmentStop property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the shipmentStop property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getShipmentStop().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getShipmentStop() {
        if (shipmentStop == null) {
            shipmentStop = new ArrayList<String>();
        }
        return this.shipmentStop;
    }

    /**
     * Gets the value of the location property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the location property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLocation().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getLocation() {
        if (location == null) {
            location = new ArrayList<String>();
        }
        return this.location;
    }

    /**
     * Gets the value of the shipUnit property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the shipUnit property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getShipUnit().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getShipUnit() {
        if (shipUnit == null) {
            shipUnit = new ArrayList<String>();
        }
        return this.shipUnit;
    }

    /**
     * Gets the value of the sShipUnit property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the sShipUnit property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSShipUnit().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getSShipUnit() {
        if (sShipUnit == null) {
            sShipUnit = new ArrayList<String>();
        }
        return this.sShipUnit;
    }

    /**
     * Obtém o valor da propriedade shipUnitViewInfo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipUnitViewInfo() {
        return shipUnitViewInfo;
    }

    /**
     * Define o valor da propriedade shipUnitViewInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipUnitViewInfo(String value) {
        this.shipUnitViewInfo = value;
    }

    /**
     * Gets the value of the release property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the release property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRelease().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getRelease() {
        if (release == null) {
            release = new ArrayList<String>();
        }
        return this.release;
    }

    /**
     * Gets the value of the shipmentOrderRelease property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the shipmentOrderRelease property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getShipmentOrderRelease().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getShipmentOrderRelease() {
        if (shipmentOrderRelease == null) {
            shipmentOrderRelease = new ArrayList<String>();
        }
        return this.shipmentOrderRelease;
    }

    /**
     * Gets the value of the packagedItem property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the packagedItem property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPackagedItem().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getPackagedItem() {
        if (packagedItem == null) {
            packagedItem = new ArrayList<String>();
        }
        return this.packagedItem;
    }

    /**
     * Gets the value of the shipUnitSpec property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the shipUnitSpec property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getShipUnitSpec().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getShipUnitSpec() {
        if (shipUnitSpec == null) {
            shipUnitSpec = new ArrayList<String>();
        }
        return this.shipUnitSpec;
    }

    /**
     * Obtém o valor da propriedade rateoffering.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRATEOFFERING() {
        return rateoffering;
    }

    /**
     * Define o valor da propriedade rateoffering.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRATEOFFERING(String value) {
        this.rateoffering = value;
    }

    /**
     * Obtém o valor da propriedade rategeo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRATEGEO() {
        return rategeo;
    }

    /**
     * Define o valor da propriedade rategeo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRATEGEO(String value) {
        this.rategeo = value;
    }

    /**
     * Gets the value of the transOrder property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the transOrder property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTransOrder().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getTransOrder() {
        if (transOrder == null) {
            transOrder = new ArrayList<String>();
        }
        return this.transOrder;
    }

    /**
     * Gets the value of the shipmentStatus property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the shipmentStatus property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getShipmentStatus().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getShipmentStatus() {
        if (shipmentStatus == null) {
            shipmentStatus = new ArrayList<String>();
        }
        return this.shipmentStatus;
    }

    /**
     * Obtém o valor da propriedade flightinstance.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFLIGHTINSTANCE() {
        return flightinstance;
    }

    /**
     * Define o valor da propriedade flightinstance.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFLIGHTINSTANCE(String value) {
        this.flightinstance = value;
    }

    /**
     * Obtém o valor da propriedade secondaryCharges.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSecondaryCharges() {
        return secondaryCharges;
    }

    /**
     * Define o valor da propriedade secondaryCharges.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSecondaryCharges(String value) {
        this.secondaryCharges = value;
    }

    /**
     * Obtém o valor da propriedade primaryShipmentRefInfo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrimaryShipmentRefInfo() {
        return primaryShipmentRefInfo;
    }

    /**
     * Define o valor da propriedade primaryShipmentRefInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrimaryShipmentRefInfo(String value) {
        this.primaryShipmentRefInfo = value;
    }

    /**
     * Obtém o valor da propriedade driver.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDriver() {
        return driver;
    }

    /**
     * Define o valor da propriedade driver.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDriver(String value) {
        this.driver = value;
    }

    /**
     * Obtém o valor da propriedade powerUnit.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPowerUnit() {
        return powerUnit;
    }

    /**
     * Define o valor da propriedade powerUnit.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPowerUnit(String value) {
        this.powerUnit = value;
    }

}
